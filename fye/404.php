<?php get_header(); ?>

<div id="content" class="content">
	<div class="wrap">

			<h1 class="main-title">404 Not Found</h1>
            <p>We apologize, but the page you were looking for was not found.</p>

	</div><!-- .wrap -->
</div><!-- #content -->

<?php get_footer(); ?>