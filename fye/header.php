<!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<?php // Custom Fonts ?>
<script src="//use.typekit.net/ppq7ssm.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<link rel="stylesheet" type="text/css" href="//cloud.typography.com/668448/688326/css/fonts.css" />
<link href='https://fonts.googleapis.com/css?family=Bitter:400,700|Copse' rel='stylesheet' type='text/css'>
<?php // END Custom Fonts ?>

<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<div id="header">
	<div class="wrap">
    	<h2 class="tagline"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo bloginfo('name'); ?></a></h2>
    	<a href="#" id="nav_toggle"><i class="mobile-icon closed"></i> <span>Menu</span></a>
		<h1 class="logo"><a href="http://www.wpi.edu" title="Worcester Polytechnic Institute">Worcester Polytechnic Institute</a></h1>
	</div>
</div>

<div id="nav">
	<div class="wrap">
    	<?php
		$args = array(
			'container'		=> '',
			'theme_location'=> 'main_menu',
			'menu_id' 		=> 'menu-main-menu',
			'depth'			=> 3,
			'menu_class'	=> 'sf-menu'
		);
		wp_nav_menu($args);
		?>
	</div>
</div>