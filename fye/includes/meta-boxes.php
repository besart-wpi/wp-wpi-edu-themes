<?php
/**
 * Initialize the custom Meta Boxes. 
 */
add_action( 'admin_init', 'custom_meta_boxes' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function custom_meta_boxes() {
  
  /**
   * Create a custom meta boxes array that we pass to 
   * the OptionTree Meta Box API Class.
   */
   
   /* -- Metabox for Slideshow -- */
   $home_page_slide_properties = array(
		'id'          => 'home_page_slide_properties',
		'title'       => __( 'Slide Properties', 'fye_theme' ),
		'desc'        => '',
		'pages'       => array( 'home_page_slideshow' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'          => 'image',
				'label'       => __( 'Image', 'fye_theme' ),
				'std'         => '',
				'type'        => 'upload',	
				'class'       => 'ot-upload-attachment-id',			
			),
			array(
				'id'          => 'caption',
				'label'       => __( 'Caption', 'fye_theme' ),
				'std'         => '',
				'type'        => 'textarea-simple',				
			),
			array(
				'id'          => 'url',
				'label'       => __( 'Caption URL', 'fye_theme' ),
				'std'         => '',
				'desc'		  => __( 'Optional', 'fye_theme' ),
				'type'        => 'text',				
			),
		)
	);
	
	
	/* -- Metabox for Photo Gallery post type -- */
	if(isset($_GET['post']) || isset($_POST['post_ID'])):
		if(isset($_POST['post_ID'])){
			$post_id = $_POST['post_ID'];
		}
		
		if(isset($_GET['post'])){
			$post_id = $_GET['post'];
		}
	endif;
    $gallery_properties = array(
		'id'          => 'gallery_properties',
		'title'       => __( 'Gallery Properties', 'fye_theme' ),
		'desc'        => '',
		'pages'       => array( 'gallery' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'          => 'demo_textblock_titled',
				'label'       => __( 'Shortcode', 'fye_theme' ),
				'desc'        => '[galleria id="'.$post_id.'"]',
				'std'         => '',
				'type'        => 'textblock-titled',
			),
			array(
				'id'          => 'images',
				'label'       => __( 'Images', 'fye_theme' ),
				'std'         => '',
				'type'        => 'gallery'		
			),
		)
	);
	
	/* -- Metabox for 2-Column Content on Home Page -- */
    $home_page_2col_content = array(
		'id'          => 'home_page_2col_content',
		'title'       => __( '2-Column Content', 'fye_theme' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'          => 'layout_variation',
				'label'       => __( 'Layout Variation', 'fye_theme' ),
				'std'         => '',
				'type'        => 'select',	
				'choices'     => array( 
					array(
						'value'       => 'with_headings',
						'label'       => __( 'With Top Headings (default)', 'fye_theme' ),
						'src'         => ''
					),
					array(
						'value'       => 'without_headings',
						'label'       => __( 'Without Top Headings', 'fye_theme' ),
						'src'         => ''
					),
				)		
			),
			array(
				'id'          => 'column1',
				'label'       => __( 'Column 1 (Left)', 'fye_theme' ),
				'std'         => '',
				'type'        => 'list-item',	
				'settings'    => array( 
					// We get "Title" for free
					array(
						'id'          => 'content',
						'label'       => __( 'Content', 'fye_theme' ),
						'desc'        => '',
						'std'         => '',
						'type'        => 'textarea',
					),
					array(
						'id'          => 'heading_top',
						'label'       => __( 'Heading Top', 'fye_theme' ),
						'desc'        => '',
						'std'         => '',
						'type'        => 'text',
					),
					array(
						'id'          => 'expiration_date',
						'label'       => __( 'Expiration Date', 'fye_theme' ),
						'desc'        => 'Select a date after which this content should no longer be displayed',
						'std'         => '',
						'type'        => 'date-picker',
					)
				)
			),
			array(
				'id'          => 'column2',
				'label'       => __( 'Column 2 (Right)', 'fye_theme' ),
				'std'         => '',
				'type'        => 'list-item',	
				'settings'    => array( 
					// We get "Title" for free
					array(
						'id'          => 'content',
						'label'       => __( 'Content', 'fye_theme' ),
						'desc'        => '',
						'std'         => '',
						'type'        => 'textarea',
					),
					array(
						'id'          => 'heading_top',
						'label'       => __( 'Heading Top', 'fye_theme' ),
						'desc'        => '',
						'std'         => '',
						'type'        => 'text',
					),
					array(
						'id'          => 'expiration_date',
						'label'       => __( 'Expiration Date', 'fye_theme' ),
						'desc'        => 'Select a date after which this content should no longer be displayed',
						'std'         => '',
						'type'        => 'date-picker',
					)
				)
			),
		)
	);
	
	/* -- Metabox for Lower Callout on Home Page -- */
    $home_page_lower_callout = array(
		'id'          => 'home_page_lower_callout',
		'title'       => __( 'Lower Callout', 'fye_theme' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'          => 'lower_callout_heading',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text',		
			),
			array(
				'id'          => 'lower_callout_text',
				'label'       => __( 'Text', 'fye_theme' ),
				'std'         => '',
				'type'        => 'textarea',		
			),
			array(
				'id'          => 'lower_callout_image',
				'label'       => __( 'Image', 'fye_theme' ),
				'std'         => '',
				'type'        => 'upload',	
				'class'       => 'ot-upload-attachment-id',	
			),
		)
	);
	
	
	/* -- Metabox for Icon Content on "Date Page" Template -- */
	$date_page_icon_content = array(
		'id'          => 'date_page_icon_content',
		'title'       => __( 'Icon Content', 'fye_theme' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
		
			// Introduction
			array(
				'label'       => __( 'Introduction', 'fye_theme' ),
				'id'          => 'icon_content_tab0',
				'type'        => 'tab'
			),
			array(
				'id'          => 'icon_content_introduction',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => 'This optional content displays directly above the icons',
				'type'        => 'textarea',				
			),
			
			// Academics (1)
			array(
				'label'       => __( 'Academics', 'fye_theme' ),
				'id'          => 'icon_content_tab1',
				'type'        => 'tab'
			),
			array(
				'id'          => 'icon_content_heading1',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'icon_content_content1',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Housing & Dining (2)
			array(
				'label'       => __( 'Housing & Dining', 'fye_theme' ),
				'id'          => 'icon_content_tab2',
				'type'        => 'tab'
			),
			array(
				'id'          => 'icon_content_heading2',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'icon_content_content2',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Health Services (3)
			array(
				'label'       => __( 'Health Services', 'fye_theme' ),
				'id'          => 'icon_content_tab3',
				'type'        => 'tab'
			),
			array(
				'id'          => 'icon_content_heading3',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'icon_content_content3',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Get Connected (4)
			array(
				'label'       => __( 'Get Connected', 'fye_theme' ),
				'id'          => 'icon_content_tab4',
				'type'        => 'tab'
			),
			array(
				'id'          => 'icon_content_heading4',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'icon_content_content4',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Student Life (5)
			array(
				'label'       => __( 'Student Life', 'fye_theme' ),
				'id'          => 'icon_content_tab5',
				'type'        => 'tab'
			),
			array(
				'id'          => 'icon_content_heading5',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'icon_content_content5',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Additional Information (6)
			array(
				'label'       => __( 'Additional Information', 'fye_theme' ),
				'id'          => 'icon_content_tab6',
				'type'        => 'tab'
			),
			array(
				'id'          => 'icon_content_heading6',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'icon_content_content6',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Financial (7)
			array(
				'label'       => __( 'Financial', 'fye_theme' ),
				'id'          => 'icon_content_tab7',
				'type'        => 'tab'
			),
			array(
				'id'          => 'icon_content_heading7',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'icon_content_content7',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
		)
	);
	
	
	/* -- Metabox for Steps on "Course Registration" Template -- */
	$course_registration_steps = array(
		'id'          => 'course_registration_steps',
		'title'       => __( 'Icon Content', 'fye_theme' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			
			// Step 1
			array(
				'label'       => __( 'Step 1', 'fye_theme' ),
				'id'          => 'step_tab1',
				'type'        => 'tab'
			),
			array(
				'id'          => 'step_heading1',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'step_content1',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Step 2
			array(
				'label'       => __( 'Step 2', 'fye_theme' ),
				'id'          => 'step_tab2',
				'type'        => 'tab'
			),
			array(
				'id'          => 'step_heading2',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'step_content2',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Step 3
			array(
				'label'       => __( 'Step 3', 'fye_theme' ),
				'id'          => 'step_tab3',
				'type'        => 'tab'
			),
			array(
				'id'          => 'step_heading3',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'step_content3',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Step 4
			array(
				'label'       => __( 'Step 4', 'fye_theme' ),
				'id'          => 'step_tab4',
				'type'        => 'tab'
			),
			array(
				'id'          => 'step_heading4',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'step_content4',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Step 5
			array(
				'label'       => __( 'Step 5', 'fye_theme' ),
				'id'          => 'step_tab5',
				'type'        => 'tab'
			),
			array(
				'id'          => 'step_heading5',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'step_content5',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
			// Step 6
			array(
				'label'       => __( 'Step 6', 'fye_theme' ),
				'id'          => 'step_tab6',
				'type'        => 'tab'
			),
			array(
				'id'          => 'step_heading6',
				'label'       => __( 'Heading', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'	
			),
			array(
				'id'          => 'step_content6',
				'label'       => __( 'Content', 'fye_theme' ),
				'std'         => '',
				'desc'		  => '',
				'type'        => 'textarea',				
			),
			
		)
	);
	
	
	/* -- Metabox for "Timeline to Campus" Page Template -- */
	$timeline_to_campus = array(
		'id'          => 'timeline_to_campus',
		'title'       => __( 'Timeline to Campus Page Details', 'fye_theme' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
		
			// Splash
			array(
				'label'       => __( 'Splash', 'fye_theme' ),
				'id'          => 'tab_splash',
				'type'        => 'tab'
			),
			array(
				'id'          => 'splash_image',
				'label'       => __( 'Splash Image', 'fye_theme' ),
				'std'         => '',
				'type'        => 'upload',	
				'class'       => 'ot-upload-attachment-id',				
			),
			array(
				'id'          => 'splash_caption',
				'label'       => __( 'Splash Caption', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			
			// Date Buttons
			array(
				'label'       => __( 'Date Buttons', 'fye_theme' ),
				'id'          => 'tab_date_buttons',
				'type'        => 'tab'
			),
			// Yellow
			array(
				'id'          => 'button_yellow_label',
				'label'       => __( 'Yellow Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_yellow_url',
				'label'       => __( 'Yellow Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			// Green
			array(
				'id'          => 'button_green_label',
				'label'       => __( 'Green Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_green_url',
				'label'       => __( 'Green Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			// Blue
			array(
				'id'          => 'button_blue_label',
				'label'       => __( 'Blue Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_blue_url',
				'label'       => __( 'Blue Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			// Purple
			array(
				'id'          => 'button_purple_label',
				'label'       => __( 'Purple Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_purple_url',
				'label'       => __( 'Purple Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			// Black
			array(
				'id'          => 'button_black_label',
				'label'       => __( 'Black Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_black_url',
				'label'       => __( 'Black Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			
		)
	);


	/* -- Metabox for "New Student Orientation" Page Template -- */
	$new_student_orientation = array(
		'id'          => 'new_student_orientation',
		'title'       => __( 'New Student Orientation Page Details', 'fye_theme' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
		
			// Splash
			array(
				'label'       => __( 'Splash', 'fye_theme' ),
				'id'          => 'tab_splash',
				'type'        => 'tab'
			),
			array(
				'id'          => 'splash_image',
				'label'       => __( 'Splash Image', 'fye_theme' ),
				'std'         => '',
				'type'        => 'upload',	
				'class'       => 'ot-upload-attachment-id',				
			),
			array(
				'id'          => 'splash_caption',
				'label'       => __( 'Splash Caption', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			
			// Date Buttons
			array(
				'label'       => __( 'Date Buttons', 'fye_theme' ),
				'id'          => 'tab_date_buttons',
				'type'        => 'tab'
			),
			// Yellow
			array(
				'id'          => 'button_yellow_label',
				'label'       => __( 'Yellow Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_yellow_url',
				'label'       => __( 'Yellow Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			// Green
			array(
				'id'          => 'button_green_label',
				'label'       => __( 'Green Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_green_url',
				'label'       => __( 'Green Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			// Blue
			array(
				'id'          => 'button_blue_label',
				'label'       => __( 'Blue Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_blue_url',
				'label'       => __( 'Blue Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			// Purple
			array(
				'id'          => 'button_purple_label',
				'label'       => __( 'Purple Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_purple_url',
				'label'       => __( 'Purple Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			// Black
			array(
				'id'          => 'button_black_label',
				'label'       => __( 'Black Button - Label', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'			
			),
			array(
				'id'          => 'button_black_url',
				'label'       => __( 'Black Button - URL', 'fye_theme' ),
				'std'         => '',
				'type'        => 'text'		
			),
			
		)
	);

  
  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
	if ( function_exists( 'ot_register_meta_box' ) ):		
		
		$template_file = '';
		$post_id = '';
		
		/* -- Get Template File Name & Post ID -- */
		if(isset($_GET['post']) || isset($_POST['post_ID'])):
		
			if(isset($_POST['post_ID'])){
				$post_id = $_POST['post_ID'];
			}
			
			if(isset($_GET['post'])){
				$post_id = $_GET['post'];
			}
			
			if($post_id) {
				$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);				
			}
		endif;
		
		
		/* ================================
		Set Metabox for custom post
		=================================== */
		ot_register_meta_box( $home_page_slide_properties );
		ot_register_meta_box( $gallery_properties );
		
		
		/* ================================
		Set Metabox for common page except home page
		=================================== */
		if($post_id != get_option('page_on_front')){
			
		}
		
		/* ================================
		Set Metabox for specific page
		=================================== */		
		if($post_id == get_option('page_on_front')){
			/* -- Set metabox for Front Page -- */		
			ot_register_meta_box( $home_page_2col_content );	
			ot_register_meta_box( $home_page_lower_callout );
				
		} elseif($template_file == 'page-templates/date-page.php') {
			/* -- Set metabox for Date Page -- */
			ot_register_meta_box( $date_page_icon_content );	
			
		} elseif($template_file == 'page-templates/course-registration.php'){
			/* -- Set metabox for Course Registration -- */
			ot_register_meta_box( $course_registration_steps );	
			
		} elseif($template_file == 'page-templates/timeline-to-campus.php'){
			/* -- Set metabox for Timeline to Campus page -- */
			ot_register_meta_box( $timeline_to_campus );	
			
		} elseif($template_file == 'page-templates/new-student-orientation.php'){
			/* -- Set metabox for New Student Orientation page -- */
			ot_register_meta_box( $new_student_orientation );	
			
		} else {
			// Nothing Yet		
		}
		
	endif;

}