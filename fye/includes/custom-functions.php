<?php
// =================================================
//	:: Get Step Content ::
// -------------------------------------------------
add_action( 'wp_ajax_getstep', 'getstep_callback' );
add_action( 'wp_ajax_nopriv_getstep', 'getstep_callback' );

function getstep_callback(){
	$nonce = $_POST['nonce'];
	$num = $_POST['num'];
	
	if ( ! wp_verify_nonce( $nonce, 'fye-ajax-nonce' ) )
		die ( 'Nope!' );
		
	// Get step content...
	
	exit;
}	

function strip_p( $content ){
	
	$content = str_replace('<p>', '', $content);
	$content = str_replace('</p>', "\n", $content);	
	$content = trim($content);
	
	return $content;
}