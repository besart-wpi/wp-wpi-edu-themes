<?php
// =================================================
//	:: Custom Post Type :: Home Page Slideshow ::
// -------------------------------------------------
add_action('init', 'cpt_home_page_slideshow');
function cpt_home_page_slideshow() {
	$labels = array(
			'name' => _x('Home Page Slideshow', 'post type general name'),
			'singular_name' => _x('Slide', 'post type singular name'),
			'add_new' => _x('Add New', 'home_page_slideshow'),
			'add_new_item' => __('Add Slide'),
			'edit_item' => __('Edit Slide'),
			'new_item' => __('New Slide'),
			'all_items' => __('All Slides'),
			'view_item' => __('View Slide'),
			'search_items' => __('Search Slides'),
			'not_found' =>  __('No Slides found'),
			'not_found_in_trash' => __('No Slides found in Trash'), 
			'parent_item_colon' => '',
			'menu_name' => 'Home Page Slideshow'
	);
	
	$args = array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'_builtin' =>  false,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',			
			'hierarchical' => false,
			'exclude_from_search' => true,
			'rewrite' => array("slug" => "slideshow"),
			'menu_icon' => 'dashicons-format-gallery',
			'supports' => array('title')
	);
	register_post_type( 'home_page_slideshow' , $args );
}


// =================================================
//	:: Custom Post Type :: Photo Gallery ::
// -------------------------------------------------
add_action('init', 'cpt_gallery');
function cpt_gallery() {
	$labels = array(
			'name' => _x('Gallery', 'post type general name'),
			'singular_name' => _x('Gallery', 'post type singular name'),
			'add_new' => _x('Add New', 'gallery'),
			'add_new_item' => __('Add Gallery'),
			'edit_item' => __('Edit Gallery'),
			'new_item' => __('New Gallery'),
			'all_items' => __('All Galleries'),
			'view_item' => __('View Gallery'),
			'search_items' => __('Search Galleries'),
			'not_found' =>  __('No Galleries found'),
			'not_found_in_trash' => __('No Galleries found in Trash'), 
			'parent_item_colon' => '',
			'menu_name' => 'Photo Galleries'
	);
	
	$args = array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'_builtin' =>  false,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',			
			'hierarchical' => false,
			'exclude_from_search' => true,
			'rewrite' => array("slug" => "gallery"),
			'menu_icon' => 'dashicons-images-alt2',
			'supports' => array('title')
	);
	register_post_type( 'gallery' , $args );
}