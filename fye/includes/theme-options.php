<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
	$custom_settings = array( 
		
		/* ======== Section Settings ========= */
		'sections'        => array(
			array(
				'id'          => 'footer_options',
				'title'       => __( 'Footer Options', 'fye_theme' )
			),
			array(
				'id'          => 'home_page_options',
				'title'       => __( 'Home Page Options', 'fye_theme' )
			),
		),
		
		/* ======== Fields Settings ========= */
		'settings'        => array(
			
			/* ---- Footer Options ------------------------------ */			
			array(
				'id'          => 'footer_facebook_url',
				'label'       => __( 'Facebook URL', 'fye_theme' ),
				'type'        => 'text',
				'section'     => 'footer_options'
			),
			array(
				'id'          => 'footer_twitter_url',
				'label'       => __( 'Twitter URL', 'fye_theme' ),
				'type'        => 'text',
				'section'     => 'footer_options'
			),
			array(
				'id'          => 'footer_instagram_url',
				'label'       => __( 'Instagram URL', 'fye_theme' ),
				'type'        => 'text',
				'section'     => 'footer_options'
			),
			array(
				'id'          => 'footer_address_and_contact_details',
				'label'       => __( 'Address and Contact Details', 'fye_theme' ),
				'type'        => 'textarea',
				'section'     => 'footer_options'
			),
			array(
				'id'          => 'footer_contact_form_heading',
				'label'       => __( 'Contact Form Heading', 'fye_theme' ),
				'type'        => 'text',
				'section'     => 'footer_options'
			),
			array(
				'id'          => 'footer_contact_form7',
				'label'       => __( 'Contact Form 7', 'fye_theme' ),
				'desc'		  => __( 'Please select an existing Contact Form 7 form', 'fye_theme' ),
				'type'        => 'custom-post-type-select',
				'post_type'   => 'wpcf7_contact_form',
				'section'     => 'footer_options'
			),
			
			
			/* ---- Home Page Options ------------------------------ */	
			array(
				'id'          => 'home_page_slideshow_pause_time',
				'label'       => __( 'Slideshow - Pause Time', 'fye_theme' ),
				'desc'		  => __( 'in milliseconds, defaults to 5000 (5 seconds)', 'fye_theme' ),
				'type'        => 'text',
				'section'     => 'home_page_options'
			),
			array(
				'id'          => 'home_page_slideshow_transition_time',
				'label'       => __( 'Slideshow - Transition Time', 'fye_theme' ),
				'desc'		  => __( 'in milliseconds, defaults to 1000 (1 second)', 'fye_theme' ),
				'type'        => 'text',
				'section'     => 'home_page_options'
			),
			
			
		) /* --- End Fields Settings --- */
	);
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
}