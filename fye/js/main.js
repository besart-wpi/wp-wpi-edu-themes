jQuery( document ).ready(function($) {
	
	
	// Mobile Nav Behavior
	$main_menu = $('#menu-main-menu');
	// Clone for mobile
	$main_menu.clone().appendTo('#nav').attr('id', 'mobile-menu');
	$mobile_menu = $('#mobile-menu');
	
	$('#nav_toggle').click( function(event){
		$(this).find('i').toggleClass('opened closed'); // Toggle Icon
		$mobile_menu.stop().slideToggle(400, 'swing', function(){
			$(this).css('height', 'auto');	
		});
		event.preventDefault();
	});
	
	// Second level drop-downs
	$mobile_menu.find('ul.sub-menu').attr('style','');
	$('> li.menu-item-has-children', $mobile_menu).each(function(){
		$(this).append(
			$('<div class="subbut"><i class="fa fa-plus"></i></div>')
			.click(function(event){
				// Toggle Icon
				$('i', this).toggleClass('fa-plus fa-minus');
				// Hide other expanded menus
				$('ul.sub-menu', $mobile_menu).stop().slideUp('fast');
				
				// Expand our menu
				$(this).parent().find('ul.sub-menu').stop().slideToggle(); 
				event.preventDefault();
			})
		);
	});
	
	// Superfish Menus
	$main_menu.superfish();
	
	
	// Buttons
	$('.btn-colord').wrapInner('<span></span>');
	
	
	// FitVids - Make videos responsive
	// $('.video-holder').fitVids();
	$('.wrap iframe[src*="youtube.com"]').each(function() {
		$(this).wrap( "<div class='video-holder'></div>" );
	});
	$('.wrap').fitVids({ ignore: '.main_box'}); // "main_box" class relates to YouTube Playlist Shortcode plugin
	
	
	// Accordion Tabs
	$('.ac-tab').on('click',function(){
		var ac = $(this).next('.ac-content');
		if( ac.is(':hidden') ){
			ac.slideDown();
			$(this).removeClass('icon-expand').addClass('icon-collapse');
		}else{
			ac.slideUp();
			$(this).removeClass('icon-collapse').addClass('icon-expand');
		}
		return false;
	});
	
	
	// Add expand icons
	$('.list-dl li').addClass('icon-expand');
	
	
	// Smooth Scroll Icon Anchor Links
	$('.btn-circles a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - 50 // Add some margin at the top
				}, 1000);
				return false;
			}
		}
	});
	
	
	// Step Button Behavior
	$('.btn-steps a').click(function(event) {
		var target = $(this.hash);
		var stepsTop = $('#steps_top');
		
		// Check if target is already visible
		if( $(target).is(":visible") ){
			// Just scroll to it
			$('html,body').animate({
				scrollTop: stepsTop.offset().top - 50 // Add some margin at the top
			}, 1000);
		} else {
			// Handle button active state
			$('.btn-steps .item').removeClass('active');
			$(this).addClass('active');
			
			// Hide others
			$('.step-info').stop().hide();
			// Show target
			$(target).stop().fadeIn();
			// Scroll to target
			$('html,body').animate({
				scrollTop: stepsTop.offset().top - 50 // Add some margin at the top
			}, 1000);
		}
		
		event.preventDefault();
	});
	
	// Add class to Callout Box (feedback) when heading contains an icon
	$('.feedback').each(function(){
		if( $('*[class^="icon"]', this).length ){
			$(this).addClass('hasIcon');	
		}
	});
	
	// IE8 fallback for nth-child(even/odd)
	$("#content .sked-buttons ul ul li:nth-child(even)").addClass("even");
    $("#content .sked-buttons ul ul li:nth-child(odd)").addClass("odd");
	$("#content .sked-buttons ul ul li:last-child").addClass("last");
	
	
	// Get Step content via AJAX
	function ajax_step(num){
		//console.log( "action=members&nonce="+ajax_var.nonce+"&type="+type+"&key="+key+"&col="+column );
		$.ajax({
			type: "post",
			url: ajax_var.url, 
			data: "action=getstep&nonce="+ajax_var.nonce+"&num="+num,
			success: function(response){
				console.log( response );
			}
		});
	}
	window.ajax_step = ajax_step;
	
});