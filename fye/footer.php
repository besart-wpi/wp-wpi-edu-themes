<div id="footer">
	<div class="wrap">
		<div class="col">
        	<?php 
			$footer_facebook_url = ot_get_option('footer_facebook_url');
			$footer_twitter_url = ot_get_option('footer_twitter_url');
			$footer_instagram_url = ot_get_option('footer_instagram_url');
			
			if($footer_facebook_url || $footer_twitter_url): ?>
			<ul class="follow">
				<?php echo $footer_facebook_url ? '<li><a href="'.esc_url($footer_facebook_url).'" target="_blank" class="btn-fb">Find Us on Facebook</a></li>' : ''; ?>
				<?php echo $footer_twitter_url ? '<li><a href="'.esc_url($footer_twitter_url).'" target="_blank" class="btn-tw">Follow Us on Twitter</a></li>' : ''; ?>
                <?php echo $footer_instagram_url ? '<li class="last"><a href="'.esc_url($footer_instagram_url).'" target="_blank" class="btn-insta">Find Us on Instagram</a></li>' : ''; ?>
			</ul>
			<?php endif; ?>
			
			<?php 
			/*
			echo ot_get_option('footer_contact_form_heading') ? '<h2 class="footer_contact_heading">'.ot_get_option('footer_contact_form_heading').'</h2>' : ''; 
			echo ot_get_option('footer_contact_form7') ? do_shortcode('[contact-form-7 id="'.ot_get_option('footer_contact_form7').'"]') : ''; 
			*/
			?>
		</div>
		<div class="col last">
<!--			<div class="clear"></div> -->
			<a href="http://www.wpi.edu" class="logo-footer" title="Worcester Polytechnic Institute">Worcester Polytechnic Institute</a>
			<div class="contact-info">
				<?php echo ot_get_option('footer_address_and_contact_details'); ?>
			</div>
			<div class="clear"></div>
			<div class="copy">&copy; <?php echo date('Y'); ?> Worcester Polytechnic Institute. All rights reserved.</div>
		</div>
	</div>
</div>
	
<?php wp_footer(); ?>

</body>
</html>