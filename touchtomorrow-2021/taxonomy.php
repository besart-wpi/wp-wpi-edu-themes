<?php get_header(); ?>

	<?php get_template_part("template-parts/_taxonomy-hero");?>
	<?php get_template_part("template-parts/_taxonomy-intro");?>
			
	<?php 	if (have_posts()) :?>
	<?php 	$post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

	<?php 	get_template_part("template-parts/_loop_posts");?>

	<?php 	else: ?>

			<h3 class="text-center mb-5">Nothing found</h3>

	<?php 	endif; ?>

<?php get_footer(); ?>
