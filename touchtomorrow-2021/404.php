<?php get_header(); ?>

<div class="home-banner overflow-hidden no-hero-image">
  <div class="caption">
    <div class="container">
      <div class="wrapper">
        <h1 class="the-title">404 Not Found</h1>
				<p class="the-desc">The page You are looking for was not found.</p>
      </div>
    </div>
  </div>
  <!-- .caption -->
</div>
<!-- .home-banner -->

<?php get_footer(); ?>