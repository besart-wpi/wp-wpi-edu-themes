<?php 
// Register Custom Post Type
function custom_post_type() {
  // Activity Post Type
	$activity_labels = array(
		'name'                  => _x( 'Activities', 'Post Type General Name', 'wpmix' ),
		'singular_name'         => _x( 'Activity', 'Post Type Singular Name', 'wpmix' ),
		'menu_name'             => __( 'Activities', 'wpmix' ),
		'name_admin_bar'        => __( 'Activity', 'wpmix' ),
		'archives'              => __( 'Activity Archives', 'wpmix' ),
		'attributes'            => __( 'Activity Attributes', 'wpmix' ),
		'parent_item_colon'     => __( 'Parent Activity:', 'wpmix' ),
		'all_items'             => __( 'All Activities', 'wpmix' ),
		'add_new_item'          => __( 'Add New Activity', 'wpmix' ),
		'add_new'               => __( 'Add New', 'wpmix' ),
		'new_item'              => __( 'New Activity', 'wpmix' ),
		'edit_item'             => __( 'Edit Activity', 'wpmix' ),
		'update_item'           => __( 'Update Activity', 'wpmix' ),
		'view_item'             => __( 'View Activity', 'wpmix' ),
		'view_items'            => __( 'View Activities', 'wpmix' ),
		'search_items'          => __( 'Search Activity', 'wpmix' ),
		'not_found'             => __( 'Not found', 'wpmix' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wpmix' ),
		'featured_image'        => __( 'Featured Image', 'wpmix' ),
		'set_featured_image'    => __( 'Set featured image', 'wpmix' ),
		'remove_featured_image' => __( 'Remove featured image', 'wpmix' ),
		'use_featured_image'    => __( 'Use as featured image', 'wpmix' ),
		'insert_into_item'      => __( 'Insert into activity', 'wpmix' ),
		'uploaded_to_this_item' => __( 'Uploaded to this activity', 'wpmix' ),
		'items_list'            => __( 'Activities list', 'wpmix' ),
		'items_list_navigation' => __( 'Activities list navigation', 'wpmix' ),
		'filter_items_list'     => __( 'Filter activities list', 'wpmix' ),
	);
	$activity_args = array(
		'label'                 => __( 'Activity', 'wpmix' ),
		'labels'                => $activity_labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		//'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'       			=> 'dashicons-universal-access-alt',
	);
	register_post_type( 'activity', $activity_args );

}
add_action( 'init', 'custom_post_type', 0 );