<?php
// Define Menus
function wpmix_setup_theme(){
	// Theme Supports
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );


	// Define Thumbnail Sizes
	add_image_size( 'hero-image-m', 750, 420, true ); 
	add_image_size( 'hero-image-t', 1024, 574, true ); 
	add_image_size( 'hero-image-d', 2560, 1066, true ); 
	add_image_size( 'carousel-image-m', 756, 424, true ); 
	add_image_size( 'carousel-image-t', 1076, 604, true ); 
	add_image_size( 'carousel-image-d-half', 720, 405, true ); 
	add_image_size( 'carousel-image-d', 1440, 810, true ); 
	add_image_size( 'activity-image-m', 678, 448, true ); 
	add_image_size( 'activity-image-t', 840, 560, true ); 
	add_image_size( 'activity-image-d', 960, 640, true ); 
	//add_image_size( 'sponsor-logo', 192, 110, false ); 
	add_image_size( 'sponsor-logo', 520, 240, false ); 
	add_image_size( 'carousel-gallery-m', 338, 190, true ); 
	add_image_size( 'carousel-gallery-t', 538, 243, true ); 
	add_image_size( 'carousel-gallery-d', 720, 405, true ); 


	// register menus
	register_nav_menus( 

		array( 
			'menu-column-1' => __( 'Menu Column 1', 'amd' ), 
			'menu-column-2' => __( 'Menu Column 2', 'amd' ), 
			'wpi-menu' => __( 'WPI Menu', 'amd' ), 
			'copyright-menu' => __( 'Copyright Menu', 'amd' ), 
		) 

	);
}
add_action( 'after_setup_theme', 'wpmix_setup_theme' );