<?php
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title'    => __('Theme Settings'),
		'menu_title'    => __('Theme Settings'),
		'menu_slug'     => 'theme-settings',
		'capability'    => 'edit_posts',
		'redirect'      => true
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Header',
		'menu_title'	=> 'Header',
		'menu_slug' 	=> 'theme-header',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'parent' 		=> 'theme-settings'
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'menu_slug' 	=> 'theme-footer',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'parent' 		=> 'theme-settings'
	));
	
}
