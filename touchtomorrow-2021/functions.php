<?php
// All Includes
include( get_template_directory() . '/inc/setup.php' );

include( get_template_directory() . '/inc/enqueue.php' );

include( get_template_directory() . '/inc/custom_post_types.php' );

include( get_template_directory() . '/inc/custom_taxonomies.php' );

include( get_template_directory() . '/inc/widgets.php' );

include( get_template_directory() . '/inc/hooks.php' );

include( get_template_directory() . '/inc/shortcodes.php' );

include( get_template_directory() . '/inc/custom_functions.php' );

include( get_template_directory() . '/inc/theme_options.php' ); 

include( get_template_directory() . '/inc/scpo/simple-custom-post-order.php' ); 

include( get_template_directory() . '/inc/expand_search.php' ); 