<?php 
$title = get_sub_field('title');
$sponsors = get_sub_field('sponsors');
$button = get_sub_field('button');
$button_style = get_sub_field('button_style');
$button_foreground_color = get_sub_field('button_foreground_color');
$button_background_color = get_sub_field('button_background_color');
$heading_rule_color = get_sub_field('heading_rule_color');
$background_color = get_sub_field('background_color');
if(empty($background_color)) $background_color = 'white';
$background_image = get_sub_field('background_image');
$background_image_alignment = get_sub_field('background_image_alignment');
$sponsor_image_size = get_sub_field('sponsor_image_size');
if( have_rows('sponsors') ): ?>
<div class="sponsors overflow bg-color-<?php echo $background_color;?>">
  <div class="container relative text-center">
    <?php 
    if($background_image){  
      echo wp_get_attachment_image( $background_image['ID'], 'full', false, array( 'class' => 'background-image align-'.$background_image_alignment ) );
    }
    ?>
    <?php if(!empty($heading_rule_color) && $heading_rule_color!=='none'):?>
      <div class="heading-rule bg-<?php echo $heading_rule_color;?>"></div>
    <?php endif;?>
    
    <?php if( $title ): ?>
    	<h2 class="section-title"><?php echo $title;?></h2>
    <?php endif; ?>

    <div class="sponsor-logos d-flex flex-wrap justify-content-center">
    <?php while( have_rows('sponsors') ): the_row(); ?>
      <?php 
      $sponsor_image = get_sub_field('sponsor_image');
      $link = get_sub_field('link');
      $open_link_in_a_new_tab = get_sub_field('open_link_in_a_new_tab');
      ?>
      <div class="the-logo size-<?php echo $sponsor_image_size;?>">
        <a
          href="<?php echo $link;?>"
          class="d-flex align-items-center justify-content-center"
          <?php echo($open_link_in_a_new_tab==true)?'target="_blank"':'';?>
        >
          <?php echo wp_get_attachment_image( $sponsor_image['ID'], 'sponsor-logo', false, array( 'class' => '' ) ); ?>
        </a>
      </div>
    <?php endwhile;?>
    </div>
    <!-- .sponsor-logos -->
		
    <?php if( $button ): ?>
    <a href="<?php echo $button['url'];?>" target="<?php echo $button['target'];?>" class="the-button <?php echo $button_background_color;?> <?php echo $button_style;?> fg-<?php echo $button_foreground_color;?>"><?php echo $button['title'];?></a>
    <?php endif; ?>
  </div>
</div>
<!-- .sponsors -->
<?php endif; ?>