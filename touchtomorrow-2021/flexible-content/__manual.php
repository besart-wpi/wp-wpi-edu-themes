<?php 
$image = get_sub_field('image');
$image_alignment = get_sub_field('image_alignment');
$youtube_video_url = get_sub_field('youtube_video_url');
$heading = get_sub_field('heading');
$content = get_sub_field('content');
?>
<div class="the-activity">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-thumb <?php echo ($image_alignment=='Right') ? 'order-md-2':'';?>">
      <?php if(!empty($youtube_video_url)):?>
        <a
          href="<?php echo $youtube_video_url;?>"
          data-fancybox
          class="the-thumb"
          ><?php echo wp_get_attachment_image( $image['ID'], 'activity-image-d', false, array( 'class' => '' ) ); ?>
          <span class="overlay d-flex align-items-center justify-content-center">
            <span class="play-button"
              ><span class="icon-play-triangle"></span
            ></span> </span
        ></a>
      <?php else:?>
        <div class="the-thumb"><?php echo wp_get_attachment_image( $image['ID'], 'activity-image-d', false, array( 'class' => '' ) ); ?></div>
      <?php endif;?>
      </div>
      <div class="col-md-6 col-details <?php echo ($image_alignment=='Right') ? 'order-md-1':'';?>">
        <div class="the-title font2 text-navy-blue font-weight-bold"
          ><?php echo $heading;?></div
        >

        <div class="the-desc"><?php echo $content;?></div>
      </div>
    </div>
  </div>
</div>
<!-- .the-activity -->