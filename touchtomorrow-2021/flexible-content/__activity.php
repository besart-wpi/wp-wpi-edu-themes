<?php 
$post = get_sub_field('activity');
$image_alignment = get_sub_field('image_alignment');
if($post):
setup_postdata($post); 
?>
<div class="the-activity">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-thumb <?php echo ($image_alignment=='Right') ? 'order-md-2':'';?>">
        <a href="<?php the_permalink();?>" class="the-thumb">
        <?php if(has_post_thumbnail()):?>
          <?php get_thumb('activity-image-d', '', '');?>
          <?php else:?>
          <img src="<?php bloginfo("template_url");?>/img/placeholder/activity-image-d.jpg" alt="<?php echo esc_attr($post->post_title);?>"/>
        <?php endif;?>
        <?php echo get_section_tag($post);?>
        </a>
      </div>
      <div class="col-md-6 col-details <?php echo ($image_alignment=='Right') ? 'order-md-1':'';?>">
        <a href="<?php the_permalink();?>" class="the-title font2 text-navy-blue font-weight-bold"
          ><?php the_title();?></a
        >

        <div class="the-desc">
        	<?php 
        	if ( ! has_excerpt() ) {
        		the_content();
        	} else { 
        		the_excerpt();
        	} ?>
        </div>

        <?php $terms = get_the_terms( $post->ID, 'age-group' );?>
        <?php if($terms):?>
        <div class="post-meta font2">
          <span class="text-uppercase font-weight-bold text-navy-blue">Ages:</span> <?php echo get_terms_links($terms);?>
        </div>
        <?php endif;?>
        
        <?php $terms = get_the_terms( $post->ID, 'activity-category' );?>
        <?php if($terms):?>
        <div class="post-meta font2">
          <span class="text-uppercase font-weight-bold text-navy-blue">Category:</span> <?php echo get_terms_links($terms);?>
        </div>
        <?php endif;?>

        <?php $terms = get_the_terms( $post->ID, 'discipline' );?>
        <?php if($terms):?>
        <div class="post-meta font2">
          <span class="text-uppercase font-weight-bold text-navy-blue">Disciplines:</span> <?php echo get_terms_links($terms);?>
        </div>
        <?php endif;?>

        <?php /* $terms = get_the_terms( $post->ID, 'section' );?>
        <?php if($terms):?>
        <div class="post-meta font2">
          <span class="text-uppercase font-weight-bold text-navy-blue">Sections:</span> <?php echo get_terms_links($terms);?>
        </div>
        <?php endif; */?>

        <?php 
				$sponsor_link = get_field('sponsor_link');
				$sponsor_link_label = get_field('sponsor_link_label') ? get_field('sponsor_link_label') : 'Sponsored by';
				
				if($sponsor_link):?>
          <div class="post-meta font2 spsr-box">
            <span class="text-uppercase font-weight-bold text-navy-blue"><?php echo $sponsor_link_label; ?>:</span> <a href="<?php echo $sponsor_link['url'];?>" target="<?php echo $sponsor_link['target'];?>"><?php echo $sponsor_link['title'];?></a>
          </div>
        <?php endif;?>

        <a
          href="<?php the_permalink();?>"
          class="view-activity font2 font-weight-bold text-wpi-red"
          >View the Activity <span class="icon-angle-right"></span
        ></a>
      </div>
    </div>
  </div>
</div>
<!-- .the-activity -->
<?php wp_reset_postdata();?>
<?php 
endif;?>