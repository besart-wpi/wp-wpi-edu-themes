<?php 
$image = get_sub_field('image');
$title = get_sub_field('title');
if(empty($title)) $title = get_the_title();
$text = get_sub_field('text');
$button_type = get_sub_field('button_type');
$provided_by_wpi = get_sub_field('provided_by_wpi');
?>
<div class="home-banner">
  <div class="gradient-shade"></div>
  <div class="hero-image-wrapper">
    <?php echo wp_get_attachment_image( $image['ID'], 'hero-image-d', false, array( 'class' => 'bg d-none d-xl-block' ) ); ?>
    <?php echo wp_get_attachment_image( $image['ID'], 'hero-image-t', false, array( 'class' => 'bg d-xl-none' ) ); ?>
    <?php if($provided_by_wpi):?>
    	<span class="wpi-tag d-md-none"><a href="https://www.wpi.edu" target="_blank"><img src="<?php bloginfo("template_url");?>/img/brought-to-you-by-wpi.png" alt="Brought to you by WPI"></a></span>
    <?php endif;?>
  </div>
  
  <div class="caption">
    <div class="container">
      <div class="wrapper">
        <h1 class="the-title"><?php echo $title;?></h1>
        <?php if(!empty($text)):?>
        <p class="the-desc">
          <?php echo $text;?>
        </p>
        <?php endif;?>
        
        <?php if(!empty($button_type) && $button_type!=='None'):?>
        
          <?php if($button_type=='Link'):
            $button_link = get_sub_field('button_link');?>
          <a
            href="<?php echo $button_link['url'];?>" target="<?php echo $button_link['target'];?>"
            class="the-button yellow"
            ><span class="icon-play-triangle"></span> <?php echo $button_link['title'];?></a
          >
          <?php endif;?>
        
          <?php if($button_type=='Video'):
            $button_youtube_url = get_sub_field('button_youtube_url');?>
          <a
            href="<?php echo $button_youtube_url['url'];?>"
            data-fancybox
            class="the-button yellow"
            ><span class="icon-play-triangle"></span> <?php echo $button_youtube_url['title'];?></a
          >
          <?php endif;?>

        <?php endif;?>

        <?php get_template_part("template-parts/_socialmedia-share");?>
      </div>
    </div>
  </div>
  <!-- .caption -->
	<?php if($provided_by_wpi):?>
	<span class="wpi-tag d-none d-md-block d-lg-block d-xl-block"><a href="https://www.wpi.edu" target="_blank"><img src="<?php bloginfo("template_url");?>/img/brought-to-you-by-wpi.png" alt="Brought to you by WPI"></a></span>
	<?php endif;?>
</div>
<!-- .home-banner -->