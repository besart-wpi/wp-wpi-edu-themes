<?php 
$heading = get_sub_field('heading');
$background_color = get_sub_field('background_color');
$foreground_color = get_sub_field('foreground_color');
?>
<div class="carousel-gallery overflow bg-<?php echo $background_color;?> text-<?php echo $foreground_color;?>">
	
  <?php if( $heading ): ?>
  	<h2 class="the-title text-center"><?php echo $heading;?></h2>
  <?php endif; ?>
  <div class="carousel-wrapper relative">
    <button type="button" class="slick-prev-slide">
      <span class="sr-only">Previous Slide</span>
      <span class="icon-angle-left"></span>
    </button>
    <button type="button" class="slick-next-slide">
      <span class="sr-only">Next Slide</span>
      <span class="icon-angle-right"></span>
    </button>
    <div class="the-carousel">
      <?php if( have_rows('slides') ): ?>
      <?php while( have_rows('slides') ): the_row(); ?>
          <?php 
          $image = get_sub_field('image'); 
          $link_type = get_sub_field('link_type'); 
          $youtube_url = get_sub_field('youtube_url'); 
          $link_url = get_sub_field('link_url'); 
          $caption = get_sub_field('caption');
          $section = get_sub_field('section'); // term ID
			
          $before_image = $link_url && $link_type == 'url' ? '<a href="'.esc_url($link_url).'">' : '';
          $after_image = $link_url && $link_type == 'url' ? '</a>' : '';
          ?>
          <div class="the-slide">
            <?php if(!empty($youtube_url) && $link_type == 'video'):?>
            <a class="the-image" href="<?php echo $youtube_url;?>" data-fancybox>
              <?php echo wp_get_attachment_image( $image['ID'], 'carousel-gallery-d', false, array( 'class' => '' ) ); ?>
              <span
                class="overlay d-flex align-items-center justify-content-center"
              >
                <span class="play-button"
                  ><span class="icon-play-triangle"></span
                ></span>
              </span>
              <?php 
              if( $section ){
              	$section_term = get_term( $section, 'section' );
              	if( !is_wp_error($section_term) ){
              		$section_color = get_field('section_color', $section_term);
              		echo '<span class="image-tag bg-'.$section_color.' text-white">'.$section_term->name.'</span>';
              	}
              } ?>
            </a>
            <?php else:?>
            <div class="the-image">
              <?php echo $before_image; ?>
              <?php echo wp_get_attachment_image( $image['ID'], 'carousel-gallery-d', false, array( 'class' => '' ) ); ?>
              <?php 
              if( $section ){
              	$section_term = get_term( $section, 'section' );
              	if( !is_wp_error($section_term) ){
              		$section_color = get_field('section_color', $section_term);
              		echo '<span class="image-tag bg-'.$section_color.' text-white">'.$section_term->name.'</span>';
              	}
              } ?>
              <?php echo $after_image; ?>
            </div>
            <?php endif;?>
            <div class="the-caption"><?php echo $caption;?></div>
          </div>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<!-- .caroousel-gallery -->