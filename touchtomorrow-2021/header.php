<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php if (is_search()) { ?>
			<meta name="robots" content="noindex, nofollow" />
	<?php } ?>

	<title><?php wp_title( '-', true, 'right' ); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>

	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
</head>

<body <?php body_class(); ?>>

	<ul class="skip-navigation">
		<li><a href="#content-top">Skip navigation</a></li>
	</ul>
	
	<header class="d-flex align-items-center justify-content-center <?php echo !has_subhero() ? 'no-hero-image' : '';?>">
		<div class="container">
			<div class="wrapper d-flex justify-content-between align-items-center">
				<a href="<?php echo home_url(); ?>" class="logo">
					<img
						src="<?php bloginfo("template_url");?>/img/logo-main.svg"
						alt="Touch Tomorrow"
						class="main-logo"
					/>
					<img src="<?php bloginfo("template_url");?>/img/logo-main-alt.svg" alt="Touch Tomorrow" class="alt" />
				</a>
				<button class="menu-open hamburger hamburger--slider" type="button" aria-label="Primary Navigation Menu">
					<span class="hamburger-label d-none d-md-block">
						<span class="closed">Menu</span>
						<span class="opened">Close</span>
					</span>
					<span class="hamburger-box">
						<?php /* SVG source in /img/icon-menu-search.svg */ ?>
						<svg version="1.1" id="MenuIcon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 60.2 51.9" xml:space="preserve">
							<path d="M17.6,47.7c4.2-3.6,8-6.9,11.4-9.8c-6-10-3.3-19.6,4.4-24.7c6.6-4.3,16-3.4,21.6,2.2c5.9,5.8,6.8,14.6,2.2,21.5
								C52.2,44.6,41,46.2,33,41.3c-3.4,3.3-7.5,6.9-11.3,10.6C20.5,50.8,19.6,49.6,17.6,47.7z M55,27.5c0-6.6-5.5-12-12.1-12
								c-6.5,0.1-11.7,5.2-11.8,11.6c-0.1,6.8,5,12.1,11.8,12.2C49.5,39.4,55,34.1,55,27.5z"/>
							<path d="M0,0c16.3,0,32.1,0,48.3,0l0.1,4.9c-16.4,0-32,0-48.3,0C0,3.3,0,1.7,0,0z"/>
							<path d="M0,18.7c0-1.8,0-3.2,0-5c7.2,0,14.2,0,21.4,0c0,1.8,0,3.2,0,5C14.3,18.7,7.5,18.7,0,18.7z"/>
							<path d="M18,27.2c0,1.7,0,2.9,0,4.7c-5.9,0-11.9,0-18,0v-4.7C6.1,27.2,11.8,27.2,18,27.2z"/>
						</svg>
						<?php /* SVG source in /img/icon-close.svg */ ?>
						<svg version="1.1" id="CloseIcon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 60.2 51.9" style="enable-background:new 0 0 60.2 51.9;" xml:space="preserve">
						<path d="M34.8,26L53.9,45l-4.7,4.7L30.1,30.7L11,49.8L6.3,45L25.4,26L6.3,6.9L11,2.1l19.1,19.1L49.2,2.1l4.7,4.7L34.8,26z"/>
						</svg>
					</span>
				</button>
			</div>
		</div>
	</header>

	<nav class="megamenu" id="navigation" role="navigation">
		<div class="container">
			<div class="row row-search-form">
				<div class="col-md-12">
					<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<div class="input-group">
							<input type="search" class="search-field form-control" placeholder="Search for an activity, event, topic, etc..." value="<?php echo get_search_query(); ?>" name="s" />
							<input type="submit" class="search-submit" value="Search" />
						</div>
					</form>
				</div>
			</div>
			<div class="row justify-content-between">
				<div class="col-megamenu col-md-8 col-sm-6">
					<label><?php echo get_field('menu_heading', 'options');?></label>

					<div class="row">
						<div class="col-md-6">
							<?php wp_nav_menu( array(
									'theme_location' => 'menu-column-1',
									'menu_class' => 'the-menu',
									'container' => false,
									'fallback_cb' => false,
							) );?>
						</div>
						<div class="col-md-6">
							<?php wp_nav_menu( array(
									'theme_location' => 'menu-column-2',
									'menu_class' => 'the-menu',
									'container' => false,
									'fallback_cb' => false,
							) );?>
						</div>
					</div>
				</div>

				<div
					class="wpi-menu-col col-md-4 col-sm-6 external-links d-flex justify-content-center"
				>
					<div class="wrapper">
						<label><?php echo get_field('wpi_menu_heading', 'options');?></label>
						
						<?php 
						$wpi_menu = wp_get_nav_menu_items('wpi-menu');
						if(is_array($wpi_menu)):
							foreach($wpi_menu as $wpi_menu_item):?>
							<a href="<?php echo $wpi_menu_item->url; ?>" target="_blank" class="the-button wpi-red lined"
								><?php echo $wpi_menu_item->post_title; ?>&nbsp;<span class="icon-link"></span
							></a>
							<?php 
							endforeach;
						endif;?>
					</div>
				</div>
			</div>
		</div>
	</nav>
	<!-- .megamenu -->
	
	<div id="content-top"></div>