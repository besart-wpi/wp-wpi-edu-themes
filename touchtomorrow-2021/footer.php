	<div class="socialmedia-links" id="socialmedia-links">
		<div class="container">
			<ul>
				<?php if( get_field('facebook_url', 'options') ): ?>
				<li>
					<a class="text-navy-blue" href="<?php echo get_field('facebook_url', 'options');?>" target="_blank"
						><img src="<?php bloginfo("template_url");?>/img/icon-facebook-social.svg" alt="Facebook Link" />
						Facebook</a
					>
				</li>
				<?php endif; ?>
				<?php if( get_field('twitter_url', 'options') ): ?>
				<li>
					<a class="text-navy-blue" href="<?php echo get_field('twitter_url', 'options');?>" target="_blank"
						><img src="<?php bloginfo("template_url");?>/img/icon-twitter-social.svg" alt="Twitter Link" />
						Twitter</a
					>
				</li>
				<?php endif; ?>
				<?php if( get_field('instagram_url', 'options') ): ?>
				<li>
					<a class="text-navy-blue" href="<?php echo get_field('instagram_url', 'options');?>" target="_blank"
						><img src="<?php bloginfo("template_url");?>/img/icon-instagram-social.svg" alt="Instagram Link" />
						Instagram</a
					>
				</li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
	<!-- .socialmedia-links -->
	

	<footer class="text-white bg-blue text-center text-md-left" id="footer">
		<div class="footer-content footer__runway">
			<img src="<?php bloginfo("template_url");?>/img/robot.svg" id="robot" alt="Robot" />
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-6 col-lg-5" id="frunway__left">
						<img
							src="<?php bloginfo("template_url");?>/img/logo-main.svg"
							alt="Touch Tomorrow"
							class="the-logo logo-main"
						/>

						<div class="row">
							<div class="col-md-6">
								<?php wp_nav_menu( array(
									'theme_location' => 'menu-column-1',
									'menu_class' => '',
									'container' => false,
									'depth' => 1,
									'fallback_cb' => false,
								) );?>
							</div>
							<div class="col-md-6">
								<?php wp_nav_menu( array(
									'theme_location' => 'menu-column-2',
									'menu_class' => '',
									'container' => false,
									'depth' => 1,
									'fallback_cb' => false,
								) );?>
							</div>
						</div>
					</div>
					<div class="col-md-3" id="frunway__right">
						<a href="https://www.wpi.edu" target="_blank"><img src="<?php bloginfo("template_url");?>/img/logo-wpi.svg" alt="WPI" class="the-logo logo-wpi" /></a>

						<?php 
						$wpi_menu = wp_get_nav_menu_items('wpi-menu');
						if(is_array($wpi_menu)):
							echo '<ul class="external-links">';
							foreach($wpi_menu as $wpi_menu_item):?>
							<li><a href="<?php echo $wpi_menu_item->url; ?>" target="_blank"
								><span class="the-text"><?php echo $wpi_menu_item->post_title; ?></span> <span class="icon-link"></span
							></a></li>
							<?php 
							endforeach;
							echo '</ul>';
						endif;?>
					</div>
				</div>
			</div>
		</div>

		<div class="copyright bg-navy-blue footer__copyright">
			<div
				class="container d-md-flex align-items-center justify-content-start"
			>
				&copy; <?php echo date("Y");?> Worcester Polytechnic Institute
				<?php wp_nav_menu( array(
						'theme_location' => 'copyright-menu',
						'menu_class' => '',
						'container' => false,
						'depth' => 1,
						'fallback_cb' => false,
					) );?>
			</div>
		</div>
	</footer>

	
	<?php wp_footer(); ?>

	<!-- Don't forget analytics -->

</body>

</html>
