<?php get_header(); ?>

	<div class="home-banner overflow-hidden no-hero-image">
		<div class="caption">
			<div class="container">
				<div class="wrapper" style="min-height:0">
					<h1 class="the-title">Search: <?php echo get_search_query(); ?></h1>
				</div>
			</div>
		</div>
		<!-- .caption -->
	</div>
	<!-- .home-banner -->

	

  <div class="container">
    <div class="row">
			
			<?php if (have_posts()) : ?>
				
				<?php while (have_posts()) : the_post(); ?>
					<div class="col-md-12 col-details my-4 pb-4 border-bottom">
						<h3><a href="<?php the_permalink();?>" class="the-title font2 text-navy-blue font-weight-bold"><?php the_title();?></a></h3>
						<div class="the-desc">
							<?php the_excerpt(); ?>
						</div>
						<a href="<?php the_permalink();?>" class="view-activity font2 font-weight-bold text-wpi-red">Read More<span class="icon-angle-right"></span></a>
					</div>
				<?php endwhile; ?>
				<?php get_template_part("template-parts/_nav");?>
			
			<?php else: ?>
				<div class="col-md-12 col-details my-4">
					<p>Sorry, no results were found.</p>
				</div>
			<?php endif; ?>
			
    </div>
  </div>

<?php get_footer(); ?>