<?php 
$current_taxonomy = get_query_var( 'taxonomy' );
$term = get_term_by( 'slug', get_query_var( 'term' ), $current_taxonomy);
$current_term_id = $term->term_id; 
$acf_term = $current_taxonomy.'_'.$current_term_id;

$introduction_title = get_field('introduction_title', $acf_term);
$introduction = get_field('introduction', $acf_term);
if(!empty($introduction_title) && !empty($introduction_title)):
?>
<div class="intro-text">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-8 text-center">
        <h2>
          <?php echo $introduction_title;?>
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="the-content column-1 text-center">
          <?php echo $introduction;?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- .intro-text -->
<?php 
endif;?>