<?php 
$title = get_the_title();
$text = get_the_content();
?>
<div class="home-banner overflow-hidden <?php echo (!has_post_thumbnail())?'no-hero-image':'';?>">
<?php if(has_post_thumbnail()):?>  
  <div class="gradient-shade"></div>
  <div class="hero-image-wrapper">
    <?php
    get_thumb('hero-image-d', 'bg d-none d-xl-block', '');
    get_thumb('hero-image-t', 'bg d-xl-none', '');
    ?>
  </div>
<?php endif;?>

  <div class="caption">
    <div class="container">
      <div class="wrapper">
        <h1 class="the-title"><?php the_title();?></h1>
        <p class="the-desc">
          <?php echo $text;?>
        </p>

        <?php get_template_part("template-parts/_socialmedia-share");?>
      </div>
    </div>
  </div>
  <!-- .caption -->
</div>
<!-- .home-banner -->