<?php while (have_posts()) : the_post(); ?>

	<?php 
	if(empty($image_alignment) || $image_alignment=='Right'){
		$image_alignment='Left';
	}
	else{
		$image_alignment='Right';
	}
	?>

	<?php get_template_part("template-parts/_the_post", null, array('image_alignment' => $image_alignment));?>

<?php endwhile;?>

<?php get_template_part("template-parts/_nav");?>