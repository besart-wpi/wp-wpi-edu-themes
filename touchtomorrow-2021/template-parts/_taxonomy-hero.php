<?php 
$current_taxonomy = get_query_var( 'taxonomy' );
$term = get_term_by( 'slug', get_query_var( 'term' ), $current_taxonomy);
$current_term_id = $term->term_id; 
$acf_term = $current_taxonomy.'_'.$current_term_id;

$image = get_field('hero_image', $acf_term);
$title = get_field('hero_title', $acf_term);
if(empty($title)){
  $title = $term->name;
}
$text = get_field('hero_text', $acf_term);
?>
<div class="home-banner overflow-hidden <?php echo (!$image || empty($image)) ? 'no-hero-image' : '';?>">
<?php if($image):?>
  <div class="gradient-shade"></div>
  <div class="hero-image-wrapper">
    <?php echo wp_get_attachment_image( $image['ID'], 'hero-image-d', false, array( 'class' => 'bg d-none d-xl-block' ) ); ?>
    <?php echo wp_get_attachment_image( $image['ID'], 'hero-image-t', false, array( 'class' => 'bg d-xl-none' ) ); ?>
  </div>
<?php endif;?>

  <div class="caption">
    <div class="container">
      <div class="wrapper">
        <h1 class="the-title"><?php echo $title;?></h1>
        <?php if( $text ): ?>
        <p class="the-desc">
          <?php echo $text;?>
        </p>
        <?php endif; ?>
        <?php get_template_part("template-parts/_socialmedia-share");?>
      </div>
    </div>
  </div>
  <!-- .caption -->
</div>
<!-- .home-banner -->