jQuery(function($){
	"use strict";
	
	// Set z-index for each page section
	$('.section-page').each(function(i, c) {
        $(c).css('z-index', 10 - i);
    });
	
	// Add hover class (for ie8)
	$('li').hover( 
		function(){
			$(this).addClass('hover');
		}, function() {
			$(this).removeClass('hover');
		}
	);
	
	// Floatlabels
	// http://clubdesign.github.io/floatlabels.js/
	$('input.floatlabel, textarea.floatlabel').floatlabel();	
	
	
	// Add first and last section classes
	$('.section-page:first-child').addClass('first-section');
	$('.section-page:last-child').addClass('last-section');
	
	
	// Window Resize
	$(window).resize(_.debounce(function(){
		
		$('.et-vid a').unbind('click').on('click', function(){			
			var youtubeID = $(this).data('youtube');
			var episode_num = $(this).data('episode_num');
			var vid_title = $(this).data('vid_title');
			var vid_desc = $(this).data('vid_desc');
				
			if($(window).width() > 980){
				$('#main-video .main-vid').html('<iframe src="http://www.youtube.com/embed/'+ youtubeID +'?wmode=transparent&rel=0&autoplay=1" width="320" height="240" frameborder="0" allowfullscreen="true"></iframe>');
				$('#main-video .main-vid-desc span.episode_num').html(episode_num);
				$('#main-video .main-vid-desc span.vid_title').html(vid_title);
				$('#main-video .main-vid-desc p').html(vid_desc);

				$('html, body').stop().animate({
					 'scrollTop': $('#main-video').offset().top - 200
				}, 500, 'swing');				
			} else {
				var container = $(this).closest('.et-vid');
				$('.et-vid').not(container).find('.vid-mobile-wrap').remove();
				var videoCode = '<div class="vid-mobile-wrap"><iframe src="http://www.youtube.com/embed/'+ youtubeID +'?wmode=transparent&rel=0&autoplay=1" width="320" height="240" allowfullscreen="true"></iframe></div>';
				container.append(videoCode);
			}
			
			return false;
		});
		
		if($(window).width() > 980){
			$('.et-vid').find('.vid-mobile-wrap').remove();
		}
		
	}, 500));
	$(window).resize();
	
	
	// Video heading behavior
	$('.evd-heading a').on('click', function(){
		$(this).closest('.episode-entry').find('.ee-inner').slideToggle();
		$(this).toggleClass('active-vdt');
		return false;
	});
	
	
	// One page navigation
	$('#nav').onePageNav({
		currentClass: 'current-active-nav',
		scrollSpeed: 800,
		begin: function() {
			if($(window).width() < 980){
				$('#nav').fadeOut(200);
			}
		}
	});
	
	
	// Radio inputs
	$('.form-radio input').each(function(){
		var self = $(this),
		label = self.next(),
		label_text = label.text();
		
		label.remove();
		self.iCheck({
			insert: label_text
		});
	});
	
	// Mobile drop-down
	$('#show-menu').on('click', function(event){
		$('#nav').fadeToggle(200);
		event.preventDefault();
	});
	
});