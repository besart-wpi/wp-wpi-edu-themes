<?php /* Template Name: Find Out More */ ?>
<?php get_header(); ?>
	
	<div id="main">
		<?php if(have_posts()): while(have_posts()): the_post(); ?>
			<?php		
			$counter = 1;
			get_template_part('template-parts/find_out_more');
			?>
		<?php endwhile; endif; ?>		
	</div><!-- end #main -->

<?php get_footer();