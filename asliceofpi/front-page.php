<?php get_header(); ?>
	
	<div id="main">
		
		<?php
		$args = array(
			'post_type' => 'page',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'posts_per_page'=> -1
		);
		$loop = new WP_Query($args);		
		$counter = 1;
		if($loop->have_posts()) : while ( $loop->have_posts() ) : $loop->the_post();		

			$template_file = get_post_meta($post->ID, '_wp_page_template', TRUE);
			$template_name = basename($template_file, '.php');
			
			switch($template_name){
				case 'template-episodes':
					get_template_part('template-parts/episodes');
					break;
				case 'template-about':
					get_template_part('template-parts/about');
					break;
				case 'template-cast':
					get_template_part('template-parts/cast');
					break;
				case 'template-findoutmore':
					get_template_part('template-parts/find_out_more');
					break;
			}	
		
		endwhile; endif; wp_reset_postdata();
		
		?>		

		
	</div><!-- end #main -->

<?php get_footer();