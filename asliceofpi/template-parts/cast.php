<div id="<?php echo $post->post_name; ?>" class="section-cast section-page">
	<div class="container">
	
		<div class="section-title clearfix">
			<div class="st-number">
				<div class="st-number-bg"></div>
				<?php global $counter; ?>
				<div class="st-number-num"><?php echo $counter; ?>.</div>
				<?php $counter++; ?>
			</div>
			<h2><?php the_title(); ?></h2>
		</div>
		<div class="clear"></div>
		
		<div class="cast-content clearfix">
			<div class="cast-member-wrap clearfix">
				
				<?php
				$cast_left_photo = get_post_meta($post->ID, '_cast_left_photo', true);
				$cast_left_name = get_post_meta($post->ID, '_cast_left_name', true);
				$cast_left_twitter = trim(get_post_meta($post->ID, '_cast_left_twitter', true));
				$cast_left_bio = get_post_meta($post->ID, '_cast_left_bio', true);
				?>
				<div class="cast-member cm-left">
					<div class="cm-photo">
						<img src="<?php echo theme_get_ot_image_url($cast_left_photo, 'thumb-photo'); ?>" alt="">							
					</div>
					<h3 class="cm-name"><?php echo trim($cast_left_name); ?></h3>
					<p class="cm-twitter"><a href="https://twitter.com/<?php echo str_replace('@', '', $cast_left_twitter); ?>" target="_blank"><?php echo $cast_left_twitter; ?></a></p>
					<div class="cm-desc clearfix the-content">
						<?php echo wpautop($cast_left_bio); ?>
					</div>
				</div>
				
				<div class="cast-decor"><img src="<?php bloginfo('template_url'); ?>/images/three_triangle2.png" alt=""></div>

				<?php
				$cast_right_photo = get_post_meta($post->ID, '_cast_right_photo', true);
				$cast_right_name = get_post_meta($post->ID, '_cast_right_name', true);
				$cast_right_twitter = trim(get_post_meta($post->ID, '_cast_right_twitter', true));
				$cast_right_bio = get_post_meta($post->ID, '_cast_right_bio', true);
				?>
				<div class="cast-member cm-right">
					<div class="cm-photo">
						<img src="<?php echo theme_get_ot_image_url($cast_right_photo, 'thumb-photo'); ?>" alt="">							
					</div>
					<h3 class="cm-name"><?php echo trim($cast_right_name); ?></h3>
					<p class="cm-twitter"><a href="https://twitter.com/<?php echo str_replace('@', '', $cast_right_twitter); ?>" target="_blank"><?php echo $cast_right_twitter; ?></a></p>
					<div class="cm-desc clearfix the-content">
						<?php echo wpautop($cast_right_bio); ?>
					</div>
				</div>	
				
			</div>
		</div>
		
	</div>
	
	
	<div class="bottom-decoration"><img src="<?php bloginfo('template_url'); ?>/images/decor_bottom_cast.png" alt=""></div>
</div>