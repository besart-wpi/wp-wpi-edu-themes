<div id="<?php echo $post->post_name; ?>" class="section-find-out-more section-page">
	<div class="container">
	
		<div class="section-title clearfix">
			<div class="st-number">
				<div class="st-number-bg"></div>
				<?php global $counter; ?>
				<div class="st-number-num"><?php echo $counter; ?>.</div>
				<?php $counter++; ?>
			</div>
			<h2><?php the_title(); ?></h2>
		</div>
		<div class="clear"></div>
		
		<div class="content-find-out-more clearfix the-content">
			<?php the_content(); ?>			
		</div>		
		
	</div>
	
</div>