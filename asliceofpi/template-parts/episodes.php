<div id="<?php echo $post->post_name; ?>" class="section-episodes section-page">
	<div class="container">
		<div class="section-title clearfix">
			<div class="st-number">
				<div class="st-number-bg"></div>
				<?php global $counter; ?>
				<div class="st-number-num"><?php echo $counter; ?>.</div>
				<?php $counter++; ?>
			</div>
			<h2><?php the_title(); ?></h2>
		</div>
		<div class="clear"></div>
		
		<div class="episodes-container">
			
			<?php $youtube_videos = get_post_meta($post->ID, '_youtube_videos', true); ?>
			<?php if(!empty($youtube_videos)): ?>
				<?php $first_video = $youtube_videos[0]; ?>
				<div id="main-video">
					<div class="main-vid">
						<?php
						preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $first_video['youtube_url'], $matchex);
						$yt_id = $matchex[1];
						if($yt_id): ?>
							<iframe src="http://www.youtube.com/embed/<?php echo $yt_id; ?>?wmode=transparent&rel=0" width="320" height="240" allowfullscreen="true"></iframe>
						<?php
						endif;
						?>				
					</div>
					<div class="main-vid-desc">						
						<h3 class="video-desc-title">Episode <span class="episode_num" style="color:#fff"><?php echo $first_video['episode_number']; ?></span>: <span class="vid_title"><?php echo $first_video['title']; ?></span></h3>
						<?php echo wpautop(stripslashes($first_video['description'])); ?>
					</div>
				</div>
				
				<div class="episodes-thumb-wrap clearfix">
					<?php $count = 1; ?>
					<?php foreach($youtube_videos as $video): ?>
					
						<div class="episode-entry ee-<?php echo $count; ?>"><div class="episode-entry-inner">
							<?php
							preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $video['youtube_url'], $matches);
							$yt_id = $matches[1];
							?>
							<div class="et-vid-desc evd-mobile evd-heading">
								<h3 class="video-desc-title"><a href="#">Episode <?php echo $video['episode_number']; ?>: <span><?php echo $video['title']; ?></span><i class="vdt-icon"></i></a></h3>
							</div>
							<div class="ee-inner">
								<div class="et-vid-desc evd-mobile evd-mobile-desc">								
									<?php echo wpautop(stripslashes($video['description'])); ?>
								</div>
								<div class="et-vid"><a href="<?php echo esc_url($video['youtube_url']); ?>" data-youtube="<?php echo $yt_id; ?>" data-episode_num="<?php echo $video['episode_number']; ?>" data-vid_title="<?php echo $video['title']; ?>" data-vid_desc="<?php echo stripslashes($video['description']); ?>">							
									<img src="https://img.youtube.com/vi/<?php echo $yt_id; ?>/0.jpg" alt="" class="youtube-thumbnail" />
									<img src="<?php bloginfo('template_url'); ?>/images/play.png" class="yt-play" alt="" />
								</a></div>
								<div class="et-vid-desc evd-desktop">
									<h3 class="video-desc-title">Episode <?php echo $video['episode_number']; ?>: <span><?php echo $video['title']; ?></span></h3>
									<?php echo wpautop(stripslashes($video['description'])); ?>
								</div>
							</div>
						</div></div>
						
						<?php
						if ($count % 2 == 0) {
							echo '<div class="clear"></div>';
						}
						$count++;
						?>
					
					<?php endforeach; ?>							
					
				</div>
			
			<?php endif; ?>
			
		</div>
		
	</div>
	
	<div class="bottom-decoration"><img src="<?php bloginfo('template_url'); ?>/images/decor_bottom_episodes.png" alt="" /></div>
	
</div>