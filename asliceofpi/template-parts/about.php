<div id="<?php echo $post->post_name; ?>" class="section-about section-page">
	<div class="container">
	
		<div class="section-title clearfix">
			<div class="st-number">
				<div class="st-number-bg"></div>
				<?php global $counter; ?>
				<div class="st-number-num"><?php echo $counter; ?>.</div>
				<?php $counter++; ?>
			</div>
			<h2><?php the_title(); ?></h2>
		</div>
		<div class="clear"></div>
		
		
		<div class="container-about">
			<div class="container-about-bg"><img src="<?php bloginfo('template_url'); ?>/images/bg_about.png" alt=""></div>
			
			<div class="about-content-wrap">
				
				<div class="about-banner-wrap"><div class="about-banner clearfix">
					<?php for($i = 1; $i <= 3; $i++):?>
						<?php
						$banner_image = get_post_meta($post->ID, '_banner_image_'.$i, true);
						if($banner_image):
						?>					
							<div class="banner-image bi-<?php echo $i; ?>">
								<img src="<?php echo theme_get_ot_image_url($banner_image, 'thumb-big'); ?>" alt="">
							</div>
						<?php
						endif;
						?>
					
					<?php endfor; ?>
				</div></div>
				
				<div class="about-content clearfix the-content">
					<?php
					$content_left = get_post_meta($post->ID, '_content_left', true);
					$content_right = get_post_meta($post->ID, '_content_right', true);
					?>
					<div class="about-left clearfix">
						<?php echo wpautop($content_left); ?>
					</div>
					<div class="ac-decor"><img src="<?php bloginfo('template_url'); ?>/images/three_triangle.png" alt=""></div>
					<div class="about-right clearfix">
						<?php echo wpautop($content_right); ?>
					</div>
				</div>
				
			</div>
		
		</div>
		
	</div>
	
	<div class="bottom-decoration"><img src="<?php bloginfo('template_url'); ?>/images/decor_bottom_about.png" alt=""></div>
</div>