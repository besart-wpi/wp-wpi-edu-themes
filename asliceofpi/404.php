<?php get_header(); ?>
	
	<div id="main">
		
			<div class="section-find-out-more section-page">
				<div class="container">
				
					<div class="section-title clearfix">
						<div class="st-number">
							<div class="st-number-bg"></div>							
							<div class="st-number-num"></div>
						</div>
						<h2>Not Found</h2>
					</div>
					<div class="clear"></div>
					
					<div class="content-find-out-more clearfix the-content">
						<h1>404 Not Found</h1>
						<p>We apologize, but the page you were looking for was not found.</p>
					</div>		
					
				</div>
				
			</div>
	
	</div><!-- end #main -->

<?php get_footer();