<?php
/**
 * Initialize the custom Meta Boxes. 
 */
add_action( 'admin_init', 'custom_meta_boxes' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function custom_meta_boxes() {
  
  /**
   * Create a custom meta boxes array that we pass to 
   * the OptionTree Meta Box API Class.
   */   
 
	
	/* -----------------------------------
	** Metabox for About Page
	----------------------------------- */
	$about_metabox = array(
		'id'          => 'about_metabox',
		'title'       => __( 'About Section Content', 'asop-backed' ),
		'desc'        => '',
		'pages'       => array( 'page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> 'banner_images',
				'label'		=> __( 'Banner Images', 'asop-backed' ),
				'type'		=> 'tab'
			),		
				array(
					'id'		=> '_banner_image_1',
					'label'		=> __( 'Image 1', 'asop-backed' ),
					'type'		=> 'upload',
					'class'		=> 'ot-upload-attachment-id'
				),
				array(
					'id'		=> '_banner_image_2',
					'label'		=> __( 'Image 2', 'asop-backed' ),
					'type'		=> 'upload',
					'class'		=> 'ot-upload-attachment-id'
				),
				array(
					'id'		=> '_banner_image_3',
					'label'		=> __( 'Image 3', 'asop-backed' ),
					'type'		=> 'upload',
					'class'		=> 'ot-upload-attachment-id'
				),
			array(
				'id'		=> 'content_left',
				'label'		=> __( 'Content Left', 'asop-backed' ),
				'type'		=> 'tab'
			),
				array(
					'id'		=> '_content_left',
					'label'		=> __( 'Content Left', 'asop-backed' ),
					'type'		=> 'textarea'
				),
			array(
				'id'		=> 'content_right',
				'label'		=> __( 'Content Right', 'asop-backed' ),
				'type'		=> 'tab'
			),
				array(
					'id'		=> '_content_right',
					'label'		=> __( 'Content Right', 'asop-backed' ),
					'type'		=> 'textarea'
				),	
		)
	);
	
	/* -----------------------------------
	** Metabox for Episodes Page
	----------------------------------- */	
	$youtube_videos_metabox = array(
		'id'          => 'youtube_videos_metabox',
		'title'       => __( 'YouTube Videos', 'asop-backed' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> '_youtube_videos',
				'label'		=> __( 'Videos', 'asop-backed' ),
				'type'		=> 'list-item',
				'settings'	=> array(
					array(
						'id'		=> 'episode_number',
						'label'		=> __( 'Episode Number', 'asop-backed' ),
						'type'		=> 'text'
					),
					array(
						'id'		=> 'youtube_url',
						'label'		=> __( 'YouTube URL', 'asop-backed' ),
						'type'		=> 'text'
					),
					array(
						'id'		=> 'description',
						'label'		=> __( 'Description', 'asop-backed' ),
						'type'		=> 'textarea-simple',
						'rows'		=> '3'
					),					
				)
			),	
		)
	);
	
	/* -----------------------------------
	** Metabox for Cast Page
	----------------------------------- */
	$cast_metabox = array(
		'id'          => 'cast_metabox',
		'title'       => __( 'Cast Section Content', 'asop-backed' ),
		'desc'        => '',
		'pages'       => array( 'page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> 'cast_left',
				'label'		=> __( 'Cast Member Left', 'asop-backed' ),
				'type'		=> 'tab'
			),		
				array(
					'id'		=> '_cast_left_photo',
					'label'		=> __( 'Photo', 'asop-backed' ),
					'type'		=> 'upload',
					'class'		=> 'ot-upload-attachment-id'
				),
				array(
					'id'		=> '_cast_left_name',
					'label'		=> __( 'Name', 'asop-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_cast_left_twitter',
					'label'		=> __( 'Twitter Handle', 'asop-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_cast_left_bio',
					'label'		=> __( 'Bio', 'asop-backed' ),
					'type'		=> 'textarea'
				),
			array(
				'id'		=> 'cast_right',
				'label'		=> __( 'Cast Member Right', 'asop-backed' ),
				'type'		=> 'tab'
			),		
				array(
					'id'		=> '_cast_right_photo',
					'label'		=> __( 'Photo', 'asop-backed' ),
					'type'		=> 'upload',
					'class'		=> 'ot-upload-attachment-id'
				),
				array(
					'id'		=> '_cast_right_name',
					'label'		=> __( 'Name', 'asop-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_cast_right_twitter',
					'label'		=> __( 'Twitter Handle', 'asop-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_cast_right_bio',
					'label'		=> __( 'Bio', 'asop-backed' ),
					'type'		=> 'textarea'
				),
		)
	);
	
	
  
  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
	if ( function_exists( 'ot_register_meta_box' ) ):		
		
		$template_file = '';
		$post_id = '';
		$post_type = '';
		
		/* -- Get Template File Name & Post ID -- */
		if(isset($_GET['post']) || isset($_POST['post_ID'])):
		
			if(isset($_POST['post_ID'])){
				$post_id = $_POST['post_ID'];
			}
			
			if(isset($_GET['post'])){
				$post_id = $_GET['post'];
			}
			
			if($post_id) {
				$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
				$post_type = get_post_type($post_id);
			}
		endif;	
		
		
	/* ================================
	Set Metabox for specific page
	=================================== */		
		if($template_file == 'page-templates/template-about.php'){		
			ot_register_meta_box( $about_metabox );				
		} elseif($template_file == 'page-templates/template-episodes.php'){
			ot_register_meta_box( $youtube_videos_metabox );		
		} elseif($template_file == 'page-templates/template-cast.php'){
			ot_register_meta_box( $cast_metabox );		
		}
	
		
		
	endif; // End if ( function_exists( 'ot_register_meta_box' ) )

} // End of function custom_meta_boxes()