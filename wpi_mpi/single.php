<?php get_header();
// =================================================
// :: Gets the .hero background ::
// -------------------------------------------------
if($featuredImage = get_post_thumbnail_id()){
	$featuredImage = get_post($featuredImage)->guid;
}

?>
	<div class="hero <?php if($featuredImage) echo 'imaged" style="background: url('. $featuredImage . '); background-position: center; background-repeat: no-repeat; background-size: cover; -ms-behavior: url('.get_bloginfo('template_url').'/js/backgroundsize.min.htc);'; ?>">
		<header>
			<h1>
				<?php if($featuredImage) echo '<span>' ?>
					<?php the_title() ?>
				<?php if($featuredImage) echo '</span>' ?>
			</h1>
		</header>
	</div>


	<section id="the-content">
		<div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Displays either content as full width or with sidebar
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$sidebar = getSidebar('single');
			$contentWidth = $sidebar ? 'c8' : 'c12';
			?>
			<article class="main <?= $contentWidth ?>">
				<?php get_template_part('loop', 'single') ?>
				<?php @comments_template() ?>
			</article>
			<?php if($sidebar): ?>
				<aside class="main c4">
					<?= apply_filters('the_content', $sidebar) ?>
				</aside>
			<?php endif; ?>
			<div class="clear"></div>
		</div>
		</div>
	</section>

<?php get_footer() ?>