<?php if ( is_active_sidebar('homefooter') ): ?>
	
    <aside id="sidebar_homefooter">
        <?php dynamic_sidebar( 'homefooter' ); ?>
    </aside>
    <div class="clear"></div>	
	
<?php endif; ?>