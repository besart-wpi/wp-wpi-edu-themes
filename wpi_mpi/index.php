<?php get_header() ?>
	<div class="hero">
		<header>
			<h1><?= ot_get_option('landing_pages_news_title') ?> <?= getArchiveLabel() ?></h1>
		</header>
	</div>

	<section id="the-content">
		<div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Displays either content as full width or with sidebar
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$sidebar = getSidebar('single');
			$contentWidth = $sidebar ? 'c8' : 'c12';
			?>
			<article class="main <?= $contentWidth ?>">
				<?php get_template_part('loop', 'index') ?>
				<?php posts_nav_link(); ?> 
			</article>
			<?php if($sidebar): ?>
				<aside class="main c4">
					<ul>
						<?= apply_filters('the_content', $sidebar) ?>
					</ul>
				</aside>
			<?php endif; ?>
			<div class="clear"></div>
		</div>
	</section>

<?php get_footer() ?>