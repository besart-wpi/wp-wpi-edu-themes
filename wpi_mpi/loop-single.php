<?php if(have_posts()):
	while(have_posts()): the_post(); ?>
		<h2><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h2>
		<div class="post-meta">
			<span><?= get_avatar($post->post_author, 24) ?> By <?php the_author_posts_link() ?></span> <span><?= the_time(get_option( 'date_format' )) ?></span>
		</div>
		<?php the_content() ?>
		<br>
		<br>
	<?php endwhile;
else: ?>
	<h2>Posts are coming soon!</h2>
<?php endif; ?>