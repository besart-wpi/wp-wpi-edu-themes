		<?php if($content = Oz::getField('content', 'footer-callout')): ?>
			<section id="footer-callout">
				<?= apply_filters('the_content', $content) ?>
			</section>
		<?php endif; ?>

		<div class="footer-wrap">
			<footer class="main">
				<div class="copyright c6">
					<?= apply_filters('the_content', ot_get_option('footer_copyright')) ?>
				</div>
				<div class="contact c6">
					<div class="wrap">
						<?= apply_filters('the_content', ot_get_option('footer_contact')) ?>
					</div>
					<div class="clear"></div>
				</div>
			</footer>
		</div>
	</div> <!-- id="body-wrap" -->
	<aside id="mobile-menu">
		<div class="logo">
        	<?= img(ot_get_option('header_logo')) ?>
		</div>		
		<?php wp_nav_menu(array(
			'theme_location'	=> 'main'
		)) ?>		
	</aside>
	<?php wp_footer() ?>
</body>
</html>