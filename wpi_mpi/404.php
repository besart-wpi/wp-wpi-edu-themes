<?php get_header();?>
	<div class="hero">
		<header>
			<h1>Not Found</h1>
		</header>
	</div>

	<section id="the-content">
		<div id="content">
			<article class="main c12">
				<h2>404 Page Not Found</h2>
				<p>Sorry, but the page you were looking for is not here.</p>
				<?php get_search_form(); ?>
			</article>
			<div class="clear"></div>
		</div>
		</div>
	</section>
<?php get_footer() ?>