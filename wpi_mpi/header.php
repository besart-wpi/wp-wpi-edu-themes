<!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js ie ie6"><![endif]-->
<!--[if IE 7]><html class="no-js ie ie7"><![endif]-->
<!--[if IE 8]><html class="no-js ie ie8"><![endif]-->
<!--[if IE 9]><html class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
    <title><?= get_bloginfo('name'), ' ', wp_title() ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="<?= ot_get_option('favicon') ?>" />
    <link href='https://fonts.googleapis.com/css?family=Lato:400,900,400italic,700italic' rel='stylesheet' type='text/css'>

    <!--[if IE 8]>
        <?= js('respond.js') ?>
        <?= js('modernizr.js') ?>
    <![endif]-->

    <?php getThemeStyles() ?>
    <?php wp_head() ?>
    <!--[if IE]><link rel='stylesheet' href="<?= get_stylesheet_directory_uri() . '/css/responsive.css' ?>"><![endif]-->
</head>
<body <?php body_class() ?>>
    <input type="checkbox" id="menu-button">
    <section class="mobile">
    	<label class="menu-button" for="menu-button"></label>
	</section>
    
    <div id="body-wrap">
	    <header class="main">
	        <div class="logo">
                <?php 
                $offset = ot_get_option('header_logo_offset');
                if(!$offset) $offset = '0px';
                ?>
	        	<a href="<?= bloginfo('url') ?>" style="position: relative; top: <?= $offset ?>"><?= img(ot_get_option('header_logo')) ?></a>
	        </div>
        	<div class="icons">
        		<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        		// WPI Icons
        		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        		$icons = ot_get_option('header_sub_logos');
                if(is_array($icons) && count($icons)):
            		foreach($icons as $icon): ?>
    	        		<a target="_blank" href="<?= $icon['header_sub_logos_link'] ?>"><?= img($icon['header_sub_logos_icon']) ?></a>
    	        	<?php endforeach; ?>
                <?php endif; ?>
        	</div>
        	<nav class="tablet-plus">
        		<?php wp_nav_menu(array(
        			'theme_location'	=> 'main',
					'menu_id' => 'menu-main'
        		)) ?>
        	</nav>
	        <div class="clear"></div>
	    </header>