<?php 
/**
 * @see  /templates/projects.php
 */
?>
<div id="portfolio-wrap">
	<?php foreach($posts as $post){ 
		/**
		 * Get the first taxonomy
		 * @var [ARR]
		 */
		$terms = wp_get_post_terms($post->ID, 'project-categories');
		$term = isset($terms[0]->name) ? $terms[0]->name : 'Misc';
		$termClasses = 'cat-all ';
		foreach($terms as $tag){
			$termClasses .= ' cat-' . $tag->slug;
		}

		/**
		 * Get first image
		 */
		$images = get_post_meta($post->ID, 'portfolio_gallery_items', true);
		$image = '';
		if($images){
			$imageID = current(explode(',', $images));
			$image = wp_get_attachment_image_src($imageID, 'project-small');
			$image = $image[0];
			$bgSize = 'cover; -ms-behavior: url(' . get_stylesheet_directory_uri() . '/js/backgroundsize.min.htc';
		}
		if(!$image) {
			$bgSize = '100% auto';
			$image = ot_get_option('header_logo');
		}
		?>
		<div class="portfolio-thumb-wrap c3 <?= $termClasses ?>">
			<a href="<?= get_post_permalink($post->ID) ?>" class="portfolio-thumb">
				<header style="background: url(<?= $image ?>) no-repeat center; background-size: <?= $bgSize ?>;"></header>
				<article>
					<p><strong><?= $post->post_title ?></strong></p>
					<?php /* <p><?= $term ?></p> */ ?>
				</article>
			</a>
			<div style="height: 60px"></div>
		</div>
	<?php } ?>
</div>