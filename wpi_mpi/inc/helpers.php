<?php //#########################################
// Helper Functions
//############################################### 

/**
 * Load an image, either relative to the folder directory or absolute
 * @param  [STR] $uri   Location, relative to "/assets/image"
 * @param  [ARR/STR] $atts   Attributes or Alt tag ('if string')
 * @param  [OBJ] $page  If passed, will load relative to the current pages folder
 * @return [STR] The <img> tag
 */
function img($uri, $atts = '', $page = false){    
    //- - - - - - - - - - - - - - - - - - - - - - - -
    // Relative
    // :: If $local, we'll look in the pages folder
    //- - - - - - - - - - - - - - - - - - - - - - - -
    if(substr($uri, 0, 4) !== 'http'){
        $uri = url('img/') . $uri;
    }

    //- - - - - - - - - - - - - - - - - - - - - - - -
    // Build the attributes
    //- - - - - - - - - - - - - - - - - - - - - - - -
    if(is_array($atts)){
        if(!isset($atts['alt'])) $atts['alt'] = '';

        $str = '';
        foreach($atts as $key=>$att)
            $str .= $key . "='$att'";
        $atts = $str;
    } else {
        $atts = "alt='$atts'";
    }

    return "<img src='$uri' $atts>";
}


/**
 * Slugifies a string, lowercasing and replacing spaces with hyphens
 * @param  [STR] $url string to slugify
 * @return [STR]      slugged string
 */
function slugify($str){
    // replace non letter or digits by -
    $str = preg_replace('~[^\\pL\d]+~u', '-', $str);

    // trim
    $str = trim($str, '-');

    // transliterate
    $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);

    // lowercase
    $str = strtolower($str);

    // remove unwanted characters
    $str = preg_replace('~[^-\w]+~', '', $str);

    if (empty($str))
      return 'n-a';

    return $str;
}

/**
 * Shorthand for wp_enqueue_script
 * If .js is not supplied, then a built in WordPress script will be loaded
 *
 * @example
 *  js('jquery'); //same as wp_enqueue_script('jquery')
 *
 * @link  http://codex.wordpress.org/Function_Reference/wp_enqueue_script#Default_Scripts_Included_and_Registered_by_WordPress
 * 
 * @param  [STR] $filename The filename, relative to /js/
 * @return  null
 */
function js($filename){
    if(substr($filename, 0, 4) == 'http') 
        wp_enqueue_script($filename, $filename);
    elseif(!strpos($filename, '.js'))
        wp_enqueue_script($filename);
    else
        wp_enqueue_script($filename, get_stylesheet_directory_uri() . '/js/' . $filename);
}

/**
 * Shorthand for wp_enqueue_style
 * @param  [STR] $filename The filename, relative to /
 * @return null
 */
function css($filename){
    wp_enqueue_style($filename, get_stylesheet_directory_uri() . '/' . $filename);
}

/**
 * Returns a url relative to the template directory
 * @param  [STR] $str The URL, relative to the template directory
 * @return [STR] The url
 */
function url($str){
    $url = get_stylesheet_directory_uri() . '/' . $str;

    return $url;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Dummy Color Darkener
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
function darken_color($rgb, $darker=2) {
    $hash = (strpos($rgb, '#') !== false) ? '#' : '';
    $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
    if(strlen($rgb) != 6) return $hash.'000000';
    $darker = ($darker > 1) ? $darker : 1;

    list($R16,$G16,$B16) = str_split($rgb,2);

    $R = sprintf("%02X", floor(hexdec($R16)/$darker));
    $G = sprintf("%02X", floor(hexdec($G16)/$darker));
    $B = sprintf("%02X", floor(hexdec($B16)/$darker));

    return $hash.$R.$G.$B;
}