<?php 
// =================================================
// :: Custom Post Types (CPT) ::
// -------------------------------------------------

add_action('admin_menu', 'defineCPTs', 1);
function defineCPTs(){
	global $oz;
	// =================================================
	// :: Home Page ::
	// -------------------------------------------------
	$oz->metabox(array(
		'id'		=> 'the-content',
		'only-ids'	=> get_option('page_on_front'),
		'fields'	=> array(
			'content' 	=> array(
				'label'	=> '',
				'stack'	=> true,
				'type'	=> 'editor'
			)
		)
	));
	$oz->metabox(array(
		'id'			=> 'right-side',
		'label'			=> 'Right-handed Content',
		'only-ids'		=> get_option('page_on_front'),
		'fields'		=> array(
			'sidebar'	=> array(
				'label'	=> '',
				'stack'	=> true,
				'type'	=> 'editor',
			)
		)
	));

	//Get the POST ID	and template file
	$postID = 0;
	if(isset($_GET['post']))
		$postID = $_GET['post'];
	elseif(isset($_POST['post_ID']))
		$postID = $_POST['post_ID'];
	
	$template_file = get_post_meta($postID,'_wp_page_template',TRUE);
	
	

	// Custom Meta Box
	if(get_option('page_on_front') == $postID){
		$callouts = array(
			'id'          => 'callouts_meta_box',
			'title'       => __( 'Callouts', 'wpi-mpi' ),
			'desc'        => '',
			'pages'       => array( 'page' ),
			'context'     => 'normal',
			'priority'    => 'high',
			'fields'      => array(
				array(
					'label'       => __( 'Callouts Title', 'wpi-mpi' ),
					'id'          => 'callouts_title',
					'type'        => 'text',
				),
				array(
					'label'       => __( 'Callout Items', 'wpi-mpi' ),
					'id'          => 'callouts',
					'type'        => 'list-item',
					'settings'    => array(
						array(
							'id'	=> 'callout-link',
							'label'	=> __('Link', 'wpi-mpi'),
							'type'	=> 'text'
						),
						array(
							'id' 	=> 'callout_image',
							'label' => __( 'Callout Image', 'wpi-mpi' ),
							'type'  => 'upload'
						),
					)
				)	
			) // fields
		);
	
		ot_register_meta_box( $callouts );
	}


	if(/* ot_get_option('home_page_callouts') == 'on' && */ get_option('page_on_front') == $postID){
		$quotes = array(
			'id'          => 'quotes_metabox',
			'title'       => __( 'Testimonial', 'wpi-mpi' ),
			'pages'       => array( 'page' ),
			'context'     => 'normal',
			'priority'    => 'high',
			'fields'      => array(
				array(
					'id'          => 'quotes',
					'type'        => 'custom-post-type-select',
					'section'     => 'option_types',
					'post_type'   => 'testimonials'
				),
			)
		);
		ot_register_meta_box( $quotes );
	}
	
	
	$oz->metabox(array(
		'id'		=> 'footer-callout',
		'only-ids'	=> get_option('page_on_front'),
		'fields'	=> array(
			'content'	=> array(
				'label'	=> '',
				'type'	=> 'editor',
				'stack'	=> true
			)
		)
	));

	// =================================================
	// :: Pages ::
	// -------------------------------------------------
	$oz->metabox(array(
		'id'		=> 'sidebar',
		'post-types'	=> 'page',
		'templates'	=> array('default', 'templates/news.php', 'page-landing.php'),
		'exclude-ids'	=> get_option('page_on_front'),
		'fields'	=> array(
			'content'	=> array(
				'type'	=> 'editor',
				'stack'	=> true,
				'label'	=> ''
			)
		)
	));

	/**
	 * Events
	 */
	if($template_file == 'templates/news.php'){
		$oz->metabox(array(
			'id'		=> 'Events',
			'post-types'=> 'page',
			'templates'	=> 'templates/news.php',
			'fields'	=> array(
				'title',
				'event'	=> array(
					'type'	=> 'group',
					'label'	=> 'Event: %#title',
					'fields'=> array(
						'title',
						'title-color',
						'content'	=> 'editor'
					)
				)
			)
		));
	}


	// =================================================
	// :: Testimonials ::
	// -------------------------------------------------
	remove_action( 'admin_init', 'wpi_testimonials_custom_meta_boxes' );

	$oz->metabox(array(
		'id'			=> 'properties',
		'post-types'	=> 'testimonials',
		'fields'		=> array('title', 'company')
	));


	// =================================================
	// :: Portfolios ::
	// -------------------------------------------------
	$oz->metabox(array(
		'id'		=> 'info',
		'post-types'=> 'project_portfolio',
		'fields'	=> array(
			'overview'	=> 'editor',
			'client' => 'label=Researchers',
      'additional' => array(
        'label' => 'Additional Information',
        'type' => 'editor'
        ),
/*			'services'	=> array(
				'type'	=> 'text',
				'repeat'=> true
			)
*/		)
	));
}