<?php get_header() ?>
	<div class="front-page cycle-slideshow" data-cycle-slides="> div">
		<?php $slides = get_posts(array(
			'post_type'	=> 'home_page_slideshow',
			'posts_per_page'	=> 0,
			'orderby'	=> 'menu_order',
			'order'		=> 'ASC'
		));
		foreach($slides as $slide): 
			// =================================================
			// :: Display the thumbnail as a background ::
			// -------------------------------------------------
			$bg = get_post_thumbnail_id($slide->ID);
			$bg = get_post($bg);
			if($bg){
				$bg = 'style="background: url('. $bg->guid. '); background-position: center; background-repeat: no-repeat; background-size: cover; -ms-behavior: url('.get_bloginfo('template_url').'/js/backgroundsize.min.htc);"';
			}
			// Get link URL
			$link_url = get_post_meta($slide->ID, 'link_url', true);
		?>
			<div <?php echo $bg, $link_url ? ' data-link-url="'.esc_url($link_url).'" class="has-link"' : ''; ?>>
				<section>
					<div class="content-wrap">
						<?php if($slide->post_title): ?><h1><span><?= $slide->post_title ?></span></h1><?php endif; ?>
						<?php if($slide->post_content): ?><div class="content"><?= apply_filters('the_content', $slide->post_content) ?></div><?php endif; ?>
					</div>
				</section>
			</div>
		<?php endforeach; ?>
	</div>
	
	<?php 
	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	// Get Callouts now and use them below (prepares the body padding)
	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	$callouts = get_post_meta($post->ID, 'callouts', true);
	$callouts_title = get_post_meta($post->ID, 'callouts_title', true);
	
	if(!is_array($callouts)) $callouts = array();
	// $callouts = array_merge(array('callouts' => array(), 'title' => ''), $callouts);
	 ?>
	<section id="the-content" <?php if(!count($callouts)) echo 'class="no-padding"' ?>>
		<div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Intro block
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			if($post->post_content): ?>
				<div class="intro">
					<?= apply_filters('the_content', $post->post_content) ?>
				</div>
			<?php endif; ?>

			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Displays either content as full width or with sidebar
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$sidebar = get_post_meta($post->ID, 'right-side', true);
			$theContent = Oz::getField('content', 'the-content');
			if(!is_array($sidebar)) $sidebar = array();
			$sidebar = array_merge(array('sidebar'=>''), $sidebar);
			$contentWidth = $sidebar['sidebar'] ? 'c6' : 'c12';
			?>
			<article class="main <?= $contentWidth ?>">
				<?php while(have_posts()): the_post(); ?>
					<?= apply_filters('the_content', $theContent) ?>
				<?php endwhile; ?>
			</article>
			<?php if($sidebar['sidebar']): ?>
				<aside class="main c6">
					<?= apply_filters('the_content', $sidebar['sidebar']) ?>
				</aside>
			<?php endif; ?>
			<?php get_sidebar('home'); ?>            
            
            <div class="clear"></div>
		</div>

		<div id="research-centers">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Image Callouts
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			if( count($callouts) && ot_get_option('home_page_callouts') == 'on' ):
			?>
				<?php echo $callouts_title ? '<h2>'.$callouts_title.'</h2>' : ''; ?>
				<div class="callout centers">
					<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
					// Get the column widths
					//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
					$width = 'c12';
					$count = count($callouts);
					if($count == 2) 
						$width = 'c6';
					elseif($count > 2)
						$width = 'c4';

					foreach($callouts as $callout):
						$link = isset($callout['callout-link']) ? $callout['callout-link'] : '';
					?>
						<div class="center <?= $width ?>">
							<?php if($link): ?><a href="<?= $link ?>"><?php endif; ?>
								<img src="<?php echo $callout['callout_image']; ?>" alt="" />
							<?php if($link): ?></a><?php endif; ?>
						</div>
					<?php endforeach; ?>
					<div class="clear"></div>
				</div>
			<?php endif; ?>

			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Testimonials
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$quote = get_post_meta($post->ID, 'quotes', true);

/*			if($quote){
				$test = get_post($quote);
			} else {
				$testimonials = get_posts(array(
					'posts_per_page'	=> 1,
					'post_type'			=> 'testimonials',
					'orderby'			=> 'rand'
				));
				$test = $testimonials[0];			
			}
*/
			if($quote):
			?>
				<div class="callout say">
					<h2>What Members Say</h2>
					<?php if($quote) echo displayTestimonial($quote) ?>
					<?php /* ?>
					<div class="c4 person">
						<?= get_the_post_thumbnail($test->ID, 'testimonial-small', array('class'=>'photo')) ?>
						<div class="meta">
							<p class="name"><?= $test->post_title ?>
							<?php if($props['title']): ?><p class="title"><?= $props['title'] ?><?php endif; ?>
							<?php if($props['company']): ?><p class="company"><?= $props['company'] ?><?php endif; ?>
						</div>
						<div class="clear"></div>
					</div>
					<div class="c8 content">
						<?= apply_filters('the_content', '"' . $test->post_content . '"') ?>
					</div>
					<div class="clear"></div>
					<?php */ ?>
				</div>
			<?php endif; ?>
		</div>
        
        <?php get_sidebar('homefooter'); ?>
	</section>
<?php get_footer() ?>