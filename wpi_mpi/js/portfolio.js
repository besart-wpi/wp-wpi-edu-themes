jQuery(function($){
	var $portfolio 	= $('#portfolio-wrap')
	var $projects 	= $('.portfolio-thumb-wrap')
	var $cats 		= $('#project-categories a')
	$thumbs = $('.portfolio-thumb-wrap'); 

	/**
	 * Equalize heights
	 */
	 
	var resizeThumbs = function() {
		var tallest = 0;
		$('article', $thumbs).height('auto');
		$('article', $thumbs).each(function(){
			tallest = Math.max($(this).height(), tallest)
		}).height(tallest);
	}

	$cats.click(function(){
		var $this = $(this);

		$projects.show()
			.not($('.' + $this.attr('href').replace('#', ''))).hide()
		$portfolio.masonry('reloadItems')
		$cats.removeClass('selected')
		$this.addClass('selected')

		return false;
	})

	$('window').ready(function(){
		var $ = jQuery
		if($thumbs.length){
			resizeThumbs();
		}
		setTimeout(function(){
			var $portfolio 	= $('#portfolio-wrap')
			if(typeof $portfolio != "undefined")
			{
				$portfolio.masonry({
					itemSelector: '.portfolio-thumb-wrap'
				})
			}
		}, 1)
	})

	
	$(window).resize(function(){
		if($thumbs.length){
			resizeThumbs();
		}
	});
})
