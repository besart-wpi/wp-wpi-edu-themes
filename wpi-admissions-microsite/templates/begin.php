<?php 

/* 	Template Name: Begin your journey
	Template Post Type: topic */

get_header(); ?>

<?php the_post(); ?>

<?php 
$begin_heading 				= get_field( 'begin_heading' );
$begin_bg_image 			= get_field( 'begin_bg_image' );
$begin_text_buttons 		= get_field( 'begin_text_buttons' );
$begin_cta_btn_small_text 	= get_field( 'begin_cta_btn_small_text' );
$begin_cta_btn_large_text 	= get_field( 'begin_cta_btn_large_text' );
$begin_cta_btn_icon 		= get_field( 'begin_cta_btn_icon' );
$begin_cta_btn_url 			= get_field( 'begin_cta_btn_url' );
$begin_cta_btn_new_tab 		= get_field( 'begin_cta_btn_new_tab' );
$begin_cta_btn_bg_image 	= get_field( 'begin_cta_btn_bg_image' );

//CDN+AJAX fix
if($begin_cta_btn_icon && function_exists('_campus_cdn_get_base_upload_url')) {
	$begin_cta_btn_icon = str_replace(site_url('/files/'), _campus_cdn_get_base_upload_url(), $begin_cta_btn_icon);
}

if( $begin_bg_image )
	$begin_bg = sprintf( ' style="background-image: url(%s);"', $begin_bg_image[ 'sizes' ][ 'thumb-full-screen' ] );

if( $begin_heading || $begin_text_buttons || $begin_cta_btn_url ){ ?>
	<section class="row begin-journey"<?php echo $begin_bg; ?>>
		<div class="wrap">
			<div class="text">
				<h1><?php echo $begin_heading; ?></h1>
			</div>
			<?php if( $begin_text_buttons ){ ?>
				<div class="cta-buttons">
					<?php foreach( $begin_text_buttons as $button ){ 
						$small_text = $button[ 'small_text' ];
						$large_text = $button[ 'large_text' ];
						$url 		= $button[ 'url' ];
						$new_tab 	= $button[ 'new_tab' ];
						$label 		= $small_text . ' ' . $large_text;
						$target 	= $new_tab ? '_blank' : ''; ?>
						<div class="btn-cta outline">
							<a href="<?php echo esc_url( $url ); ?>" title="<?php echo esc_attr( $label ); ?>" target="<?php echo $target; ?>"><span class="hidden"><?php echo $label; ?></span></a>
							<span class="subtitle"><?php echo $small_text; ?></span>
							<span class="title"><?php echo $large_text; ?></span>
							<div class="circle-small">
						        <div class="inner"></div><div class="outer"></div>
						        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/chev-right.svg" class="svg" alt="<?php echo esc_attr( $label ); ?>">
							</div>							
						</div>
					<?php } ?>
				</div>
				<div class="cta-buttons-mobile">
					<ul class="menu">
						<?php foreach( $begin_text_buttons as $button ){ 
							$small_text = $button[ 'small_text' ];
							$large_text = $button[ 'large_text' ];
							$url 		= $button[ 'url' ];
							$new_tab 	= $button[ 'new_tab' ];
							$label 		= $small_text . ' ' . $large_text;
							$target 	= $new_tab ? '_blank' : '';
							echo sprintf( '<li><a href="%s" target="%s"><span>%s</span></a></li>', esc_url( $url ), $target, $label );
						} ?>
					</ul>
				</div>
			<?php } ?>
			<div class="image-links">
				<?php 
				if( $begin_cta_btn_bg_image )
					$cta_bg = sprintf( ' style="background-image: url(%s);"', $begin_cta_btn_bg_image[ 'sizes' ][ 'thumb-cta' ] );

				if( $begin_cta_btn_small_text && $begin_cta_btn_large_text )
					$cta_label = $begin_cta_btn_small_text . ' ' . $begin_cta_btn_large_text;

				if( $begin_cta_btn_new_tab )
					$cta_target = $begin_cta_btn_new_tab ? '_blank' : '';

				if( $begin_cta_btn_url ){ ?>
					<div class="btn-cta"<?php echo $cta_bg; ?>>
						<a href="<?php echo esc_url( $begin_cta_btn_url ); ?>" title="<?php echo esc_attr( $cta_label ); ?>" target="<?php echo $cta_target; ?>"><span class="hidden"><?php echo $cta_label; ?></span></a>
						<span class="subtitle"><?php echo $begin_cta_btn_small_text; ?></span>
						<span class="title"><?php echo $begin_cta_btn_large_text; ?></span>
						<?php if( $begin_cta_btn_icon ){ ?>
					      	<div class="circle-small">
						        <div class="inner"></div><div class="outer"></div>
						        <img src="<?php echo $begin_cta_btn_icon; ?>" class="svg" alt="<?php echo esc_attr( $cta_label ); ?>">
					      	</div>
				      	<?php } ?>
					</div>
				<?php } ?>

				<div class="btn-cta share">
					<a href="#" title="Share Experience" class="show-sharer"><span class="hidden">Show it off Share Experience</span></a>
					<span class="subtitle">Show it off</span>
					<span class="title">Share Experience</span>
			      	<div class="circle-small">
				        <div class="inner"></div><div class="outer"></div>
				        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/share.svg" class="svg" alt="">
			      	</div>							
				</div>
			</div>
		</div>
	</section>
<?php } ?>	

<?php get_footer( );  ?>