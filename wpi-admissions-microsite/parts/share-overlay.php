	<section class="row sharer" id="overlay-share">
		<a href="#" class="close-share" title="Close"><span class="hidden">Close</span></a>
		<div class="label-mobile">
			<h2>Share This</h2>
			<span class="arrow"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/chev-down.svg" class="svg" alt="arrow down"></span>
		</div>
		<div class="col">
			<div class="text twitter">
				<a href="#" data-js="twitter-share" title="Share via Twitter"><span class="hidden">Share via Twitter</span></a>
				<figure></figure>
				<span class="subtitle">Share via</span>
				<span class="title">Twitter</span>
		      	<div class="circle-medium">
		        	<div class="cm-outer"></div><div class="cm-inner"></div>
			        <div class="rotator"><div class="rotate0"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate10"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate20"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate30"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate40"><div class="rotate-bar"></div><div class="rotate-bar"></div></div></div>
		        	<div class="icon"><svg width="30px" height="30px" fill="white" ><polygon class="fill" points="12.0454545 5 22.0454545 15 12.0454545 25 10 22.9545455 17.9545455 15 10 7.04545455" ></polygon></svg></div>
		      	</div>
			</div>
			<div class="bg"></div>
		</div>
		<div class="col">
			<div class="text facebook">
				<a href="#" data-js="facebook-share" title="Share via Facebook"><span class="hidden">Share via Facebook</span></a>
				<figure></figure>
				<span class="subtitle">Share via</span>
				<span class="title">Facebook</span>
		      	<div class="circle-medium">
		        	<div class="cm-outer"></div><div class="cm-inner"></div>
			        <div class="rotator"><div class="rotate0"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate10"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate20"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate30"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate40"><div class="rotate-bar"></div><div class="rotate-bar"></div></div></div>
		        	<div class="icon"><svg width="30px" height="30px" fill="white" ><polygon class="fill" points="12.0454545 5 22.0454545 15 12.0454545 25 10 22.9545455 17.9545455 15 10 7.04545455" ></polygon></svg></div>
		      	</div>
			</div>
			<div class="bg"></div>
		</div>
		<div class="col">
			<div class="text share">
				<a href="#" data-js="copy-share" title="Share via Share Link"><span class="hidden">Share via Share Link</span></a>
				<div class="copied">
					<span class="note">Copied to clipboard</span><span class="url share-url"><?php echo get_permalink(); ?></span>
				</div>
				<figure></figure>
				<span class="subtitle">Share via</span>
				<span class="title">Share Link</span>
		      	<div class="circle-medium">
		        	<div class="cm-outer"></div><div class="cm-inner"></div>
			        <div class="rotator"><div class="rotate0"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate10"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate20"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate30"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate40"><div class="rotate-bar"></div><div class="rotate-bar"></div></div></div>
		        	<div class="icon"><svg width="30px" height="30px" fill="white" ><polygon class="fill" points="12.0454545 5 22.0454545 15 12.0454545 25 10 22.9545455 17.9545455 15 10 7.04545455" ></polygon></svg></div>
		      	</div>
			</div>
			<div class="bg"></div>
		</div>
	</section>	