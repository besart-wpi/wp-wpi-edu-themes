			<div class="sharer-inline">
                <?php
                $sidebar_icons_heading = get_field('sidebar_icons_heading');
                $sidebar_icons = get_field('sidebar_icons');
                
                if( !empty($sidebar_icons) ):
                    echo $sidebar_icons_heading ? '<span class="label">' . esc_html($sidebar_icons_heading) . '</span>' : '';
                    foreach($sidebar_icons as $index => $sidebar_icon): 
                        $label = $sidebar_icon['label'];
                        $show_label = $sidebar_icon['show_label'];
                        $icon = $sidebar_icon['icon']; // apply_now, connect, enroll, learn_more, questions, visit, what_we_look_for
                        $icon_color = $sidebar_icon['icon_color']; // Hex color
                        $link_url = $sidebar_icon['link_url'];
                        $link_target = $sidebar_icon['link_new_tab'] ? '_blank' : '_self';
                        ?>
                        <div class="link sidebar-icon-<?php echo esc_attr($index); ?>" aria-label="<?php echo esc_attr($label); ?>" data-microtip-position="right" role="tooltip">
                            <a href="<?php echo esc_url($link_url); ?>" title="<?php echo esc_attr($label); ?>" target="<?php echo $link_target; ?>"><span class="hidden"><?php echo esc_html($label); ?></span></a>
                            <div class="circle-small">
                                <div class="inner"></div><div class="outer"></div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sidebar_icon-<?php echo $icon; ?>.svg" class="svg" alt="">
                            </div>
                            <?php if($show_label): ?><span class="visible-label"><?php echo esc_html($label); ?></span><?php endif; ?>
                        </div>
                        <style>
                            .row.content .sharer-inline .link.sidebar-icon-<?php echo $index; ?>:hover .circle-small .inner { 
                                border-color: <?php echo $icon_color; ?>;
                            }
                            .row.content .sharer-inline .link.sidebar-icon-<?php echo $index; ?>:hover .circle-small .outer {
                                border-color: <?php echo hex2rgba($icon_color, 0.6); ?>;
                            }
                            .row.content .sharer-inline .link.sidebar-icon-<?php echo $index; ?>:hover .circle-small svg path, 
                            .row.content .sharer-inline .link.sidebar-icon-<?php echo $index; ?>:hover .circle-small svg polygon {
                                fill: <?php echo $icon_color; ?>;
                            }
                            
                            @media screen and (max-width: 1199px) {
                                .row.content .sharer-inline .link.sidebar-icon-<?php echo $index; ?> .circle-small .inner { 
                                    border-color: <?php echo $icon_color; ?>;
                                }
                                .row.content .sharer-inline .link.sidebar-icon-<?php echo $index; ?> .circle-small .outer {
                                    border-color: <?php echo hex2rgba($icon_color, 0.6); ?>;
                                }
                                .row.content .sharer-inline .link.sidebar-icon-<?php echo $index; ?> .circle-small svg path, 
                                .row.content .sharer-inline .link.sidebar-icon-<?php echo $index; ?> .circle-small svg polygon {
                                    fill: <?php echo $icon_color; ?>;
                                }
                            }
                        </style>
                    <?php endforeach; ?>
                    <div class="break"></div>
                <?php endif; ?>
                
                <?php 
                $share_icons_hide_all = get_field('share_icons_hide_all');
                $share_icons_hide_facebook = get_field('share_icons_hide_facebook');
                $share_icons_hide_twitter = get_field('share_icons_hide_twitter');
                $share_icons_hide_clipboard = get_field('share_icons_hide_clipboard');
                
                // Hide all if "Hide all..." is checked, or if all three site specific are checked
                if($share_icons_hide_all == false && !($share_icons_hide_facebook == true && $share_icons_hide_twitter == true && $share_icons_hide_clipboard == true)): ?>
                    <span class="label">Share:</span>
                    <?php if($share_icons_hide_facebook == false): ?>
                    <div class="link facebook" aria-label="Facebook" data-microtip-position="right" role="tooltip">
                        <a href="#" data-js="facebook-share" title="Facebook"><span class="hidden">Facebook</span></a>
                        <div class="circle-small">
                            <div class="inner"></div><div class="outer"></div>
                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/facebook.svg" class="svg" alt="">
                        </div>	
                    </div>
                    <?php endif; ?>
                    <?php if($share_icons_hide_twitter == false): ?>
                    <div class="link twitter" aria-label="Twitter" data-microtip-position="right" role="tooltip">
                        <a href="#" data-js="twitter-share" title="Twitter"><span class="hidden">Twitter</span></a>
                        <div class="circle-small">
                            <div class="inner"></div><div class="outer"></div>
                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/twitter.svg" class="svg" alt="">
                        </div>	
                    </div>
                    <?php endif; ?>
                    <?php if($share_icons_hide_clipboard == false): ?>
                    <div class="link share" aria-label="Share Link" data-microtip-position="right" role="tooltip">
                        <a href="#" data-js="copy-share-inline" title="Share Link"><span class="hidden">Share Link</span></a>
                        <div class="copied">
                            <span class="note">Copied</span><span class="url share-url-inline"><?php the_permalink(); ?></span>
                        </div>					
                        <div class="circle-small">
                            <div class="inner"></div><div class="outer"></div>
                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/share.svg" class="svg" alt="">
                        </div>	
                    </div>
                    <?php endif; ?>
                <?php endif; ?>
			</div>