document.addEventListener("DOMContentLoaded", function() {
    var lazyVideos = [].slice.call(document.querySelectorAll("video.lazy"));
    //console.log(lazyVideos);
    if ("IntersectionObserver" in window) {
      var lazyVideoObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(video) {
          if (video.isIntersecting) {
            for (var source in video.target.children) {
              var videoSource = video.target.children[source];
              if (typeof videoSource.tagName === "string" && videoSource.tagName === "SOURCE") {
                videoSource.src = videoSource.dataset.src;
              }
            }

            video.target.load();
            video.target.classList.remove("lazy");
            video.target.play();
            lazyVideoObserver.unobserve(video.target);
          }
        });
      });

      lazyVideos.forEach(function(lazyVideo) {
        lazyVideoObserver.observe(lazyVideo);
      });
    }
  });

  document.addEventListener("DOMContentLoaded", function() {
    var lazyBackgrounds = [].slice.call(document.querySelectorAll(".lazy-background"));

    if ("IntersectionObserver" in window) {
      let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            //entry.target.classList.add("visible");
            //console.log("TIME TO LOAD");
            entry.target.style.backgroundImage = "url(" + entry.target.getAttribute("data-info") + ")";
            lazyBackgroundObserver.unobserve(entry.target);
          }
        });
      });

      lazyBackgrounds.forEach(function(lazyBackground) {
        lazyBackgroundObserver.observe(lazyBackground);
      });
    }
  });

  document.addEventListener("DOMContentLoaded", function() {
    var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));

    if ("IntersectionObserver" in window) {
      let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            let lazyImage = entry.target;

            if(typeof lazyImage.dataset.src !== 'undefined') {
                lazyImage.src = lazyImage.dataset.src;
            }

            if (typeof lazyImage.dataset.srcset !== 'undefined') {
                lazyImage.srcset = lazyImage.dataset.srcset;
            }

            lazyImage.classList.remove("lazy");
            lazyImageObserver.unobserve(lazyImage);
          }
        });
      });

      lazyImages.forEach(function(lazyImage) {
        lazyImageObserver.observe(lazyImage);
      });
    } else {
      // Possibly fall back to a more compatible method here
    }
  });

(function($) {
    var menuIsOpen = 0;
    var videoModalIsOpen = 0;

    $('.btn-menu').on('mouseover',function(){
        $(".hover").addClass("active");
    });

    $('.wrap').on('mouseleave',function(){
        $(".hover").removeClass("active");
    });

    $('.ShowMenuClass').on('click', clickedShowMenu);

    function clickedShowMenu(){
        menuClick();
    }

    function menu(){
        // $('ul.sf-menu').superfish();
        $('.menu-topic > li > a').append('<div class="blimp"><div class="bl-inner"></div><div class="bl-outer"></div><div class="bl-blink"></div></div>');
        $('.menu-infos a').wrapInner('<span></span>');

        var header = $('#header');

        header.find('.lower a').attr('tabindex','-1');
        header.find('.lower .topics h6').after('<a href="#"></a>');

        $('.btn-menu').on('click',function(){
            menuClick();
        });

        $('#footer').clone().removeAttr('id').appendTo( $('#header .lower') );

        if( $('.os-ios').length ){
            $('#header button').on('touchstart', function () {
                $(this).trigger('click');
            }).on('touchend', function () {
                $(this).trigger('click');
            });

            $('#header .menu-topic a').on('touchstart', function () {
                $(this).trigger('click');
            }).on('touchend', function () {
                $(this).trigger('click');
            });
        }
    }

    function menuClick()
    {
        var header = $('#header');
        // $('#header').toggleClass('menu-open');
            // console.log('click menu');
            if( header.hasClass('menu-open') ){
                header.removeClass('menu-open');
                header.find('.lower a').attr('tabindex','-1');
                header.find('.btn-menu').next('.logo').focus();
                menuIsOpen = 0;
            }else{
                header.addClass('menu-open');
                header.find('.lower a').attr('tabindex',9998);
                header.find('.lower a:last').attr('tabindex',9999);
                menuIsOpen = 1;
                // header.find('.lower a').attr('tabindex','');
                header.find('.lower a:first').focus();
            }
            return false;
    }

    function sharer(){
        $('.show-sharer').on('click',function(){
            $('#overlay-share').addClass('active');
            $('html').addClass('disable-scroll');
            return false;
        });

        $('.close-share').on('click',function(){
            $('html').removeClass('disable-scroll');
            $('#overlay-share').removeClass('active');
            $('[data-js="copy-share"]').next('.copied').css('opacity',0);
            $('.video-detail').removeClass('open-share');
            return false;
        });

        $('.btn-vid-share').on('click',function(){
            $('.video-detail').removeClass('open').addClass('open-share');
        });

        $(document).on('click','[data-js="twitter-share"]',function(){
            var $this = $(this);
            var twitterWindow = window.open('https://twitter.com/share?url=' + encodeURIComponent(window.location.href), 'twitter-popup', 'height=350,width=600');
            if( twitterWindow.focus ){
                twitterWindow.focus();
            }
            // console.log( 'twitter-share' );
            return false;
        });

        $(document).on('click','[data-js="facebook-share"]',function(){
            var $this = $(this);
            var facebookWindow = window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(window.location.href), 'facebook-popup', 'height=350,width=600');
            if( facebookWindow.focus ){
                facebookWindow.focus();
            }
            // console.log( 'facebook-share' );
            return false;
        });

        $(document).on('click','[data-js="copy-share"]',function(){
            var $this = $(this);
            $this.next('.copied').find('.share-url').empty().text(window.location.href);
            var shareURL = document.querySelector('.share-url');
            var range = document.createRange();
            range.selectNode(shareURL);
            window.getSelection().addRange(range);

            try {
                var successful = document.execCommand('copy');
                if( successful ){
                    $this.siblings('.copied').css('opacity',1);
                }
            } catch(err) {
                console.log('Oops, unable to copy');
            }
            window.getSelection().removeAllRanges();
            return false;
        });

        $(document).on('click','[data-js="copy-share-inline"]',function(e){
            var $this = $(this);
            $this.next('.copied').find('.share-url-inline').empty().text(window.location.href);
            // var shareURL = document.querySelector('.share-url-inline');
            var shareURL = e.target.nextElementSibling.children[1];
            var range = document.createRange();
            range.selectNode(shareURL);
            window.getSelection().addRange(range);

            try {
                var successful = document.execCommand('copy');
                if( successful ){
                    $this.siblings('.copied').css('opacity',1);
                    setTimeout(function(){
                        $this.siblings('.copied').css('opacity',0);
                    },2000);
                }
            } catch(err) {
                console.log('Oops, unable to copy');
            }
            window.getSelection().removeAllRanges();
            return false;
        });
    }

    var player;
    var triggerByHash = 0;

    function videojsInit(){
        if( $('.video-js').length ){
            // console.log('...................................................runing videojsInit');

            if( triggerByHash == 1 ){
                var defaultAutoplay = false;
                var defaultBigPlayButton = true;
            }else{
                var defaultAutoplay = true;
                var defaultBigPlayButton = false;
            }

            if( $('.os-ios').length || $('.os-android').length ){
                defaultBigPlayButton = true;
            }

            var options = {
                fluid: true,
                autoplay: defaultAutoplay,
                controls: true,
                preload: true,
                bigPlayButton: defaultBigPlayButton,
                controlBar: {
                    playToggle: false,
                    volumePanel: false,
                    remainingTimeDisplay: false,
                    fullscreenToggle: false
                }
            };

            // player.dispose();

            setTimeout(function(){
                // console.log('delay video init');
                player = videojs('full-vid', options);

                // console.log( 'player running' );

                player.on('play', function() {
                    // console.log("play");
                    $('.btn-toggle-video').addClass('playing');

                });

                player.on('pause', function() {
                    // console.log("pause");
                    $('.btn-toggle-video').removeClass('playing');
                });

                player.on('timeupdate', function(){
                    var _percent = (player.getChild('ControlBar').getChild('ProgressControl').getChild('SeekBar').getPercent() * 100).toFixed(2);
                    $('.btn-toggle-video').css('left', _percent + '%');
                });

                $('.btn-toggle-video').clone().removeClass('hidden').appendTo( $('#full-vid') );

                $('#full-vid .btn-toggle-video').on('click',function(){
                    var playBtn = $(this);
                    if( playBtn.hasClass('playing') ){
                        player.pause();
                        playBtn.removeClass('playing');
                    }else{
                        player.play();
                        playBtn.addClass('playing');
                    }
                    return false;
                });

                $('#full-vid .vjs-big-play-button').empty()
                $('.hero-video .btn-play .circle-large').clone().appendTo( $('#full-vid .vjs-big-play-button') );
            },1500);
        }
    }

    var hero_audio_state = "paused";

    function audioVideo(){
        // var hero_audio_state = "paused";
        $('.audio-bars').on('click',function(){
            // console.log( 'audio bar' );
            var $this   = $(this),
                video   = $this.data('video');

            if( $this.data('state') )
                hero_audio_state = $this.data('state');

            if( hero_audio_state == "playing" ){
                $this.css('opacity','0.3');
                $this.children('div').css('animationPlayState','paused');
                hero_audio_state = 'paused';

                $this.data('state','paused');

                if( $(video).hasClass('video-js') ){
                    if( $('.video-js').length ) videojs( video ).muted(true);
                }else{
                    $( video ).prop('muted', true);
                }
            }else if( hero_audio_state == "paused" ){
                $this.css('opacity','1');
                $this.children('div').css('animationPlayState','running');
                hero_audio_state = 'playing';

                $this.data('state','playing');

                if( $(video).hasClass('video-js') ){
                    if( $('.video-js').length ) videojs( video ).muted(false);
                }else{
                    $( video ).prop('muted', false);
                }
            }

            if( $this.parents('.hero-video').length ){
                // console.log( 'mirror audio hero-video ' + hero_audio_state );
                if( hero_audio_state == 'paused' ){
                    $('.content .audio-bars').css('opacity','0.3');
                    $('.content .audio-bars div').css('animationPlayState','paused');
                }else{
                    $('.content .audio-bars').css('opacity','1');
                    $('.content .audio-bars div').css('animationPlayState','running');
                }
            }else if( $this.parents('.video-content').length ){
                // console.log( 'mirror audio video-content ' + hero_audio_state );
                if( hero_audio_state == 'paused' ){
                    $('.hero-video .audio-bars').css('opacity','0.3');
                    $('.hero-video .audio-bars div').css('animationPlayState','paused');
                }else{
                    $('.hero-video .audio-bars').css('opacity','1');
                    $('.hero-video .audio-bars div').css('animationPlayState','running');
                }
            }
        });

        var last = 0;
        var diff = 0;
        videojsInit();

            $('#footer').clone().removeAttr('id').appendTo( $('.full-video .lower') );

            // buttons
            var fullVideo = $('.full-video'),
                fullVideoDetail = fullVideo.find('.video-detail');

            $('.play-full').on('click',function(e){
                //this function is what prevents the weird duplicate video bug,
                //for some reason this would be called 3 times when refreshed.
                if(last) {
                  diff = Date.now() - last;
                  last = Date.now();
                  if (diff < 5000) {
                    return;
                  }
                }
                last = Date.now();
                var source          = $(this).data('content'),
                    fullVideo       = $('.full-video'),
                    currentVideo    = $('input[name="current-video"]'),
                    vidIntro        = $('#video-hero-intro');

                history.pushState(null, null, $(this).attr('href') );

                if( hero_audio_state == 'playing' ){
                    // console.log( 'vidIntro have sounds' );
                    vidIntro.prop('muted', true);
                    hero_audio_state = 'paused';
                    $('.content .audio-bars').css('opacity','0.3').data('state','paused');;
                    $('.content .audio-bars div').css('animationPlayState','paused');
                    $('.hero-video .audio-bars').css('opacity','0.3').data('state','paused');;
                    $('.hero-video .audio-bars div').css('animationPlayState','paused');
                }

                if( $(this).data('content') == currentVideo.val() && $(window).width() > 700 ){
                    // if( $(window).width() > 700 ){
                    //     setTimeout(function(){
                    //         fullVideo.fadeIn('fast');
                    //         player.play();
                    //     },3000);
                    // }else{
                    //     setTimeout(function(){
                    //         fullVideo.fadeIn('fast');
                    //         $('#full-vid').get(0).play();
                    //     },3000);
                    // }
                    setTimeout(function(){
                        fullVideo.fadeIn('fast');
                        player.play();
                    },3000);

                    videoModalIsOpen = 1;
                }else{

                    fullVideo.find('.caption').empty().html( $( source ).find('.caption').html() );
                    fullVideo.find('.text').empty().html( $( source ).find('.text').html() );

                    if( $('.video-js').length || player ){
                        player.dispose();
                    }

                    var mobilePlay = '';
                    if( $('body').is('.os-android, .os-ios') ){
                        mobilePlay = 'playsinline autoplay';
                    }

                    fullVideo.find('.video').append('<video id="full-vid" '+ mobilePlay +' class="video-js vjs-default-skin vjs-custom-skin vjs-big-play-centered"></div>');
                    fullVideo.find('.video-js').html( $( source ).find('.video').html() );


                    if( $(window).width() > 700 ){
                        // console.log( 'skipping videojs' );
                        videojsInit();

                        setTimeout(function(){
                            player.on("play", function (e) {
                                fullVideo.find('.audio-bars').css('opacity','1');
                                fullVideo.find('.audio-bars').children('div').css('animationPlayState','running');
                                hero_audio_state = 'playing';
                                fullVideo.find('.audio-bars').data('state','playing');
                                player.muted(false);
                            });

                            player.on("pause", function (e) {
                                fullVideo.find('.audio-bars').css('opacity','0.3');
                                fullVideo.find('.audio-bars').children('div').css('animationPlayState','paused');
                                hero_audio_state = 'paused';
                                fullVideo.find('.audio-bars').data('state','paused');
                                player.muted(true);
                            });
                        },1600);
                    }

                    triggerByHash = 0;

                    setTimeout(function(){
                        fullVideo.fadeIn('fast');
                    },1200);

                    currentVideo.val( source );

                    if( $(window).width() < 700 ){
                        $('#full-vid').removeAttr('class').attr('controls','');
                        setTimeout(function(){
                            // console.log('xxx full-vid play');
                            $('#full-vid').get(0).play();
                        },3000);
                    }else{
                        setTimeout(function(){
                            player.play();
                        },3000);
                    }


                    // Card Carousel
                    var time = 3, // 3 seconds
                        bar,
                        vidDetailCarousel,
                        isPause,
                        tick,
                        percentTime;

                    fullVideo.find('.cards').addClass('vid-detail-carousel');

                    vidDetailCarousel = $(".vid-detail-carousel");

                    vidDetailCarousel.slick({
                        infinite: true,
                        draggable: true,
                        arrows: true,
                        speed: 1200,
                        adaptiveHeight: false,
                        pauseOnHover: true, /* add this option */
                        slidesToShow: 1,
                        prevArrow: '<span class="prev"></span>',
                        nextArrow: '<span class="next"></span>'
                    });

                    bar = $('.progress-bar div');

                    function startProgressbar() {
                        resetProgressbar();
                        percentTime = 0;
                        isPause = false;
                        tick = setInterval(interval, 30);
                    }

                    function interval(){
                        if (isPause === false) {
                            percentTime += 1 / (time + 0.1);
                            bar.css({
                                width: percentTime + "%"
                            });
                            if (percentTime >= 100) {
                                vidDetailCarousel.slick('slickNext');
                                startProgressbar();
                            }
                        }
                    }

                    function resetProgressbar() {
                        bar.css({
                        width: 0 + '%'
                        });
                        clearTimeout(tick);
                    }

                    startProgressbar();

                    vidDetailCarousel.on('beforeChange', function(event, slick, currentSlide, nextSlide){
                        startProgressbar();
                    });

                    vidDetailCarousel.on("mouseenter", function(){
                        isPause = true;
                    }).on("mouseleave", function(){
                        isPause = false;
                    });

                    $('html').addClass('disable-scroll');

                }

                return false;
            });

            $('.btn-vid-close').on('click',function(){
                fullVideo.fadeOut('fast');
                player.pause();
                fullVideoDetail.removeClass('open');
                $('html').removeClass('disable-scroll');
                videoModalIsOpen = 0;
                return false;
            });

            $('.btn-vid-arrow').on('click',function(){
                if( fullVideoDetail.hasClass('open') ){
                    fullVideoDetail.removeClass('open');
                }else{
                    fullVideoDetail.addClass('open');
                    $(window).trigger('resize');
                }
                return false;
            });

            $('.btn-vid-share').on('click',function(){

                return false;
            });

            $('.btn-modal-vid-close').on('click',function(){
                fullVideo.fadeOut('fast');
                fullVideoDetail.removeClass('open');
                $('html').removeClass('disable-scroll');
                fullVideoDetail.removeClass('open');
                $('#full-vid').remove();
                videoModalIsOpen = 0;
                return false;
            });

        $(window).on('load', function () {
            var videoHash   = window.location.hash,
                playTrigger = $('[data-content="'+ videoHash +'"]');

            if( playTrigger.length ){
                triggerByHash = 1;
                // console.log( 'playTrigger.length' + triggerByHash );
                setTimeout(function(){
                    playTrigger.trigger( 'click' );
                },1000);
            }
        });
    }

    function slider(){
        // Video Carousel
        var vidCarousel = $('.vid-carousel');

        vidCarousel.owlCarousel({
            center: true,
            loop: true,
            autoWidth: true,
            margin: 0,
            nav: true,
            navContainer: '.controls-arrows',
            navText: ['<span class="hidden">Next</span>','<span class="hidden">Prev</span>'],
            responsive:{
                0:{
                    items: 1
                },
                380:{
                    items: 3
                },
                1990:{
                    items: 5
                }
            }
        });

        if( vidCarousel.length ){
            $(document).on('keydown', function(e) {
                if(e.keyCode == 37) {
                    vidCarousel.trigger('prev.owl.carousel');
                }
                if(e.keyCode == 39) {
                    vidCarousel.trigger('next.owl.carousel');
                }
            });
        }

        // Card Carousel
        var relatedTime = 3, // 3 seconds
            relatedBar,
            roiCarousel,
            relatedIsPause,
            relatedTick,
            relatedPercentTime;

        roiCarousel = $(".roi-carousel");

        roiCarousel.slick({
            infinite: true,
            draggable: true,
            arrows: true,
            speed: 1200,
            adaptiveHeight: false,
            pauseOnHover: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '<span class="prev"></span>',
            nextArrow: '<span class="next"></span>',
            appendArrows: $('.roi-arrows'),
            responsive: [
                {
                  breakpoint: 850,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                  }
                },
                {
                  breakpoint: 650,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                }
            ]
        });

        relatedBar = $('.progress-bar div');

        function relatedStartProgressbar() {
            relatedResetProgressbar();
            relatedPercentTime = 0;
            relatedIsPause = false;
            relatedTick = setInterval(relatedInterval, 30);
        }

        function relatedInterval(){
            if (relatedIsPause === false) {
                relatedPercentTime += 1 / (relatedTime + 0.1);
                relatedBar.css({
                    width: relatedPercentTime + "%"
                });
                if (relatedPercentTime >= 100) {
                    roiCarousel.slick('slickNext');
                    relatedStartProgressbar();
                }
            }
        }

        function relatedResetProgressbar() {
            relatedBar.css({
            width: 0 + '%'
            });
            clearTimeout(relatedTick);
        }

        relatedStartProgressbar();

        roiCarousel.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            relatedStartProgressbar();
        });

        roiCarousel.on("mouseenter", function(){
            relatedIsPause = true;
        }).on("mouseleave", function(){
            relatedIsPause = false;
        });

    }

    function footer(){
        if( $('.next-topic').length ){
            $('.empty-space').remove();
            var less = 60;

            if( $(window).width() < 1200 )
                less = 0;

            var nextTopicHeight  = $('.next-topic').outerHeight() - less;
            $('.next-topic').before('<section class="row empty-space" style="height:'+ nextTopicHeight +'px"></section>');
        }
    }

    function tooltip(){
        var winWidth = $(window).width();
        if( winWidth < 1200 ){
            $('[role="tooltip"]').attr('data-microtip-position','top');
        }else{
            $('[role="tooltip"]').attr('data-microtip-position','right');
        }
    }

        // var readyOnce = function() {

        //     $('html').addClass('disable-scroll');

        //     if(true) {
        //         // console.log( 'fadein now' );
        //         $('.from-zero').removeClass('from-white');

        //         // console.log( 'pace done' );
        //         $('body').removeAttr('class').addClass( $('#body-classes').attr('class') );

        //         // start animation
        //         if( $('body').hasClass('home') ){
        //             $('.loader-anim').removeAttr('style').addClass('start-anim');
        //         }

        //         // home specific
        //         if( $('body').hasClass('home') ){
        //             setTimeout(function(){
        //                 $('.row.home').addClass('now-loaded');
        //             },1500);

        //             setTimeout(function(){
        //                 $('.row.home').removeClass('now-loaded').removeClass('still-loading');
        //                 $('.loader-anim').hide();
        //             },3000);
        //         }

        //         // remove loader
        //         setTimeout(function(){
        //             // $('html').removeClass('disable-scroll');

        //             if( $('body').hasClass('home') ){
        //                 $('.loader-anim').removeClass('start-anim');
        //                 $('html').removeClass('disable-scroll');
        //             }else{
        //                 // $('.loader-anim').removeClass('start-anim-inner').hide();
        //                 $('.loader-anim').removeClass('from-zero').hide();
        //             }

        //             if( $('.full-video').is(':hidden') ){
        //                 $('html').removeClass('disable-scroll');
        //             }
        //         },3000);
        //     }

        //     // Pace.on('stop', function() {
        //     //     // console.log( 'pace stop' );
        //     // });

        //     // var HideShowTransition = Barba.BaseTransition.extend({
        //     //     start: function() {
        //     //         this.newContainerLoading.then(this.finish.bind(this));

        //     //         // console.log( $('body').attr('class') );

        //     //         $('.loader-anim').removeAttr('style').addClass('from-zero');
        //     //         setTimeout(function(){
        //     //             $('.from-zero').addClass('from-white');
        //     //         },200);
        //     //         // console.log( 'fadeout now' );

        //     //         // console.log( 'Transition start' );
        //     //     },
        //     //     finish: function() {
        //     //         $(window).scrollTop(0);
        //     //         this.done();
        //     //         menu();
        //     //         sharer();
        //     //         audioVideo();
        //     //         slider();
        //     //         footer();
        //     //         tooltip();
        //     //         replaceSVG();

        //     //         $('body').removeAttr('class').addClass( $('#body-classes').attr('class') );

        //     //         // console.log( 'Transition finish' );
        //     //     }
        //     // });

        //     var popping = false;

        //     window.addEventListener('popstate', function () {
        //         // console.log( 'popping true' );
        //         popping = true;
        //     });

        //     // Barba.Pjax.start();

        //     // Barba.Pjax.getTransition = function() {
        //     //     console.log( 'getTransition popping ' + popping );
        //     //     return popping ? HideShowTransition : FadeTransition;
        //     // };

        //     // Barba.Pjax.getTransition = function() {
        //     //     if( popping == true ){
        //     //         //Pace.start();
        //     //     }

        //     //     return HideShowTransition;
        //     // };

        //     // Barba.Dispatcher.on('initStateChange', function() {
        //     //     popping = false;
        //     //     // console.log( 'getTransition popping ' + popping );
        //     // });
        // }

    function form(){
        $('.form select').selectric({
            arrowButtonMarkup: '<span class="arrow"></span>'
        });

        $('.input-date').mask('00/00/0000');
    }

    function keyEvents(){
        $(document).keyup(function(e) {
            if( e.key === "Escape" ){
                if( menuIsOpen == 1 ){
                    $('.btn-menu').trigger('click');
                }else if( videoModalIsOpen == 1 ){
                    $('.full-video .btn-vid-close').trigger('click');
                }
            }

            if( e.keyCode === 75 || e.keyCode === 32 ){
                // console.log('video play/pause');
                if( videoModalIsOpen == 1 ){
                    if( player.paused() ){
                        player.play();
                    }else{
                        player.pause();
                    }
                }else{
                    $('.hero-video .play-full').trigger('click');
                }
            }

            if( e.keyCode === 77 ){
                // console.log('video volume');
                if( videoModalIsOpen == 1 ){
                    $('.full-video .audio-toggle .audio-bars').trigger('click');
                }else{
                    $('.hero-video .btn-audio .audio-bars').trigger('click');
                }
            }

            if( e.keyCode === 37 ||  e.keyCode === 39 ){
                console.log('video detail sidebar toggle+ ');
                if( $('.full-video').not(':hidden') ){
                    $('.full-video .btn-vid-arrow').trigger('click');
                }
            }
        });
    }

    $(function(){
        // readyOnce();
        menu();
        sharer();
        audioVideo();
        slider();
        footer();
        tooltip();
        form();
        keyEvents();

        // debugging ios
        // if( $(window).width() < 420 && $('#body-classes').is('.single-topic, .chrome') ){
        //     $('body').append('<div class="debug" style="width: 300px;height: 100px;position: fixed;z-index: 3424;background: cyan; right: 0;top: 40%;overflow: auto;"></div>')
        // }
    });

    function replaceSVG(){
        $('img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src')+'?x-reques=ajax';

            $.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = $(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });
    }

    $(window).on('resize',function(){
        footer();
        tooltip();
    });

    $(window).on('load', function () {
        footer();
        replaceSVG();
    });

    $(window).scroll(function () {
        var top = $(window).scrollTop();

        if( top > 10 ){
            $('body').addClass('scrolled');
        }else{
            $('body').removeClass('scrolled');
        }

        if( $('.hero-video').length && $(window).width() > 799 ){
            // var hero        = $('.hero-video');
                // startAnim   = hero.outerHeight() - 180,
                // vidIntro    = $('#video-hero-intro'),
                // vidHero     = $('#video-hero'),
            var vidContent  = $('#video-content'),
                videos      = $('.content').next('.row');
                // endAnim     = videos.offset().top - 380; // 180 + 200 buffer

            // console.log( 'top ' + top );
            // console.log( 'endAnim ' + endAnim );
            // console.log( 'content ' + ( ( top - endAnim ) / 180 ) );

            // if( top >= endAnim ){
            //     vidContent.css('opacity',( 1 - ( top - endAnim ) / 180 ));
            //     if( hero_audio_state == 'playing' ){
            //         hero.find('.audio-bars').trigger('click');

            //         vidIntro.prop( 'muted', true );

            //         hero.find('.audio-bars').css('opacity','0.3');
            //         hero.find('.audio-bars').children('div').css('animationPlayState','paused');

            //         vidContent.find('.audio-bars').css('opacity','0.3');
            //         vidContent.find('.audio-bars').children('div').css('animationPlayState','paused');

            //         hero_audio_state = 'paused';
            //     }
            // }else if( top >= startAnim ){
            //     vidIntro.appendTo( vidContent.find('.video') );
            //     vidContent.css('opacity',( ( top - startAnim ) / 180 ));
            // }else{
            //     // console.log('appding to video hero');
            //     // vidIntro.appendTo( vidHero );
            //     if( vidHero.find('video').length == 0 )
            //         vidIntro.appendTo( vidHero );

            //     vidContent.css('opacity',0);
            // }

            if( top >= ( vidContent.offset().top - 30 ) ){
                vidContent.addClass('sticky');
            }else{
                vidContent.removeClass('sticky');
            }
        }

        const mobile_top = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)

        if( $(window).width() < 420 && $('#body-classes').is('.single-topic, .os-ios, .iphone') ){
            // console.log( ( $('.empty-space').offset().top - $(window).height() ) + ' ------ ' + mobile_top );
            // $('.debug').empty().append( ( $('.empty-space').offset().top - $(window).height() ) + ' -- ' + mobile_top + ' -- ' + $('#body-classes').attr('class') );
            if( mobile_top > ( $('.empty-space').offset().top - $(window).height() ) ){
                $('#body-classes').addClass('show-next-topic');
                $('#body-classes').removeClass('hide-next-topic');
            }else{
                $('#body-classes').removeClass('show-next-topic');
                $('#body-classes').addClass('hide-next-topic');
            }
        }
    });

    document.addEventListener('keyup', function (e) {
        var ele = document.activeElement;
        // console.log( ele.tabIndex );
        // console.log( ele.getAttribute('href') );
        // console.log( ele.getAttribute('class') );
        // console.log( ele.getAttribute('title') );
        // console.log( 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' );
        if( menuIsOpen == 1 && ele.tabIndex == 0 )
            $('#header').find('.lower a:first').focus();

    }, false);

})(jQuery);