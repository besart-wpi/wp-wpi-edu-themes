<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon.ico" type="image/vnd.microsoft.icon" />
<link href="<?php bloginfo( 'template_url' ); ?>/assets/images/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180" />
<link href="<?php bloginfo( 'template_url' ); ?>/assets/images/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
<link href="<?php bloginfo( 'template_url' ); ?>/assets/images/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120" />
<link href="<?php bloginfo( 'template_url' ); ?>/assets/images/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" />
<link href="<?php bloginfo( 'template_url' ); ?>/assets/images/apple-touch-icon.png" rel="apple-touch-icon" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="page">

	<div id="">
	  <div class="">
      		<div id="body-classes" <?php body_class(); ?>></div>


	<header id="header" class="site-header" role="header">
		<div class="scroller">
			<div class="upper">
				<div class="wrap">
					<button class="btn-menu" tabindex="2">
						<span><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/menu.svg" alt="menu"></span>
						<span><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/close.svg" alt="close menu"></span>
						<span><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/arrow-down.svg" alt="arrow down"></span>
					</button>
                    <?php
                    $logo_link_url = get_field('logo_link_url', 'option') ? get_field('logo_link_url', 'option') : get_bloginfo( 'url' );
                    $logo_link_target = get_field('logo_link_open_in_new_tab', 'option') ? '_blank' : '_self';
                    ?>
					<div class="logo"><a href="<?php echo esc_url($logo_link_url); ?>" title="<?php bloginfo( 'name' ); ?>" tabindex="1" target="<?php echo $logo_link_target; ?>"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/logowhite.svg" alt="BW <?php bloginfo( 'name' ); ?>"></a></div>
					<div class="hover">
						<div class="logo"><a href="<?php echo esc_url($logo_link_url); ?>" tabIndex="-1" title="<?php bloginfo( 'name' ); ?> - Colored" target="<?php echo $logo_link_target; ?>"><img class="lazy" src="<?php bloginfo( 'template_url' ); ?>/assets/images/logowhite.svg" data-src="<?php bloginfo( 'template_url' ); ?>/assets/images/logo-colored.svg" loading="lazy" data-srcset="<?php bloginfo( 'template_url' ); ?>/assets/images/logo-colored.svg" alt="Colored <?php bloginfo( 'name' ); ?>" height="50" width="150"></a></div>
						<span class="label">
							<a href="JavaScript:void(0);" class="ShowMenuClass">Show Menu</a>
						</span>
					</div>
					<span class="border-l"></span>
					<span class="border-r"></span>
					<span class="border-b"></span>
				</div>
			</div>
			<div class="lower">
				<div class="wrap">
					<div class="topics">
						<?php
						$walker 	= new primaryWalker;
						$menu_name 	= 'primary-menu';
						$locations 	= get_nav_menu_locations();
						$menu_id 	= $locations[ $menu_name ] ;
						$primary 	= wp_get_nav_menu_object($menu_id);

						echo sprintf( '<h6>%s</h6>', $primary->name );
						wp_nav_menu( array( 'theme_location' => $menu_name, 'container' => false, 'menu_class' => 'menu menu-topic', 'depth' => 1, 'walker' => $walker ) ); ?>
					</div>
					<div class="infos">
						<?php
						$menu_name 	= 'secondary-menu';
						$locations 	= get_nav_menu_locations();
						$menu_id 	= $locations[ $menu_name ] ;
						$secondary 	= wp_get_nav_menu_object($menu_id);

						echo sprintf( '<h6>%s</h6>', $secondary->name );
						wp_nav_menu( array( 'theme_location' => $menu_name, 'container' => false, 'menu_class' => 'menu menu-infos', 'depth' => 1 ) ); ?>
						<div class="image-links">
							<?php
							$cta_btn_small_text = get_field( 'cta_btn_small_text', 'option' );
							$cta_btn_large_text = get_field( 'cta_btn_large_text', 'option' );
							$cta_btn_icon 		= get_field( 'cta_btn_icon', 'option' );
							$cta_btn_url 		= get_field( 'cta_btn_url', 'option' );
							$cta_btn_new_tab 	= get_field( 'cta_btn_new_tab', 'option' );
							$cta_btn_bg_image 	= get_field( 'cta_btn_bg_image', 'option' );

							//CDN+AJAX fix
							if($cta_btn_icon && function_exists('_campus_cdn_get_base_upload_url')) {
								$cta_btn_icon = str_replace(site_url('/files/'), _campus_cdn_get_base_upload_url(), $cta_btn_icon);
							}

							if( $cta_btn_bg_image )
								$cta_bg = sprintf( ' style="background-image: url(%s);"', $cta_btn_bg_image[ 'sizes' ][ 'thumb-cta' ] );

							if( $cta_btn_small_text && $cta_btn_large_text )
								$cta_label = $cta_btn_small_text . ' ' . $cta_btn_large_text;

							if( $cta_btn_new_tab )
								$cta_target = $cta_btn_new_tab ? '_blank' : '';

							if( $cta_btn_url ){ ?>
								<div class="btn-cta"<?php echo $cta_bg; ?>>
									<a href="<?php echo esc_url( $cta_btn_url ); ?>" title="<?php echo esc_attr( $cta_label ); ?>" target="<?php echo $cta_target; ?>"><span class="hidden"><?php echo esc_attr( $cta_label ); ?></span></a>
									<span class="subtitle"><?php echo $cta_btn_small_text; ?></span>
									<span class="title"><?php echo $cta_btn_large_text; ?></span>
									<?php if( $cta_btn_icon ){ ?>
								      	<div class="circle-small">
									        <div class="inner"></div><div class="outer"></div>
									        <img src="<?php echo $cta_btn_icon; ?>" class="svg" alt="<?php echo esc_attr( $cta_label ); ?>">
								      	</div>
								    <?php } ?>
								</div>
							<?php } ?>

							<div class="btn-cta share">
								<a href="#" class="show-sharer" title="Show it off Share Experience"><span class="hidden">Show it off Share Experience</span></a>
								<span class="subtitle">Show it off</span>
								<span class="title">Share Experience</span>
						      	<div class="circle-small">
							        <div class="inner"></div><div class="outer"></div>
							        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/share.svg" class="svg" alt="Show it off Share Experience">
						      	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>