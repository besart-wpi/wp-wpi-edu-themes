<?php get_header(); ?>

<?php the_post(); ?>

	<section class="row content content-page">
		<div class="wrap">
			<main>
				<h2 class="heading"><?php the_title(); ?></h2>
				
				<form action="" class="form">

					<h5>Select</h5>
					<div class="input-group">
						<select>
							<option value="option_1">Option 1</option>
							<option value="option_2">Option 2</option>
							<option value="option_3">Option 3</option>
							<option value="option_4">Option 4</option>
						</select> 
					</div>
					
					<br><br>

					<h5>Date</h5>
					<div class="input-group">
						<input class="input-date" type="text">
					</div>
					
					<br><br>
					
					<h5>Checkboxes</h5>
					<div class="input-group">
						<label class="checkbox"><input type="checkbox" name="checkbox-opts" value="Option 1"> <span class="label">Option 1</span></label>				
						<label class="checkbox"><input type="checkbox" name="checkbox-opts" value="Option 2"> <span class="label">Option 2</span></label>				
						<label class="checkbox"><input type="checkbox" name="checkbox-opts" value="Option 3" disabled=""> <span class="label">Option 3</span></label>				
					</div>
					
					<br><br>

					<h5>Radios</h5>
					<div class="input-group">
						<label class="radio"><input type="radio" name="radio-opts" value="Option 1"> <span class="label">Option 1</span></label>				
						<label class="radio"><input type="radio" name="radio-opts" value="Option 2"> <span class="label">Option 2</span></label>				
						<label class="radio"><input type="radio" name="radio-opts" value="Option 3" disabled=""> <span class="label">Option 3</span></label>				
					</div>
					
					<br><br>

					<h5>Textbox</h5>
					<div class="input-group">
						<input type="text" placeholder="First Name">
						<input type="text" placeholder="Last Name">
					</div>
				</form>

			</main>
		</div>
	</section>	

<?php get_footer(); ?>