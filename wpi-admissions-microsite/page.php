<?php get_header(); ?>

<?php the_post(); ?>

	<section class="row content content-page">
		<div class="wrap">
			<main>
				<h2 class="heading"><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</main>
		</div>
	</section>	

<?php get_footer(); ?>