<?php get_header(); ?>

<?php the_post(); ?>

	<?php
	$hero_heading 			= get_field( 'hero_heading' );
	$hero_subheading 		= get_field( 'hero_subheading' );
	$intro_bg_video_mp4 	= get_field( 'intro_bg_video_mp4' );
	$intro_bg_video_webm 	= get_field( 'intro_bg_video_webm' );
	$intro_bg_video_ogv 	= get_field( 'intro_bg_video_ogv' );
	$intro_bg_video_params 	= array();

	if( $intro_bg_video_mp4 )
		$intro_bg_video_params[] = sprintf( '<source data-src="%s" type="video/mp4" />', $intro_bg_video_mp4 );

	if( $intro_bg_video_webm )
		$intro_bg_video_params[] = sprintf( '<source data-src="%s" type="video/webm" />', $intro_bg_video_webm );

	if( $intro_bg_video_ogv )
		$intro_bg_video_params[] = sprintf( '<source data-src="%s" type="video/ogg" />', $intro_bg_video_ogv );

	if( $hero_heading || $intro_bg_video_params ){ ?>
		<section class="row hero-video">
			<div class="btn-play">
				<a href="#main-video" class="play-full" data-content="#main-video" title="Play"><span class="hidden">Play</span></a>
			    <div class="circle-large">
			        <div class="icon"><svg width="48px" height="48px" fill="#ffffff"><polygon id="fill" points="12 43 12 5 43 24"></polygon></svg></div>
			        <div class="cl-outer"></div>
			        <div class="cl-inner-big"></div>
			        <div class="rotator"><div class="rotate-10"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate-20"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate-30"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate-40"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate-50"><div class="rotate-bar"></div><div class="rotate-bar"></div></div>
			        </div>
			        <div class="cl-inner-small"></div>
			    </div>
			</div>
			<div class="btn-audio">
			    <div class="circle-medium">
			        <div class="cm-outer"></div>
			        <div class="cm-inner"></div>
				    <div class="rotator"><div class="rotate0"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate10"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate20"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate30"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate40"><div class="rotate-bar"></div><div class="rotate-bar"></div></div>
				    </div>
			       	<div class="icon">
					   	<div class="audio-bars" style="opacity: 0.3;" data-video="#video-hero-intro"><div class="audio-bar-1" style="animation-play-state: paused;"></div><div class="audio-bar-2" style="animation-play-state: paused;"></div><div class="audio-bar-3" style="animation-play-state: paused;"></div><div class="audio-bar-4" style="animation-play-state: paused;"></div><div class="audio-bar-5" style="animation-play-state: paused;"></div>
					    </div>
			        </div>
			    </div>
			</div>
			<h1 class="jumbo"><?php echo $hero_heading; ?></h1>
			<div class="video" id="video-hero">
				<?php if( ! empty( $intro_bg_video_params ) ){ ?>
					<video class="lazy" playsinline autoplay muted loop id="video-hero-intro">
						<?php echo implode( "\n", $intro_bg_video_params );?>
					</video>
				<?php } ?>
			</div>
			<div class="mobile-controls">
				<a href="#main-video" class="btn-play-sm play-full play-full-video" tabindex="-1" data-content="#main-video" title="Play"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/play-small.svg" class="svg" alt="play"></a>
				<div class="audio-bars" style="opacity: 0.3;" data-video="#video-hero-intro">
				    <div class="audio-bar-1" style="animation-play-state: paused;"></div>
				    <div class="audio-bar-2" style="animation-play-state: paused;"></div>
				    <div class="audio-bar-3" style="animation-play-state: paused;"></div>
				    <div class="audio-bar-4" style="animation-play-state: paused;"></div>
				    <div class="audio-bar-5" style="animation-play-state: paused;"></div>
				</div>
			</div>
		</section>
	<?php } ?>

	<?php
	$full_bg_video_mp4 			= get_field( 'full_bg_video_mp4' );
	$full_bg_video_webm 		= get_field( 'full_bg_video_webm' );
	$full_bg_video_ogv 			= get_field( 'full_bg_video_ogv' );
	$full_bg_video_params 		= array();
	$flyout_content_top 		= get_field( 'flyout_content_top' );
	$flyout_related_roi_cards 	= get_field( 'flyout_related_roi_cards' );
	$flyout_content_bottom 		= get_field( 'flyout_content_bottom' );

	if( $full_bg_video_mp4 )
		// $full_bg_video_params[] = sprintf( '<span data-type="mp4">%s</span>', $full_bg_video_mp4 );
		$full_bg_video_params[] = sprintf( '<source src="%s" type="video/mp4" />', $full_bg_video_mp4 );

	if( $full_bg_video_webm )
		// $full_bg_video_params[] = sprintf( '<span data-type="webm">%s</span>', $full_bg_video_webm );
		$full_bg_video_params[] = sprintf( '<source src="%s" type="video/webm" />', $full_bg_video_webm );

	if( $full_bg_video_ogv )
		// $full_bg_video_params[] = sprintf( '<span data-type="ogg">%s</span>', $full_bg_video_ogv );
		$full_bg_video_params[] = sprintf( '<source src="%s" type="video/ogg" />', $full_bg_video_ogv );

	if( $full_bg_video_params ){ ?>
		<div class="video-content hidden" id="main-video">
			<div class="caption">
				<h3><?php echo $hero_heading; ?></h3>
				<h5><?php echo $hero_subheading; ?></h5>
			</div>
			<div class="video">
				<?php echo implode( "\n", $full_bg_video_params ); ?>
			</div>
			<div class="text">
				<?php echo $flyout_content_top; ?>
				<?php if( $flyout_related_roi_cards ){ ?>
					<div class="video-cards">
						<div class="cards">
							<?php foreach( $flyout_related_roi_cards as $card ){ ?>
								<div class="item">
									<?php
									$image_id = get_post_thumbnail_id( $card->ID );
									$image = wp_get_attachment_image_src( $image_id, 'thumb-card' ); ?>
									<img class="lazy" style="background-color:rgba(0, 0, 0, 0.0);" src="<?php echo $image[0]; ?>" loading="lazy" width="250" alt="<?php echo $card->post_title ?>">
								</div>
							<?php } ?>
						</div>
						<div class="progress-bar"><div></div></div>
					</div>
				<?php } ?>
				<?php echo $flyout_content_bottom; ?>
			</div>
		</div>
	<?php } ?>

	<section class="row content">
		<div class="mobile-heading"><h2><?php
            // Show first word of title in bold
            $title = get_the_title();
            $words = explode(' ', $title);
            if( !empty($words) ){
                $final_title = '<strong>' . $words[0] . '</strong> ' . implode(" ", array_slice($words,1));
            } else {
                $final_title = $title;
            }
            echo $final_title;
            ?></h2></div>
		<div class="wrap">
			<div class="padding">
				<?php the_content(); ?>
				<?php get_template_part( 'parts/inline-share' ); ?>
			</div>
				<div class="video-content" id="video-content">
					<div class="inner">
						<div class="video">
							<span class="btn-play-sm play-full play-full-video" data-content="#main-video"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/play-small.svg" class="svg" alt="play"></span>
							<div class="audio-bars" style="opacity: 0.3;" data-video="#video-hero-intro">
							    <div class="audio-bar-1" style="animation-play-state: paused;"></div>
							    <div class="audio-bar-2" style="animation-play-state: paused;"></div>
							    <div class="audio-bar-3" style="animation-play-state: paused;"></div>
							    <div class="audio-bar-4" style="animation-play-state: paused;"></div>
							    <div class="audio-bar-5" style="animation-play-state: paused;"></div>
							</div>
						</div>
					</div>
				</div>
		</div>
	</section>

	<?php
	$related_heading 		= get_field( 'related_heading' );
	$related_introduction 	= get_field( 'related_introduction' );
	$related_roi_cards 		= get_field( 'related_roi_cards' );
	if( $related_roi_cards ){ ?>
		<section class="row related-roi">
			<div class="wrap">
				<?php echo $related_heading ? sprintf( '<h3>%s</h3>', $related_heading ) : ''; ?>
				<?php echo $related_introduction; ?>
				<div class="roi">
					<div class="cards roi-carousel">
						<?php foreach( $related_roi_cards as $card ){ ?>
							<div class="item">
								<?php
								$image_id = get_post_thumbnail_id( $card->ID );
								$image = wp_get_attachment_image_src( $image_id, 'thumb-card' ); ?>
								<img class="lazy" src="<?php echo $image[0]; ?>" style="background-color:rgba(0, 0, 0, 0.0);" loading="lazy" width="250" alt="<?php echo $card->post_title ?>">
							</div>
						<?php } ?>
					</div>
					<div class="roi-arrows"></div>
					<div class="progress-bar"><div></div></div>
				</div>
			</div>
		</section>
	<?php } ?>

	<?php
	$videos = get_field( 'videos' );
	if( $videos ){ ?>
		<section class="row videos">
			<div class="owl-carousel vid-carousel">
				<?php
				$counter = 1;
				foreach( $videos as $video ){
					$thumbnail_image 			= $video[ 'thumbnail_image' ];
					$caption_heading 			= $video[ 'caption_heading' ];
					$caption_body 				= $video[ 'caption_body' ];
					$video_player_heading 		= $video[ 'video_player_heading' ];
					$video_player_subheading 	= $video[ 'video_player_subheading' ];
					$video_label 				= esc_attr( $video_player_heading ) . ' - ' . $video_player_subheading;

					if( $thumbnail_image )
						$video_bg = sprintf( ' style="background-image: url(%s);"', $thumbnail_image[ 'sizes' ][ 'thumb-video-caro' ] ); ?>
				    <div class="item">
				    	<div class="inner">
							<div class="video"<?php echo $video_bg; ?>>
							    <div class="circle-large">
							    	<a href="#<?php echo sanitize_title( $video_label ); ?>" class="play-full" tabindex="-1" data-content="#<?php echo sanitize_title( $video_label ); ?>" title="Play"><span class="hidden"><?php echo $video_label; ?></span></a>
							        <div class="icon"><svg width="48px" height="48px" fill="#ffffff"><polygon id="fill" points="12 43 12 5 43 24"></polygon></svg></div>
							        <div class="cl-outer"></div><div class="cl-inner-big"></div>
							        <div class="rotator"><div class="rotate-10"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate-20"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate-30"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate-40"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate-50"><div class="rotate-bar"></div><div class="rotate-bar"></div></div>
							        </div>
							        <div class="cl-inner-small"></div>
							    </div>
							</div>
							<div class="text">
								<h3><?php echo $caption_heading; ?></h3>
								<?php echo $caption_body; ?>
							</div>
					    </div>
					</div>
				<?php $counter++;
				} ?>
			</div>
			<div class="controls">
				<div class="circle-small">
				   	<div class="inner"></div><div class="outer"></div>
				   	<img src="<?php bloginfo( 'template_url' ); ?>/assets/images/hand.svg" class="svg" alt="">
				</div>
				<div class="arrows controls-arrows"></div>
				<span class="label">Drag to navigate</span>
			</div>

			<?php
			$counter = 1;
			foreach( $videos as $video ){
				$video_mp4 					= $video[ 'video_mp4' ];
				$video_webm 				= $video[ 'video_webm' ];
				$video_ogv 					= $video[ 'video_ogv' ];
				$video_player_heading 		= $video[ 'video_player_heading' ];
				$video_player_subheading 	= $video[ 'video_player_subheading' ];
				$flyout_content_top 		= $video[ 'flyout_content_top' ];
				$flyout_related_roi_cards 	= $video[ 'flyout_related_roi_cards' ];
				$flyout_content_bottom 		= $video[ 'flyout_content_bottom' ];
				$video_params 				= array();
				$video_label 				= esc_attr( $video_player_heading ) . ' - ' . $video_player_subheading;

				if( $video_mp4 )
					$video_params[] = sprintf( '<source src="%s" type="video/mp4" />', $video_mp4 );

				if( $video_webm )
					$video_params[] = sprintf( '<source src="%s" type="video/webm" />', $video_webm );

				if( $video_ogv )
					$video_params[] = sprintf( '<source src="%s" type="video/ogg" />', $video_ogv ); ?>

				<div class="video-content hidden" id="<?php echo sanitize_title( $video_label ); ?>">
					<div class="caption">
						<h3><?php echo $video_player_heading; ?></h3>
						<h5><?php echo $video_player_subheading; ?></h5>
					</div>
					<div class="video">
						<?php echo implode( "\n", $video_params ); ?>
					</div>
					<div class="text">
						<?php echo $flyout_content_top; ?>
						<?php if( $flyout_related_roi_cards ){ ?>
							<div class="video-cards">
								<div class="cards">
									<?php foreach( $flyout_related_roi_cards as $card ){ ?>
										<div class="item">
											<?php
											$image_id = get_post_thumbnail_id( $card->ID );
											$image = wp_get_attachment_image_src( $image_id, 'thumb-card' ); ?>
											<img class="lazy" src="<?php echo $image[0]; ?>" style="background-color:rgba(0, 0, 0, 0.0);" loading="lazy" width="250" alt="<?php echo $card->post_title ?>">
										</div>
									<?php } ?>
								</div>
								<div class="progress-bar"><div></div></div>
							</div>
						<?php } ?>
						<?php echo $flyout_content_bottom; ?>
					</div>
				</div>
			<?php $counter++;
			} ?>
		</section>
	<?php } ?>

	<?php
	$next_post 		= get_next_post();
	$accent_color 	= get_field( 'accent_color', $next_post->ID );
	if( $next_post ){ ?>
		<section class="row next-topic next-<?php echo $accent_color; ?>">
			<div class="wrap">
				<a href="<?php echo get_permalink( $next_post->ID ); ?>" title="<?php echo $next_post->post_title; ?>" class="btn-tag">Next Topic</a>
				<div class="inner">
					<a href="<?php echo get_permalink( $next_post->ID ); ?>" title="<?php echo $next_post->post_title; ?>"><span class="hidden"><?php echo $next_post->post_title; ?></span></a>
					<h2><span><?php echo $next_post->post_title; ?></span></h2>
					<?php if( $next_post->post_excerpt ){ ?>
						<p><?php echo $next_post->post_excerpt; ?></p>
					<?php }else{ ?>
						<p><?php echo wp_trim_words( $next_post->post_content, 20, '...' ); ?></p>
					<?php } ?>
					<?php
					if( has_post_thumbnail( $next_post->ID ) ){
						$thumb_obj 	= wp_get_attachment_image_src( get_post_thumbnail_id( $next_post->ID ), 'thumb-next-topic' );
						$thumb_bg 	= sprintf( ' style="background-color:rgba(0, 0, 0, 0.0);" data-info="%s"', $thumb_obj[0] );
					} ?>
					<figure class="lazy-background" <?php echo $thumb_bg; ?>></figure>
				</div>
			</div>
		</section>
	<?php } ?>

	<?php get_template_part( 'parts/full-video' ); ?>

<?php get_footer(); ?>