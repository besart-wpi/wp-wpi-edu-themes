<?php get_header(); ?>
        
    <div id="main" class="main-interior">
        
        <?php get_template_part( 'includes/robot_background' ); ?>
        
        <?php get_template_part( 'includes/floating_callout' ); ?>
        
        <div id="interior_content" class="the-content">
        	
            <?php if( have_posts() ): while( have_posts() ): the_post(); ?>
            
            <h1 id="page_title"><?php the_title(); ?></h1>
            <div class="interior_content_inner">
				<?php the_content(); ?>
            </div><!-- .interior_content_inner -->
            
            <?php endwhile; endif; ?>
            
        </div>
        
    </div><!-- #main -->
        
<?php get_footer(); ?>