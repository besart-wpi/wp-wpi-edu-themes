<?php if( ot_get_option('social_facebook_url') || ot_get_option('social_twitter_url') || ot_get_option('social_instagram_url') ): ?>
<ul id="social_icons_footer" class="clear">
    <?php echo ot_get_option('social_facebook_url') ? '<li class="social_icon_facebook"><a href="'. esc_url(ot_get_option('social_facebook_url')) .'" title="' . get_bloginfo('name') . ' on Facebook" target="_blank"><span>Facebook</span></a></li>' : ''; ?>
    <?php echo ot_get_option('social_twitter_url') ? '<li class="social_icon_twitter"><a href="'. esc_url(ot_get_option('social_twitter_url')) .'" title="' . get_bloginfo('name') . ' on Twitter" target="_blank"><span>Twitter</span></a></li>' : ''; ?>
    <?php echo ot_get_option('social_instagram_url') ? '<li class="social_icon_instagram"><a href="'. esc_url(ot_get_option('social_instagram_url')) .'" title="' . get_bloginfo('name') . ' on Instagram" target="_blank"><span>Instagram</span></a></li>' : ''; ?>
</ul>
<?php endif; ?>

<footer id="footer_main">
        	<div id="footer_inner" class="clear">
            	
              <div id="footer_block_wpi" class="footer_block">
                  <div class="aboutwpi_wrapper"><a class="text aboutwpi" href="http://wp.wpi.edu/touchtomorrow/about-wpi">&raquo; About WPI</a></div>              </div> 
              
              <div class="wgbh_wrapper"><div id="footer_block_wgbh" class="footer_block"></div></div>
                            
            </div><!-- #footer_inner -->
            <!--<p id="copyright_line">Copyright &copy; <?php echo date('Y'); ?> Worcester Polytechnic Institute. All Rights Reserved.</p> -->

        </footer>
        
        
    </div><!-- #container -->

<?php wp_footer(); ?>
</body>
</html>