<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title(''); ?></title>
 <?php 
 /*  EKG 4-14-16 Installed Yoast SEO which is now taking care of all this.
    <title><?php
	// is the current page a tag archive page?
	if (function_exists('is_tag') && is_tag()) { 
	
		// if so, display this custom title
		echo 'Tag Archive for &quot;'.$tag.'&quot; - '; 

	// or, is the page an archive page?
	} elseif (is_archive()) { 
	
		// if so, display this custom title
		wp_title(''); echo ' Archive - '; 
	
	// or, is the page a search page?
	} elseif (is_search()) { 
	
		// if so, display this custom title
		echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; 
	
	} elseif( is_front_page() ){
		// Nothing...
		
	// or, is the page a single post or a literal page?
	} elseif (!(is_404()) && (is_single()) || (is_page())) { 
	
		// if so, display this custom title
		wp_title(''); echo ' - '; 
	
	// or, is the page an error page?
	} elseif (is_404()) {
	
		// yep, you guessed it
		echo 'Not Found - '; 
	
	}
	// finally, display the blog name for all page types
	bloginfo('name'); 
	if(is_front_page()){ echo ' - ' . get_bloginfo('description'); }
	
	?></title>

    <!-- Facebook -->
    <meta property="og:title" content="TouchTomorrow - A Festival of Science, Technology &amp; Robots" />
    <meta property="og:image" content="http://wp.wpi.edu/touchtomorrow/files/2015/03/facebook-june13-2015.png" />
    <meta property="og:description" content="A family-friendly festival at WPI featuring NASA, WGBH, and friends.  June 13th, Rain or Shine.  Free and open to the public." />

*/
?>	
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/images/favicon.png">

    <?php wp_head(); ?>
    <!--[if lt IE 9]>
		<link rel="stylesheet" href="<?= get_stylesheet_directory_uri() ?>/css/ie7.css">
    <![endif]-->
</head>

<body <?php body_class(); ?>>

	<div id="container">
		<div class="masthead-background">
			<header id="masthead">
				
				<div id="mobnav-btn"></div>
				
				<h1 id="logo"><a href="<?php echo site_url(); ?>" title="Return to the <?php bloginfo('name'); ?> Home Page"><?php bloginfo('name'); ?></a></h1>
				
				<div id="date_block">
					<?php echo ot_get_option('header_date') ? '<span class="date">' . ot_get_option('header_date') .'</span>' : ''; ?> <?php echo ot_get_option('header_time') ? '<span class="time">' . ot_get_option('header_time') . '</span>' : ''; ?></span><br />
					<?php echo ot_get_option('header_location') ? '<span class="location">' . ot_get_option('header_location') . '</span>' : ''; ?>
				</div>
				
				<nav id="nav_main_menu">
					<?php wp_nav_menu( array( 
						'theme_location' => 'main_menu', 
						'menu_class' => 'nav-menu sf-menu', 
						'fallback_cb' => '', 
						'depth' => 0,
						'link_before' => '<span>',
						'link_after' => '</span>'
					)); ?>
				</nav>
				
				<?php if( ot_get_option('social_facebook_url') || ot_get_option('social_twitter_url') || ot_get_option('social_instagram_url') ): ?>
				<ul id="social_media_icons">
					<?php echo ot_get_option('social_facebook_url') ? '<li class="social_icon_facebook"><a href="'. esc_url(ot_get_option('social_facebook_url')) .'" title="' . get_bloginfo('name') . ' on Facebook" target="_blank"><span>Facebook</span></a></li>' : ''; ?>
					<?php echo ot_get_option('social_twitter_url') ? '<li class="social_icon_twitter"><a href="'. esc_url(ot_get_option('social_twitter_url')) .'" title="' . get_bloginfo('name') . ' on Twitter" target="_blank"><span>Twitter</span></a></li>' : ''; ?>
					<?php echo ot_get_option('social_instagram_url') ? '<li class="social_icon_instagram"><a href="'. esc_url(ot_get_option('social_instagram_url')) .'" title="' . get_bloginfo('name') . ' on Instagram" target="_blank"><span>Instagram</span></a></li>' : ''; ?>
				</ul>
				<?php endif; ?>
			</header>
		</div>