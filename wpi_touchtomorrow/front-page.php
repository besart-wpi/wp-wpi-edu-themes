<?php get_header(); ?>

    <div id="main">
        <div id="slideshow_wrap">

            <div id="slideshow_home" class="cycle-slideshow"
            data-cycle-fx="scrollHorz"
            data-cycle-pause-on-hover="true"
            data-cycle-speed="1500"
            data-cycle-timeout="4000"
            data-cycle-easing="easeInOutCubic"
            data-cycle-prev="#prev"
            data-cycle-next="#next"
            >
                <?php
                $args = array(
                    'post_type'         => 'home_page_slideshow',
                    'posts_per_page'    => -1,
                    'orderby'           => 'menu_order',
                    'order'             => 'ASC'
                );
                $slideshow_slides = new WP_Query( $args ); ?>

                <?php if ( $slideshow_slides->have_posts() ) : while ( $slideshow_slides->have_posts() ) : $slideshow_slides->the_post(); ?>
                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'home_page_slideshow' ); $url = $image['0']; ?>
                    <img src="<?php echo $url; ?>" alt="" />
                <?php
                endwhile;
                endif;
                wp_reset_postdata();
                ?>
            </div><!-- #slideshow_home -->

            <?php if( $slideshow_slides->post_count > 1 ): ?>
            <div id="slideshow_home_nav">
                <a href="#" id="next">Next</a>
                <a href="#" id="prev">Prev</a>
            </div>
            <?php endif; ?>

        </div><!-- #slideshow_wrap -->

        <?php get_template_part( 'includes/robot_background' ); ?>

        <?php get_template_part( 'includes/floating_callout' ); ?>

        <div id="home_content" class="the-content">
            <div id="mobile-robot-right-arm"></div>
            <?php if (have_posts()) : while (have_posts()) : the_post();
                the_content();
            endwhile; endif; ?>
        </div>
    </div><!-- #main -->

<?php get_footer(); ?>