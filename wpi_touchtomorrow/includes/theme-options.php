<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
   
	$custom_settings = array( 
	
		'sections'        => array( 
			array(
			'id'          => 'section_home_page_slideshow',
			'title'       => __( 'Home Page Slideshow', 'wpi-parent' )
			),
			array(
			'id'          => 'section_home_page_videos',
			'title'       => __( 'Home Page Video Gallery', 'wpi-parent' )
			),
			array(
			'id'          => 'section_header_options',
			'title'       => __( 'Header Options', 'wpi-parent' )
			),
			array(
			'id'          => 'section_social_media',
			'title'       => __( 'Social Media Options', 'wpi-parent' )
			),
			array(
			'id'          => 'section_floating_callout',
			'title'       => __( 'Floating Callout Options', 'wpi-parent' )
			),
			array(
			'id'          => 'section_footer_options',
			'title'       => __( 'Footer Options', 'wpi-parent' )
			)
		),
		
		'settings'        => array( 
		
			// Home Page Slideshow
			array(
				'id'          => 'home_page_slideshow_pause_time',
				'label'       => __( 'Pause Time', 'wpi-parent' ),
				'desc'        => __( 'Slide pause time (in milliseconds)', 'wpi-parent' ),
				'std'         => '5000',
				'type'        => 'text',
				'section'     => 'section_home_page_slideshow',
				'class'       => '',
			),
			array(
				'id'          => 'home_page_slideshow_transition_time',
				'label'       => __( 'Transition Time', 'wpi-parent' ),
				'desc'        => __( 'Slide transition time (in milliseconds)', 'wpi-parent' ),
				'std'         => '1000',
				'type'        => 'text',
				'section'     => 'section_home_page_slideshow',
				'class'       => '',
			),
			
			
			// Home Page Videos
			array(
				'id'          => 'home_page_video_gallery_id',
				'label'       => __( 'Home Page Video Gallery', 'wpi-parent' ),
				'desc'        => __( 'Select an existing video gallery', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'custom-post-type-select',
				'post_type'   => 'video_gallery',
				'section'     => 'section_home_page_videos',
				'class'       => '',
			),
			
			
			// Header Options
			array(
				'id'          => 'header_date',
				'label'       => __( 'Header Date', 'wpi-parent' ),
				'desc'        => __( 'Enter a date to display in the header (example: June 14, 2014)', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_header_options',
				'class'       => '',
			),
			array(
				'id'          => 'header_time',
				'label'       => __( 'Header Time', 'wpi-parent' ),
				'desc'        => __( 'Enter a time to display in the header (example: 10AM - 4PM)', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_header_options',
				'class'       => '',
			),
			array(
				'id'          => 'header_location',
				'label'       => __( 'Header Location', 'wpi-parent' ),
				'desc'        => __( 'Enter a location to display in the header (example: at Worcester Polytechnic Institute)', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_header_options',
				'class'       => '',
			),
			
			// Social Media
			array(
				'id'          => 'social_facebook_url',
				'label'       => __( 'Facebook URL', 'wpi-parent' ),
				'desc'        => __( 'Enter a URL', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_social_media',
				'class'       => '',
			),
			array(
				'id'          => 'social_twitter_url',
				'label'       => __( 'Twitter URL', 'wpi-parent' ),
				'desc'        => __( 'Enter a URL', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_social_media',
				'class'       => '',
			),
			array(
				'id'          => 'social_instagram_url',
				'label'       => __( 'Instagram URL', 'wpi-parent' ),
				'desc'        => __( 'Enter a URL', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_social_media',
				'class'       => '',
			),
			
			// Floating Callout
			array(
				'id'          => 'floating_callout_text',
				'label'       => __( 'Floating Callout Text', 'wpi-parent' ),
				'desc'        => __( 'This text appears next to the sun icon on the home and interior pages', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'textarea-simple',
				'rows'		  => 3,
				'section'     => 'section_floating_callout',
				'class'       => '',
			),
			
			// Footer Options
			array(
				'id'          => 'footer_wpi_url',
				'label'       => __( 'WPI Logo URL', 'wpi-parent' ),
				'desc'        => __( 'Enter a URL', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_footer_options',
				'class'       => '',
			),
			array(
				'id'          => 'footer_nasa_url',
				'label'       => __( 'NASA Logo URL', 'wpi-parent' ),
				'desc'        => __( 'Enter a URL', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_footer_options',
				'class'       => '',
			),
			array(
				'id'          => 'footer_usa_url',
				'label'       => __( 'USA SEF Festival Logo URL', 'wpi-parent' ),
				'desc'        => __( 'Enter a URL', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_footer_options',
				'class'       => '',
			),
			array(
				'id'          => 'footer_wgbh',
				'label'       => __( 'WGBH Logo', 'wpi-parent' ),
				'desc'        => __( 'Enter a URL', 'wpi-parent' ),
				'std'         => '',
				'type'        => 'text',
				'section'     => 'section_footer_options',
				'class'       => '',
			)
		)
	);
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
}