<?php if(ot_get_option('floating_callout_text')): ?>
    <div id="floating_callout">
        <p><?php echo nl2br(ot_get_option('floating_callout_text')); ?></p>
    </div><!-- #floating_callout -->
<?php endif; ?>