var sf;

jQuery(document).ready(function($){
	
	// Superfish Menu;
	sf = $('ul.sf-menu');
	
    if( !$('#mobnav-btn').is(":visible") ) {
		sf.supersubs({
			minWidth:	10,	 // minimum width of submenus in em units
			maxWidth:	16,	 // maximum width of submenus in em units
			extraWidth:	1
		}).superfish({
			cssArrows: false
		});
    }
	
    $(window).resize(function() {
		
		if( $('#mobnav-btn').is(":visible") && sf.hasClass('sf-js-enabled') ) {
            // smaller screen, disable SuperFish
			// console.log('Superfish DESTROY');
            sf.superfish('destroy');
        } else if( !$('#mobnav-btn').is(":visible") && !sf.hasClass('sf-js-enabled') ) {
			// console.log('Superfish INIT');
            // you only want SuperFish to be re-enabled once (sf.hasClass)
			sf.supersubs({
				minWidth:	10,	 // minimum width of submenus in em units
				maxWidth:	16,	 // maximum width of submenus in em units
				extraWidth:	1
			}).superfish({
				cssArrows: false
			});
        }
		
    });
	
	
	// Add drop-down buttons for mobile menu
	jQuery('ul.nav-menu > li > ul.sub-menu').before('<div class="mobnav-subarrow"></div>');
	
	// Mobile Menu
	$('#mobnav-btn').click(function () {
		$('.sf-menu').toggleClass("xactive");
	});
	
	$('.mobnav-subarrow').click(function () {
		$(this).parent().toggleClass("xpopdrop");
	});


	/*
	|--------------------------------------------------------------------------
	| Robot Animation
	|--------------------------------------------------------------------------
	|
	| Using JS vs CSS for better legacy IE support.
	|
	| Basically, here's what's gonna happen when you hover over a menu:
	| 	1. The Festival - Make head "party rock"
	| 	2. The Challenge - Say Hi!
	| 	3. Media - Make Eyes Pop
	|	4. Exhibitors - Get Shocked
	|	5. Connect with Us - Smiley Eyes
	|
	
	var $robot = {
		all: 	$('#robot_background'),
		head: 	$('#robot_head'),
		eyes: 	$('#robot_eyes'),
		body: 	$('#robot_body'),
		left: 	$('#robot_left_arm'),
		right: 	$('#robot_right_arm'),
		treads: $('#robot_treads'),
		radar: 	$('#antenna'),

		speed: 	{
			blip: 250
		},

		//- - - - - - - - - - - - - - - - - - - - - - - -
		// Blip Effect
		// 
		// callback	[Function]	Callback function to execute after animation ends
		// 			[INT]		Repeat blip this many MORE times
		// 			
		// returns: $robot, for chaining
		//- - - - - - - - - - - - - - - - - - - - - - - -
		blip: 	function(callback){
			//Reset
			$robot.radar.stop(true).css({
				width: 0, height: 0,
				left: 117, top: 30,
				opacity: 0,
				borderWidth: 8
			})
			//Animate
			$robot.radar.fadeTo($robot.speed.blip / 2, 1, function(){
				$robot.radar.fadeTo($robot.speed.blip / 4, 0)
			})

			$robot.radar.animate({
				width: 128, height: 128,
				left: '-=56', top: '-=56',	//56 instead of 64 to account for added border-width
				borderWidth: 1
			}, $robot.speed.blip, function(){
				if(typeof callback == 'function') callback()
				else if(typeof callback == 'number' && callback > 0) $robot.blip(--callback)
			})

			return $robot;
		},

		//- - - - - - - - - - - - - - - - - - - - - - - -
		// Party Rock
		// 
		// callback	[Function]	Callback function to execute after animation ends
		// 			[INT]		Party Rock this many MORE times
		//- - - - - - - - - - - - - - - - - - - - - - - -
		partyRock: function(callback){
			$robot.head.rotate(100, -10, function(){
				$robot.head.rotate(100, 10, function(){
					$robot.head.rotate(100, -10, function(){
						$robot.head.rotate(100, 10, function(){
							$robot.head.rotate(100, 0, -10)
						}, -10)
					}, 10)
				}, -10)
			})
			return $robot
		}
	}

	//- - - - - - - - - - - - - - - - - - - - - - - -
	// "Systems Check"
	// ! Only systems check on homepage, otherwise it gets annoying
	//- - - - - - - - - - - - - - - - - - - - - - - -
	// console.log('Robot Systems Check...');
	$robot.blip(2)
//	$robot.partyRock()

	//- - - - - - - - - - - - - - - - - - - - - - - -
	// Menu Based Animations
	//- - - - - - - - - - - - - - - - - - - - - - - -
	$('#nav_main_menu a').mouseenter(function(){
		var $this = $(this)
		var $parent = $this.parent()
		$robot.blip()

//		if($parent.hasClass('menu_the_festival')) $robot.partyRock()
	})
	*/
	
}); // document ready



/*
|--------------------------------------------------------------------------
| Some Rotation Magic
|--------------------------------------------------------------------------
|
| speed 	[INT]	number of milliseconds to reach target
| degrees	[INT]	Target degrees
| callback 	[func]	Callback function to call after animation ends
| current 	[INT] 	The angle to start out at
|
*/
jQuery.fn.rotate = function(speed, degrees, callback, current) {
    var self = jQuery(this)
    current = current || 0
    interval = Math.abs(speed / degrees)

    if(degrees < 0 ) current--
	else current++

	//- - - - - - - - - - - - - - - - - - - - - - - -
	// Rotate...limited support!
	//- - - - - - - - - - - - - - - - - - - - - - - -
    self.css({
        '-webkit-transform' : 'rotate(' + current + 'deg)',
        '-moz-transform' : 'rotate(' + current + 'deg)',
        '-ms-transform' : 'rotate(' + current + 'deg)',
        'transform' : 'rotate(' + current + 'deg)'
    })

	//- - - - - - - - - - - - - - - - - - - - - - - -
	// Keep rotating until we hit the target
	//- - - - - - - - - - - - - - - - - - - - - - - -
    if (current < degrees - 1 || current > degrees + 1) {
        setTimeout(function() {
            self.rotate(speed, degrees, callback, current)
        }, interval)
    } else {
	    self.css({
	        '-webkit-transform' : 'rotate(' + degrees + 'deg)',
	        '-moz-transform' : 'rotate(' + degrees + 'deg)',
	        '-ms-transform' : 'rotate(' + degrees + 'deg)',
	        'transform' : 'rotate(' + degrees + 'deg)'
	    })
	    if(typeof callback == 'function') callback()
    }
}