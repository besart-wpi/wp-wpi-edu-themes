<?php get_header(); ?>
	
	<div id="main">
		<div class="section-results section-page"><div class="section-page-border"></div><div class="section-page-inner">
			<div class="common-content container the-content clearfix">
				<?php if(have_posts()): while(have_posts()): the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div><!-- end #main -->

<?php get_footer();