	<footer id="footer">
		<div class="container clearfix">
			<div class="footer-left">
				<div class="footer-left-info clearfix">
					<div class="footer-logo">
						<?php
						$logo_link = esc_url(home_url('/'));
						if(is_front_page()){
							$logo_link = '#top';
						}
						?>
						<a href="http://www.wpi.edu/admissions/undergraduate.html"><img src="<?php bloginfo('template_url'); ?>/images/logo_footer.png" alt="<?php bloginfo('name'); ?>"></a>
					</div>
					<div class="footer-left-text clearfix">
						<address class="footer-address">
							<?php ot_echo_option('footer_address'); ?>
						</address>
						<ul class="clearfix">						
							<?php if($the_link = esc_url(ot_get_option('footer_contact_url'))): ?>
								<li><a href="<?php echo $the_link; ?>"><?php _e('Contact Us', 'roi'); ?></a></li>
							<?php endif; ?>
							<?php if($the_link = esc_url(ot_get_option('footer_directions_url'))): ?>
								<li><a href="<?php echo $the_link; ?>"><?php _e('Directions', 'roi'); ?></a></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
				<div class="footer-copyright">&copy; <?php echo date('Y'); ?> Worcester Polytechnic Institute. All rights reserved.</div>
			</div>
			<div class="footer-right clearfix">
				<span class="social-label"><?php _e('Connect With Us!', 'roi'); ?></span>
                
				<?php if($the_link = esc_url(ot_get_option('instagram_url'))): ?>
					<a href="<?php echo $the_link; ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_instagram.png" alt=""></a>
				<?php endif; ?>
				<?php if($the_link = esc_url(ot_get_option('twitter_url'))): ?>
					<a href="<?php echo $the_link; ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_twitter.png" alt=""></a>
				<?php endif; ?>
				<?php if($the_link = esc_url(ot_get_option('facebook_url'))): ?>
					<a href="<?php echo $the_link; ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_facebook.png" alt=""></a>
				<?php endif; ?>
				<?php if($the_link = esc_url(ot_get_option('youtube_url'))): ?>
					<a href="<?php echo $the_link; ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_youtube.png" alt=""></a>
				<?php endif; ?>
                <br class="icon-break">
				<?php if($the_link = esc_url(ot_get_option('inquire_url'))): ?>
					<a href="<?php echo $the_link; ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_inquire.png" alt=""><span><?php _e('Inquire', 'roi'); ?></span></a>
				<?php endif; ?>
				<?php if($the_link = esc_url(ot_get_option('visit_url'))): ?>
					<a href="<?php echo $the_link; ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_visit.png" alt=""><span><?php _e('Visit', 'roi'); ?></span></a>
				<?php endif; ?>
				<?php if($the_link = esc_url(ot_get_option('apply_url'))): ?>
					<a href="<?php echo $the_link; ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_apply.png" alt=""><span><?php _e('Apply', 'roi'); ?></span></a>
				<?php endif; ?>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>