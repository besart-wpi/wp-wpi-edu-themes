<?php /* Template Name: Imagine */ ?>
<?php get_header(); ?>
	
	<div id="main">
		<?php if(have_posts()): while(have_posts()): the_post(); ?>
			<?php		
			$counter = 1;
			get_template_part('template-parts/template', 'imagine');
			?>
		<?php endwhile; endif; ?>		
	</div><!-- end #main -->

<?php get_footer();