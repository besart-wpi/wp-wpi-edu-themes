<div id="<?php echo $post->post_name; ?>" class="section-imagine section-page"><div class="section-page-border"></div><div class="section-page-inner">

	<?php get_template_part('template-parts/content'); ?>

	<?php get_template_part('template-parts/quotes'); ?>

	<?php get_template_part('template-parts/video'); ?>
	
	<div class="icon-text-boxes"><div class="container clearfix">
		
		<h3 class="itb-heading mobile-view"><?php echo trim(get_post_meta($post->ID, '_icon_text_heading', true)); ?></h3>
		
		<div class="itb-wrap clearfix desktop-view">			
			<div class="itb-border-vertical ibv-1 desktop-view"></div>
			<div class="itb-border-vertical ibv-2 desktop-view"></div>
			<div class="itb-box itb-box-1 desktop-view">
				<h3 class="itb-heading"><?php echo trim(get_post_meta($post->ID, '_icon_text_heading', true)); ?></h3>
			</div>
			<?php for($i = 1; $i <= 5; $i++): ?>
			<div class="itb-box itb-cycle itb-box-<?php echo $i+1; ?> clearfix"><div class="itb-inner">
				<div class="itb-image">
					<?php
					$itb_image = get_post_meta($post->ID, '_itb_'.$i.'_icon', true);
					if($itb_image):
						$image_srcset = theme_get_scrset($itb_image, 'icon-text-size');
					?>
						<img srcset="<?php echo $image_srcset; ?>" alt="">
					<?php
					endif;
					?>				
				</div>
				<div class="itb-text">
					<?php
					$itb_text = get_post_meta($post->ID, '_itb_'.$i.'_text', true);
					echo wpautop(stripslashes($itb_text));
					?>
				</div>
			</div></div>
				<?php if($i == 2): ?>
				<div class="clear desktop-view"></div>
				<div class="itb-border-horizontal desktop-view"></div>
				<?php endif; ?>
			<?php endfor; ?>
		</div>
		
		<div class="itb-wrap clearfix mobile-view mobile-cycle"
		data-cycle-fx="scrollHorz"
		data-cycle-timeout="0"		
		data-cycle-auto-height="calc"
		data-cycle-slides="> div.itb-cycle"	
		data-cycle-swipe="true"
		>
			<a href="#" class="cycle-prev mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/prev_white.png" alt=""></a>
			<a href="#" class="cycle-next mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/next_white.png" alt=""></a>			
			
			<?php for($i = 1; $i <= 5; $i++): ?>
			<div class="itb-box itb-cycle itb-box-<?php echo $i+1; ?> clearfix"><div class="itb-inner">
				<div class="itb-image">
					<?php
					$itb_image = get_post_meta($post->ID, '_itb_'.$i.'_icon', true);
					if($itb_image):
						$image_srcset = theme_get_scrset($itb_image, 'icon-text-size');
					?>
						<img srcset="<?php echo $image_srcset; ?>" alt="">
					<?php
					endif;
					?>				
				</div>
				<div class="itb-text">
					<?php
					$itb_text = get_post_meta($post->ID, '_itb_'.$i.'_text', true);
					echo wpautop(stripslashes($itb_text));
					?>
				</div>
			</div></div>
				
			<?php endfor; ?>
		</div>
		
	</div></div>

</div></div>