<div class="container page-content clearfix">
	<?php $hero_image_desktop = get_post_meta($post->ID, '_hero_image_desktop', true); ?>
	<?php if($hero_image_desktop): ?>
		<div class="hero-image desktop-view">
			<img srcset="<?php echo theme_get_scrset($hero_image_desktop, 'bg-size'); ?>" alt="">
		</div>
	<?php endif; ?>
	<?php $hero_image_mobile = get_post_meta($post->ID, '_hero_image_mobile', true); ?>
	<?php if($hero_image_mobile): ?>
		<div class="hero-image mobile-view">
			<img srcset="<?php echo theme_get_scrset($hero_image_mobile, 'bg-size'); ?>" alt="">
		</div>
	<?php endif; ?>
	<div class="the-content-wrap clearfix">
		<div class="page-title-wrap clearfix">
			<h2 class="page-content-title"><?php the_title(); ?></h2>
		</div>
		<div class="the-content page-main-content clearfix">
			<?php the_content(); ?>
		</div>
	</div>
</div>