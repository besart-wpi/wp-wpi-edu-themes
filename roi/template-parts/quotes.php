<?php $quotes = get_post_meta($post->ID, '_quotes', true); ?>
<?php if(!empty($quotes)): ?>
	<div class="quotes-section"><div class="container">
		<div class="quotes-slideshow cycle-slideshow"
		data-cycle-slides="> div.slide-quote"
		data-cycle-auto-height="calc"
		data-cycle-timeout="0"
		data-cycle-swipe="true"
		>
			<?php if(count($quotes) > 1): ?>
				<div class="cycle-prev desktop-view button-with-hover"><img src="<?php bloginfo('template_url'); ?>/images/quote_prev.png" alt="" class="unhover-button"><img src="<?php bloginfo('template_url'); ?>/images/quote_prev_hover.png" alt="" class="hover-button"></div>
				<div class="cycle-next desktop-view button-with-hover"><img src="<?php bloginfo('template_url'); ?>/images/quote_next.png" alt="" class="unhover-button"><img src="<?php bloginfo('template_url'); ?>/images/quote_next_hover.png" alt="" class="hover-button"></div>
				<a href="#" class="cycle-prev mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/slide_mobile_prev.png" alt=""></a>
				<a href="#" class="cycle-next mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/slide_mobile_next.png" alt=""></a>		

			<?php endif; ?>
			
			<?php foreach($quotes as $quote): ?>
				<div class="slide-quote">
					<div class="quote"><div class="quote-inner">
						<div class="quote-open"><img src="<?php bloginfo('template_url'); ?>/images/quote_open.png" alt=""></div>
						<div class="quote-close"><img src="<?php bloginfo('template_url'); ?>/images/quote_close.png" alt=""></div>
						<div class="quote-text">
							<?php
							$quote_text = '<span class="mobile-quote-open quote-open"><img src="'.get_bloginfo('template_url').'/images/quote_open.png" alt=""></span> ';				
							$quote_text .= stripslashes($quote['text']);
							?>
							<?php echo wpautop($quote_text); ?>
						</div>
						<?php if($quote['author']): ?>
						<div class="quote-author">– <?php echo $quote['author']; ?>
							<span class="mobile-quote-open quote-close"><img src="<?php bloginfo('template_url'); ?>/images/quote_close.png" alt=""></span>
						</div>
						<?php endif; ?>
					</div></div>
				</div>
			<?php endforeach; ?>
		</div>
		
		
	
	</div></div><!-- .quotes-section -->

<?php endif;