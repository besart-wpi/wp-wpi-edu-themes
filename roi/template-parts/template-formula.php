<div id="<?php echo $post->post_name; ?>" class="section-formula section-page"><div class="section-page-border"></div><div class="section-page-inner">

	<?php get_template_part('template-parts/content');  ?>

	<div class="equations-section"><div class="container clearfix">
	
		<div class="equation-column">
			<a href="#" class="equation-button">
				<img src="<?php bloginfo('template_url'); ?>/images/eq_theory.png" alt="">
				<h3>Theory</h3>
			</a>
			<?php $facts = get_post_meta($post->ID, '_theory_facts', true); ?>
			<?php if(!empty($facts)): ?>
				<div class="equation-list">
					<ol>
					<?php foreach($facts  as $fact): ?>
						<li><div class="eq-list"><?php echo wpautop($fact['text']); ?></div></li>
					<?php endforeach; ?>
					</ol>
				</div>
			<?php endif; ?>
		</div>
		
		<div class="equation-column">			
			<a href="#" class="equation-button">
				<img src="<?php bloginfo('template_url'); ?>/images/eq_practice.png" alt="">
				<h3>Practice</h3>
				<div class="equation-x"><img src="<?php bloginfo('template_url'); ?>/images/x_bigger.png" alt=""></div>
			
			</a>
			<?php $facts = get_post_meta($post->ID, '_practice_facts', true); ?>
			<?php if(!empty($facts)): ?>
				<div class="equation-list">
					<ol>
					<?php foreach($facts  as $fact): ?>
						<li><div class="eq-list"><?php echo wpautop($fact['text']); ?></div></li>
					<?php endforeach; ?>
					</ol>
				</div>
			<?php endif; ?>
		</div>
		
		<div class="equation-column">
			
			<a href="#" class="equation-button">
				<img src="<?php bloginfo('template_url'); ?>/images/eq_global.png" alt="">
				<h3>Global</h3>
				<div class="equation-x"><img src="<?php bloginfo('template_url'); ?>/images/x_bigger.png" alt=""></div>
			
			</a>
			<?php $facts = get_post_meta($post->ID, '_global_facts', true); ?>
			<?php if(!empty($facts)): ?>
				<div class="equation-list">
					<ol>
					<?php foreach($facts  as $fact): ?>
						<li><div class="eq-list"><?php echo wpautop($fact['text']); ?></div></li>
					<?php endforeach; ?>
					</ol>
				</div>
			<?php endif; ?>
		</div>
		
		<div class="equation-column">
			
			<a href="#" class="equation-button">
				<img src="<?php bloginfo('template_url'); ?>/images/eq_impact.png" alt="">
				<h3>Impact</h3>
				<div class="equation-x"><img src="<?php bloginfo('template_url'); ?>/images/x_bigger.png" alt=""></div>
			
			</a>
			<?php $facts = get_post_meta($post->ID, '_impact_facts', true); ?>
			<?php if(!empty($facts)): ?>
				<div class="equation-list">
					<ol>
					<?php foreach($facts  as $fact): ?>
						<li><div class="eq-list"><?php echo wpautop($fact['text']); ?></div></li>
					<?php endforeach; ?>
					</ol>
				</div>
			<?php endif; ?>
		</div>

		<div class="equation-column last-eq">
			<div class="equation-x"><img src="<?php bloginfo('template_url'); ?>/images/equal.png" alt=""></div>
			
			<img src="<?php bloginfo('template_url'); ?>/images/wpi.png" alt="" class="eq-wpi">
		</div>
		
		
	</div></div><!-- .equations-section -->

	<?php get_template_part('template-parts/quotes'); ?>

	<?php get_template_part('template-parts/video'); ?>	

</div></div>

