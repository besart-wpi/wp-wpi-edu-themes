<div id="<?php echo $post->post_name; ?>" class="section-learnmore section-page"><div class="section-page-border"></div><div class="section-page-inner">

	<?php get_template_part('template-parts/content');  ?>

	
	<?php
	$form_caption = get_post_meta($post->ID, '_form_caption', true);
	$form_heading = trim(get_post_meta($post->ID, '_form_heading', true));
	$form_id = get_post_meta($post->ID, '_form_id', true);
	?>
	<div class="request-information"><div class="container clearfix">
		<div class="ri-left">
			<?php if($form_caption): ?>
			<div class="ri-caption">
				<?php echo wpautop(stripslashes($form_caption)); ?>
			</div>
			<?php endif; ?>
			<div class="ri-heading">
				<h3><?php echo $form_heading; ?></h3>
			</div>
		</div>
		<div class="ri-right">
			<div class="contact-form">
				<?php echo do_shortcode('[contact-form-7 id="'.$form_id.'"]'); ?>
			</div>
		</div>
	</div></div>
	
	
	<div class="learn-more-statistic">
		<div class="statistic-boxes"><div class="container clearfix">
			<?php if($heading = trim(get_post_meta($post->ID, '_statistic_heading', true))): ?>
				<h3 class="stat-heading"><?php echo $heading; ?></h3>
			<?php endif; ?>
			<a href="#" id="learnmore-stat-prev" class="cycle-prev mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/prev_white.png" alt=""></a>
			<a href="#" id="learnmore-stat-next" class="cycle-next mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/next_white.png" alt=""></a>
				
			<div class="container-inner statistic-wrap"
			data-cycle-fx="carousel"
			data-cycle-timeout="0"
			data-cycle-carousel-visible="2"
			data-cycle-carousel-fluid="true"
			data-cycle-slides="> .statistic-box-outer"
			data-cycle-prev="#learnmore-stat-prev"
			data-cycle-next="#learnmore-stat-next"
			data-cycle-swipe="true"
			>
				
				<?php for($i = 1; $i <= 10; $i++): ?>
					<?php
					$box_heading = trim(get_post_meta($post->ID, '_box_'.$i.'_heading', true));
					$box_sub_heading = trim(get_post_meta($post->ID, '_box_'.$i.'_sub_heading', true));
					$box_text = trim(get_post_meta($post->ID, '_box_'.$i.'_text', true));
					$box_url = esc_url(get_post_meta($post->ID, '_box_'.$i.'_url', true));
					?>
					<div class="statistic-box-outer"><div class="statistic-box-outer-inner">
					<?php if($box_url): ?>
						<a class="box-<?php echo $i; ?> statistic-box" href="<?php echo $box_url; ?>">
					<?php else: ?>
						<div class="box-<?php echo $i; ?> statistic-box">
					<?php endif; ?>
							<div class="box-inner"><div class="box-inner-inner">
								<div class="box-heading-wrap">								
									<span class="box-heading"><?php echo $box_heading; ?></span><span class="box-sub-heading"><?php echo $box_sub_heading; ?></span>
								</div><div class="box-text">
									<?php echo $box_text; ?>
								</div>
							</div></div>
					<?php if($box_url): ?>
						</a>
					<?php else: ?>
						</div>
					<?php endif; ?>
					</div></div>
				<?php endfor; ?>	
			</div>
		</div></div><!-- .statistic-boxes -->
	</div>

</div></div>