<?php
include (trailingslashit(get_template_directory()).'includes/config.php');
include (trailingslashit(get_template_directory()).'includes/custom-shortcodes.php');
include (trailingslashit(get_template_directory()).'includes/custom-functions.php');
include (trailingslashit(get_template_directory()).'includes/custom-cf7-tags.php');

/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );
add_filter( 'ot_show_pages', '__return_false' );
add_filter( 'ot_show_new_layout', '__return_false' );
add_filter( 'ot_override_forced_textarea_simple', '__return_true' );

/**
 * Required: include OptionTree.
 */
require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );
require( trailingslashit( get_template_directory() ) . 'includes/theme-options.php' );
require( trailingslashit( get_template_directory() ) . 'includes/meta-boxes.php' );