<?php get_header(); ?>	
	
	<div id="main">
	
		<?php
		$args = array(
			'post_type' => 'page',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'posts_per_page'=> -1
		);
		$loop = new WP_Query($args);		
		$counter = 1;
		if($loop->have_posts()) : while ( $loop->have_posts() ) : $loop->the_post();		

			$template_file = get_post_meta($post->ID, '_wp_page_template', TRUE);
			$template_name = basename($template_file, '.php');
			
			switch($template_name){
				case 'results':
					get_template_part('template-parts/template', 'results');
					break;
				case 'formula':
					get_template_part('template-parts/template', 'formula');
					break;
				case 'imagine':
					get_template_part('template-parts/template', 'imagine');
					break;
				case 'innovate':
					get_template_part('template-parts/template', 'innovate');
					break;
				case 'learnmore':
					get_template_part('template-parts/template', 'learnmore');
					break;	
			}	
		
		endwhile; endif; wp_reset_postdata();		
		?>
		
	</div><!-- end #main -->
	
<?php get_footer(); ?>