<?php get_header(); ?>
	
	<div id="main">
		<div class="section-results section-page"><div class="section-page-border"></div><div class="section-page-inner">
			<div class="common-content container the-content">
				<h1>404 Not Found</h1>
				<p>We apologize, but the page you were looking for was not found.</p>
			</div>
		</div>
	</div><!-- end #main -->

<?php get_footer();