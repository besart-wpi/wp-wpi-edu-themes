<?php
function more_mobile_shortcode( $atts, $content = null ) {
	return '<span class="more-mobile"><span class="more-mobile-content">' . $content . '</span><span class="more-mobile-button">Read More</span></span>';
}
add_shortcode( 'more-mobile', 'more_mobile_shortcode' );