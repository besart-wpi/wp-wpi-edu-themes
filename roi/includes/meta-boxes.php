<?php
/**
 * Initialize the custom Meta Boxes. 
 */
add_action( 'admin_init', 'custom_meta_boxes' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function custom_meta_boxes() {
	
	$contact_form7_choices =  array(
		array(
			'value'       => '',
			'label'       => __( '-- Choose One --', 'asop-backend' ),
		)
	);
	
	$cf7_forms = get_posts(array('post_type' => 'wpcf7_contact_form', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1));
	if(!empty($cf7_forms)){
		foreach($cf7_forms as $form){
			$contact_form7_choices[] = array(
				'value'       => $form->ID,
				'label'       => $form->post_title,
			);
		}		
	}
  
  /**
   * Create a custom meta boxes array that we pass to 
   * the OptionTree Meta Box API Class.
   */  
   
	
	$hero_image_metabox = array(
		'id'          => 'hero_image_metabox',
		'title'       => __( 'Hero Image', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(			
			array(
				'id'		=> '_hero_image_desktop',
				'label'		=> __( 'Hero Image (Desktop)', 'roi-backed' ),
				'type'		=> 'upload',				
				'class'		=> 'ot-upload-attachment-id',				
			),
			array(
				'id'		=> '_hero_image_mobile',
				'label'		=> __( 'Hero Image (Mobile)', 'roi-backed' ),
				'type'		=> 'upload',				
				'class'		=> 'ot-upload-attachment-id',				
			),
		)
	);
	$statistic_boxes_metabox = array(
		'id'          => 'statistic_boxes_metabox',
		'title'       => __( 'Statistic Boxes', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> 'sb_tab_1',
				'label'		=> __( 'Box 1 (rectangle, vertical narrow)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_1_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_1_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_1_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_1_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_2',
				'label'		=> __( 'Box 2 (square, large)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_2_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_2_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_2_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_2_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_3',
				'label'		=> __( 'Box 3 (rectangle, horizontal narrow)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_3_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_3_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_3_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_3_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_4',
				'label'		=> __( 'Box 4 (rectangle, vertical large)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_4_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_4_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_4_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_4_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_5',
				'label'		=> __( 'Box 5 (rectangle, vertical small)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_5_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_5_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_5_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_5_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_6',
				'label'		=> __( 'Box 6 (rectangle, horizontal narrow)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_6_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_6_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_6_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_6_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_7',
				'label'		=> __( 'Box 7 (rectangle, vertical small)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_7_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_7_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_7_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_7_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_8',
				'label'		=> __( 'Box 8 (rectangle, vertical large)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_8_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_8_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_8_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_8_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_9',
				'label'		=> __( 'Box 9 (square, small)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_9_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_9_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_9_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_9_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_10',
				'label'		=> __( 'Box 10 (square, small)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_10_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_10_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_10_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_10_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),	
		)
	);
	
	$quotes_slider_metabox = array(
		'id'          => 'quotes_slider_metabox',
		'title'       => __( 'Quotes Slider', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> '_quotes',
				'label'		=> __( 'Quotes', 'roi-backed' ),
				'type'		=> 'list-item',
				'settings'	=> array(
					array(
						'id'		=> 'text',
						'label'		=> __( 'Quote Text', 'roi-backed' ),
						'type'		=> 'textarea-simple'
					),
					array(
						'id'		=> 'author',
						'label'		=> __( 'Quote Author', 'roi-backed' ),
						'type'		=> 'text'
					),			
				)
			),
		)
	);
	
	$featured_video_metabox = array(
		'id'          => 'featured_video_metabox',
		'title'       => __( 'Featured Video', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> '_video_youtube_id',
				'label'		=> __( 'YouTube Video ID', 'roi-backed' ),
				'type'		=> 'text',				
			),
			array(
				'id'		=> '_video_caption',
				'label'		=> __( 'Caption', 'roi-backed' ),
				'type'		=> 'textarea-simple',				
			),
			array(
				'id'		=> '_video_image',
				'label'		=> __( 'Still Image Override', 'roi-backed' ),
				'type'		=> 'upload',				
				'class'		=> 'ot-upload-attachment-id',				
			),
		)
	);
	
	$graduate_workplaces_metabox = array(
		'id'          => 'graduate_workplaces_metabox',
		'title'       => __( 'Graduate Workplaces', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> '_workplaces_heading',
				'label'		=> __( 'Heading', 'roi-backed' ),
				'type'		=> 'text',				
			),
			array(
				'id'		=> '_workplaces',
				'label'		=> __( 'Workplaces', 'roi-backed' ),
				'type'		=> 'list-item',
				// 'settings'	=> array(
					// array(
						// 'id'		=> 'title',
						// 'label'		=> __( 'Workplace', 'roi-backed' ),
						// 'type'		=> 'text'
					// ),
				// )
			),
		)
	);
	
	$equations_metabox = array(
		'id'          => 'equations_metabox',
		'title'       => __( 'Equations', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> '_theory_facts_tab',
				'label'		=> __( 'Theory Facts', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_theory_facts',
					'label'		=> __( 'Theory Facts', 'roi-backed' ),
					'type'		=> 'list-item',
					'settings'	=> array(
						array(
							'id'		=> 'text',
							'label'		=> __( 'Equation Text', 'roi-backed' ),
							'type'		=> 'textarea-simple'
						),							
					)
				),
			
			array(
				'id'		=> '_practice_facts_tab',
				'label'		=> __( 'Practice Facts', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_practice_facts',
					'label'		=> __( 'Practice Facts', 'roi-backed' ),
					'type'		=> 'list-item',
					'settings'	=> array(
						array(
							'id'		=> 'text',
							'label'		=> __( 'Equation Text', 'roi-backed' ),
							'type'		=> 'textarea-simple'
						),							
					)
				),
			
			array(
				'id'		=> '_global_facts_tab',
				'label'		=> __( 'Global Facts', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_global_facts',
					'label'		=> __( 'Global Facts', 'roi-backed' ),
					'type'		=> 'list-item',
					'settings'	=> array(
						array(
							'id'		=> 'text',
							'label'		=> __( 'Equation Text', 'roi-backed' ),
							'type'		=> 'textarea-simple'
						),							
					)
				),
			
			array(
				'id'		=> '_impact_facts_tab',
				'label'		=> __( 'Impact Facts', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_impact_facts',
					'label'		=> __( 'Impact Facts', 'roi-backed' ),
					'type'		=> 'list-item',
					'settings'	=> array(
						array(
							'id'		=> 'text',
							'label'		=> __( 'Equation Text', 'roi-backed' ),
							'type'		=> 'textarea-simple'
						),							
					)
				),
		)
	);
	
	$icon_text_boxes_metabox = array(
		'id'          => 'icon_text_boxes_metabox',
		'title'       => __( 'Icon/Text Boxes', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(			
			array(
				'id'		=> '_icon_text_heading',
				'label'		=> __( 'Heading', 'roi-backed' ),
				'type'		=> 'text',					
			),
			array(
				'id'		=> 'box_1_tab',
				'label'		=> __( 'Box 1', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_itb_1_icon',
					'label'		=> __( 'Icon', 'roi-backed' ),
					'type'		=> 'upload',				
					'class'		=> 'ot-upload-attachment-id',				
				),
				array(
					'id'		=> '_itb_1_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea-simple',					
					'rows'		=> '3',					
				),
			array(
				'id'		=> 'box_2_tab',
				'label'		=> __( 'Box 2', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_itb_2_icon',
					'label'		=> __( 'Icon', 'roi-backed' ),
					'type'		=> 'upload',				
					'class'		=> 'ot-upload-attachment-id',				
				),
				array(
					'id'		=> '_itb_2_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea-simple',					
					'rows'		=> '3',					
				),
			array(
				'id'		=> 'box_3_tab',
				'label'		=> __( 'Box 3', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_itb_3_icon',
					'label'		=> __( 'Icon', 'roi-backed' ),
					'type'		=> 'upload',				
					'class'		=> 'ot-upload-attachment-id',				
				),
				array(
					'id'		=> '_itb_3_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea-simple',					
					'rows'		=> '3',			
				),
			array(
				'id'		=> 'box_4_tab',
				'label'		=> __( 'Box 4', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_itb_4_icon',
					'label'		=> __( 'Icon', 'roi-backed' ),
					'type'		=> 'upload',				
					'class'		=> 'ot-upload-attachment-id',				
				),
				array(
					'id'		=> '_itb_4_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea-simple',					
					'rows'		=> '3',			
				),
			array(
				'id'		=> 'box_5_tab',
				'label'		=> __( 'Box 5', 'roi-backed' ),
				'type'		=> 'tab'				
			),
				array(
					'id'		=> '_itb_5_icon',
					'label'		=> __( 'Icon', 'roi-backed' ),
					'type'		=> 'upload',				
					'class'		=> 'ot-upload-attachment-id',				
				),
				array(
					'id'		=> '_itb_5_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea-simple',					
					'rows'		=> '3',			
				),		
		)
	);
	
	$quotes_grid_metabox = array(
		'id'          => 'quotes_grid_metabox',
		'title'       => __( 'Quotes Grid', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> '_quotes_grid_heading',
				'label'		=> __( 'Heading', 'roi-backed' ),
				'type'		=> 'text',				
			),
			array(
				'id'		=> '_quotes',
				'label'		=> __( 'Quotes', 'roi-backed' ),
				'type'		=> 'list-item',
				'settings'	=> array(
					array(
						'id'		=> 'photo',
						'label'		=> __( 'Photo', 'roi-backed' ),
						'type'		=> 'upload',
						'class'		=> 'ot-upload-attachment-id'
					),
					array(
						'id'		=> 'quote',
						'label'		=> __( 'Quote', 'roi-backed' ),
						'type'		=> 'textarea-simple',
						'rows'		=> '3',
					),
					array(
						'id'		=> 'class',
						'label'		=> __( 'Class', 'culinart-backed' ),
						'type'		=> 'select',
						'choices'	=> array(
							array(
								'value'       => 'Freshman',
								'label'       => 'Freshman',
							),
							array(
								'value'       => 'Sophomore',
								'label'       => 'Sophomore',
							),
							array(
								'value'       => 'Junior',
								'label'       => 'Junior',
							),
							array(
								'value'       => 'Senior',
								'label'       => 'Senior',
							)
						)
					),						
					array(
						'id'		=> 'major',
						'label'		=> __( 'Major', 'roi-backed' ),
						'type'		=> 'text'
					),
				)
			),
		)
	);
	
	$form_details_metabox = array(
		'id'          => 'form_details_metabox',
		'title'       => __( 'Form Details', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> '_form_id',
				'label'		=> __( 'Contact Form 7 form', 'culinart-backed' ),
				'type'		=> 'select',
				'choices'	=> $contact_form7_choices
			),		
			array(
				'id'		=> '_form_caption',
				'label'		=> __( 'Caption', 'roi-backed' ),
				'type'		=> 'textarea-simple',				
				'rows'		=> '3',				
			),
			array(
				'id'		=> '_form_heading',
				'label'		=> __( 'Heading', 'roi-backed' ),
				'type'		=> 'text',				
			),			
		)
	);
	
	
	$statistic_boxes_learnmore_metabox = array(
		'id'          => 'statistic_boxes_learnmore_metabox',
		'title'       => __( 'Statistic Boxes', 'roi-backed' ),
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'id'		=> '_statistic_heading',
				'label'		=> __( 'Heading', 'roi-backed' ),
				'type'		=> 'text',
			),
			array(
				'id'		=> 'sb_tab_1',
				'label'		=> __( 'Box 1 (rectangle, vertical narrow)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_1_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_1_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_1_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_1_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_2',
				'label'		=> __( 'Box 2 (square, large)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_2_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_2_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_2_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_2_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_3',
				'label'		=> __( 'Box 3 (rectangle, horizontal narrow)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_3_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_3_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_3_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_3_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_4',
				'label'		=> __( 'Box 4 (rectangle, vertical large)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_4_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_4_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_4_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_4_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_5',
				'label'		=> __( 'Box 5 (rectangle, vertical small)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_5_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_5_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_5_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_5_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_6',
				'label'		=> __( 'Box 6 (rectangle, horizontal narrow)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_6_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_6_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_6_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_6_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_7',
				'label'		=> __( 'Box 7 (rectangle, vertical small)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_7_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_7_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_7_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_7_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_8',
				'label'		=> __( 'Box 8 (rectangle, vertical large)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_8_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_8_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_8_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_8_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_9',
				'label'		=> __( 'Box 9 (square, small)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_9_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_9_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_9_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_9_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),
			array(
				'id'		=> 'sb_tab_10',
				'label'		=> __( 'Box 10 (square, small)', 'roi-backed' ),
				'type'		=> 'tab',
			),
				array(
					'id'		=> '_box_10_heading',
					'label'		=> __( 'Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_10_sub_heading',
					'label'		=> __( 'Sub Heading', 'roi-backed' ),
					'type'		=> 'text',
				),
				array(
					'id'		=> '_box_10_text',
					'label'		=> __( 'Text', 'roi-backed' ),
					'type'		=> 'textarea',
				),
				array(
					'id'		=> '_box_10_url',
					'label'		=> __( 'Link URL', 'roi-backed' ),
					'type'		=> 'text',
				),	
		)
	);
	
	
  
  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
	if ( function_exists( 'ot_register_meta_box' ) ):		
		
		$template_file = '';
		$post_id = '';
		$post_type = '';
		
		/* -- Get Template File Name & Post ID -- */
		if(isset($_GET['post']) || isset($_POST['post_ID'])):
		
			if(isset($_POST['post_ID'])){
				$post_id = $_POST['post_ID'];
			}
			
			if(isset($_GET['post'])){
				$post_id = $_GET['post'];
			}
			
			if($post_id) {
				$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
				$post_type = get_post_type($post_id);
			}
		endif;	
		
		
	/* ================================
	Set Metabox
	=================================== */		
		
		if($template_file == 'page-templates/results.php'){
			ot_register_meta_box( $hero_image_metabox );
			ot_register_meta_box( $statistic_boxes_metabox );
			ot_register_meta_box( $quotes_slider_metabox );
			ot_register_meta_box( $featured_video_metabox );
			ot_register_meta_box( $graduate_workplaces_metabox );
		} elseif($template_file == 'page-templates/formula.php'){
			ot_register_meta_box( $hero_image_metabox );
			ot_register_meta_box( $equations_metabox );	
			ot_register_meta_box( $quotes_slider_metabox );
			ot_register_meta_box( $featured_video_metabox );					
		} elseif($template_file == 'page-templates/imagine.php'){
			ot_register_meta_box( $hero_image_metabox );			
			ot_register_meta_box( $quotes_slider_metabox );
			ot_register_meta_box( $featured_video_metabox );					
			ot_register_meta_box( $icon_text_boxes_metabox );					
		} elseif($template_file == 'page-templates/innovate.php'){
			ot_register_meta_box( $hero_image_metabox );							
			ot_register_meta_box( $quotes_grid_metabox );
			ot_register_meta_box( $featured_video_metabox );		
		} elseif($template_file == 'page-templates/learnmore.php'){
			ot_register_meta_box( $hero_image_metabox );			
			ot_register_meta_box( $form_details_metabox );					
			ot_register_meta_box( $statistic_boxes_learnmore_metabox );					
		}
		
		
		
	endif; // End if ( function_exists( 'ot_register_meta_box' ) )

} // End of function custom_meta_boxes()



function filter_list_item_title_label( $label, $id ) {
  if ( $id == '_workplaces' ) {
    $label = __( 'Workplace', 'theme-domain' );
  }
  return $label;
}
add_filter( 'ot_list_item_title_label', 'filter_list_item_title_label', 10, 2 );

add_filter( 'ot_list_item_settings', 'filter_ot_list_item_settings', 10, 2 );
function filter_ot_list_item_settings( $settings, $id ) {

  // Only remove settings from a specific option
  if ( $id == '_workplaces' )
    return array();

  return $settings;

}