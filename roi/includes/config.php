<?php
// =================================================
//	:: Admin Favicon ::
// -------------------------------------------------
	function add_favicon() {
		$favicon_url = get_stylesheet_directory_uri() . '/admin-favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
	}
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');
	
// =================================================
//	:: Enqueue Scripts ::
// -------------------------------------------------
	function theme_enqueue_scripts(){
		/* -- Enqueue CSS File -- */
		//wp_enqueue_style( 'archer', '//cloud.typography.com/668448/657068/css/fonts.css' );
		wp_enqueue_style( 'archer', get_template_directory_uri().'/archer.css' );
		
		wp_enqueue_style( 'theme-style', get_stylesheet_uri());			
		
		/* -- Enqueue JS File -- */
		wp_enqueue_script('jquery');		
		
		wp_enqueue_script( 'picturefill', get_template_directory_uri() . '/js/picturefill.js');
		wp_enqueue_script( 'jquery-cycle-2', get_template_directory_uri() . '/js/jquery.cycle2.min.js', array( 'jquery' ));
		wp_enqueue_script( 'jquery-cycle-2-swipe', get_template_directory_uri() . '/js/jquery.cycle2.swipe.min.js', array( 'jquery' ));
		wp_enqueue_script( 'jquery-cycle-2-carousel', get_template_directory_uri() . '/js/jquery.cycle2.carousel.min.js', array( 'jquery' ));
		wp_enqueue_script( 'jquery-floatlabel', get_template_directory_uri() . '/js/floatlabels.js', array( 'jquery' ));
		wp_enqueue_script( 'jquery-icheck', get_template_directory_uri() . '/js/icheck.min.js', array( 'jquery' ));	
		wp_enqueue_script( 'jquery-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ));
		wp_enqueue_script( 'jquery-one-page-nav', get_template_directory_uri() . '/js/jquery.nav.js', array( 'jquery' ));		
		wp_enqueue_script( 'jquery-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js', array( 'jquery' ));			
		wp_enqueue_script( 'jquery-fancybox-media', get_template_directory_uri() . '/js/jquery.fancybox-media.js', array( 'jquery' ));			
			
		wp_enqueue_script( 'theme-main', get_template_directory_uri() . '/js/main.js', array( 'jquery' ));
	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );
	
	
// =================================================
//	:: Theme Configuration ::
// -------------------------------------------------
	function theme_configuration(){
		
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		
		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );		
		
		/* -------------------------------------------------
		*	Post Thumbnail Config
		* ------------------------------------------------- */
			add_theme_support( 'post-thumbnails');
			set_post_thumbnail_size( 200, 150, true );		
			add_image_size( 'bg-size', 1280, 99999, false );		
			add_image_size( 'bg-size-2x', 2560, 99999, false );
			add_image_size( 'video-size', 1280, 438, true );		
			add_image_size( 'video-size-2x', 2560, 876, true );	
			add_image_size( 'icon-text-size', 82, 82, true );	
			add_image_size( 'icon-text-size-2x', 255, 255, true );
			add_image_size( 'photo-size', 76, 76, true );	
			add_image_size( 'photo-size-2x', 152, 152, true );	
		
	}	
	add_action( 'after_setup_theme', 'theme_configuration' );
	

// =================================================
//	:: Excerpt ::
// -------------------------------------------------
function custom_excerpt_length( $length ) {
	return 55;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );	
function new_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


// =================================================
//	:: Add CSS For Admin Page ::
// -------------------------------------------------
add_action('admin_head', 'theme_admin_css');

function theme_admin_css() {
  echo '<link rel="stylesheet" href="'.get_bloginfo('template_url').'/admin-css/admin-stylesheet.css" type="text/css" media="all" />';
}

// =================================================
//	:: Add Style Selection for Editor ::
// -------------------------------------------------
// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');


// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Heading Highlight',  
			'inline' => 'span',  
			'classes' => 'heading-highlight',
			'wrapper' => false,
			
		),  		
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  


// =================================================
//	:: IE Fix ::
// -------------------------------------------------	
	add_filter( 'wp_head' , 'add_shiv_for_ie' );
	function add_shiv_for_ie() { ?>
		<!--[if lt IE 9]>			
			<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>			
			<script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
			<style type="text/css" media="screen">			
			.page-content-title, .quote-photo, .quote-photo img, #find-out-more
			{
					behavior: url("<?php echo get_stylesheet_directory_uri() . '/js/PIE.htc'; ?>");
					position: relative;
					zoom: 1;
			}
			#find-out-more {
				position: fixed;				
			}
			</style>
		<![endif]-->		
	<?php
	}