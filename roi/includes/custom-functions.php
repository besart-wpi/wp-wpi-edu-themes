<?php
// =================================================
//	:: Get Image By Attachment ID ::
// -------------------------------------------------
function theme_get_image($attachment_id, $image_size = NULL, $is_img_tag = true){
	if($attachment_id) {
		if($image_size == NULL) {
			$image_size = 'post-thumbnail';
		}
		$image = wp_get_attachment_image_src($attachment_id, $image_size);
		if(!empty($image)){
			$image = $image[0];
			if($is_img_tag) {
				return '<img src="'.$image.'" alt="" />';
			} else {
				return $image;
			}
		} else {
			return '';
		}
	} else {
		return '';
	}
}

// =================================================
//	:: Get scrset ::
// -------------------------------------------------
function theme_get_scrset($attachment_id, $image_size){
	if($attachment_id){
		$output = theme_get_image($attachment_id, $image_size, false);
		$output .= ' 1x, '.theme_get_image($attachment_id, $image_size.'-2x', false).' 2x';
		return $output;
	}
}

// =================================================
//	:: Get Top Parent Page ::
// -------------------------------------------------
function theme_get_parent_page($page_id) {	
	if($page_id) {
		$page_parent = get_post($page_id);
		if ($page_parent->post_parent)	{
			$ancestors=$page_parent->ancestors;
			$root=count($ancestors)-1;
			$parent = $ancestors[$root];
		} else {
			$parent = $page_parent->ID;
		}
		return $parent;
	}
}



// =================================================
//	:: Get Image URL ::
// -------------------------------------------------
function theme_get_ot_image_url($image, $image_size){
	if(is_numeric($image)){
		$image = theme_get_image($image, $image_size, false);
	}
	return $image;
}