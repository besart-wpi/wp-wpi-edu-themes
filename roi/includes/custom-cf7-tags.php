<?php
add_action( 'wpcf7_init', 'custom_add_shortcode_graduation' );
 
function custom_add_shortcode_graduation() {
	wpcf7_add_shortcode( array( 'graduation', 'graduation*' ), 'graduation_shortcode_handler', true );
}

function graduation_shortcode_handler( $tag ) {
	
	$tag = new WPCF7_Shortcode( $tag );

	if ( empty( $tag->name ) )
		return '';

	$validation_error = wpcf7_get_validation_error( $tag->name );

	$class = wpcf7_form_controls_class( $tag->type );

	if ( $validation_error )
		$class .= ' wpcf7-not-valid';

	$label_first = $tag->has_option( 'label_first' );
	$use_label_element = $tag->has_option( 'use_label_element' );
	$exclusive = $tag->has_option( 'exclusive' );
	$free_text = $tag->has_option( 'free_text' );
	$multiple = false;

	// if ( 'checkbox' == $tag->basetype )
		// $multiple = ! $exclusive;
	// else // radio
		// $exclusive = false;

	// if ( $exclusive )
		// $class .= ' wpcf7-exclusive-checkbox';

	$atts = array();

	$atts['class'] = $tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();

	$tabindex = $tag->get_option( 'tabindex', 'int', true );

	if ( false !== $tabindex )
		$tabindex = absint( $tabindex );

	$html = '';
	$count = 0;
	
	$start_date = date('Y');
	
	if (new DateTime() > new DateTime($start_date."-04-30 00:00:00")) {
		// current time is greater than April 30
		$start_date++;
	}
	
	$values = array();
	for($i = 1; $i <= 4; $i++){
		$values[] = $start_date;
		$start_date++;
	}
	$values[] = 'Other';

	if ( $data = (array) $tag->get_data_option() ) {
		if ( $free_text ) {
			$values = array_merge(
				array_slice( $values, 0, -1 ),
				array_values( $data ),
				array_slice( $values, -1 ) );
			$labels = array_merge(
				array_slice( $labels, 0, -1 ),
				array_values( $data ),
				array_slice( $labels, -1 ) );
		} else {
			$values = array_merge( $values, array_values( $data ) );
			$labels = array_merge( $labels, array_values( $data ) );
		}
	}

	$defaults = array();

	$default_choice = $tag->get_default_option( null, 'multiple=1' );

	foreach ( $default_choice as $value ) {
		$key = array_search( $value, $values, true );

		if ( false !== $key ) {
			$defaults[] = (int) $key + 1;
		}
	}

	if ( $matches = $tag->get_first_match_option( '/^default:([0-9_]+)$/' ) ) {
		$defaults = array_merge( $defaults, explode( '_', $matches[1] ) );
	}

	$defaults = array_unique( $defaults );

	$hangover = wpcf7_get_hangover( $tag->name, $multiple ? array() : '' );

	foreach ( $values as $key => $value ) {
		$class = 'wpcf7-list-item';

		$checked = false;

		if ( $hangover ) {
			if ( $multiple ) {
				$checked = in_array( esc_sql( $value ), (array) $hangover );
			} else {
				$checked = ( $hangover == esc_sql( $value ) );
			}
		} else {
			$checked = in_array( $key + 1, (array) $defaults );
		}

		if ( isset( $labels[$key] ) )
			$label = $labels[$key];
		else
			$label = $value;

		$item_atts = array(
			'type' => 'radio',
			'name' => $tag->name . ( $multiple ? '[]' : '' ),
			'value' => $value,
			'checked' => $checked ? 'checked' : '',
			'tabindex' => $tabindex ? $tabindex : '' );

		$item_atts = wpcf7_format_atts( $item_atts );
		
		$item = sprintf(
		'<input %2$s /><label>%1$s</label>',
		esc_html( $label ), $item_atts );

		// if ( $label_first ) { // put label first, input last
			// $item = sprintf(
				// '<span class="wpcf7-list-item-label">%1$s</span>&nbsp;<input %2$s />',
				// esc_html( $label ), $item_atts );
		// } else {
			// $item = sprintf(
				// '<input %2$s />&nbsp;<span class="wpcf7-list-item-label">%1$s</span>',
				// esc_html( $label ), $item_atts );
		// }

		// //if ( $use_label_element )
		// $item = '<label>' . $item . '</label>';

		if ( false !== $tabindex )
			$tabindex += 1;

		$count += 1;

		if ( 1 == $count ) {
			$class .= ' first';
		}

		if ( count( $values ) == $count ) { // last round
			$class .= ' last';			
		}

		//$item = '<span class="' . esc_attr( $class ) . '">' . $item . '</span>';
		$html .= $item;
	}

	$atts = wpcf7_format_atts( $atts );

	$html = sprintf(
		'<span class="wpcf7-form-control-wrap %1$s"><span %2$s>%3$s</span>%4$s</span>',
		sanitize_html_class( $tag->name ), $atts, $html, $validation_error );

	return $html;
	
}


add_filter( 'wpcf7_validate_graduation', 'graduation_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_graduation*', 'graduation_validation_filter', 10, 2 );

function graduation_validation_filter( $result, $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	$type = $tag->type;
	$name = $tag->name;

	$value = isset( $_POST[$name] ) ? (array) $_POST[$name] : array();

	if ( $tag->is_required() && empty( $value ) ) {
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	}

	return $result;
}