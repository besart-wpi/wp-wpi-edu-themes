jQuery(function($){
	
	$('#nav').onePageNav({
		currentClass: 'current-menu-item',
		changeHash: true,
		scrollSpeed: 800,
		begin: function() {
			if( $('#show-menu').is(':visible') ){
				$('#nav').slideUp(250);
			}
		},
	});	
	
	$('.lightbox-media').fancybox({		
		helpers : {
			media : {}
		}
	});
	
	$(".video-section").fitVids();
	
	$('.equation-button').click(function(){
		$('.equation-button').not(this).siblings('.equation-list').hide();
		$('.equation-button').not(this).find('h3').removeClass('active-eq');
		$('.equation-button').not(this).removeClass('active-eq-button');
		
		$(this).siblings('.equation-list').stop().slideToggle(300);
		$(this).toggleClass('active-eq-button');
		$(this).find('h3').toggleClass('active-eq');
		return false;
	});
	
	$('a.find-out-more-close').click(function(){
		$('#find-out-more').fadeOut(200);
		return false;
	});
	
	$('input.floatlabel, textarea.floatlabel').floatlabel();
	
	$('.form-radio input').each(function(){
	var self = $(this),
	  label = self.next(),
	  label_text = label.text();

	label.remove();
		self.iCheck({
		  insert: label_text
		});
	});
	
	$('#show-menu').click(function(){
		$('#nav').slideToggle(250);
		return false;		
	});
	
	$('.more-mobile-button').click(function(){
		$(this).siblings('.more-mobile-content').fadeToggle(200, function(){
			if ($(this).is(':visible'))
			$(this).css('display','inline');
		});
		if($(this).text() == 'Read More'){
			$(this).text('Read Less');
		} else {
			$(this).text('Read More');			
		}
		return false;
	});
	
	function responsiveCycle(){	
		
		// .mobile-cycle-flex-visible to force single visible under 420px
		if($(window).width() < 420){
			$('.mobile-cycle-flex-visible').each(function(){
				$(this).cycle({
					carouselVisible: 1
				});
			});
		} else {
			if($(window).width() < 1024){
				$('.mobile-cycle-flex-visible').each(function(){
					$(this).cycle();
				});
			} else {
				$('.mobile-cycle-flex-visible').each(function(){
					$(this).cycle('destroy');
				});
			}
		}
			
		if($(window).width() < 1024){
			$('.statistic-wrap, .mobile-cycle').each(function(){
				$(this).cycle();
			});
		} else {
			$('.statistic-wrap, .mobile-cycle').each(function(){
				$(this).cycle('destroy');
			});
		}		
		
		
	}
	responsiveCycle();	
	$(window).resize(responsiveCycle);
	
});