<?php 
$posts = get_posts(array(
	'post_type'			=> 'testimonials',
	'posts_per_page'	=> 0,
	'orderby' => 'menu_order',
	'order' => 'ASC'
));
if(count($posts)):
	foreach ($posts as $key => $post): ?>
		<blockquote>
			<?= apply_filters('the_content', $post->post_content) ?>
			<p class="align-right"><strong><?= $post->post_title ?> <small><small><?= Oz::getField('title', 'properties') ?></small></small></strong></p>
		</blockquote>
	<?php endforeach;
else: ?>
	<h2>Posts are coming soon!</h2>
<?php endif; ?>