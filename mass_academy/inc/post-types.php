<?php 
// =================================================
// :: Custom Post Types (CPT) ::
// -------------------------------------------------

add_action('admin_menu', 'defineCPTs', 1);
function defineCPTs(){
	global $oz;
	// =================================================
	// :: Home Page ::
	// -------------------------------------------------
	$oz->metabox(array(
		'id'		=> 'the-content',
		'only-ids'	=> get_option('page_on_front'),
		'fields'	=> array(
			'content' 	=> array(
				'label'	=> '',
				'stack'	=> true,
				'type'	=> 'editor'
			)
		)
	));
	$oz->metabox(array(
		'id'			=> 'right-side',
		'label'			=> 'Right-handed Content',
		'only-ids'		=> get_option('page_on_front'),
		'fields'		=> array(
			'sidebar'	=> array(
				'label'	=> '',
				'stack'	=> true,
				'type'	=> 'editor',
			)
		)
	));

	//Get the POST ID	and template file
	$postID = 0;
	if(isset($_GET['post']))
		$postID = $_GET['post'];
	elseif(isset($_POST['post_ID']))
		$postID = $_POST['post_ID'];
	
	$template_file = get_post_meta($postID,'_wp_page_template',TRUE);

	// =================================================
	// :: Pages ::
	// -------------------------------------------------
	$oz->metabox(array(
		'id'		=> 'sidebar',
		'post-types'	=> 'page',
		'templates'	=> array('default', 'templates/news.php', 'page-landing.php'),
		'exclude-ids'	=> get_option('page_on_front'),
		'fields'	=> array(
			'content'	=> array(
				'type'	=> 'editor',
				'stack'	=> true,
				'label'	=> ''
			)
		)
	));

	/**
	 * Events
	 */
	if($template_file == 'templates/news.php'){
		$oz->metabox(array(
			'id'		=> 'Events',
			'post-types'=> 'page',
			'templates'	=> 'templates/news.php',
			'fields'	=> array(
				'title',
				'event'	=> array(
					'type'	=> 'group',
					'label'	=> 'Event: %#title',
					'fields'=> array(
						'title',
						'title-color',
						'content'	=> 'editor'
					)
				)
			)
		));
	}


	// =================================================
	// :: Testimonials ::
	// -------------------------------------------------
	remove_action( 'admin_init', 'wpi_testimonials_custom_meta_boxes' );

	$oz->metabox(array(
		'id'			=> 'properties',
		'post-types'	=> 'testimonials',
		'fields'		=> array('title')
	));


	// =================================================
	// :: Portfolios ::
	// -------------------------------------------------
	$oz->metabox(array(
		'id'		=> 'info',
		'post-types'=> 'project_portfolio',
		'fields'	=> array(
			'overview'	=> 'editor',
			'client' => 'label=Researchers',
      'additional' => array(
        'label' => 'Additional Information',
        'type' => 'editor'
        ),
/*			'services'	=> array(
				'type'	=> 'text',
				'repeat'=> true
			)
*/		)
	));
}