<?php //#########################################################
// Testimonials Widget
//#########################################################

// =================================================
// :: The Widget ::
// -------------------------------------------------
function register_widget_testimonials() {
    register_widget( 'Widget_Testimonials' );
}
add_action( 'widgets_init', 'register_widget_testimonials' );
class Widget_Testimonials extends WP_Widget{
	// =================================================
	// :: Constructor ::
	// -------------------------------------------------
	function __construct(){
		parent::__construct(
			'testimonials_widget',
			__('Testimonials'),
			array('description' => 'Displays a random testimonial')
		);
	}

	// =================================================
	// :: Frontend ::
	// -------------------------------------------------
	function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		echo displayTestimonial(0, __('Sorry, there are no testimonials yet'));

		echo $args['after_widget'];
	}

	// =================================================
	// :: Widget screen ::
	// -------------------------------------------------
	function form($instance){
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'text_domain' );
		} ?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>
		<?php 		
	}

	// =================================================
	// :: Saving ::
	// -------------------------------------------------
	function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}	
}

//#########################################################
// Create widgets
//#########################################################
/**
 * Built sepearately in order to reuse within content
 *
 * @param  [STR] $isSorry [The "Sorry..." text, only shown if passed]
 * @return [STR] The content
 */
function displayTestimonial($quoteID = 0, $isSorry = ''){
	if(!$quoteID)
		$quote = get_posts(array(
			'post_type' => 'testimonials',
			'orderby'	=> 'rand',
			'numberposts'=> 1
		));
	else
		$quote = array(get_post($quoteID));

	ob_start();
  if(count($quote)): $quote = $quote[0];

  $has_image = has_post_thumbnail() ? ' no-image' : ' has-image';
  ?>
  <blockquote class="testimonial<?= $has_image; ?>">

    <?php if ($has_image === ' has-image'): ?>
      <div class="testimonial-image">
        <?= get_the_post_thumbnail($quote->ID, 'testimonial-small', array('class'=>'testimonial-photo')) ?>
      </div>
    <?php endif; ?>
      <div class="testimonial-info">

        <?php if ($has_image == ' has-image'): ?>

          <?php if (!empty($quote->post_title)): ?>
            <div class="testimonial-name"><?= $quote->post_title ?></div>
          <?php endif; ?>

          <?php if($testimonial_title = Oz::getField('title', 'properties', null, $quote->ID)): ?>
            <div class="testimonial-title"><?= $testimonial_title ?></div>
          <?php endif; ?>

          <?php if($testimonial_company = Oz::getField('company', 'properties', null, $quote->ID)): ?>
            <div class="testimonial-company"><?= $testimonial_company ?></div>
          <?php endif; ?>

          <?php if (!empty($quote->post_content)): ?>
            <div class="testimonial-quote<?= $has_image; ?>"><?= apply_filters('the_content', $quote->post_content) ?></div>
          <?php endif; ?>

        <?php else: ?>

          <?php if (!empty($quote->post_content)): ?>
            <div class="testimonial-quote<?= $has_image; ?>"><?= apply_filters('the_content', $quote->post_content) ?></div>
          <?php endif; ?>

          <?php if (!empty($quote->post_title)): ?>
            <div class="testimonial-name"><?= $quote->post_title ?></div>
          <?php endif; ?>

          <?php if($testimonial_title = Oz::getField('title', 'properties', null, $quote->ID)): ?>
            <div class="testimonial-title"><?= $testimonial_title ?></div>

          <?php if($testimonial_company = Oz::getField('company', 'properties', null, $quote->ID)): ?>
            <div class="testimonial-company"><?= $testimonial_company ?></div>
          <?php endif; ?>
          <?php endif; ?>

        <?php endif; ?>
      </div>

  </blockquote>
		<?php elseif($isSorry):
			echo $isSorry;
		endif;
	return ob_get_clean();
}