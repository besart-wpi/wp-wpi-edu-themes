<?php

/*
Template Name: Search Page
*/

get_header();

// =================================================
// :: Gets the .hero background ::
// -------------------------------------------------
if($featuredImage = get_post_thumbnail_id()){
	$featuredImage = get_post($featuredImage)->guid;
}

?>
<?php if($featuredImage): ?>
  <div class="hero-wrapper">
    <div class="hero" style="background-image: url(<?php echo $featuredImage; ?>"></div>
  </div>
<?php endif; ?>
	<section id="the-content" class="search-page-results" role="contentinfo" aria-label="<?= wp_title(''); ?>">
    <h1 class="page-title"><?php the_title() ?></h1>
		<div id="content">
      <gcse:searchresults-only></gcse:searchresults-only>
		</div>
	</section>

<?php get_footer(); ?>