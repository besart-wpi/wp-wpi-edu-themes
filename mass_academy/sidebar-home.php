<?php if ( is_active_sidebar('home') ): ?>
	
    <aside id="sidebar_home">
        <?php dynamic_sidebar( 'home' ); ?>
        <div class="clear"></div>	
    </aside>
    <div class="clear"></div>	
	
<?php endif; ?>