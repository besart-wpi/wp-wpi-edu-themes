<?php 
get_header();

// =================================================
// :: Gets the .hero background ::
// -------------------------------------------------
if($featuredImage = get_post_thumbnail_id()){
	$featuredImage = get_post($featuredImage)->guid;
}

?>
<?php if($featuredImage): ?>
  <div class="hero-wrapper">
    <div class="hero" style="background-image: url(<?php echo $featuredImage; ?>"></div>
  </div>
<?php endif; ?>
	<section id="the-content" role="contentinfo" aria-label="<?php the_title(); ?>">
    <h1 class="page-title"><?php the_title(); ?></h1>
		<div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Displays either content as full width or with sidebar
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			$sidebar = getSidebar('page');
			$hasSidebar = is_active_sidebar('global') || $sidebar;
      		$fullWidth = $hasSidebar ? ' c8' : '';

			?>
			<article class="main<?= $fullWidth; ?>">
				<?php get_template_part('loop', 'page') ?>
			</article>
			<?php if($hasSidebar): ?>
				<aside class="main c4">
					<?= apply_filters('the_content', $sidebar) ?>
					<?php dynamic_sidebar('global'); ?>
				</aside>
			<?php endif; ?>
			<div class="clear"></div>
		</div>
	</section>

<?php get_footer(); ?>