// =================================================
// :: Theme stuff ::
// -------------------------------------------------

(function($) {
  var $content, $window, $bodyWrap, $footerWrap, $sidebar, $article;

  $(function() {
    $content = $('#the-content');
    $window = $(window);
    $bodyWrap = $('#body-wrap');
    $footerWrap = $('.footer-wrap').first();
    $article = $('article.main');
    $sidebar = $('aside.main');

    // Get height for header content
    var wpadminbar = getAdminBarHeight();

    // Handle home page slideshow links
    $('.front-page.cycle-slideshow > div.has-link').click(function(e) {
      e.preventDefault();
      var link_url = $(this).attr('data-link-url');
      if (link_url) {
        window.location = link_url;
      }
    });

    // IE8 Widget Odd Class
    $("#sidebar_home .widget:nth-child(odd)").addClass("odd");

    // =================================================
    // :: Reposition form labels before input fields when
    // switching into mobile ::
    // -------------------------------------------------
    var $comment = $('#commentform');
    $window.resize(function() {
      equalizeColumns(true);
      equalizeMembers(true);
      wpadminbar = getAdminBarHeight();

      $('input + label', $comment).each(function() {
        var $this = $(this);
        $this.insertBefore($this.prev());
      });

    }).resize();

    // =================================================
    // :: Responsive iFrames ::
    // -------------------------------------------------
    $content.fitVids();


    // Dropdown for menu and search on tablet down
    var $dropdownButtons = $('.top-bar-icon-mobile');
    var $menu = $('.menu-wrapper');
    var $search = $('.search-wrapper');

    $dropdownButtons.each(function() {
      $(this).click(function() {
        var $classes = $(this).attr('class');
        $(this).siblings().removeClass('active');
        $(this).toggleClass('active');
        if ($classes.indexOf('search') >= 0) {
          $menu.removeClass('active');
          $search.toggleClass('active');
        } else
        if ($classes.indexOf('menu') >= 0) {
          $search.removeClass('active');
          $menu.toggleClass('active');
        }
      });
    });

    $('.page-header').css('top', wpadminbar);

    function toggleChevron(el) {
      var $siblings = el.parent().siblings('.menu-item-has-children');
      var $cousins = el.parents('.menu-items-wrapper').siblings().children();
      $siblings.children().removeClass('active');
      $cousins.children().removeClass('active');
      el.toggleClass('active');
      el.siblings('a').toggleClass('active');
    }

    // Add chevron to menu items that have children on tablet down
    var chevron = $(document.createElement('span'));
    chevron.attr('tabindex', '0');
    chevron.attr('class', 'toggle-sub-menu');
    chevron.on('keypress', function(e) {
      if (e.keyCode === 13) {
        toggleChevron($(this));
      }
    });
    chevron.click(function() {
      // Since the menu items are grouped into two divs, we need siblings and cousins
      toggleChevron($(this));
    });
    $('.menu-item-has-children').prepend(chevron);

    // For medium screen breakpoint split the menu into two containers for layout help
    var topLevelItemsCount = $('#menu-main > .menu-item').length;
    var middle = Math.ceil(topLevelItemsCount / 2);
    var middleChild = $('#menu-main > .menu-item:eq(' + middle + ')');
    var separator = $(document.createElement('span'));
    separator.attr('class', 'vertical-menu-separator');
    middleChild.before(separator);

    $('#menu-main > .menu-item:gt(' + (middle - 1) + ')').wrapAll('<div class="menu-items-wrapper right-half">');
    $('#menu-main > .menu-item:lt(' + (middle) + ')').wrapAll('<div class="menu-items-wrapper left-half">');

    function replaceSearchButton() {
      // Desktop search

      var desktopSearchWrapper = $('.desktop-search-box .gsc-input-box');
      var desktopSearchInput = $('.desktop-search-box .gsc-input-box .gsc-input');
      var desktopSearchButton = $('.desktop-search-box .gsc-search-button-v2');

      $('.desktop-search-box .gsib_b').css('display', 'none');

      desktopSearchButton.addClass('sr-only');

      desktopSearchIcon = $('<span />');
      desktopSearchIcon.addClass('icon-search');
      desktopSearchIcon.attr('id', 'desktop-search-magnifying-glass');
      desktopSearchIcon.attr('tabindex', '0');
      desktopSearchIcon.attr('title', 'Click to search');
      desktopSearchIcon.attr('alt', 'Click to search');
      desktopSearchIcon.click(function(e) {
        desktopSearchButton.click();
      });
      desktopSearchIcon.keypress(function(e) {
        if (e.keyCode === 13) {
          desktopSearchButton.click();
        }
      });

      desktopSearchInput.attr('placeholder', 'Search');
      desktopSearchWrapper.append(desktopSearchIcon);

      // Mobile search
      var mobileSearchInput = $('.mobile-dropdown-content .gsc-input-box .gsc-input');
      mobileSearchInput.attr('placeholder', 'Search');

      var mobileSearchTerm = '';
      var mobileClearButton = $('.mobile-dropdown-content .gsib_b .gsst_a');
      var mobileSearchButton = $('<span>Search</search>');
      var mobileGoogleSearch = $('.mobile-dropdown-content .gsc-search-button');
      var gcseInput = $('.mobile-dropdown-content .gsc-search-button input');
      gcseInput.css('display', 'none');

      mobileSearchButton.addClass('mobile-search-button-text');
      mobileSearchButton.attr('tabindex', '0');
      mobileSearchButton.attr('title', 'Click to search');
      mobileSearchButton.attr('alt', 'Click to search');
      mobileSearchInput.on('keyup', function(e) {
        mobileSearchTerm = this.value;
        if (e.keyCode === 13) {
          mobileGoogleSearch.click();
        }
      });

      mobileSearchButton.click(function(e) {
        mobileGoogleSearch.click();
      });
      mobileSearchButton.keypress(function(e) {
        $('.mobile-dropdown-content .gsc-search-button input').click();
      });

      mobileGoogleSearch.append(mobileSearchButton);
      mobileClearButton.attr('tabindex', '0');

      // 404 page search
      var errorSearchWrapper = $('.error404-page-content .gsc-input-box');
      var errorSearchInput = $('.error404-page-content .gsc-input-box .gsc-input');
      var errorSearchButton = $('.error404-page-content .gsc-search-button-v2');
      var errorClearButton = $('.error404-page-content .gsib_b .gsst_a');

      errorSearchButton.css('display', 'none');

      errorClearButton.attr('tabindex', '0');

      var customErrorSearchButton = $('<span>Search</span>');
      customErrorSearchButton.addClass('button-search');
      customErrorSearchButton.attr('id', 'error-search-magnifying-glass');
      customErrorSearchButton.attr('tabindex', '0');
      customErrorSearchButton.attr('title', 'Click to search');
      customErrorSearchButton.attr('alt', 'Click to search');
      customErrorSearchButton.click(function(e) {
        errorSearchButton.click();
      });
      customErrorSearchButton.keypress(function(e) {
        e.preventDefault();
        if (e.keyCode === 13) {
          errorSearchButton.click();
        }
      });

      errorSearchInput.attr('placeholder', 'Search');
      errorSearchInput.on('keyup', function(e) {
        if (e.keyCode === 13) {
          errorSearchButton.click();
        }
      });

      errorSearchWrapper.after(customErrorSearchButton);
    }

    window.__gcse = {
      callback: replaceSearchButton
    };

    (function() {
      var cx = '000720435508958471272:ht4r4lckytk';
      var gcse = document.createElement('script');
      gcse.type = 'text/javascript';
      gcse.async = true;
      gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(gcse, s);
    })();

  });

  // Get admin bar heigth
  function getAdminBarHeight() {
    return $('#wpadminbar').height() ? $('#wpadminbar').height() : 0;
  }

  // Get menu header
  function getmenuBarHeight() {
    return $('.menu-wrapper').height() ? $('.menu-wrapper').height() : 0;
  }

  // =================================================
  // :: Equalizes sidebar height ::
  // -------------------------------------------------
  // The sidebar contains the border, this will ensure
  // it always fills the content height

  // =================================================
  // :: Equalize columns ::
  // -------------------------------------------------
  function equalizeColumns(windowResized) {
    if (windowResized) {
      $('.equalize-children .col').css('height', 'auto');
    }

    if ($window.width() > '767') {
      $('.equalize-children').equalize('outerHeight');
    }
  }

  // =================================================
  // :: Equalize team members ::
  // -------------------------------------------------
  function wrapMembers() {
    var members = $(".team-members > .team-member");
    var wrapper = 0;
    for (var i = 0; i < members.length; i += 2) {
      wrapper += 1;
      members.slice(i, i + 2).wrapAll("<div class='cf members-wrapper members-wrapper-" + wrapper + "'/>");
    }
  }
  // =================================================
  // :: Equalize team members ::
  // -------------------------------------------------
  function equalizeMembers(windowResized) {
    if (windowResized) {
      $('.team-members .team-member').css('height', 'auto');
    }

    if ($window.width() > '1000') {
      $('.members-wrapper').equalize('outerHeight');
    }
  }

  function responsiveTables() {
    // Responsive Tables
    var $table = $('#content table');
    $table.find('tr').each(function() {
      var row = this;
      var count = 0;
      $(row).find('td').each(function() {
        count++;
        var th = ($table.find('th:nth-of-type(' + count + ')').first().text());
        $(this).attr('data-th', th);
      });
    });
    $(window).resize(function() {
      if ($window.width() > '767') {
        $table.find('td[data-th]').each(function() {
          if ($.trim($(this).text()) === '') {
            $(this).addClass('hide');
          }
        });
      } else {
        $table.find('td[data-th]').removeClass('hide');
      }
    });
  }


  // function hideMagnifyingGlass(selector){
  //   var el = $(selector);
  //   if(el.val().length > 0) {
  //     $('#desktop-search-magnifying-glass').hide();
  //   }
  // }

  // Load typekit fonts
  function loadTypekit() {
    try {
      Typekit.load({
        active: function() {
          equalizeColumns(true);
        },
      });
    } catch (e) {}
  }

  //#########################################################
  // Window Ready
  //#########################################################

  $(window).ready(function() {
    wrapMembers();
    responsiveTables();
    loadTypekit();
    $("td.gsc-search-button").empty().html('<input type="button" value="search" title="search">');

  });

  $(window).load(function() {
    equalizeColumns();
    equalizeMembers();
  });

})(jQuery);