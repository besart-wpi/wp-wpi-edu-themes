'use strict';
 
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var concat = require('gulp-concat');
var cssnano = require('cssnano');
var livereload = require('gulp-livereload');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var postcss = require('gulp-postcss');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');

var scss = 'scss/**/*.scss';
var js = ['js/libs/**/*.js', 'js/_*.js'];

var plugins = [
  autoprefixer({browsers: ['last 2 versions', 'ie > 8']}),
  cssnano({zindex: false})
];

gulp.task('styles', function () {
   gulp.src(scss)

    .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError)) //error log to keep session going when scss contains error
      .pipe(postcss(plugins))
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('./'));
});

gulp.task('scripts', function () {
   gulp.src(js)
    .pipe(sourcemaps.init())
      .pipe(uglify()) 
      .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('./js'));
});

gulp.task('jshint', function () {
  gulp.src('js/_*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
});

gulp.task('watch', function() {
  gulp.watch(scss, ['styles']);
  gulp.watch('js/_*.js', ['scripts', 'jshint']);
});

gulp.task('default', ['watch']);
