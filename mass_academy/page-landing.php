<?php
/*
Template Name: Landing Page
*/

get_header( 'landing' );

// =================================================
// :: Gets the .hero background ::
// -------------------------------------------------
if($featuredImage = get_post_thumbnail_id()){
	$featuredImage = get_post($featuredImage)->guid;
}

?>
	<div class="hero <?php if($featuredImage) echo 'imaged" style="background: url('. $featuredImage . '); background-position: center; background-repeat: no-repeat; background-size: cover;'; ?>">
		<header>
			<h1>
				<?php if($featuredImage) echo '<span>' ?>
					<?php the_title() ?>
				<?php if($featuredImage) echo '</span>' ?>
			</h1>
		</header>
	</div>

	<section id="the-content" role="contentinfo" aria-label="<?= wp_title(''); ?>">
		<div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Displays either content as full width or with sidebar
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$sidebar = getSidebar('page');
			?>
			<article class="main">
				<?php get_template_part('loop', 'page') ?>
			</article>
			<div class="clear"></div>
		</div>
	</section>

<?php get_footer() ?>