<?php get_header(); ?>

<?php 
$args=array(
  'order'=>'ASC',
  'orderby'=> 'menu_order',
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 100
);
query_posts($args);
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php
$title = get_the_title();
$loweredTitle = strtolower($title);
$thetitle = str_replace(" ", "", $loweredTitle);
?>
<section class="block <?php print $thetitle; ?>" id="block<?php the_ID(); ?>">
	<div class="arrow-l"></div>
	<div class="arrow-m"></div>
	<div class="arrow-r"></div>
	<div class="container-wrapper">
		<div class="container">
			<div class="container-content">
				<h1><?php the_title(); ?>.</h1>
				<h2><?php the_field('block-headline'); ?></h2>
				<div class="left-column">
					<?php the_content(); ?>
				</div>
				<?php if( get_field('right-column') ): ?>
				<div class="right-column">
					<?php the_field('right-column'); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
