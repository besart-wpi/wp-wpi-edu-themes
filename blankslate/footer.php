</div>
<footer id="footer" role="contentinfo">
<div id="copyright">
<?php //echo sprintf( __( '%1$s %2$s %3$s', 'blankslate' ), '&copy;', date('Y'), esc_html(get_bloginfo('name')) ); ?>
&copy; <?php echo date('Y'); ?> Worcester Polytechnic Institute. All rights reserved.
</div>
</footer>
</div>
<?php wp_footer(); ?>

<!--[if !lte IE 8]><!-->
<script>
jQuery(".flexnav").flexNav();
</script>
<!--<![endif]-->

</body>
</html>
