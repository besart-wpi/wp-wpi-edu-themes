<!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js ie ie6"><![endif]-->
<!--[if IE 7]><html class="no-js ie ie7"><![endif]-->
<!--[if IE 8]><html class="no-js ie ie8"><![endif]-->
<!--[if IE 9]><html class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
<title><?php wp_title(' | ', true, 'right'); ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />

<!--[if !lte IE 8]><!-->
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/themes/blankslate/css/flexnav.css" />
 <!--<![endif]-->

<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<!--
/**
 * @license
 * MyFonts Webfont Build ID 2703593, 2013-12-07T12:46:37-0500
 * 
 * The fonts listed in this notice are subject to the End User License
 * Agreement(s) entered into by the website owner. All other parties are 
 * explicitly restricted from using the Licensed Webfonts(s).
 * 
 * You may obtain a valid license at the URLs below.
 * 
 * Webfont: Museo Sans 700 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-sans/700/
 * Copyright: Copyright (c) 2008 by Jos Buivenga. All rights reserved.
 * 
 * Webfont: Museo Sans 300 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-sans/300/
 * Copyright: Copyright (c) 2008 by Jos Buivenga. All rights reserved.
 * 
 * Webfont: Museo Sans 500 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-sans/500/
 * Copyright: Copyright (c) 2008 by Jos Buivenga. All rights reserved.
 * 
 * Webfont: Museo Sans 300 Italic by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-sans/300-italic/
 * Copyright: Copyright (c) 2008 by Jos Buivenga. All rights reserved.
 * 
 * Webfont: Museo Slab 100 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-slab/100/
 * Copyright: Copyright (c) 2009 by Jos Buivenga. All rights reserved.
 * 
 * Webfont: Museo Slab 300 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-slab/300/
 * Copyright: Copyright (c) 2009 by Jos Buivenga. All rights reserved.
 * 
 * Webfont: Museo Slab 500 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-slab/500/
 * Copyright: Copyright (c) 2009 by Jos Buivenga. All rights reserved.
 * 
 * 
 * License: http://www.myfonts.com/viewlicense?type=web&buildid=2703593
 * Licensed pageviews: 10,000
 * 
 * © 2013 MyFonts Inc
*/

-->

<link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/themes/blankslate/css/webfonts.css" />
<?php wp_head(); ?>
<script src="<?php echo get_site_url(); ?>/wp-content/themes/blankslate/js/animatescroll.js"></script>

<!--[if !lte IE 8]><!-->
<script src="<?php echo get_site_url(); ?>/wp-content/themes/blankslate/js/jquery.flexnav.min.js"></script>
<!--<![endif]-->

<script>
// add HTML5 support to IE
document.createElement('header');document.createElement('footer');document.createElement('section');document.createElement('aside');document.createElement('nav');document.createElement('article');

</script>

</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
<nav id="menu" role="navigation">
<?php
$args=array(
  'order'=>'ASC',
  'orderby'=> 'menu_order',
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 100
);
query_posts($args);
?>
<div class="menu-button"><i class="fa fa-align-justify"></i></div>
<ul class="fl flexnav one-page" data-breakpoint="800">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php $hide = get_field('hide-from-nav'); ?>
<?php if($hide == "") { ?>
<li><a onclick="jQuery('#block<?php the_ID(); ?>').animatescroll({scrollSpeed:1000, padding: 86});"><?php the_title(); ?></li></a>
<?php } ?><?php endwhile; endif; ?>
</ul>

<a class="fr" href="http://www.wpi.edu/"><img src="<?php echo get_site_url(); ?>/wp-content/themes/blankslate/img/wpi-logo.png" height="55px" /></a>

</nav>
</header>
<div class="main" id="container">
