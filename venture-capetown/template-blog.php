<?php
/*
Template Name: Blog
*/
?>
 
<?php get_header(); ?>

<div id="heading">

	<div>
	
		<h2><?php the_title(); ?></h2>
 
		<?php
		$btn_title = trim( get_post_meta( get_the_ID(), 'wpzoom_top_button_title', true ) );
		$btn_url = trim( get_post_meta( get_the_ID(), 'wpzoom_top_button_url', true ) );

		if ( !empty($btn_title) && !empty($btn_url) ) {
			echo '<div id="top_button"><a href="' . esc_url($btn_url) . '">' . esc_html($btn_title) . '</a></div>';
		}
		?>

	</div>

</div><!-- / #welcome -->

<div id="content-wrap">

	<div id="content">

		<div class="post_content">
	 
			<?php // WP 3.0 PAGED BUG FIX
				if ( get_query_var('paged') )
					$paged = get_query_var('paged');
				elseif ( get_query_var('page') ) 
					$paged = get_query_var('page');
				else 
					$paged = 1;
				//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
				 
				query_posts("post_type=post&paged=$paged"); if (have_posts()) :
			?>	
			
				<?php get_template_part('loop'); ?>
				
			<?php endif; ?>
	 
		</div><!-- / .post_content -->

		<!-- blog sidebar -->
		<div id="sidebar">

			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog: Sidebar')) { } ?>
			<div class="cleaner">&nbsp;</div>

		</div><!-- / #sidebar -->
		<div class="cleaner">&nbsp;</div>

	 
	</div><!-- / #content -->

</div>

<?php get_footer(); ?>