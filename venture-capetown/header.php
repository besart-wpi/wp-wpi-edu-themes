<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<title><?php ui::title(); ?></title>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link href="https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300,300italic,900,900italic" rel="stylesheet" type="text/css" />

		<?php wp_head(); ?>
 		<?php if (is_home() || get_post_type() == 'portfolio') { ui::js("flexslider"); } ?>
	</head>

	<body <?php body_class() ?>>
		<?php global $woocommerce; ?>
		<div id="wrapper">
			<div id="inner-wrap">
				<div id="header">
					<span class="loginLink"><a href="<?php echo wp_login_url(); ?>"><?php if (is_user_logged_in()): ?>Dashboard &raquo;<?php else: ?>Login &raquo;<?php endif; ?></a></span>
					<div id="logo">						 
						 <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/wpi-logo.png">
						 <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						 <h3 class="site-description"><?php bloginfo( 'description' ); ?></h3>						 
					</div>
									
					<a class="btn_menu" id="toggle" href="#">Menu</a>
					<div class="clear"></div>

 					<div id="menu">
						<div class="divide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/orangeLine.png"></div>	
 						<div class="menuDiv">
						<?php if (has_nav_menu( 'primary' )) {

 							if (current_theme_supports('woocommerce') && option::get('cart_icon') == 'on') {

 								$variable = '<li class="menu-item"><a href="' . $woocommerce->cart->get_cart_url() . '" title="' . __( 'View your shopping cart', 'wpzoom' ) . '" class="cart-button">' . '<span>' . sprintf(_n( '%d item &ndash; ', '%d items &ndash; ', $woocommerce->cart->get_cart_contents_count(), 'wpzoom'), $woocommerce->cart->get_cart_contents_count()) . $woocommerce->cart->get_cart_total() . '</span></a></li>';
							}
  
							$menu = wp_nav_menu( array(
								'container' => 'menu',
								'container_class' => '',
								'menu_class' => 'dropdown',
								'menu_id' => 'mainmenu',
								'echo' => false,
								'sort_column' => 'menu_order',
								'theme_location' => 'primary',
								'items_wrap'=>'<ul id="%1$s" class="%2$s">%3$s'.$variable.'</ul>'

							));

						print $menu;
 
						} else {
							echo '<p>Please set your Main navigation menu on the <strong><a href="'.get_admin_url().'nav-menus.php">Appearance > Menus</a></strong> page.</p>';
						} ?>
						</div>
						<div class="menuDivide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blueLine.png"></div>
  						
					</div>
											 
					<div class="clear"></div>					
					
					<?php if (option::get('featured_posts_show') == 'on' && is_home() ) { // Show the Featured Slider
						  get_template_part('wpzoom-slider'); 
					} else { ?>
					   	  <div class="intImage">
						  <?php dynamic_sidebar('interior-image'); ?>
						  </div>
					<?php } ?>
 					 
				</div><!-- / #header-->		
				
				<div id="main">					

					<?php if ( is_home() && option::get('homepage_intro') == 'on' && option::get('homepage_intro_msg') != '' ) { ?>
						<div id="heading">
							<div id="intro">
								<p><?php echo esc_html( trim( option::get('homepage_intro_msg') ) ); ?></p>
							</div>
						</div>
					<?php } ?>

					<div id="tab" class="green"></div>