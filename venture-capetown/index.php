<?php get_header(); ?>

	<div id="content-wrap">
		<div id="homewidgets">
			<div class="table">
				<?php
				if ( is_active_sidebar('home-main') ) {
					$class = !is_active_sidebar('home-rightmain') ? ' class="fullwidth"' : '';
					?><div id="homewidgets-main"<?php echo $class; ?>><?php dynamic_sidebar('Homepage: Main'); ?></div><?php
				}

				if ( is_active_sidebar('home-rightmain') ) {
					?><div id="homewidgets-rightmain"><?php dynamic_sidebar('Homepage: Right of Main'); ?></div><?php
				}
				?>
			</div>

			<div class="cleaner">&nbsp;</div>

			<?php
			if ( is_active_sidebar('home-belowmainwide') ) {
				?><div id="homewidgets-wide"><?php dynamic_sidebar('Homepage: Wide Below Main'); ?></div><?php
			}

			if ( is_active_sidebar('home-belowwidelarge') ) {
				?><div id="homewidgets-large"><?php dynamic_sidebar('Homepage: Large Below Wide'); ?></div><?php
			}

			if ( is_active_sidebar('home-rightlarge') ) {
				?><div id="homewidgets-rightlarge"><?php dynamic_sidebar('Homepage: Right of Large'); ?></div><?php
			}
			?>

			<div class="cleaner">&nbsp;</div>

			<?php
			if ( is_active_sidebar('home-bottomwide') ) {
				?><div id="homewidgets-bottom"><?php dynamic_sidebar('Homepage: Bottom Wide'); ?></div><?php
			}
			?>
		</div>
	</div>

<?php get_footer(); ?>
