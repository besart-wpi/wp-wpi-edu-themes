<?php
/*
Template Name: Testimonials
*/
?>
 
<?php get_header(); ?>

<div id="content-wrap">

	<div class="titleBox">

		<h1><?php the_title(); ?></h1>

		<?php
		$btn_title = trim( get_post_meta( get_the_ID(), 'wpzoom_top_button_title', true ) );
		$btn_url = trim( get_post_meta( get_the_ID(), 'wpzoom_top_button_url', true ) );

		if ( !empty($btn_title) && !empty($btn_url) ) {
			echo '<div id="top_button"><a href="' . esc_url($btn_url) . '">' . esc_html($btn_title) . '</a></div>';
		}
		?>

	</div>
	<div class="searchBox"><?php get_search_form(); ?></div>
	<div class="clear"></div>
	
	<div id="content">

		<div class="post_content">

			<div class="wpzoom-testimonial">
	 
			<?php 


		$loop = new WP_Query( array( 'post_type' => 'testimonial', 'posts_per_page' => 99, 'orderby' => 'date') ); 

		while ( $loop->have_posts() ) : $loop->the_post();

		$customFields = get_post_custom();
		
 			$testimonial_author = $customFields['wpzoom_testimonial_author'][0]; 
		 
			$testimonial_position = $customFields['wpzoom_testimonial_author_position'][0];  
		 
			$testimonial_company = $customFields['wpzoom_testimonial_author_company'][0];  
	 
			$testimonial_company_url = $customFields['wpzoom_testimonial_author_company_url'][0];  
		 
		 
		?>
				 
				<div class="testimonial" style="margin:0 0 45px;">

					<?php  get_the_image( array( 'size' => 'testimonial-widget-author-photo', 'width' => 72, 'before' => '<div class="cover">', 'after' => '</div>', 'link_to_post' => false ) );
					 

					 echo "<h4>$testimonial_author</h4>";
					echo "<p class=\"position\">$testimonial_position</p>";
				 
						echo '<p class="company">';
						 echo "<a href=\"$testimonial_company_url\">";
						 
						  echo '</a>';
						echo '</p>';
					 
					?>

				<blockquote><?php the_excerpt(); ?></blockquote>
				<div class="cleaner">&nbsp;</div>

				</div><!-- end .testimonial -->
				 
			<?php
			endwhile; 
			
			//Reset query_posts
			wp_reset_query();	


			?>
	 
	 </div><!-- end .testimonial -->
		</div><!-- / .post_content -->
		<?php get_sidebar(); ?>
	 
	</div><!-- / #content -->

</div>

<?php get_footer(); ?>