<?php
/*
Template Name: Full Width No Nav
*/
?>
<?php get_header(); ?>

<div id="content-wrap">

	<div class="titleBox">

		<h1><?php the_title(); ?></h1>

		<?php
		$btn_title = trim( get_post_meta( get_the_ID(), 'wpzoom_top_button_title', true ) );
		$btn_url = trim( get_post_meta( get_the_ID(), 'wpzoom_top_button_url', true ) );

		if ( !empty($btn_title) && !empty($btn_url) ) {
			echo '<div id="top_button"><a href="' . esc_url($btn_url) . '">' . esc_html($btn_title) . '</a></div>';
		}
		?>

	</div>
	<div class="searchBox"><?php get_search_form(); ?></div>
	<div class="clear"></div>	 
	
	<div id="content" class="full-width">
	 
		<div class="post_content_left">
			<?php wp_reset_query(); if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php edit_post_link( __('Edit', 'wpzoom'), '<div class="meta"> ', '</div>'); ?>

			<div class="entry">			
				
				<?php the_content(); ?>			
				<?php wp_link_pages(array('before' => '<p class="pages"><strong>'.__('Pages', 'wpzoom').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

				<div class="cleaner">&nbsp;</div>

			</div><!-- / .entry -->

			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria', 'wpzoom');?>.</p>
			<?php endif; ?>

			<div class="cleaner">&nbsp;</div>          
		</div><!-- / .post_content -->
	 
		<div class="cleaner">&nbsp;</div>
	</div><!-- / #content -->

</div>
 
<?php get_footer(); ?>