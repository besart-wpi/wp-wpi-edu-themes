<?php
/*
Template Name: Left Nav Template
*/
?>
<?php get_header(); ?>


<div id="content-wrap">

	<div class="leftNav">
		 <div class="searchBox"><?php get_search_form(); ?></div>
		 
		 <?php wp_nav_menu( array(
		 	   'theme_location' => 'submenu',
			   'menu_class' => 'nav-menu'
	 	 ) );
		 ?>	
		 
	</div>
	<div class="clear"></div>	
	
	<div id="content" class="left-nav">
	 
		<div class="post_content_left">
			
			<?php wp_reset_query(); if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="entry">			
				
				<?php edit_post_link( __('Edit', 'wpzoom'), '<div class="meta"> ', '</div>'); ?>
				<h1><?php the_title(); ?></h1>
			
				<?php the_content(); ?>
			
				<?php wp_link_pages(array('before' => '<p class="pages"><strong>'.__('Pages', 'wpzoom').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

				<div class="cleaner">&nbsp;</div>

			</div><!-- / .entry -->

			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria', 'wpzoom');?>.</p>
			<?php endif; ?>

			<div class="cleaner">&nbsp;</div>          
		</div><!-- / .post_content -->
	 
		<div class="cleaner">&nbsp;</div>
	</div><!-- / #content -->

</div>
 
<?php get_footer(); ?>