<?php get_header() ?>	
	<div id="main">	
		
		<div class="interior">
			
			<h1 class="page-title"><?php _e('Not Found', 'wpi'); ?></h1>				
			
			<div class="main-content the-content">
				<p><?php _e('We apologize, but the page you were looking for was not found.', 'wpi'); ?></p>
				<div class="clear"></div>
			</div>
			
		</div>
		
	</div>    
<?php get_footer() ?>