<?php get_header() ?>	
	<div id="main">	
		
		<div class="interior timeline-interior">
			<?php
			global $wp_query;
			$args = array_merge( $wp_query->query_vars, array(
				'meta_key' => '_start_date',
				'meta_type' => 'DATE',
				'order' => 'ASC',
				'orderby' => 'meta_value',
				'posts_per_page' => '1' ) );
			query_posts( $args );
			?>
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
			
				<?php get_template_part('content', 'timeline'); ?>
			
			<?php endwhile; endif; ?>
		</div>
		
	</div>    
<?php get_footer() ?>