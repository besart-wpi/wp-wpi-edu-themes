<?php get_header() ?>	
	<div id="main">	
		
		<div class="interior timeline-interior">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
			
				<?php get_template_part('content', 'timeline'); ?>
			
			<?php endwhile; endif; ?>
		</div>
		
	</div>    
<?php get_footer() ?>