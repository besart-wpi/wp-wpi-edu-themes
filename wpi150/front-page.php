<?php get_header() ?>	
	<div id="main">
		
		<div class="home-content">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
				<div class="home-right">
					<?php
					$content = get_the_content();
					$content = apply_filters('the_content', $content); 
					$content = str_replace(']]>', ']]&gt;', $content); 
					echo truncate_html($content, 291, '');
					
					$intro_url = get_post_meta($post->ID, '_intro_url', true);
					if($intro_url){
						$intro_label = 'Read More';
						if(get_post_meta($post->ID, '_intro_label', true)){
							$intro_label = get_post_meta($post->ID, '_intro_label', true);
						}
						echo '<a href="'.esc_url($intro_url).'" class="home-more">'.$intro_label.'</a>';
					}
					?>
				</div>
				<div class="home-left">
					<?php
					$placeholder_image = get_post_meta($post->ID, '_placeholder_image', true); // Used for events with no thumbnail or when there are no upcoming events
					$events = tribe_get_events(array('eventDisplay' => 'upcoming', 'posts_per_page'   => 1));
					
					if(!empty($events)):
						foreach ( $events as $post ) :
						setup_postdata( $post );
					?>
						<div class="home-event">
							<div class="ec-top">
								<div class="ec-date">								
									<span class="ec-date-month"><?php echo tribe_get_start_date(NULL, 'true', 'M'); ?></span>
									<span class="ec-date-day"><?php echo tribe_get_start_date(NULL, 'true', 'd'); ?></span>							</div>
								<div class="ec-image">
                                	<?php 
									if(has_post_thumbnail()){ 
										the_post_thumbnail('event-graphic-thumb');
									
									// Placeholder
									} else {
										if($placeholder_image){
											// The field contains a URL, try to get the image ID instead
											if(!is_numeric($placeholder_image)){
												$placeholder_image = get_attachment_id_from_url( $placeholder_image );
											}
											
											// Get the image
											if(is_numeric($placeholder_image)){
												$placeholder_image = theme_get_image($placeholder_image, 'event-graphic-thumb');
												echo $placeholder_image;
											} else {
												// Something went wrong, show nothing
											}
										}
									} ?>
								</div>
								<div class="clear"></div>
							</div>
							<div class="ec-title">
								<h3><?php the_title(); ?></h3>
								<?php
								$sub_heading = get_post_meta($post->ID, '_sub_heading', true);
								if($sub_heading){
									echo '<h4>'.$sub_heading.'</h4>';
								}
								?>
							</div>

							<a href="<?php echo tribe_get_events_link(); ?>" class="view-events">View More Events</a>
						</div>
					<?php

						endforeach;
					else:
						if($placeholder_image){
							if(is_numeric($placeholder_image)){
								$placeholder_image = theme_get_image($placeholder_image, 'placeholder-size');
							}
							echo '<img src="'.$placeholder_image.'" alt="">';
						}
					endif;
					?>

				</div>
				<div class="clear"></div>
			<?php endwhile; endif; ?>
		</div>	
	</div>    
<?php get_footer() ?>