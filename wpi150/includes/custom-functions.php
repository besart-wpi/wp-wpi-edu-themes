<?php
// =================================================
//	:: Get Image By Attachment ID ::
// -------------------------------------------------
function theme_get_image($attachment_id, $image_size = NULL, $is_img_tag = true){
	if($attachment_id) {
		if($image_size == NULL) {
			$image_size = 'post-thumbnail';
		}
		$image = wp_get_attachment_image_src($attachment_id, $image_size);
		if(!empty($image)){
			$image = $image[0];
			if($is_img_tag) {
				return '<img src="'.$image.'" alt="" />';
			} else {
				return $image;
			}
		} else {
			return '';
		}
	} else {
		return '';
	}
}


// =================================================
//	:: Get Image By Attachment ID ::
// -------------------------------------------------
function get_attachment_id_from_url( $attachment_url = '' ) {
    return;
    
    global $wpdb;
    $attachment_id = false;

    // If there is no url, return.
    if ( '' == $attachment_url )
        return;

    // Get the upload directory paths
    $upload_dir_paths = wp_upload_dir();

    // Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
    if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {

        // If this is the URL of an auto-generated thumbnail, get the URL of the original image
        $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

        // Remove the upload path base directory from the attachment URL
        $attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );

        // Finally, run a custom database query to get the attachment ID from the modified attachment URL
        $attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );

    }

    return $attachment_id;
}

// =================================================
//	:: Truncate HTML String while preserving tags ::
// -------------------------------------------------
function truncate_html($s, $l, $e = '&hellip;', $isHTML = true) {
    $s = trim($s);
    $e = (strlen(strip_tags($s)) > $l) ? $e : '';
    $i = 0;
    $tags = array();

    if($isHTML) {
        preg_match_all('/<[^>]+>([^<]*)/', $s, $m, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
        foreach($m as $o) {
            if($o[0][1] - $i >= $l) {
                break;                  
            }
            $t = substr(strtok($o[0][0], " \t\n\r\0\x0B>"), 1);
            if($t[0] != '/') {
                $tags[] = $t;                   
            }
            elseif(end($tags) == substr($t, 1)) {
                array_pop($tags);                   
            }
            $i += $o[1][1] - $o[0][1];
        }
    }
    $output = substr($s, 0, $l = min(strlen($s), $l + $i)) . (count($tags = array_reverse($tags)) ? '</' . implode('></', $tags) . '>' : '') . $e;
    return $output;
}
?>