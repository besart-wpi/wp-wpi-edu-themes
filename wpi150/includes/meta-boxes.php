<?php
/**
 * Initialize the custom Meta Boxes. 
 */
add_action( 'admin_init', 'custom_meta_boxes' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function custom_meta_boxes() {
  
  /**
   * Create a custom meta boxes array that we pass to 
   * the OptionTree Meta Box API Class.
   */
	$timeline_event_metabox = array(
		'id'          => 'timeline_event_metabox',
		'title'       => __( 'Event Details', 'wpi-dashboard' ),
		'desc'        => '',
		'pages'       => array( 'timeline_event' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(			
			array(
			'label'       => __( 'Story Date', 'wpi-dashboard' ),
			'id'          => '_story_date',
			'type'        => 'text',
			'desc'        => 'Optional. Use this date field to override the generated date that displays on the event detail page below the title. Plain text, can be in any format.'
			),
			array(
			'label'       => __( 'Start Date', 'wpi-dashboard' ),
			'id'          => '_start_date',
			'type'        => 'date-picker',
			'desc'        => 'Start date for the event'
			),
			array(
			'label'       => __( 'End Date', 'wpi-dashboard' ),
			'id'          => '_end_date',
			'type'        => 'date-picker',
			'desc'        => 'Optional. End date for the event.'
			),
			array(
			'label'       => __( 'Image', 'wpi-dashboard' ),
			'id'          => '_event_image',
			'type'        => 'upload',
			'class'       => 'ot-upload-attachment-id required',
			'desc'        => 'REQUIRED! Larger image used on the event detail page. Image should be at least 683x286 (larger will be cropped)'
			),
			array(
			'label'       => __( 'YouTube URL', 'wpi-dashboard' ),
			'id'          => '_youtube_url',
			'type'        => 'text',
			'desc'        => 'Optional. The YouTube URL should be in this format: https://www.youtube.com/watch?v=XXXXXXXXXXX'
			),
			array(
			'label'       => __( 'SoundCloud URL', 'wpi-dashboard' ),
			'id'          => '_soundcloud_url',
			'type'        => 'text',
			'desc'        => 'Optional. The SoundCloud URL should be in this format: https://soundcloud.com/user/track'
			),
		)
	);
	
	$tribe_event_metabox = array(
		'id'          => 'tribe_event_metabox',
		'title'       => __( 'Event Details', 'wpi-dashboard' ),
		'desc'        => '',
		'pages'       => array( 'tribe_events' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(			
			array(
			'label'       => __( 'Sub-Heading', 'wpi-dashboard' ),
			'id'          => '_sub_heading',
			'type'        => 'text',
			'desc'		  => 'Optional. Sub-heading displays directly below event name.'
			)
		)
	);
	
	$homepage_metabox = array(
		'id'          => 'homepage_metabox',
		'title'       => __( 'Home Page Options', 'wpi-dashboard' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(			
			array(
			'label'       => __( 'Intro Text Call to Action URL', 'wpi-dashboard' ),
			'id'          => '_intro_url',
			'type'        => 'text'
			),
			array(
			'label'       => __( 'Intro Text Call to Action Label', 'wpi-dashboard' ),
			'id'          => '_intro_label',
			'type'        => 'text'
			),
			array(
			'label'       => __( 'Event Placeholder Image', 'wpi-dashboard' ),
			'desc'		  => __( 'This field expects an image ID, not an absolute URL (Ex: 123) Use the [+] button to the left to upload an image or select one from the Media Library. This image will be used for the thumbnail on events with no featured image set (on the Upcoming Events page too). It will also be used when there are no upcoming events to display.', 'wpi-dashboard' ),
			'id'          => '_placeholder_image',
			'type'        => 'upload',
			'class'       => 'ot-upload-attachment-id'
			)
		)
	);
	
	$logo_interactive_metabox = array(
		'id'          => 'logo_interactive_metabox',
		'title'       => __( 'Logo Section Details', 'wpi-dashboard' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(	
			array(
			'label'       => __( 'Logo Section 1', 'wpi-dashboard' ),
			'id'          => 'logo_section_1',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_1_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_1_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_1_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_1_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_1_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 2', 'wpi-dashboard' ),
			'id'          => 'logo_section_2',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_2_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_2_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_2_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_2_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_2_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 3', 'wpi-dashboard' ),
			'id'          => 'logo_section_3',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_3_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_3_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_3_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_3_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_3_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 4', 'wpi-dashboard' ),
			'id'          => 'logo_section_4',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_4_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_4_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_4_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_4_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_4_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 5', 'wpi-dashboard' ),
			'id'          => 'logo_section_5',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_5_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_5_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_5_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_5_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_5_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 6', 'wpi-dashboard' ),
			'id'          => 'logo_section_6',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_6_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_6_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_6_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_6_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_6_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 7', 'wpi-dashboard' ),
			'id'          => 'logo_section_7',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_7_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_7_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_7_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_7_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_7_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 8', 'wpi-dashboard' ),
			'id'          => 'logo_section_8',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_8_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_8_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_8_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_8_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_8_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 9', 'wpi-dashboard' ),
			'id'          => 'logo_section_9',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_9_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_9_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_9_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_9_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_9_gallery',
				'type'        => 'gallery'
				),
			array(
			'label'       => __( 'Logo Section 10', 'wpi-dashboard' ),
			'id'          => 'logo_section_10',
			'type'        => 'tab'
			),
				array(
				'label'       => __( 'Icon', 'wpi-dashboard' ),
				'id'          => '_section_10_icon',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id'
				),
				array(
				'label'       => __( 'Headline', 'wpi-dashboard' ),
				'id'          => '_section_10_headline',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Sub-Head', 'wpi-dashboard' ),
				'id'          => '_section_10_subhead',
				'type'        => 'text'
				),
				array(
				'label'       => __( 'Content', 'wpi-dashboard' ),
				'id'          => '_section_10_content',
				'type'        => 'textarea'
				),
				array(
				'label'       => __( 'Gallery', 'wpi-dashboard' ),
				'id'          => '_section_10_gallery',
				'type'        => 'gallery'
				),
		)
	);

  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
	if ( function_exists( 'ot_register_meta_box' ) ):	
		
		ot_register_meta_box( $tribe_event_metabox );		
		ot_register_meta_box( $timeline_event_metabox );
		
		$template_file = '';
		$post_id = '';
		
		if(isset($_GET['post']) || isset($_POST['post_ID'])):
		
			if(isset($_POST['post_ID'])){
				$post_id = $_POST['post_ID'];
			}
			
			if(isset($_GET['post'])){
				$post_id = $_GET['post'];
			}
			
			if($post_id) {
				$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);				
			}
		endif;
		
		if($post_id == get_option('page_on_front')){
			ot_register_meta_box( $homepage_metabox );			
		} elseif ($template_file == 'page-templates/logo-interactive.php') {
			ot_register_meta_box( $logo_interactive_metabox );
		}
		
	endif;

}
?>