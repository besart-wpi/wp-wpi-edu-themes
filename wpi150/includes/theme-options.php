<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
   
	$custom_settings = array( 
	
		'sections'        => array( 
			array(
				'id'          => 'periods_options',
				'title'       => __( 'Time Periods Options', 'wpi-wpi150' )
			),
			array(
				'id'          => 'footer_options',
				'title'       => __( 'Footer Options', 'wpi-wpi150' )
			)
		),
		
		'settings'        => array( 
			
			// Periods Settings
			array(
				'id'          => 'periods_past',
				'label'       => __( 'Past', 'wpi-wpi150' ),
				'type'        => 'taxonomy-select',
				'taxonomy'    => 'periods',
				'section'     => 'periods_options',
			),
			array(
				'id'          => 'periods_present',
				'label'       => __( 'Present', 'wpi-wpi150' ),
				'type'        => 'taxonomy-select',
				'taxonomy'    => 'periods',
				'section'     => 'periods_options',
			),
			array(
				'id'          => 'periods_future',
				'label'       => __( 'Future', 'wpi-wpi150' ),
				'type'        => 'taxonomy-select',
				'taxonomy'    => 'periods',
				'section'     => 'periods_options',
			),
			
			// Footer Options 
			// -- Block 1
			array(
				'id'          => 'block_1_heading',
				'label'       => __( 'Block 1 - Heading', 'wpi-wpi150' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			array(
				'id'          => 'block_1_subheading',
				'label'       => __( 'Block 1 - Sub-heading', 'wpi-wpi150' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			array(
				'id'          => 'block_1_graphic',
				'label'       => __( 'Block 1 - Graphic', 'wpi-wpi150' ),
				'desc'		  => 'Image should be 120x114px',
				'type'        => 'upload',				
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => 'ot-upload-attachment-id',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'block_1_url',
				'label'       => __( 'Block 1 - Link URL', 'wpi-wpi150' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			
			// -- Block 2
			array(
				'id'          => 'block_2_heading',
				'label'       => __( 'Block 2 (Social Media) - Heading', 'wpi-wpi250' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			array(
				'id'          => 'block_2_subheading',
				'label'       => __( 'Block 2 (Social Media) - Sub-heading', 'wpi-wpi250' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),			
			array(
				'id'          => 'block_2_facebook',
				'label'       => __( 'Block 2 (Social Media) - Facebook URL', 'wpi-wpi250' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			array(
				'id'          => 'block_2_twitter',
				'label'       => __( 'Block 2 (Social Media) - Twitter URL', 'wpi-wpi250' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			array(
				'id'          => 'block_2_instagram',
				'label'       => __( 'Block 2 (Social Media) - Instagram URL', 'wpi-wpi250' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			
			// -- Block 3
			array(
				'id'          => 'block_3_heading',
				'label'       => __( 'Block 3 - Heading', 'wpi-wpi350' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			array(
				'id'          => 'block_3_subheading',
				'label'       => __( 'Block 3 - Sub-heading', 'wpi-wpi350' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
			array(
				'id'          => 'block_3_graphic',
				'label'       => __( 'Block 3 - Graphic', 'wpi-wpi350' ),
				'desc'		  => 'Image should be 120x114px',
				'type'        => 'upload',
				'class'       => 'ot-upload-attachment-id',
				'section'     => 'footer_options',
			),
			array(
				'id'          => 'block_3_url',
				'label'       => __( 'Block 3 - Link URL', 'wpi-wpi350' ),
				'type'        => 'text',
				'section'     => 'footer_options',
			),
		
			
		) // END settings
		
	); // END $custom_settings
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
}
