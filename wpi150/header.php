<!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!--><html <?php language_attributes(); ?>><!--<![endif]-->
<head>
    <title><?php echo get_bloginfo('name'), ' ', wp_title(); ?></title>
    <meta charset="<?php echo get_bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/668448/607806/css/fonts.css" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>><div class="wrap">
	<header id="header">
		<div id="logo">
			<?php
			// For SEO
			if(is_front_page()):
				echo '<h1>'.get_bloginfo('name').'</h1>';
			endif;
			?>
			<a href="<?php echo esc_url(home_url('/')); ?>"><img class="desktop" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt=""><img class="mobile" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt=""></a>
		</div>
		
		<a href="#" id="show-menu">Main Menu</a>
		
		<nav id="nav" class="clearfix">
			<?php
			$args = array(
				'container'		=> '',
				'theme_location'=> 'main',
				'depth'			=> 1,
				'menu_class'	=> 'clearfix',
				'walker'        => new WPI_Walker_Nav_Menu				
			);
			wp_nav_menu($args);
			?>			
		</nav>
		<div class="clear"></div>
	</header>
	
	<?php
	$template_file = get_post_meta($post->ID, '_wp_page_template', TRUE);
	if($template_file != 'page-templates/logo-interactive.php') :
	?>
	
		<div class="timeline-wrap">
			<div class="timeline cycle-slideshow"
			data-cycle-swipe="true"
			data-cycle-fx="carousel"
			data-cycle-slides="> div"
			data-cycle-timeout="0"
			data-cycle-prev=".timeline-prev"
			data-cycle-next=".timeline-next"
			data-cycle-carousel-visible="10"
			data-cycle-allow-wrap="false"
			>
				<?php
				$periods_past = ot_get_option('periods_past');
				$periods_present = ot_get_option('periods_present');
				$periods_future = ot_get_option('periods_future');
				$args = array(
					'post_type' => 'timeline_event',
					'posts_per_page' => -1,
					'meta_key' => '_start_date',
					'meta_type' => 'DATE',
					'order' => 'ASC',
					'orderby' => 'meta_value',
					'tax_query' => array(
						array(
							'taxonomy' => 'periods',
							'field'    => 'id',
							'terms'    => $periods_past,
						),
					)
				);
				$current_post_id = '';
				if(is_singular('timeline_event')){
					$current_post_id = $post->ID;
				}
				if(is_tax('periods')){
					$queried_object = get_queried_object();
					$argx = array(
						'post_type' => 'timeline_event',
						'posts_per_page' => 1,
						'meta_key' => '_start_date',
						'meta_type' => 'DATE',
						'order' => 'ASC',
						'orderby' => 'meta_value',
						'tax_query' => array(
							array(
								'taxonomy' => 'periods',
								'field'    => 'id',
								'terms'    => $queried_object->term_id,
							),
						)
					);
					$periods = get_posts($argx);
					if(!empty($periods)){
						$current_post_id = $periods[0]->ID;
					}
				}
				$loop = new WP_Query($args);
				if($loop->have_posts()) : while ( $loop->have_posts() ) : $loop->the_post();
				?>
					<div class="timeline-slide <?php if($current_post_id == $post->ID) { echo 'active-ts'; } ?>">
						<?php $start_date = get_post_meta($post->ID, '_start_date', true); ?>
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('event-thumbnail', array('class'=>'timeline-image')); ?>
							<span class="ts-year"><?php echo date('Y', strtotime($start_date)); ?></span>
						</a>
					</div>
				<?php
				endwhile; endif; wp_reset_postdata();
				?>
				
				<?php
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'periods',
						'field'    => 'id',
						'terms'    => $periods_present,
					),
				);
				$loop = new WP_Query($args);
				if($loop->have_posts()) : while ( $loop->have_posts() ) : $loop->the_post();
				?>
					<div class="timeline-slide <?php if($current_post_id == $post->ID) { echo 'active-ts'; } ?>">
						<?php $start_date = get_post_meta($post->ID, '_start_date', true); ?>
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('event-thumbnail', array('class'=>'timeline-image')); ?>
							<span class="ts-year"><?php echo date('Y', strtotime($start_date)); ?></span>
						</a>
					</div>
				<?php
				endwhile; endif; wp_reset_postdata();
				?>
				
				<?php
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'periods',
						'field'    => 'id',
						'terms'    => $periods_future,
					),
				);
				$loop = new WP_Query($args);
				if($loop->have_posts()) : while ( $loop->have_posts() ) : $loop->the_post();
				?>
					<div class="timeline-slide <?php if($current_post_id == $post->ID) { echo 'active-ts'; } ?>">
						<?php $start_date = get_post_meta($post->ID, '_start_date', true); ?>
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('event-thumbnail', array('class'=>'timeline-image')); ?>
							<span class="ts-year"><?php echo date('Y', strtotime($start_date)); ?></span>
						</a>
					</div>
				<?php
				endwhile; endif; wp_reset_postdata();
				?>
				
			</div>
			<span class="timeline-prev"><span>Prev</span></span>
			<span class="timeline-next"><span>Next</span></span>
		</div><!-- end .timeline-wrap -->
	
	<?php endif; ?>