<?php
function battlecry_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'battlecry_enqueue_styles' );


/**
 *
 * Shortcode to insert team listing feed from http://battlecry.wpi.edu/battlecry/admin/reports/teams
 *
 */

function battlecry_team_list_shortcode( $atts ){
    $url = 'http://battlecry.wpi.edu/battlecry/admin/reports/teams';

    $response_transient_name = 'battlecry_team_list';
    $response = get_transient( $response_transient_name );

    if ( false === $response ) {
        $response = wp_remote_get( esc_url_raw( $url ) );
        set_transient( $response_transient_name, $response, DAY_IN_SECONDS );
    }

    if ( is_array( $response ) ) {
        $body = wp_remote_retrieve_body( $response );
        return $body;
    } elseif( is_wp_error( $response ) && defined('WP_DEBUG') && true === WP_DEBUG ){
        return $response->get_error_message();
    }
}
add_shortcode( 'battlecry_team_list', 'battlecry_team_list_shortcode' );