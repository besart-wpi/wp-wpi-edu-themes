<?php 

// =================================================
//	:: Register Thumbnails ::
// -------------------------------------------------
	add_theme_support( 'post-thumbnails' );



// =================================================
//	:: Parent Theme Components ::
// ------------------------------------------------

// Optional Post Type Frameworks
// Note: WPI_POST_TYPE_FRAMEWORKS must be set to 'true' for any of the individual frameworks to activate
if (!defined('WPI_POST_TYPE_FRAMEWORKS')) define('WPI_POST_TYPE_FRAMEWORKS', false);
if (!defined('WPI_PTF_HOMEPAGESLIDESHOW')) define('WPI_PTF_HOMEPAGESLIDESHOW', false); 
if (!defined('WPI_PTF_PHOTOGALLERY')) define('WPI_PTF_PHOTOGALLERY', false); 
if (!defined('WPI_PTF_LOGOPARADE')) define('WPI_PTF_LOGOPARADE', false); 
if (!defined('WPI_PTF_VIDEOGALLERY')) define('WPI_PTF_VIDEOGALLERY', true);
if (!defined('WPI_PTF_TESTIMONIALS')) define('WPI_PTF_TESTIMONIALS', false);
if (!defined('WPI_PTF_PROJECTPORTFOLIO')) define('WPI_PTF_PROJECTPORTFOLIO', false);


if (!defined('WPI_OPTIONTREE')) define('WPI_OPTIONTREE', true); // OptionTree
if (!defined('WPI_EASY_TINYMCE')) define('WPI_EASY_TINYMCE', true); // Easy TinyMCE Class
if (!defined('WPI_POST_ORDER')) define('WPI_POST_ORDER', true); // Post Ordering
if (!defined('WPI_VISUAL_SHORTCODES')) define('WPI_VISUAL_SHORTCODES', true); // Visual Shortcodes
if (!defined('WPI_HIGHLIGHT_BOX_SHORTCODE')) define('WPI_HIGHLIGHT_BOX_SHORTCODE', true); // Highlight Box Shortcode
if (!defined('WPI_VIDEO_GALLERY_SHORTCODE')) { define('WPI_VIDEO_GALLERY_SHORTCODE', WPI_PTF_VIDEOGALLERY == true ? true : false); } // Video Gallery Shortcode


// =================================================
//	:: Option Tree ::
//  https://github.com/valendesigns/option-tree
// -------------------------------------------------
// Theme Options UI Builder for WordPress. 
// A simple way to create & save Theme Options and Meta Boxes for free or premium themes.
// See /option-tree/example-theme-options.php

if(WPI_OPTIONTREE){
	add_filter( 'ot_theme_mode', '__return_true' ); // Set child theme mode
	add_filter( 'ot_show_pages', '__return_false' ); // Hide settings and documentation in Dashboard
	add_filter( 'ot_show_new_layout', '__return_false' ); // Hide 'New Layout' button in Theme Options 
	add_filter( 'ot_header_logo_link', '__return_false' ); // Hide OptoinTree link
	add_filter( 'ot_header_version_text', '__return_false' ); // Hide OptionTree version number
	
	load_template( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );
}



// =================================================
//	:: Easy TinyMCE Class ::
//  https://github.com/sazzadh/easy-tinymce-class
// -------------------------------------------------
// Helper Class to create TinyMCE buttons and Shortcode editor
// See /includes/easy-tinymce-class/example/ for a complete example

if(WPI_EASY_TINYMCE){
	load_template( trailingslashit( get_template_directory() ) . 'includes/easy-tinymce-class/Easy-tinyMCE.class.php' );
	load_template( trailingslashit( get_template_directory() ) . 'includes/easy-tinymce-class/Easy-tinyMCE-content.class.php' );
}



// =================================================
//	:: Post Type Frameworks ::
// -------------------------------------------------
// Post Type Frameworks are meant to be used as starting points for common post types. 
// They include definitions of the post type, meta boxes (custom fields), and theme options variables where appropriate.

if(WPI_POST_TYPE_FRAMEWORKS){
	load_template( trailingslashit( get_template_directory() ) . 'includes/post-type-frameworks.php' );
}



// =================================================
//	:: Post Ordering ::
// -------------------------------------------------
// Adds an interface to drag and drop posts into a custom order (menu_order)
// $post_order_whitelist array to set allowed post types
// Example: global $post_order_whitelist = array('page');
// Example: global $post_order_whitelist[] = 'page';

if(WPI_POST_ORDER){
	define( 'POST_ORDER_OF_OZ_URL', trailingslashit( get_template_directory_uri() ) . 'includes/post-order/' );
	load_template( trailingslashit( get_template_directory() ) . 'includes/post-order/post-order.php' );
}



// =================================================
// :: Shortcodes ::
// -------------------------------------------------

// Photo Gallery Shortcode
// Example: [gallery id=123]
if(WPI_PTF_PHOTOGALLERY){
	include( trailingslashit( get_template_directory() ) . 'includes/shortcodes/photo_gallery.php');
	include( trailingslashit( get_template_directory() ) . 'includes/tinymce-plugins/gallery-shortcode-tinymce/gallery-shortcode-tinymce.php');
}

// Highlight Box Shortcode
// Example: [hbox heading="Sample Heading" image="http://domain.com/image.jpg"]Content Here[/hbox]
if(WPI_HIGHLIGHT_BOX_SHORTCODE){ 
	include( trailingslashit( get_template_directory() ) . 'includes/shortcodes/highlight_box.php');
	include( trailingslashit( get_template_directory() ) . 'includes/tinymce-plugins/highlight-box-shortcode-tinymce/highlight-box-shortcode-tinymce.php');
}

// Video Gallery Shortcode
// Example: [vgallery id=123]
if(WPI_VIDEO_GALLERY_SHORTCODE){ 
	include( trailingslashit( get_template_directory() ) . 'includes/shortcodes/video_gallery.php');
	include( trailingslashit( get_template_directory() ) . 'includes/tinymce-plugins/video-gallery-shortcode-tinymce/vgallery-shortcode-tinymce.php');
}




// =================================================
//	:: Visual Shortcodes ::
// -------------------------------------------------
// This is a utility plugin that will allow other plugins and themes to swap out shortcodes with custom images
// https://wordpress.org/plugins/visual-shortcodes/
// Example:
// 
// add_filter('jpb_visual_shortcodes','add_visual_shortcode_image');
// function add_visual_shortcode_image($shortcodes){
// 	$shortcodes[] = array(
// 		'shortcode' => 'gravityform',
// 		'image' => get_stylesheet_directory_uri().'/images/form.png',
// 		'command' => NULL,
// 	);
// 	$shortcodes[] = array(
// 		'shortcode' => 'entry_grid',
// 		'image' => get_stylesheet_directory_uri().'/images/listings.png',
// 		'command' => NULL,
// 	);
// 	return $shortcodes;
// }

if(WPI_VISUAL_SHORTCODES){
	load_template( trailingslashit( get_template_directory() ) . 'includes/visual-shortcodes/visual-shortcodes.php' );
}

// Photo Gallery Visual Shortcode
if(WPI_PTF_PHOTOGALLERY){
	add_filter('jpb_visual_shortcodes','add_visual_shortcode_photo_gallery');
	function add_visual_shortcode_photo_gallery($shortcodes){
		$shortcodes[] = array(
			'shortcode' => 'gallery',
			'image' => get_template_directory_uri().'/images/shortcode_gallery.png',
			'command' => 'gallery_shortcode_uid',
		);
		return $shortcodes;
	}
}

// Video Gallery Visual Shortcode
if(WPI_VIDEO_GALLERY_SHORTCODE){
	add_filter('jpb_visual_shortcodes','add_visual_shortcode_video_gallery');
	function add_visual_shortcode_video_gallery($shortcodes){
		$shortcodes[] = array(
			'shortcode' => 'vgallery',
			'image' => get_template_directory_uri().'/images/shortcode_video_gallery.png',
			'command' => 'vgallery_shortcode_uid',
		);
		return $shortcodes;
	}
}

// Highlight Box Visual Shortcode
/* DISABLED: After testing, the Visual Shortcodes plugin chokes on shortcodes with content (ex: [shortcode]content[/shortcode]
if(WPI_HIGHLIGHT_BOX_SHORTCODE){
	add_filter('jpb_visual_shortcodes','add_visual_shortcode_highlight_box');
	function add_visual_shortcode_highlight_box($shortcodes){
		$shortcodes[] = array(
			'shortcode' => 'hbox',
			'image' => get_template_directory_uri().'/images/shortcode_highlight_box.png',
			'command' => 'highlight_box_shortcode_uid',
		);
		return $shortcodes;
	}
}
*/



// =================================================
//	:: Enqueue Scripts and Styles ::
// -------------------------------------------------

function wpi_parent_enqueue(){
	
	// CSS Reset
	wp_enqueue_style( 'wpi-parent-reset', get_template_directory_uri() . '/css/reset.css'); 
	
	// Core WordPress Styles
	wp_enqueue_style( 'wpi-parent-wpcore', get_template_directory_uri() . '/css/wpcore.css'); 

	//wp_enqueue_script('jquery');
	//wp_enqueue_script( 'jquery-cycle-2', get_template_directory_uri() . '/scripts/jquery.cycle2.min.js', array( 'jquery' ));		
}
add_action( 'wp_enqueue_scripts', 'wpi_parent_enqueue' );

?>