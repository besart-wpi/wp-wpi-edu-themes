<?php
/*------------Start Edting------------*/
$vgallery_tinyMCE = new Easy_TinyMCE_Class_content;
$vgallery_tinyMCE->id = 'vgallery';
$vgallery_tinyMCE->shortcode = 'vgallery';
$vgallery_tinyMCE->content = false;
$vgallery_tinyMCE->options = array(
	array(
		'id' => 'id',
		'field' => 'post-select', //text, textarea, select, post-select, tax-select
		'title' => __( 'Select a Video Gallery', 'wpi_mpi' ),
		'des' => 'Select an existing Video Gallery. Note: New video galleries can be created from the "Video Gallery" button in the Dashboard.',
		'post_type' => 'video_gallery',
		'multiple' => false,
		'content' => false,
		'option' => '',
	)
	
);
$vgallery_tinyMCE->html();