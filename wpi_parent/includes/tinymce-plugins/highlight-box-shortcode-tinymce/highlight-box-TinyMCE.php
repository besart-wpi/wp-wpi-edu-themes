<?php
/*------------Start Edting------------*/
$highlight_box_tinyMCE = new Easy_TinyMCE_Class_content;
$highlight_box_tinyMCE->id = 'highlight_box';
$highlight_box_tinyMCE->shortcode = 'hbox';
$highlight_box_tinyMCE->content = true;
$highlight_box_tinyMCE->options = array(
	array(
		'id' => 'heading',
		'field' => 'text', //text, textarea, select, post-select, tax-select
		'title' => __( 'Heading', 'wpi_parent' ),
		'des' => 'Enter an optional heading',
		'multiple' => false,
		'content' => false,
		'option' => ''
	),
	array(
		'id' => 'image',
		'field' => 'upload', //text, textarea, select, post-select, tax-select
		'title' => __( 'Image URL', 'wpi_parent' ),
		'des' => 'Upload or enter an optional image URL',
		'multiple' => false,
		'content' => false,
		'option' => ''
	),
	array(
		'id' => 'content',
		'field' => 'textarea', //text, textarea, select post-select, tax-select
		'title' => __( 'Content', 'wpi_parent' ),
		'des' => '',
		'post_type' => '',
		'std' => '',
		'multiple' => false,
		'content' => true,
		'option' => '',
	)
	
);
$highlight_box_tinyMCE->html();