// Replace all "highlight_box_shortcode_uid"

jQuery(document).ready(function($) {
    tinymce.create('tinymce.plugins.highlight_box_shortcode_uid', {
        init : function(ed, url) {
                ed.addCommand('highlight_box_shortcode_uid', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();
					
					var width = jQuery(window).width(), H = jQuery(window).height(), W = ( 720 < width ) ? 720 : width;
					W = W - 80;
					H = H - 120;
					tb_show( 'Highlight Box', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=highlight_box_shortcode_uid_template' );
                    
                });
            ed.addButton('highlight_box_shortcode_uid', {title : 'Highlight Box', cmd : 'highlight_box_shortcode_uid', image: url + '/icon.png' });
        },   
    });
    tinymce.PluginManager.add('highlight_box_shortcode_uid', tinymce.plugins.highlight_box_shortcode_uid);
});