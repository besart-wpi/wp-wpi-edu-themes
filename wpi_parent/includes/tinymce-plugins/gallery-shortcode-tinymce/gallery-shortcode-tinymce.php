<?php 

	$sample_easy_TinyMCE = new Easy_TinyMCE_Class;
	$sample_easy_TinyMCE->title = 'Insert Gallery'; // Popup Window Title
	$sample_easy_TinyMCE->js_url = trailingslashit( get_template_directory_uri() ) . 'includes/tinymce-plugins/gallery-shortcode-tinymce/tinyMCE-plugin.js';
	$sample_easy_TinyMCE->uid = 'gallery_shortcode_uid';
	$sample_easy_TinyMCE->templates = array(
		array('name'=> 'Gallery', 'url'=> trailingslashit( get_template_directory() ) .'includes/tinymce-plugins/gallery-shortcode-tinymce/gallery-TinyMCE.php')
	);
	
?>