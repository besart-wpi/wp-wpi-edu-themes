<?php
/*------------Start Edting------------*/
$tinyMCE = new Easy_TinyMCE_Class_content;
$tinyMCE->id = 'gallery';
$tinyMCE->shortcode = 'gallery';
$tinyMCE->content = false;
$tinyMCE->options = array(
	array(
		'id' => 'id',
		'field' => 'post-select', //text, textarea, select, post-select, tax-select
		'title' => __( 'Select a Photo Gallery', 'wpi_mpi' ),
		'des' => 'Select an existing Photo Gallery. Note: New photo galleries can be created from the "Photo Gallery" button in the Dashboard.',
		'post_type' => 'photo_gallery',
		'multiple' => false,
		'content' => false,
		'option' => '',
	)
	
);
$tinyMCE->html();