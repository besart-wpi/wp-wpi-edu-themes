<?php
// =================================================
//	:: Highlight Box Shortcode ::
// ------------------------------------------------

function wpi_highlight_box( $atts, $content="" ) {
    $atts = shortcode_atts( array(
 	      'heading' => '',
 	      'image' => ''
      ), $atts );
	
	// $content
	// $atts['heading']
	// $atts['image']
	 
	ob_start(); ?> 
    <div class="highlight_box<?php echo !empty($atts['image']) ? ' has_image' : ''; ?>">
    	<?php echo !empty($atts['image']) ? '<img src="' . esc_url($atts['image']) . '" class="highlight_box_thumb" alt="">' : ''; ?>
        <?php echo !empty($atts['heading']) ? '<h3>'.$atts['heading'].'</h3>' : ''; ?>
        <?php echo !empty($content) ? apply_filters('the_content', $content) : ''; ?>        
    </div>
	<?php
    return ob_get_clean();
}
add_shortcode( 'hbox', 'wpi_highlight_box' );


// Enqueue Highlight Box Styles
function wpi_highlight_box_styles(){
	wp_enqueue_style( 'wpi-highlight-box', get_template_directory_uri() . '/css/highlight-box.css'); 	
}
add_action( 'wp_enqueue_scripts', 'wpi_highlight_box_styles' );


?>