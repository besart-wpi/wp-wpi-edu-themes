<?php
// =================================================
//	:: Project Portfolio - Post Type Framework ::
// -------------------------------------------------
// Available filter hooks: 
// apply_filters( 'wpi_cpt_project_portfolio_labels', $labels)
// apply_filters( 'wpi_cpt_project_portfolio_supports', $supports)
// apply_filters( 'wpi_cpt_project_portfolio_args', $args)	//register_post_type arguments

add_action('init', 'wpi_cpt_project_portfolio');
function wpi_cpt_project_portfolio() {
	
	$labels = array(
		'name'               => _x( 'Project Portfolio', 'post type general name', 'wpi-parent' ),
		'singular_name'      => _x( 'Portfolio Item', 'post type singular name', 'wpi-parent' ),
		'menu_name'          => _x( 'Project Portfolio', 'admin menu', 'wpi-parent' ),
		'name_admin_bar'     => _x( 'Project Portfolio Item', 'add new on admin bar', 'wpi-parent' ),
		// When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new'            => _x( 'Add New Portfolio Item', 'project_portfolio', 'wpi-parent' ), 
		'add_new_item'       => __( 'Add New Portfolio Item', 'wpi-parent' ),
		'new_item'           => __( 'New Portfolio Item', 'wpi-parent' ),
		'edit_item'          => __( 'Edit Portfolio Item', 'wpi-parent' ),
		'view_item'          => __( 'View Portfolio Item', 'wpi-parent' ),
		'all_items'          => __( 'All Portfolio Items', 'wpi-parent' ),
		'search_items'       => __( 'Search Portfolio Items', 'wpi-parent' ),
		'parent_item_colon'  => __( 'Parent Portfolio Items:', 'wpi-parent' ),
		'not_found'          => __( 'No portfolio items found.', 'wpi-parent' ),
		'not_found_in_trash' => __( 'No portfolio items found in Trash.', 'wpi-parent' ),
	);
	
	$supports = array('title', 'editor', 'thumbnail');
	$args = apply_filters('wpi_cpt_project_portfolio_args', array(
		'labels' => apply_filters( 'wpi_cpt_project_portfolio_labels', $labels),
		'public' => true,
		'show_ui' => true,
		'_builtin' =>  false,
		'show_in_nav_menus' => true,
		'capability_type' => 'post',			
		'hierarchical' => false,
		'exclude_from_search' => false,
		'rewrite' => array("slug" => "project-portfolio"),
		'menu_icon' => 'dashicons-portfolio', // See: http://melchoyce.github.io/dashicons/
		'supports' =>  apply_filters( 'wpi_cpt_project_portfolio_supports', $supports)
	));
	register_post_type( 'project_portfolio' , $args );
	
	// Custom Columns Edit
	add_filter( 'manage_edit-project_portfolio_columns', 'wpi_project_portfolio_edit_columns' ) ;
	function wpi_project_portfolio_edit_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'image' => __( 'Cover Image' ),
			'title' => __( 'Portfolio Name' ),
			'date' => __( 'Date' )
		);
		return $columns;
	}
	
	// Custom Columns Manage
	add_action( 'manage_project_portfolio_posts_custom_column', 'wpi_project_portfolio_manage_columns', 10, 2 );
	function wpi_project_portfolio_manage_columns( $columns ) {
		global $post;
		switch( $columns ) {

			case 'image' :
				if ( has_post_thumbnail() ) {
					echo '<a href="'.get_edit_post_link().'">';
					the_post_thumbnail(array(50,50));
					echo '</a>';
				} else {
					echo __( 'N/A' );
				}
				break;

			default :
				break;
		}
	}
	
	// Custom Columns Style
	function wpi_project_portfolio_column_style() {
	   echo '<style type="text/css">
			   body.post-type-project_portfolio table.wp-list-table .column-image  { width: 100px; }
			   body.post-type-project_portfolio #ot-gallery-frame .gallery-settings { display: none; }
			 </style>';
	}
	
	add_action('admin_head', 'wpi_project_portfolio_column_style');
	
	// Custom Featured Image Label
	add_action( 'admin_head', 'wpi_project_portfolio_custom_featured_image_box' );
	function wpi_project_portfolio_custom_featured_image_box() {
		remove_meta_box( 'postimagediv', 'project_portfolio', 'side' );
		add_meta_box('postimagediv', __('Portfolio Item Cover Image'), 'post_thumbnail_meta_box', 'project_portfolio', 'side');
	}
	
	// Custom Meta Box
	add_action( 'admin_init', 'wpi_project_portfolio_custom_meta_boxes' );

	function wpi_project_portfolio_custom_meta_boxes() {
		$my_meta_box = array(
			'id'          => 'project_portfolio_meta_box',
			'title'       => __( 'Portfolio Item Properties', 'wpi-parent' ),
			'desc'        => '',
			'pages'       => array( 'project_portfolio' ),
			'context'     => 'normal',
			'priority'    => 'high',
			'fields'      => array(
				array(
					'label'       => __( 'Photo Gallery', 'wpi-parent' ),
					'id'          => 'portfolio_gallery_items',
					'type'        => 'gallery'
				),
			)
		);
		
		if ( function_exists( 'ot_register_meta_box' ) )
			ot_register_meta_box( $my_meta_box );
	}
	
	// Custom "Enter title here" hint
	function change_default_title_project_portfolio( $title ){
		$screen = get_current_screen();
		
		if  ( 'project_portfolio' == $screen->post_type ) {
			$title = 'Enter portfolio title';
		}
		
		return $title;
	}
	
	add_filter( 'enter_title_here', 'change_default_title_project_portfolio' );
	
} // END wpi_cpt_project_portfolio
?>