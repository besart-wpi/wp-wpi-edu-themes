<?php
// =================================================
//	:: Home Page Slideshow - Post Type Framework ::
// -------------------------------------------------
// Available filter hooks: 
// apply_filters( 'wpi_cpt_home_page_slideshow_labels', $labels)
// apply_filters( 'wpi_cpt_home_page_slideshow_supports', $supports)

add_action('init', 'wpi_cpt_home_page_slideshow');
function wpi_cpt_home_page_slideshow() {
	
	$labels = array(
		'name'               => _x( 'Home Page Slideshow', 'post type general name', 'wpi-parent' ),
		'singular_name'      => _x( 'Slide', 'post type singular name', 'wpi-parent' ),
		'menu_name'          => _x( 'Home Page Slideshow', 'admin menu', 'wpi-parent' ),
		'name_admin_bar'     => _x( 'Home Page Slideshow Slide', 'add new on admin bar', 'wpi-parent' ),
		// When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new'            => _x( 'Add New Slide', 'home_page_slideshow', 'wpi-parent' ), 
		'add_new_item'       => __( 'Add New Slide', 'wpi-parent' ),
		'new_item'           => __( 'New Slide', 'wpi-parent' ),
		'edit_item'          => __( 'Edit Slide', 'wpi-parent' ),
		'view_item'          => __( 'View Slide', 'wpi-parent' ),
		'all_items'          => __( 'All Slides', 'wpi-parent' ),
		'search_items'       => __( 'Search Slides', 'wpi-parent' ),
		'parent_item_colon'  => __( 'Parent Slides:', 'wpi-parent' ),
		'not_found'          => __( 'No slides found.', 'wpi-parent' ),
		'not_found_in_trash' => __( 'No slides found in Trash.', 'wpi-parent' ),
	);
	
	$supports = array('title','editor','thumbnail');

	$args = array(
			'labels' => apply_filters( 'wpi_cpt_home_page_slideshow_labels', $labels),
			'public' => true,
			'show_ui' => true,
			'_builtin' =>  false,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',			
			'hierarchical' => false,
			'exclude_from_search' => true,
			'rewrite' => array("slug" => "home-page-slideshow"),
			'menu_icon' => 'dashicons-format-gallery', // See: http://melchoyce.github.io/dashicons/
			'supports' =>  apply_filters( 'wpi_cpt_home_page_slideshow_supports', $supports)
	);
	register_post_type( 'home_page_slideshow' , $args );
	
	// Custom Columns Edit
	add_filter( 'manage_edit-home_page_slideshow_columns', 'wpi_home_page_slideshow_edit_columns' ) ;
	function wpi_home_page_slideshow_edit_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'image' => __( 'Slide' ),
			'title' => __( 'Slide Title' ),
			'date' => __( 'Date' )
		);
		return $columns;
	}
	
	// Custom Columns Manage
	add_action( 'manage_home_page_slideshow_posts_custom_column', 'wpi_home_page_slideshow_manage_columns', 10, 2 );
	function wpi_home_page_slideshow_manage_columns( $columns ) {
		global $post;
		switch( $columns ) {

			case 'image' :
				if ( has_post_thumbnail() ) {
					echo '<a href="'.get_edit_post_link().'">';
					the_post_thumbnail(array(50,50));
					echo '</a>';
				} else {
					echo __( 'N/A' );
				}
				break;

			default :
				break;
		}
	}
	
	// Custom Columns Style
	function wpi_home_page_slideshow_column_style() {
	   echo '<style type="text/css">
			   body.post-type-home_page_slideshow table.wp-list-table .column-image  { width: 100px; }
			 </style>';
	}
	
	add_action('admin_head', 'wpi_home_page_slideshow_column_style');
	
	// Custom Featured Image Label
	add_action( 'admin_head', 'wpi_home_page_slideshow_custom_featured_image_box' );
	function wpi_home_page_slideshow_custom_featured_image_box() {
		remove_meta_box( 'postimagediv', 'home_page_slideshow', 'side' );
		add_meta_box('postimagediv', __('Add a Slide Image'), 'post_thumbnail_meta_box', 'home_page_slideshow', 'side');
	}
	
	// Custom "Enter title here" hint
	function change_default_title_home_page_slideshow( $title ){
		$screen = get_current_screen();
		
		if  ( 'home_page_slideshow' == $screen->post_type ) {
			$title = 'Enter slide title';
		}
		
		return $title;
	}
	
	add_filter( 'enter_title_here', 'change_default_title_home_page_slideshow' );
	
}
?>