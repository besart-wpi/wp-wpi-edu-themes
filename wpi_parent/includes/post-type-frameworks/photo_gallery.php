<?php
// =================================================
//	:: Photo Gallery - Post Type Framework ::
// -------------------------------------------------
// Available filter hooks: 
// apply_filters( 'wpi_cpt_photo_gallery_labels', $labels)
// apply_filters( 'wpi_cpt_photo_gallery_supports', $supports)

add_action('init', 'wpi_cpt_photo_gallery');
function wpi_cpt_photo_gallery() {
	$shortcodeGallery = new Shortcode_Gallery;
	
	$labels = array(
		'name'               => _x( 'Photo Gallery', 'post type general name', 'wpi-parent' ),
		'singular_name'      => _x( 'Gallery', 'post type singular name', 'wpi-parent' ),
		'menu_name'          => _x( 'Photo Gallery', 'admin menu', 'wpi-parent' ),
		'name_admin_bar'     => _x( 'Photo Gallery', 'add new on admin bar', 'wpi-parent' ),
		// When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new'            => _x( 'Add New Gallery', 'photo_gallery', 'wpi-parent' ), 
		'add_new_item'       => __( 'Add New Gallery', 'wpi-parent' ),
		'new_item'           => __( 'New Gallery', 'wpi-parent' ),
		'edit_item'          => __( 'Edit Gallery', 'wpi-parent' ),
		'view_item'          => __( 'View Gallery', 'wpi-parent' ),
		'all_items'          => __( 'All Galleries', 'wpi-parent' ),
		'search_items'       => __( 'Search Galleries', 'wpi-parent' ),
		'parent_item_colon'  => __( 'Parent Galleries:', 'wpi-parent' ),
		'not_found'          => __( 'No galleries found.', 'wpi-parent' ),
		'not_found_in_trash' => __( 'No galleries found in Trash.', 'wpi-parent' ),
	);
	
	$supports = array('title', 'thumbnail');

	$args = array(
			'labels' => apply_filters( 'wpi_cpt_photo_gallery_labels', $labels),
			'public' => true,
			'show_ui' => true,
			'_builtin' =>  false,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',			
			'hierarchical' => false,
			'exclude_from_search' => true,
			'rewrite' => array("slug" => "photo-gallery"),
			'menu_icon' => 'dashicons-images-alt', // See: http://melchoyce.github.io/dashicons/
			'supports' =>  apply_filters( 'wpi_cpt_photo_gallery_supports', $supports)
	);
	register_post_type( 'photo_gallery' , $args );
	
	// Custom Columns Edit
	add_filter( 'manage_edit-photo_gallery_columns', 'wpi_photo_gallery_edit_columns' ) ;
	function wpi_photo_gallery_edit_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'image' => __( 'Cover Image' ),
			'title' => __( 'Gallery Title' ),
			'date' => __( 'Date' )
		);
		return $columns;
	}
	
	// Custom Columns Manage
	add_action( 'manage_photo_gallery_posts_custom_column', 'wpi_photo_gallery_manage_columns', 10, 2 );
	function wpi_photo_gallery_manage_columns( $columns ) {
		global $post;
		switch( $columns ) {

			case 'image' :
				if ( has_post_thumbnail() ) {
					echo '<a href="'.get_edit_post_link().'">';
					the_post_thumbnail(array(50,50));
					echo '</a>';
				} else {
					echo __( 'N/A' );
				}
				break;

			default :
				break;
		}
	}
	
	// Custom Columns Style
	function wpi_photo_gallery_column_style() {
	   echo '<style type="text/css">
			   body.post-type-photo_gallery table.wp-list-table .column-image  { width: 100px; }
			   body.post-type-photo_gallery #ot-gallery-frame .gallery-settings { display: none; }
			 </style>';
	}
	
	add_action('admin_head', 'wpi_photo_gallery_column_style');
	
	// Custom Featured Image Label
	add_action( 'admin_head', 'wpi_photo_gallery_custom_featured_image_box' );
	function wpi_photo_gallery_custom_featured_image_box() {
		remove_meta_box( 'postimagediv', 'photo_gallery', 'side' );
		add_meta_box('postimagediv', __('Gallery Cover Image'), 'post_thumbnail_meta_box', 'photo_gallery', 'side');
	}
	
	// Custom Meta Box
	add_action( 'admin_init', 'wpi_photo_gallery_custom_meta_boxes' );

	function wpi_photo_gallery_custom_meta_boxes() {
		$my_meta_box = array(
			'id'          => 'photo_gallery_meta_box',
			'title'       => __( 'Photo Gallery', 'wpi-parent' ),
			'desc'        => '',
			'pages'       => array( 'photo_gallery' ),
			'context'     => 'normal',
			'priority'    => 'high',
			'fields'      => array(
				array(
					'label'       => __( 'Items', 'wpi-parent' ),
					'id'          => 'gallery_items',
					'type'        => 'gallery',
				),
			)
		);
		
		if ( function_exists( 'ot_register_meta_box' ) )
			ot_register_meta_box( $my_meta_box );
	}
	
	// Custom "Enter title here" hint
	function change_default_title_photo_gallery( $title ){
		$screen = get_current_screen();
		
		if  ( 'photo_gallery' == $screen->post_type ) {
			$title = 'Enter photo gallery title';
		}
		
		return $title;
	}
	
	add_filter( 'enter_title_here', 'change_default_title_photo_gallery' );
	


} // END wpi_cpt_photo_gallery