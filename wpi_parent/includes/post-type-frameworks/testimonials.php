<?php
// =================================================
//	:: Testimonials - Post Type Framework ::
// -------------------------------------------------
// Available filter hooks: 
// apply_filters( 'wpi_cpt_testimonials_labels', $labels)
// apply_filters( 'wpi_cpt_testimonials_supports', $supports)

add_action('init', 'wpi_cpt_testimonials');
function wpi_cpt_testimonials() {
	
	$labels = array(
		'name'               => _x( 'Testimonials', 'post type general name', 'wpi-parent' ),
		'singular_name'      => _x( 'Testimonial', 'post type singular name', 'wpi-parent' ),
		'menu_name'          => _x( 'Testimonials', 'admin menu', 'wpi-parent' ),
		'name_admin_bar'     => _x( 'Testimonial', 'add new on admin bar', 'wpi-parent' ),
		// When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new'            => _x( 'Add New Testimonial', 'testimonials', 'wpi-parent' ), 
		'add_new_item'       => __( 'Add New Testimonial', 'wpi-parent' ),
		'new_item'           => __( 'New Testimonial', 'wpi-parent' ),
		'edit_item'          => __( 'Edit Testimonial', 'wpi-parent' ),
		'view_item'          => __( 'View Testimonial', 'wpi-parent' ),
		'all_items'          => __( 'All Testimonials', 'wpi-parent' ),
		'search_items'       => __( 'Search Testimonials', 'wpi-parent' ),
		'parent_item_colon'  => __( 'Parent Testimonials:', 'wpi-parent' ),
		'not_found'          => __( 'No testimonials found.', 'wpi-parent' ),
		'not_found_in_trash' => __( 'No testimonials found in Trash.', 'wpi-parent' ),
	);
	
	$supports = array('title', 'editor', 'thumbnail');

	$args = array(
			'labels' => apply_filters( 'wpi_cpt_testimonials_labels', $labels),
			'public' => true,
			'show_ui' => true,
			'_builtin' =>  false,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',			
			'hierarchical' => false,
			'exclude_from_search' => true,
			'rewrite' => array("slug" => "testimonials"),
			'menu_icon' => 'dashicons-format-quote', // See: http://melchoyce.github.io/dashicons/
			'supports' =>  apply_filters( 'wpi_cpt_testimonials_supports', $supports)
	);
	register_post_type( 'testimonials' , $args );
	
	// Custom Columns Edit
	add_filter( 'manage_edit-testimonials_columns', 'wpi_testimonials_edit_columns' ) ;
	function wpi_testimonials_edit_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Author Name' ),
			'the_excerpt' => __( 'Excerpt' ),
			'date' => __( 'Date' )
		);
		return $columns;
	}
	
	// Custom Columns Manage
	add_action( 'manage_testimonials_posts_custom_column', 'wpi_testimonials_manage_columns', 10, 2 );
	function wpi_testimonials_manage_columns( $columns ) {
		global $post;
		switch( $columns ) {

			case 'the_excerpt' :
				echo get_the_excerpt();
				break;

			default :
				break;
		}
	}
	
	// Custom Columns Style
	function wpi_testimonials_column_style() {
	   echo '<style type="text/css">
			   body.post-type-testimonials table.wp-list-table .column-title  { width: 25%; }
			 </style>';
	}
	
	add_action('admin_head', 'wpi_testimonials_column_style');
	
	// Custom Featured Image Label
	add_action( 'admin_head', 'wpi_testimonials_custom_featured_image_box' );
	function wpi_testimonials_custom_featured_image_box() {
		remove_meta_box( 'postimagediv', 'testimonials', 'side' );
		add_meta_box('postimagediv', __('Author Photo'), 'post_thumbnail_meta_box', 'testimonials', 'side');
	}
	
	// Custom Meta Box
	add_action( 'admin_init', 'wpi_testimonials_custom_meta_boxes' );

	function wpi_testimonials_custom_meta_boxes() {
		$my_meta_box = array(
			'id'          => 'testimonials_meta_box',
			'title'       => __( 'Testimonial Properties', 'wpi-parent' ),
			'desc'        => '',
			'pages'       => array( 'testimonials' ),
			'context'     => 'normal',
			'priority'    => 'default',
			'fields'      => array(
				array(
					'label'       => __( 'Author Title / Position', 'wpi-parent' ),
					'id'          => 'author_title',
					'type'        => 'text',
					'desc'		  => 'Optional author title / position'
				),
			)
		);
		
		if ( function_exists( 'ot_register_meta_box' ) )
			ot_register_meta_box( $my_meta_box );
	}
	
	// Custom "Enter title here" hint
	function change_default_title_testimonials( $title ){
		$screen = get_current_screen();
		
		if  ( 'testimonials' == $screen->post_type ) {
			$title = 'Enter author name';
		}
		
		return $title;
	}
	
	add_filter( 'enter_title_here', 'change_default_title_testimonials' );
	
} // END wpi_cpt_testimonials
?>