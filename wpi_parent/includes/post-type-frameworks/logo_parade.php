<?php
// =================================================
//	:: Logo Parade - Post Type Framework ::
// -------------------------------------------------
// Available filter hooks: 
// apply_filters( 'wpi_cpt_logo_parade_labels', $labels)
// apply_filters( 'wpi_cpt_logo_parade_supports', $supports)

add_action('init', 'wpi_cpt_logo_parade');
function wpi_cpt_logo_parade() {
	
	$labels = array(
		'name'               => _x( 'Logo Parade', 'post type general name', 'wpi-parent' ),
		'singular_name'      => _x( 'Logo', 'post type singular name', 'wpi-parent' ),
		'menu_name'          => _x( 'Logo Parade', 'admin menu', 'wpi-parent' ),
		'name_admin_bar'     => _x( 'Logo Parade Item', 'add new on admin bar', 'wpi-parent' ),
		// When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new'            => _x( 'Add New Logo', 'logo_parade', 'wpi-parent' ), 
		'add_new_item'       => __( 'Add New Logo', 'wpi-parent' ),
		'new_item'           => __( 'New Logo', 'wpi-parent' ),
		'edit_item'          => __( 'Edit Logo', 'wpi-parent' ),
		'view_item'          => __( 'View Logo', 'wpi-parent' ),
		'all_items'          => __( 'All Logos', 'wpi-parent' ),
		'search_items'       => __( 'Search Logos', 'wpi-parent' ),
		'parent_item_colon'  => __( 'Parent Logos:', 'wpi-parent' ),
		'not_found'          => __( 'No logos found.', 'wpi-parent' ),
		'not_found_in_trash' => __( 'No logos found in Trash.', 'wpi-parent' ),
	);
	
	$supports = array('title', 'thumbnail');

	$args = array(
			'labels' => apply_filters( 'wpi_cpt_logo_parade_labels', $labels),
			'public' => true,
			'show_ui' => true,
			'_builtin' =>  false,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',			
			'hierarchical' => false,
			'exclude_from_search' => true,
			'rewrite' => array("slug" => "logo-parade"),
			'menu_icon' => 'dashicons-editor-insertmore', // See: http://melchoyce.github.io/dashicons/
			'supports' =>  apply_filters( 'wpi_cpt_logo_parade_supports', $supports)
	);
	register_post_type( 'logo_parade' , $args );
	
	// Custom Columns Edit
	add_filter( 'manage_edit-logo_parade_columns', 'wpi_logo_parade_edit_columns' ) ;
	function wpi_logo_parade_edit_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'image' => __( 'Logo' ),
			'title' => __( 'Organization Name' ),
			'date' => __( 'Date' )
		);
		return $columns;
	}
	
	// Custom Columns Manage
	add_action( 'manage_logo_parade_posts_custom_column', 'wpi_logo_parade_manage_columns', 10, 2 );
	function wpi_logo_parade_manage_columns( $columns ) {
		global $post;
		switch( $columns ) {

			case 'image' :
				if ( has_post_thumbnail() ) {
					echo '<a href="'.get_edit_post_link().'">';
					the_post_thumbnail(array(50,50));
					echo '</a>';
				} else {
					echo __( 'N/A' );
				}
				break;

			default :
				break;
		}
	}
	
	// Custom Columns Style
	function wpi_logo_parade_column_style() {
	   echo '<style type="text/css">
			   body.post-type-logo_parade table.wp-list-table .column-image  { width: 100px; }
			 </style>';
	}
	
	add_action('admin_head', 'wpi_logo_parade_column_style');
	
	// Custom Featured Image Label
	add_action( 'admin_head', 'wpi_logo_parade_custom_featured_image_box' );
	function wpi_logo_parade_custom_featured_image_box() {
		remove_meta_box( 'postimagediv', 'logo_parade', 'side' );
		add_meta_box('postimagediv', __('Logo Image'), 'post_thumbnail_meta_box', 'logo_parade', 'normal', 'high');
	}
	
	// Custom Meta Box
	add_action( 'admin_init', 'wpi_logo_parade_custom_meta_boxes' );

	function wpi_logo_parade_custom_meta_boxes() {
		$my_meta_box = array(
			'id'          => 'logo_parade_meta_box',
			'title'       => __( 'Logo Properties', 'wpi-parent' ),
			'desc'        => '',
			'pages'       => array( 'logo_parade' ),
			'context'     => 'normal',
			'priority'    => 'default',
			'fields'      => array(
				array(
					'label'       => __( 'Logo URL', 'wpi-parent' ),
					'id'          => 'logo_url',
					'type'        => 'text',
					'desc'		  => 'Optional URL to link the logo to'
				),
			)
		);
		
		if ( function_exists( 'ot_register_meta_box' ) )
			ot_register_meta_box( $my_meta_box );
	}
	
	// Custom "Enter title here" hint
	function change_default_title_logo_parade( $title ){
		$screen = get_current_screen();
		
		if  ( 'logo_parade' == $screen->post_type ) {
			$title = 'Enter organization name';
		}
		
		return $title;
	}
	
	add_filter( 'enter_title_here', 'change_default_title_logo_parade' );
	
} // END wpi_cpt_logo_parade
?>