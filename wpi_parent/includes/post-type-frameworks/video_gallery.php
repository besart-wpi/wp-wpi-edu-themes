<?php
// =================================================
//	:: Video Gallery - Post Type Framework ::
// -------------------------------------------------
// Available filter hooks: 
// apply_filters( 'wpi_cpt_video_gallery_labels', $labels)
// apply_filters( 'wpi_cpt_video_gallery_supports', $supports)

add_action('init', 'wpi_cpt_video_gallery');
function wpi_cpt_video_gallery() {
	
	$labels = array(
		'name'               => _x( 'Video Gallery', 'post type general name', 'wpi-parent' ),
		'singular_name'      => _x( 'Gallery', 'post type singular name', 'wpi-parent' ),
		'menu_name'          => _x( 'Video Gallery', 'admin menu', 'wpi-parent' ),
		'name_admin_bar'     => _x( 'Video Gallery', 'add new on admin bar', 'wpi-parent' ),
		// When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new'            => _x( 'Add New Gallery', 'video_gallery', 'wpi-parent' ), 
		'add_new_item'       => __( 'Add New Video Gallery', 'wpi-parent' ),
		'new_item'           => __( 'New Gallery', 'wpi-parent' ),
		'edit_item'          => __( 'Edit Gallery', 'wpi-parent' ),
		'view_item'          => __( 'View Gallery', 'wpi-parent' ),
		'all_items'          => __( 'All Galleries', 'wpi-parent' ),
		'search_items'       => __( 'Search Galleries', 'wpi-parent' ),
		'parent_item_colon'  => __( 'Parent Galleries:', 'wpi-parent' ),
		'not_found'          => __( 'No galleries found.', 'wpi-parent' ),
		'not_found_in_trash' => __( 'No galleries found in Trash.', 'wpi-parent' ),
	);
	
	$supports = array('title', 'thumbnail');

	$args = array(
			'labels' => apply_filters( 'wpi_cpt_video_gallery_labels', $labels),
			'public' => true,
			'show_ui' => true,
			'_builtin' =>  false,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',			
			'hierarchical' => false,
			'exclude_from_search' => true,
			'rewrite' => array("slug" => "video-gallery"),
			'menu_icon' => 'dashicons-format-video', // See: http://melchoyce.github.io/dashicons/
			'supports' =>  apply_filters( 'wpi_cpt_video_gallery_supports', $supports)
	);
	register_post_type( 'video_gallery' , $args );
	
	// Custom Columns Edit
	add_filter( 'manage_edit-video_gallery_columns', 'wpi_video_gallery_edit_columns' ) ;
	function wpi_video_gallery_edit_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'image' => __( 'Cover Image' ),
			'title' => __( 'Gallery Title' ),
			'date' => __( 'Date' )
		);
		return $columns;
	}
	
	// Custom Columns Manage
	add_action( 'manage_video_gallery_posts_custom_column', 'wpi_video_gallery_manage_columns', 10, 2 );
	function wpi_video_gallery_manage_columns( $columns ) {
		global $post;
		switch( $columns ) {

			case 'image' :
				if ( has_post_thumbnail() ) {
					echo '<a href="'.get_edit_post_link().'">';
					the_post_thumbnail(array(50,50));
					echo '</a>';
				} else {
					echo __( 'N/A' );
				}
				break;

			default :
				break;
		}
	}
	
	// Custom Columns Style
	function wpi_video_gallery_column_style() {
	   echo '<style type="text/css">
			   body.post-type-video_gallery table.wp-list-table .column-image  { width: 100px; }
			   body.post-type-video_gallery #ot-gallery-frame .gallery-settings { display: none; }
			 </style>';
	}
	
	add_action('admin_head', 'wpi_video_gallery_column_style');
	
	// Custom Featured Image Label
	add_action( 'admin_head', 'wpi_video_gallery_custom_featured_image_box' );
	function wpi_video_gallery_custom_featured_image_box() {
		remove_meta_box( 'postimagediv', 'video_gallery', 'side' );
		add_meta_box('postimagediv', __('Video Gallery Cover Image'), 'post_thumbnail_meta_box', 'video_gallery', 'side');
	}
	
	// Custom Meta Box
	add_action( 'admin_init', 'wpi_video_gallery_custom_meta_boxes' );

	function wpi_video_gallery_custom_meta_boxes() {
		$my_meta_box = array(
			'id'          => 'video_gallery_meta_box',
			'title'       => __( 'Gallery Videos', 'wpi-parent' ),
			'desc'        => '',
			'pages'       => array( 'video_gallery' ),
			'context'     => 'normal',
			'priority'    => 'high',
			'fields'      => array(
				array(
					'label'		  => '',
					'id'		  => 'gallery_video_hints',
					'type'		  => 'textblock_titled',
					'desc'		  => 'Add each video item using the "Add New" button below. Note that it\'s not possible to enter both a YouTube URL and upload a video file for a single item. Once a value is entered in either field, the other field will no longer be available for that item.' 
				),
				array(
					'label'       => '',
					'id'          => 'gallery_videos',
					'type'        => 'list-item',
					'desc'		  => '',
					'settings'    => array( 
						array(
							'id'          => 'video_cover_image',
							'label'       => __( 'Video Cover Image', 'wpi-parent' ),
							'desc'		  => __( 'Upload a video cover image:', 'wpi-parent' ),
							'std'         => '',
							'type'        => 'upload',
							'class'		  => 'video_cover_image'
						),
						array(
							'id'          => 'video_youtube_url',
							'label'       => __( 'YouTube URL', 'wpi-parent' ),
							'desc'        => __( 'Enter the YouTube URL of the video:', 'wpi-parent' ),
							'std'         => '',
							'type'        => 'text',
							'condition'   => 'video_upload:is()'
						),
						array(
							'id'          => 'video_upload',
							'label'       => __( 'Upload a Video', 'wpi-parent' ),
							'desc'		  => __( 'Upload a supported video file (mp4/webm/ogg):', 'wpi-parent' ),
							'std'         => '',
							'type'        => 'upload',
							'condition'   => 'video_youtube_url:is()'
						)
					)
				)
			)
		);
		
		// Custom Upload Button
		add_filter( 'ot_upload_text', 'wpi_video_gallery_custom_upload_button' );
		function wpi_video_gallery_custom_upload_button() {
			return __( 'Select Media', 'wpi-parent' );
		}
		
		if ( function_exists( 'ot_register_meta_box' ) )
			ot_register_meta_box( $my_meta_box );
	}
	
	// Custom "Enter title here" hint
	function change_default_title_video_gallery( $title ){
		$screen = get_current_screen();
		
		if  ( 'video_gallery' == $screen->post_type ) {
			$title = 'Enter video gallery title';
		}
		
		return $title;
	}
	
	add_filter( 'enter_title_here', 'change_default_title_video_gallery' );
	
} // END wpi_cpt_video_gallery
?>