'use strict';
var gulp = require('gulp'),
  concat = require('gulp-concat'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps');

gulp.task('styles', function() {
  gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./'))
});

gulp.task('default', ['styles'], function() {
  gulp.watch("*.html");
  gulp.watch("src/scss/*.scss", ['styles']);
});
