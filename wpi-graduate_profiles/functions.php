<?php


	//require_once('assets/src/Google/autoload.php');
	//require_once('assets/src/Google/Client.php');
	//require_once('assets/src/Google/Service/YouTubeReporting.php');
        
        function youtube_info( $vid ){
            return;
        }
        
	require_once('assets/aq_resizer.php');

	function thumb($f,$w,$h,$c=true,$s=true,$u=true){
		$aq = aq_resize( $f,$w,$h,$c,$s,$u );
		if( $aq ){
			$img = $aq;
		}else{
			$img = $f;
		}
		return $img;
	}
        
        function theme_get_image($attachment_id, $image_size = NULL, $is_img_tag = true){
                if($attachment_id) {
                        if($image_size == NULL) {
                                $image_size = 'post-thumbnail';
                        }
                        $image = wp_get_attachment_image_src($attachment_id, $image_size);
                        if(!empty($image)){
                                $image = $image[0];
                                if($is_img_tag) {
                                        return '<img src="'.$image.'" alt="" />';
                                } else {
                                        return $image;
                                }
                        } else {
                                return '';
                        }
                } else {
                        return '';
                }
        }        
	
	add_filter( 'ot_show_pages', '__return_false' );
	add_filter( 'ot_show_new_layout', '__return_false' );
	add_filter( 'ot_theme_mode', '__return_true' );
	add_filter( 'ot_override_forced_textarea_simple', '__return_true' );
	require( trailingslashit( get_template_directory() ) . 'assets/option-tree/ot-loader.php' );
	require( trailingslashit( get_template_directory() ) . 'assets/theme-options.php' );
	require( trailingslashit( get_template_directory() ) . 'assets/meta-boxes.php' );
	

	add_theme_support('post-thumbnails', array( 'profile' ));
	add_image_size('profile-size', 325, 325, true);
	add_image_size('profile-large-photo', 665, 406, true);
	
	include( trailingslashit( get_template_directory() ) . 'assets/body_classes.php' );

	function content($num) {
		$limit = $num+1;
		$total = explode(' ', strip_tags(get_the_content()), $limit);
		if (count($total) > $num) {
			$content = explode(' ', strip_tags(get_the_content()), $limit);
			array_pop($content);
			$content = "<p>".implode(" ",$content)."...</p>";
		} else {
			$content = "<p>".get_the_content()."</p>";
		}
		echo $content;
	}

	function get_youtube_id( $link ){
		preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $link, $matches);
		return $matches[0];
	}
        
	function custom_styles() {	
		wp_register_style('stylesheet', get_stylesheet_uri(), array(), null, 'all');	
		
		wp_enqueue_style('stylesheet');
	}  
	add_action( 'wp_enqueue_scripts', 'custom_styles', 99 );

	function custom_scripts() {  

		wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr-2.6.2-respond-1.1.0.min.js', 'jquery',  null, TRUE);
		wp_register_script('fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', 'jquery',  null, TRUE);
		wp_register_script('main', get_template_directory_uri() . '/js/main.js', 'jquery',  null, TRUE);

		wp_enqueue_script('modernizr');
		wp_enqueue_script('jquery');
		wp_enqueue_script('fitvids');
		wp_enqueue_script('main');
	}
	add_action( 'wp_enqueue_scripts', 'custom_scripts', 10 );	
        
        
	add_action('init', 'profiles_post_type_init');
	function profiles_post_type_init() {
		$labels = array(
			'name' => _x('Graduate Profiles', 'post type general name'),
			'singular_name' => _x('Profile', 'post type singular name'),
			'add_new' => _x('Add New', 'Profile'),
			'add_new_item' => __('Add new profile'),
			'edit_item' => __('Edit profile'),
			'new_item' => __('New profile'),
			'view_item' => __('View profile'),
			'search_items' => __('Search profile'),
			'not_found' =>  __('No profiles found'),
			'not_found_in_trash' => __('No profiles found in Trash'), 
			'parent_item_colon' => ''
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'rewrite' => true,
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'_builtin' => false, 
			'rewrite' => array( 'slug' => 'profiles', 'with_front' => true ),
			'show_in_nav_menus' => false,
			'menu_position' => 20,
			'menu_icon' => 'dashicons-groups',
			'supports' => array(
				'title',
				'editor',
				'thumbnail'
			)
		  );
		  register_post_type('profile',$args);
		  flush_rewrite_rules();
	}	
        
	add_filter( 'manage_edit-profile_columns', 'custom_edit_profile_columns' ) ;
	function custom_edit_profile_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'image' => __( 'Photo' ),	
			'title' => __( 'Name' ),
			'department' => __( 'Department' ),
			'date' => __( 'Date' )
		);
		return $columns;  
	}  
 
	add_action( 'manage_profile_posts_custom_column', 'custom_manage_profile_columns', 10, 2 );
	function custom_manage_profile_columns( $columns ) {
		global $post;
		switch( $columns ) {
		
			case 'image' :
				if ( has_post_thumbnail() )
					the_post_thumbnail( array(50,50) );
				else 
					echo '<img src="'.get_bloginfo('template_url').'/images/no-50.gif" alt="" />';
				break;
                                
			case 'department' :
				$department = get_meta( 'prof_department' );
				if ( !empty( $department ) )
					echo $department;
				else 
					echo 'N/A';
				break;

			default :
				break;
		}
	}   
        
	
	function get_meta($key,$pid = NULL){
		global $post;
		if( $pid )
			$post_id = $pid;
		else
			$post_id = $post->ID;
		$meta = get_post_meta($post_id, $key, true);
		return $meta;
	} 
	
	
	add_action( 'admin_init', 'hide_editor' );
	function hide_editor() {
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		if( !isset( $post_id ) ) return;

		$template_file = get_post_meta($post_id, '_wp_page_template', true);
		
		if($template_file == 'front-page.php'){ // edit the template name
			remove_post_type_support('page', 'editor');
		}
	}	  

?>