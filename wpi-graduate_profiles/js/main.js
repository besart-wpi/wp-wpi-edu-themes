function getViewportWidth() {
  "use strict";

  var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  return viewportWidth;
}

jQuery(function($) {
  "use strict";

  $('.close-profile').on('click', function(event) {
    var $itemProfile = $(this).closest('.profile-full').prev('.item-profile');
    $itemProfile.trigger('click');
    $('html, body').scrollTop($itemProfile.offset().top);
    event.preventDefault();
  });


  var profiles = $('.profiles');
  var profile = $('.item-profile');
  profile.on('click', function() {

    var _this = $(this),
      win_w = getViewportWidth(),
      vid;

    if (_this.hasClass('open')) {
      profiles.empty();
      _this.removeClass('open');
    } else {
      _this.addClass('open');
      profiles.empty();
      _this.next().clone().removeAttr('style').appendTo(profiles);
      profiles.slideDown();


      profiles.find('.close').on('click', function() {
        profile.removeClass('open');
        profiles.slideUp('fast', function() {
          $(this).empty();
        });
      });
    }

    // _this.siblings('.open').trigger('click');
    _this.siblings('.open').removeClass('open');
    if (win_w <= 700) {
      var full = _this.next('.profile-full');
      if (full.is(':hidden')) {
        $('.profile-full').hide();

        $('html, body').scrollTop(_this.offset().top);

        setTimeout(function() {
          full.slideDown();
        }, 200);

        if (full.find('.video-wrap').length) {
          vid = full.find('.video-wrap').data('youtube');
          full.find('.video-wrap > div').remove();
          full.find('.video-wrap').append('<iframe width="325" height="183" src="https://www.youtube.com/embed/' + vid + '" frameborder="0" allowfullscreen></iframe>');
          full.find('.video-wrap').fitVids();
        }


      } else {
        _this.next('.profile-full').hide();
        _this.removeClass('open');

        if (full.find('.video-wrap').length) {
          full.find('.video-wrap > div').remove();
        }
      }
    } else {

      if ($('.video-wrap', profiles).length) {
        vid = profiles.find('.video-wrap').data('youtube');
        profiles.find('.video-wrap > div').remove();
        profiles.find('.video-wrap').append('<iframe width="665" height="342" src="https://www.youtube.com/embed/' + vid + '" frameborder="0" allowfullscreen></iframe>');
        profiles.find('.video-wrap').fitVids();
      }

      $('html, body').animate({
        scrollTop: profiles.offset().top - 30
      }, 600);
    }

  });

  if ($('.ie-version-8').length) {
    $('.profile-full').eq(1).after('<div class="clear"></div>');
    $('.profile-full').eq(4).after('<div class="clear"></div>');
  }
});

var j = jQuery.noConflict();

var winWidth = getViewportWidth(),
  resizeTimeout;

j(window).resize(function() {
  "use strict";

  var onResize = function() {
    re_calc();
  };

  var profile = j('.item-profile.open');
  var profiles = j('.profiles');
  profiles.empty();
  profile.removeClass('open');

  //New width
  var winNewWidth = getViewportWidth();

  // compare the new width with old one
  if (winWidth !== winNewWidth) {

    // Fire re_calc() to reset profiles layout if we cross the desktop to mobile threshold in either direction, Desktop -> Mobile or Mobile -> Desktop
    if ((winWidth > 700 && winNewWidth <= 700) || (winWidth <= 700 && winNewWidth > 700)) {
      onResize = function() {
        re_calc(true);
      };
    }

    window.clearTimeout(resizeTimeout);
    resizeTimeout = window.setTimeout(onResize, 10);
  }
  //Update the width and height
  winWidth = winNewWidth;
});

j(window).bind('load', function() {
  "use strict";
  re_calc();
});

function re_calc(reset_profiles) {
  "use strict";

  var win_w = getViewportWidth(),
    item_logo = j('.item-logo'),
    item_prof = j('.item-profile'),
    item_text = j('.item .text'),
    item_height = item_prof.outerHeight();

  if (reset_profiles === true) {
    j('.profiles').hide();
    j('.profile-full').hide();
  }

  item_logo.removeAttr('style');
  item_text.removeAttr('style');
  item_text.parent().removeAttr('style');
  if (win_w >= 701 && win_w <= 1120) {
    item_text.css({ height: item_height, lineHeight: item_height + 'px' });
    item_text.parent().css({ height: item_height });
    item_logo.css({ height: item_height, lineHeight: item_height + 'px' });
  }
}
