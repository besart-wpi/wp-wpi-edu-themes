<div id="footer">
	<div class="wrap">
		<p>
		<?php if( ot_get_option('contact_info_heading') ){ ?><strong><?php echo ot_get_option('contact_info_heading') ?></strong><br><?php } ?>
		<?php echo nl2br( ot_get_option('contact_info_text') ) ?>
		</p>
	<?php 
	$email 		= ot_get_option('connect_email');
	$website 	= ot_get_option('connect_website');
	$facebook 	= ot_get_option('connect_facebook');
	if( $email || $website || $facebook ){?>	
		<div class="socials">
			<span>Connect with us!</span>
			<?php if( $email ){ ?><a href="mailto:<?php echo antispambot( $email ) ?>" class="social-1">email</a><?php } ?>
			<?php if( $website ){ ?><a href="<?php echo $website ?>" class="social-2" target="_blank">website</a><?php } ?>
			<?php if( $facebook ){ ?><a href="<?php echo $facebook ?>" class="social-3" target="_blank">facebook</a><?php } ?>
		</div>
	<?php } ?>	
	</div>
</div>		
<?php wp_footer(); ?>
</body>
</html>