<?php get_header(); ?>

<?php the_post() ?>

    <div id="black">
            <div class="wrap">
                    <div class="inner">
                            <h2>Nothing Found</h2>
                    </div>
            </div>
    </div>
    <div id="content">
            <div class="wrap">  
			<p>Apologies, but no results were found for the request.</p>
            </div>
    </div>

<?php get_footer(); ?>
