<!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"  />
<title><?php if (is_home () ) { bloginfo('name'); echo " - "; bloginfo('description'); 
} elseif (is_category() ) {single_cat_title(); echo " - "; bloginfo('name');
} elseif (is_single() || is_page() ) {single_post_title(); echo " - "; bloginfo('name');
} elseif (is_search() ) {bloginfo('name'); echo " search results: "; echo wp_specialchars($s);
} else { wp_title('',true); }?></title>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<script src="https://use.typekit.net/olf1zdq.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/668448/7313152/css/fonts.css" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="header">
	<div class="wrap">
                <?php if( ot_get_option('gp_logo_url') ){ ?><h3 class="tagline"><a href="<?php echo ot_get_option('gp_logo_url') ?>" target="_blank" title="Graduate Programs">Worcester Polytechnic Institute <strong>Graduate Programs</strong></a></h3><?php } ?>
		<?php if( ot_get_option('wpi_logo_url') ){ ?><h1 class="logo"><a href="<?php echo ot_get_option('wpi_logo_url') ?>" target="_blank"  title="Worcester Polytechnic Institute">Worcester Polytechnic Institute</a></h1><?php } ?>
	</div>
</div>			 