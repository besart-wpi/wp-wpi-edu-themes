<section class="section-content-with-sidebar">
<div class="container container-smaller">
	
	<div class="content-main the-content">
		<?php the_title('<h2>', '</h2>'); ?>
		<p class="meta"><i class="icon icon-calendar"></i><?php echo get_the_date(); ?></p>
		<?php the_content(); ?>
	</div>
	
	<aside class="sidebar">
		<?php dynamic_sidebar('single_story'); ?>	
	</aside>
	
</div>
</section>