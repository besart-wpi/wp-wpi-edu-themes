<?php
$has_hero = false;
if(have_rows('flexible_content')): while (have_rows('flexible_content')) : the_row();	
	$row_layout = get_row_layout();
	if(get_row_index() == 1 && ($row_layout == 'video_hero' || $row_layout == 'interior_hero')):
		get_template_part( 'template-parts/flexible-contents/' . $row_layout );
		$has_hero = true;
	endif;
endwhile; endif;

if(!$has_hero):
?>

	<section class="section-hero section-hero-interior">
	<div class="section-hero-inner">
	<div class="container container-full">
	<div class="container-inner">
		<div class="hero-border">
			<div class="tlt"></div>
			<div class="tll"><div class="inner"></div></div>
			<div class="trt"></div>
			<div class="trr"><div class="inner"></div></div>
			
			<div class="blb"></div>
			<div class="bll"><div class="inner"></div></div>
			<div class="brb"></div>
			<div class="brr"><div class="inner"></div></div>
		</div>
		<div class="hero-content hero-content-with-dot">
			<h1><?php the_title(); ?></h1>		
		</div>		
		<div class="vertical-dot"></div>
		<div class="hero-bottom-text">
			<p><?php _e('The Campaign for Worcester Polytechnic Institute'); ?></p>
		</div>
	</div>
	</div>
	</div>
	</section>

<?php
endif;