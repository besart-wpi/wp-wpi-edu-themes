<?php
$section_classes = 'fg-color-'.get_sub_field('foreground_color');
$bg_color = get_sub_field('background_color');

if(!empty(get_sub_field('call_to_action_blocks')) && $bg_color == 'none'):
	$bg_color = 'gray';
endif;

$section_classes .= ' bg-color-'.$bg_color;
$section_classes .= ' top-bg-'.$bg_color;
$section_classes .= ' bottom-bg-'.$bg_color;

$bg_image = get_sub_field('background_image');
if($bg_image):
	$section_classes .= ' section-with-bg-image';
endif;
?>
<section class="section-grid-block <?php echo $section_classes; ?>">
	
	<?php
	if($bg_image):
		echo '<div class="section-bg">'.theme_get_img($bg_image, 'full-width', array('data-object-fit'=>'cover')).'</div>';
	endif;
	?>

	<div class="container">
		<div class="grid-block-row image-align-<?php the_sub_field('image_alignment'); ?>">
			<figure class="grid-block-image">
				<?php
				echo theme_get_img(get_sub_field('image'), 'grid-block');
				$video_url = get_sub_field('youtube_url');
				if($video_url):
					echo '<a href="'.$video_url.'" class="btn-play" data-fancybox><i class="icon icon-play"></i><span class="sr-only">Play Video</span></a>';
				endif;
				?>
			</figure>
			<div class="grid-block-content font-size-medium">
			<div class="grid-block-content-inner bg-color-<?php the_sub_field('content_background_color'); ?>">
			<div class="grid-block-content-inner-inner">
				<?php
				the_theme_output(get_sub_field('heading'), '<h2>', '</h2>');
				the_sub_field('introduction');
				$button = get_sub_field('button');
				$button_icon = get_sub_field('button_icon');
				$button_style = get_sub_field('button_style');
				if(!empty($button)):
					$btn_args = array(
						'url' 		=> $button['url'],
						'label'		=> $button['title'],
						'new_tab'	=> $button['target'],
						'icon'		=> get_sub_field('button_icon'),				
						'style'		=> get_sub_field('button_style'),				
					);
					the_theme_button($btn_args);	
				endif;
				?>
			</div>
			</div>
			</div>
		</div>
		
		<?php if(have_rows('call_to_action_blocks')): ?>
		<div class="cta-blocks-row">
			<?php while(have_rows('call_to_action_blocks')): the_row();				

				$link_url = get_sub_field('link_url');
				$new_tab = get_sub_field('open_in_a_new_tab');
				if($link_url):
					$before = '<a class="cta-block-inner" href="'.$link_url.'" '.open_new_tab($new_tab, false).'>';
					$after = '</a>';
				else:
					$before = '<div class="cta-block-inner">';
					$after = '</div>';
				endif;
				
				echo '<div class="cta-block">'.$before;
				the_theme_output(get_sub_field('heading'), '<h5 class="font-size-large">', '</h5>');
				echo wpautop(get_sub_field('text'));
				echo $after.'</div>';
				?>
				
				
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</section>