<?php
$section_classes = 'fg-color-'.get_sub_field('foreground_color');
$section_classes .= ' bg-color-'.get_sub_field('background_color');
$section_classes .= ' overlay-color-'.get_sub_field('overlay_color');
$bg_image = get_sub_field('background_image');
if($bg_image):
	$section_classes .= ' section-with-bg-image';
endif;
?>
<section class="section-hero section-hero-interior <?php echo $section_classes; ?>">
<div class="section-hero-inner">
	<?php
	if($bg_image):
		echo '<div class="section-bg">'.theme_get_img($bg_image, 'full-width', array('data-object-fit'=>'cover')).'</div>';
	endif;
	?>
	
	<div class="container container-full">
	<div class="container-inner">
		<div class="hero-border">
			<div class="tlt"></div>
			<div class="tll"><div class="inner"></div></div>
			<div class="trt"></div>
			<div class="trr"><div class="inner"></div></div>
			
			<div class="blb"></div>
			<div class="bll"><div class="inner"></div></div>
			<div class="brb"></div>
			<div class="brr"><div class="inner"></div></div>
		</div>
		<div class="hero-content">
			<?php the_theme_output(get_sub_field('heading'), '<h1>', '</h1>'); ?>
		</div>	
		
		
		<div class="hero-bottom-text">
			<p><?php _e('The Campaign for Worcester Polytechnic Institute'); ?></p>
		</div>
	</div>
	</div>
</div>

<?php if(have_rows('buttons')): ?>
	<div class="hero-buttons-wrap">
		<?php
		while(have_rows('buttons')): the_row();
			$btn_args = array(
				'url' 		=> get_sub_field('url'),
				'label'		=> get_sub_field('label'),
				'new_tab'	=> get_sub_field('open_in_new_tab'),
				'icon'		=> get_sub_field('icon'),				
				// 'before'	=> '<div class="btn-col">',				
				// 'after'		=> '</div>',				
			);
			the_theme_button($btn_args);
		endwhile; ?>
	</div>
<?php endif; ?>

</section>
