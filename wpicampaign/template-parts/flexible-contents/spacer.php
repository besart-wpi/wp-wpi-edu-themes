<?php
$graphic = get_sub_field('graphic');
$bg_color = get_sub_field('background_color');

$section_classes = ' bg-color-'.$bg_color;
$section_classes .= ' bottom-bg-'.$bg_color;

$style = '';
if(get_sub_field('spacer_height_mode') == 'custom'):
	$section_classes .= ' spacer-height-custom';
	$style = 'height: '.get_sub_field('spacer_height').'px;';
endif;
?>

<div class="spacer spacer-<?php echo $graphic; ?> <?php echo $section_classes; ?>">
<div class="container container-full container-no-padding" style="<?php echo $style; ?>">
	<div class="spacer-image">
	<?php
	switch ($graphic):
		case 'dots-1':
			echo '<img src="'.get_template_directory_uri().'/assets/images/spacer-1.svg" alt="Spacer Image" class="svg-inject" />';
			break;
		case 'dots-2':
			echo '<img src="'.get_template_directory_uri().'/assets/images/spacer-2.svg" class="desktop-only tablet-only svg-inject" alt="Spacer Image" />';
			echo '<img src="'.get_template_directory_uri().'/assets/images/spacer-2-phone.svg" class="phone-only svg-inject" alt="Spacer Image" />';
			break;
		case 'dots-3':
			echo '<img src="'.get_template_directory_uri().'/assets/images/spacer-3.svg" class="desktop-only tablet-only svg-inject" alt="Spacer Image" />';
			echo '<img src="'.get_template_directory_uri().'/assets/images/spacer-3-phone.svg" class="phone-only svg-inject" alt="Spacer Image" />';
			break;
	endswitch;
	?>
	</div>
</div>
</div>