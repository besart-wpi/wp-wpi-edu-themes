<section class="section-hero section-hero-video hero-with-bg">

	<div class="container container-full">
	<div class="container-inner">
		<div class="hero-border">
			<div class="tlt"></div>
			<div class="tll"><div class="inner"></div></div>
			<div class="trt"></div>
			<div class="trr"><div class="inner"></div></div>
			
			<div class="blb"></div>
			<div class="bll"><div class="inner"></div></div>
			<div class="brb"></div>
			<div class="brr"><div class="inner"></div></div>
		</div>
		<div class="hero-content">
			<?php
			the_theme_output(get_sub_field('heading'), '<h1>', '</h1>');
			$video_url = get_sub_field('youtube_video_url');
			if($video_url):
				echo '<a href="'.$video_url.'" class="btn-play" data-fancybox><i class="icon icon-play"></i><span class="sr-only">Play Video</span></a>';
			endif;
			?>			
		</div>
		<div class="hero-bottom-text">
			<p><?php _e('The Campaign for Worcester Polytechnic Institute'); ?></p>
		</div>
	</div>
	</div>

	<div class="hero-background">
		<video class="desktop-only tablet-only" data-object-fit="cover" muted loop autoplay>
			
			<?php
			$mp4 = get_sub_field('background_video_mp4');
			$webm = get_sub_field('background_video_webm');
			$ogv = get_sub_field('background_video_ogg');
			?>
			<?php if($webm): ?>
				<source src="<?php echo $webm; ?>" type="video/webm">
			<?php endif; ?>
			<?php if($mp4): ?>
				<source src="<?php echo $mp4; ?>" type="video/mp4">
			<?php endif; ?>
			<?php if($ogv): ?>
				<source src="<?php echo $ogv; ?>" type="video/ogg">
			<?php endif; ?>
			<?php _e('Your browser does not support the video tag.'); ?>
		</video>
		<?php echo theme_get_img(get_sub_field('background_image_fallback_for_mobile'), 'full-width', array('class'=>'phone-only', 'data-object-fit'=>'cover')); ?>
	</div>
</section>
