<?php
$section_classes = 'fg-color-'.get_sub_field('foreground_color');
$section_classes .= ' bg-color-'.get_sub_field('background_color');
$bg_image = get_sub_field('background_image');
if($bg_image):
	$section_classes .= ' section-with-bg-image';
endif;
?>
<section class="section-text-box <?php echo $section_classes; ?>">
<?php
if($bg_image):
	echo '<div class="section-bg">'.theme_get_img($bg_image, 'full-width', array('data-object-fit'=>'cover')).'</div>';
endif;
?>
<div class="container container-larger">	
<div class="text-box-inner">
	
	<?php
	$heading = trim(get_sub_field('heading'));
	$tb_class = ($heading) ? 'has-heading font-size-large font-size-mobile-small' : 'font-minion-large';
	?>
	
	<div class="text-box-content <?php echo $tb_class; ?>">
		<?php
		the_theme_output($heading, '<h2>', '</h2>');
		the_sub_field('content');
		if(have_rows('buttons')):
			echo '<div class="buttons-wrap">';
			while(have_rows('buttons')): the_row();
				$btn_args = array(
					'url' 		=> get_sub_field('url'),
					'label'		=> get_sub_field('label'),
					'new_tab'	=> get_sub_field('open_in_new_tab'),
					'icon'		=> get_sub_field('icon'),					
					'class'		=> 'btn-outline',					
				);
				the_theme_button($btn_args);				
			endwhile;
			echo '</div>';
		endif;
		?>
	</div>
	
</div>
</div>
</section>