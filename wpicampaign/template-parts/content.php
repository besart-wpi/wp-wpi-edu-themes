<?php if(theme_is_has_content()): ?>
	<section class="section-full-width-content sc-type-none">
	<div class="container container-smaller">
		
		<div class="fwc-content the-content">
			<?php the_content(); ?>
		</div>
		
	</div>
	</section>
<?php endif;