<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'button-block-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'block-quote-with-author';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$quote = get_field('quote') ?: '';
$author_name = get_field('author_name') ?: '';
$author_photo = get_field('author_photo') ?: '';
$blockquote_classes = '';
if($author_photo):
	$blockquote_classes .= ' blockquote-with-photo';
endif;
?>
<blockquote id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className).$blockquote_classes; ?>">
	<?php if($author_photo): ?>
		<div class="quote-author-photo">
			<?php echo theme_get_img($author_photo, 'headshot'); ?>
		</div>
	<?php endif; ?>
	<div class="blockquote-inner">
	<?php
	echo $quote;
	if($author_name):
		echo '<cite>'.$author_name.'</cite>';
	endif;
	?>
	</div>
</blockquote>