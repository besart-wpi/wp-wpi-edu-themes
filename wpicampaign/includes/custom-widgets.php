<?php
class Sidebar_Callout_Widget extends WP_Widget {

	
	public function __construct() {
		$widget_ops = array(
		'classname' => 'widget-sidebar-callout',		
		);
		parent::__construct( 'callout', 'Sidebar Callout', $widget_ops );
	}

	/**
	* Outputs the content of the widget
	*
	* @param array $args
	* @param array $instance
	*/
	public function widget( $args, $instance ) {
		
		
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		
		// outputs the content of the widget
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		// widget ID with prefix for use in ACF API functions
		$widget_id = 'widget_' . $args['widget_id'];
		
		$text = get_field( 'text', $widget_id );	
		$icon = get_field( 'icon', $widget_id );	
		$link_url = get_field( 'link_url', $widget_id );	
		$foreground_color = get_field( 'foreground_color', $widget_id );	
		$background_color = get_field( 'background_color', $widget_id );	
		$background_image = get_field( 'background_image', $widget_id );
		
		if($link_url) {
			echo $args['before_widget'];
			
			$wi_classes = 'fg-color-'.$foreground_color;
			$wi_classes .= ' bg-color-'.$background_color;
			if($background_image) {
				$wi_classes .= ' widget-with-bg-image section-with-bg-image';
			}
			$btn_text = ($text) ? $text : __('Learn More');
			if($icon) {
				$btn_text = '<i class="icon icon-'.$icon.'"></i>'.$btn_text;
			}
		?>
			<div class="callout-widget-inner <?php echo $wi_classes; ?>">
				<?php
				if($background_image) {
					echo '<div class="widget-bg section-bg">'.theme_get_img($background_image, 'secondary-content').'</div>';
				}
				?>
				<div class="widget-content">
					<a href="<?php echo $link_url; ?>" class="widget-button"><?php echo $btn_text; ?></a>
				</div>
			</div>
		<?php
			echo $args['after_widget'];
		}
		
	}
	
	
	public function form( $instance ) {
		
	}

}

class Share_Links_Widget extends WP_Widget {

	
	public function __construct() {
		$widget_ops = array(
		'classname' => 'widget-share-links',		
		);
		parent::__construct( 'share-links-widget', 'Share Links', $widget_ops );
	}

	/**
	* Outputs the content of the widget
	*
	* @param array $args
	* @param array $instance
	*/
	public function widget( $args, $instance ) {
		
		
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );		
		
		echo $args['before_widget'];
		if($title) {
			echo $args['before_title'].$title.$args['after_title'];
		}
		?>
			<ul>
				<li><a href="#" class="share-facebook"><i class="icon icon-facebook"></i>Facebook</a></li>
				<li><a href="#" class="share-twitter"><i class="icon icon-twitter"></i>Twitter</a></li>
				<li><a href="#" class="share-link"><i class="icon icon-share"></i>Copy Link
					<div class="copied">
						<span class="note">Copied</span><span class="url share-url-inline sr-only"></span>
					</div>
					</a>
				</li>
			</ul>
		<?php
		echo $args['after_widget'];
		
	}
	
	public function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$new_instance         = wp_parse_args(
			(array) $new_instance,
			array(
				'title'    => '',				
			)
		);
		$instance['title']    = sanitize_text_field( $new_instance['title'] );	

		return $instance;
	}
	
	
	public function form( $instance ) {
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title'    => '',				
			)
		);
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" /></p>
		<?php
	}

}



function theme_register_widget() {
	register_widget( 'Sidebar_Callout_Widget' );
	register_widget( 'Share_Links_Widget' );
}
add_action( 'widgets_init', 'theme_register_widget' );