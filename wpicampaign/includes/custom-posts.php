<?php	
//	:: Custom Post Type :: Story ::
// -------------------------------------------------
	add_action('init', 'theme_register_story_post');
	function theme_register_story_post() {
		$labels = array(				
				'singular_name' => _x('Story', 'post type singular name'),
				'add_new' => _x('Add New', 'story'),
				'add_new_item' => __('Add Story'),
				'edit_item' => __('Edit Story'),
				'new_item' => __('New Story'),				
				'view_item' => __('View Story'),
				'all_items' => __('All Stories'),
				'search_items' => __('Search Stories'),
				'not_found' => __('No Stories found'),
				'not_found_in_trash' => __('No Stories found in Trash'), 
				'menu_name' => 'Stories',
				'name' => _x('Stories', 'post type general name'),
				'parent_item_colon' => '',				
		);
		
		$args = array(
				'labels' => $labels,
				'public' => true,
				'show_ui' => true,
				'_builtin' =>  false,
				'show_in_nav_menus' => true,
				'capability_type' => 'post',			
				'hierarchical' => true,
				'exclude_from_search' => false,
				'has_archive' => false,
				'rewrite' => array("slug" => "story"),
				'menu_icon' => 'dashicons-testimonial',
				'show_in_rest' => true,
				'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes')
		);
		register_post_type( 'story' , $args );		
	}