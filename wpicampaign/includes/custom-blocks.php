<?php
function theme_custom_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'wpi-campaign-blocks',
                'title' => __( 'WPI Campaign Microsite' ),
            ),
        )
    );
}
add_filter( 'block_categories', 'theme_custom_block_categories', 10, 2 );


function theme_register_acf_block_types() {	
	
	acf_register_block_type(array(
        'name'              => 'Introduction',
        'title'             => __('Introduction'),
        'description'       => __('Introduction Block'),
        'render_template'   => 'template-parts/blocks/introduction.php',
        'category'          => 'wpi-campaign-blocks',
        'icon'              => 'text',
        'mode'              => 'edit',
        'keywords'          => array( 'introduction', 'text', 'wpi', 'campaign', 'microsite' ),
    ));
	
	acf_register_block_type(array(
        'name'              => 'Quote with Author Name',
        'title'             => __('Quote with Author Name'),
        'description'       => __('Quote with Author Name Block'),
        'render_template'   => 'template-parts/blocks/quote-with-author-name.php',
        'category'          => 'wpi-campaign-blocks',
        'icon'              => 'format-quote',
        'mode'              => 'edit',
        'keywords'          => array( 'quote', 'author', 'wpi', 'campaign', 'microsite' ),
    ));	
 
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'theme_register_acf_block_types');
}
