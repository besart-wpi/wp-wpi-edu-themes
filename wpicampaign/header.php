<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />	
	<?php wp_head(); ?>	
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
<div id="page-inner">
<a href="#main" class="skip-nav-btn button btn-no-arrow"><?php _e('Skip Navigation'); ?></a>
		
	<header id="header" role="banner">
	<div class="container container-full">	
		
		<nav id="nav">
			<?php
			$args = array(
				'container'		=> '',
				'theme_location'=> 'primary_menu',
				'depth'			=> 1,
				'fallback_cb' 	=> false,					
			);
			wp_nav_menu($args);				
			?>		
			<?php if($btn_url = get_field('header_give_now_button_url', 'options')): ?>
				<a href="<?php echo $btn_url; ?>" class="header-corner-btn"><i class="icon icon-gift"></i><?php _e('Give Now'); ?></a>
			<?php endif; ?>
		</nav>
		
		<a href="#" id="nav-toggle" class="nav-toggle"><i class="icon icon-menu"></i>Menu</a>
		
	</div>
	</header>

	<div id="main">