</div><!-- end #main -->	
	
<footer id="footer" role="contentinfo">
	<div class="footer-top">
	<div class="container">	
		<ul class="footer-menu">
		<?php
		$args = array(
			'container'		=> '',
			'theme_location'=> 'footer_menu',
			'depth'			=> 1,
			'fallback_cb' 	=> false,
			'items_wrap' 	=> '%3$s'
		);
		wp_nav_menu($args);	
		?>
		<?php if($btn_url = get_field('footer_give_now_button_url', 'options')): ?>
			<li><a href="<?php echo $btn_url; ?>"><i class="icon icon-gift"></i><?php _e('Give Now'); ?></a></li>
		<?php endif; ?>	
		</ul>
	</div>
	</div>
	
	
	<div class="footer-bottom">
	<div class="container">
		<div class="footer-bottom-row">
			<div class="footer-bottom-left">
				<a href="https://wpi.edu"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/wpi-logo.png" alt="WPI Logo" width="179" /></a>
			</div>
			<div class="footer-bottom-center">
				<?php
				echo wpautop(get_field('address', 'options'));
				$phone_number = trim(get_field('phone_number', 'options'));
				if($phone_number):
					echo '<p>Ph: <a href="'.esc_url('tel:'.theme_phone_number_cleanup($phone_number)).'">'.$phone_number.'</a></p>';
				endif;
				?>				
			</div>
			<div class="footer-bottom-right">
				<div class="social-links">
					<?php if($link_url = get_field('facebook_url', 'options')): ?>
						<a href="<?php echo $link_url; ?>" target="_blank" rel="noopener"><i class="icon icon-facebook"></i><span class="sr-only">Facebook</span></a>
					<?php endif; ?>
					<?php if($link_url = get_field('twitter_url', 'options')): ?>
						<a href="<?php echo $link_url; ?>" target="_blank" rel="noopener"><i class="icon icon-twitter"></i><span class="sr-only">Twitter</span></a>
					<?php endif; ?>
					<?php if($link_url = get_field('youtube_url', 'options')): ?>
						<a href="<?php echo $link_url; ?>" target="_blank" rel="noopener"><i class="icon icon-youtube"></i><span class="sr-only">YouTube</span></a>
					<?php endif; ?>
				</div>
			</div>
		</div>		
	</div>
	</div>
	
	<div class="footer-copyright">
	<div class="container">
		<p>&copy; <?php echo date('Y'); ?> WPI Beyond These Towers</p>
		<?php
		$args = array(
			'container'		=> '',
			'theme_location'=> 'copyright_menu',
			'depth'			=> 1,
			'fallback_cb' 	=> false					
		);
		wp_nav_menu($args);	
		?>	
	</div>
	</div>
	
</footer>

<div id="mobile-menu">
<div class="mobile-menu-inner">
	<a href="#" class="nav-toggle"><i class="icon icon-close"></i>Close</a>
	<?php
	$args = array(
		'container'		=> '',
		'theme_location'=> 'primary_menu',
		'depth'			=> 2,
		'fallback_cb' 	=> false					
	);
	wp_nav_menu($args);	
	?>
	<?php if($btn_url = get_field('header_give_now_button_url', 'options')): ?>
		<a href="<?php echo $btn_url; ?>" class="button"><i class="icon icon-gift"></i><?php _e('Give Now'); ?></a>
	<?php endif; ?>	
</div>
</div>


</div><!-- end #page-inner -->
</div><!-- end #page -->

<?php wp_footer(); ?>
</body>
</html>