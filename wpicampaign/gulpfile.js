'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var beep = require('beepbeep');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();


//script paths
var jsFiles = './assets/js/concat/*.js',
    jsDest = './assets/js';


// Sass Function
function sassFunc() {
  return gulp.src('./assets/sass/style.scss')	
    .pipe(sourcemaps.init())
	.pipe(sass({
			outputStyle: 'compact'
			//outputStyle: 'compressed'
		}).on('error', function(err){
		sass.logError.bind(this)(err);
		beep();
	}))
	.pipe(sourcemaps.init({loadMaps: true}))
	.pipe(autoprefixer())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./assets/css'))
	.pipe(browserSync.stream());
}	
	
// Sass Task
gulp.task('sass', sassFunc);

gulp.task('scripts', function() {
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});


// Watch Task
gulp.task('watch', function () {
	
	
	browserSync.init({
        proxy: "localhost/wpi"
    });
	
	sassFunc();
	
	gulp.watch('./assets/sass/**/*.scss', {interval: 1000}, gulp.series('sass'));
	gulp.watch('./assets/js/concat/*.js', {interval: 1000}, gulp.series('scripts'));
});


// Default Task
gulp.task('default', gulp.series('watch'));