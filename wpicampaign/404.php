<?php get_header(); ?>

<section class="section-hero section-hero-interior">
<div class="section-hero-inner">
	<div class="container container-full">
	<div class="container-inner">
		<div class="hero-border">
			<div class="tlt"></div>
			<div class="tll"><div class="inner"></div></div>
			<div class="trt"></div>
			<div class="trr"><div class="inner"></div></div>
			
			<div class="blb"></div>
			<div class="bll"><div class="inner"></div></div>
			<div class="brb"></div>
			<div class="brr"><div class="inner"></div></div>
		</div>
		<div class="hero-content hero-content-up">
			<?php
			$heading = trim(get_field('404_heading', 'options'));
			if(!$heading):
				$heading = __('404 Not Found');
			endif;
			?>
			<h1><?php echo $heading; ?></h1>
			<div class="hero-content-text font-minion-large">
				<?php the_field('404_content', 'options'); ?>
			</div>
		</div>	
		
		<div class="hero-bottom-text">
			<p><?php _e('The Campaign for Worcester Polytechnic Institute'); ?></p>
		</div>
	</div>
	</div>
</div>
</section>

<?php get_footer();