<?php get_header(); ?>
<section id="content" role="main">
	<div class="container-wrapper cf">
		<div class="container cf">
			<div class="container-content cf">
                <header class="header">
                <?php the_post(); ?>
                <h1 class="entry-title author"><?php _e( 'Author Archives', 'blankslate' ); ?>: <?php the_author_link(); ?></h1>
                <?php if ( '' != get_the_author_meta('user_description') ) echo apply_filters('archive_meta', '<div class="archive-meta">' . get_the_author_meta('user_description') . '</div>'); ?>
                <?php rewind_posts(); ?>
                </header>
                <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part('entry'); ?>
                <?php endwhile; ?>
                <?php get_template_part('nav', 'below'); ?>
			</div>
		</div>
	</div>                
</section>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>