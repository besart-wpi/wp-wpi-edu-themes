<?php get_header(); ?>
<section id="content" role="main">
    <div class="container-wrapper cf">
        <div class="container cf">
        	<div class="container-content cf">
                <header class="header">
                <h1 class="entry-title"><?php _e( 'Tag Archives: ', 'blankslate' ); ?><span><?php single_tag_title(); ?></span></h1>
                </header>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php get_template_part('entry'); ?>
                <?php endwhile; endif; ?>
                <?php get_template_part('nav', 'below'); ?>
        	</div>
        </div>
    </div>
</section>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>