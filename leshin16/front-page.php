<?php get_header(); ?>

<?php 
$args=array(
  'order'=>'ASC',
  'orderby'=> 'menu_order',
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 100,
  'post_parent' => get_option('page_on_front') // Only get direct children of the front page
);
query_posts($args);
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'partials/loop-section-block' ); ?>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
