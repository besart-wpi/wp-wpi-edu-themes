<?php get_header(); ?>

<div id="interior_splash">
<?php 
$args=array(
  'order'=>'ASC',
  'orderby'=> 'menu_order',
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 1, // Only get first block, intro!
  'post_parent' => get_option('page_on_front') // Only get direct children of the front page
);
$header_block = new WP_Query($args);
if ( $header_block->have_posts() ) : while ( $header_block->have_posts() ) : $header_block->the_post(); 
	get_template_part( 'partials/loop-section-block' );
endwhile; endif;
wp_reset_postdata();
?>
</div>

<section id="content" role="main" class="block cf">
	<div class="container-wrapper cf">
		<div class="container cf">
			<div class="container-content cf">
				<?php if ( have_posts() ) : ?>
                    <header class="header">
                    	<h1 class="entry-title"><?php printf( __( 'Search Results for: %s', 'blankslate' ), get_search_query() ); ?></h1>
                    </header>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part('entry'); ?>
                    <?php endwhile; ?>
                        <?php get_template_part('nav', 'below'); ?>
                    <?php else : ?>
                    <article id="post-0" class="post no-results not-found">
                        <header class="header">
                            <h2 class="entry-title"><?php _e( 'Nothing Found', 'blankslate' ); ?></h2>
                        </header>
                        <section class="entry-content">
                            <p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'blankslate' ); ?></p>
                            <?php get_search_form(); ?>
                        </section>
                    </article>
                <?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>