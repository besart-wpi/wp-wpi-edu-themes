jQuery(document).ready(function($){

	// determine padding for anchor scrolling bases on the current site
	var headerPadding = 50;
	if (jQuery('body').hasClass('strategicplan')) {
		headerPadding = 170;
	}

	// Smooth scroll anchor links on same page
	jQuery('a').each( function() {
		var $this = jQuery(this),
			target = this.hash;
		jQuery(this).click(function (e) {
			if( $(target).length > 0 ) { // The anchor target is on this page, proceed
				e.preventDefault();
				jQuery(target).animatescroll({scrollSpeed:1000, padding: headerPadding});
			}
		});
	});

}); // END document ready