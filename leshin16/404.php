<?php get_header(); ?>

<div id="interior_splash">
<?php 
$args=array(
  'order'=>'ASC',
  'orderby'=> 'menu_order',
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 1, // Only get first block, intro!
  'post_parent' => get_option('page_on_front') // Only get direct children of the front page
);
$header_block = new WP_Query($args);
if ( $header_block->have_posts() ) : while ( $header_block->have_posts() ) : $header_block->the_post(); 
	get_template_part( 'partials/loop-section-block' );
endwhile; endif;
wp_reset_postdata();
?>
</div>

<section class="block cf" id="content">   
	<div class="container-wrapper cf">
		<div class="container cf">
			<div class="container-content cf">
                
                    <h1 class="main-title"><?php _e( 'Not Found', 'blankslate' ); ?></h1>
                    <h2 class="block-headline">404</h2>
                    <div class="left-column">
                        <p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'blankslate' ); ?></p>
                        <?php get_search_form(); ?>
                    </div>

			</div>
		</div>
	</div>
</section>

<?php // get_sidebar(); ?>
<?php get_footer(); ?>