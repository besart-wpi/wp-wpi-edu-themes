<?php
global $interior; // set in header.php
$title = get_the_title();
$theslug = sanitize_title($title)
?>
<section class="block cf<?php echo get_field('section_class') ? ' ' . get_field('section_class') : ''; ?>" <?php if(!$interior): ?>id="<?php echo $theslug; ?>"<?php endif; ?>>
	<div class="arrow-l"></div>
	<div class="arrow-m"></div>
	<div class="arrow-r"></div>    
	<div class="container-wrapper cf">
		<div class="container cf">
			<div class="container-content cf">
				<h1 class="main-title"><?php the_title(); ?></h1>
				<h2 class="block-headline"><?php the_field('block-headline'); ?></h2>
				<div class="left-column">
					<?php the_content(); ?>
				</div>
				<?php if( get_field('right-column') ): ?>
				<div class="right-column">
					<?php the_field('right-column'); ?>
				</div>
				<?php endif; ?>
                <?php if( get_field('footer_content') ): ?>
				<div class="footer-content cf">
					<?php the_field('footer_content'); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
        <div class="elem1"></div>
        <div class="elem2"></div>
        <div class="elem3"></div>
        <div class="elem4"></div>
        <div class="elem5"></div>
	</div>
</section>