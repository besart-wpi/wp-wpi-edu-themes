</div>

<?php

$path = explode("/", get_stylesheet_directory_uri());
$directoryname = $path[3];

if ($directoryname == "strategicplan") {

?>

<footer id="sp-footer" role="contentinfo">

	<div class="container-wrapper">
    <div class="container-content cf">
    
        <div class="left-column">
        <span>
			<a href="http://www.wpi.edu"></a>  
   		</span>
        <p class="sp-address"><strong>Worcester Polytechnic Institute</strong><br>
         
         100 Institute Road<br/>Worcester, MA 01609-2280<BR>
         Ph: +1-508-831-5000<BR>
             <a href="http://www.wpi.edu/about/web.html">Contact Us</a>&nbsp; &nbsp; | &nbsp; &nbsp;<a href="http://www.wpi.edu/about/visitors/directions.html">Directions</a>
        </p>
        </div><!-- .left-column -->
        
        <div class="right-column">
        <div id="floatrightcol">
        <p style="margin-bottom: 14px;"><strong>Resources:</strong></p>
        <ul class="firstcolumn">
        <li><a href="http://www.wpi.edu/calendars/index.html">Academic Calendars</a></li>
        <li><a href="http://maps.wpi.edu/map/?id=609" target="_blank">Campus Maps</a></li>
        <li><a href="http://www.wpi.edu/offices/hr.html">Human Resources</a></li>
 		</ul>
		<ul class="columns">
        <li><a href="http://www.wpi.edu/offices/policies/copyright.html">Copyright</a></li>
        <li><a href="http://www.wpi.edu/directories/index.html">Directories</a></li>
        <li><a href="http://www.wpi.edu/academics/library.html">Gordon Library</a></li>
        </ul>
		<ul class="columns">
        <li><a href="https://my.wpi.edu">myWPI</a></li>
        <li><a href="http://wpi.bncollege.com/webapp/wcs/stores/servlet/BNCBHomePage?storeId=32554&catalogId=10001&langId=-1 ">Bookstore</a></li>
        <li><a href="http://www.wpi.edu/offices/index.html">Offices &amp; Services</a></li>
        </ul>
        </div>
        </div><!-- .right-column -->
        
    </div>
    </div><!-- .container-wrapper -->

</footer>

<?php  } else {  ?>

<footer id="footer" role="contentinfo">

	<div class="container-wrapper">
    <div class="container-content cf">
    
        <div class="left-column">
            <a href=" <?php echo esc_url( home_url( '/' ) ); ?> "><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_footer.png" alt="Worcester Polytechnic Institute" /></a>
            <span class="footer-address">100 Institute Road&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Worcester, MA  01609-2280</span>
        </div><!-- .left-column -->
        
        <div class="right-column">
            <span class="copyright">&copy; <?php echo date('Y'); ?> Worcester Polytechnic Institute.<br />All rights reserved.</span>
        </div><!-- .right-column -->
        
    </div>
    </div><!-- .container-wrapper -->

</footer>

<?php  } ?>

</div>
<?php wp_footer(); ?>

<!--[if !lte IE 8]><!-->
<script>
jQuery(".flexnav").flexNav();
</script>
<!--<![endif]-->

</body>
</html>
