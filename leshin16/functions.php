<?php
	include_once(CP_ACF_PATH);
	
	add_action('after_setup_theme', 'blankslate_setup');
	function blankslate_setup(){
		load_theme_textdomain('blankslate', get_template_directory() . '/languages');
		add_theme_support('automatic-feed-links');
		add_theme_support('post-thumbnails');
		global $content_width;
		
		if ( ! isset( $content_width ) ) $content_width = 640;
		register_nav_menus(array( 'main-menu' => __( 'Main Menu', 'blankslate' ) ));
	}

	add_action('wp_enqueue_scripts', 'blankslate_load_scripts');
	function blankslate_load_scripts(){
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'theme-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0.0', true );
	}

	add_action('comment_form_before', 'blankslate_enqueue_comment_reply_script');
	function blankslate_enqueue_comment_reply_script(){
		
		if (get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}

	}

	add_filter('the_title', 'blankslate_title');
	function blankslate_title($title) {
		
		if ($title == '') {
			return '&rarr;';
		} else {
			return $title;
		}

	}

	add_filter('wp_title', 'blankslate_filter_wp_title');
	function blankslate_filter_wp_title($title){
		return $title . esc_attr(get_bloginfo('name'));
	}

	add_action('widgets_init', 'blankslate_widgets_init');
	function blankslate_widgets_init(){
		register_sidebar( array ('name' => __('Sidebar Widget Area', 'blankslate'),'id' => 'primary-widget-area','before_widget' => '<li id="%1$s" class="widget-container %2$s">','after_widget' => "</li>",'before_title' => '<h3 class="widget-title">','after_title' => '</h3>',) );
	}

	
	function blankslate_custom_pings($comment){
		$GLOBALS['comment'] = $comment;
		?>
<li <?php  comment_class(); ?> id="li-comment-<?php  comment_ID(); ?>"><?php  echo comment_author_link(); ?></li>
<?php 
	}

	add_filter('get_comments_number', 'blankslate_comments_number');
	function blankslate_comments_number($count){
		
		if (!is_admin()) {
			global $id;
			$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
			return count($comments_by_type['comment']);
		} else {
			return $count;
		}

	}

	/**
 * Adds a new shortcode in order to include custom fields contents in the page content:
 * To use, add a shortcode such as this to the editor:
 * [field name=name-of-your-custom-field]
 */
	function wpi_custom_field_shortcode($atts) {
		global $post;
		$name = $atts['name'];
		
		if (empty($name)) return;
		return get_post_meta($post->ID, $name, true);
	}

	add_shortcode('field', 'wpi_custom_field_shortcode');
	
function add_video_wmode_transparent($html, $url, $attr) {
	if ( strpos( $html, "<embed src=" ) !== false )
	   { return str_replace('</param><embed', '</param><param name="wmode" value="opaque"></param><embed wmode="opaque" ', $html); }
	elseif ( strpos ( $html, 'feature=oembed' ) !== false )
	   { return str_replace( 'feature=oembed', 'feature=oembed&wmode=opaque', $html ); }
	else
	   { return $html; }
}
add_filter( 'embed_oembed_html', 'add_video_wmode_transparent', 10, 3);
?>