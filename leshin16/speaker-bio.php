<?php 
/*
Template Name: Speaker Bio
*/

get_header(); ?>

<div id="interior_splash">
<?php 
$args=array(
  'order'=>'ASC',
  'orderby'=> 'menu_order',
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 1, // Only get first block, intro!
  'post_parent' => get_option('page_on_front') // Only get direct children of the front page
);
$header_block = new WP_Query($args);
if ( $header_block->have_posts() ) : while ( $header_block->have_posts() ) : $header_block->the_post(); 
	get_template_part( 'partials/loop-section-block' );
endwhile; endif;
wp_reset_postdata();
?>
</div>

<section class="block cf" id="content">   
	<div class="container-wrapper cf">
		<div class="container cf">
			<div class="container-content cf">
            	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                	
                    <?php if($post->post_parent): ?>
                    <h1 class="main-title"><?php echo get_the_title($post->post_parent); ?></h1>
                    <?php else: ?>
                    <h1 class="main-title"><?php the_title(); ?></h1>
                    <?php endif; ?>
                    
                    <h2 class="block-headline"><?php the_field('block-headline'); ?></h2>
                    <div class="left-column">
                        <?php the_content(); ?>
                    </div>
                    <?php if( get_field('right-column') ): ?>
                    <div class="right-column">
                        <?php the_field('right-column'); ?>
                    </div>
                    <?php endif; ?>
                    <?php if( get_field('footer_content') ): ?>
                    <div class="footer-content cf">
                        <?php the_field('footer_content'); ?>
                    </div>
                    <?php endif; ?>
                    
                <?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</section>

<?php // get_sidebar(); ?>
<?php get_footer(); ?>