jQuery(function( $ ) {
  
	$('.single-portfolio .aside').jScroll({speed:0});

 	$(".single-nav a").tipsy({ gravity: "s" });
	$(".social a").tipsy({ gravity: "s" });


	if ( $('.wpzoom-portfolio-slider-tabs').length > 0 ) {
		$('.wpzoom-portfolio-slider-tabs .pst-nav li').click(function(){
			$(this).addClass('current').siblings().removeClass('current');
			$(this).parent().parent().find('.pst-slider').removeClass('current').filter('.' + $(this).attr('data-value')).addClass('current');
		});

		$('.pst-slider').flexslider({
			selector: 'ul > li',
			controlNav: false,
			directionNav: true,
			animationLoop: false,
			animation: 'slide',
			slideshow: false,
			useCSS: false,
			smoothHeight: false,
			animationSpeed: 600
		});
	}

    var $folioitems = $('#portfolio #portfolio-items');
	if ( typeof wpz_currPage != 'undefined' && wpz_currPage < wpz_maxPages ) {
		$('.navigation').after('<a id="load-more" href="#">Load More&hellip;</a>').remove();
		$('#load-more').click(function(){
			if ( wpz_currPage < wpz_maxPages ) {
				$(this).text('Loading...');
				wpz_currPage++;
				$.get(wpz_pagingURL + wpz_currPage + '/', function(data){
					$newItems = $('#portfolio-items li', data).addClass('hidden').hide();
					$folioitems.append($newItems);
					$folioitems.find('li.hidden').show('slow').removeClass('hidden');

 					if ( (wpz_currPage+1) <= wpz_maxPages ) {

						$('#load-more').text('Load More\u2026');
					} else {
						$('#load-more').animate({height:'hide', opacity:'hide'}, 'slow', function(){$(this).remove();});
					}
				});
			}
			return false;
		});
	}

	if ( $('#respond input, #respond textarea').length > 0 && 'placeholder' in $('#respond input, #respond textarea')[0] ) {
		$('#respond').find('#commentform p label').hide();
	}
});

function wpz_sliderTextAnimate(slider) {
 	var $currentSlide = jQuery('#slider .slides li').not('.clone').eq(slider.animatingTo),
	    $otherSlides = jQuery('#slider .slides li').not($currentSlide);

	$otherSlides.find('h3').animate({'margin-top': 0, 'opacity': 0}, 800);
	$otherSlides.find('.excerpt').animate({'left': '-680px', 'opacity': 0}, 800);
	$currentSlide.find('h3').animate({'margin-top': '130px', 'opacity': 1}, 800);
	$currentSlide.find('.excerpt').animate({'left': 0, 'opacity': 1}, 800);
}

(function($) {
 
jQuery.fn.sorted = function(customOptions) {
		var options = {
			reversed: false,
			by: function(a) {
				return a.text();
			}
		};
		jQuery.extend(options, customOptions);
	
		$data = jQuery(this);
        var arr = $data.get();
		arr.sort(function(a, b) {
			
		   	var valA = options.by(jQuery(a));
		   	var valB = options.by(jQuery(b));
			if (options.reversed) {
				return (valA < valB) ? 1 : (valA > valB) ? -1 : 0;				
			} else {		
				return (valA < valB) ? -1 : (valA > valB) ? 1 : 0;	
			}
		});
		return jQuery(arr);
	};

})(jQuery);

jQuery(function() {
  
  var read_button = function(class_names) {
    var r = {
      selected: false,
      type: 0
    };
    for (var i=0; i < class_names.length; i++) {
      if (class_names[i].indexOf('active') == 0) {
        r.selected = true;
      }
      if (class_names[i].indexOf('segment-') == 0) {
        r.segment = class_names[i].split('-')[1];
      }
    };
    return r;
  };
  
  var determine_sort = function($buttons) {
    var $selected = $buttons.parent().filter('[class*="active"]');
    return $selected.find('a').attr('data-value');
  };
  
  var determine_kind = function($buttons) {
    var $selected = $buttons.parent().filter('[class*="active"]');
    return $selected.find('a').attr('data-value');
  };
  
  var $preferences = {
    duration: 400,    
    adjustHeight: 'dynamic',
	useScaling: true
  };
  
  var $list = jQuery('#portfolio-items');
  var $data = $list.clone();
  
  var $controls = jQuery('#portfolio-tags');
  
  $controls.each(function(i) {
    
    var $control = jQuery(this);
    var $buttons = $control.find('a');
    
    $buttons.bind('click', function(e) {
      
      var $button = jQuery(this);
      var $button_container = $button.parent();
      var button_properties = read_button($button_container.attr('class').split(' '));      
      var selected = button_properties.selected;
      var button_segment = button_properties.segment;

      if (!selected) {

        $buttons.parent().removeClass('active');
        $button_container.addClass('active');
        
        var sorting_type = determine_sort($controls.eq(1).find('a'));
        var sorting_kind = determine_kind($controls.eq(0).find('a'));
        
        if (sorting_kind == 'all') {
          var $filtered_data = $data.find('li');
        } else {
          var $filtered_data = $data.find('li.' + sorting_kind);
        }
        
        if (sorting_type == 'size') {
          var $sorted_data = $filtered_data.sorted({
            by: function(v) {
              return parseFloat(jQuery(v).find('span').text());
            }
          });
        } else {
          var $sorted_data = $filtered_data.sorted({
            by: function(v) {
              return jQuery(v).find('strong').text().toLowerCase();
            }
          });
        }
        
        $list.quicksand($sorted_data, $preferences);
        
      }
      
      e.preventDefault();
    });
    
  }); 

 
});



// jScroll 1.1 - William Duffy - http://www.wduffy.co.uk/jScroll
(function($){$.fn.jScroll=function(e){var f=$.extend({},$.fn.jScroll.defaults,e);return this.each(function(){var a=$(this);var b=$(window);var c=new location(a);b.scroll(function(){a.stop().animate(c.getMargin(b),f.speed)})});function location(d){this.min=d.offset().top;this.originalMargin=parseInt(d.css("margin-top"),10)||0;this.getMargin=function(a){var b=d.parent().height()-d.outerHeight();var c=this.originalMargin;if(a.scrollTop()>=this.min)c=c+f.top+a.scrollTop()-this.min;if(c>b)c=b;return({"marginTop":c+'px'})}}};$.fn.jScroll.defaults={speed:"slow",top:10}})(jQuery);