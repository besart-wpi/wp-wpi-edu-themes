<?php
$template = get_post_meta($post->ID, 'wpzoom_post_template', true);
?>

<?php get_header(); ?>

<div id="heading">
	
 	<h2><?php echo option::get('blog_title'); ?></h2>

</div><!-- / #welcome -->
 
<div id="content-wrap">

	<div id="content" <?php 
		if ($template == 'side-left') { echo " class=\"side-left\""; }
		if ($template == 'full') { echo " class=\"full-width\""; } 
		?>>
 
		<div class="post_content">

			<?php wp_reset_query(); if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
				
					<h1 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a><?php edit_post_link( __('Edit', 'wpzoom'), ' <small>', '</small>' ); ?></h1>

					<div class="meta">
						<?php
						if ( option::get('post_author') == 'on' ) { echo '<p class="author">' . __('By ', 'wpzoom'); the_author_posts_link(); echo '</p>'; }
						if ( option::get('post_date') == 'on' ) { echo '<p class="date">' . get_the_date() . '</p>'; }
						if ( option::get('post_comments') == 'on' ) { echo '<p class="comments">'; comments_popup_link( __('0 comments', 'wpzoom'), __('1 comment', 'wpzoom'), __('% comments', 'wpzoom'), '', __('Comments are Disabled', 'wpzoom') ); echo '</p>'; }
						if ( option::get('post_category') == 'on' ) { echo '<p class="category">'; the_category(', '); echo '</p>'; }
						?>
					</div>

					<div class="entry">
					
						<?php the_content(); ?>
					
						<?php wp_link_pages(array('before' => '<p class="pages"><strong>'.__('Pages', 'wpzoom').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
						
						<?php the_tags('<p class="tags">Tags ', ' ', '</p>'); ?>

						<div class="cleaner">&nbsp;</div>
				
					</div><!-- / .entry -->
					
					<?php comments_template(); ?>  

				</div><!-- #post-<?php the_ID(); ?> -->
 			
			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria', 'wpzoom');?>.</p>
			<?php endif; ?>

			<div class="cleaner">&nbsp;</div>          
		</div><!-- / .post_content -->
		
		<?php if ($template != 'full') { get_sidebar(); } ?>

	</div><!-- / #content -->
	<div class="cleaner">&nbsp;</div>

</div>
 
<?php get_footer(); ?>