<?php 
/*-----------------------------------------------------------------------------------*/
/* Initializing Widgetized Areas (Sidebars)																			 */
/*-----------------------------------------------------------------------------------*/


/*----------------------------------*/
/* Sidebar							*/
/*----------------------------------*/
 
 register_sidebar(array(
	'name'=>'Sidebar',
	'id' => 'sidebar',
	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '<div class="cleaner">&nbsp;</div></div>',
	'before_title' => '<h3 class="title">',
	'after_title' => '</h3>',
));



/*----------------------------------*/
/* Homepage widgetized areas		*/
/*----------------------------------*/
 
register_sidebar(array(
	'name'=>'Homepage: Main',
	'id' => 'home-main',
	'description' => 'Main widget area in the upper-left of the homepage. &#13; &#10; &#09; Add here: "WPZOOM: Single Page" widget.',
	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="title"><span>',
	'after_title' => '</span></h3>',
));

register_sidebar(array(
	'name'=>'Homepage: Right of Main',
	'id' => 'home-rightmain',
	'description' => 'Smaller widget area to the right of the main widget area. Add here: "WPZOOM: Portfolio Slider Tabs" widget.',
	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '<div class="cleaner">&nbsp;</div></div>',
	'before_title' => '<h3 class="title"><span>',
	'after_title' => '</span></h3>',
));

register_sidebar(array(
	'name'=>'Homepage: Wide Below Main',
	'id' => 'home-belowmainwide',
	'description' => 'A wide widget area below the main and right widget areas. Add here: "WPZOOM: Call to Action" widget.',
	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '<div class="cleaner">&nbsp;</div></div>',
	'before_title' => '<h3 class="title"><span>',
	'after_title' => '</span></h3>',
));

register_sidebar(array(
	'name'=>'Homepage: Large Below Wide',
	'id' => 'home-belowwidelarge',
	'description' => 'A large widget area below the wide widget area. Add here: "WPZOOM: Recent Posts" widget.',
	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '<div class="cleaner">&nbsp;</div></div>',
	'before_title' => '<h3 class="title"><span>',
	'after_title' => '</span></h3>',
));

register_sidebar(array(
	'name'=>'Homepage: Right of Large',
	'id' => 'home-rightlarge',
	'description' => 'Smaller widget area to the right of the large widget area. Add here: "WPZOOM: Testimonial" widget.',
	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '<div class="cleaner">&nbsp;</div></div>',
	'before_title' => '<h3 class="title"><span>',
	'after_title' => '</span></h3>',
));

register_sidebar(array(
	'name'=>'Homepage: Bottom Wide',
	'id' => 'home-bottomwide',
	'description' => 'Wide widget area below all other homepage widget areas. Add here: "WPZOOM: Clients" widget.',
	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '<div class="cleaner">&nbsp;</div></div>',
	'before_title' => '<h3 class="title"><span>',
	'after_title' => '</span></h3>',
));

 
 
/*----------------------------------*/
/* Footer widgetized areas		*/
/*----------------------------------*/

register_sidebar(array('name'=>'Footer: Wide',
	'description' => 'Wide widget area below all other homepage widget areas. Add here: "WPZOOM: Twitter Bubble" widget.',
 	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
));

register_sidebar(array('name'=>'Footer: Column 1',
	'id' => 'footer_1',
 	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="title">',
	'after_title' => '</h3>',
));

register_sidebar(array('name'=>'Footer: Column 2',
	'id' => 'footer_2',
 	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="title">',
	'after_title' => '</h3>',
));

register_sidebar(array('name'=>'Footer: Column 3',
	'id' => 'footer_3',
 	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="title">',
	'after_title' => '</h3>',
));

register_sidebar(array('name'=>'Footer: Right of Copyright',
	'description' => 'Widget area for WPZOOM: Social Widget.',
	'before_widget' => '<div class="widget %2$s" id="%1$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
));
 
 
?>