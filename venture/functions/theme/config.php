<?php return array(
/**
 * Theme configuration goes there
 */
    'name'    => 'Venture',   														 // theme name
    'styled'  => false,              									 			 // true if current theme supports styling
    'scripts' => array("dropdown", "jquery.quicksand", "jquery.tipsy", "custom")     // javascripts theme uses
);