<?php

/*------------------------------------------*/
/* WPZOOM: Clients                          */
/*------------------------------------------*/

class Wpzoom_Clients extends WP_Widget {

	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'wpzoom-clients', 'description' => 'A sliding list of clients and/or partners from the clients custom post type.' );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'wpzoom-clients' );

		/* Create the widget. */
		$this->WP_Widget( 'wpzoom-clients', 'WPZOOM: Clients', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {

		extract( $args );

		/* User-selected settings. */
		$title 			= apply_filters('widget_title', $instance['title'] );
		$show_count 	= $instance['show_count'];
		$orderby = $instance['random'] == 'on' ? 'rand' : 'date';

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Title of widget (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;

		?><script type="text/javascript">
		jQuery(function($){
			$('#<?php echo $widget_id; ?>').flexslider({
				selector: '.clients_widget > li',
				controlNav: false,
				directionNav: true,
				animationLoop: false,
				animation: 'slide',
				slideshow: false,
				useCSS: false,
				smoothHeight: false,
				animationSpeed: 600,
				itemWidth: 200,
				itemMargin: 30,
				minItems: 1,
				maxItems: 4
			});
		});
		</script>
		<ul class="clients_widget"><?php

		$query_opts = apply_filters('wpzoom_query', array(
			'posts_per_page' => $show_count,
			'post_type' => 'client',
			'orderby' => $orderby
		));

		query_posts($query_opts);
		if ( have_posts() ) : while ( have_posts() ) : the_post();
			$url = esc_url( trim( get_post_meta( get_the_ID(), 'wpzoom_client_url', true ) ) );

			echo '<li>';

			if ( !empty( $url ) ) echo '<a href="' . $url . '" title="' . the_title_attribute('echo=0') . '">';

      get_the_image( array( 'size' => 'clients-logo', 'width' => 200, 'link_to_post' => false ) );

			if ( !empty( $url ) ) echo '</a>';

			echo '</li>';

		endwhile; endif;

			//Reset query_posts
			wp_reset_query();
		echo '</ul><div class="clear"></div>';

		/* After widget (defined by themes). */
		echo $after_widget;
	}


	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['show_count'] = $new_instance['show_count'];
		$instance['random'] = $new_instance['random'];

		return $instance;
	}

	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Valuble Clients', 'show_count' => 8, 'random' => 'on' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label><br />
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" type="text" class="widefat" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'show_count' ); ?>">Show:</label>
			<input id="<?php echo $this->get_field_id( 'show_count' ); ?>" name="<?php echo $this->get_field_name( 'show_count' ); ?>" value="<?php echo $instance['show_count']; ?>" type="text" size="2" /> posts
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['random'], 'on' ); ?> id="<?php echo $this->get_field_id( 'random' ); ?>" name="<?php echo $this->get_field_name( 'random' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'random' ); ?>">Show random clients</label>
		</p>

		<?php
	}
}

function wpzoom_register_cl_widget() {
	register_widget('Wpzoom_Clients');
}
add_action('widgets_init', 'wpzoom_register_cl_widget');