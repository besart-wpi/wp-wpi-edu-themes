<?php while (have_posts()) : the_post();?>

	<div id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>

		<h2 class="title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a><?php edit_post_link( __('Edit', 'wpzoom'), ' <small>', '</small>' ); ?></h2>

		<div class="meta">
			<?php
			if ( option::get('display_author') == 'on' ) { echo '<p class="author">' . __('By ', 'wpzoom'); the_author_posts_link(); echo '</p>'; }
			if ( option::get('display_date') == 'on' ) { echo '<p class="date">' . get_the_date() . '</p>'; }
			if ( option::get('display_comments') == 'on' ) { echo '<p class="comments">'; comments_popup_link( __('0 comments', 'wpzoom'), __('1 comment', 'wpzoom'), __('% comments', 'wpzoom'), '', __('Comments are Disabled', 'wpzoom') ); echo '</p>'; }
			if ( option::get('display_category') == 'on' ) { echo '<p class="category">'; the_category(', '); echo '</p>'; }
			?>
		</div>

		<?php if (option::get('index_thumb') == 'on') {

 			get_the_image( array( 'size' => 'loop', 'width' => option::get('thumb_width'), 'height' => option::get('thumb_height'), 'before' => '<div class="post-thumb">', 'after' => '</div>' ) );

		} ?>

 		<div class="entry">
			<?php if (option::get('display_content') == 'Full Content') {  the_content('<span>'.__('Read more', 'wpzoom').' &#8250;</span>'); } if (option::get('display_content') == 'Excerpt')  { the_excerpt(); } ?>

			<?php if ( option::get('display_readmore') == 'on' ) { ?><p class="readmore"><a href="<?php the_permalink(); ?>"><?php _e('Read more', 'wpzoom'); ?></a></p><?php } ?>
		</div>

		<div class="cleaner">&nbsp;</div>

	</div><!-- /.post -->

<?php endwhile; ?>

<div class="cleaner">&nbsp;</div>
<?php get_template_part( 'pagination'); ?>
<?php wp_reset_query(); ?>
<div class="cleaner">&nbsp;</div>