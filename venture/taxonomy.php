<?php get_header(); ?>

<?php
$taxonomy_obj = $wp_query->get_queried_object();
$taxonomy_nice_name = $taxonomy_obj->name;

if (have_posts()) : ?>

<div id="heading">

	<h2><?php echo $taxonomy_nice_name;?></h2>

</div><!-- / #welcome -->

<div id="content-wrap">
	<div class="sub-heading">
		<h3><?php _e('Portfolio', 'wpzoom'); ?></h3>

		<?php if (option::get('portfolio_tags') == 'on') { ?>
			<ul id="portfolio-fixed" >
				<li><a class="all" data-value="all" href="<?php echo get_page_link(option::get('portfolio_url')); ?>"><?php _e('All', 'wpzoom'); ?></a></li>
				<?php wp_list_categories(array('title_li' => '', 'hierarchical' => 0, 'taxonomy' => 'skill-type')); ?>
			</ul>
		<?php } ?>

		<div class="clear"></div>
	</div>

	<div class="clear"></div>

	<div id="portfolio">
		<ul id="portfolio-items" >
			<?php while ( have_posts() ) : the_post(); $count = 1;
				$terms = get_the_terms( get_the_ID(), 'skill-type' ); if (!$terms) $terms = array(); ?>
			<li data-id="id-<?php echo $count; ?>" class="<?php foreach ($terms as $term) { echo strtolower(preg_replace('/\s+/', '-', $term->name)). ' '; } ?>" >

				<a href="<?php the_permalink(); ?>" rel="bookmark" class="thumb" title="<?php printf(__('Permanent Link to %s', 'wpzoom'), get_the_title()); ?>">
					<?php get_the_image( array( 'size' => 'portfolio-thumb',  'width' => 220, 'height' => 140, 'link_to_post' => false  ) ); ?>
				</a>

				<div class="meta">
					<h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'wpzoom'), get_the_title()); ?>">  <?php the_title(); ?></a></h3>
				</div>
			</li>
			<?php $count++; ?>
			<?php endwhile; endif; ?>
		</ul>
		<div class="clear"></div>
		<?php get_template_part( 'pagination'); ?>
		<?php wp_reset_query(); ?>
		<div class="clear"></div>
	</div><!-- / #portfolio -->

	<div class="clear"></div>
</div>

<?php get_footer(); ?>