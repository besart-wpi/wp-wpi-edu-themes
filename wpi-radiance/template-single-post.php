<?php
/*
Template Name: Single Post With Comments
*/
?>

<?php get_header(); ?>

<?php get_template_part( 'content', 'before' ); ?>

<?php

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array(
    // Change these category SLUGS to suit your use.
    //'category_name' => 'music, videos',
    'post_type' => 'post',
    //'paged' => $paged
);

$list_of_posts = new WP_Query( $args );

if ( $list_of_posts->have_posts() ) : ?>

    <?php /* The loop */ ?>
    <?php while ( $list_of_posts->have_posts() ) : $list_of_posts->the_post(); ?>

        <?php // Display content of posts ?>

        <div class="post" id="post-main-<?php the_ID(); ?>">

            <div class="entry">

                <h1 class="post-title single"><?php the_title(); ?></h1>

                <!--<?php get_template_part( 'postinfo' ); ?>-->

                <?php if ( get_post_meta( $post->ID, 'video_embed', true ) ) { ?>
                    <div class="single-video">
                        <?php echo get_post_meta( $post->ID, 'video_embed', true ); ?>
                    </div>
                <?php } ?>

                <?php the_content(); ?>

                <div style="clear:both;"></div>

                <?php wp_link_pages(); ?>

            </div>

            <?php if(function_exists('the_tags')) { the_tags('<p><strong>'. __('Tags', "solostream"). '</strong>: ', ', ', '</p>'); } ?>

        </div>

        <!--<?php get_template_part( 'auth-bio' ); ?>-->

        <?php comments_template('', true); ?>

        <?php get_template_part( 'bot-nav' ); ?>

    <?php endwhile; ?>

<?php else : ?>

    <?php get_template_part( 'content', 'none' ); ?>

<?php endif; ?>

<?php get_template_part( 'content', 'after' ); ?>