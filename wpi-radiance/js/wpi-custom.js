
jQuery(document).ready(function($) {

    /**
     * Adds a class to topnav items with children
     * @type {[type]}
     */
    $('#topnav ul.nav li:has(ul)').addClass('has-children');

    $('#toggle').click(function() {
        $('#topnav').slideToggle(400);
        $(this).toggleClass("active");
        return false;
    });

});