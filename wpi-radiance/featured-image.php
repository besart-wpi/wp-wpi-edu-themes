<div class="featured wide pages clearfix">

    <div class="left-cover"></div>
    <div class="right-cover"></div>

    <div class="container pages-slider">

        <div id="featured-pages" class="flexslider">

            <ul class="slides">



                <li id="feature-page-1">

                    <div class="slide-container clearfix">
                        <div class="post" id="featurepage-post-<?php the_ID(); ?>">
                            <?php wpi_solostream_feature_image_wide(); ?>
                        </div>
                    </div>

                </li>



            </ul>

        </div>

    </div>

</div>
<div class="clearfix"></div>