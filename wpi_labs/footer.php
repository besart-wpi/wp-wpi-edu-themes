		<?php if($content = Oz::getField('content', 'footer-callout')): ?>
			<section id="footer-callout">
				<?= apply_filters('the_content', $content) ?>
			</section> <!-- id="footer-callout" -->
		<?php endif; ?>

		<div class="footer-wrap">
			<footer class="main">
				<div class="copyright c6">
          <div class="footer-social-container">
            <?php if ($twitter_url = esc_url(ot_get_option( 'twitter_url' ))): ?>
              <a class="footer-social" href="<?php echo esc_url($twitter_url); ?>" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri() . '/img/twitter.png'?>">
              </a>
            <?php endif; ?>
            <?php if ($facebook_url = esc_url(ot_get_option( 'facebook_url' ))): ?>
              <a class="footer-social" href="<?php echo esc_url($facebook_url); ?>" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri() . '/img/facebook.png'?>">
              </a>
            <?php endif; ?>
            <?php if ($linkedin_url = esc_url(ot_get_option( 'linkedin_url' ))): ?>
              <a class="footer-social" href="<?php echo esc_url($linkedin_url); ?>" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri() . '/img/linkedin.png'?>">
              </a>
            <?php endif; ?>
          </div> <!-- footer-social-container -->
					<?= apply_filters('the_content', ot_get_option('footer_copyright')) ?>
				</div>
				<div class="contact c6">
					<div class="wrap">
						<?= apply_filters('the_content', ot_get_option('footer_contact')) ?>
					</div> <!-- class="wrap" -->
					<div class="clear"></div>
				</div> <!-- contact c6 -->


			</footer> <!-- class="main" -->
		</div> <!-- class="footer-wrap" -->
	</div> <!-- id="body-wrap" -->
	<aside id="mobile-menu">
<!-- 		<div class="logo">
        	<?= bloginfo('name') ?>
		</div> -->
		<?php wp_nav_menu(array(
			'theme_location'	=> 'main'
		)) ?>		
	</aside> <!-- id="mobile-menu" -->
	<?php wp_footer() ?>
</body>
</html>