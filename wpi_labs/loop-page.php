<?php if(have_posts()):
	while(have_posts()): the_post(); ?>
		<?php the_content() ?>
	<?php endwhile;
else: ?>
	<h2>Posts are coming soon!</h2>
<?php endif; ?>