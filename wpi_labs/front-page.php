<?php get_header() ?>
	 <div class="hero">
    <header>
      <h1><?= ot_get_option('landing_pages_news_title') ?> <?= getArchiveLabel() ?></h1>
    </header>
  </div> <!-- class="hero" -->
  <section id="the-content">
    <div id="content">
      <?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      // Displays either content as full width or with sidebar
      //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
      $sidebar = getSidebar('page');
      $hasSidebar = is_active_sidebar('global') || $sidebar;
      $contentWidth = $hasSidebar ? 'c8' : 'c12';
      ?>
      <article class="main <?= $contentWidth ?>">
        <?php get_template_part('loop', 'page') ?>
      </article>
      <?php if($hasSidebar): ?>
        <aside class="main c4">
          <?= apply_filters('the_content', $sidebar) ?>
          <?php if($sidebar) echo '<hr>'; ?>
          <?php dynamic_sidebar('global') ?>
        </aside>
      <?php endif; ?>
      <div class="clear"></div>
    </div>
  </section>
  <?php get_footer() ?>