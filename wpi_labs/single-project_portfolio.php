<?php 
get_header();
$overview = trim(Oz::getField('overview', 'info'));
$client = trim(Oz::getField('client', 'info'));
$services = Oz::getField('services', 'info');
$terms = wp_get_post_terms($post->ID, 'project-categories');

$hasOverview = $overview || $client /* || $services  || count($terms) */;
$articleWidth = $hasOverview ? 'c8' : 'c12';

// =================================================
// :: Gets the .hero background ::
// -------------------------------------------------
if($featuredImage = get_post_thumbnail_id()){
	$featuredImage = get_post($featuredImage)->guid;
}

?>

<?php
    $all_portfolios = new WP_Query(array(
		'post_type' => 'project_portfolio',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => -1
    ));

    foreach($all_portfolios->posts as $key => $value) {
        if($value->ID == $post->ID){
            $nextID = $all_portfolios->posts[$key + 1]->ID;
            $prevID = $all_portfolios->posts[$key - 1]->ID;
            break;
        }
    }
?>

	<div class="hero <?php if($featuredImage) echo 'imaged" style="background: url('. $featuredImage . '); background-position: center; background-repeat: no-repeat; background-size: cover; -ms-behavior: url('.get_bloginfo('template_url').'/js/backgroundsize.min.htc);'; ?>">
		<header>
			<h1>
				<?php if($featuredImage) echo '<span>' ?>
					<?php the_title() ?>
				<?php if($featuredImage) echo '</span>' ?>

				<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
				// Next / Prev posts
				//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - ?>
				<?php if($nextID): ?>
					<a href="<?= get_the_permalink($nextID) ?>" rel="next">></a>
				<?php endif; ?>
				<?php if($prevID): ?>
					<a href="<?= get_the_permalink($prevID) ?>" rel="prev"><</a>
				<?php endif; ?>
			</h1>
		</header>
	</div>


	<section id="the-content">
		<div id="content">
			<article class="main <?= $articleWidth ?>">
				<div class="project cycle-slideshow-wrap">
					<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
					// Image slideshow
					//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
					if($images = get_post_meta($post->ID, 'portfolio_gallery_items', true)):
						$images = explode(',', $images); ?>
						<div class="project cycle-slideshow"
					      data-cycle-loader=wait
						  data-cycle-timeout="0"
						  data-cycle-auto-height="container"  
						  data-cycle-prev="#prev"
					      data-cycle-next="#next">
							<?php foreach ($images as $key => $img) {
								$attachment = wp_get_attachment_image($img, 'full');
								echo preg_replace('#(width|height)="\d+"#','',$attachment);
							} ?>
						</div>
						<?php if(count($images) > 1): ?>
							<div class="pager">
								<div id="prev">&#8594;</div>
								<div id="next">&#8592;</div>
							</div>
						<?php endif; ?>
						<div class="clear"></div>
					</div>
				<?php endif ?>

				<?php get_template_part('loop', 'page') ?>
			</article>
			<?php if($hasOverview): ?>
				<aside class="main c4">
					<div id="portfolio-sidebar">
						<h2>Overview</h2>
						<div>
							<?= $overview ?>
	
							<?php if($client): ?>
								<h3>Researchers</h3>
								<?= $client ?>
							<?php endif; ?>

<?php /* ?>
							<?php if($services): ?>
								<h3>Services</h3>
								<?php foreach($services as $service): ?>
									<p class="service"><?= $service ?>
								<?php endforeach; ?>
							<?php endif; ?>
<?php */ ?>

							<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
							// Display all terms 
							//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
							if(count($terms)): 
								$cptURL = get_post_type_archive_link('project_portfolio') ?>
								<h3>Category</h3>
								<p>
								<?php foreach($terms as $key=>$term): ?>
									<a href="<?= $cptURL ?>"><?= $term->name ?></a><?php if($key < count($terms)-1) echo ', '; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
					</div>
				</aside>
			<?php endif; ?>
			<div class="clear"></div>
		</div>
	</section>


	<script>
		jQuery(window).ready(function(){
			jQuery('.cycle-slideshow').cycle('reinit')
		})
	</script>
<?php get_footer() ?>