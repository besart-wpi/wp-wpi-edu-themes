<?php
// =================================================
// :: Photo Gallery Shortcode ::
// -------------------------------------------------
// NOTE: Replaces the default [gallery]
/**
 * Removes the rendering of the gallery shortcode
 * @see  http://codex.wordpress.org/TinyMCE
 */

// This filter causess core TinyMCE buttons to disappear (ex: buttons to add and remove links)
// add_filter('tiny_mce_before_init', 'tinyMCE_remove_gallery');
function tinyMCE_remove_gallery($in){
	$in['plugins'] = str_replace('wpgallery,', '', $in['plugins']);
	return $in;
}
remove_shortcode('gallery');

/**
 * Gallery Class
 */
class Shortcode_Gallery{
	function __construct(){
		add_shortcode('gallery', array(&$this, 'process'));
		add_action('admin_init', array(&$this, 'options'));
	}

	/**
	 * Processes the shortcode
	 *
	 * @param  [$atts] [List of attribtues]
	 * @return [STR] The output, also displays an error if the gallery doesn't exist
	 */
	function process($atts){
		/**
		 * Validate
		 */
		if(isset($atts[0]) && !isset($atts['id'])) $atts['id'] = $atts[0];	//Allows for attribute-less execution
		$atts = shortcode_atts(array(
			'id'	=> 0
		), $atts);
		if(!$atts['id']) return '<b>[' . __('gallery: ID does not exist') . ']</b>';

		/**
		 * Load the gallery CPT
		 */
		$gallery = get_posts(array(
			'posts_per_page'	=> 1,
			'post__in'			=> array($atts['id']),
			'post_type'			=> 'photo_gallery'
		));
		if(!$gallery) return '<b>[' . __('gallery: '.$atts['id'].' does not exist') . ']</b>';

		/**
		 * Scripts
		 */
		wp_enqueue_script('galleria', get_template_directory_uri() . '/js/lib/galleria/galleria-1.4.7.min.js');
		add_action('wp_footer', array(&$this, 'galleria_scripts'));

		/**
		 * Display the gallery
		 */
		ob_start();
			$post = $gallery[0];
			$photos = explode(',', get_post_meta($post->ID, 'gallery_items', true));
			echo '<div class="galleria test">';
				foreach($photos as $key => $photo):
					$image_attributes = wp_get_attachment_image_src( $photo, 'full' );
					$image_attachment = get_post( $photo );
					?>
					<a href="<?= $image_attributes[0] ?>"<?php echo $key > 0 ? ' style="display: none;"' : ''; ?>><img src="<?= $image_attributes[0] ?>" height="<?= $image_attributes[2] ?>" width="<?= $image_attributes[1] ?>" data-title="<?php echo $image_attachment->post_title; ?>" data-description="<?php echo $image_attachment->post_excerpt; ?>" /></a>
				<?php endforeach;
			echo '</div>';
		return ob_get_clean();
	}

	/**
	 * "Enqueue" hardcoded scripts
	 *
	 * Adds in the galleria theme and Theme Options theme overrides
	 */
	function galleria_scripts(){
		/**
		 * Scripts
		 */
		$theme = ot_get_option('gallery_black_icons', 'off') == 'on' ? 'inverted' : 'classic';
		$autoplay_speed = ot_get_option('gallery_autoplay_speed', '7000');
		?>

        <?php if(ot_get_option('gallery_show_thumbs') == 'off'): ?>
        <style>
			.galleria-thumbnails-container { display: none !important; }
		</style>
        <?php endif; ?>
		<script>
			if(typeof galleriaInit == 'undefined'){
				galleriaInit = true;
				jQuery(function($){

					Galleria.loadTheme('<?= get_template_directory_uri() . '/js/lib/galleria/themes/'. $theme .'/galleria.'.$theme.'.min.js' ?>');

					$(window).load(function(){
						$('.galleria').each(function(){

							// Get height of tallest image
							var elementHeights = $('img', this).map(function() {
								return $(this).height();
							}).get();
							var maxHeight = Math.max.apply(null, elementHeights);

							// Minimum height is width of galleria element
							if( maxHeight < $(this).width() ){
								maxHeight = $(this).width();
							}

							$(this).galleria({
								width: '100%',
								height: maxHeight ? maxHeight : 1.0,
								<?php if(ot_get_option('gallery_show_description') == 'off') echo 'showInfo: false,' ?>
								<?php if(ot_get_option('gallery_show_thumbs') == 'off') echo 'thumbnails: false,' ?>
								<?php if(ot_get_option('gallery_show_arrows') == 'off') echo 'showImagenav: false,' ?>
								lightbox: true,
								autoplay: <?php echo $autoplay_speed; ?>,
								imageMargin: 0,
								_toggleInfo: false // Set this to false if you want the caption to show always
							});
						});
					}); // END window.load


				})
			}
		</script>

		<?php /**
		 * Styles
		 */ ?>
		<style>
			body .galleria-container {background: <?= ot_get_option('gallery_background', '#000') ?>;}
		</style>
	<?php }

	/**
	 * Theme Options
	 */
	function options(){
		$saved_settings = get_option( ot_settings_id(), array() );

		$custom_settings = array(

			'sections'        => array(
				array(
					'id'          => 'section_gallery_shortcode',
					'title'       => __( 'Gallery Shortcode', 'wpi-parent' )
				),
			),

			'settings'        => array(
				array(
					'id'          => 'gallery_background',
					'label'       => __( 'Background Color', 'wpi-parent' ),
					'type'        => 'colorpicker',
					'section'     => 'section_gallery_shortcode',
				),
				array(
					'id'		=> 'gallery_show_description',
					'label'		=> __('Image Description', 'wpi-parent'),
					'type'		=> 'on-off',
					'section'	=> 'section_gallery_shortcode'
				),
				array(
					'id'		=> 'gallery_show_thumbs',
					'label'		=> __('Thumbnails', 'wpi-parent'),
					'type'		=> 'on-off',
					'section'	=> 'section_gallery_shortcode'
				),
				array(
					'id'		=> 'gallery_show_arrows',
					'label'		=> __('Show Navigation Arrows', 'wpi-parent'),
					'type'		=> 'on-off',
					'section'	=> 'section_gallery_shortcode'
				),
				array(
					'id'		=> 'gallery_black_icons',
					'label'		=> __('Black Icons', 'wpi-parent'),
					'type'		=> 'on-off',
					'std'		=> 'off',
					'section'	=> 'section_gallery_shortcode'
				),
				array(
					'id'		=> 'gallery_autoplay_speed',
					'label'		=> __('Autoplay Speed', 'wpi-parent'),
					'dec'		=> 'Gallery autoplay speed in milliseconds. Set to 0 to disable. Defaults to 7000 (7 seconds).',
					'type'		=> 'text',
					'std'		=> '7000',
					'section'	=> 'section_gallery_shortcode'
				)
			)
		);
		$custom_settings = array_merge_recursive($saved_settings, $custom_settings);

		// settings are not the same update the DB
		if ( $saved_settings !== $custom_settings ) {
			update_option( ot_settings_id(), $custom_settings );
		}
	}
} ?>