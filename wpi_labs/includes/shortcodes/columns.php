<?php 

// =================================================
//  :: Columns Shortcode ::
// ------------------------------------------------

function shortcode_column_half( $atts, $content = null ) {
  return '<div class="col half">' . apply_filters('the_content', $content ) . '</div>';
}
add_shortcode('half', 'shortcode_column_half');

function shortcode_column_half_last( $atts, $content = null ) {
  return '<div class="col half last">' . apply_filters('the_content', $content ) . '</div><div class="clear"></div>';
}
add_shortcode('half_last', 'shortcode_column_half_last');

?>