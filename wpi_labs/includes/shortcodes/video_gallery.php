<?php
// =================================================
//	:: Video Gallery Shortcode ::
// ------------------------------------------------

function wpi_shortcode_video_gallery( $atts, $content="" ) {
    $atts = shortcode_atts( array(
 	      'id' => ''
      ), $atts );
	  
	// $atts['id']
	
	if(!empty($atts['id'])):
	ob_start(); 
	// ############################## ?> 
    
    	<?php // Get post ID of video gallery
		if($atts['id']){ $gallery_videos = get_post_meta($atts['id'], 'gallery_videos', true); }
		
		// var_dump($gallery_videos);
		
		// If our gallery has videos
		if( is_array($gallery_videos) && !empty($gallery_videos) ): ?>
        
        <div class="wpi_video_gallery" data-playerid="<?php echo $atts['id']; ?>">
        	
            <div class="video_gallery_wrap">
            
            	<div class="video_gallery_player_element">
                
					<?php
                    // YOUTUBE
                    if($gallery_videos[0]['video_youtube_url']) {
                        $url = $gallery_videos[0]['video_youtube_url'];
                        preg_match(
                                '/[\\?\\&]v=([^\\?\\&]+)/',
                                $url,
                                $matches
                            );
                        $id = $matches[1];
                         
                        $width = '480';
                        $height = '270';
                        echo '<iframe width="' . $width . '" height="' . $height . '" src="http://www.youtube.com/embed/' . $id . '?autoplay=0&wmode=transparent" frameborder="0" allowfullscreen></iframe>';
                    
                    // LOCAL VIDEO (MP4)
                    } elseif($gallery_videos[0]['video_upload']) { ?>
                    
                        <video id="videojs_player" height="270" width="480" class="video-js vjs-default-skin" controls>
                            <?php echo $gallery_videos[0]['video_upload'] ? '<source src="'.$gallery_videos[0]['video_upload'].'" type="video/mp4"></source>' : ''; ?>
                        </video>
                        
                    <?php } ?>
            	
                </div><!-- .video_gallery_player_element -->
                
            </div><!-- .video_gallery_wrap -->


            <div class="cycle-slideshow video_carousel" 
                data-cycle-slides="> a.video_thumbnail" 
                data-cycle-fx="carousel"
                data-cycle-easing="easeInOutCubic"
                data-cycle-timeout=0
                data-cycle-next="> .video_next"
                data-cycle-prev="> .video_prev"
                data-allow-wrap="false"
                >
                
                <?php foreach($gallery_videos as $video): 
                    $video_cover_image_id = custom_get_attachment_id( $video['video_cover_image'] );
                    $video_cover_image_attributes = wp_get_attachment_image_src( $video_cover_image_id, 'thumbnail' );
                    ?>
                    <a href="<?php echo $video['video_youtube_url'] ? $video['video_youtube_url'] : $video['video_upload']; ?>" title="<?php echo $video['title']; ?>" data-video-type="<?php echo $video['video_youtube_url'] ? 'youtube' : 'mp4'; ?>" class="video_thumbnail"><img src="<?php echo $video_cover_image_attributes[0] ? $video_cover_image_attributes[0] : get_stylesheet_directory_uri() . '/images/video_thumbnail_default.png' ; ?>" class="video_thumbnail_image" /><?php /* <span class="video_title"><?php echo $video['title']; ?></span> */ ?></a>
                <?php endforeach; ?>
                
                <a href="#" class="video_prev video_nav">&laquo; Prev</a>
                <a href="#" class="video_next video_nav">Next &raquo;</a>

            </div><!-- .video_carousel -->
        
        </div><!-- .wpi_video_gallery -->

        <?php endif; // END check for videos in gallery ?>
    
	<?php
	// ##############################
    return ob_get_clean();
	endif;
}
add_shortcode( 'vgallery', 'wpi_shortcode_video_gallery' );


// Enqueue scripts and styles needed by the shortcode
function wpi_shortcode_video_gallery_enqueue(){
	wp_enqueue_style( 'wpi-parent-video-gallery', get_template_directory_uri() . '/css/video-gallery.css' );
	wp_enqueue_style( 'videojs', get_template_directory_uri() . '/js/lib/videojs/video-js.min.css' );
	
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-cycle-2', get_template_directory_uri() . '/js/lib/cycle2/jquery.cycle2.min.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-cycle-2-carousel', get_template_directory_uri() . '/js/lib/cycle2/jquery.cycle2.carousel.min.js', array( 'jquery', 'jquery-cycle-2' ), true);	
	wp_enqueue_script('jquery-easing', get_template_directory_uri() . '/js/lib/easing/jquery.easing.1.3.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-fitvids', get_template_directory_uri() . '/js/lib/fitvids/jquery.fitvids.js', array( 'jquery' ), true);
	wp_enqueue_script('videojs', get_template_directory_uri() . '/js/lib/videojs/video.js', array( 'jquery' ), true);
  wp_enqueue_script('wpi-labs-video-gallery', get_info('url') . '/js/video_gallery.js', array( 'jquery' ), true);
  // wp_enqueue_script('wpi-parent-video-gallery', get_template_directory_uri() . '/js/video_gallery.js', array( 'jquery' ), true);
}

add_action( 'wp_enqueue_scripts', 'wpi_shortcode_video_gallery_enqueue' );





// =================================================
//	:: custom_get_attachment_id( $guid )
//  Returns the attachment ID of an image URL ($guid)
// -------------------------------------------------
if (!function_exists('custom_get_attachment_id')) {
	function custom_get_attachment_id( $guid ) {
	  global $wpdb;
	
	  /* nothing to find return false */
	  if ( ! $guid )
		return false;
	
	  $id = url_to_postid( $guid );
	
	  return $id;
	}
} ?>