<?php 

	$sample_easy_TinyMCE = new Easy_TinyMCE_Class;
	// $sample_easy_TinyMCE->title = 'Insert Testimonial / Quote'; // Popup Window Title
	$sample_easy_TinyMCE->js_url = trailingslashit( get_stylesheet_directory_uri() ) . 'inc/quote-shortcode-tinymce/tinyMCE-plugin.js';
	$sample_easy_TinyMCE->uid = 'quote_shortcode_uid';
	$sample_easy_TinyMCE->templates = array(
		array('name'=> 'Testimonial / Quote', 'url'=> trailingslashit( get_stylesheet_directory() ) .'inc/quote-shortcode-tinymce/quote-TinyMCE.php')
	);
	
?>