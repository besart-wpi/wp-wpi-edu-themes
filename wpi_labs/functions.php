<?php
// Limit Theme Options to Network Admin
// =================================================
function ot_theme_options_custom_cap(){
	return 'manage_network';
}
add_filter('ot_theme_options_capability','ot_theme_options_custom_cap');


define('WPI_POST_TYPE_FRAMEWORKS', true);
// define('WPI_PTF_HOMEPAGESLIDESHOW', true); 
// define('WPI_PTF_TESTIMONIALS', true); 
define('WPI_PTF_PROJECTPORTFOLIO', true); 
// define('WPI_PTF_LOGOPARADE', true);
define('WPI_PTF_PHOTOGALLERY', true);

load_template( trailingslashit( get_template_directory() ) . 'includes/easy-tinymce-class/Easy-tinyMCE.class.php' );
load_template( trailingslashit( get_template_directory() ) . 'includes/easy-tinymce-class/Easy-tinyMCE-content.class.php' );

// =================================================
// 	:: Strip Empty Paragraphs ::
//  Strip any content wrapped with attribute-less 
//  <p> tags - Like: <p>Sample</p>
//  This is a workaround for the_editor() stripping
//  line breaks and adding paragraph tags before
//  content is sent to the database. It causes
//  oEmbed to not work in this situation.
// -------------------------------------------------
function strip_empty_paragraphs($content){
	$content = preg_replace("/<p>(.*?)<\/p>/", "$1" . PHP_EOL . PHP_EOL, $content);
	return $content;
}
add_filter('the_content','strip_empty_paragraphs', 1);

// =================================================
// 	:: Post Order Whitelist ::
// -------------------------------------------------
global $post_order_whitelist;
$post_order_whitelist = array('project_portfolio');

include 'inc/helpers.php';
include 'inc/framework-of-oz/init.php'; class Oz extends Framework_of_Oz{};
include 'inc/post-types.php';
include 'inc/theme-options.php';
// include 'inc/widgets/testimonials.php';
include 'inc/quote-shortcode-tinymce/quote-shortcode-tinymce.php';
if ( !class_exists( 'WP_Widget_Black_Studio_TinyMCE' ) ) {
	include 'lib/black-studio-tinymce-widget/black-studio-tinymce-widget.php';
}
include 'includes/shortcodes/columns.php';

// Filter Yoast Meta Priority
add_filter( 'wpseo_metabox_prio', function() { return 'low';});

// =================================================
// 	:: Menus ::
// -------------------------------------------------
register_nav_menus(array(
	'main'	=> 'Main Navigation'
));

// =================================================
// :: Thumbnails and Favicon::
// -------------------------------------------------
// add_image_size('testimonial-small', 120, 150, true);
add_image_size('project-small', 208, 130, true);
add_filter('upload_mimes', 'customMimes');
function customMimes($mimes){
	$mimes['ico'] = 'image/x-icon';
	return $mimes;
}

// =================================================
// :: Taxonomies ::
// -------------------------------------------------
register_taxonomy('project-categories', 'project_portfolio', array(
	'hierarchical'	=> true,
	'label'			=> 'Category'
));
register_taxonomy_for_object_type('project-categories', 'project_portfolio');

// =================================================
// :: Enqueues ::
// -------------------------------------------------
add_action('wp_enqueue_scripts', 'enqueue_mpi_scripts', 11);
function enqueue_mpi_scripts(){
	wp_deregister_script('wpi-parent-reset');
	
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-fitvids', get_stylesheet_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-cycle-2', get_stylesheet_directory_uri() . '/js/cycle2.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-cycle-2-carousel', get_stylesheet_directory_uri() . '/js/jquery.cycle2.carousel.min.js', array( 'jquery' ), true);
  wp_enqueue_script('jquery-nicescroll', get_stylesheet_directory_uri() . '/js/jquery.nicescroll.min.js', array( 'jquery' ), true);
  wp_enqueue_script('wpi-mpi-main', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ), true);
	
	wp_enqueue_style( 'mpi-styles', get_stylesheet_directory_uri() . '/style.css'); 
	wp_enqueue_style( 'mpi-responsive', get_stylesheet_directory_uri() . '/css/responsive.css'); 
}

// =================================================
// ::  Shortcodes::
// -------------------------------------------------

// ======================================================
// :: Check if shortcode has attribute without a value ::
// ------------------------------------------------------
function is_flag( $flag, $atts ) {
  if (is_array($atts) || is_object($atts)) {
    foreach ( $atts as $key => $value ) {
      if ( $value === $flag && is_int( $key ) ) {
        return true;
      }
    }
    return false;
  }
}

// ======================================================
// :: Get titles of categor(y|ies) ::
// ------------------------------------------------------
function get_project_category_titles($category = '') {
	if (!empty($category)) {
		$term_args = array(
			'slug' => $category
		);
		return get_terms('project-categories', $term_args);
	} else {
    	return get_terms('project-categories');
	}
}


// ======================================================
// :: Get posts of categor(y|ies) ::
// ------------------------------------------------------
function compare_portfolios($a, $b) { //for ordering portfolios by their menu_order id
	return ($a->menu_order > $b->menu_order);
}

function get_project_category_posts($category = '') {
	$output = '';

	if (!empty($category)) {
	    $posts = get_posts(array(
			'post_type' => 'project_portfolio',
			'numberposts' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'project-categories',
					'field' => 'name',
					'terms' => $category
				)
			)
		));
	} else {
	    $posts = get_posts(array(
			'post_type' => 'project_portfolio',
			'numberposts' => -1,
		));
	}

	usort($posts, "compare_portfolios"); //sort using the custom comparator function we declared above [compare_portfolios() to find usage]

	foreach ($posts as $post) {
		$image = '';
		$images = get_post_meta($post->ID, 'portfolio_gallery_items', true);

		if (has_post_thumbnail($post)) {
			$image .= get_the_post_thumbnail_url($post->ID, 'medium');
		} elseif($images) {
			$imageID = current(explode(',', $images));
			$image = wp_get_attachment_image_src($imageID, 'project-small');
			$image = $image[0];
		} else {
			$image = ot_get_option('header_logo');//default case
		}

		$output .= '<div class="portfolio-thumb-wrap c3 post-id-'.$post->ID.'">';
		$output .= '<a href="' . get_post_permalink($post->ID) . '" class="portfolio-thumb" title="' . $post->post_title . '">';
		$output .= '<header style="background: url(' . $image . ') no-repeat center; background-size: cover"></header>';
		$output .= '<article>';
		$output .= '<p><strong>' . $post->post_title . '</strong></p>';
		$output .= '</article>';
		$output .= '</a>';
		$output .= '</div>';

	}
	return $output;
}

/**
 * Displays the current year
 * @return [STR] The month...just kidding, the year
 */
function shortcode_year(){
	return date('Y');
}
add_shortcode('year', 'shortcode_year');

// =================================================
// :: Shortcode for listing project portfolios ::
// -------------------------------------------------
function project_portfolios($atts){
    $output = '';

    /* 
     * We take in a title and a category as attributes. 
     * The title is a string that shows up as a heading.
     * The category is the name of the category, not the slug.
     * If there is no category specified and no title, all items are displayed
     * together separated by category. If there is no category and a title, 
     * the title shows up at the top and all items are displayed one after 
     * the other not separated.
     * 
     * If there is a category specified, by default the title is the name of
     * the category. If there is a category and a title, the default category
     * title is replaced by the value of the title attribute.
     *
     * There is also a hide_title attribute that can be added to the shortcode.
     * If the hide_title attribute is used with no category specified, all items
     * are displayed together with nothing separating them. 
     */

	$a = shortcode_atts( array(
        'title' => '',
        'category' => '',
    ), $atts );


	// If the category attribute is not set.
    if (empty($a['category'])) {
    	// Get all of the categories that have articles associated with them.
    	$category_titles = get_project_category_titles('');
    	
    	// If the title attribute is set, add it to the top of all of the items.
    	if (!empty($a['title'])) {
    		$output .= '<h2 id="project-category-title" class="project-category-title">' . $a['title'] . '</h2>';
    	}

    	// If there is not hide title attribute and there is not title override,
    	// print the title for every category followed by the items in the category
	    if (!is_flag('hide_title', $atts) && empty($a['title'])) {
	    	foreach ($category_titles as $cat) {
	    		$output .= '<div class="clearfix project-category-wrapper category-' . $cat->slug . '">';
		    	$output .= '<h2 id="project-category-title-' . $cat->slug . '" class="project-category-title">' . $cat->name . '</h2>';
	    		$output .= get_project_category_posts($cat->name);
		    	$output .= '</div>';
		    }
	    } else {
	    	$output .= get_project_category_posts();
	    }

    } else {
    	// If there is a category specified wrap the output
    	$output .= '<div class="clearfix project-category-wrapper">';
    	
    	// If the title should not be hidden
    	if (!is_flag('hide_title', $atts)) {
    		// Check if there is a title override, if yes, use the new title
    		// if there's no title override, use the category as the title
    		$title = !empty($a['title']) ? $a['title'] : $a['category'];
	    	$output .= '<h2 id="project-category-title-' . $a['category'] . '" class="project-category-title">' . $title . '</h2>';
	    }

	    // Get all posts with the specified category
    	$output .= get_project_category_posts($a['category']);
	    $output .= '</div>';
    }

    return $output;
}
add_shortcode('projects_listing', 'project_portfolios');

// =================================================
// :: Add Editor Style ::
// -------------------------------------------------
add_action('init', 'custom_editor_styles');
function custom_editor_styles(){
	add_editor_style('css/editor.css');
}
add_filter('mce_buttons_2', 'add_styleselect_to_tinymce');
function add_styleselect_to_tinymce( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter( 'tiny_mce_before_init', 'custom_tinymce_before_inserts' );  
function custom_tinymce_before_inserts($init){
	$formats = array(
		array(
			'title'		=> 'Callout',
			'block'		=> 'div',
			'classes'   => 'inline-callout',
			'wrapper'	=> true
		)
	);
	$init['style_formats'] = json_encode($formats);
	return $init;
}

// =================================================
// ::  Sidebars ::
// -------------------------------------------------
register_sidebar(array(
	'name'	=> 'News',
	'id'	=> 'news',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
));
register_sidebar(array(
	'name'	=> 'Global',
	'id'	=> 'global',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
));

register_sidebar(array(
	'name'	=> 'Home Page',
	'id'	=> 'home',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => '</h3>'
));
register_sidebar(array(
	'name'	=> 'Home Page Footer',
	'id'	=> 'homefooter',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => '</h3>'
));

/**
 * Helps determine if the content should be displayed 
 * in full width or not
 * 
 * @param  string $template [Template identifier]
 * @return [STR]
 */
function getSidebar($template = 'default'){
	global $post;
	ob_start();
		switch($template){
			case 'news': 
			case 'single':
				dynamic_sidebar('news');
				if($sidebar = get_post_meta($post->ID, 'sidebar', true)){
					$sidebar = array_merge(array('content'=>''), $sidebar);
					echo apply_filters('the_content', $sidebar['content']);
				}
			break;
			case 'page':
				if($sidebar = get_post_meta($post->ID, 'sidebar', true)){
					$sidebar = array_merge(array('content'=>''), $sidebar);
					echo apply_filters('the_content', $sidebar['content']);
				}
			break;
		}
	return ob_get_clean();
}

// =================================================
// :: Blog ::
// -------------------------------------------------
/**
 * Gets the archive label to show next to "News" 
 * (or whatever it's set to)
 * 
 * @return [STR]
 */
function getArchiveLabel(){
	global $wp_query;
	if(is_category()) {
		$cat = get_category(get_query_var('cat'),false);
		return '- Category:  <strong>' . $cat->name . '</strong>';
	}
	if(is_tag()){
		$tag = get_term_by('slug', get_query_var('tag'), $wp_query->tax_query->queries[0]['taxonomy']);
		return '- Tag: <strong>' . $tag->name . '</strong>';
	}
	if(is_author()){
		return '- Author:  <strong>' . get_query_var('author_name') . '</strong>';
	}
	if(is_archive()){
		return '- Archive: <strong>' . get_query_var('monthnum') . '/' . get_query_var('year') . '</strong>';		
	}
	if(is_search()){
		return '- ' . get_search_query();
	}
}

// =================================================
// :: Get Theme Styles ::
// -------------------------------------------------
function getThemeStyles(){ 
	$colors = array(
		'links' => ot_get_option('color_links'),
		'menu' 	=> ot_get_option('color_menu_bg'),
		'menu-active' => ot_get_option('color_menu_active_bg'),
		'slide'	=> ot_get_option('color_slide_bg'),
		'border-top'=> ot_get_option('color_border_top'),
		'border'=> ot_get_option('color_border'),
		'heading' => ot_get_option('color_heading'),
		'titles' => ot_get_option('color_titles'),
		'quotes' => ot_get_option('color_quote'),
	);
	?>
	
	<style>
		/** Menus **/
		header #menu-main a:hover {
		  background: <?= $colors['menu'] ?>;
		  opacity: 1;
		}
		header li.current-menu-item a, header li.current-menu-parent > a {
		  <?php if($colors['menu-active']): ?>
		  	background: <?= $colors['menu-active'] ?>;
		  <?php else: ?>
		  	background: <?= $colors['menu'] ?>;
		  <?php endif; ?>
		  color: #fff;
		}
		header li:hover, header li:hover > a{
		  background: <?= $colors['menu'] ?>;
		  color: #fff;	
		}
		#mobile-menu li.current-menu-item > a, #mobile-menu li.current-menu-parent > a {
		  <?php if($colors['menu-active']): ?>
		  	background: <?= $colors['menu-active'] ?>;
		  <?php else: ?>
		  	background: <?= $colors['menu'] ?>;
		  <?php endif; ?>
		}
		#project-categories li a.selected, #project-categories li a:hover{
		  color: <?= $colors['menu'] ?>;
		}
		header .sub-menu li, header .sub-menu li a{
			<?php $colors_menu_rgb = hex2rgb( $colors['menu'] ); ?>
			background: <?= $colors['menu'] ?>;
			background-color: rgba(<?php echo $colors_menu_rgb[0]; ?>, <?php echo $colors_menu_rgb[1]; ?>, <?php echo $colors_menu_rgb[2]; ?>, 0.3) !important;
			color: #fff;
		}
		header .sub-menu li:hover, header .sub-menu li a:hover{
			background: #<?= darken_color($colors['menu'], 1) ?> !important;
		}

		/** Border Colors **/
		header li > a:after{
		  border-color: <?= $colors['menu'] ?> transparent transparent transparent;
		}
		
		header li.current-menu-item > a:after,
		header li.current-menu-parent > a:after {
		  <?php if($colors['menu-active']): ?>
		  	border-color: <?= $colors['menu-active'] ?> transparent transparent transparent;
		  <?php else: ?>
		  	border-color: <?= $colors['menu'] ?> transparent transparent transparent;
		  <?php endif; ?>
		}
		
		#the-content:before{
			background: <?= $colors['border'] ?>;
		}
		#the-content{
			border-top: 7px solid <?= $colors['border-top'] ?>;
		}

		/** Slide Background **/
		.front-page.cycle-slideshow h1:before {
		  background: <?= $colors['slide'] ?>;
		}

		/** Links **/
		a{color: <?= $colors['links'] ?>;}
		.portfolio-thumb:hover header{
		  border-color: <?= $colors['border'] ?>;
		}
		
		/** Heading **/
		h3{
		  color: <?= $colors['heading'] ?>;
		}
		h2 a, #the-content h1{color: <?= $colors['heading'] ?>;}
		#the-content h2 {color: <?= $colors['heading'] ?>;}

		/** Titles **/
	    .hero header{
			color: <?= $colors['titles'] ?>;
	    }
	    .hero.imaged h1:before{
	    	background: <?= $colors['titles'] ?>;
	    }
	</style>
<?php }

// =================================================
// :: Logo Parade Shortcode ::
// -------------------------------------------------
add_shortcode( 'parade', 'logo_parade_shortcode' );
function logo_parade_shortcode($atts) {
	ob_start(); ?> 
    
    <?php 
		$args = array(
			'post_type' 		=> 'logo_parade',
			'posts_per_page' 	=> -1,
			'orderby' 			=> 'menu_order',
			'order' 			=> 'ASC'
		);
		$logo_parade_logos = new WP_Query( $args ); 
		$atts = shortcode_atts(array(
			'manual'	=> false
		), $atts);

		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		// Add random number to pagers
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		$nextID = 'next-' . rand();
		$prevID = 'prev-' . rand();
	?>
	
	<?php if ( $logo_parade_logos->have_posts() ) : ?>
		<style>
			div.logo_parade {
				margin: 20px 0px;
			}
			div.logo_parade img { 
				width: auto; 
				height: auto; 
				vertical-align: middle;
				}
		</style>
		<div class="logo-parade-wrap">
			<div class="cycle-slideshow logo_parade" 
				data-cycle-loader=wait
				data-cycle-fx=carousel
				data-cycle-carousel-visible=3
				data-cycle-carousel-fluid=true
				data-cycle-auto-height="calc"
				data-cycle-pause-on-hover="true"
				data-cycle-slides="> .logo_parade_slide"
				<?php if($atts['manual']): ?>
					data-cycle-next="#<?= $nextID ?>"
					data-cycle-prev="#<?= $prevID ?>"
					data-cycle-timeout=0
                <?php else: ?>
                	data-cycle-timeout=2000
				<?php endif; ?>
			>
			
				<?php while ( $logo_parade_logos->have_posts() ) : $logo_parade_logos->the_post(); 
					
					
					$logo_url = get_post_meta( get_the_ID(), 'logo_url', true);
					?>
					
					<div class="logo_parade_slide">
						<?php echo $logo_url ? '<a href="'.$logo_url.'">' : ''; ?> 
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' ); ?>
						<img src="<?php echo $thumb['0']; ?>" alt="" />
						<?php echo $logo_url ? '</a>' : ''; ?>
					</div>
					
				<?php endwhile; ?>

			</div>
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Pagers
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			if($atts['manual']): ?>
				<div class="carousel pagers">
					<a class="prev" id="<?= $prevID ?>" href="#">&lt;</a>
					<a class="next" id="<?= $nextID ?>" href="#">&gt;</a>
				</div>
			<?php endif; ?>
		</div>
	<?php wp_reset_postdata(); endif; ?>
    
    <?php return ob_get_clean();
}

function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}


// =================================================
// :: Homepage Slideshow Meta Box ::
// -------------------------------------------------
add_action( 'admin_init', 'wpi_mpi_homepage_slideshow_meta_box' );

function wpi_mpi_homepage_slideshow_meta_box() {
	$slideshow_meta_box = array(
		'id'          => 'slideshow_meta_box',
		'title'       => __( 'Slide Details', 'wpi-parent' ),
		'desc'        => '',
		'pages'       => array( 'home_page_slideshow' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'label'       => __( 'Slide Link URL', 'wpi-mpi' ),
				'desc' 		  => __( 'This link will apply to the entire slide image', 'wpi-mpi'),
				'id'          => 'link_url',
				'type'        => 'text'
			),
		)
	);
	
	if ( function_exists( 'ot_register_meta_box' ) )
		ot_register_meta_box( $slideshow_meta_box );
}