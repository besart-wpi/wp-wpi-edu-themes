<?php

/* Template Name: Features */

get_header(); ?>

<?php the_post(); ?>
    
    <h1 class="sr-only"><?php the_title(); ?></h1>
    
    <section class="section features">
        <div class="wrap">
            <?php
            $allFeats = array();
            $currentIssue = wpi_journal_current_issue();

            $currentFeats = get_posts( array( 'post_type' => 'article', 'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key'   => 'feature_article',
                        'value' => '1',
                    )
                ),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'issue_cat',
                        'field'    => 'term_id',
                        'terms'    => $currentIssue->term_id
                    ),
                )
            ) );

            if( $currentFeats ){
                foreach( $currentFeats as $item ) $allFeats[] = $item->ID;
            }

            $arg = array( 'post_type' => 'article', 'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key'   => 'feature_article',
                        'value' => '1',
                    )
                ) );

            if( ! empty( $allFeats ) ){
                $arg = array_merge( $arg, array( 'post__not_in' => $allFeats ) );
            }

            $olderFeats = get_posts( $arg );

            if( $olderFeats ){
                foreach( $olderFeats as $item ) $allFeats[] = $item->ID;
            }

            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $articles = new WP_Query( array( 'post_type' => 'article', 'paged' => $paged, 'post__in' => $allFeats, 'orderby' => 'post__in', 'order' => 'ASC' ) );
            if( $articles->have_posts() ): ?>
                <div class="list">
                    <?php while( $articles->have_posts() ) : $articles->the_post();
                        $post_id        = $post->ID;
                        $subheading     = $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content, 16, '...' );
                        $image          = false;
                        $youtube_video_id = false;
                        
                        $hero_slides    = get_field( 'hero_slides', $post->ID );

                        if( $hero_slides ){
                            $image              = $hero_slides[0][ 'image' ];
                            $youtube_video_id   = $hero_slides[0][ 'youtube_video_id' ];
                        }
                        include( get_stylesheet_directory() . '/parts/item-article.php' );
                    endwhile; ?>
                </div>
                <?php if (function_exists('wp_pagenavi')) { wp_pagenavi( array( 'query' => $articles ) ); } ?>
            <?php else: ?>
                <h3>No articles found.</h3>
            <?php endif; wp_reset_query(); ?>
        </div>
    </section>

<?php get_footer(); ?>