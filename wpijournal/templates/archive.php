<?php

/* Template Name: Archive */

get_header(); ?>

<?php the_post(); ?>

    <?php
    $showPage   = sanitize_text_field( filter_input(INPUT_GET, 'show') );
    $page       = ( get_query_var('paged') ) ? get_query_var( 'paged' ) : 1;
    $perPage    = $showPage ? $showPage : 8; // number of tags to show per-page
    $offset     = ( $page - 1 ) * $perPage;
    $taxonomy   = 'issue_cat';
    $args       = array( 'number' => $perPage, 'offset' => $offset, 'hide_empty' => false,
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'meta_key' => 'issue_date',
        'meta_query' => array( array( 'key' => 'show_in_archive', 'value'   => '1' ) )
    );
    $taxTerms  = get_terms( $taxonomy, $args ); ?>

    <section class="section archive">
        <div class="wrap">
            <div class="upper">
                <h1>Archive</h1>
                <div class="tools">
                    <div class="showing">
                        <h6 class="label">Showing</h6>
                        <div class="count">
                            <a title="Show list by 8" <?php echo ( $showPage == 8 || ! $showPage ) ? 'class="current"' : ''; ?> href="<?php the_permalink() ?>?show=8">8</a>
                            <a title="Show list by 12" <?php echo ( $showPage == 12 ) ? 'class="current"' : ''; ?> href="<?php the_permalink() ?>?show=12">12</a>
                            <a title="Show list by 20" <?php echo ( $showPage == 20 ) ? 'class="current"' : ''; ?> href="<?php the_permalink() ?>?show=20">20</a>
                        </div>
                    </div>
                    <div class="view-as">
                        <h6 class="label">View As</h6>
                        <div class="layout">
                            <a title="View layout by list" href="#" data-view="list"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/view-list.svg" class="svg" alt=""> <span>List</span></a>
                            <a title="View layout by grid" href="#" data-view="grid" class="current"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/view-grid.svg" class="svg" alt=""> <span>Grid</span></a>
                        </div>
                    </div>
                </div>
            </div>

            <?php if( $taxTerms ){ ?>
                <div class="list view-grid" id="archive-list">
                    <?php
                    $counter = 1;
                    foreach( $taxTerms as $taxTerm ){
                        $key = $taxonomy . '_' . $taxTerm->term_id;
                        $issue_cover    = get_field( 'issue_cover_image', $key );
                        $issue_pdf      = get_field( 'issue_pdf', $key );
                        $issue_date     = get_field( 'issue_date', $key ); ?>
                        <div class="item">
                            <figure>
                                <a title="<?php echo $taxTerm->name; ?>" href="#<?php echo sanitize_title( $taxTerm->name ) ?>-<?php echo $counter ?>-modal" data-fancybox="issue-image"><span class="sr"><?php echo $taxTerm->name; ?></span></a>
                                <?php if( $issue_cover ){ ?>
                                    <?php echo wp_get_attachment_image( $issue_cover['ID'], 'thumb-archive' ); ?>
                                <?php }else{ ?>
                                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/no-archive.php" alt="<?php echo $taxTerm->name; ?>">
                                <?php }?>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/expand.svg" alt="Click to enlarge" class="svg">
                                <span class="before"></span><span class="after"></span>
                            </figure>
                            <div class="modal-content" id="<?php echo sanitize_title( $taxTerm->name ) ?>-<?php echo $counter ?>-modal" style="width: <?php echo $issue_cover[ 'width' ] ?>px;">
                                <figure><?php echo wp_get_attachment_image( $issue_cover[ 'ID' ], 'image-modal' ); ?></figure>
                                <figcaption>
                                    <?php echo $taxTerm->name ? sprintf( '<h4>%s</h4>', $taxTerm->name ) : ''; ?>
                                </figcaption>
                            </div>
                            <div class="text">
                                <h3><?php echo $taxTerm->name; ?></h3>
                                <div class="links">
                                    <?php echo ( $taxTerm->count > 0 ) ? sprintf( '<a href="%s" title="Online Stories">Online Stories</a>', get_term_link( $taxTerm->slug, $taxonomy ) ) : ''; ?>
                                    <?php echo ( $issue_pdf && $taxTerm->count > 0 ) ? sprintf( '<span></span>' ) : ''; ?>
                                    <?php echo $issue_pdf ? sprintf( '<a href="%s" title="Download PDF" target="_blank" rel="noopener noreferrer">Download PDF</a>', $issue_pdf ) : ''; ?>
                                </div>
                            </div>
                        </div>
                    <?php $counter ++;
                    } ?>
                </div>

                <?php
                $total_terms = wp_count_terms( $taxonomy, array( 'hide_empty' => false, 'meta_query' => array( array( 'key' => 'show_in_archive', 'value'   => '1' ) ) ) );
                $pages = ceil( $total_terms / $perPage );
                if( $pages > 1 ){ // if there's more than one page ?>
                    <div class="wp-pagenavi">
                        <?php if( $page > 1 ){ ?>
                            <a href="<?php the_permalink(); ?>page/<?php echo $page - 1 ?>/" class="previouspostslink">Prev</a>
                        <?php } ?>

                        <?php
                        for( $pagecount = 1; $pagecount <= $pages; $pagecount++ ):
                            if( ! empty( $showPage ) )
                                $param = '?show=' . $showPage;
                            else
                                $param = '';

                            if( $pagecount == $page )
                                echo sprintf( '<span class="current">%s</span>', $pagecount );
                            else
                                echo sprintf( '<a href="%spage/%s/%s">%s</a>', get_permalink(), $pagecount, $param, $pagecount );
                        endfor; ?>
                        
                        <?php if( $page != $pages && $pages > 1 ){ ?>
                            <a href="<?php the_permalink(); ?>page/<?php echo $page + 1 ?>/" class="nextpostslink">Next</a>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php }else{ ?>
                <h3>No issues found.</h3>
            <?php } ?>
        </div>
    </section>

<?php get_footer(); ?>