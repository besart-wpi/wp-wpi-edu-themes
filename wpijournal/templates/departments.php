<?php

/* Template Name: Departments */

get_header(); ?>

<?php the_post(); ?>
    
    <h1 class="sr-only"><?php the_title(); ?></h1>
    
    <section class="section departments">
        <div class="wrap">
            <?php
            $currentIssue = wpi_journal_current_issue();
            if( ! empty( $currentIssue ) ){
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $departments_page = get_field( 'departments_page', 'option' );
                    $currentOrder = get_field( 'articles_ids', $departments_page );

                    $arg = array( 'post_type' => 'article', 'paged' => $paged,
                        'meta_query' => array(
                            array(
                                'key'   => 'feature_article',
                                'value' => '1',
                                'compare' => '!=',
                            )
                        ),
                        'tax_query' => array(
                            'relation' => 'AND',
                            array(
                                'taxonomy' => 'issue_cat',
                                'field'    => 'term_id',
                                'terms'    => $currentIssue->term_id
                            ),
                            array(
                                'taxonomy' => 'department_cat',
                                'field'    => 'slug',
                                'terms'    => 'wpie',
                                'operator' => 'NOT IN'
                            )
                        ),
                        'orderby' => 'menu_order',
                        'order' => 'ASC'
                    );

                    if( ! empty( $currentOrder ) ){
                        $arg = array_merge( $arg, array( 'post__in' => explode( ',', $currentOrder ), 'orderby' => 'post__in' ) );
                    }

                    $articles = new WP_Query( $arg );
                    if( $articles->have_posts() ): ?>
                        <div class="list">
                            <?php while( $articles->have_posts() ) : $articles->the_post();
                                $post_id        = $post->ID;
                                $subheading     = $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content, 16, '...' );
                                $image          = false;
                                $youtube_video_id = false;
                                
                                $hero_slides    = get_field( 'hero_slides', $post->ID );

                                if( $hero_slides ){
                                    $image              = $hero_slides[0][ 'image' ];
                                    $youtube_video_id   = $hero_slides[0][ 'youtube_video_id' ];
                                }
                                include( get_stylesheet_directory() . '/parts/item-article.php' );
                            endwhile; ?>
                        </div>
                        <?php if (function_exists('wp_pagenavi')) { wp_pagenavi( array( 'query' => $articles ) ); } ?>
                    <?php else: ?>
                        <h3>No articles found.</h3>
                    <?php endif; wp_reset_query();
            }else{
                echo '<h3>No articles found.</h3>';
            } ?>
        </div>
    </section>

<?php get_footer(); ?>