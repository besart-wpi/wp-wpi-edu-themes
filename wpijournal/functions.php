<?php
	// Add Theme Support
    function wpi_journal_theme_support(){
        add_theme_support( 'responsive-embeds' );
        add_theme_support( 'title-tag' );
    }
    add_action('after_setup_theme','wpi_journal_theme_support');


	// Include plugins
	add_action('after_setup_theme', 'wpi_journal_embed_plugin');
	function wpi_journal_embed_plugin() {
		if (!class_exists('wp_pagenavi')) {
			include_once( TEMPLATEPATH . '/assets/wp-pagenavi/wp-pagenavi.php');
		}
	}


	// ACF
	add_filter('acf/settings/save_json', 'wpi_journal_custom_acf_json_save_point');
	function wpi_journal_custom_acf_json_save_point( $path ) {
		$path = get_template_directory() . '/acf-json';
		return $path;
	}

	add_filter('acf/settings/load_json', 'custom_acf_json_load_point');
	function custom_acf_json_load_point( $paths ) {
		unset($paths[0]);
		$paths[] = get_template_directory() . '/acf-json';
		return $paths;
	}

	if( function_exists('acf_add_options_page') ) {
        // add parent
        $parent = acf_add_options_page(array(
                'page_title' 	=> 'Theme Options',
                'menu_title' 	=> 'Theme Options',
                'redirect' 		=> false
        ));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Footer',
			'menu_title' 	=> 'Footer',
			'parent_slug' 	=> $parent['menu_slug'],
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Day Mode / Night Mode',
			'menu_title' 	=> 'Day Mode / Night Mode',
			'parent_slug' 	=> $parent['menu_slug'],
		));
	}

	add_action('acf/input/admin_enqueue_scripts', 'wpi_journal_acf_admin_enqueue_scripts');
	function wpi_journal_acf_admin_enqueue_scripts() {
		wp_enqueue_style( 'custom-acf-input-css', get_stylesheet_directory_uri() . '/custom-acf-input.css', false, '1.0.0' );
		wp_enqueue_script( 'custom-acf-input-js', get_stylesheet_directory_uri() . '/assets/scripts/custom-acf-input.min.js', false, '1.0.0' );
	}


	// Gutenberg
	add_action('acf/init', 'wpi_journal_custom_acf_init');
	function wpi_journal_custom_acf_init() {

		if( function_exists('acf_register_block') ) {

			acf_register_block(array(
				'name'				=> 'two-col-image-text',
				'title'				=> __('Two-column Image and Text Block'),
				'description'		=> __('A two-column image and text block.'),
				'render_callback'	=> 'wpi_journal_custom_acf_block_render_callback',
				'category'			=> 'formatting',
				'icon'				=> 'align-left',
				'keywords'			=> array( 'image text', 'two column image & text' ),
			));

			acf_register_block(array(
				'name'				=> 'image-caption-credit',
				'title'				=> __('Image with Caption and Credit Block'),
				'description'		=> __('A image with caption and credit block.'),
				'render_callback'	=> 'wpi_journal_custom_acf_block_render_callback',
				'category'			=> 'formatting',
				'icon'				=> 'format-image',
				'keywords'			=> array( 'image credit', 'image with caption and credit' ),
			));

			acf_register_block(array(
				'name'				=> 'simple-quote',
				'title'				=> __('Simple Quote Block'),
				'description'		=> __('A simple quote block.'),
				'render_callback'	=> 'wpi_journal_custom_acf_block_render_callback',
				'category'			=> 'common',
				'icon'				=> 'format-status',
				'keywords'			=> array( 'simple quote', 'quote' ),
			));
		}
	}

	function wpi_journal_custom_acf_block_render_callback( $block ) {

		$slug = str_replace('acf/', '', $block['name']);

		// include a template part from within the "parts/block" folder
		if( file_exists( get_theme_file_path("/parts/block-{$slug}.php") ) ) {
			include( get_theme_file_path("/parts/block-{$slug}.php") );
		}
	}

	add_filter( 'render_block', 'wpi_journal_custom_blocks', 10, 2 );
	function wpi_journal_custom_blocks( $block_content, $block ) {
		// print_r( $block );
		if( 'core/paragraph' == $block['blockName'] ) {
			if( array_key_exists( 'dropCap', $block[ 'attrs' ] ) ){
				$return  = '<div class="post-item drop-caps"><div class="inner">';
				$return .= str_replace( 'has-drop-cap', 'custom-has-drop-cap', $block_content );
			}else{
				$return  = '<div class="post-item standard"><div class="inner">';
				$return .= $block_content;
			}
			$return .= '</div></div>';
		}else{
			return $block_content;
		}
		return $return;
	}


	// Thumbnails
	// add_theme_support('post-thumbnails', array( 'feat_post' ));
	add_image_size('thumb-slider', 1920, 1164, true);
	add_image_size('thumb-highlight', 660, 400, true);
	add_image_size('thumb-gallery', 960, 600, true);
	add_image_size('thumb-story', 366, 222, true);
	add_image_size('thumb-faculty', 417, 9999, false);
	add_image_size('thumb-menu-issue', 100, 120, true);
	add_image_size('thumb-archive', 363, 436, true);
	add_image_size('image-credit', 876, 9999, false);
	add_image_size('image-modal', 1920, 1920, false);


	// Register Menus
	register_nav_menu( 'primary-menu', 'Primary' );


	// Highlight 'Archive' in primary nav when viewing a non-current Issue
	add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2);
	function current_type_nav_class($classes, $item) {
		$currentIssue = wpi_journal_current_issue();

		if( ! is_tax( 'issue_cat', $currentIssue->slug ) && !is_front_page() && !(is_singular('article') && has_term($currentIssue->term_id, 'issue_cat')) ){
			if( in_array( 'article-menu-item', $classes ) ){
				array_push($classes, 'current-menu-item');
			}
		}
	    return $classes;
	}
	
	
	// Add Current Issue and Search to Primary Nav
	add_filter( 'wp_nav_menu_items', 'wpi_journal_add_menu_items', 10, 2 );
	function wpi_journal_add_menu_items( $items, $args ) {
		global $wp_query;

		// default active item
		if( strpos( $items, 'current-menu-item' ) !== false)
			$searchActiveClass = '';
		else
			$searchActiveClass = 'current-menu-item';

		// current issue active item
		$currentIssue = wpi_journal_current_issue();
		if( $currentIssue ){
			if( is_tax( 'issue_cat', $currentIssue->slug ) ){
				$issueActiveClass = 'current-menu-item';
            } elseif ( is_singular('article') && has_term($currentIssue->term_id, 'issue_cat') ){ 
                $issueActiveClass = 'current-menu-item';
			} else {
				$issueActiveClass = '';
            }

			$current = sprintf( '<li class="current-issue %s"><a href="%s" title="Current Issue">Current Issue</a></li>', $issueActiveClass, get_term_link( $currentIssue->slug, 'issue_cat' ) );
		}

		if( $args->theme_location == 'primary-menu' ){
			$search = sprintf( '<li class="%s search-item"><a href="#" class="btn-search pop-search"><img src="%s/assets/images/search.svg" alt="search" class="svg"><span class="sr">Search WPI Journal</span></a></li>', $searchActiveClass, get_bloginfo( 'template_url' ) );
			$items = $current . $items . $search;
		}
		return $items;
	}


	// Eneuque Scripts and Styles
    function wpi_journal_custom_scripts() {
		$version = '1.0.0';
		global $is_IE;

    	// Styles
		wp_register_style( 'fonts', '//use.typekit.net/llo6ldt.css', array(), $version );
		if( isset($_GET['debug']) ){
			wp_register_style( 'stylesheet', get_stylesheet_directory_uri() . '/style.css', array(), $version );
		} else {
			wp_register_style( 'stylesheet', get_stylesheet_directory_uri() . '/style.min.css', array(), $version );
		}

        wp_enqueue_style ( 'fonts' );
        wp_enqueue_style ( 'stylesheet' );


        // Scripts
        wp_register_script( 'plugins', get_template_directory_uri() . '/assets/scripts/plugins.min.js', array( 'jquery' ), $version, true );
		if( isset($_GET['debug']) ){
			wp_register_script( 'custom', get_template_directory_uri() . '/assets/scripts/custom.js', array( 'jquery' ), $version, true );
		} else {
			wp_register_script( 'custom', get_template_directory_uri() . '/assets/scripts/custom.min.js', array( 'jquery' ), $version, true );
		}

        wp_enqueue_script( 'plugins' );
        wp_enqueue_script( 'custom' );
    }
    add_action( 'wp_enqueue_scripts', 'wpi_journal_custom_scripts', 10 );

	function wpi_journal_custom_admin_script() {
    	wp_enqueue_style( 'acf-terms-validations-css', get_template_directory_uri() . '/custom-acf-terms-validation.css', array(), $version );
    	wp_enqueue_script( 'acf-terms-validations-js', get_template_directory_uri() . '/assets/scripts/custom-acf-terms-validation.js', array( 'jquery' ), $version, true );
	}
	add_action( 'admin_enqueue_scripts', 'wpi_journal_custom_admin_script' );


	// Register custom post types and taxonomies
	add_action('init', 'article_post_type_init');
	function article_post_type_init() {
		$labels = array(
			'name' => _x('Articles', 'post type general name'),
			'singular_name' => _x('Article', 'post type singular name'),
			'add_new' => _x('Add New', 'Article'),
			'add_new_item' => __('Add new article'),
			'edit_item' => __('Edit article'),
			'new_item' => __('New article'),
			'view_item' => __('View article'),
			'search_items' => __('Search article'),
			'not_found' =>  __('No articles found'),
			'not_found_in_trash' => __('No articles found in trash'), 
			'parent_item_colon' => ''
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'rewrite' => true,
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'_builtin' => false,
			'rewrite' => array( 'slug' => 'articles', 'with_front' => true ),
			'show_in_nav_menus' => true,
			'menu_position' => 20,
			'menu_icon' => 'dashicons-art',
			'show_in_rest' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt'
			)
		);
		register_post_type('article',$args);
	}

	add_action( 'init', 'create_article_taxonomies', 0 );
	function create_article_taxonomies() {
		$tax_issue = array(
			'name' => _x( 'Issues', 'taxonomy general name' ),
			'singular_name' => _x( 'Issue', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search issue' ),
			'all_items' => __( 'All issue' ),
			'parent_item' => __( 'Parent issue' ),
			'parent_item_colon' => __( 'Parent issue:' ),
			'edit_item' => __( 'Edit issue' ),
			'update_item' => __( 'Update issue' ),
			'add_new_item' => __( 'Add new issue' ),
			'new_item_name' => __( 'New issue name' ),
		);

		register_taxonomy( 'issue_cat', array( 'article' ), array(
			'hierarchical' => false,
			'labels' => $tax_issue,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'issue' )
		));

		$tax_department = array(
			'name' => _x( 'Departments', 'taxonomy general name' ),
			'singular_name' => _x( 'Department', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search department' ),
			'all_items' => __( 'All department' ),
			'parent_item' => __( 'Parent department' ),
			'parent_item_colon' => __( 'Parent department:' ),
			'edit_item' => __( 'Edit department' ),
			'update_item' => __( 'Update department' ),
			'add_new_item' => __( 'Add new department' ),
			'new_item_name' => __( 'New department name' ),
		);

		register_taxonomy( 'department_cat', array( 'article' ), array(
			'hierarchical' => false,
			'labels' => $tax_department,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'department' )
		));
	}

	add_filter( 'manage_edit-article_columns', 'custom_edit_article_columns' ) ;
	function custom_edit_article_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'image' => __( 'Image' ),
			'title' => __( 'Name' ),
			'issue' => __( 'Issue' ),
			'dept' => __( 'Department' ),
			'feature' => __( 'Feature' ),
			'date' => __( 'Date' )
		);
		return $columns;
	}

	add_action( 'manage_article_posts_custom_column', 'custom_manage_article_columns', 10, 2 );
	function custom_manage_article_columns( $columns ) {
		global $post;
        
        $first_image = false;
        
		$hero_slides = get_field( 'hero_slides', $post->ID );
		if( $hero_slides ){
			$first_image = $hero_slides[0][ 'image' ];
		}

		switch( $columns ) {

			case 'image' :
				if ( $first_image )
					echo sprintf( '<img src="%s" style="max-width: 50px;" alt="">', $first_image['sizes']['thumbnail'] );
				else
					echo '<img src="' . get_bloginfo( 'template_url' ) . '/assets/images/no-50.png" style="max-width: 50px;" alt="" />';
				break;

			case 'issue' :
				$terms = get_the_terms( $post->ID, 'issue_cat' );
				if ( !empty( $terms ) ) {
					$out = array();
					foreach ( $terms as $term ) {
						$out[] = esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'issue_cat', 'display' ) );
					}
					echo join( ', ', $out );
				}else {
					_e( 'No issue' );
				}

				break;

				case 'dept' :
					$terms = get_the_terms( $post->ID, 'department_cat' );
					if ( !empty( $terms ) ) {
						$out = array();
						foreach ( $terms as $term ) {
							$out[] = esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'department_cat', 'display' ) );
						}
						echo join( ', ', $out );
					}else {
						_e( 'No department' );
					}

					break;

				case 'feature' :
					$feature = get_field( 'feature_article', $post->ID );
					if ( $feature ) {
						echo '<span class="dashicons-before dashicons-yes" style="color: #ca4a1f;"></span>';
					}else {
						echo '<span class="dashicons-before dashicons-no" style="opacity: 0.5;"></span>';
					}

					break;

			default :
				break;
		}
	}

	function add_issue_cat_columns( $columns ) {
		$columns['current_issue'] = 'Current Issue';
		return $columns;
	}
	add_filter( 'manage_edit-issue_cat_columns', 'add_issue_cat_columns' );

	function add_issue_cat_column_content($content,$column_name,$term_id){
		$term = get_term($term_id, 'issue_cat');
		$isCurrent = get_field( 'current_issue', 'issue_cat_' . $term_id );
		switch( $column_name ){
			case 'current_issue':
				//do your stuff here with $term or $term_id
				if( $isCurrent ){
					$content = '<span class="dashicons-before dashicons-yes" style="color: #ca4a1f;"></span>';
				}else{
					$content = '<span class="dashicons-before dashicons-no" style="opacity: 0.5;"></span>';
				}
				break;

			default:
				break;
		}
		return $content;
	}
	add_filter('manage_issue_cat_custom_column', 'add_issue_cat_column_content',10,3);

	// Display a custom taxonomy issue and department dropdown in admin
	add_action('restrict_manage_posts', 'wpi_journal_article_filter_by_taxonomy');
	function wpi_journal_article_filter_by_taxonomy() {
		global $typenow;

		if( $typenow == 'article' ){

			$issue_selected = isset($_GET[ 'issue_cat' ]) ? $_GET[ 'issue_cat' ] : '';
			$issue_cat_name = 'issue_cat';
			$issue_cat 		= get_taxonomy( $issue_cat_name );
			wp_dropdown_categories(array(
				'show_option_all' => sprintf( 'Show all %s', $issue_cat->label ),
				'taxonomy'        => $issue_cat_name,
				'name'            => $issue_cat_name,
				'orderby'         => 'name',
				'selected'        => $issue_selected,
				'show_count'      => true,
				'hide_empty'      => true,
			));

			$department_selected 	= isset($_GET[ 'department_cat' ]) ? $_GET[ 'department_cat' ] : '';
			$department_cat_name 	= 'department_cat';
			$department_cat 		= get_taxonomy( $department_cat_name );
			wp_dropdown_categories(array(
				'show_option_all' => sprintf( 'Show all %s', $department_cat->label ),
				'taxonomy'        => $department_cat_name,
				'name'            => $department_cat_name,
				'orderby'         => 'name',
				'selected'        => $department_selected,
				'show_count'      => true,
				'hide_empty'      => true,
			));
		};
	}

	// Filter articles by taxonomy in admin
	add_filter('parse_query', 'wpi_journal_article_id_to_term');
	function wpi_journal_article_id_to_term($query) {
		global $pagenow;
		global $typenow;

		$q_vars = &$query->query_vars;

		// Check if the user is editing an Article
		if( $pagenow == 'edit.php' && isset($q_vars[ 'post_type' ]) && $q_vars[ 'post_type' ] == 'article' ) {

			$issue_cat = 'issue_cat';
			$issue_obj = get_term_by('id', $q_vars[ $issue_cat ], $issue_cat);
			if( isset($q_vars[ $issue_cat ]) && is_numeric($q_vars[ $issue_cat ]) && $q_vars[ $issue_cat ] != 0 ){
				$q_vars[ $issue_cat ] = $issue_obj->slug;
			}

			$department_cat = 'department_cat';
			$department_obj = get_term_by('id', $q_vars[ $department_cat ], $department_cat);
			if( isset($q_vars[ $department_cat ]) && is_numeric($q_vars[ $department_cat ]) && $q_vars[ $department_cat ] != 0 ){
				$q_vars[ $department_cat ] = $department_obj->slug;
			}
		}
	}



	// Add custom body classes
	function wpi_journal_body_classes($classes) {
		global $wp_query;
        
        // Apply night theme based on cookie preference
        if( isset($_COOKIE['theme']) && $_COOKIE['theme'] == 'night' ){
            $classes[] = 'theme-night';
        }
        
		global $is_IE, $is_edge;

		$is_iphone = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPhone');
		$is_ipad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
		$is_android = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'Android');

		if($is_IE) $classes[] = 'ie';
		elseif($is_edge) $classes[] = 'edge';
		elseif($is_ipad || $is_iphone ) $classes[] = 'ios';
		elseif($is_android) $classes[] = 'android';

		$user_agent = $_SERVER['HTTP_USER_AGENT'];

		// detecting rendering engine
		if ( stripos($user_agent, 'MSIE') !== false ) {
			$classes[] = 'msie';
		}

		if( $wp_query->query_vars['post_type'] == 'article' ){
			$department = get_the_terms( $wp_query->queried_object_id, 'department_cat' );
			if( $department ){
				foreach( $department as $dept ){
					$classes[] = 'department-' . sanitize_key( $dept->slug );
				}
			}
		}

		return $classes;

		return array_unique($classes);
	}
	add_filter('body_class','wpi_journal_body_classes');

    
    function wpi_journal_article_save( $post_id, $post, $update ) {
        if ( 'article' !== $post->post_type ) {
            return;
        }
        
        // Don't allow Feature articles from having any associated Department terms
        $feature_article = get_field( 'feature_article', $post_id );
        if( $feature_article ){
            wp_set_post_terms( $post_id, array(), 'department_cat' );
        }
	}
    add_action( 'save_post', 'wpi_journal_article_save', 10,3 );


	// Get the current Issue
    // Returns a WP_Term object
	function wpi_journal_current_issue(){
		$issues = get_terms( array( 'taxonomy' => 'issue_cat' ) );
		$current_issue = '';
		if( $issues ){
			foreach( $issues as $item ){
				// print_r( $item->taxonomy );
				$isCurrent = get_field( 'current_issue', 'issue_cat_' . $item->term_id );
				// print_r( $isCurrent );
				if( $isCurrent ){
					$current_issue = $item;
				}
			}
		}
		return $current_issue;
	}


    // Filter Queries
	function wpi_journal_custom_article_query($query) {
    
        // Hide "Feature Article" Articles from Department archives
		if ( $query->is_tax( 'department_cat' ) && !is_admin() && $query->is_main_query() ) {
			$arg = array( array( 'key' => 'feature_article', 'value' => '0' ) );
			$query->set( 'meta_query', $arg );
            
        // Show "Feature Article" Articles first when viewing an Issue
		}elseif ( $query->is_tax( 'issue_cat' ) && !is_admin() && $query->is_main_query() ) {
			$currentIssue = get_query_var( 'issue_cat' );
            $issueIDs = array();

            // Get "Feature Article" Articles in the issue being viewed
            $features = get_posts( array( 'post_type' => 'article', 'posts_per_page' => -1, 'meta_key' => 'feature_article', 'meta_value' => '1', 'orderby' => 'title', 'order' => 'ASC',
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'issue_cat',
                        'field'    => 'slug',
                        'terms'    => $currentIssue
                    )
                )
            ) );

            if( $features ){
                foreach( $features as $item ) $issueIDs[] = $item->ID;
            }
            
            // Get Articles associated with a Department in the issue being viewed
            $departments = get_terms( array( 'taxonomy' => 'department_cat', 'orderby' => 'name', 'hide_empty' => false ) );
            foreach( $departments as $dept ){
                $dept_posts = get_posts( array( 'post_type' => 'article', 'posts_per_page' => -1, 'meta_key' => 'feature_article', 'meta_value' => '0', 'orderby' => 'title', 'order' => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'issue_cat',
                            'field'    => 'slug',
                            'terms'    => $currentIssue
                        ),
                        array(
                            'taxonomy' => 'department_cat',
                            'field'    => 'term_id',
                            'terms'    => $dept->term_id
                        ),
                    )
                ) );

                if( $dept_posts ){
                    foreach( $dept_posts as $item ) $issueIDs[] = $item->ID;
                }
			}

			$query->set( 'post__in', $issueIDs );
			$query->set( 'orderby', 'post__in' );
        
        // Show Posts, Articles and Pages in Searches
		}elseif( !is_admin() && is_main_query() && is_search() ){
			$query->set( 'post_type', array( 'post', 'article', 'page' ) );
		}
		return $query;
	}
	add_filter( 'pre_get_posts', 'wpi_journal_custom_article_query' );
    
    
    // Hide "Posts" and "Comments" in Dashboard menu and admin bar
    add_action( 'admin_menu', 'wpi_journal_remove_menu_pages' );
    function wpi_journal_remove_menu_pages() {
        remove_menu_page('edit.php'); // Posts
        remove_menu_page('edit-comments.php'); // Comments
    }

    function wpi_journal_remove_from_admin_bar($wp_admin_bar) {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('new-post'); // hide +New Post
        $wp_admin_bar->remove_node('comments'); // hide Comments
    }
    add_action('wp_before_admin_bar_render', 'wpi_journal_remove_from_admin_bar', 999);

    
    // Only allow a single Issue term to be the "Current Issue"
    function wpi_journal_edited_issue_cat( $term_id, $tt_id ){
        $term = get_term($term_id, 'issue_cat');
		$isCurrent = get_field( 'current_issue', 'issue_cat_' . $term_id );
        
        // Make sure all other issues are not current
        if( $isCurrent ){
        
            $issues = get_terms( array(
                'taxonomy' => 'issue_cat',
                'hide_empty' => false,
                'exclude' => $term_id,
            ) );
            
            foreach( $issues as $issue ){
                update_field('current_issue', '', 'issue_cat_' . $issue->term_id);
            }
            
            // Make sure articles on the Departments page belong to the current issue
            $departments_page = get_field( 'departments_page', 'option' ); // post ID
            $currentOrder = get_field( 'articles_ids', $departments_page ); // string of comma separated post IDs

            // Make sure articles in $currentOrder belong to the current issue. If not, remove them.
            if( !empty($currentOrder) ){
                $currentOrderArticles = explode(',', $currentOrder); // convert to array, $currentOrder is a string
                $currentOrderArticlesChecked = array();

                if( is_array($currentOrderArticles) && !empty( $currentOrderArticles) ){
                    foreach( $currentOrderArticles as $currentOrderArticle ){
                        // check if the article belongs to current issue
                        if( has_term($term_id, 'issue_cat', $currentOrderArticle) ){
                            $currentOrderArticlesChecked[] = $currentOrderArticle;
                        }
                    }
                }
                if( $currentOrder !== implode(',', $currentOrderArticlesChecked) ){
                    update_field( 'articles_ids', $currentOrderArticlesChecked, $departments_page); // update ACF field
                }
            }
        }
    }
    add_action( 'edited_issue_cat', 'wpi_journal_edited_issue_cat', 10, 2 );