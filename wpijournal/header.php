<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://use.typekit.net/llo6ldt.css">
<?php wp_head(); ?>
<?php include( get_stylesheet_directory() . '/parts/colors.php' ); ?>
</head>
<body <?php body_class(); ?>>
<div id="page">
    <header id="header" class="site-header">
        <div class="wrap">
            <div class="logo"><a href="<?php bloginfo( 'url' ); ?>" title="<?php bloginfo( 'name' ); ?>" aria-label="<?php bloginfo( 'name' ); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/logo.svg" alt="<?php bloginfo( 'name' ); ?>" class="svg" ></a></div>
            <nav class="primary">
                <?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => false, 'menu_class' => 'menu primary-ul sf-menu', 'depth' => 2 ) ); ?>
            </nav>
            <button class="mobile-menu-toggle">
                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/menu.svg" alt="Open Menu" class="svg menu">
                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/close.svg" alt="Close Menu" class="svg close">
            </button>
            <button class="mobile-search-toggle pop-search" tabindex="-1"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="Search" class="svg"><span class="sr">Search WPI Journal</span></button>
        </div>
    </header>
    <div class="scroll-progress" id="progress"></div>
    <div id="body">