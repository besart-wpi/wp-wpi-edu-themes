<?php get_header(); ?>

    <section class="section content">
        <div class="wrap">
            <main class="center">
                <div class="item-head">
                    <h1><?php the_title(); ?></h1>
                </div>
                
                <?php 
                global $page, $numpages;
                if( $page > 1 ) {
                    echo '<div class="post-item standard"><div class="inner">';
                    echo '<p class="page-number">Page ' . $page . ' of ' . $numpages . '</p>';
                    echo '</div></div>';
                } ?>
                
                <?php the_content(); ?>
                
                <?php if (function_exists('wp_pagenavi')) { wp_pagenavi( array( 'type' => 'multipart' ) ); } ?>
            </main>
        </div>
    </section>

<?php get_footer(); ?>