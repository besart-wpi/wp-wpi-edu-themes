    </div><!-- EOF BODY -->
    <div id="body-search"></div>
    <footer id="footer" class="site-footer">
        <div class="wrap">
            <a href="https://www.wpi.edu" title="WPI Homepage" aria-label="WPI Homepage" class="logo-footer">
                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/logo-footer.svg" alt="<?php bloginfo( 'name' ); ?>" class="svg colored">
                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/logo-footer-white.svg" alt="<?php bloginfo( 'name' ); ?>" class="svg white">
            </a>
            <?php
            $address_heading    = get_field( 'address_heading', 'option' );
            $address            = get_field( 'address', 'option' );
            $phone_number       = get_field( 'phone_number', 'option' );
            $button_cta         = get_field( 'button_cta', 'option' ); ?>
            <div class="contacts">
                <?php echo $address_heading ? sprintf( '<h6>%s</h6>', $address_heading ) : ''; ?>
                <p>
                <?php echo $address ? $address : ''; ?>
                <?php echo $phone_number ? sprintf( '<br>Ph: <a href="tel:%s">%s</a>', $phone_number, $phone_number ) : ''; ?>
                </p>
            </div>
            <?php if( $button_cta ){ ?>
				<a href="<?php echo $button_cta[ 'url' ]; ?>" class="btn class-notes" <?php if( $button_cta[ 'target' ] == '_blank' ){ ?>target="_blank" rel="noopener noreferrer"<?php } ?>><?php echo $button_cta[ 'title' ] ?></a>
			<?php } ?>
        </div>
    </footer>

    <div class="theme-toggler sticky">
        <?php get_template_part( 'parts/theme-toggler' ); ?>
    </div>

    <section class="section search-bar">
        <div class="wrap">
            <button class="close-search" tabindex="-1"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/close.svg" alt="Close Search" class="svg"><span class="sr">Close Menu</span></button>
            <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" id="site-search">
                <div class="fields">
                    <input type="search" title="Search WPI Journal"  tabindex="-1" name="s" value="<?php echo get_search_query(); ?>" placeholder="Search WPI Journal..." autocomplete="off">
                    <button class="btn go-search" tabindex="-1">Go</button>
                </div>
            </form>
        </div>
    </section>

    <nav id="mobile-menu">
        <div>
            <?php
            $currentIssue = wpi_journal_current_issue();
            if( $currentIssue ){
                $taxonomy = 'issue_cat';
                $key =  $taxonomy . '_' . $currentIssue->term_id;
                $issue_cover    = get_field( 'issue_cover_image', $key ); ?>
                <div class="current">
                    <figure>
                        <?php if( $issue_cover ){ ?>
                            <?php echo wp_get_attachment_image( $issue_cover['ID'], 'thumb-archive' ); ?>
                        <?php }else{ ?>
                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/no-archive.php" alt="<?php echo $currentIssue->name; ?>">
                        <?php }?>
                    </figure>
                    <div class="text">
                        <h6>Current Issue</h6>
                        <h4><?php echo $currentIssue->name; ?></h4>
                        <a href="<?php echo get_term_link( $currentIssue->slug, $taxonomy ) ?>" class="link-arrow">View Issue</a>
                    </div>
                </div>
            <?php } ?>

            <?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => false, 'menu_class' => 'menu', 'depth' => 2 ) ); ?>
            <div class="toggler">
                <div class="theme-toggler mobile">
                    <?php get_template_part( 'parts/theme-toggler' ); ?>
                </div>
            </div>
        </div>
    </nav>
</div>
<?php wp_footer(); ?>
</body>
</html>