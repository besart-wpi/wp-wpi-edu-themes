<?php get_header(); ?>

<?php $search_query = get_search_query(); ?>

    <section class="section search-results">
        <div class="wrap">
            <?php if( have_posts() ): ?>
                <div class="list">
                    <?php while( have_posts() ): the_post(); ?>
                        <div class="item">
                            <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                            <div class="text">
                                <p><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_permalink(); ?></a></p>
                                <?php
                                if( ! empty( $post->post_excerpt ) ) 
                                    echo sprintf( '<p>%s</p>', $post->post_excerpt );
                                else
                                    echo sprintf( '<p>%s</p>', wp_trim_words( get_the_content(), 30, '...' ) );
                                ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php if (function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
            <?php else: ?>
                <h3>No results found.</h3>
            <?php endif; ?>
        </div>
    </section>

<?php get_footer(); ?>