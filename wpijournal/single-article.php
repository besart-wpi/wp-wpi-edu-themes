<?php get_header(); ?>

<?php the_post(); ?>

    <?php
    $department = get_the_terms( $post->ID, 'department_cat' );
    $issue      = get_the_terms( $post->ID, 'issue_cat' );
    $currentIssue = wpi_journal_current_issue();

    if( $issue[0]->term_id == $currentIssue->term_id ){
        $issueTitle = 'Current Issue';
    } else {
        $issueTitle = $issue[0]->name;
    }

    $feature_article = get_field( 'feature_article' );
    if( $feature_article || $department[0]->slug == 'faculty-snapshot' ){
        $special = 1;
    } ?>

    <section class="section hero <?php echo $special ? 'special' : ''; ?>">
        <?php if( $special ){ ?>
            <div class="upper">
                <div class="wrap">
                    <div class="meta">
                        <?php if( $department ){ ?>
                            <a href="<?php echo get_term_link( $department[0]->slug, 'department_cat' ) ?>" title="<?php echo $department[0]->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department[0]->slug ) ?>">
                                <span><?php echo $department[0]->name; ?></span>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-left.svg" alt="left bracket" class="svg"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-right.svg" alt="right bracket" class="svg">
                            </a>
                        <?php } ?>
                        
                        <?php if( $feature_article ){ ?>
                            <span class="bracket">
                                <span>Feature</span>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-left.svg" alt="left bracket" class="svg"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-right.svg" alt="right bracket" class="svg">
                            </span>
                        <?php } ?>

                        <?php if( $issue ){ ?>
                            <a href="<?php echo get_term_link( $issue[0]->term_id, 'issue_cat' ); ?>" title="<?php echo $issueTitle; ?>"><?php echo $issueTitle; ?></a>
                        <?php } ?>
                    </div>
                    <h1><?php the_title(); ?></h1>
                    <?php echo wpautop( get_field('subheading') ); ?>
                </div>
            </div>
        <?php } ?>

        <?php
        $hero_slides = get_field( 'hero_slides' );
        if( $hero_slides ){ ?>
            <div class="images">
                <div class="image-arrows"><div></div></div>
                <div class="image-slider">
                    <?php foreach( $hero_slides as $image ){ ?>
                        <div class="item">
                            <figure class="image">
                                <?php if( $image[ 'youtube_video_id' ] ){ ?>
                                    <a data-fancybox="" href="//www.youtube.com/watch?v=<?php echo $image[ 'youtube_video_id' ]; ?>" class="play-video"><span class="icon"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/play.svg" class="svg" alt="play"></span></a>
                                <?php } ?>
                                <?php echo $image[ 'image' ] ? wp_get_attachment_image( $image[ 'image' ][ 'ID' ], 'thumb-slider' ) : ''; ?>
                            </figure>
                            <?php if( $image[ 'caption' ] ){ ?>
                                <div class="caption">
                                    <p><?php echo $image[ 'caption' ]; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </section>

    <section class="section content">
        <div class="wrap">
            <main>
                <?php if( ! $feature_article && ! in_array( $department[0]->slug, array( 'class-notes', 'faculty-snapshot' ) ) ){ ?>
                    <div class="post-head">
                        <div class="inner">
                            <div class="meta">
                                <?php if( $department ){ ?>
                                    <a href="<?php echo get_term_link( $department[0]->slug, 'department_cat' ) ?>" title="<?php echo $department[0]->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department[0]->slug ) ?>">
                                        <span><?php echo $department[0]->name; ?></span>
                                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-left.svg" alt="left bracket" class="svg"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-right.svg" alt="right bracket" class="svg">
                                    </a>
                                <?php } ?>

                                <?php if( $issue ){ ?>
                                    <a href="<?php echo get_term_link( $issue[0]->term_id, 'issue_cat' ); ?>" title="<?php echo $issueTitle; ?>" class="issue-term"><?php echo $issueTitle; ?></a>
                                <?php } ?>
                            </div>
                            <h1><?php the_title(); ?></h1>
                        </div>
                    </div>
                <?php }elseif( ! $feature_article && $department[0]->slug == 'class-notes' ){ ?>
                    <div class="item-head">
                        <h1><?php the_title(); ?></h1>
                    </div>
                <?php } ?>

                <div class="inline-aside">
                    <div class="metas"></div>
                    <div class="date-share"></div>
                    <div class="ctas"></div>
                </div>
                
                <?php 
                global $page, $numpages;
                if( $page > 1 ) {
                    echo '<div class="post-item standard"><div class="inner">';
                    echo '<p class="page-number">Page ' . $page . ' of ' . $numpages . '</p>';
                    echo '</div></div>';
                } ?>

                <?php the_content(); ?>
                
                <?php if (function_exists('wp_pagenavi')) { wp_pagenavi( array( 'type' => 'multipart' ) ); } ?>

                <?php
                $image_text_boxes = get_field( 'image_text_boxes' );
                if( $image_text_boxes ){ ?>
                    <div class="post-item more-less">
                        <div class="list">
                            <?php
                            $counter = 1;
                            foreach( $image_text_boxes as $item ){ ?>
                                <div class="item">
                                    <?php if( $item[ 'image' ] ){ ?>
                                        <figure>
                                            <a href="#<?php echo sanitize_title( $item[ 'title' ] ) ?>-<?php echo $counter ?>-modal" data-fancybox="faculty-image"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/expand.svg" alt="zoom" class="svg"><span class="sr">Zoom image <?php echo $item[ 'title' ]; ?></span></a>
                                            <?php echo wp_get_attachment_image( $item[ 'image' ][ 'ID' ], 'thumb-faculty' ); ?>
                                        </figure>
                                        <div class="modal-content" id="<?php echo sanitize_title( $item[ 'title' ] ) ?>-<?php echo $counter ?>-modal" style="width: <?php echo $item[ 'image' ][ 'width' ] ?>px;">
                                            <figure><?php echo wp_get_attachment_image( $item[ 'image' ][ 'ID' ], 'image-modal' ); ?></figure>
                                            <figcaption>
                                                <?php echo $item[ 'title' ] ? sprintf( '<h3>%s</h3>', $item[ 'title' ] ) : ''; ?>
                                                <?php echo $item[ 'text' ] ? sprintf( '<p>%s</p>', $item[ 'text' ] ) : ''; ?>
                                            </figcaption>
                                        </div>
                                    <?php } ?>
                                    <div class="text">
                                        <?php echo $item[ 'title' ] ? sprintf( '<h3>%s</h3>', $item[ 'title' ] ) : ''; ?>
                                        <?php
                                        $text = $item[ 'text' ];
                                        $word_count = str_word_count( $text );
                                        if( $word_count > 17 ){
                                            $pieces = explode(" ", $text);
                                            $excerpt = implode(" ", array_splice($pieces, 0, 17)); ?>
                                            <p class="short"><?php echo $excerpt; ?></p>
                                            <p class="long"><?php echo $text; ?></p>
                                            <a href="#" class="more"><span>More</span><span>Less</span></a>
                                        <?php }else{ ?>
                                            <p><?php echo $text; ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php $counter ++;
                            } ?>
                        </div>
                    </div>
                <?php } ?>

                <?php
                $class_notes = get_field( 'class_notes' );
                if( $class_notes ){ ?>
                    <div class="list-class">
                        <?php foreach( $class_notes as $note ){ ?>
                            <div class="item">
                                <?php echo $note[ 'year' ] ? sprintf( '<h2>%s</h2>', $note[ 'year' ] ) : ''; ?>
                                <?php echo $note[ 'content' ]; ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php
                $related_article = get_field( 'related_article' );
                if( $related_article ){ ?>
                    <div class="spacer"></div>
                <?php } ?>
            </main>
            <aside class="left">
                <?php if( $department[0]->slug != 'class-notes' ){ ?>
                    <div class="widget stamp">
                        <div class="item">
                            <h5>Author</h5>
                            <div>
                                <?php
                                $customAuthor = get_field( 'author' );
                                $author_photo = get_field( 'author_photo' );
                                if( $customAuthor ){
                                    echo $author_photo ? sprintf( '<figure>%s</figure>', wp_get_attachment_image( $author_photo['ID'], 'thumbnail' ) ) : '';
                                    echo $customAuthor ? sprintf( '<span>%s</span>', $customAuthor ) : '';
                                }else{
                                    $author = get_userdata( $post->post_author );
                                    $avatar = get_avatar( $author->ID );
                                    $name   = $author->data->display_name ? $author->data->display_name : $author->data->user_nicename;
                                    echo $avatar ? sprintf( '<figure>%s</figure>', $avatar ) : '';
                                    echo $name ? sprintf( '<span>%s</span>', $name ) : '';
                                } ?>
                            </div>
                        </div>

                        <?php
                        $photographer       = get_field( 'photographer' );
                        $photographer_photo = get_field( 'photographer_photo' );
                        if( $photographer ){ ?>
                            <div class="item photographer">
                                <h5>Photographer</h5>
                                <div>
                                    <?php if( $photographer_photo ){ ?>
                                        <figure><?php echo wp_get_attachment_image( $photographer_photo['ID'], 'thumbnail' ); ?></figure>
                                    <?php } ?>
                                    <span><?php echo $photographer; ?></span>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        $illustrator       = get_field( 'illustrator' );
                        $illustrator_photo = get_field( 'illustrator_photo' );
                        if( $illustrator ){ ?>
                            <div class="item illustrator">
                                <h5>Illustrator</h5>
                                <div>
                                    <?php if( $illustrator_photo ){ ?>
                                        <figure><?php echo wp_get_attachment_image( $illustrator_photo['ID'], 'thumbnail' ); ?></figure>
                                    <?php } ?>
                                    <span><?php echo $illustrator; ?></span>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="item posted">
                            <h5>Posted</h5>
                            <div>
                                <span><?php the_date( 'F jS Y' ); ?></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="widget sharing share-btn" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>">
                    <?php 
                    if ( $department && !is_wp_error( $department ) && $department[0]->slug == 'class-notes' ){
                        $sharing_widget_heading = __('Share Notes', 'wpi_journal_theme');
                    } else {
                         $sharing_widget_heading = __('Share Story', 'wpi_journal_theme');
                    }
                    ?>
                    <h5><?php echo esc_html($sharing_widget_heading); ?></h5>
                    <div class="item">
                        <a href="<?php the_permalink(); ?>#Share on Facebook" class="btn-facebook" data-id="fb" title="Share on Facebook"><span class="sr">Facebook</span></a>
                        <figure><svg width="10px" height="18px" viewBox="0 0 10 18" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M2.70702 18L2.70702 9.94919L0 9.94919L0 6.74998L2.70702 6.74998L2.70702 4.21874C2.70702 2.8828 3.08202 1.8457 3.83202 1.10742C4.58202 0.36914 5.57811 0 6.82029 0C7.8281 0 8.64841 0.0468747 9.28122 0.140625L9.28122 2.98827L7.59373 2.98827C6.96092 2.98827 6.52733 3.1289 6.29295 3.41015C6.10545 3.64452 6.0117 4.01952 6.0117 4.53514L6.0117 6.74998L8.99997 6.74998L8.5781 9.94919L6.0117 9.94919L6.0117 18L2.70702 18Z" id="fill" fill="#1D1D21" stroke="none" /></svg></figure>
                        <span>Facebook</span>
                    </div>
                    <div class="item">
                        <a href="<?php the_permalink(); ?>#Share on Twitter" class="btn-twitter" data-id="tw" title="Share on Twitter"><span class="sr">Twitter</span></a>
                        <figure><svg width="18px" height="15px" viewBox="0 0 18 15" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M16.1367 3.65624C16.1601 3.74999 16.1718 3.90233 16.1718 4.11327C16.1718 5.80076 15.7617 7.42967 14.9414 8.99997C14.0976 10.664 12.914 11.9882 11.3906 12.9726C9.72654 14.0742 7.81638 14.625 5.66014 14.625C3.59765 14.625 1.71093 14.0742 0 12.9726C0.257812 12.9961 0.55078 13.0078 0.878904 13.0078C2.58984 13.0078 4.12499 12.4804 5.48436 11.4257C4.66405 11.4257 3.94335 11.1855 3.32226 10.705C2.70116 10.2246 2.27343 9.62107 2.03906 8.89451C2.27343 8.91794 2.49609 8.92966 2.70702 8.92966C3.03515 8.92966 3.36327 8.89451 3.6914 8.82419C2.84765 8.63669 2.14453 8.2031 1.58203 7.52342C1.01953 6.84373 0.738279 6.0703 0.738279 5.20311L0.738279 5.1328C1.2539 5.43748 1.80468 5.60155 2.39062 5.62498C1.89843 5.27342 1.5 4.82811 1.19531 4.28905C0.890622 3.74999 0.738279 3.15819 0.738279 2.51366C0.738279 1.86914 0.91406 1.2539 1.26562 0.667967C2.17968 1.8164 3.29882 2.73046 4.62303 3.41015C5.94725 4.08983 7.35935 4.46483 8.85935 4.53514C8.81248 4.25389 8.78904 3.97264 8.78904 3.6914C8.78904 3.03515 8.9531 2.41991 9.28122 1.8457C9.60935 1.27148 10.0547 0.82031 10.6172 0.492186C11.1797 0.164062 11.789 0 12.4453 0C12.9843 0 13.4824 0.105468 13.9394 0.316405C14.3964 0.527342 14.8007 0.808591 15.1523 1.16015C15.996 0.996091 16.7812 0.703123 17.5078 0.281249C17.2265 1.14843 16.6875 1.82812 15.8906 2.32031C16.5937 2.22656 17.2968 2.02734 18 1.72265C17.4843 2.47265 16.8632 3.11718 16.1367 3.65624Z" id="fill" fill="#1D1D21" stroke="none" /></svg></figure>
                        <span>Twitter</span>
                    </div>
                    <div class="item">
                        <a href="<?php the_permalink(); ?>#Copy page URL" class="btn-copy-url" data-clipboard-text="<?php the_permalink(); ?>" title="Copy page URL"><span class="notif">Copied!</span><span class="sr">Copy Link</span></a>
                        <figure><svg width="16px" height="18px" viewBox="0 0 16 18" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M12.375 11.25C13.3125 11.25 14.1094 11.5781 14.7656 12.2344C15.4219 12.8906 15.75 13.6875 15.75 14.625C15.75 15.5625 15.4219 16.3594 14.7656 17.0156C14.1094 17.6719 13.3125 18 12.375 18C11.4375 18 10.6406 17.6719 9.98438 17.0156C9.32812 16.3594 9 15.5625 9 14.625C9 14.3672 9.02344 14.1211 9.07031 13.8867L5.48438 11.6367C4.875 12.1289 4.17188 12.375 3.375 12.375C2.4375 12.375 1.64063 12.0469 0.984375 11.3906C0.328122 10.7344 0 9.9375 0 9C0 8.0625 0.328122 7.26563 0.984375 6.60938C1.64063 5.95312 2.4375 5.625 3.375 5.625C4.17188 5.625 4.875 5.87109 5.48438 6.36328L9.07031 4.11328C9.02344 3.87891 9 3.63281 9 3.375C9 2.4375 9.32812 1.64063 9.98438 0.984375C10.6406 0.328122 11.4375 0 12.375 0C13.3125 0 14.1094 0.328122 14.7656 0.984375C15.4219 1.64063 15.75 2.4375 15.75 3.375C15.75 4.3125 15.4219 5.10937 14.7656 5.76563C14.1094 6.42188 13.3125 6.75 12.375 6.75C11.5781 6.75 10.875 6.50391 10.2656 6.01172L6.67969 8.26172C6.77344 8.75391 6.77344 9.24609 6.67969 9.73828L10.2656 11.9883C10.875 11.4961 11.5781 11.25 12.375 11.25Z" id="fill" fill="#1D1D21" stroke="none" /></svg></figure>
                        <span>Copy Link</span>
                    </div>
                </div>
            </aside>
            <aside class="right">
                <?php if( $department[0]->slug != 'class-notes' ){ ?>
                    <div class="widget back-issue">
                        <a href="<?php echo get_term_link( $issue[0]->term_id, 'issue_cat' ); ?>" title="Back to issue"><span class="sr">Back to issue</span></a>
                        <?php
                        $issue_cover_image = get_field( 'issue_cover_image', 'issue_cat_' . $issue[0]->term_id );
                        if( $issue_cover_image ){ ?>
                            <figure>
                                <?php  echo wp_get_attachment_image( $issue_cover_image['ID'], 'thumb-archive' ); ?>
                                <svg width="8px" height="16px" viewBox="0 0 8 16" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M6.96725 0.963252L0.891669 0.00706891C0.878889 0.00474526 0.866496 0.00668164 0.853716 0.00513253C0.815763 0.00125978 0.778584 -0.00106388 0.740631 0.000485227C0.718944 0.00125978 0.698418 0.00397071 0.677118 0.00668164C0.6442 0.0109417 0.611668 0.0167508 0.579525 0.0248836C0.558224 0.0303054 0.537699 0.0361146 0.517173 0.0434728C0.485029 0.0547038 0.454435 0.0686457 0.42384 0.0841367C0.405251 0.0934314 0.387049 0.101564 0.369234 0.112408C0.337477 0.132159 0.308044 0.155008 0.278612 0.179407C0.267381 0.188701 0.253826 0.195672 0.242982 0.205741C0.240659 0.208065 0.239109 0.211163 0.236398 0.2131C0.20348 0.245243 0.17366 0.28126 0.145776 0.319988C0.132609 0.338577 0.12099 0.357941 0.109372 0.377692C0.088072 0.412934 0.0679336 0.448563 0.0528299 0.485741C0.0458589 0.503556 0.0423734 0.522533 0.0365643 0.541122C0.0257206 0.575202 0.0156514 0.609282 0.00945503 0.644524C0.00596955 0.66505 0.005195 0.68635 0.00364589 0.70765C0.000934966 0.741343 -0.000614136 0.775036 0.000934966 0.808729C0.00170952 0.819186 -0.000614136 0.829255 0.000160415 0.839711L0.524919 6.82583C0.560161 7.22898 0.898252 7.53261 1.2956 7.53261C1.31845 7.53261 1.3413 7.53183 1.36414 7.5299C1.79054 7.49233 2.10539 7.11667 2.06821 6.69067L1.70185 2.5139C3.13245 3.80082 5.22838 6.11286 5.6327 8.89543C5.92276 10.8914 5.28725 12.8557 3.74434 14.7336C3.47286 15.064 3.52049 15.5519 3.85084 15.8238C3.99491 15.9423 4.16879 16 4.34229 16C4.56575 16 4.78766 15.9036 4.94102 15.7173C6.77477 13.4858 7.52299 11.113 7.16437 8.665C6.74766 5.81698 4.91662 3.47551 3.39153 1.96862L6.72675 2.49338C7.14656 2.5596 7.54545 2.27147 7.61206 1.84895C7.67867 1.42605 7.39015 1.02986 6.96725 0.963252" id="fill" fill="#1D1D21" stroke="none" /></svg>
                            </figure>
                        <?php } ?>
                        <span>Back to issue</span>
                    </div>
                <?php } ?>

                <div class="widget submit-notes">
                    <a href="<?php echo home_url( '/contact-us/' ); ?>" title="Submit Class Notes"><span class="sr">Submit Class Notes</span></a>
                    <figure><svg width="21px" height="24px" viewBox="0 0 21 24" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M1.71696 2.51836C1.9945 2.51836 2.21976 2.29344 2.21976 2.01556C2.21976 1.45879 2.64479 1.0056 3.1677 1.0056L17.2381 1.0056C17.7603 1.0056 18.1857 1.45879 18.1857 2.01556L18.1857 2.65277C18.1857 2.93066 18.4109 3.15557 18.6885 3.15557C18.966 3.15557 19.1913 2.93066 19.1913 2.65277L19.1913 2.01556C19.1913 0.90437 18.3151 0 17.2381 0L3.1677 0C2.09037 0 1.21416 0.90437 1.21416 2.01556C1.21416 2.29344 1.43941 2.51836 1.71696 2.51836ZM1.08175 5.26498C0.922868 5.26498 0.765995 5.18956 0.668451 5.04877L0.0895608 4.21479C-0.0022841 4.08206 -0.0250777 3.91345 0.0285543 3.76127C0.0821864 3.60875 0.20554 3.49177 0.360067 3.44584L2.47149 2.82137C2.73764 2.7436 3.01753 2.89444 3.09631 3.16126C3.17541 3.42741 3.0229 3.70697 2.75675 3.78574L1.31237 4.21312L1.49472 4.47558C1.65293 4.70385 1.59662 5.01726 1.36835 5.17548C1.28086 5.23581 1.18064 5.26498 1.08175 5.26498ZM20.945 7.40926C20.945 7.54267 20.8921 7.67071 20.7979 7.7649L11.9258 16.6367C11.8276 16.7349 11.6989 16.7838 11.5702 16.7838C11.4418 16.7838 11.3131 16.7349 11.2148 16.6367L8.27548 13.6973C8.18129 13.6031 8.12833 13.4754 8.12833 13.342C8.12833 13.2086 8.18129 13.0805 8.27548 12.9863L17.1476 4.11457C17.344 3.91815 17.6621 3.91815 17.8585 4.11457L20.7979 7.05395C20.8921 7.14814 20.945 7.27585 20.945 7.40926ZM17.5032 5.18118L19.7313 7.40926L11.5702 15.57L9.34209 13.342L17.5032 5.18118ZM0.668451 7.67173C0.765995 7.81251 0.922868 7.88793 1.08175 7.88793C1.18064 7.88793 1.28086 7.85877 1.36835 7.79843C1.59662 7.63988 1.65293 7.32681 1.49472 7.09854L1.31237 6.83574L2.75675 6.40869C3.0229 6.32992 3.17541 6.05036 3.09631 5.78388C3.01753 5.5174 2.73764 5.36589 2.47149 5.44432L0.360067 6.0688C0.20554 6.11472 0.0821864 6.23171 0.0285543 6.38389C-0.0250777 6.53607 -0.0022841 6.70501 0.0895608 6.83741L0.668451 7.67173ZM1.08175 10.5109C0.922868 10.5109 0.765995 10.4354 0.668451 10.2947L0.0895608 9.46034C-0.0022841 9.32794 -0.0250777 9.15933 0.0285543 9.00682C0.0821864 8.85464 0.20554 8.73765 0.360067 8.69173L2.47149 8.06725C2.73764 7.98881 3.01753 8.14066 3.09631 8.40681C3.17541 8.67329 3.0229 8.95285 2.75675 9.03162L1.31237 9.459L1.49472 9.72146C1.65293 9.94974 1.59662 10.2631 1.36835 10.4214C1.28086 10.4817 1.18064 10.5109 1.08175 10.5109ZM0.668451 12.9176C0.765995 13.0584 0.922868 13.1338 1.08175 13.1338C1.18064 13.1338 1.28086 13.1046 1.36835 13.044C1.59662 12.8858 1.65293 12.5727 1.49472 12.3444L1.31237 12.0816L2.75675 11.6546C3.0229 11.5758 3.17541 11.2962 3.09631 11.0298C3.01753 10.7633 2.73764 10.6118 2.47149 10.6902L0.360067 11.3147C0.20554 11.3603 0.0821864 11.4776 0.0285543 11.6298C-0.0250777 11.7819 -0.0022841 11.9509 0.0895608 12.0833L0.668451 12.9176ZM19.1913 21.9848L19.1913 11.9016C19.1913 11.6241 18.966 11.3988 18.6885 11.3988C18.4109 11.3988 18.1857 11.6241 18.1857 11.9016L18.1857 21.9848C18.1857 22.5415 17.7603 22.9944 17.2381 22.9944L3.1677 22.9944C2.64479 22.9944 2.21976 22.5415 2.21976 21.9848C2.21976 21.7069 1.9945 21.482 1.71696 21.482C1.43941 21.482 1.21416 21.7069 1.21416 21.9848C1.21416 23.096 2.09037 24 3.1677 24L17.2381 24C18.3151 24 19.1913 23.096 19.1913 21.9848ZM1.08175 15.7568C0.922868 15.7568 0.765995 15.6813 0.668451 15.5406L0.0895608 14.7062C-0.0022841 14.5738 -0.0250777 14.4049 0.0285543 14.2527C0.0821864 14.1005 0.20554 13.9836 0.360067 13.9376L2.47149 13.3132C2.73764 13.2347 3.01753 13.3862 3.09631 13.6527C3.17541 13.9192 3.0229 14.1988 2.75675 14.2775L1.31237 14.7049L1.49472 14.9674C1.65293 15.1956 1.59662 15.5087 1.36835 15.6673C1.28086 15.7276 1.18064 15.7568 1.08175 15.7568ZM7.1653 14.1887C7.34028 14.1398 7.52732 14.1897 7.6557 14.3177L10.5947 17.2568C10.7231 17.3852 10.7724 17.5725 10.7238 17.7472C10.6752 17.9221 10.5357 18.0569 10.3601 18.1005L6.45602 19.0652C6.41579 19.0752 6.37524 19.0799 6.33501 19.0799C6.20361 19.0799 6.0749 19.028 5.9797 18.9328C5.855 18.8081 5.80472 18.6277 5.84696 18.4565L6.812 14.5524C6.85558 14.3764 6.99033 14.2373 7.1653 14.1887ZM9.25394 17.3379L7.02318 17.8893L7.57458 15.6585L9.25394 17.3379ZM0.668451 18.1635C0.765995 18.3043 0.922868 18.3797 1.08175 18.3797C1.18064 18.3797 1.28086 18.3505 1.36835 18.2899C1.59662 18.1316 1.65293 17.8186 1.49472 17.5903L1.31237 17.3275L2.75675 16.9005C3.0229 16.8217 3.17541 16.5418 3.09631 16.2756C3.01753 16.0092 2.73764 15.858 2.47149 15.9361L0.360067 16.5606C0.20554 16.6061 0.0821864 16.7235 0.0285543 16.8756C-0.0250777 17.0278 -0.0022841 17.1968 0.0895608 17.3292L0.668451 18.1635ZM1.31237 19.9505L1.49472 20.2133C1.65293 20.4415 1.59662 20.7546 1.36835 20.9131C1.28086 20.9735 1.18064 21.0026 1.08175 21.0026C0.922868 21.0026 0.765995 20.9272 0.668451 20.7864L0.0895608 19.9521C-0.0022841 19.8197 -0.0250777 19.6508 0.0285543 19.4986C0.0821864 19.3464 0.20554 19.2294 0.360067 19.1835L2.47149 18.559C2.73764 18.4809 3.01753 18.6321 3.09631 18.8986C3.17541 19.1651 3.0229 19.4446 2.75675 19.5234L1.31237 19.9505Z" id="fill" fill="#1D1D21" fill-rule="evenodd" stroke="none" /></svg></figure>
                    <span>Submit Class Notes</span>
                </div>

                <div class="widget widget-toggler">
                    <div class="theme-toggler">
                    <?php get_template_part( 'parts/theme-toggler' ); ?>
                    </div>
                </div>
            </aside>
        </div>
    </section>

    <?php get_template_part( 'parts/related-article' ); ?>

    <?php get_template_part( 'parts/photos' ); ?>

    <?php get_template_part( 'parts/other-stories' ); ?>

<?php get_footer(); ?>