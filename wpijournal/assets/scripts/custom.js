
(function($) {
	
		$('nav.primary ul.sf-menu').superfish({
			cssArrows: false,
		});
	
    var menuInit = 0;
    function menu(){
        var $primary = $('nav.primary'),
            leftPosition = 0,
            currentItem = [],
            isClicked = 0;

        $primary.find('a').on('click',function(){
            isClicked = 1;
        });

        $primary.find('> ul > li').each(function(){
            var $this = $(this);
            // console.log( 'marginLeft ' + parseInt( $this.css('margin-right') ) );
            $this.attr('data-width', $this.outerWidth());

            $this.attr('data-left', leftPosition);

            if( $this.hasClass('current-menu-item') ){
                currentItem.push( $this.outerWidth() );
                currentItem.push( leftPosition );
            }

            if( $(window).width() < 960 && $this.is(':nth-last-child(2)') ){
                leftPosition = $this.parent('ul').outerWidth() - $this.outerWidth();
            }else{
                leftPosition += $this.outerWidth() + parseInt( $this.css('margin-right') );
            }

        }).on('mouseenter',function(){
            var $this = $(this);
            // console.log( 'left ' + $this.data('left') + ' / width ' + $this.data('width') );
            $primary.find('.line').css({ 'left': $this.data('left') + 'px', 'width': $this.data('width') + 'px' });
        }).on('focusin',function(){
            var $this = $(this);
            // console.log( 'left ' + $this.data('left') + ' / width ' + $this.data('width') );
            $primary.find('.line').css({ 'left': $this.data('left') + 'px', 'width': $this.data('width') + 'px' });
        });

        $primary.find('.line').remove();
        $primary.append('<span class="line" style="width:'+ currentItem[0] +'px;left:'+ currentItem[1] +'px;"></span>');
        // console.log( 'adding line' );

        $primary.on('mouseleave',function(){
            var $current = $primary.find('.current-menu-item');
            if( isClicked == 0 ){
                $primary.find('.line').css({ 'left': $current.data('left') + 'px', 'width': $current.data('width') + 'px' });
            }
        });

        if( menuInit == 0 ){
            $('.mobile-menu-toggle').on('click',function(){
                $('body').toggleClass('mob-nav-on');
                $(this).toggleClass('active');
                return false;
            });
            menuInit = 1;
        }

        if( $(window).width() > 719 ){
            $('body').removeClass('mob-nav-on');
            $('.mobile-menu-toggle').removeClass('active');
        }

        $('.primary .pop-search').focusout(function(){
            var $current = $primary.find('.current-menu-item');
            $primary.find('.line').css({ 'left': $current.data('left') + 'px', 'width': $current.data('width') + 'px' });
        });
			
				$('#mobile-menu ul.menu > li.menu-item-has-children > a').on('click', function(event){
					if( !$(this).closest('li').hasClass('subnav-open') ){
						event.preventDefault();
						$(this).closest('li').addClass('subnav-open').find('> .sub-menu').slideDown();
					}
				});
    }

    function sliders(){
        if( $('.hero-slider').length ){
            var heroSlider = $('.hero-slider');

            heroSlider.clone().removeClass('hero-slider').addClass('mobile-hero-slider').appendTo( $('.mobile-slider') );
            $('.mobile-slider').find('.btn').removeClass('light');
            var mobileHeroSlider = $('.mobile-hero-slider');

            heroSlider.slick({
                arrows: true,
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                appendArrows: $('.hero-arrows > div'),
                prevArrow: '<div class="prev"><span class="arrow left"></span></div>',
                nextArrow: '<div class="next"><span class="arrow right"></span></div>',
                asNavFor: '.mobile-hero-slider',
                adaptiveHeight: true,
                responsive: [
                    {
                        breakpoint: 720,
                        settings: {
                            arrows: false,
                            dots: true,
                        }
                    }
                ]
            });

            mobileHeroSlider.slick({
                arrows: false,
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                adaptiveHeight: true,
                asNavFor: '.hero-slider',
            });
        }

        if( $('.gal-slider').length ){
            var galSlider   = $('.gal-slider'),
                autoPlay    = galSlider.data( 'autoplay' ) ? 'true' : 'false',
                speed       = galSlider.data( 'speed' ) ? ( galSlider.data( 'speed' ) * 1000 ) : 0;

            // console.log( 'autoPlay ' + autoPlay );

            galSlider.clone().removeClass('gal-slider').addClass('mobile-gal-slider').appendTo( $('.mobile-gallery') );
            $('.mobile-gallery').find('figure').remove();
            var mobileGalSlider = $('.mobile-gal-slider');

            galSlider.slick({
                autoplay: autoPlay,
                autoplaySpeed: speed,
                arrows: true,
                dots: false,
                infinite: true,
                speed: 200,
                slidesToShow: 1,
                // adaptiveHeight: true,
                appendArrows: $('.gal-arrows'),
                prevArrow: '<div class="prev"><span class="arrow left"></span></div>',
                nextArrow: '<div class="next"><span class="arrow right"></span></div>',
                asNavFor: '.mobile-gal-slider',
                responsive: [
                    {
                        breakpoint: 720,
                        settings: {
                            arrows: false,
                            dots: true
                        }
                    }
                ]
            });

            mobileGalSlider.slick({
                arrows: false,
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                adaptiveHeight: true,
                asNavFor: '.gal-slider',
            });

            if( autoPlay == 'false' || autoPlay == '' ) // force stop on init
                galSlider.slick('slickPause');
        }

        if( $('.story-slider').length ){
            var storySlider = $('.story-slider');

            storySlider.clone().removeClass('story-slider').addClass('mobile-story-slider').appendTo( $('.slider-wrap') );
            $('.mobile-story-slider figure').remove();

            storySlider.slick({
                arrows: true,
                dots: false,
                infinite: true,
                speed: 200,
                slidesToShow: 3,
                appendArrows: $('.story-arrows'),
                prevArrow: '<div class="prev"><span class="arrow left"></span></div>',
                nextArrow: '<div class="next"><span class="arrow right"></span></div>',
                asNavFor: '.mobile-story-slider',
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            arrows: true,
                            dots: false,
                            slidesToShow: 1,
                        }
                    },
                    {
                        breakpoint: 720,
                        settings: {
                            arrows: false,
                            dots: true,
                            slidesToShow: 1,
                        }
                    }
                ]
            });

            var mobileStorySlider = $('.mobile-story-slider');
            mobileStorySlider.slick({
                arrows: false,
                dots: false,
                infinite: true,
                speed: 200,
                slidesToShow: 1,
                adaptiveHeight: true,
                asNavFor: '.story-slider',
            });
        }

        if( $('.image-slider').length ){
            var imageSlider = $('.image-slider');

            imageSlider.on('beforeChange setPosition', function(slick, currentSlide){
                var imageHeight = imageSlider.find('[data-slick-index="'+ currentSlide.currentSlide +'"]').find('figure').outerHeight()  - 115;
                // console.log( 'imageHeight 1 ' + imageHeight );
                $('.image-arrows > div').css('top', imageHeight + 'px');
            });

            imageSlider.on('init', function(slick){
                var imageHeight = imageSlider.find('.item').find('figure').outerHeight()  - 115;
                $('.image-arrows > div').css('top', imageHeight + 'px');
                // $('.image-arrows').css('minHeight', imageHeight + 'px');
            });

            imageSlider.slick({
                arrows: true,
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                adaptiveHeight: true,
                appendArrows: $('.image-arrows > div'),
                prevArrow: '<div class="prev"><span class="arrow left"></span></div>',
                nextArrow: '<div class="next"><span class="arrow right"></span></div>',
            });
        }
    }

    function dropCap() {
        if( $('.drop-caps').length ){

            $('.drop-caps').each(function() {
                var $p = $(this).find('p:first');
                var text = $.trim($p.text());
                var firstLetter = text[0];
                var marginalDropCap = '<span class="dropcap">' + firstLetter + '</span>';

                $p.html(text.replace(firstLetter, marginalDropCap));
            });
        }
    }

    function moreLess(){
        if( $('.more-less').length ){
            $('.more-less .more').on('click',function(){
                $(this).toggleClass('less');
                // $(this).prev('p').find('span').toggle();
                $(this).prevAll('p').toggle();
                return false;
            });
        }
    }

    function form(){
        if( $('select').length ){
            $('select').on('selectric-init selectric-change', function(event, element, selectric) {
                var selectedIndex = $('option:selected', element).index();
                if( selectedIndex == 0 ){
                    $(element).closest('.selectric-wrapper').addClass('first-option');
                } else {
                    $(element).closest('.selectric-wrapper').removeClass('first-option');
                }
             });
            $('select').selectric({ arrowButtonMarkup: '<span class="sel-arrow"></span>' }).trigger('change');
            $(".selectric-input").each(function(){
                var label = $(this).siblings('.selectric').children('.label').text();
                // console.log( 'label ' + label );
                $(this).wrap("<label class='sr' aria-hidden='true'>" + label + "</label>");
            });
        }
    }

    var infoIsActive = 0;

    function themeToggle(){
        var theme = Cookies.get('theme');
        if( theme ){
            // console.log( 'theme ' + theme );
            if( theme == 'night' ){
                $('body').addClass('theme-night');
                $('.toggler-slider').each(function(){
                    $(this).find('input').prop("checked", true);
                });
            }
        }

        if( $('.theme-toggler').length ){
            $('.toggler-slider').each(function(count){
                $(this).find('input').attr('id','toggler-slider-' + count).attr('name','toggler-slider-' + count);
                $(this).find('label').attr('for','toggler-slider-' + count);
            });

            $('input[name^="toggler-slider"]').click(function(){
                if( $(this).is(':checked') ){
                    // alert("Checkbox is checked.");
                    $('body').removeClass('theme-morning').addClass('theme-night');
                    Cookies.set('theme', 'night', { expires: 365 });
                    $('input[name^="toggler-slider"]').prop("checked", true);
                }else if( $(this).is(":not(:checked)") ){
                    // alert("Checkbox is unchecked.");
                    $('body').removeClass('theme-night').addClass('theme-morning');
                    Cookies.set('theme', 'morning', { expires: 365 });
                    $('input[name^="toggler-slider"]').prop("checked", false);
                }
            });

            $('.btn-info').on('click',function(){
                if( $(this).hasClass('active') ){
                    $(this).removeClass('active');
                    infoIsActive = 0;
                }else{
                    $(this).addClass('active');
                    infoIsActive = 1;
                }
            });
        }
    }

    function modal(){
        $( '[data-fancybox="faculty-image"]' ).fancybox({
            infobar: false,
            smallBtn: false,
            arrows: false,
            gutter: 0,
            baseClass: "fancy-faculty",
            buttons: [ "close" ]
        });

        $( '[data-fancybox="issue-image"]' ).fancybox({
            infobar: false,
            smallBtn: false,
            arrows: false,
            gutter: 0,
            baseClass: "fancy-issue",
            buttons: [ "close" ]
        });

        $(document).on('click', '.fancy-issue .modal-content', function(){
            $.fancybox.close();
        });
    }

    function viewAs(){
        if( $('.view-as').length ){
            $('.view-as a').on('click',function(){
                var link   = $(this),
                    list    = $('#archive-list'),
                    view    = link.data('view');

                link.siblings('a').removeClass('current');
                link.addClass('current');

                if( view == 'list' ){
                    list.removeClass( 'view-grid' ).addClass( 'view-list' );
                    Cookies.set('archive-view', 'list', { expires: 365 });
                }else{
                    list.removeClass( 'view-list' ).addClass( 'view-grid' );
                    Cookies.set('archive-view', 'grid', { expires: 365 });
                }
                return false;
            });

            var archiveView = Cookies.get('archive-view');
            if( archiveView ){
                // console.log( 'archiveView ' + archiveView );
                if( archiveView == 'list' ){
                    $('.view-as a[data-view="list"]').trigger('click');
                }
            }
        }
    }

    function search(){
        $('.search-bar').before( '<div class="search-screen"><div></div></div>' );
        var searchScreen = $('.search-screen');
        $('.pop-search').on('click',function(){
            searchScreen.addClass('active');
            $('body').addClass('search-on');
            $('.logo a').attr('tabindex','8000');
            $('.search-bar input').attr('tabindex','8001');
            $('.search-bar .go-search').attr('tabindex','8002');
            $('.search-bar .close-search').attr('tabindex','8003');
            setTimeout(function(){
                $('.search-bar input[type=search]').focus();
            },1000);
            return false;
        });

        $('.close-search').on('click',function(){
            if( searchScreen.hasClass( 'active' ) ){
                searchScreen.addClass('closing-active');
            }else if( searchScreen.hasClass( 'showing' ) ){
                searchScreen.addClass('closing-show');
            }
            setTimeout(function(){
                searchScreen.removeClass('active closing-active results showing closing-show');
            },800);
            $('#body').show();
            $('#body-search').empty();
            $('body').removeClass('search-on');
            $('.search-bar input[type=search]').val('');
            $('.logo a').attr('tabindex','0');
            $('.search-bar input').attr('tabindex','-1');
            $('.search-bar .go-search').attr('tabindex','-1');
            $('.search-bar .close-search').attr('tabindex','-1');
            return false;
        });

        $('#site-search').on('submit',function(){
            var $this   = $(this),
                string  = $this.find('input').val();
                bodySearch = $('#body-search');
            // console.log( 'string ' + string );

            $this.find('input').attr('disabled','disabled');
            bodySearch.addClass('loading');

            if( string.length ){
                var baseURL     = window.location.origin,
                    searchURL   = baseURL + '/journal?s=' + string + ' #body';

                $('#body').hide();

                bodySearch.load( searchURL, function(){
                    $(this).children(':first').children(':first').unwrap();
                    searchScreen.removeClass('active').addClass('results');
                    setTimeout(function(){
                        searchScreen.removeClass('results').addClass('showing');
                        bodySearch.removeClass('loading');
                    },600);
                });
            }else{
                alert( 'Please enter keyword!' );
            }

            $this.find('input').removeAttr('disabled');
            return false;
        });

        $(document).on( 'click', '.search-results .wp-pagenavi a', function(){
            var $this       = $(this),
                searchURL   = $this.attr('href') + ' #body';

            // console.log( 'href ' + searchURL );

            $('#body-search').load( searchURL, function(){
                $(this).children(':first').children(':first').unwrap();
            });

            $('html, body').animate({
                scrollTop: 0
            },600);

            return false;
        });
    }

    function sharer(){
        if( $('.btn-copy-url').length ){
            var copyURL = new ClipboardJS('.btn-copy-url');
            copyURL.on('success', function(e) {
                $( e.trigger ).children('span').fadeIn().delay(1000).fadeOut('fast');
                e.clearSelection();
            });
        }
    }

    function aside(){
        if( $('aside.left').length ){
            $('aside.left .stamp .item:not(.posted)').clone().appendTo( $('.inline-aside .metas') );
            $('aside.left .stamp .posted').clone().appendTo( $('.inline-aside .date-share') );
            $('aside.left .sharing').clone().appendTo( $('.inline-aside .date-share') );
            $('aside.right .widget').clone().appendTo( $('.inline-aside .ctas') );
        }
    }

    $(function(){
        menu();
        aside();
        search();
        sliders();
        dropCap();
        moreLess();
        form();
        modal();
        themeToggle();
        viewAs();
        sharer();
    });

    $(document).on('mouseup',function(e){
        if( infoIsActive == 1 && !$('.tip').is(e.target) && $('.tip').has(e.target).length === 0 ){
            $('.tip').prev().removeClass('active');
            infoIsActive = 0;
        }
    });

    $(window).on('resize',function(){
        menu();
        $(window).trigger('scroll');
    });

    $(window).on('scroll',function(){
        var viewportTop     = $(window).scrollTop(),
            body            = $('body'),
            togglerTop      = $('aside .widget-toggler').length ? ( $('aside .widget-toggler').offset().top - ( $(window).height() / 2 ) ) : 0,
            footerTop       = $('.site-footer').offset().top,
            viewportBottom  = viewportTop + $(window).height();
            togglerBottom   = ( togglerTop - $(window).height() ) + 70,
            footerBottom    = footerTop - $(window).height();

        if( viewportTop > 50 ){
            body.addClass('scrolled');
        }else{
            body.removeClass('scrolled');
        }

        // console.log( 'viewportTop ' + viewportTop );
        // console.log( 'togglerTop ' + togglerTop );

        if( viewportTop > togglerTop && $('aside .widget-toggler').length ){
            body.addClass('toggler-stick');
        }else{
            body.removeClass('toggler-stick');
        }

        // console.log( 'top absolute ' + ( footerTop - 70 ) );
        // console.log( 'footerTop ' + footerBottom );

        if( viewportTop > footerBottom ){
            $('body').removeClass('toggler-stick').addClass('toggler-attached');
            $('.theme-toggler:not(.mobile)').css('top', ( footerTop - 50 ) + 'px');
            if( $('#wpadminbar').length ){
                $('.theme-toggler:not(.mobile)').css('top', ( footerTop - 80 ) + 'px');
            }
        }else{
            $('body').removeClass('toggler-attached');
            $('.theme-toggler:not(.mobile)').removeAttr('style');

        }
    });

    $(window).on('load', function (){
        // console.log( 'load' );
        menu();
    });

    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    function updateProgress(num1, num2){
        var percent = Math.ceil( num1 / num2 * 100 ) + '%';
        document.getElementById('progress').style.width = percent;
    }

    window.addEventListener('scroll', function(){
        var top = window.scrollY;
        var height = document.body.getBoundingClientRect().height - window.innerHeight;
        updateProgress(top, height);
    });

    document.addEventListener('keyup', function (e) {
        var ele = document.activeElement;
        // console.log( ele.tabIndex );
        // console.log( ele.getAttribute('href') );
        // console.log( ele.getAttribute('class') );
        // console.log( ele.getAttribute('title') );
        // console.log( 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' );

    }, false);

})(jQuery);