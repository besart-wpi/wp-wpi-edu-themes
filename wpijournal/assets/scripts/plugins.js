/*!
 * clipboard.js v2.0.4
 * https://zenorocha.github.io/clipboard.js
 * 
 * Licensed MIT © Zeno Rocha
 */
!function(t,e){"object"==typeof exports&&"object"==typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.ClipboardJS=e():t.ClipboardJS=e()}(this,function(){return function(n){var o={};function r(t){if(o[t])return o[t].exports;var e=o[t]={i:t,l:!1,exports:{}};return n[t].call(e.exports,e,e.exports,r),e.l=!0,e.exports}return r.m=n,r.c=o,r.d=function(t,e,n){r.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:n})},r.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},r.t=function(e,t){if(1&t&&(e=r(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var o in e)r.d(n,o,function(t){return e[t]}.bind(null,o));return n},r.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return r.d(e,"a",e),e},r.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},r.p="",r(r.s=0)}([function(t,e,n){"use strict";var r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},i=function(){function o(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}return function(t,e,n){return e&&o(t.prototype,e),n&&o(t,n),t}}(),a=o(n(1)),c=o(n(3)),u=o(n(4));function o(t){return t&&t.__esModule?t:{default:t}}var l=function(t){function o(t,e){!function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,o);var n=function(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}(this,(o.__proto__||Object.getPrototypeOf(o)).call(this));return n.resolveOptions(e),n.listenClick(t),n}return function(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}}),e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}(o,c.default),i(o,[{key:"resolveOptions",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:{};this.action="function"==typeof t.action?t.action:this.defaultAction,this.target="function"==typeof t.target?t.target:this.defaultTarget,this.text="function"==typeof t.text?t.text:this.defaultText,this.container="object"===r(t.container)?t.container:document.body}},{key:"listenClick",value:function(t){var e=this;this.listener=(0,u.default)(t,"click",function(t){return e.onClick(t)})}},{key:"onClick",value:function(t){var e=t.delegateTarget||t.currentTarget;this.clipboardAction&&(this.clipboardAction=null),this.clipboardAction=new a.default({action:this.action(e),target:this.target(e),text:this.text(e),container:this.container,trigger:e,emitter:this})}},{key:"defaultAction",value:function(t){return s("action",t)}},{key:"defaultTarget",value:function(t){var e=s("target",t);if(e)return document.querySelector(e)}},{key:"defaultText",value:function(t){return s("text",t)}},{key:"destroy",value:function(){this.listener.destroy(),this.clipboardAction&&(this.clipboardAction.destroy(),this.clipboardAction=null)}}],[{key:"isSupported",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:["copy","cut"],e="string"==typeof t?[t]:t,n=!!document.queryCommandSupported;return e.forEach(function(t){n=n&&!!document.queryCommandSupported(t)}),n}}]),o}();function s(t,e){var n="data-clipboard-"+t;if(e.hasAttribute(n))return e.getAttribute(n)}t.exports=l},function(t,e,n){"use strict";var o,r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},i=function(){function o(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}return function(t,e,n){return e&&o(t.prototype,e),n&&o(t,n),t}}(),a=n(2),c=(o=a)&&o.__esModule?o:{default:o};var u=function(){function e(t){!function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,e),this.resolveOptions(t),this.initSelection()}return i(e,[{key:"resolveOptions",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:{};this.action=t.action,this.container=t.container,this.emitter=t.emitter,this.target=t.target,this.text=t.text,this.trigger=t.trigger,this.selectedText=""}},{key:"initSelection",value:function(){this.text?this.selectFake():this.target&&this.selectTarget()}},{key:"selectFake",value:function(){var t=this,e="rtl"==document.documentElement.getAttribute("dir");this.removeFake(),this.fakeHandlerCallback=function(){return t.removeFake()},this.fakeHandler=this.container.addEventListener("click",this.fakeHandlerCallback)||!0,this.fakeElem=document.createElement("textarea"),this.fakeElem.style.fontSize="12pt",this.fakeElem.style.border="0",this.fakeElem.style.padding="0",this.fakeElem.style.margin="0",this.fakeElem.style.position="absolute",this.fakeElem.style[e?"right":"left"]="-9999px";var n=window.pageYOffset||document.documentElement.scrollTop;this.fakeElem.style.top=n+"px",this.fakeElem.setAttribute("readonly",""),this.fakeElem.value=this.text,this.container.appendChild(this.fakeElem),this.selectedText=(0,c.default)(this.fakeElem),this.copyText()}},{key:"removeFake",value:function(){this.fakeHandler&&(this.container.removeEventListener("click",this.fakeHandlerCallback),this.fakeHandler=null,this.fakeHandlerCallback=null),this.fakeElem&&(this.container.removeChild(this.fakeElem),this.fakeElem=null)}},{key:"selectTarget",value:function(){this.selectedText=(0,c.default)(this.target),this.copyText()}},{key:"copyText",value:function(){var e=void 0;try{e=document.execCommand(this.action)}catch(t){e=!1}this.handleResult(e)}},{key:"handleResult",value:function(t){this.emitter.emit(t?"success":"error",{action:this.action,text:this.selectedText,trigger:this.trigger,clearSelection:this.clearSelection.bind(this)})}},{key:"clearSelection",value:function(){this.trigger&&this.trigger.focus(),window.getSelection().removeAllRanges()}},{key:"destroy",value:function(){this.removeFake()}},{key:"action",set:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:"copy";if(this._action=t,"copy"!==this._action&&"cut"!==this._action)throw new Error('Invalid "action" value, use either "copy" or "cut"')},get:function(){return this._action}},{key:"target",set:function(t){if(void 0!==t){if(!t||"object"!==(void 0===t?"undefined":r(t))||1!==t.nodeType)throw new Error('Invalid "target" value, use a valid Element');if("copy"===this.action&&t.hasAttribute("disabled"))throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');if("cut"===this.action&&(t.hasAttribute("readonly")||t.hasAttribute("disabled")))throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');this._target=t}},get:function(){return this._target}}]),e}();t.exports=u},function(t,e){t.exports=function(t){var e;if("SELECT"===t.nodeName)t.focus(),e=t.value;else if("INPUT"===t.nodeName||"TEXTAREA"===t.nodeName){var n=t.hasAttribute("readonly");n||t.setAttribute("readonly",""),t.select(),t.setSelectionRange(0,t.value.length),n||t.removeAttribute("readonly"),e=t.value}else{t.hasAttribute("contenteditable")&&t.focus();var o=window.getSelection(),r=document.createRange();r.selectNodeContents(t),o.removeAllRanges(),o.addRange(r),e=o.toString()}return e}},function(t,e){function n(){}n.prototype={on:function(t,e,n){var o=this.e||(this.e={});return(o[t]||(o[t]=[])).push({fn:e,ctx:n}),this},once:function(t,e,n){var o=this;function r(){o.off(t,r),e.apply(n,arguments)}return r._=e,this.on(t,r,n)},emit:function(t){for(var e=[].slice.call(arguments,1),n=((this.e||(this.e={}))[t]||[]).slice(),o=0,r=n.length;o<r;o++)n[o].fn.apply(n[o].ctx,e);return this},off:function(t,e){var n=this.e||(this.e={}),o=n[t],r=[];if(o&&e)for(var i=0,a=o.length;i<a;i++)o[i].fn!==e&&o[i].fn._!==e&&r.push(o[i]);return r.length?n[t]=r:delete n[t],this}},t.exports=n},function(t,e,n){var d=n(5),h=n(6);t.exports=function(t,e,n){if(!t&&!e&&!n)throw new Error("Missing required arguments");if(!d.string(e))throw new TypeError("Second argument must be a String");if(!d.fn(n))throw new TypeError("Third argument must be a Function");if(d.node(t))return s=e,f=n,(l=t).addEventListener(s,f),{destroy:function(){l.removeEventListener(s,f)}};if(d.nodeList(t))return a=t,c=e,u=n,Array.prototype.forEach.call(a,function(t){t.addEventListener(c,u)}),{destroy:function(){Array.prototype.forEach.call(a,function(t){t.removeEventListener(c,u)})}};if(d.string(t))return o=t,r=e,i=n,h(document.body,o,r,i);throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList");var o,r,i,a,c,u,l,s,f}},function(t,n){n.node=function(t){return void 0!==t&&t instanceof HTMLElement&&1===t.nodeType},n.nodeList=function(t){var e=Object.prototype.toString.call(t);return void 0!==t&&("[object NodeList]"===e||"[object HTMLCollection]"===e)&&"length"in t&&(0===t.length||n.node(t[0]))},n.string=function(t){return"string"==typeof t||t instanceof String},n.fn=function(t){return"[object Function]"===Object.prototype.toString.call(t)}},function(t,e,n){var a=n(7);function i(t,e,n,o,r){var i=function(e,n,t,o){return function(t){t.delegateTarget=a(t.target,n),t.delegateTarget&&o.call(e,t)}}.apply(this,arguments);return t.addEventListener(n,i,r),{destroy:function(){t.removeEventListener(n,i,r)}}}t.exports=function(t,e,n,o,r){return"function"==typeof t.addEventListener?i.apply(null,arguments):"function"==typeof n?i.bind(null,document).apply(null,arguments):("string"==typeof t&&(t=document.querySelectorAll(t)),Array.prototype.map.call(t,function(t){return i(t,e,n,o,r)}))}},function(t,e){if("undefined"!=typeof Element&&!Element.prototype.matches){var n=Element.prototype;n.matches=n.matchesSelector||n.mozMatchesSelector||n.msMatchesSelector||n.oMatchesSelector||n.webkitMatchesSelector}t.exports=function(t,e){for(;t&&9!==t.nodeType;){if("function"==typeof t.matches&&t.matches(e))return t;t=t.parentNode}}}])});
/**
 * hoverIntent is similar to jQuery's built-in "hover" method except that
 * instead of firing the handlerIn function immediately, hoverIntent checks
 * to see if the user's mouse has slowed down (beneath the sensitivity
 * threshold) before firing the event. The handlerOut function is only
 * called after a matching handlerIn.
 *
 * hoverIntent r7 // 2013.03.11 // jQuery 1.9.1+
 * http://cherne.net/brian/resources/jquery.hoverIntent.html
 *
 * You may use hoverIntent under the terms of the MIT license. Basically that
 * means you are free to use hoverIntent as long as this header is left intact.
 * Copyright 2007, 2013 Brian Cherne
 *
 * // basic usage ... just like .hover()
 * .hoverIntent( handlerIn, handlerOut )
 * .hoverIntent( handlerInOut )
 *
 * // basic usage ... with event delegation!
 * .hoverIntent( handlerIn, handlerOut, selector )
 * .hoverIntent( handlerInOut, selector )
 *
 * // using a basic configuration object
 * .hoverIntent( config )
 *
 * @param  handlerIn   function OR configuration object
 * @param  handlerOut  function OR selector for delegation OR undefined
 * @param  selector    selector OR undefined
 * @author Brian Cherne <brian(at)cherne(dot)net>
 **/
(function($) {
    $.fn.hoverIntent = function(handlerIn,handlerOut,selector) {

        // default configuration values
        var cfg = {
            interval: 100,
            sensitivity: 7,
            timeout: 0
        };

        if ( typeof handlerIn === "object" ) {
            cfg = $.extend(cfg, handlerIn );
        } else if ($.isFunction(handlerOut)) {
            cfg = $.extend(cfg, { over: handlerIn, out: handlerOut, selector: selector } );
        } else {
            cfg = $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut } );
        }

        // instantiate variables
        // cX, cY = current X and Y position of mouse, updated by mousemove event
        // pX, pY = previous X and Y position of mouse, set by mouseover and polling interval
        var cX, cY, pX, pY;

        // A private function for getting mouse position
        var track = function(ev) {
            cX = ev.pageX;
            cY = ev.pageY;
        };

        // A private function for comparing current and previous mouse position
        var compare = function(ev,ob) {
            ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            // compare mouse positions to see if they've crossed the threshold
            if ( ( Math.abs(pX-cX) + Math.abs(pY-cY) ) < cfg.sensitivity ) {
                $(ob).off("mousemove.hoverIntent",track);
                // set hoverIntent state to true (so mouseOut can be called)
                ob.hoverIntent_s = 1;
                return cfg.over.apply(ob,[ev]);
            } else {
                // set previous coordinates for next time
                pX = cX; pY = cY;
                // use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
                ob.hoverIntent_t = setTimeout( function(){compare(ev, ob);} , cfg.interval );
            }
        };

        // A private function for delaying the mouseOut function
        var delay = function(ev,ob) {
            ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            ob.hoverIntent_s = 0;
            return cfg.out.apply(ob,[ev]);
        };

        // A private function for handling mouse 'hovering'
        var handleHover = function(e) {
            // copy objects to be passed into t (required for event object to be passed in IE)
            var ev = jQuery.extend({},e);
            var ob = this;

            // cancel hoverIntent timer if it exists
            if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); }

            // if e.type == "mouseenter"
            if (e.type == "mouseenter") {
                // set "previous" X and Y position based on initial entry point
                pX = ev.pageX; pY = ev.pageY;
                // update "current" X and Y position based on mousemove
                $(ob).on("mousemove.hoverIntent",track);
                // start polling interval (self-calling timeout) to compare mouse coordinates over time
                if (ob.hoverIntent_s != 1) { ob.hoverIntent_t = setTimeout( function(){compare(ev,ob);} , cfg.interval );}

                // else e.type == "mouseleave"
            } else {
                // unbind expensive mousemove event
                $(ob).off("mousemove.hoverIntent",track);
                // if hoverIntent state is true, then call the mouseOut function after the specified delay
                if (ob.hoverIntent_s == 1) { ob.hoverIntent_t = setTimeout( function(){delay(ev,ob);} , cfg.timeout );}
            }
        };

        // listen for mouseenter and mouseleave
        return this.on({'mouseenter.hoverIntent':handleHover,'mouseleave.hoverIntent':handleHover}, cfg.selector);
    };
})(jQuery);
/* ===================================================
 *  jquery-sortable.js v0.9.13
 *  http://johnny.github.com/jquery-sortable/
 * ===================================================
 *  Copyright (c) 2012 Jonas von Andrian
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ========================================================== */

!function ( $, window, pluginName, undefined){
  var containerDefaults = {
    // If true, items can be dragged from this container
    drag: true,
    // If true, items can be droped onto this container
    drop: true,
    // Exclude items from being draggable, if the
    // selector matches the item
    exclude: "",
    // If true, search for nested containers within an item.If you nest containers,
    // either the original selector with which you call the plugin must only match the top containers,
    // or you need to specify a group (see the bootstrap nav example)
    nested: true,
    // If true, the items are assumed to be arranged vertically
    vertical: true
  }, // end container defaults
  groupDefaults = {
    // This is executed after the placeholder has been moved.
    // $closestItemOrContainer contains the closest item, the placeholder
    // has been put at or the closest empty Container, the placeholder has
    // been appended to.
    afterMove: function ($placeholder, container, $closestItemOrContainer) {
    },
    // The exact css path between the container and its items, e.g. "> tbody"
    containerPath: "",
    // The css selector of the containers
    containerSelector: "ol, ul",
    // Distance the mouse has to travel to start dragging
    distance: 0,
    // Time in milliseconds after mousedown until dragging should start.
    // This option can be used to prevent unwanted drags when clicking on an element.
    delay: 0,
    // The css selector of the drag handle
    handle: "",
    // The exact css path between the item and its subcontainers.
    // It should only match the immediate items of a container.
    // No item of a subcontainer should be matched. E.g. for ol>div>li the itemPath is "> div"
    itemPath: "",
    // The css selector of the items
    itemSelector: "li",
    // The class given to "body" while an item is being dragged
    bodyClass: "dragging",
    // The class giving to an item while being dragged
    draggedClass: "dragged",
    // Check if the dragged item may be inside the container.
    // Use with care, since the search for a valid container entails a depth first search
    // and may be quite expensive.
    isValidTarget: function ($item, container) {
      return true
    },
    // Executed before onDrop if placeholder is detached.
    // This happens if pullPlaceholder is set to false and the drop occurs outside a container.
    onCancel: function ($item, container, _super, event) {
    },
    // Executed at the beginning of a mouse move event.
    // The Placeholder has not been moved yet.
    onDrag: function ($item, position, _super, event) {
      $item.css(position)
    },
    // Called after the drag has been started,
    // that is the mouse button is being held down and
    // the mouse is moving.
    // The container is the closest initialized container.
    // Therefore it might not be the container, that actually contains the item.
    onDragStart: function ($item, container, _super, event) {
      $item.css({
        height: $item.outerHeight(),
        width: $item.outerWidth()
      })
      $item.addClass(container.group.options.draggedClass)
      $("body").addClass(container.group.options.bodyClass)
    },
    // Called when the mouse button is being released
    onDrop: function ($item, container, _super, event) {
      $item.removeClass(container.group.options.draggedClass).removeAttr("style")
      $("body").removeClass(container.group.options.bodyClass)
    },
    // Called on mousedown. If falsy value is returned, the dragging will not start.
    // Ignore if element clicked is input, select or textarea
    onMousedown: function ($item, _super, event) {
      if (!event.target.nodeName.match(/^(input|select|textarea)$/i)) {
        event.preventDefault()
        return true
      }
    },
    // The class of the placeholder (must match placeholder option markup)
    placeholderClass: "placeholder",
    // Template for the placeholder. Can be any valid jQuery input
    // e.g. a string, a DOM element.
    // The placeholder must have the class "placeholder"
    placeholder: '<li class="placeholder"></li>',
    // If true, the position of the placeholder is calculated on every mousemove.
    // If false, it is only calculated when the mouse is above a container.
    pullPlaceholder: true,
    // Specifies serialization of the container group.
    // The pair $parent/$children is either container/items or item/subcontainers.
    serialize: function ($parent, $children, parentIsContainer) {
      var result = $.extend({}, $parent.data())

      if(parentIsContainer)
        return [$children]
      else if ($children[0]){
        result.children = $children
      }

      delete result.subContainers
      delete result.sortable

      return result
    },
    // Set tolerance while dragging. Positive values decrease sensitivity,
    // negative values increase it.
    tolerance: 0
  }, // end group defaults
  containerGroups = {},
  groupCounter = 0,
  emptyBox = {
    left: 0,
    top: 0,
    bottom: 0,
    right:0
  },
  eventNames = {
    start: "touchstart.sortable mousedown.sortable",
    drop: "touchend.sortable touchcancel.sortable mouseup.sortable",
    drag: "touchmove.sortable mousemove.sortable",
    scroll: "scroll.sortable"
  },
  subContainerKey = "subContainers"

  /*
   * a is Array [left, right, top, bottom]
   * b is array [left, top]
   */
  function d(a,b) {
    var x = Math.max(0, a[0] - b[0], b[0] - a[1]),
    y = Math.max(0, a[2] - b[1], b[1] - a[3])
    return x+y;
  }

  function setDimensions(array, dimensions, tolerance, useOffset) {
    var i = array.length,
    offsetMethod = useOffset ? "offset" : "position"
    tolerance = tolerance || 0

    while(i--){
      var el = array[i].el ? array[i].el : $(array[i]),
      // use fitting method
      pos = el[offsetMethod]()
      pos.left += parseInt(el.css('margin-left'), 10)
      pos.top += parseInt(el.css('margin-top'),10)
      dimensions[i] = [
        pos.left - tolerance,
        pos.left + el.outerWidth() + tolerance,
        pos.top - tolerance,
        pos.top + el.outerHeight() + tolerance
      ]
    }
  }

  function getRelativePosition(pointer, element) {
    var offset = element.offset()
    return {
      left: pointer.left - offset.left,
      top: pointer.top - offset.top
    }
  }

  function sortByDistanceDesc(dimensions, pointer, lastPointer) {
    pointer = [pointer.left, pointer.top]
    lastPointer = lastPointer && [lastPointer.left, lastPointer.top]

    var dim,
    i = dimensions.length,
    distances = []

    while(i--){
      dim = dimensions[i]
      distances[i] = [i,d(dim,pointer), lastPointer && d(dim, lastPointer)]
    }
    distances = distances.sort(function  (a,b) {
      return b[1] - a[1] || b[2] - a[2] || b[0] - a[0]
    })

    // last entry is the closest
    return distances
  }

  function ContainerGroup(options) {
    this.options = $.extend({}, groupDefaults, options)
    this.containers = []

    if(!this.options.rootGroup){
      this.scrollProxy = $.proxy(this.scroll, this)
      this.dragProxy = $.proxy(this.drag, this)
      this.dropProxy = $.proxy(this.drop, this)
      this.placeholder = $(this.options.placeholder)

      if(!options.isValidTarget)
        this.options.isValidTarget = undefined
    }
  }

  ContainerGroup.get = function  (options) {
    if(!containerGroups[options.group]) {
      if(options.group === undefined)
        options.group = groupCounter ++

      containerGroups[options.group] = new ContainerGroup(options)
    }

    return containerGroups[options.group]
  }

  ContainerGroup.prototype = {
    dragInit: function  (e, itemContainer) {
      this.$document = $(itemContainer.el[0].ownerDocument)

      // get item to drag
      var closestItem = $(e.target).closest(this.options.itemSelector);
      // using the length of this item, prevents the plugin from being started if there is no handle being clicked on.
      // this may also be helpful in instantiating multidrag.
      if (closestItem.length) {
        this.item = closestItem;
        this.itemContainer = itemContainer;
        if (this.item.is(this.options.exclude) || !this.options.onMousedown(this.item, groupDefaults.onMousedown, e)) {
            return;
        }
        this.setPointer(e);
        this.toggleListeners('on');
        this.setupDelayTimer();
        this.dragInitDone = true;
      }
    },
    drag: function  (e) {
      if(!this.dragging){
        if(!this.distanceMet(e) || !this.delayMet)
          return

        this.options.onDragStart(this.item, this.itemContainer, groupDefaults.onDragStart, e)
        this.item.before(this.placeholder)
        this.dragging = true
      }

      this.setPointer(e)
      // place item under the cursor
      this.options.onDrag(this.item,
                          getRelativePosition(this.pointer, this.item.offsetParent()),
                          groupDefaults.onDrag,
                          e)

      var p = this.getPointer(e),
      box = this.sameResultBox,
      t = this.options.tolerance

      if(!box || box.top - t > p.top || box.bottom + t < p.top || box.left - t > p.left || box.right + t < p.left)
        if(!this.searchValidTarget()){
          this.placeholder.detach()
          this.lastAppendedItem = undefined
        }
    },
    drop: function  (e) {
      this.toggleListeners('off')

      this.dragInitDone = false

      if(this.dragging){
        // processing Drop, check if placeholder is detached
        if(this.placeholder.closest("html")[0]){
          this.placeholder.before(this.item).detach()
        } else {
          this.options.onCancel(this.item, this.itemContainer, groupDefaults.onCancel, e)
        }
        this.options.onDrop(this.item, this.getContainer(this.item), groupDefaults.onDrop, e)

        // cleanup
        this.clearDimensions()
        this.clearOffsetParent()
        this.lastAppendedItem = this.sameResultBox = undefined
        this.dragging = false
      }
    },
    searchValidTarget: function  (pointer, lastPointer) {
      if(!pointer){
        pointer = this.relativePointer || this.pointer
        lastPointer = this.lastRelativePointer || this.lastPointer
      }

      var distances = sortByDistanceDesc(this.getContainerDimensions(),
                                         pointer,
                                         lastPointer),
      i = distances.length

      while(i--){
        var index = distances[i][0],
        distance = distances[i][1]

        if(!distance || this.options.pullPlaceholder){
          var container = this.containers[index]
          if(!container.disabled){
            if(!this.$getOffsetParent()){
              var offsetParent = container.getItemOffsetParent()
              pointer = getRelativePosition(pointer, offsetParent)
              lastPointer = getRelativePosition(lastPointer, offsetParent)
            }
            if(container.searchValidTarget(pointer, lastPointer))
              return true
          }
        }
      }
      if(this.sameResultBox)
        this.sameResultBox = undefined
    },
    movePlaceholder: function  (container, item, method, sameResultBox) {
      var lastAppendedItem = this.lastAppendedItem
      if(!sameResultBox && lastAppendedItem && lastAppendedItem[0] === item[0])
        return;

      item[method](this.placeholder)
      this.lastAppendedItem = item
      this.sameResultBox = sameResultBox
      this.options.afterMove(this.placeholder, container, item)
    },
    getContainerDimensions: function  () {
      if(!this.containerDimensions)
        setDimensions(this.containers, this.containerDimensions = [], this.options.tolerance, !this.$getOffsetParent())
      return this.containerDimensions
    },
    getContainer: function  (element) {
      return element.closest(this.options.containerSelector).data(pluginName)
    },
    $getOffsetParent: function  () {
      if(this.offsetParent === undefined){
        var i = this.containers.length - 1,
        offsetParent = this.containers[i].getItemOffsetParent()

        if(!this.options.rootGroup){
          while(i--){
            if(offsetParent[0] != this.containers[i].getItemOffsetParent()[0]){
              // If every container has the same offset parent,
              // use position() which is relative to this parent,
              // otherwise use offset()
              // compare #setDimensions
              offsetParent = false
              break;
            }
          }
        }

        this.offsetParent = offsetParent
      }
      return this.offsetParent
    },
    setPointer: function (e) {
      var pointer = this.getPointer(e)

      if(this.$getOffsetParent()){
        var relativePointer = getRelativePosition(pointer, this.$getOffsetParent())
        this.lastRelativePointer = this.relativePointer
        this.relativePointer = relativePointer
      }

      this.lastPointer = this.pointer
      this.pointer = pointer
    },
    distanceMet: function (e) {
      var currentPointer = this.getPointer(e)
      return (Math.max(
        Math.abs(this.pointer.left - currentPointer.left),
        Math.abs(this.pointer.top - currentPointer.top)
      ) >= this.options.distance)
    },
    getPointer: function(e) {
      var o = e.originalEvent || e.originalEvent.touches && e.originalEvent.touches[0]
      return {
        left: e.pageX || o.pageX,
        top: e.pageY || o.pageY
      }
    },
    setupDelayTimer: function () {
      var that = this
      this.delayMet = !this.options.delay

      // init delay timer if needed
      if (!this.delayMet) {
        clearTimeout(this._mouseDelayTimer);
        this._mouseDelayTimer = setTimeout(function() {
          that.delayMet = true
        }, this.options.delay)
      }
    },
    scroll: function  (e) {
      this.clearDimensions()
      this.clearOffsetParent() // TODO is this needed?
    },
    toggleListeners: function (method) {
      var that = this,
      events = ['drag','drop','scroll']

      $.each(events,function  (i,event) {
        that.$document[method](eventNames[event], that[event + 'Proxy'])
      })
    },
    clearOffsetParent: function () {
      this.offsetParent = undefined
    },
    // Recursively clear container and item dimensions
    clearDimensions: function  () {
      this.traverse(function(object){
        object._clearDimensions()
      })
    },
    traverse: function(callback) {
      callback(this)
      var i = this.containers.length
      while(i--){
        this.containers[i].traverse(callback)
      }
    },
    _clearDimensions: function(){
      this.containerDimensions = undefined
    },
    _destroy: function () {
      containerGroups[this.options.group] = undefined
    }
  }

  function Container(element, options) {
    this.el = element
    this.options = $.extend( {}, containerDefaults, options)

    this.group = ContainerGroup.get(this.options)
    this.rootGroup = this.options.rootGroup || this.group
    this.handle = this.rootGroup.options.handle || this.rootGroup.options.itemSelector

    var itemPath = this.rootGroup.options.itemPath
    this.target = itemPath ? this.el.find(itemPath) : this.el

    this.target.on(eventNames.start, this.handle, $.proxy(this.dragInit, this))

    if(this.options.drop)
      this.group.containers.push(this)
  }

  Container.prototype = {
    dragInit: function  (e) {
      var rootGroup = this.rootGroup

      if( !this.disabled &&
          !rootGroup.dragInitDone &&
          this.options.drag &&
          this.isValidDrag(e)) {
        rootGroup.dragInit(e, this)
      }
    },
    isValidDrag: function(e) {
      return e.which == 1 ||
        e.type == "touchstart" && e.originalEvent.touches.length == 1
    },
    searchValidTarget: function  (pointer, lastPointer) {
      var distances = sortByDistanceDesc(this.getItemDimensions(),
                                         pointer,
                                         lastPointer),
      i = distances.length,
      rootGroup = this.rootGroup,
      validTarget = !rootGroup.options.isValidTarget ||
        rootGroup.options.isValidTarget(rootGroup.item, this)

      if(!i && validTarget){
        rootGroup.movePlaceholder(this, this.target, "append")
        return true
      } else
        while(i--){
          var index = distances[i][0],
          distance = distances[i][1]
          if(!distance && this.hasChildGroup(index)){
            var found = this.getContainerGroup(index).searchValidTarget(pointer, lastPointer)
            if(found)
              return true
          }
          else if(validTarget){
            this.movePlaceholder(index, pointer)
            return true
          }
        }
    },
    movePlaceholder: function  (index, pointer) {
      var item = $(this.items[index]),
      dim = this.itemDimensions[index],
      method = "after",
      width = item.outerWidth(),
      height = item.outerHeight(),
      offset = item.offset(),
      sameResultBox = {
        left: offset.left,
        right: offset.left + width,
        top: offset.top,
        bottom: offset.top + height
      }
      if(this.options.vertical){
        var yCenter = (dim[2] + dim[3]) / 2,
        inUpperHalf = pointer.top <= yCenter
        if(inUpperHalf){
          method = "before"
          sameResultBox.bottom -= height / 2
        } else
          sameResultBox.top += height / 2
      } else {
        var xCenter = (dim[0] + dim[1]) / 2,
        inLeftHalf = pointer.left <= xCenter
        if(inLeftHalf){
          method = "before"
          sameResultBox.right -= width / 2
        } else
          sameResultBox.left += width / 2
      }
      if(this.hasChildGroup(index))
        sameResultBox = emptyBox
      this.rootGroup.movePlaceholder(this, item, method, sameResultBox)
    },
    getItemDimensions: function  () {
      if(!this.itemDimensions){
        this.items = this.$getChildren(this.el, "item").filter(
          ":not(." + this.group.options.placeholderClass + ", ." + this.group.options.draggedClass + ")"
        ).get()
        setDimensions(this.items, this.itemDimensions = [], this.options.tolerance)
      }
      return this.itemDimensions
    },
    getItemOffsetParent: function  () {
      var offsetParent,
      el = this.el
      // Since el might be empty we have to check el itself and
      // can not do something like el.children().first().offsetParent()
      if(el.css("position") === "relative" || el.css("position") === "absolute"  || el.css("position") === "fixed")
        offsetParent = el
      else
        offsetParent = el.offsetParent()
      return offsetParent
    },
    hasChildGroup: function (index) {
      return this.options.nested && this.getContainerGroup(index)
    },
    getContainerGroup: function  (index) {
      var childGroup = $.data(this.items[index], subContainerKey)
      if( childGroup === undefined){
        var childContainers = this.$getChildren(this.items[index], "container")
        childGroup = false

        if(childContainers[0]){
          var options = $.extend({}, this.options, {
            rootGroup: this.rootGroup,
            group: groupCounter ++
          })
          childGroup = childContainers[pluginName](options).data(pluginName).group
        }
        $.data(this.items[index], subContainerKey, childGroup)
      }
      return childGroup
    },
    $getChildren: function (parent, type) {
      var options = this.rootGroup.options,
      path = options[type + "Path"],
      selector = options[type + "Selector"]

      parent = $(parent)
      if(path)
        parent = parent.find(path)

      return parent.children(selector)
    },
    _serialize: function (parent, isContainer) {
      var that = this,
      childType = isContainer ? "item" : "container",

      children = this.$getChildren(parent, childType).not(this.options.exclude).map(function () {
        return that._serialize($(this), !isContainer)
      }).get()

      return this.rootGroup.options.serialize(parent, children, isContainer)
    },
    traverse: function(callback) {
      $.each(this.items || [], function(item){
        var group = $.data(this, subContainerKey)
        if(group)
          group.traverse(callback)
      });

      callback(this)
    },
    _clearDimensions: function  () {
      this.itemDimensions = undefined
    },
    _destroy: function() {
      var that = this;

      this.target.off(eventNames.start, this.handle);
      this.el.removeData(pluginName)

      if(this.options.drop)
        this.group.containers = $.grep(this.group.containers, function(val){
          return val != that
        })

      $.each(this.items || [], function(){
        $.removeData(this, subContainerKey)
      })
    }
  }

  var API = {
    enable: function() {
      this.traverse(function(object){
        object.disabled = false
      })
    },
    disable: function (){
      this.traverse(function(object){
        object.disabled = true
      })
    },
    serialize: function () {
      return this._serialize(this.el, true)
    },
    refresh: function() {
      this.traverse(function(object){
        object._clearDimensions()
      })
    },
    destroy: function () {
      this.traverse(function(object){
        object._destroy();
      })
    }
  }

  $.extend(Container.prototype, API)

  /**
   * jQuery API
   *
   * Parameters are
   *   either options on init
   *   or a method name followed by arguments to pass to the method
   */
  $.fn[pluginName] = function(methodOrOptions) {
    var args = Array.prototype.slice.call(arguments, 1)

    return this.map(function(){
      var $t = $(this),
      object = $t.data(pluginName)

      if(object && API[methodOrOptions])
        return API[methodOrOptions].apply(object, args) || this
      else if(!object && (methodOrOptions === undefined ||
                          typeof methodOrOptions === "object"))
        $t.data(pluginName, new Container($t, methodOrOptions))

      return this
    });
  };

}(jQuery, window, 'sortable');

/*
 * Replace all SVG images with inline SVG
 */
jQuery('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});
// ==================================================
// fancyBox v3.5.7
//
// Licensed GPLv3 for open source use
// or fancyBox Commercial License for commercial use
//
// http://fancyapps.com/fancybox/
// Copyright 2019 fancyApps
//
// ==================================================
!function(t,e,n,o){"use strict";function i(t,e){var o,i,a,s=[],r=0;t&&t.isDefaultPrevented()||(t.preventDefault(),e=e||{},t&&t.data&&(e=h(t.data.options,e)),o=e.$target||n(t.currentTarget).trigger("blur"),(a=n.fancybox.getInstance())&&a.$trigger&&a.$trigger.is(o)||(e.selector?s=n(e.selector):(i=o.attr("data-fancybox")||"",i?(s=t.data?t.data.items:[],s=s.length?s.filter('[data-fancybox="'+i+'"]'):n('[data-fancybox="'+i+'"]')):s=[o]),r=n(s).index(o),r<0&&(r=0),a=n.fancybox.open(s,e,r),a.$trigger=o))}if(t.console=t.console||{info:function(t){}},n){if(n.fn.fancybox)return void console.info("fancyBox already initialized");var a={closeExisting:!1,loop:!1,gutter:50,keyboard:!0,preventCaptionOverlap:!0,arrows:!0,infobar:!0,smallBtn:"auto",toolbar:"auto",buttons:["zoom","slideShow","thumbs","close"],idleTime:3,protect:!1,modal:!1,image:{preload:!1},ajax:{settings:{data:{fancybox:!0}}},iframe:{tpl:'<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" allowfullscreen="allowfullscreen" allow="autoplay; fullscreen" src=""></iframe>',preload:!0,css:{},attr:{scrolling:"auto"}},video:{tpl:'<video class="fancybox-video" controls controlsList="nodownload" poster="{{poster}}"><source src="{{src}}" type="{{format}}" />Sorry, your browser doesn\'t support embedded videos, <a href="{{src}}">download</a> and watch with your favorite video player!</video>',format:"",autoStart:!0},defaultType:"image",animationEffect:"zoom",animationDuration:366,zoomOpacity:"auto",transitionEffect:"fade",transitionDuration:366,slideClass:"",baseClass:"",baseTpl:'<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div><div class="fancybox-inner"><div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div><div class="fancybox-toolbar">{{buttons}}</div><div class="fancybox-navigation">{{arrows}}</div><div class="fancybox-stage"></div><div class="fancybox-caption"><div class="fancybox-caption__body"></div></div></div></div>',spinnerTpl:'<div class="fancybox-loading"></div>',errorTpl:'<div class="fancybox-error"><p>{{ERROR}}</p></div>',btnTpl:{download:'<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="{{DOWNLOAD}}" href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.62 17.09V19H5.38v-1.91zm-2.97-6.96L17 11.45l-5 4.87-5-4.87 1.36-1.32 2.68 2.64V5h1.92v7.77z"/></svg></a>',zoom:'<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="{{ZOOM}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.7 17.3l-3-3a5.9 5.9 0 0 0-.6-7.6 5.9 5.9 0 0 0-8.4 0 5.9 5.9 0 0 0 0 8.4 5.9 5.9 0 0 0 7.7.7l3 3a1 1 0 0 0 1.3 0c.4-.5.4-1 0-1.5zM8.1 13.8a4 4 0 0 1 0-5.7 4 4 0 0 1 5.7 0 4 4 0 0 1 0 5.7 4 4 0 0 1-5.7 0z"/></svg></button>',close:'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"/></svg></button>',arrowLeft:'<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}"><div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.28 15.7l-1.34 1.37L5 12l4.94-5.07 1.34 1.38-2.68 2.72H19v1.94H8.6z"/></svg></div></button>',arrowRight:'<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}"><div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15.4 12.97l-2.68 2.72 1.34 1.38L19 12l-4.94-5.07-1.34 1.38 2.68 2.72H5v1.94z"/></svg></div></button>',smallBtn:'<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}"><svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"/></svg></button>'},parentEl:"body",hideScrollbar:!0,autoFocus:!0,backFocus:!0,trapFocus:!0,fullScreen:{autoStart:!1},touch:{vertical:!0,momentum:!0},hash:null,media:{},slideShow:{autoStart:!1,speed:3e3},thumbs:{autoStart:!1,hideOnClose:!0,parentEl:".fancybox-container",axis:"y"},wheel:"auto",onInit:n.noop,beforeLoad:n.noop,afterLoad:n.noop,beforeShow:n.noop,afterShow:n.noop,beforeClose:n.noop,afterClose:n.noop,onActivate:n.noop,onDeactivate:n.noop,clickContent:function(t,e){return"image"===t.type&&"zoom"},clickSlide:"close",clickOutside:"close",dblclickContent:!1,dblclickSlide:!1,dblclickOutside:!1,mobile:{preventCaptionOverlap:!1,idleTime:!1,clickContent:function(t,e){return"image"===t.type&&"toggleControls"},clickSlide:function(t,e){return"image"===t.type?"toggleControls":"close"},dblclickContent:function(t,e){return"image"===t.type&&"zoom"},dblclickSlide:function(t,e){return"image"===t.type&&"zoom"}},lang:"en",i18n:{en:{CLOSE:"Close",NEXT:"Next",PREV:"Previous",ERROR:"The requested content cannot be loaded. <br/> Please try again later.",PLAY_START:"Start slideshow",PLAY_STOP:"Pause slideshow",FULL_SCREEN:"Full screen",THUMBS:"Thumbnails",DOWNLOAD:"Download",SHARE:"Share",ZOOM:"Zoom"},de:{CLOSE:"Schlie&szlig;en",NEXT:"Weiter",PREV:"Zur&uuml;ck",ERROR:"Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es sp&auml;ter nochmal.",PLAY_START:"Diaschau starten",PLAY_STOP:"Diaschau beenden",FULL_SCREEN:"Vollbild",THUMBS:"Vorschaubilder",DOWNLOAD:"Herunterladen",SHARE:"Teilen",ZOOM:"Vergr&ouml;&szlig;ern"}}},s=n(t),r=n(e),c=0,l=function(t){return t&&t.hasOwnProperty&&t instanceof n},d=function(){return t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||t.oRequestAnimationFrame||function(e){return t.setTimeout(e,1e3/60)}}(),u=function(){return t.cancelAnimationFrame||t.webkitCancelAnimationFrame||t.mozCancelAnimationFrame||t.oCancelAnimationFrame||function(e){t.clearTimeout(e)}}(),f=function(){var t,n=e.createElement("fakeelement"),o={transition:"transitionend",OTransition:"oTransitionEnd",MozTransition:"transitionend",WebkitTransition:"webkitTransitionEnd"};for(t in o)if(void 0!==n.style[t])return o[t];return"transitionend"}(),p=function(t){return t&&t.length&&t[0].offsetHeight},h=function(t,e){var o=n.extend(!0,{},t,e);return n.each(e,function(t,e){n.isArray(e)&&(o[t]=e)}),o},g=function(t){var o,i;return!(!t||t.ownerDocument!==e)&&(n(".fancybox-container").css("pointer-events","none"),o={x:t.getBoundingClientRect().left+t.offsetWidth/2,y:t.getBoundingClientRect().top+t.offsetHeight/2},i=e.elementFromPoint(o.x,o.y)===t,n(".fancybox-container").css("pointer-events",""),i)},b=function(t,e,o){var i=this;i.opts=h({index:o},n.fancybox.defaults),n.isPlainObject(e)&&(i.opts=h(i.opts,e)),n.fancybox.isMobile&&(i.opts=h(i.opts,i.opts.mobile)),i.id=i.opts.id||++c,i.currIndex=parseInt(i.opts.index,10)||0,i.prevIndex=null,i.prevPos=null,i.currPos=0,i.firstRun=!0,i.group=[],i.slides={},i.addContent(t),i.group.length&&i.init()};n.extend(b.prototype,{init:function(){var o,i,a=this,s=a.group[a.currIndex],r=s.opts;r.closeExisting&&n.fancybox.close(!0),n("body").addClass("fancybox-active"),!n.fancybox.getInstance()&&!1!==r.hideScrollbar&&!n.fancybox.isMobile&&e.body.scrollHeight>t.innerHeight&&(n("head").append('<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar{margin-right:'+(t.innerWidth-e.documentElement.clientWidth)+"px;}</style>"),n("body").addClass("compensate-for-scrollbar")),i="",n.each(r.buttons,function(t,e){i+=r.btnTpl[e]||""}),o=n(a.translate(a,r.baseTpl.replace("{{buttons}}",i).replace("{{arrows}}",r.btnTpl.arrowLeft+r.btnTpl.arrowRight))).attr("id","fancybox-container-"+a.id).addClass(r.baseClass).data("FancyBox",a).appendTo(r.parentEl),a.$refs={container:o},["bg","inner","infobar","toolbar","stage","caption","navigation"].forEach(function(t){a.$refs[t]=o.find(".fancybox-"+t)}),a.trigger("onInit"),a.activate(),a.jumpTo(a.currIndex)},translate:function(t,e){var n=t.opts.i18n[t.opts.lang]||t.opts.i18n.en;return e.replace(/\{\{(\w+)\}\}/g,function(t,e){return void 0===n[e]?t:n[e]})},addContent:function(t){var e,o=this,i=n.makeArray(t);n.each(i,function(t,e){var i,a,s,r,c,l={},d={};n.isPlainObject(e)?(l=e,d=e.opts||e):"object"===n.type(e)&&n(e).length?(i=n(e),d=i.data()||{},d=n.extend(!0,{},d,d.options),d.$orig=i,l.src=o.opts.src||d.src||i.attr("href"),l.type||l.src||(l.type="inline",l.src=e)):l={type:"html",src:e+""},l.opts=n.extend(!0,{},o.opts,d),n.isArray(d.buttons)&&(l.opts.buttons=d.buttons),n.fancybox.isMobile&&l.opts.mobile&&(l.opts=h(l.opts,l.opts.mobile)),a=l.type||l.opts.type,r=l.src||"",!a&&r&&((s=r.match(/\.(mp4|mov|ogv|webm)((\?|#).*)?$/i))?(a="video",l.opts.video.format||(l.opts.video.format="video/"+("ogv"===s[1]?"ogg":s[1]))):r.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i)?a="image":r.match(/\.(pdf)((\?|#).*)?$/i)?(a="iframe",l=n.extend(!0,l,{contentType:"pdf",opts:{iframe:{preload:!1}}})):"#"===r.charAt(0)&&(a="inline")),a?l.type=a:o.trigger("objectNeedsType",l),l.contentType||(l.contentType=n.inArray(l.type,["html","inline","ajax"])>-1?"html":l.type),l.index=o.group.length,"auto"==l.opts.smallBtn&&(l.opts.smallBtn=n.inArray(l.type,["html","inline","ajax"])>-1),"auto"===l.opts.toolbar&&(l.opts.toolbar=!l.opts.smallBtn),l.$thumb=l.opts.$thumb||null,l.opts.$trigger&&l.index===o.opts.index&&(l.$thumb=l.opts.$trigger.find("img:first"),l.$thumb.length&&(l.opts.$orig=l.opts.$trigger)),l.$thumb&&l.$thumb.length||!l.opts.$orig||(l.$thumb=l.opts.$orig.find("img:first")),l.$thumb&&!l.$thumb.length&&(l.$thumb=null),l.thumb=l.opts.thumb||(l.$thumb?l.$thumb[0].src:null),"function"===n.type(l.opts.caption)&&(l.opts.caption=l.opts.caption.apply(e,[o,l])),"function"===n.type(o.opts.caption)&&(l.opts.caption=o.opts.caption.apply(e,[o,l])),l.opts.caption instanceof n||(l.opts.caption=void 0===l.opts.caption?"":l.opts.caption+""),"ajax"===l.type&&(c=r.split(/\s+/,2),c.length>1&&(l.src=c.shift(),l.opts.filter=c.shift())),l.opts.modal&&(l.opts=n.extend(!0,l.opts,{trapFocus:!0,infobar:0,toolbar:0,smallBtn:0,keyboard:0,slideShow:0,fullScreen:0,thumbs:0,touch:0,clickContent:!1,clickSlide:!1,clickOutside:!1,dblclickContent:!1,dblclickSlide:!1,dblclickOutside:!1})),o.group.push(l)}),Object.keys(o.slides).length&&(o.updateControls(),(e=o.Thumbs)&&e.isActive&&(e.create(),e.focus()))},addEvents:function(){var e=this;e.removeEvents(),e.$refs.container.on("click.fb-close","[data-fancybox-close]",function(t){t.stopPropagation(),t.preventDefault(),e.close(t)}).on("touchstart.fb-prev click.fb-prev","[data-fancybox-prev]",function(t){t.stopPropagation(),t.preventDefault(),e.previous()}).on("touchstart.fb-next click.fb-next","[data-fancybox-next]",function(t){t.stopPropagation(),t.preventDefault(),e.next()}).on("click.fb","[data-fancybox-zoom]",function(t){e[e.isScaledDown()?"scaleToActual":"scaleToFit"]()}),s.on("orientationchange.fb resize.fb",function(t){t&&t.originalEvent&&"resize"===t.originalEvent.type?(e.requestId&&u(e.requestId),e.requestId=d(function(){e.update(t)})):(e.current&&"iframe"===e.current.type&&e.$refs.stage.hide(),setTimeout(function(){e.$refs.stage.show(),e.update(t)},n.fancybox.isMobile?600:250))}),r.on("keydown.fb",function(t){var o=n.fancybox?n.fancybox.getInstance():null,i=o.current,a=t.keyCode||t.which;if(9==a)return void(i.opts.trapFocus&&e.focus(t));if(!(!i.opts.keyboard||t.ctrlKey||t.altKey||t.shiftKey||n(t.target).is("input,textarea,video,audio,select")))return 8===a||27===a?(t.preventDefault(),void e.close(t)):37===a||38===a?(t.preventDefault(),void e.previous()):39===a||40===a?(t.preventDefault(),void e.next()):void e.trigger("afterKeydown",t,a)}),e.group[e.currIndex].opts.idleTime&&(e.idleSecondsCounter=0,r.on("mousemove.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle",function(t){e.idleSecondsCounter=0,e.isIdle&&e.showControls(),e.isIdle=!1}),e.idleInterval=t.setInterval(function(){++e.idleSecondsCounter>=e.group[e.currIndex].opts.idleTime&&!e.isDragging&&(e.isIdle=!0,e.idleSecondsCounter=0,e.hideControls())},1e3))},removeEvents:function(){var e=this;s.off("orientationchange.fb resize.fb"),r.off("keydown.fb .fb-idle"),this.$refs.container.off(".fb-close .fb-prev .fb-next"),e.idleInterval&&(t.clearInterval(e.idleInterval),e.idleInterval=null)},previous:function(t){return this.jumpTo(this.currPos-1,t)},next:function(t){return this.jumpTo(this.currPos+1,t)},jumpTo:function(t,e){var o,i,a,s,r,c,l,d,u,f=this,h=f.group.length;if(!(f.isDragging||f.isClosing||f.isAnimating&&f.firstRun)){if(t=parseInt(t,10),!(a=f.current?f.current.opts.loop:f.opts.loop)&&(t<0||t>=h))return!1;if(o=f.firstRun=!Object.keys(f.slides).length,r=f.current,f.prevIndex=f.currIndex,f.prevPos=f.currPos,s=f.createSlide(t),h>1&&((a||s.index<h-1)&&f.createSlide(t+1),(a||s.index>0)&&f.createSlide(t-1)),f.current=s,f.currIndex=s.index,f.currPos=s.pos,f.trigger("beforeShow",o),f.updateControls(),s.forcedDuration=void 0,n.isNumeric(e)?s.forcedDuration=e:e=s.opts[o?"animationDuration":"transitionDuration"],e=parseInt(e,10),i=f.isMoved(s),s.$slide.addClass("fancybox-slide--current"),o)return s.opts.animationEffect&&e&&f.$refs.container.css("transition-duration",e+"ms"),f.$refs.container.addClass("fancybox-is-open").trigger("focus"),f.loadSlide(s),void f.preload("image");c=n.fancybox.getTranslate(r.$slide),l=n.fancybox.getTranslate(f.$refs.stage),n.each(f.slides,function(t,e){n.fancybox.stop(e.$slide,!0)}),r.pos!==s.pos&&(r.isComplete=!1),r.$slide.removeClass("fancybox-slide--complete fancybox-slide--current"),i?(u=c.left-(r.pos*c.width+r.pos*r.opts.gutter),n.each(f.slides,function(t,o){o.$slide.removeClass("fancybox-animated").removeClass(function(t,e){return(e.match(/(^|\s)fancybox-fx-\S+/g)||[]).join(" ")});var i=o.pos*c.width+o.pos*o.opts.gutter;n.fancybox.setTranslate(o.$slide,{top:0,left:i-l.left+u}),o.pos!==s.pos&&o.$slide.addClass("fancybox-slide--"+(o.pos>s.pos?"next":"previous")),p(o.$slide),n.fancybox.animate(o.$slide,{top:0,left:(o.pos-s.pos)*c.width+(o.pos-s.pos)*o.opts.gutter},e,function(){o.$slide.css({transform:"",opacity:""}).removeClass("fancybox-slide--next fancybox-slide--previous"),o.pos===f.currPos&&f.complete()})})):e&&s.opts.transitionEffect&&(d="fancybox-animated fancybox-fx-"+s.opts.transitionEffect,r.$slide.addClass("fancybox-slide--"+(r.pos>s.pos?"next":"previous")),n.fancybox.animate(r.$slide,d,e,function(){r.$slide.removeClass(d).removeClass("fancybox-slide--next fancybox-slide--previous")},!1)),s.isLoaded?f.revealContent(s):f.loadSlide(s),f.preload("image")}},createSlide:function(t){var e,o,i=this;return o=t%i.group.length,o=o<0?i.group.length+o:o,!i.slides[t]&&i.group[o]&&(e=n('<div class="fancybox-slide"></div>').appendTo(i.$refs.stage),i.slides[t]=n.extend(!0,{},i.group[o],{pos:t,$slide:e,isLoaded:!1}),i.updateSlide(i.slides[t])),i.slides[t]},scaleToActual:function(t,e,o){var i,a,s,r,c,l=this,d=l.current,u=d.$content,f=n.fancybox.getTranslate(d.$slide).width,p=n.fancybox.getTranslate(d.$slide).height,h=d.width,g=d.height;l.isAnimating||l.isMoved()||!u||"image"!=d.type||!d.isLoaded||d.hasError||(l.isAnimating=!0,n.fancybox.stop(u),t=void 0===t?.5*f:t,e=void 0===e?.5*p:e,i=n.fancybox.getTranslate(u),i.top-=n.fancybox.getTranslate(d.$slide).top,i.left-=n.fancybox.getTranslate(d.$slide).left,r=h/i.width,c=g/i.height,a=.5*f-.5*h,s=.5*p-.5*g,h>f&&(a=i.left*r-(t*r-t),a>0&&(a=0),a<f-h&&(a=f-h)),g>p&&(s=i.top*c-(e*c-e),s>0&&(s=0),s<p-g&&(s=p-g)),l.updateCursor(h,g),n.fancybox.animate(u,{top:s,left:a,scaleX:r,scaleY:c},o||366,function(){l.isAnimating=!1}),l.SlideShow&&l.SlideShow.isActive&&l.SlideShow.stop())},scaleToFit:function(t){var e,o=this,i=o.current,a=i.$content;o.isAnimating||o.isMoved()||!a||"image"!=i.type||!i.isLoaded||i.hasError||(o.isAnimating=!0,n.fancybox.stop(a),e=o.getFitPos(i),o.updateCursor(e.width,e.height),n.fancybox.animate(a,{top:e.top,left:e.left,scaleX:e.width/a.width(),scaleY:e.height/a.height()},t||366,function(){o.isAnimating=!1}))},getFitPos:function(t){var e,o,i,a,s=this,r=t.$content,c=t.$slide,l=t.width||t.opts.width,d=t.height||t.opts.height,u={};return!!(t.isLoaded&&r&&r.length)&&(e=n.fancybox.getTranslate(s.$refs.stage).width,o=n.fancybox.getTranslate(s.$refs.stage).height,e-=parseFloat(c.css("paddingLeft"))+parseFloat(c.css("paddingRight"))+parseFloat(r.css("marginLeft"))+parseFloat(r.css("marginRight")),o-=parseFloat(c.css("paddingTop"))+parseFloat(c.css("paddingBottom"))+parseFloat(r.css("marginTop"))+parseFloat(r.css("marginBottom")),l&&d||(l=e,d=o),i=Math.min(1,e/l,o/d),l*=i,d*=i,l>e-.5&&(l=e),d>o-.5&&(d=o),"image"===t.type?(u.top=Math.floor(.5*(o-d))+parseFloat(c.css("paddingTop")),u.left=Math.floor(.5*(e-l))+parseFloat(c.css("paddingLeft"))):"video"===t.contentType&&(a=t.opts.width&&t.opts.height?l/d:t.opts.ratio||16/9,d>l/a?d=l/a:l>d*a&&(l=d*a)),u.width=l,u.height=d,u)},update:function(t){var e=this;n.each(e.slides,function(n,o){e.updateSlide(o,t)})},updateSlide:function(t,e){var o=this,i=t&&t.$content,a=t.width||t.opts.width,s=t.height||t.opts.height,r=t.$slide;o.adjustCaption(t),i&&(a||s||"video"===t.contentType)&&!t.hasError&&(n.fancybox.stop(i),n.fancybox.setTranslate(i,o.getFitPos(t)),t.pos===o.currPos&&(o.isAnimating=!1,o.updateCursor())),o.adjustLayout(t),r.length&&(r.trigger("refresh"),t.pos===o.currPos&&o.$refs.toolbar.add(o.$refs.navigation.find(".fancybox-button--arrow_right")).toggleClass("compensate-for-scrollbar",r.get(0).scrollHeight>r.get(0).clientHeight)),o.trigger("onUpdate",t,e)},centerSlide:function(t){var e=this,o=e.current,i=o.$slide;!e.isClosing&&o&&(i.siblings().css({transform:"",opacity:""}),i.parent().children().removeClass("fancybox-slide--previous fancybox-slide--next"),n.fancybox.animate(i,{top:0,left:0,opacity:1},void 0===t?0:t,function(){i.css({transform:"",opacity:""}),o.isComplete||e.complete()},!1))},isMoved:function(t){var e,o,i=t||this.current;return!!i&&(o=n.fancybox.getTranslate(this.$refs.stage),e=n.fancybox.getTranslate(i.$slide),!i.$slide.hasClass("fancybox-animated")&&(Math.abs(e.top-o.top)>.5||Math.abs(e.left-o.left)>.5))},updateCursor:function(t,e){var o,i,a=this,s=a.current,r=a.$refs.container;s&&!a.isClosing&&a.Guestures&&(r.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-zoomOut fancybox-can-swipe fancybox-can-pan"),o=a.canPan(t,e),i=!!o||a.isZoomable(),r.toggleClass("fancybox-is-zoomable",i),n("[data-fancybox-zoom]").prop("disabled",!i),o?r.addClass("fancybox-can-pan"):i&&("zoom"===s.opts.clickContent||n.isFunction(s.opts.clickContent)&&"zoom"==s.opts.clickContent(s))?r.addClass("fancybox-can-zoomIn"):s.opts.touch&&(s.opts.touch.vertical||a.group.length>1)&&"video"!==s.contentType&&r.addClass("fancybox-can-swipe"))},isZoomable:function(){var t,e=this,n=e.current;if(n&&!e.isClosing&&"image"===n.type&&!n.hasError){if(!n.isLoaded)return!0;if((t=e.getFitPos(n))&&(n.width>t.width||n.height>t.height))return!0}return!1},isScaledDown:function(t,e){var o=this,i=!1,a=o.current,s=a.$content;return void 0!==t&&void 0!==e?i=t<a.width&&e<a.height:s&&(i=n.fancybox.getTranslate(s),i=i.width<a.width&&i.height<a.height),i},canPan:function(t,e){var o=this,i=o.current,a=null,s=!1;return"image"===i.type&&(i.isComplete||t&&e)&&!i.hasError&&(s=o.getFitPos(i),void 0!==t&&void 0!==e?a={width:t,height:e}:i.isComplete&&(a=n.fancybox.getTranslate(i.$content)),a&&s&&(s=Math.abs(a.width-s.width)>1.5||Math.abs(a.height-s.height)>1.5)),s},loadSlide:function(t){var e,o,i,a=this;if(!t.isLoading&&!t.isLoaded){if(t.isLoading=!0,!1===a.trigger("beforeLoad",t))return t.isLoading=!1,!1;switch(e=t.type,o=t.$slide,o.off("refresh").trigger("onReset").addClass(t.opts.slideClass),e){case"image":a.setImage(t);break;case"iframe":a.setIframe(t);break;case"html":a.setContent(t,t.src||t.content);break;case"video":a.setContent(t,t.opts.video.tpl.replace(/\{\{src\}\}/gi,t.src).replace("{{format}}",t.opts.videoFormat||t.opts.video.format||"").replace("{{poster}}",t.thumb||""));break;case"inline":n(t.src).length?a.setContent(t,n(t.src)):a.setError(t);break;case"ajax":a.showLoading(t),i=n.ajax(n.extend({},t.opts.ajax.settings,{url:t.src,success:function(e,n){"success"===n&&a.setContent(t,e)},error:function(e,n){e&&"abort"!==n&&a.setError(t)}})),o.one("onReset",function(){i.abort()});break;default:a.setError(t)}return!0}},setImage:function(t){var o,i=this;setTimeout(function(){var e=t.$image;i.isClosing||!t.isLoading||e&&e.length&&e[0].complete||t.hasError||i.showLoading(t)},50),i.checkSrcset(t),t.$content=n('<div class="fancybox-content"></div>').addClass("fancybox-is-hidden").appendTo(t.$slide.addClass("fancybox-slide--image")),!1!==t.opts.preload&&t.opts.width&&t.opts.height&&t.thumb&&(t.width=t.opts.width,t.height=t.opts.height,o=e.createElement("img"),o.onerror=function(){n(this).remove(),t.$ghost=null},o.onload=function(){i.afterLoad(t)},t.$ghost=n(o).addClass("fancybox-image").appendTo(t.$content).attr("src",t.thumb)),i.setBigImage(t)},checkSrcset:function(e){var n,o,i,a,s=e.opts.srcset||e.opts.image.srcset;if(s){i=t.devicePixelRatio||1,a=t.innerWidth*i,o=s.split(",").map(function(t){var e={};return t.trim().split(/\s+/).forEach(function(t,n){var o=parseInt(t.substring(0,t.length-1),10);if(0===n)return e.url=t;o&&(e.value=o,e.postfix=t[t.length-1])}),e}),o.sort(function(t,e){return t.value-e.value});for(var r=0;r<o.length;r++){var c=o[r];if("w"===c.postfix&&c.value>=a||"x"===c.postfix&&c.value>=i){n=c;break}}!n&&o.length&&(n=o[o.length-1]),n&&(e.src=n.url,e.width&&e.height&&"w"==n.postfix&&(e.height=e.width/e.height*n.value,e.width=n.value),e.opts.srcset=s)}},setBigImage:function(t){var o=this,i=e.createElement("img"),a=n(i);t.$image=a.one("error",function(){o.setError(t)}).one("load",function(){var e;t.$ghost||(o.resolveImageSlideSize(t,this.naturalWidth,this.naturalHeight),o.afterLoad(t)),o.isClosing||(t.opts.srcset&&(e=t.opts.sizes,e&&"auto"!==e||(e=(t.width/t.height>1&&s.width()/s.height()>1?"100":Math.round(t.width/t.height*100))+"vw"),a.attr("sizes",e).attr("srcset",t.opts.srcset)),t.$ghost&&setTimeout(function(){t.$ghost&&!o.isClosing&&t.$ghost.hide()},Math.min(300,Math.max(1e3,t.height/1600))),o.hideLoading(t))}).addClass("fancybox-image").attr("src",t.src).appendTo(t.$content),(i.complete||"complete"==i.readyState)&&a.naturalWidth&&a.naturalHeight?a.trigger("load"):i.error&&a.trigger("error")},resolveImageSlideSize:function(t,e,n){var o=parseInt(t.opts.width,10),i=parseInt(t.opts.height,10);t.width=e,t.height=n,o>0&&(t.width=o,t.height=Math.floor(o*n/e)),i>0&&(t.width=Math.floor(i*e/n),t.height=i)},setIframe:function(t){var e,o=this,i=t.opts.iframe,a=t.$slide;t.$content=n('<div class="fancybox-content'+(i.preload?" fancybox-is-hidden":"")+'"></div>').css(i.css).appendTo(a),a.addClass("fancybox-slide--"+t.contentType),t.$iframe=e=n(i.tpl.replace(/\{rnd\}/g,(new Date).getTime())).attr(i.attr).appendTo(t.$content),i.preload?(o.showLoading(t),e.on("load.fb error.fb",function(e){this.isReady=1,t.$slide.trigger("refresh"),o.afterLoad(t)}),a.on("refresh.fb",function(){var n,o,s=t.$content,r=i.css.width,c=i.css.height;if(1===e[0].isReady){try{n=e.contents(),o=n.find("body")}catch(t){}o&&o.length&&o.children().length&&(a.css("overflow","visible"),s.css({width:"100%","max-width":"100%",height:"9999px"}),void 0===r&&(r=Math.ceil(Math.max(o[0].clientWidth,o.outerWidth(!0)))),s.css("width",r||"").css("max-width",""),void 0===c&&(c=Math.ceil(Math.max(o[0].clientHeight,o.outerHeight(!0)))),s.css("height",c||""),a.css("overflow","auto")),s.removeClass("fancybox-is-hidden")}})):o.afterLoad(t),e.attr("src",t.src),a.one("onReset",function(){try{n(this).find("iframe").hide().unbind().attr("src","//about:blank")}catch(t){}n(this).off("refresh.fb").empty(),t.isLoaded=!1,t.isRevealed=!1})},setContent:function(t,e){var o=this;o.isClosing||(o.hideLoading(t),t.$content&&n.fancybox.stop(t.$content),t.$slide.empty(),l(e)&&e.parent().length?((e.hasClass("fancybox-content")||e.parent().hasClass("fancybox-content"))&&e.parents(".fancybox-slide").trigger("onReset"),t.$placeholder=n("<div>").hide().insertAfter(e),e.css("display","inline-block")):t.hasError||("string"===n.type(e)&&(e=n("<div>").append(n.trim(e)).contents()),t.opts.filter&&(e=n("<div>").html(e).find(t.opts.filter))),t.$slide.one("onReset",function(){n(this).find("video,audio").trigger("pause"),t.$placeholder&&(t.$placeholder.after(e.removeClass("fancybox-content").hide()).remove(),t.$placeholder=null),t.$smallBtn&&(t.$smallBtn.remove(),t.$smallBtn=null),t.hasError||(n(this).empty(),t.isLoaded=!1,t.isRevealed=!1)}),n(e).appendTo(t.$slide),n(e).is("video,audio")&&(n(e).addClass("fancybox-video"),n(e).wrap("<div></div>"),t.contentType="video",t.opts.width=t.opts.width||n(e).attr("width"),t.opts.height=t.opts.height||n(e).attr("height")),t.$content=t.$slide.children().filter("div,form,main,video,audio,article,.fancybox-content").first(),t.$content.siblings().hide(),t.$content.length||(t.$content=t.$slide.wrapInner("<div></div>").children().first()),t.$content.addClass("fancybox-content"),t.$slide.addClass("fancybox-slide--"+t.contentType),o.afterLoad(t))},setError:function(t){t.hasError=!0,t.$slide.trigger("onReset").removeClass("fancybox-slide--"+t.contentType).addClass("fancybox-slide--error"),t.contentType="html",this.setContent(t,this.translate(t,t.opts.errorTpl)),t.pos===this.currPos&&(this.isAnimating=!1)},showLoading:function(t){var e=this;(t=t||e.current)&&!t.$spinner&&(t.$spinner=n(e.translate(e,e.opts.spinnerTpl)).appendTo(t.$slide).hide().fadeIn("fast"))},hideLoading:function(t){var e=this;(t=t||e.current)&&t.$spinner&&(t.$spinner.stop().remove(),delete t.$spinner)},afterLoad:function(t){var e=this;e.isClosing||(t.isLoading=!1,t.isLoaded=!0,e.trigger("afterLoad",t),e.hideLoading(t),!t.opts.smallBtn||t.$smallBtn&&t.$smallBtn.length||(t.$smallBtn=n(e.translate(t,t.opts.btnTpl.smallBtn)).appendTo(t.$content)),t.opts.protect&&t.$content&&!t.hasError&&(t.$content.on("contextmenu.fb",function(t){return 2==t.button&&t.preventDefault(),!0}),"image"===t.type&&n('<div class="fancybox-spaceball"></div>').appendTo(t.$content)),e.adjustCaption(t),e.adjustLayout(t),t.pos===e.currPos&&e.updateCursor(),e.revealContent(t))},adjustCaption:function(t){var e,n=this,o=t||n.current,i=o.opts.caption,a=o.opts.preventCaptionOverlap,s=n.$refs.caption,r=!1;s.toggleClass("fancybox-caption--separate",a),a&&i&&i.length&&(o.pos!==n.currPos?(e=s.clone().appendTo(s.parent()),e.children().eq(0).empty().html(i),r=e.outerHeight(!0),e.empty().remove()):n.$caption&&(r=n.$caption.outerHeight(!0)),o.$slide.css("padding-bottom",r||""))},adjustLayout:function(t){var e,n,o,i,a=this,s=t||a.current;s.isLoaded&&!0!==s.opts.disableLayoutFix&&(s.$content.css("margin-bottom",""),s.$content.outerHeight()>s.$slide.height()+.5&&(o=s.$slide[0].style["padding-bottom"],i=s.$slide.css("padding-bottom"),parseFloat(i)>0&&(e=s.$slide[0].scrollHeight,s.$slide.css("padding-bottom",0),Math.abs(e-s.$slide[0].scrollHeight)<1&&(n=i),s.$slide.css("padding-bottom",o))),s.$content.css("margin-bottom",n))},revealContent:function(t){var e,o,i,a,s=this,r=t.$slide,c=!1,l=!1,d=s.isMoved(t),u=t.isRevealed;return t.isRevealed=!0,e=t.opts[s.firstRun?"animationEffect":"transitionEffect"],i=t.opts[s.firstRun?"animationDuration":"transitionDuration"],i=parseInt(void 0===t.forcedDuration?i:t.forcedDuration,10),!d&&t.pos===s.currPos&&i||(e=!1),"zoom"===e&&(t.pos===s.currPos&&i&&"image"===t.type&&!t.hasError&&(l=s.getThumbPos(t))?c=s.getFitPos(t):e="fade"),"zoom"===e?(s.isAnimating=!0,c.scaleX=c.width/l.width,c.scaleY=c.height/l.height,a=t.opts.zoomOpacity,"auto"==a&&(a=Math.abs(t.width/t.height-l.width/l.height)>.1),a&&(l.opacity=.1,c.opacity=1),n.fancybox.setTranslate(t.$content.removeClass("fancybox-is-hidden"),l),p(t.$content),void n.fancybox.animate(t.$content,c,i,function(){s.isAnimating=!1,s.complete()})):(s.updateSlide(t),e?(n.fancybox.stop(r),o="fancybox-slide--"+(t.pos>=s.prevPos?"next":"previous")+" fancybox-animated fancybox-fx-"+e,r.addClass(o).removeClass("fancybox-slide--current"),t.$content.removeClass("fancybox-is-hidden"),p(r),"image"!==t.type&&t.$content.hide().show(0),void n.fancybox.animate(r,"fancybox-slide--current",i,function(){r.removeClass(o).css({transform:"",opacity:""}),t.pos===s.currPos&&s.complete()},!0)):(t.$content.removeClass("fancybox-is-hidden"),u||!d||"image"!==t.type||t.hasError||t.$content.hide().fadeIn("fast"),void(t.pos===s.currPos&&s.complete())))},getThumbPos:function(t){var e,o,i,a,s,r=!1,c=t.$thumb;return!(!c||!g(c[0]))&&(e=n.fancybox.getTranslate(c),o=parseFloat(c.css("border-top-width")||0),i=parseFloat(c.css("border-right-width")||0),a=parseFloat(c.css("border-bottom-width")||0),s=parseFloat(c.css("border-left-width")||0),r={top:e.top+o,left:e.left+s,width:e.width-i-s,height:e.height-o-a,scaleX:1,scaleY:1},e.width>0&&e.height>0&&r)},complete:function(){var t,e=this,o=e.current,i={};!e.isMoved()&&o.isLoaded&&(o.isComplete||(o.isComplete=!0,o.$slide.siblings().trigger("onReset"),e.preload("inline"),p(o.$slide),o.$slide.addClass("fancybox-slide--complete"),n.each(e.slides,function(t,o){o.pos>=e.currPos-1&&o.pos<=e.currPos+1?i[o.pos]=o:o&&(n.fancybox.stop(o.$slide),o.$slide.off().remove())}),e.slides=i),e.isAnimating=!1,e.updateCursor(),e.trigger("afterShow"),o.opts.video.autoStart&&o.$slide.find("video,audio").filter(":visible:first").trigger("play").one("ended",function(){Document.exitFullscreen?Document.exitFullscreen():this.webkitExitFullscreen&&this.webkitExitFullscreen(),e.next()}),o.opts.autoFocus&&"html"===o.contentType&&(t=o.$content.find("input[autofocus]:enabled:visible:first"),t.length?t.trigger("focus"):e.focus(null,!0)),o.$slide.scrollTop(0).scrollLeft(0))},preload:function(t){var e,n,o=this;o.group.length<2||(n=o.slides[o.currPos+1],e=o.slides[o.currPos-1],e&&e.type===t&&o.loadSlide(e),n&&n.type===t&&o.loadSlide(n))},focus:function(t,o){var i,a,s=this,r=["a[href]","area[href]",'input:not([disabled]):not([type="hidden"]):not([aria-hidden])',"select:not([disabled]):not([aria-hidden])","textarea:not([disabled]):not([aria-hidden])","button:not([disabled]):not([aria-hidden])","iframe","object","embed","video","audio","[contenteditable]",'[tabindex]:not([tabindex^="-"])'].join(",");s.isClosing||(i=!t&&s.current&&s.current.isComplete?s.current.$slide.find("*:visible"+(o?":not(.fancybox-close-small)":"")):s.$refs.container.find("*:visible"),i=i.filter(r).filter(function(){return"hidden"!==n(this).css("visibility")&&!n(this).hasClass("disabled")}),i.length?(a=i.index(e.activeElement),t&&t.shiftKey?(a<0||0==a)&&(t.preventDefault(),i.eq(i.length-1).trigger("focus")):(a<0||a==i.length-1)&&(t&&t.preventDefault(),i.eq(0).trigger("focus"))):s.$refs.container.trigger("focus"))},activate:function(){var t=this;n(".fancybox-container").each(function(){var e=n(this).data("FancyBox");e&&e.id!==t.id&&!e.isClosing&&(e.trigger("onDeactivate"),e.removeEvents(),e.isVisible=!1)}),t.isVisible=!0,(t.current||t.isIdle)&&(t.update(),t.updateControls()),t.trigger("onActivate"),t.addEvents()},close:function(t,e){var o,i,a,s,r,c,l,u=this,f=u.current,h=function(){u.cleanUp(t)};return!u.isClosing&&(u.isClosing=!0,!1===u.trigger("beforeClose",t)?(u.isClosing=!1,d(function(){u.update()}),!1):(u.removeEvents(),a=f.$content,o=f.opts.animationEffect,i=n.isNumeric(e)?e:o?f.opts.animationDuration:0,f.$slide.removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated"),!0!==t?n.fancybox.stop(f.$slide):o=!1,f.$slide.siblings().trigger("onReset").remove(),i&&u.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing").css("transition-duration",i+"ms"),u.hideLoading(f),u.hideControls(!0),u.updateCursor(),"zoom"!==o||a&&i&&"image"===f.type&&!u.isMoved()&&!f.hasError&&(l=u.getThumbPos(f))||(o="fade"),"zoom"===o?(n.fancybox.stop(a),s=n.fancybox.getTranslate(a),c={top:s.top,left:s.left,scaleX:s.width/l.width,scaleY:s.height/l.height,width:l.width,height:l.height},r=f.opts.zoomOpacity,
"auto"==r&&(r=Math.abs(f.width/f.height-l.width/l.height)>.1),r&&(l.opacity=0),n.fancybox.setTranslate(a,c),p(a),n.fancybox.animate(a,l,i,h),!0):(o&&i?n.fancybox.animate(f.$slide.addClass("fancybox-slide--previous").removeClass("fancybox-slide--current"),"fancybox-animated fancybox-fx-"+o,i,h):!0===t?setTimeout(h,i):h(),!0)))},cleanUp:function(e){var o,i,a,s=this,r=s.current.opts.$orig;s.current.$slide.trigger("onReset"),s.$refs.container.empty().remove(),s.trigger("afterClose",e),s.current.opts.backFocus&&(r&&r.length&&r.is(":visible")||(r=s.$trigger),r&&r.length&&(i=t.scrollX,a=t.scrollY,r.trigger("focus"),n("html, body").scrollTop(a).scrollLeft(i))),s.current=null,o=n.fancybox.getInstance(),o?o.activate():(n("body").removeClass("fancybox-active compensate-for-scrollbar"),n("#fancybox-style-noscroll").remove())},trigger:function(t,e){var o,i=Array.prototype.slice.call(arguments,1),a=this,s=e&&e.opts?e:a.current;if(s?i.unshift(s):s=a,i.unshift(a),n.isFunction(s.opts[t])&&(o=s.opts[t].apply(s,i)),!1===o)return o;"afterClose"!==t&&a.$refs?a.$refs.container.trigger(t+".fb",i):r.trigger(t+".fb",i)},updateControls:function(){var t=this,o=t.current,i=o.index,a=t.$refs.container,s=t.$refs.caption,r=o.opts.caption;o.$slide.trigger("refresh"),r&&r.length?(t.$caption=s,s.children().eq(0).html(r)):t.$caption=null,t.hasHiddenControls||t.isIdle||t.showControls(),a.find("[data-fancybox-count]").html(t.group.length),a.find("[data-fancybox-index]").html(i+1),a.find("[data-fancybox-prev]").prop("disabled",!o.opts.loop&&i<=0),a.find("[data-fancybox-next]").prop("disabled",!o.opts.loop&&i>=t.group.length-1),"image"===o.type?a.find("[data-fancybox-zoom]").show().end().find("[data-fancybox-download]").attr("href",o.opts.image.src||o.src).show():o.opts.toolbar&&a.find("[data-fancybox-download],[data-fancybox-zoom]").hide(),n(e.activeElement).is(":hidden,[disabled]")&&t.$refs.container.trigger("focus")},hideControls:function(t){var e=this,n=["infobar","toolbar","nav"];!t&&e.current.opts.preventCaptionOverlap||n.push("caption"),this.$refs.container.removeClass(n.map(function(t){return"fancybox-show-"+t}).join(" ")),this.hasHiddenControls=!0},showControls:function(){var t=this,e=t.current?t.current.opts:t.opts,n=t.$refs.container;t.hasHiddenControls=!1,t.idleSecondsCounter=0,n.toggleClass("fancybox-show-toolbar",!(!e.toolbar||!e.buttons)).toggleClass("fancybox-show-infobar",!!(e.infobar&&t.group.length>1)).toggleClass("fancybox-show-caption",!!t.$caption).toggleClass("fancybox-show-nav",!!(e.arrows&&t.group.length>1)).toggleClass("fancybox-is-modal",!!e.modal)},toggleControls:function(){this.hasHiddenControls?this.showControls():this.hideControls()}}),n.fancybox={version:"3.5.7",defaults:a,getInstance:function(t){var e=n('.fancybox-container:not(".fancybox-is-closing"):last').data("FancyBox"),o=Array.prototype.slice.call(arguments,1);return e instanceof b&&("string"===n.type(t)?e[t].apply(e,o):"function"===n.type(t)&&t.apply(e,o),e)},open:function(t,e,n){return new b(t,e,n)},close:function(t){var e=this.getInstance();e&&(e.close(),!0===t&&this.close(t))},destroy:function(){this.close(!0),r.add("body").off("click.fb-start","**")},isMobile:/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),use3d:function(){var n=e.createElement("div");return t.getComputedStyle&&t.getComputedStyle(n)&&t.getComputedStyle(n).getPropertyValue("transform")&&!(e.documentMode&&e.documentMode<11)}(),getTranslate:function(t){var e;return!(!t||!t.length)&&(e=t[0].getBoundingClientRect(),{top:e.top||0,left:e.left||0,width:e.width,height:e.height,opacity:parseFloat(t.css("opacity"))})},setTranslate:function(t,e){var n="",o={};if(t&&e)return void 0===e.left&&void 0===e.top||(n=(void 0===e.left?t.position().left:e.left)+"px, "+(void 0===e.top?t.position().top:e.top)+"px",n=this.use3d?"translate3d("+n+", 0px)":"translate("+n+")"),void 0!==e.scaleX&&void 0!==e.scaleY?n+=" scale("+e.scaleX+", "+e.scaleY+")":void 0!==e.scaleX&&(n+=" scaleX("+e.scaleX+")"),n.length&&(o.transform=n),void 0!==e.opacity&&(o.opacity=e.opacity),void 0!==e.width&&(o.width=e.width),void 0!==e.height&&(o.height=e.height),t.css(o)},animate:function(t,e,o,i,a){var s,r=this;n.isFunction(o)&&(i=o,o=null),r.stop(t),s=r.getTranslate(t),t.on(f,function(c){(!c||!c.originalEvent||t.is(c.originalEvent.target)&&"z-index"!=c.originalEvent.propertyName)&&(r.stop(t),n.isNumeric(o)&&t.css("transition-duration",""),n.isPlainObject(e)?void 0!==e.scaleX&&void 0!==e.scaleY&&r.setTranslate(t,{top:e.top,left:e.left,width:s.width*e.scaleX,height:s.height*e.scaleY,scaleX:1,scaleY:1}):!0!==a&&t.removeClass(e),n.isFunction(i)&&i(c))}),n.isNumeric(o)&&t.css("transition-duration",o+"ms"),n.isPlainObject(e)?(void 0!==e.scaleX&&void 0!==e.scaleY&&(delete e.width,delete e.height,t.parent().hasClass("fancybox-slide--image")&&t.parent().addClass("fancybox-is-scaling")),n.fancybox.setTranslate(t,e)):t.addClass(e),t.data("timer",setTimeout(function(){t.trigger(f)},o+33))},stop:function(t,e){t&&t.length&&(clearTimeout(t.data("timer")),e&&t.trigger(f),t.off(f).css("transition-duration",""),t.parent().removeClass("fancybox-is-scaling"))}},n.fn.fancybox=function(t){var e;return t=t||{},e=t.selector||!1,e?n("body").off("click.fb-start",e).on("click.fb-start",e,{options:t},i):this.off("click.fb-start").on("click.fb-start",{items:this,options:t},i),this},r.on("click.fb-start","[data-fancybox]",i),r.on("click.fb-start","[data-fancybox-trigger]",function(t){n('[data-fancybox="'+n(this).attr("data-fancybox-trigger")+'"]').eq(n(this).attr("data-fancybox-index")||0).trigger("click.fb-start",{$trigger:n(this)})}),function(){var t=null;r.on("mousedown mouseup focus blur",".fancybox-button",function(e){switch(e.type){case"mousedown":t=n(this);break;case"mouseup":t=null;break;case"focusin":n(".fancybox-button").removeClass("fancybox-focus"),n(this).is(t)||n(this).is("[disabled]")||n(this).addClass("fancybox-focus");break;case"focusout":n(".fancybox-button").removeClass("fancybox-focus")}})}()}}(window,document,jQuery),function(t){"use strict";var e={youtube:{matcher:/(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,params:{autoplay:1,autohide:1,fs:1,rel:0,hd:1,wmode:"transparent",enablejsapi:1,html5:1},paramPlace:8,type:"iframe",url:"https://www.youtube-nocookie.com/embed/$4",thumb:"https://img.youtube.com/vi/$4/hqdefault.jpg"},vimeo:{matcher:/^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,params:{autoplay:1,hd:1,show_title:1,show_byline:1,show_portrait:0,fullscreen:1},paramPlace:3,type:"iframe",url:"//player.vimeo.com/video/$2"},instagram:{matcher:/(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,type:"image",url:"//$1/p/$2/media/?size=l"},gmap_place:{matcher:/(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,type:"iframe",url:function(t){return"//maps.google."+t[2]+"/?ll="+(t[9]?t[9]+"&z="+Math.floor(t[10])+(t[12]?t[12].replace(/^\//,"&"):""):t[12]+"").replace(/\?/,"&")+"&output="+(t[12]&&t[12].indexOf("layer=c")>0?"svembed":"embed")}},gmap_search:{matcher:/(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(maps\/search\/)(.*)/i,type:"iframe",url:function(t){return"//maps.google."+t[2]+"/maps?q="+t[5].replace("query=","q=").replace("api=1","")+"&output=embed"}}},n=function(e,n,o){if(e)return o=o||"","object"===t.type(o)&&(o=t.param(o,!0)),t.each(n,function(t,n){e=e.replace("$"+t,n||"")}),o.length&&(e+=(e.indexOf("?")>0?"&":"?")+o),e};t(document).on("objectNeedsType.fb",function(o,i,a){var s,r,c,l,d,u,f,p=a.src||"",h=!1;s=t.extend(!0,{},e,a.opts.media),t.each(s,function(e,o){if(c=p.match(o.matcher)){if(h=o.type,f=e,u={},o.paramPlace&&c[o.paramPlace]){d=c[o.paramPlace],"?"==d[0]&&(d=d.substring(1)),d=d.split("&");for(var i=0;i<d.length;++i){var s=d[i].split("=",2);2==s.length&&(u[s[0]]=decodeURIComponent(s[1].replace(/\+/g," ")))}}return l=t.extend(!0,{},o.params,a.opts[e],u),p="function"===t.type(o.url)?o.url.call(this,c,l,a):n(o.url,c,l),r="function"===t.type(o.thumb)?o.thumb.call(this,c,l,a):n(o.thumb,c),"youtube"===e?p=p.replace(/&t=((\d+)m)?(\d+)s/,function(t,e,n,o){return"&start="+((n?60*parseInt(n,10):0)+parseInt(o,10))}):"vimeo"===e&&(p=p.replace("&%23","#")),!1}}),h?(a.opts.thumb||a.opts.$thumb&&a.opts.$thumb.length||(a.opts.thumb=r),"iframe"===h&&(a.opts=t.extend(!0,a.opts,{iframe:{preload:!1,attr:{scrolling:"no"}}})),t.extend(a,{type:h,src:p,origSrc:a.src,contentSource:f,contentType:"image"===h?"image":"gmap_place"==f||"gmap_search"==f?"map":"video"})):p&&(a.type=a.opts.defaultType)});var o={youtube:{src:"https://www.youtube.com/iframe_api",class:"YT",loading:!1,loaded:!1},vimeo:{src:"https://player.vimeo.com/api/player.js",class:"Vimeo",loading:!1,loaded:!1},load:function(t){var e,n=this;if(this[t].loaded)return void setTimeout(function(){n.done(t)});this[t].loading||(this[t].loading=!0,e=document.createElement("script"),e.type="text/javascript",e.src=this[t].src,"youtube"===t?window.onYouTubeIframeAPIReady=function(){n[t].loaded=!0,n.done(t)}:e.onload=function(){n[t].loaded=!0,n.done(t)},document.body.appendChild(e))},done:function(e){var n,o,i;"youtube"===e&&delete window.onYouTubeIframeAPIReady,(n=t.fancybox.getInstance())&&(o=n.current.$content.find("iframe"),"youtube"===e&&void 0!==YT&&YT?i=new YT.Player(o.attr("id"),{events:{onStateChange:function(t){0==t.data&&n.next()}}}):"vimeo"===e&&void 0!==Vimeo&&Vimeo&&(i=new Vimeo.Player(o),i.on("ended",function(){n.next()})))}};t(document).on({"afterShow.fb":function(t,e,n){e.group.length>1&&("youtube"===n.contentSource||"vimeo"===n.contentSource)&&o.load(n.contentSource)}})}(jQuery),function(t,e,n){"use strict";var o=function(){return t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||t.oRequestAnimationFrame||function(e){return t.setTimeout(e,1e3/60)}}(),i=function(){return t.cancelAnimationFrame||t.webkitCancelAnimationFrame||t.mozCancelAnimationFrame||t.oCancelAnimationFrame||function(e){t.clearTimeout(e)}}(),a=function(e){var n=[];e=e.originalEvent||e||t.e,e=e.touches&&e.touches.length?e.touches:e.changedTouches&&e.changedTouches.length?e.changedTouches:[e];for(var o in e)e[o].pageX?n.push({x:e[o].pageX,y:e[o].pageY}):e[o].clientX&&n.push({x:e[o].clientX,y:e[o].clientY});return n},s=function(t,e,n){return e&&t?"x"===n?t.x-e.x:"y"===n?t.y-e.y:Math.sqrt(Math.pow(t.x-e.x,2)+Math.pow(t.y-e.y,2)):0},r=function(t){if(t.is('a,area,button,[role="button"],input,label,select,summary,textarea,video,audio,iframe')||n.isFunction(t.get(0).onclick)||t.data("selectable"))return!0;for(var e=0,o=t[0].attributes,i=o.length;e<i;e++)if("data-fancybox-"===o[e].nodeName.substr(0,14))return!0;return!1},c=function(e){var n=t.getComputedStyle(e)["overflow-y"],o=t.getComputedStyle(e)["overflow-x"],i=("scroll"===n||"auto"===n)&&e.scrollHeight>e.clientHeight,a=("scroll"===o||"auto"===o)&&e.scrollWidth>e.clientWidth;return i||a},l=function(t){for(var e=!1;;){if(e=c(t.get(0)))break;if(t=t.parent(),!t.length||t.hasClass("fancybox-stage")||t.is("body"))break}return e},d=function(t){var e=this;e.instance=t,e.$bg=t.$refs.bg,e.$stage=t.$refs.stage,e.$container=t.$refs.container,e.destroy(),e.$container.on("touchstart.fb.touch mousedown.fb.touch",n.proxy(e,"ontouchstart"))};d.prototype.destroy=function(){var t=this;t.$container.off(".fb.touch"),n(e).off(".fb.touch"),t.requestId&&(i(t.requestId),t.requestId=null),t.tapped&&(clearTimeout(t.tapped),t.tapped=null)},d.prototype.ontouchstart=function(o){var i=this,c=n(o.target),d=i.instance,u=d.current,f=u.$slide,p=u.$content,h="touchstart"==o.type;if(h&&i.$container.off("mousedown.fb.touch"),(!o.originalEvent||2!=o.originalEvent.button)&&f.length&&c.length&&!r(c)&&!r(c.parent())&&(c.is("img")||!(o.originalEvent.clientX>c[0].clientWidth+c.offset().left))){if(!u||d.isAnimating||u.$slide.hasClass("fancybox-animated"))return o.stopPropagation(),void o.preventDefault();i.realPoints=i.startPoints=a(o),i.startPoints.length&&(u.touch&&o.stopPropagation(),i.startEvent=o,i.canTap=!0,i.$target=c,i.$content=p,i.opts=u.opts.touch,i.isPanning=!1,i.isSwiping=!1,i.isZooming=!1,i.isScrolling=!1,i.canPan=d.canPan(),i.startTime=(new Date).getTime(),i.distanceX=i.distanceY=i.distance=0,i.canvasWidth=Math.round(f[0].clientWidth),i.canvasHeight=Math.round(f[0].clientHeight),i.contentLastPos=null,i.contentStartPos=n.fancybox.getTranslate(i.$content)||{top:0,left:0},i.sliderStartPos=n.fancybox.getTranslate(f),i.stagePos=n.fancybox.getTranslate(d.$refs.stage),i.sliderStartPos.top-=i.stagePos.top,i.sliderStartPos.left-=i.stagePos.left,i.contentStartPos.top-=i.stagePos.top,i.contentStartPos.left-=i.stagePos.left,n(e).off(".fb.touch").on(h?"touchend.fb.touch touchcancel.fb.touch":"mouseup.fb.touch mouseleave.fb.touch",n.proxy(i,"ontouchend")).on(h?"touchmove.fb.touch":"mousemove.fb.touch",n.proxy(i,"ontouchmove")),n.fancybox.isMobile&&e.addEventListener("scroll",i.onscroll,!0),((i.opts||i.canPan)&&(c.is(i.$stage)||i.$stage.find(c).length)||(c.is(".fancybox-image")&&o.preventDefault(),n.fancybox.isMobile&&c.parents(".fancybox-caption").length))&&(i.isScrollable=l(c)||l(c.parent()),n.fancybox.isMobile&&i.isScrollable||o.preventDefault(),(1===i.startPoints.length||u.hasError)&&(i.canPan?(n.fancybox.stop(i.$content),i.isPanning=!0):i.isSwiping=!0,i.$container.addClass("fancybox-is-grabbing")),2===i.startPoints.length&&"image"===u.type&&(u.isLoaded||u.$ghost)&&(i.canTap=!1,i.isSwiping=!1,i.isPanning=!1,i.isZooming=!0,n.fancybox.stop(i.$content),i.centerPointStartX=.5*(i.startPoints[0].x+i.startPoints[1].x)-n(t).scrollLeft(),i.centerPointStartY=.5*(i.startPoints[0].y+i.startPoints[1].y)-n(t).scrollTop(),i.percentageOfImageAtPinchPointX=(i.centerPointStartX-i.contentStartPos.left)/i.contentStartPos.width,i.percentageOfImageAtPinchPointY=(i.centerPointStartY-i.contentStartPos.top)/i.contentStartPos.height,i.startDistanceBetweenFingers=s(i.startPoints[0],i.startPoints[1]))))}},d.prototype.onscroll=function(t){var n=this;n.isScrolling=!0,e.removeEventListener("scroll",n.onscroll,!0)},d.prototype.ontouchmove=function(t){var e=this;return void 0!==t.originalEvent.buttons&&0===t.originalEvent.buttons?void e.ontouchend(t):e.isScrolling?void(e.canTap=!1):(e.newPoints=a(t),void((e.opts||e.canPan)&&e.newPoints.length&&e.newPoints.length&&(e.isSwiping&&!0===e.isSwiping||t.preventDefault(),e.distanceX=s(e.newPoints[0],e.startPoints[0],"x"),e.distanceY=s(e.newPoints[0],e.startPoints[0],"y"),e.distance=s(e.newPoints[0],e.startPoints[0]),e.distance>0&&(e.isSwiping?e.onSwipe(t):e.isPanning?e.onPan():e.isZooming&&e.onZoom()))))},d.prototype.onSwipe=function(e){var a,s=this,r=s.instance,c=s.isSwiping,l=s.sliderStartPos.left||0;if(!0!==c)"x"==c&&(s.distanceX>0&&(s.instance.group.length<2||0===s.instance.current.index&&!s.instance.current.opts.loop)?l+=Math.pow(s.distanceX,.8):s.distanceX<0&&(s.instance.group.length<2||s.instance.current.index===s.instance.group.length-1&&!s.instance.current.opts.loop)?l-=Math.pow(-s.distanceX,.8):l+=s.distanceX),s.sliderLastPos={top:"x"==c?0:s.sliderStartPos.top+s.distanceY,left:l},s.requestId&&(i(s.requestId),s.requestId=null),s.requestId=o(function(){s.sliderLastPos&&(n.each(s.instance.slides,function(t,e){var o=e.pos-s.instance.currPos;n.fancybox.setTranslate(e.$slide,{top:s.sliderLastPos.top,left:s.sliderLastPos.left+o*s.canvasWidth+o*e.opts.gutter})}),s.$container.addClass("fancybox-is-sliding"))});else if(Math.abs(s.distance)>10){if(s.canTap=!1,r.group.length<2&&s.opts.vertical?s.isSwiping="y":r.isDragging||!1===s.opts.vertical||"auto"===s.opts.vertical&&n(t).width()>800?s.isSwiping="x":(a=Math.abs(180*Math.atan2(s.distanceY,s.distanceX)/Math.PI),s.isSwiping=a>45&&a<135?"y":"x"),"y"===s.isSwiping&&n.fancybox.isMobile&&s.isScrollable)return void(s.isScrolling=!0);r.isDragging=s.isSwiping,s.startPoints=s.newPoints,n.each(r.slides,function(t,e){var o,i;n.fancybox.stop(e.$slide),o=n.fancybox.getTranslate(e.$slide),i=n.fancybox.getTranslate(r.$refs.stage),e.$slide.css({transform:"",opacity:"","transition-duration":""}).removeClass("fancybox-animated").removeClass(function(t,e){return(e.match(/(^|\s)fancybox-fx-\S+/g)||[]).join(" ")}),e.pos===r.current.pos&&(s.sliderStartPos.top=o.top-i.top,s.sliderStartPos.left=o.left-i.left),n.fancybox.setTranslate(e.$slide,{top:o.top-i.top,left:o.left-i.left})}),r.SlideShow&&r.SlideShow.isActive&&r.SlideShow.stop()}},d.prototype.onPan=function(){var t=this;if(s(t.newPoints[0],t.realPoints[0])<(n.fancybox.isMobile?10:5))return void(t.startPoints=t.newPoints);t.canTap=!1,t.contentLastPos=t.limitMovement(),t.requestId&&i(t.requestId),t.requestId=o(function(){n.fancybox.setTranslate(t.$content,t.contentLastPos)})},d.prototype.limitMovement=function(){var t,e,n,o,i,a,s=this,r=s.canvasWidth,c=s.canvasHeight,l=s.distanceX,d=s.distanceY,u=s.contentStartPos,f=u.left,p=u.top,h=u.width,g=u.height;return i=h>r?f+l:f,a=p+d,t=Math.max(0,.5*r-.5*h),e=Math.max(0,.5*c-.5*g),n=Math.min(r-h,.5*r-.5*h),o=Math.min(c-g,.5*c-.5*g),l>0&&i>t&&(i=t-1+Math.pow(-t+f+l,.8)||0),l<0&&i<n&&(i=n+1-Math.pow(n-f-l,.8)||0),d>0&&a>e&&(a=e-1+Math.pow(-e+p+d,.8)||0),d<0&&a<o&&(a=o+1-Math.pow(o-p-d,.8)||0),{top:a,left:i}},d.prototype.limitPosition=function(t,e,n,o){var i=this,a=i.canvasWidth,s=i.canvasHeight;return n>a?(t=t>0?0:t,t=t<a-n?a-n:t):t=Math.max(0,a/2-n/2),o>s?(e=e>0?0:e,e=e<s-o?s-o:e):e=Math.max(0,s/2-o/2),{top:e,left:t}},d.prototype.onZoom=function(){var e=this,a=e.contentStartPos,r=a.width,c=a.height,l=a.left,d=a.top,u=s(e.newPoints[0],e.newPoints[1]),f=u/e.startDistanceBetweenFingers,p=Math.floor(r*f),h=Math.floor(c*f),g=(r-p)*e.percentageOfImageAtPinchPointX,b=(c-h)*e.percentageOfImageAtPinchPointY,m=(e.newPoints[0].x+e.newPoints[1].x)/2-n(t).scrollLeft(),v=(e.newPoints[0].y+e.newPoints[1].y)/2-n(t).scrollTop(),y=m-e.centerPointStartX,x=v-e.centerPointStartY,w=l+(g+y),$=d+(b+x),S={top:$,left:w,scaleX:f,scaleY:f};e.canTap=!1,e.newWidth=p,e.newHeight=h,e.contentLastPos=S,e.requestId&&i(e.requestId),e.requestId=o(function(){n.fancybox.setTranslate(e.$content,e.contentLastPos)})},d.prototype.ontouchend=function(t){var o=this,s=o.isSwiping,r=o.isPanning,c=o.isZooming,l=o.isScrolling;if(o.endPoints=a(t),o.dMs=Math.max((new Date).getTime()-o.startTime,1),o.$container.removeClass("fancybox-is-grabbing"),n(e).off(".fb.touch"),e.removeEventListener("scroll",o.onscroll,!0),o.requestId&&(i(o.requestId),o.requestId=null),o.isSwiping=!1,o.isPanning=!1,o.isZooming=!1,o.isScrolling=!1,o.instance.isDragging=!1,o.canTap)return o.onTap(t);o.speed=100,o.velocityX=o.distanceX/o.dMs*.5,o.velocityY=o.distanceY/o.dMs*.5,r?o.endPanning():c?o.endZooming():o.endSwiping(s,l)},d.prototype.endSwiping=function(t,e){var o=this,i=!1,a=o.instance.group.length,s=Math.abs(o.distanceX),r="x"==t&&a>1&&(o.dMs>130&&s>10||s>50);o.sliderLastPos=null,"y"==t&&!e&&Math.abs(o.distanceY)>50?(n.fancybox.animate(o.instance.current.$slide,{top:o.sliderStartPos.top+o.distanceY+150*o.velocityY,opacity:0},200),i=o.instance.close(!0,250)):r&&o.distanceX>0?i=o.instance.previous(300):r&&o.distanceX<0&&(i=o.instance.next(300)),!1!==i||"x"!=t&&"y"!=t||o.instance.centerSlide(200),o.$container.removeClass("fancybox-is-sliding")},d.prototype.endPanning=function(){var t,e,o,i=this;i.contentLastPos&&(!1===i.opts.momentum||i.dMs>350?(t=i.contentLastPos.left,e=i.contentLastPos.top):(t=i.contentLastPos.left+500*i.velocityX,e=i.contentLastPos.top+500*i.velocityY),o=i.limitPosition(t,e,i.contentStartPos.width,i.contentStartPos.height),o.width=i.contentStartPos.width,o.height=i.contentStartPos.height,n.fancybox.animate(i.$content,o,366))},d.prototype.endZooming=function(){var t,e,o,i,a=this,s=a.instance.current,r=a.newWidth,c=a.newHeight;a.contentLastPos&&(t=a.contentLastPos.left,e=a.contentLastPos.top,i={top:e,left:t,width:r,height:c,scaleX:1,scaleY:1},n.fancybox.setTranslate(a.$content,i),r<a.canvasWidth&&c<a.canvasHeight?a.instance.scaleToFit(150):r>s.width||c>s.height?a.instance.scaleToActual(a.centerPointStartX,a.centerPointStartY,150):(o=a.limitPosition(t,e,r,c),n.fancybox.animate(a.$content,o,150)))},d.prototype.onTap=function(e){var o,i=this,s=n(e.target),r=i.instance,c=r.current,l=e&&a(e)||i.startPoints,d=l[0]?l[0].x-n(t).scrollLeft()-i.stagePos.left:0,u=l[0]?l[0].y-n(t).scrollTop()-i.stagePos.top:0,f=function(t){var o=c.opts[t];if(n.isFunction(o)&&(o=o.apply(r,[c,e])),o)switch(o){case"close":r.close(i.startEvent);break;case"toggleControls":r.toggleControls();break;case"next":r.next();break;case"nextOrClose":r.group.length>1?r.next():r.close(i.startEvent);break;case"zoom":"image"==c.type&&(c.isLoaded||c.$ghost)&&(r.canPan()?r.scaleToFit():r.isScaledDown()?r.scaleToActual(d,u):r.group.length<2&&r.close(i.startEvent))}};if((!e.originalEvent||2!=e.originalEvent.button)&&(s.is("img")||!(d>s[0].clientWidth+s.offset().left))){if(s.is(".fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container"))o="Outside";else if(s.is(".fancybox-slide"))o="Slide";else{if(!r.current.$content||!r.current.$content.find(s).addBack().filter(s).length)return;o="Content"}if(i.tapped){if(clearTimeout(i.tapped),i.tapped=null,Math.abs(d-i.tapX)>50||Math.abs(u-i.tapY)>50)return this;f("dblclick"+o)}else i.tapX=d,i.tapY=u,c.opts["dblclick"+o]&&c.opts["dblclick"+o]!==c.opts["click"+o]?i.tapped=setTimeout(function(){i.tapped=null,r.isAnimating||f("click"+o)},500):f("click"+o);return this}},n(e).on("onActivate.fb",function(t,e){e&&!e.Guestures&&(e.Guestures=new d(e))}).on("beforeClose.fb",function(t,e){e&&e.Guestures&&e.Guestures.destroy()})}(window,document,jQuery),function(t,e){"use strict";e.extend(!0,e.fancybox.defaults,{btnTpl:{slideShow:'<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.5 5.4v13.2l11-6.6z"/></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M8.33 5.75h2.2v12.5h-2.2V5.75zm5.15 0h2.2v12.5h-2.2V5.75z"/></svg></button>'},slideShow:{autoStart:!1,speed:3e3,progress:!0}});var n=function(t){this.instance=t,this.init()};e.extend(n.prototype,{timer:null,isActive:!1,$button:null,init:function(){var t=this,n=t.instance,o=n.group[n.currIndex].opts.slideShow;t.$button=n.$refs.toolbar.find("[data-fancybox-play]").on("click",function(){t.toggle()}),n.group.length<2||!o?t.$button.hide():o.progress&&(t.$progress=e('<div class="fancybox-progress"></div>').appendTo(n.$refs.inner))},set:function(t){var n=this,o=n.instance,i=o.current;i&&(!0===t||i.opts.loop||o.currIndex<o.group.length-1)?n.isActive&&"video"!==i.contentType&&(n.$progress&&e.fancybox.animate(n.$progress.show(),{scaleX:1},i.opts.slideShow.speed),n.timer=setTimeout(function(){o.current.opts.loop||o.current.index!=o.group.length-1?o.next():o.jumpTo(0)},i.opts.slideShow.speed)):(n.stop(),o.idleSecondsCounter=0,o.showControls())},clear:function(){var t=this;clearTimeout(t.timer),t.timer=null,t.$progress&&t.$progress.removeAttr("style").hide()},start:function(){var t=this,e=t.instance.current;e&&(t.$button.attr("title",(e.opts.i18n[e.opts.lang]||e.opts.i18n.en).PLAY_STOP).removeClass("fancybox-button--play").addClass("fancybox-button--pause"),t.isActive=!0,e.isComplete&&t.set(!0),t.instance.trigger("onSlideShowChange",!0))},stop:function(){var t=this,e=t.instance.current;t.clear(),t.$button.attr("title",(e.opts.i18n[e.opts.lang]||e.opts.i18n.en).PLAY_START).removeClass("fancybox-button--pause").addClass("fancybox-button--play"),t.isActive=!1,t.instance.trigger("onSlideShowChange",!1),t.$progress&&t.$progress.removeAttr("style").hide()},toggle:function(){var t=this;t.isActive?t.stop():t.start()}}),e(t).on({"onInit.fb":function(t,e){e&&!e.SlideShow&&(e.SlideShow=new n(e))},"beforeShow.fb":function(t,e,n,o){var i=e&&e.SlideShow;o?i&&n.opts.slideShow.autoStart&&i.start():i&&i.isActive&&i.clear()},"afterShow.fb":function(t,e,n){var o=e&&e.SlideShow;o&&o.isActive&&o.set()},"afterKeydown.fb":function(n,o,i,a,s){var r=o&&o.SlideShow;!r||!i.opts.slideShow||80!==s&&32!==s||e(t.activeElement).is("button,a,input")||(a.preventDefault(),r.toggle())},"beforeClose.fb onDeactivate.fb":function(t,e){var n=e&&e.SlideShow;n&&n.stop()}}),e(t).on("visibilitychange",function(){var n=e.fancybox.getInstance(),o=n&&n.SlideShow;o&&o.isActive&&(t.hidden?o.clear():o.set())})}(document,jQuery),function(t,e){"use strict";var n=function(){for(var e=[["requestFullscreen","exitFullscreen","fullscreenElement","fullscreenEnabled","fullscreenchange","fullscreenerror"],["webkitRequestFullscreen","webkitExitFullscreen","webkitFullscreenElement","webkitFullscreenEnabled","webkitfullscreenchange","webkitfullscreenerror"],["webkitRequestFullScreen","webkitCancelFullScreen","webkitCurrentFullScreenElement","webkitCancelFullScreen","webkitfullscreenchange","webkitfullscreenerror"],["mozRequestFullScreen","mozCancelFullScreen","mozFullScreenElement","mozFullScreenEnabled","mozfullscreenchange","mozfullscreenerror"],["msRequestFullscreen","msExitFullscreen","msFullscreenElement","msFullscreenEnabled","MSFullscreenChange","MSFullscreenError"]],n={},o=0;o<e.length;o++){var i=e[o];if(i&&i[1]in t){for(var a=0;a<i.length;a++)n[e[0][a]]=i[a];return n}}return!1}();if(n){var o={request:function(e){e=e||t.documentElement,e[n.requestFullscreen](e.ALLOW_KEYBOARD_INPUT)},exit:function(){t[n.exitFullscreen]()},toggle:function(e){e=e||t.documentElement,this.isFullscreen()?this.exit():this.request(e)},isFullscreen:function(){return Boolean(t[n.fullscreenElement])},enabled:function(){return Boolean(t[n.fullscreenEnabled])}};e.extend(!0,e.fancybox.defaults,{btnTpl:{fullScreen:'<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fsenter" title="{{FULL_SCREEN}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z"/></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 16h3v3h2v-5H5zm3-8H5v2h5V5H8zm6 11h2v-3h3v-2h-5zm2-11V5h-2v5h5V8z"/></svg></button>'},fullScreen:{autoStart:!1}}),e(t).on(n.fullscreenchange,function(){var t=o.isFullscreen(),n=e.fancybox.getInstance();n&&(n.current&&"image"===n.current.type&&n.isAnimating&&(n.isAnimating=!1,n.update(!0,!0,0),n.isComplete||n.complete()),n.trigger("onFullscreenChange",t),n.$refs.container.toggleClass("fancybox-is-fullscreen",t),n.$refs.toolbar.find("[data-fancybox-fullscreen]").toggleClass("fancybox-button--fsenter",!t).toggleClass("fancybox-button--fsexit",t))})}e(t).on({"onInit.fb":function(t,e){var i;if(!n)return void e.$refs.toolbar.find("[data-fancybox-fullscreen]").remove();e&&e.group[e.currIndex].opts.fullScreen?(i=e.$refs.container,i.on("click.fb-fullscreen","[data-fancybox-fullscreen]",function(t){t.stopPropagation(),t.preventDefault(),o.toggle()}),e.opts.fullScreen&&!0===e.opts.fullScreen.autoStart&&o.request(),e.FullScreen=o):e&&e.$refs.toolbar.find("[data-fancybox-fullscreen]").hide()},"afterKeydown.fb":function(t,e,n,o,i){e&&e.FullScreen&&70===i&&(o.preventDefault(),e.FullScreen.toggle())},"beforeClose.fb":function(t,e){e&&e.FullScreen&&e.$refs.container.hasClass("fancybox-is-fullscreen")&&o.exit()}})}(document,jQuery),function(t,e){"use strict";var n="fancybox-thumbs";e.fancybox.defaults=e.extend(!0,{btnTpl:{thumbs:'<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M14.59 14.59h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76H5.65V5.65z"/></svg></button>'},thumbs:{autoStart:!1,hideOnClose:!0,parentEl:".fancybox-container",axis:"y"}},e.fancybox.defaults);var o=function(t){this.init(t)};e.extend(o.prototype,{$button:null,$grid:null,$list:null,isVisible:!1,isActive:!1,init:function(t){var e=this,n=t.group,o=0;e.instance=t,e.opts=n[t.currIndex].opts.thumbs,t.Thumbs=e,e.$button=t.$refs.toolbar.find("[data-fancybox-thumbs]");for(var i=0,a=n.length;i<a&&(n[i].thumb&&o++,!(o>1));i++);o>1&&e.opts?(e.$button.removeAttr("style").on("click",function(){e.toggle()}),e.isActive=!0):e.$button.hide()},create:function(){var t,o=this,i=o.instance,a=o.opts.parentEl,s=[];o.$grid||(o.$grid=e('<div class="'+n+" "+n+"-"+o.opts.axis+'"></div>').appendTo(i.$refs.container.find(a).addBack().filter(a)),o.$grid.on("click","a",function(){i.jumpTo(e(this).attr("data-index"))})),o.$list||(o.$list=e('<div class="'+n+'__list">').appendTo(o.$grid)),e.each(i.group,function(e,n){t=n.thumb,t||"image"!==n.type||(t=n.src),s.push('<a href="javascript:;" tabindex="0" data-index="'+e+'"'+(t&&t.length?' style="background-image:url('+t+')"':'class="fancybox-thumbs-missing"')+"></a>")}),o.$list[0].innerHTML=s.join(""),"x"===o.opts.axis&&o.$list.width(parseInt(o.$grid.css("padding-right"),10)+i.group.length*o.$list.children().eq(0).outerWidth(!0))},focus:function(t){var e,n,o=this,i=o.$list,a=o.$grid;o.instance.current&&(e=i.children().removeClass("fancybox-thumbs-active").filter('[data-index="'+o.instance.current.index+'"]').addClass("fancybox-thumbs-active"),n=e.position(),"y"===o.opts.axis&&(n.top<0||n.top>i.height()-e.outerHeight())?i.stop().animate({scrollTop:i.scrollTop()+n.top},t):"x"===o.opts.axis&&(n.left<a.scrollLeft()||n.left>a.scrollLeft()+(a.width()-e.outerWidth()))&&i.parent().stop().animate({scrollLeft:n.left},t))},update:function(){var t=this;t.instance.$refs.container.toggleClass("fancybox-show-thumbs",this.isVisible),t.isVisible?(t.$grid||t.create(),t.instance.trigger("onThumbsShow"),t.focus(0)):t.$grid&&t.instance.trigger("onThumbsHide"),t.instance.update()},hide:function(){this.isVisible=!1,this.update()},show:function(){this.isVisible=!0,this.update()},toggle:function(){this.isVisible=!this.isVisible,this.update()}}),e(t).on({"onInit.fb":function(t,e){var n;e&&!e.Thumbs&&(n=new o(e),n.isActive&&!0===n.opts.autoStart&&n.show())},"beforeShow.fb":function(t,e,n,o){var i=e&&e.Thumbs;i&&i.isVisible&&i.focus(o?0:250)},"afterKeydown.fb":function(t,e,n,o,i){var a=e&&e.Thumbs;a&&a.isActive&&71===i&&(o.preventDefault(),a.toggle())},"beforeClose.fb":function(t,e){var n=e&&e.Thumbs;n&&n.isVisible&&!1!==n.opts.hideOnClose&&n.$grid.hide()}})}(document,jQuery),function(t,e){"use strict";function n(t){var e={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#39;","/":"&#x2F;","`":"&#x60;","=":"&#x3D;"};return String(t).replace(/[&<>"'`=\/]/g,function(t){return e[t]})}e.extend(!0,e.fancybox.defaults,{btnTpl:{share:'<button data-fancybox-share class="fancybox-button fancybox-button--share" title="{{SHARE}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M2.55 19c1.4-8.4 9.1-9.8 11.9-9.8V5l7 7-7 6.3v-3.5c-2.8 0-10.5 2.1-11.9 4.2z"/></svg></button>'},share:{url:function(t,e){return!t.currentHash&&"inline"!==e.type&&"html"!==e.type&&(e.origSrc||e.src)||window.location},
tpl:'<div class="fancybox-share"><h1>{{SHARE}}</h1><p><a class="fancybox-share__button fancybox-share__button--fb" href="https://www.facebook.com/sharer/sharer.php?u={{url}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m287 456v-299c0-21 6-35 35-35h38v-63c-7-1-29-3-55-3-54 0-91 33-91 94v306m143-254h-205v72h196" /></svg><span>Facebook</span></a><a class="fancybox-share__button fancybox-share__button--tw" href="https://twitter.com/intent/tweet?url={{url}}&text={{descr}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m456 133c-14 7-31 11-47 13 17-10 30-27 37-46-15 10-34 16-52 20-61-62-157-7-141 75-68-3-129-35-169-85-22 37-11 86 26 109-13 0-26-4-37-9 0 39 28 72 65 80-12 3-25 4-37 2 10 33 41 57 77 57-42 30-77 38-122 34 170 111 378-32 359-208 16-11 30-25 41-42z" /></svg><span>Twitter</span></a><a class="fancybox-share__button fancybox-share__button--pt" href="https://www.pinterest.com/pin/create/button/?url={{url}}&description={{descr}}&media={{media}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m265 56c-109 0-164 78-164 144 0 39 15 74 47 87 5 2 10 0 12-5l4-19c2-6 1-8-3-13-9-11-15-25-15-45 0-58 43-110 113-110 62 0 96 38 96 88 0 67-30 122-73 122-24 0-42-19-36-44 6-29 20-60 20-81 0-19-10-35-31-35-25 0-44 26-44 60 0 21 7 36 7 36l-30 125c-8 37-1 83 0 87 0 3 4 4 5 2 2-3 32-39 42-75l16-64c8 16 31 29 56 29 74 0 124-67 124-157 0-69-58-132-146-132z" fill="#fff"/></svg><span>Pinterest</span></a></p><p><input class="fancybox-share__input" type="text" value="{{url_raw}}" onclick="select()" /></p></div>'}}),e(t).on("click","[data-fancybox-share]",function(){var t,o,i=e.fancybox.getInstance(),a=i.current||null;a&&("function"===e.type(a.opts.share.url)&&(t=a.opts.share.url.apply(a,[i,a])),o=a.opts.share.tpl.replace(/\{\{media\}\}/g,"image"===a.type?encodeURIComponent(a.src):"").replace(/\{\{url\}\}/g,encodeURIComponent(t)).replace(/\{\{url_raw\}\}/g,n(t)).replace(/\{\{descr\}\}/g,i.$caption?encodeURIComponent(i.$caption.text()):""),e.fancybox.open({src:i.translate(i,o),type:"html",opts:{touch:!1,animationEffect:!1,afterLoad:function(t,e){i.$refs.container.one("beforeClose.fb",function(){t.close(null,0)}),e.$content.find(".fancybox-share__button").click(function(){return window.open(this.href,"Share","width=550, height=450"),!1})},mobile:{autoFocus:!1}}}))})}(document,jQuery),function(t,e,n){"use strict";function o(){var e=t.location.hash.substr(1),n=e.split("-"),o=n.length>1&&/^\+?\d+$/.test(n[n.length-1])?parseInt(n.pop(-1),10)||1:1,i=n.join("-");return{hash:e,index:o<1?1:o,gallery:i}}function i(t){""!==t.gallery&&n("[data-fancybox='"+n.escapeSelector(t.gallery)+"']").eq(t.index-1).focus().trigger("click.fb-start")}function a(t){var e,n;return!!t&&(e=t.current?t.current.opts:t.opts,""!==(n=e.hash||(e.$orig?e.$orig.data("fancybox")||e.$orig.data("fancybox-trigger"):""))&&n)}n.escapeSelector||(n.escapeSelector=function(t){return(t+"").replace(/([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g,function(t,e){return e?"\0"===t?"�":t.slice(0,-1)+"\\"+t.charCodeAt(t.length-1).toString(16)+" ":"\\"+t})}),n(function(){!1!==n.fancybox.defaults.hash&&(n(e).on({"onInit.fb":function(t,e){var n,i;!1!==e.group[e.currIndex].opts.hash&&(n=o(),(i=a(e))&&n.gallery&&i==n.gallery&&(e.currIndex=n.index-1))},"beforeShow.fb":function(n,o,i,s){var r;i&&!1!==i.opts.hash&&(r=a(o))&&(o.currentHash=r+(o.group.length>1?"-"+(i.index+1):""),t.location.hash!=="#"+o.currentHash&&(s&&!o.origHash&&(o.origHash=t.location.hash),o.hashTimer&&clearTimeout(o.hashTimer),o.hashTimer=setTimeout(function(){"replaceState"in t.history?(t.history[s?"pushState":"replaceState"]({},e.title,t.location.pathname+t.location.search+"#"+o.currentHash),s&&(o.hasCreatedHistory=!0)):t.location.hash=o.currentHash,o.hashTimer=null},300)))},"beforeClose.fb":function(n,o,i){i&&!1!==i.opts.hash&&(clearTimeout(o.hashTimer),o.currentHash&&o.hasCreatedHistory?t.history.back():o.currentHash&&("replaceState"in t.history?t.history.replaceState({},e.title,t.location.pathname+t.location.search+(o.origHash||"")):t.location.hash=o.origHash),o.currentHash=null)}}),n(t).on("hashchange.fb",function(){var t=o(),e=null;n.each(n(".fancybox-container").get().reverse(),function(t,o){var i=n(o).data("FancyBox");if(i&&i.currentHash)return e=i,!1}),e?e.currentHash===t.gallery+"-"+t.index||1===t.index&&e.currentHash==t.gallery||(e.currentHash=null,e.close()):""!==t.gallery&&i(t)}),setTimeout(function(){n.fancybox.getInstance()||i(o())},50))})}(window,document,jQuery),function(t,e){"use strict";var n=(new Date).getTime();e(t).on({"onInit.fb":function(t,e,o){e.$refs.stage.on("mousewheel DOMMouseScroll wheel MozMousePixelScroll",function(t){var o=e.current,i=(new Date).getTime();e.group.length<2||!1===o.opts.wheel||"auto"===o.opts.wheel&&"image"!==o.type||(t.preventDefault(),t.stopPropagation(),o.$slide.hasClass("fancybox-animated")||(t=t.originalEvent||t,i-n<250||(n=i,e[(-t.deltaY||-t.deltaX||t.wheelDelta||-t.detail)<0?"next":"previous"]())))})}})}(document,jQuery);
(function(factory) {
  /* global define */
  /* istanbul ignore next */
  if ( typeof define === 'function' && define.amd ) {
    define(['jquery'], factory);
  } else if ( typeof module === 'object' && module.exports ) {
    // Node/CommonJS
    module.exports = function( root, jQuery ) {
      if ( jQuery === undefined ) {
        if ( typeof window !== 'undefined' ) {
          jQuery = require('jquery');
        } else {
          jQuery = require('jquery')(root);
        }
      }
      factory(jQuery);
      return jQuery;
    };
  } else {
    // Browser globals
    factory(jQuery);
  }
}(function($) {
  'use strict';

  var $doc = $(document);
  var $win = $(window);

  var pluginName = 'selectric';
  var classList = 'Input Items Open Disabled TempShow HideSelect Wrapper Focus Hover Responsive Above Below Scroll Group GroupLabel';
  var eventNamespaceSuffix = '.sl';

  var chars = ['a', 'e', 'i', 'o', 'u', 'n', 'c', 'y'];
  var diacritics = [
    /[\xE0-\xE5]/g, // a
    /[\xE8-\xEB]/g, // e
    /[\xEC-\xEF]/g, // i
    /[\xF2-\xF6]/g, // o
    /[\xF9-\xFC]/g, // u
    /[\xF1]/g,      // n
    /[\xE7]/g,      // c
    /[\xFD-\xFF]/g  // y
  ];

  /**
   * Create an instance of Selectric
   *
   * @constructor
   * @param {Node} element - The &lt;select&gt; element
   * @param {object}  opts - Options
   */
  var Selectric = function(element, opts) {
    var _this = this;

    _this.element = element;
    _this.$element = $(element);

    _this.state = {
      multiple       : !!_this.$element.attr('multiple'),
      enabled        : false,
      opened         : false,
      currValue      : -1,
      selectedIdx    : -1,
      highlightedIdx : -1
    };

    _this.eventTriggers = {
      open    : _this.open,
      close   : _this.close,
      destroy : _this.destroy,
      refresh : _this.refresh,
      init    : _this.init
    };

    _this.init(opts);
  };

  Selectric.prototype = {
    utils: {
      /**
       * Detect mobile browser
       *
       * @return {boolean}
       */
      isMobile: function() {
        return /android|ip(hone|od|ad)/i.test(navigator.userAgent);
      },

      /**
       * Escape especial characters in string (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions)
       *
       * @param  {string} str - The string to be escaped
       * @return {string}       The string with the special characters escaped
       */
      escapeRegExp: function(str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
      },

      /**
       * Replace diacritics
       *
       * @param  {string} str - The string to replace the diacritics
       * @return {string}       The string with diacritics replaced with ascii characters
       */
      replaceDiacritics: function(str) {
        var k = diacritics.length;

        while (k--) {
          str = str.toLowerCase().replace(diacritics[k], chars[k]);
        }

        return str;
      },

      /**
       * Format string
       * https://gist.github.com/atesgoral/984375
       *
       * @param  {string} f - String to be formated
       * @return {string}     String formated
       */
      format: function(f) {
        var a = arguments; // store outer arguments
        return ('' + f) // force format specifier to String
          .replace( // replace tokens in format specifier
            /\{(?:(\d+)|(\w+))\}/g, // match {token} references
            function(
              s, // the matched string (ignored)
              i, // an argument index
              p // a property name
            ) {
              return p && a[1] // if property name and first argument exist
                ? a[1][p] // return property from first argument
                : a[i]; // assume argument index and return i-th argument
            });
      },

      /**
       * Get the next enabled item in the options list.
       *
       * @param  {object} selectItems - The options object.
       * @param  {number}    selected - Index of the currently selected option.
       * @return {object}               The next enabled item.
       */
      nextEnabledItem: function(selectItems, selected) {
        while ( selectItems[ selected = (selected + 1) % selectItems.length ].disabled ) {
          // empty
        }
        return selected;
      },

      /**
       * Get the previous enabled item in the options list.
       *
       * @param  {object} selectItems - The options object.
       * @param  {number}    selected - Index of the currently selected option.
       * @return {object}               The previous enabled item.
       */
      previousEnabledItem: function(selectItems, selected) {
        while ( selectItems[ selected = (selected > 0 ? selected : selectItems.length) - 1 ].disabled ) {
          // empty
        }
        return selected;
      },

      /**
       * Transform camelCase string to dash-case.
       *
       * @param  {string} str - The camelCased string.
       * @return {string}       The string transformed to dash-case.
       */
      toDash: function(str) {
        return str.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase();
      },

      /**
       * Calls the events registered with function name.
       *
       * @param {string}    fn - The name of the function.
       * @param {number} scope - Scope that should be set on the function.
       */
      triggerCallback: function(fn, scope) {
        var elm = scope.element;
        var func = scope.options['on' + fn];
        var args = [elm].concat([].slice.call(arguments).slice(1));

        if ( $.isFunction(func) ) {
          func.apply(elm, args);
        }

        $(elm).trigger(pluginName + '-' + this.toDash(fn), args);
      },

      /**
       * Transform array list to concatenated string and remove empty values
       * @param  {array} arr - Class list
       * @return {string}      Concatenated string
       */
      arrayToClassname: function(arr) {
        var newArr = $.grep(arr, function(item) {
          return !!item;
        });

        return $.trim(newArr.join(' '));
      }
    },

    /** Initializes */
    init: function(opts) {
      var _this = this;

      // Set options
      _this.options = $.extend(true, {}, $.fn[pluginName].defaults, _this.options, opts);

      _this.utils.triggerCallback('BeforeInit', _this);

      // Preserve data
      _this.destroy(true);

      // Disable on mobile browsers
      if ( _this.options.disableOnMobile && _this.utils.isMobile() ) {
        _this.disableOnMobile = true;
        return;
      }

      // Get classes
      _this.classes = _this.getClassNames();

      // Create elements
      var input              = $('<input/>', { 'class': _this.classes.input, 'readonly': _this.utils.isMobile() });
      var items              = $('<div/>',   { 'class': _this.classes.items, 'tabindex': -1 });
      var itemsScroll        = $('<div/>',   { 'class': _this.classes.scroll });
      var wrapper            = $('<div/>',   { 'class': _this.classes.prefix, 'html': _this.options.arrowButtonMarkup });
      var label              = $('<span/>',  { 'class': 'label' });
      var outerWrapper       = _this.$element.wrap('<div/>').parent().append(wrapper.prepend(label), items, input);
      var hideSelectWrapper  = $('<div/>',   { 'class': _this.classes.hideselect });

      _this.elements = {
        input        : input,
        items        : items,
        itemsScroll  : itemsScroll,
        wrapper      : wrapper,
        label        : label,
        outerWrapper : outerWrapper
      };

      if ( _this.options.nativeOnMobile && _this.utils.isMobile() ) {
        _this.elements.input = undefined;
        hideSelectWrapper.addClass(_this.classes.prefix + '-is-native');

        _this.$element.on('change', function() {
          _this.refresh();
        });
      }

      _this.$element
        .on(_this.eventTriggers)
        .wrap(hideSelectWrapper);

      _this.originalTabindex = _this.$element.prop('tabindex');
      _this.$element.prop('tabindex', -1);

      _this.populate();
      _this.activate();

      _this.utils.triggerCallback('Init', _this);
    },

    /** Activates the plugin */
    activate: function() {
      var _this = this;
      var hiddenChildren = _this.elements.items.closest(':visible').children(':hidden').addClass(_this.classes.tempshow);
      var originalWidth = _this.$element.width();

      hiddenChildren.removeClass(_this.classes.tempshow);

      _this.utils.triggerCallback('BeforeActivate', _this);

      _this.elements.outerWrapper.prop('class',
        _this.utils.arrayToClassname([
          _this.classes.wrapper,
          _this.$element.prop('class').replace(/\S+/g, _this.classes.prefix + '-$&'),
          _this.options.responsive ? _this.classes.responsive : ''
        ])
      );

      if ( _this.options.inheritOriginalWidth && originalWidth > 0 ) {
        _this.elements.outerWrapper.width(originalWidth);
      }

      _this.unbindEvents();

      if ( !_this.$element.prop('disabled') ) {
        _this.state.enabled = true;

        // Not disabled, so... Removing disabled class
        _this.elements.outerWrapper.removeClass(_this.classes.disabled);

        // Remove styles from items box
        // Fix incorrect height when refreshed is triggered with fewer options
        _this.$li = _this.elements.items.removeAttr('style').find('li');

        _this.bindEvents();
      } else {
        _this.elements.outerWrapper.addClass(_this.classes.disabled);

        if ( _this.elements.input ) {
          _this.elements.input.prop('disabled', true);
        }
      }

      _this.utils.triggerCallback('Activate', _this);
    },

    /**
     * Generate classNames for elements
     *
     * @return {object} Classes object
     */
    getClassNames: function() {
      var _this = this;
      var customClass = _this.options.customClass;
      var classesObj = {};

      $.each(classList.split(' '), function(i, currClass) {
        var c = customClass.prefix + currClass;
        classesObj[currClass.toLowerCase()] = customClass.camelCase ? c : _this.utils.toDash(c);
      });

      classesObj.prefix = customClass.prefix;

      return classesObj;
    },

    /** Set the label text */
    setLabel: function() {
      var _this = this;
      var labelBuilder = _this.options.labelBuilder;

      if ( _this.state.multiple ) {
        // Make sure currentValues is an array
        var currentValues = $.isArray(_this.state.currValue) ? _this.state.currValue : [_this.state.currValue];
        // I'm not happy with this, but currentValues can be an empty
        // array and we need to fallback to the default option.
        currentValues = currentValues.length === 0 ? [0] : currentValues;

        var labelMarkup = $.map(currentValues, function(value) {
          return $.grep(_this.lookupItems, function(item) {
            return item.index === value;
          })[0]; // we don't want nested arrays here
        });

        labelMarkup = $.grep(labelMarkup, function(item) {
          // Hide default (please choose) if more then one element were selected.
          // If no option value were given value is set to option text by default
          if ( labelMarkup.length > 1 || labelMarkup.length === 0 ) {
            return $.trim(item.value) !== '';
          }
          return item;
        });

        labelMarkup = $.map(labelMarkup, function(item) {
          return $.isFunction(labelBuilder)
            ? labelBuilder(item)
            : _this.utils.format(labelBuilder, item);
        });

        // Limit the amount of selected values shown in label
        if ( _this.options.multiple.maxLabelEntries ) {
          if ( labelMarkup.length >= _this.options.multiple.maxLabelEntries + 1 ) {
            labelMarkup = labelMarkup.slice(0, _this.options.multiple.maxLabelEntries);
            labelMarkup.push(
              $.isFunction(labelBuilder)
                ? labelBuilder({ text: '...' })
                : _this.utils.format(labelBuilder, { text: '...' }));
          } else {
            labelMarkup.slice(labelMarkup.length - 1);
          }
        }
        _this.elements.label.html(labelMarkup.join(_this.options.multiple.separator));

      } else {
        var currItem = _this.lookupItems[_this.state.currValue];

        _this.elements.label.html(
          $.isFunction(labelBuilder)
            ? labelBuilder(currItem)
            : _this.utils.format(labelBuilder, currItem)
        );
      }
    },

    /** Get and save the available options */
    populate: function() {
      var _this = this;
      var $options = _this.$element.children();
      var $justOptions = _this.$element.find('option');
      var $selected = $justOptions.filter(':selected');
      var selectedIndex = $justOptions.index($selected);
      var currIndex = 0;
      var emptyValue = (_this.state.multiple ? [] : 0);

      if ( $selected.length > 1 && _this.state.multiple ) {
        selectedIndex = [];
        $selected.each(function() {
          selectedIndex.push($(this).index());
        });
      }

      _this.state.currValue = (~selectedIndex ? selectedIndex : emptyValue);
      _this.state.selectedIdx = _this.state.currValue;
      _this.state.highlightedIdx = _this.state.currValue;
      _this.items = [];
      _this.lookupItems = [];

      if ( $options.length ) {
        // Build options markup
        $options.each(function(i) {
          var $elm = $(this);

          if ( $elm.is('optgroup') ) {

            var optionsGroup = {
              element       : $elm,
              label         : $elm.prop('label'),
              groupDisabled : $elm.prop('disabled'),
              items         : []
            };

            $elm.children().each(function(i) {
              var $elm = $(this);

              optionsGroup.items[i] = _this.getItemData(currIndex, $elm, optionsGroup.groupDisabled || $elm.prop('disabled'));

              _this.lookupItems[currIndex] = optionsGroup.items[i];

              currIndex++;
            });

            _this.items[i] = optionsGroup;

          } else {

            _this.items[i] = _this.getItemData(currIndex, $elm, $elm.prop('disabled'));

            _this.lookupItems[currIndex] = _this.items[i];

            currIndex++;

          }
        });

        _this.setLabel();
        _this.elements.items.append( _this.elements.itemsScroll.html( _this.getItemsMarkup(_this.items) ) );
      }
    },

    /**
     * Generate items object data
     * @param  {integer} index      - Current item index
     * @param  {node}    $elm       - Current element node
     * @param  {boolean} isDisabled - Current element disabled state
     * @return {object}               Item object
     */
    getItemData: function(index, $elm, isDisabled) {
      var _this = this;

      return {
        index     : index,
        element   : $elm,
        value     : $elm.val(),
        className : $elm.prop('class'),
        text      : $elm.html(),
        slug      : $.trim(_this.utils.replaceDiacritics($elm.html())),
        alt       : $elm.attr('data-alt'),
        selected  : $elm.prop('selected'),
        disabled  : isDisabled
      };
    },

    /**
     * Generate options markup
     *
     * @param  {object} items - Object containing all available options
     * @return {string}         HTML for the options box
     */
    getItemsMarkup: function(items) {
      var _this = this;
      var markup = '<ul>';

      if ( $.isFunction(_this.options.listBuilder) && _this.options.listBuilder ) {
        items = _this.options.listBuilder(items);
      }

      $.each(items, function(i, elm) {
        if ( elm.label !== undefined ) {

          markup += _this.utils.format('<ul class="{1}"><li class="{2}">{3}</li>',
            _this.utils.arrayToClassname([
              _this.classes.group,
              elm.groupDisabled ? 'disabled' : '',
              elm.element.prop('class')
            ]),
            _this.classes.grouplabel,
            elm.element.prop('label')
          );

          $.each(elm.items, function(i, elm) {
            markup += _this.getItemMarkup(elm.index, elm);
          });

          markup += '</ul>';

        } else {

          markup += _this.getItemMarkup(elm.index, elm);

        }
      });

      return markup + '</ul>';
    },

    /**
     * Generate every option markup
     *
     * @param  {number} index    - Index of current item
     * @param  {object} itemData - Current item
     * @return {string}            HTML for the option
     */
    getItemMarkup: function(index, itemData) {
      var _this = this;
      var itemBuilder = _this.options.optionsItemBuilder;
      // limit access to item data to provide a simple interface
      // to most relevant options.
      var filteredItemData = {
        value: itemData.value,
        text : itemData.text,
        slug : itemData.slug,
        index: itemData.index
      };

      return _this.utils.format('<li data-index="{1}" class="{2}">{3}</li>',
        index,
        _this.utils.arrayToClassname([
          itemData.className,
          index === _this.items.length - 1  ? 'last'     : '',
          itemData.disabled                 ? 'disabled' : '',
          itemData.selected                 ? 'selected' : ''
        ]),
        $.isFunction(itemBuilder)
          ? _this.utils.format(itemBuilder(itemData, this.$element, index), itemData)
          : _this.utils.format(itemBuilder, filteredItemData)
      );
    },

    /** Remove events on the elements */
    unbindEvents: function() {
      var _this = this;

      _this.elements.wrapper
        .add(_this.$element)
        .add(_this.elements.outerWrapper)
        .add(_this.elements.input)
        .off(eventNamespaceSuffix);
    },

    /** Bind events on the elements */
    bindEvents: function() {
      var _this = this;

      _this.elements.outerWrapper.on('mouseenter' + eventNamespaceSuffix + ' mouseleave' + eventNamespaceSuffix, function(e) {
        $(this).toggleClass(_this.classes.hover, e.type === 'mouseenter');

        // Delay close effect when openOnHover is true
        if ( _this.options.openOnHover ) {
          clearTimeout(_this.closeTimer);

          if ( e.type === 'mouseleave' ) {
            _this.closeTimer = setTimeout($.proxy(_this.close, _this), _this.options.hoverIntentTimeout);
          } else {
            _this.open();
          }
        }
      });

      // Toggle open/close
      _this.elements.wrapper.on('click' + eventNamespaceSuffix, function(e) {
        _this.state.opened ? _this.close() : _this.open(e);
      });

      // Translate original element focus event to dummy input.
      // Disabled on mobile devices because the default option list isn't
      // shown due the fact that hidden input gets focused
      if ( !(_this.options.nativeOnMobile && _this.utils.isMobile()) ) {
        _this.$element.on('focus' + eventNamespaceSuffix, function() {
          _this.elements.input.focus();
        });

        _this.elements.input
          .prop({ tabindex: _this.originalTabindex, disabled: false })
          .on('keydown' + eventNamespaceSuffix, $.proxy(_this.handleKeys, _this))
          .on('focusin' + eventNamespaceSuffix, function(e) {
            _this.elements.outerWrapper.addClass(_this.classes.focus);

            // Prevent the flicker when focusing out and back again in the browser window
            _this.elements.input.one('blur', function() {
              _this.elements.input.blur();
            });

            if ( _this.options.openOnFocus && !_this.state.opened ) {
              _this.open(e);
            }
          })
          .on('focusout' + eventNamespaceSuffix, function() {
            _this.elements.outerWrapper.removeClass(_this.classes.focus);
          })
          .on('input propertychange', function() {
            var val = _this.elements.input.val();
            var searchRegExp = new RegExp('^' + _this.utils.escapeRegExp(val), 'i');

            // Clear search
            clearTimeout(_this.resetStr);
            _this.resetStr = setTimeout(function() {
              _this.elements.input.val('');
            }, _this.options.keySearchTimeout);

            if ( val.length ) {
              // Search in select options
              $.each(_this.items, function(i, elm) {
                if (elm.disabled) {
                  return;
                }
                if (searchRegExp.test(elm.text) || searchRegExp.test(elm.slug)) {
                  _this.highlight(i);
                  return false;
                }
                if (!elm.alt) {
                  return;
                }
                var altItems = elm.alt.split('|');
                for (var ai = 0; ai < altItems.length; ai++) {
                  if (!altItems[ai]) {
                    break;
                  }
                  if (searchRegExp.test(altItems[ai].trim())) {
                    _this.highlight(i);
                    return false;
                  }
                }
              });
            }
          });
      }

      _this.$li.on({
        // Prevent <input> blur on Chrome
        mousedown: function(e) {
          e.preventDefault();
          e.stopPropagation();
        },
        click: function() {
          _this.select($(this).data('index'));

          // Chrome doesn't close options box if select is wrapped with a label
          // We need to 'return false' to avoid that
          return false;
        }
      });
    },

    /**
     * Behavior when keyboard keys is pressed
     *
     * @param {object} e - Event object
     */
    handleKeys: function(e) {
      var _this = this;
      var key = e.which;
      var keys = _this.options.keys;

      var isPrevKey = $.inArray(key, keys.previous) > -1;
      var isNextKey = $.inArray(key, keys.next) > -1;
      var isSelectKey = $.inArray(key, keys.select) > -1;
      var isOpenKey = $.inArray(key, keys.open) > -1;
      var idx = _this.state.highlightedIdx;
      var isFirstOrLastItem = (isPrevKey && idx === 0) || (isNextKey && (idx + 1) === _this.items.length);
      var goToItem = 0;

      // Enter / Space
      if ( key === 13 || key === 32 ) {
        e.preventDefault();
      }

      // If it's a directional key
      if ( isPrevKey || isNextKey ) {
        if ( !_this.options.allowWrap && isFirstOrLastItem ) {
          return;
        }

        if ( isPrevKey ) {
          goToItem = _this.utils.previousEnabledItem(_this.lookupItems, idx);
        }

        if ( isNextKey ) {
          goToItem = _this.utils.nextEnabledItem(_this.lookupItems, idx);
        }

        _this.highlight(goToItem);
      }

      // Tab / Enter / ESC
      if ( isSelectKey && _this.state.opened ) {
        _this.select(idx);

        if ( !_this.state.multiple || !_this.options.multiple.keepMenuOpen ) {
          _this.close();
        }

        return;
      }

      // Space / Enter / Left / Up / Right / Down
      if ( isOpenKey && !_this.state.opened ) {
        _this.open();
      }
    },

    /** Update the items object */
    refresh: function() {
      var _this = this;

      _this.populate();
      _this.activate();
      _this.utils.triggerCallback('Refresh', _this);
    },

    /** Set options box width/height */
    setOptionsDimensions: function() {
      var _this = this;

      // Calculate options box height
      // Set a temporary class on the hidden parent of the element
      var hiddenChildren = _this.elements.items.closest(':visible').children(':hidden').addClass(_this.classes.tempshow);
      var maxHeight = _this.options.maxHeight;
      var itemsWidth = _this.elements.items.outerWidth();
      var wrapperWidth = _this.elements.wrapper.outerWidth() - (itemsWidth - _this.elements.items.width());

      // Set the dimensions, minimum is wrapper width, expand for long items if option is true
      if ( !_this.options.expandToItemText || wrapperWidth > itemsWidth ) {
        _this.finalWidth = wrapperWidth;
      } else {
        // Make sure the scrollbar width is included
        _this.elements.items.css('overflow', 'scroll');

        // Set a really long width for _this.elements.outerWrapper
        _this.elements.outerWrapper.width(9e4);
        _this.finalWidth = _this.elements.items.width();
        // Set scroll bar to auto
        _this.elements.items.css('overflow', '');
        _this.elements.outerWrapper.width('');
      }

      _this.elements.items.width(_this.finalWidth).height() > maxHeight && _this.elements.items.height(maxHeight);

      // Remove the temporary class
      hiddenChildren.removeClass(_this.classes.tempshow);
    },

    /** Detect if the options box is inside the window */
    isInViewport: function() {
      var _this = this;

      if (_this.options.forceRenderAbove === true) {
        _this.elements.outerWrapper.addClass(_this.classes.above);
      } else if (_this.options.forceRenderBelow === true) {
        _this.elements.outerWrapper.addClass(_this.classes.below);
      } else {
        var scrollTop = $win.scrollTop();
        var winHeight = $win.height();
        var uiPosX = _this.elements.outerWrapper.offset().top;
        var uiHeight = _this.elements.outerWrapper.outerHeight();

        var fitsDown = (uiPosX + uiHeight + _this.itemsHeight) <= (scrollTop + winHeight);
        var fitsAbove = (uiPosX - _this.itemsHeight) > scrollTop;

        // If it does not fit below, only render it
        // above it fit's there.
        // It's acceptable that the user needs to
        // scroll the viewport to see the cut off UI
        var renderAbove = !fitsDown && fitsAbove;
        var renderBelow = !renderAbove;

        _this.elements.outerWrapper.toggleClass(_this.classes.above, renderAbove);
        _this.elements.outerWrapper.toggleClass(_this.classes.below, renderBelow);
      }
    },

    /**
     * Detect if currently selected option is visible and scroll the options box to show it
     *
     * @param {Number|Array} index - Index of the selected items
     */
    detectItemVisibility: function(index) {
      var _this = this;
      var $filteredLi = _this.$li.filter('[data-index]');

      if ( _this.state.multiple ) {
        // If index is an array, we can assume a multiple select and we
        // want to scroll to the uppermost selected item!
        // Math.min.apply(Math, index) returns the lowest entry in an Array.
        index = ($.isArray(index) && index.length === 0) ? 0 : index;
        index = $.isArray(index) ? Math.min.apply(Math, index) : index;
      }

      var liHeight = $filteredLi.eq(index).outerHeight();
      var liTop = $filteredLi[index].offsetTop;
      var itemsScrollTop = _this.elements.itemsScroll.scrollTop();
      var scrollT = liTop + liHeight * 2;

      _this.elements.itemsScroll.scrollTop(
        scrollT > itemsScrollTop + _this.itemsHeight ? scrollT - _this.itemsHeight :
          liTop - liHeight < itemsScrollTop ? liTop - liHeight :
            itemsScrollTop
      );
    },

    /**
     * Open the select options box
     *
     * @param {Event} e - Event
     */
    open: function(e) {
      var _this = this;

      if ( _this.options.nativeOnMobile && _this.utils.isMobile()) {
        return false;
      }

      _this.utils.triggerCallback('BeforeOpen', _this);

      if ( e ) {
        e.preventDefault();
        if (_this.options.stopPropagation) {
          e.stopPropagation();
        }
      }

      if ( _this.state.enabled ) {
        _this.setOptionsDimensions();

        // Find any other opened instances of select and close it
        $('.' + _this.classes.hideselect, '.' + _this.classes.open).children()[pluginName]('close');

        _this.state.opened = true;
        _this.itemsHeight = _this.elements.items.outerHeight();
        _this.itemsInnerHeight = _this.elements.items.height();

        // Toggle options box visibility
        _this.elements.outerWrapper.addClass(_this.classes.open);

        // Give dummy input focus
        _this.elements.input.val('');
        if ( e && e.type !== 'focusin' ) {
          _this.elements.input.focus();
        }

        // Delayed binds events on Document to make label clicks work
        setTimeout(function() {
          $doc
            .on('click' + eventNamespaceSuffix, $.proxy(_this.close, _this))
            .on('scroll' + eventNamespaceSuffix, $.proxy(_this.isInViewport, _this));
        }, 1);

        _this.isInViewport();

        // Prevent window scroll when using mouse wheel inside items box
        if ( _this.options.preventWindowScroll ) {
          /* istanbul ignore next */
          $doc.on('mousewheel' + eventNamespaceSuffix + ' DOMMouseScroll' + eventNamespaceSuffix, '.' + _this.classes.scroll, function(e) {
            var orgEvent = e.originalEvent;
            var scrollTop = $(this).scrollTop();
            var deltaY = 0;

            if ( 'detail'      in orgEvent ) { deltaY = orgEvent.detail * -1; }
            if ( 'wheelDelta'  in orgEvent ) { deltaY = orgEvent.wheelDelta;  }
            if ( 'wheelDeltaY' in orgEvent ) { deltaY = orgEvent.wheelDeltaY; }
            if ( 'deltaY'      in orgEvent ) { deltaY = orgEvent.deltaY * -1; }

            if ( scrollTop === (this.scrollHeight - _this.itemsInnerHeight) && deltaY < 0 || scrollTop === 0 && deltaY > 0 ) {
              e.preventDefault();
            }
          });
        }

        _this.detectItemVisibility(_this.state.selectedIdx);

        _this.highlight(_this.state.multiple ? -1 : _this.state.selectedIdx);

        _this.utils.triggerCallback('Open', _this);
      }
    },

    /** Close the select options box */
    close: function() {
      var _this = this;

      _this.utils.triggerCallback('BeforeClose', _this);

      // Remove custom events on document
      $doc.off(eventNamespaceSuffix);

      // Remove visible class to hide options box
      _this.elements.outerWrapper.removeClass(_this.classes.open);

      _this.state.opened = false;

      _this.utils.triggerCallback('Close', _this);
    },

    /** Select current option and change the label */
    change: function() {
      var _this = this;

      _this.utils.triggerCallback('BeforeChange', _this);

      if ( _this.state.multiple ) {
        // Reset old selected
        $.each(_this.lookupItems, function(idx) {
          _this.lookupItems[idx].selected = false;
          _this.$element.find('option').prop('selected', false);
        });

        // Set new selected
        $.each(_this.state.selectedIdx, function(idx, value) {
          _this.lookupItems[value].selected = true;
          _this.$element.find('option').eq(value).prop('selected', true);
        });

        _this.state.currValue = _this.state.selectedIdx;

        _this.setLabel();

        _this.utils.triggerCallback('Change', _this);
      } else if ( _this.state.currValue !== _this.state.selectedIdx ) {
        // Apply changed value to original select
        _this.$element
          .prop('selectedIndex', _this.state.currValue = _this.state.selectedIdx)
          .data('value', _this.lookupItems[_this.state.selectedIdx].text);

        // Change label text
        _this.setLabel();

        _this.utils.triggerCallback('Change', _this);
      }
    },

    /**
     * Highlight option
     * @param {number} index - Index of the options that will be highlighted
     */
    highlight: function(index) {
      var _this = this;
      var $filteredLi = _this.$li.filter('[data-index]').removeClass('highlighted');

      _this.utils.triggerCallback('BeforeHighlight', _this);

      // Parameter index is required and should not be a disabled item
      if ( index === undefined || index === -1 || _this.lookupItems[index].disabled ) {
        return;
      }

      $filteredLi
        .eq(_this.state.highlightedIdx = index)
        .addClass('highlighted');

      _this.detectItemVisibility(index);

      _this.utils.triggerCallback('Highlight', _this);
    },

    /**
     * Select option
     *
     * @param {number} index - Index of the option that will be selected
     */
    select: function(index) {
      var _this = this;
      var $filteredLi = _this.$li.filter('[data-index]');

      _this.utils.triggerCallback('BeforeSelect', _this, index);

      // Parameter index is required and should not be a disabled item
      if ( index === undefined || index === -1 || _this.lookupItems[index].disabled ) {
        return;
      }

      if ( _this.state.multiple ) {
        // Make sure selectedIdx is an array
        _this.state.selectedIdx = $.isArray(_this.state.selectedIdx) ? _this.state.selectedIdx : [_this.state.selectedIdx];

        var hasSelectedIndex = $.inArray(index, _this.state.selectedIdx);
        if ( hasSelectedIndex !== -1 ) {
          _this.state.selectedIdx.splice(hasSelectedIndex, 1);
        } else {
          _this.state.selectedIdx.push(index);
        }

        $filteredLi
          .removeClass('selected')
          .filter(function(index) {
            return $.inArray(index, _this.state.selectedIdx) !== -1;
          })
          .addClass('selected');
      } else {
        $filteredLi
          .removeClass('selected')
          .eq(_this.state.selectedIdx = index)
          .addClass('selected');
      }

      if ( !_this.state.multiple || !_this.options.multiple.keepMenuOpen ) {
        _this.close();
      }

      _this.change();

      _this.utils.triggerCallback('Select', _this, index);
    },

    /**
     * Unbind and remove
     *
     * @param {boolean} preserveData - Check if the data on the element should be removed too
     */
    destroy: function(preserveData) {
      var _this = this;

      if ( _this.state && _this.state.enabled ) {
        _this.elements.items.add(_this.elements.wrapper).add(_this.elements.input).remove();

        if ( !preserveData ) {
          _this.$element.removeData(pluginName).removeData('value');
        }

        _this.$element.prop('tabindex', _this.originalTabindex).off(eventNamespaceSuffix).off(_this.eventTriggers).unwrap().unwrap();

        _this.state.enabled = false;
      }
    }
  };

  // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations
  $.fn[pluginName] = function(args) {
    return this.each(function() {
      var data = $.data(this, pluginName);

      if ( data && !data.disableOnMobile ) {
        (typeof args === 'string' && data[args]) ? data[args]() : data.init(args);
      } else {
        $.data(this, pluginName, new Selectric(this, args));
      }
    });
  };

  /**
   * Default plugin options
   *
   * @type {object}
   */
  $.fn[pluginName].defaults = {
    onChange             : function(elm) { $(elm).change(); },
    maxHeight            : 300,
    keySearchTimeout     : 500,
    arrowButtonMarkup    : '<b class="button">&#x25be;</b>',
    disableOnMobile      : false,
    nativeOnMobile       : true,
    openOnFocus          : true,
    openOnHover          : false,
    hoverIntentTimeout   : 500,
    expandToItemText     : false,
    responsive           : false,
    preventWindowScroll  : true,
    inheritOriginalWidth : false,
    allowWrap            : true,
    forceRenderAbove     : false,
    forceRenderBelow     : false,
    stopPropagation      : true,
    optionsItemBuilder   : '{text}', // function(itemData, element, index)
    labelBuilder         : '{text}', // function(currItem)
    listBuilder          : false,    // function(items)
    keys                 : {
      previous : [37, 38],                 // Left / Up
      next     : [39, 40],                 // Right / Down
      select   : [9, 13, 27],              // Tab / Enter / Escape
      open     : [13, 32, 37, 38, 39, 40], // Enter / Space / Left / Up / Right / Down
      close    : [9, 27]                   // Tab / Escape
    },
    customClass          : {
      prefix: pluginName,
      camelCase: false
    },
    multiple              : {
      separator: ', ',
      keepMenuOpen: true,
      maxLabelEntries: false
    }
  };
}));

/*! Selectric ϟ v1.13.0 (2017-08-22) - git.io/tjl9sQ - Copyright (c) 2017 Leonardo Santos - MIT License */
!function(e){"function"==typeof define&&define.amd?define(["jquery"],e):"object"==typeof module&&module.exports?module.exports=function(t,s){return void 0===s&&(s="undefined"!=typeof window?require("jquery"):require("jquery")(t)),e(s),s}:e(jQuery)}(function(e){"use strict";var t=e(document),s=e(window),l=["a","e","i","o","u","n","c","y"],i=[/[\xE0-\xE5]/g,/[\xE8-\xEB]/g,/[\xEC-\xEF]/g,/[\xF2-\xF6]/g,/[\xF9-\xFC]/g,/[\xF1]/g,/[\xE7]/g,/[\xFD-\xFF]/g],n=function(t,s){var l=this;l.element=t,l.$element=e(t),l.state={multiple:!!l.$element.attr("multiple"),enabled:!1,opened:!1,currValue:-1,selectedIdx:-1,highlightedIdx:-1},l.eventTriggers={open:l.open,close:l.close,destroy:l.destroy,refresh:l.refresh,init:l.init},l.init(s)};n.prototype={utils:{isMobile:function(){return/android|ip(hone|od|ad)/i.test(navigator.userAgent)},escapeRegExp:function(e){return e.replace(/[.*+?^${}()|[\]\\]/g,"\\$&")},replaceDiacritics:function(e){for(var t=i.length;t--;)e=e.toLowerCase().replace(i[t],l[t]);return e},format:function(e){var t=arguments;return(""+e).replace(/\{(?:(\d+)|(\w+))\}/g,function(e,s,l){return l&&t[1]?t[1][l]:t[s]})},nextEnabledItem:function(e,t){for(;e[t=(t+1)%e.length].disabled;);return t},previousEnabledItem:function(e,t){for(;e[t=(t>0?t:e.length)-1].disabled;);return t},toDash:function(e){return e.replace(/([a-z0-9])([A-Z])/g,"$1-$2").toLowerCase()},triggerCallback:function(t,s){var l=s.element,i=s.options["on"+t],n=[l].concat([].slice.call(arguments).slice(1));e.isFunction(i)&&i.apply(l,n),e(l).trigger("selectric-"+this.toDash(t),n)},arrayToClassname:function(t){var s=e.grep(t,function(e){return!!e});return e.trim(s.join(" "))}},init:function(t){var s=this;if(s.options=e.extend(!0,{},e.fn.selectric.defaults,s.options,t),s.utils.triggerCallback("BeforeInit",s),s.destroy(!0),s.options.disableOnMobile&&s.utils.isMobile())return void(s.disableOnMobile=!0);s.classes=s.getClassNames();var l=e("<input/>",{class:s.classes.input,readonly:s.utils.isMobile()}),i=e("<div/>",{class:s.classes.items,tabindex:-1}),n=e("<div/>",{class:s.classes.scroll}),a=e("<div/>",{class:s.classes.prefix,html:s.options.arrowButtonMarkup}),o=e("<span/>",{class:"label"}),r=s.$element.wrap("<div/>").parent().append(a.prepend(o),i,l),u=e("<div/>",{class:s.classes.hideselect});s.elements={input:l,items:i,itemsScroll:n,wrapper:a,label:o,outerWrapper:r},s.options.nativeOnMobile&&s.utils.isMobile()&&(s.elements.input=void 0,u.addClass(s.classes.prefix+"-is-native"),s.$element.on("change",function(){s.refresh()})),s.$element.on(s.eventTriggers).wrap(u),s.originalTabindex=s.$element.prop("tabindex"),s.$element.prop("tabindex",-1),s.populate(),s.activate(),s.utils.triggerCallback("Init",s)},activate:function(){var e=this,t=e.elements.items.closest(":visible").children(":hidden").addClass(e.classes.tempshow),s=e.$element.width();t.removeClass(e.classes.tempshow),e.utils.triggerCallback("BeforeActivate",e),e.elements.outerWrapper.prop("class",e.utils.arrayToClassname([e.classes.wrapper,e.$element.prop("class").replace(/\S+/g,e.classes.prefix+"-$&"),e.options.responsive?e.classes.responsive:""])),e.options.inheritOriginalWidth&&s>0&&e.elements.outerWrapper.width(s),e.unbindEvents(),e.$element.prop("disabled")?(e.elements.outerWrapper.addClass(e.classes.disabled),e.elements.input&&e.elements.input.prop("disabled",!0)):(e.state.enabled=!0,e.elements.outerWrapper.removeClass(e.classes.disabled),e.$li=e.elements.items.removeAttr("style").find("li"),e.bindEvents()),e.utils.triggerCallback("Activate",e)},getClassNames:function(){var t=this,s=t.options.customClass,l={};return e.each("Input Items Open Disabled TempShow HideSelect Wrapper Focus Hover Responsive Above Below Scroll Group GroupLabel".split(" "),function(e,i){var n=s.prefix+i;l[i.toLowerCase()]=s.camelCase?n:t.utils.toDash(n)}),l.prefix=s.prefix,l},setLabel:function(){var t=this,s=t.options.labelBuilder;if(t.state.multiple){var l=e.isArray(t.state.currValue)?t.state.currValue:[t.state.currValue];l=0===l.length?[0]:l;var i=e.map(l,function(s){return e.grep(t.lookupItems,function(e){return e.index===s})[0]});i=e.grep(i,function(t){return i.length>1||0===i.length?""!==e.trim(t.value):t}),i=e.map(i,function(l){return e.isFunction(s)?s(l):t.utils.format(s,l)}),t.options.multiple.maxLabelEntries&&(i.length>=t.options.multiple.maxLabelEntries+1?(i=i.slice(0,t.options.multiple.maxLabelEntries),i.push(e.isFunction(s)?s({text:"..."}):t.utils.format(s,{text:"..."}))):i.slice(i.length-1)),t.elements.label.html(i.join(t.options.multiple.separator))}else{var n=t.lookupItems[t.state.currValue];t.elements.label.html(e.isFunction(s)?s(n):t.utils.format(s,n))}},populate:function(){var t=this,s=t.$element.children(),l=t.$element.find("option"),i=l.filter(":selected"),n=l.index(i),a=0,o=t.state.multiple?[]:0;i.length>1&&t.state.multiple&&(n=[],i.each(function(){n.push(e(this).index())})),t.state.currValue=~n?n:o,t.state.selectedIdx=t.state.currValue,t.state.highlightedIdx=t.state.currValue,t.items=[],t.lookupItems=[],s.length&&(s.each(function(s){var l=e(this);if(l.is("optgroup")){var i={element:l,label:l.prop("label"),groupDisabled:l.prop("disabled"),items:[]};l.children().each(function(s){var l=e(this);i.items[s]=t.getItemData(a,l,i.groupDisabled||l.prop("disabled")),t.lookupItems[a]=i.items[s],a++}),t.items[s]=i}else t.items[s]=t.getItemData(a,l,l.prop("disabled")),t.lookupItems[a]=t.items[s],a++}),t.setLabel(),t.elements.items.append(t.elements.itemsScroll.html(t.getItemsMarkup(t.items))))},getItemData:function(t,s,l){var i=this;return{index:t,element:s,value:s.val(),className:s.prop("class"),text:s.html(),slug:e.trim(i.utils.replaceDiacritics(s.html())),alt:s.attr("data-alt"),selected:s.prop("selected"),disabled:l}},getItemsMarkup:function(t){var s=this,l="<ul>";return e.isFunction(s.options.listBuilder)&&s.options.listBuilder&&(t=s.options.listBuilder(t)),e.each(t,function(t,i){void 0!==i.label?(l+=s.utils.format('<ul class="{1}"><li class="{2}">{3}</li>',s.utils.arrayToClassname([s.classes.group,i.groupDisabled?"disabled":"",i.element.prop("class")]),s.classes.grouplabel,i.element.prop("label")),e.each(i.items,function(e,t){l+=s.getItemMarkup(t.index,t)}),l+="</ul>"):l+=s.getItemMarkup(i.index,i)}),l+"</ul>"},getItemMarkup:function(t,s){var l=this,i=l.options.optionsItemBuilder,n={value:s.value,text:s.text,slug:s.slug,index:s.index};return l.utils.format('<li data-index="{1}" class="{2}">{3}</li>',t,l.utils.arrayToClassname([s.className,t===l.items.length-1?"last":"",s.disabled?"disabled":"",s.selected?"selected":""]),e.isFunction(i)?l.utils.format(i(s,this.$element,t),s):l.utils.format(i,n))},unbindEvents:function(){var e=this;e.elements.wrapper.add(e.$element).add(e.elements.outerWrapper).add(e.elements.input).off(".sl")},bindEvents:function(){var t=this;t.elements.outerWrapper.on("mouseenter.sl mouseleave.sl",function(s){e(this).toggleClass(t.classes.hover,"mouseenter"===s.type),t.options.openOnHover&&(clearTimeout(t.closeTimer),"mouseleave"===s.type?t.closeTimer=setTimeout(e.proxy(t.close,t),t.options.hoverIntentTimeout):t.open())}),t.elements.wrapper.on("click.sl",function(e){t.state.opened?t.close():t.open(e)}),t.options.nativeOnMobile&&t.utils.isMobile()||(t.$element.on("focus.sl",function(){t.elements.input.focus()}),t.elements.input.prop({tabindex:t.originalTabindex,disabled:!1}).on("keydown.sl",e.proxy(t.handleKeys,t)).on("focusin.sl",function(e){t.elements.outerWrapper.addClass(t.classes.focus),t.elements.input.one("blur",function(){t.elements.input.blur()}),t.options.openOnFocus&&!t.state.opened&&t.open(e)}).on("focusout.sl",function(){t.elements.outerWrapper.removeClass(t.classes.focus)}).on("input propertychange",function(){var s=t.elements.input.val(),l=new RegExp("^"+t.utils.escapeRegExp(s),"i");clearTimeout(t.resetStr),t.resetStr=setTimeout(function(){t.elements.input.val("")},t.options.keySearchTimeout),s.length&&e.each(t.items,function(e,s){if(!s.disabled){if(l.test(s.text)||l.test(s.slug))return void t.highlight(e);if(s.alt)for(var i=s.alt.split("|"),n=0;n<i.length&&i[n];n++)if(l.test(i[n].trim()))return void t.highlight(e)}})})),t.$li.on({mousedown:function(e){e.preventDefault(),e.stopPropagation()},click:function(){return t.select(e(this).data("index")),!1}})},handleKeys:function(t){var s=this,l=t.which,i=s.options.keys,n=e.inArray(l,i.previous)>-1,a=e.inArray(l,i.next)>-1,o=e.inArray(l,i.select)>-1,r=e.inArray(l,i.open)>-1,u=s.state.highlightedIdx,p=n&&0===u||a&&u+1===s.items.length,c=0;if(13!==l&&32!==l||t.preventDefault(),n||a){if(!s.options.allowWrap&&p)return;n&&(c=s.utils.previousEnabledItem(s.lookupItems,u)),a&&(c=s.utils.nextEnabledItem(s.lookupItems,u)),s.highlight(c)}if(o&&s.state.opened)return s.select(u),void(s.state.multiple&&s.options.multiple.keepMenuOpen||s.close());r&&!s.state.opened&&s.open()},refresh:function(){var e=this;e.populate(),e.activate(),e.utils.triggerCallback("Refresh",e)},setOptionsDimensions:function(){var e=this,t=e.elements.items.closest(":visible").children(":hidden").addClass(e.classes.tempshow),s=e.options.maxHeight,l=e.elements.items.outerWidth(),i=e.elements.wrapper.outerWidth()-(l-e.elements.items.width());!e.options.expandToItemText||i>l?e.finalWidth=i:(e.elements.items.css("overflow","scroll"),e.elements.outerWrapper.width(9e4),e.finalWidth=e.elements.items.width(),e.elements.items.css("overflow",""),e.elements.outerWrapper.width("")),e.elements.items.width(e.finalWidth).height()>s&&e.elements.items.height(s),t.removeClass(e.classes.tempshow)},isInViewport:function(){var e=this;if(!0===e.options.forceRenderAbove)e.elements.outerWrapper.addClass(e.classes.above);else if(!0===e.options.forceRenderBelow)e.elements.outerWrapper.addClass(e.classes.below);else{var t=s.scrollTop(),l=s.height(),i=e.elements.outerWrapper.offset().top,n=e.elements.outerWrapper.outerHeight(),a=i+n+e.itemsHeight<=t+l,o=i-e.itemsHeight>t,r=!a&&o,u=!r;e.elements.outerWrapper.toggleClass(e.classes.above,r),e.elements.outerWrapper.toggleClass(e.classes.below,u)}},detectItemVisibility:function(t){var s=this,l=s.$li.filter("[data-index]");s.state.multiple&&(t=e.isArray(t)&&0===t.length?0:t,t=e.isArray(t)?Math.min.apply(Math,t):t);var i=l.eq(t).outerHeight(),n=l[t].offsetTop,a=s.elements.itemsScroll.scrollTop(),o=n+2*i;s.elements.itemsScroll.scrollTop(o>a+s.itemsHeight?o-s.itemsHeight:n-i<a?n-i:a)},open:function(s){var l=this;if(l.options.nativeOnMobile&&l.utils.isMobile())return!1;l.utils.triggerCallback("BeforeOpen",l),s&&(s.preventDefault(),l.options.stopPropagation&&s.stopPropagation()),l.state.enabled&&(l.setOptionsDimensions(),e("."+l.classes.hideselect,"."+l.classes.open).children().selectric("close"),l.state.opened=!0,l.itemsHeight=l.elements.items.outerHeight(),l.itemsInnerHeight=l.elements.items.height(),l.elements.outerWrapper.addClass(l.classes.open),l.elements.input.val(""),s&&"focusin"!==s.type&&l.elements.input.focus(),setTimeout(function(){t.on("click.sl",e.proxy(l.close,l)).on("scroll.sl",e.proxy(l.isInViewport,l))},1),l.isInViewport(),l.options.preventWindowScroll&&t.on("mousewheel.sl DOMMouseScroll.sl","."+l.classes.scroll,function(t){var s=t.originalEvent,i=e(this).scrollTop(),n=0;"detail"in s&&(n=-1*s.detail),"wheelDelta"in s&&(n=s.wheelDelta),"wheelDeltaY"in s&&(n=s.wheelDeltaY),"deltaY"in s&&(n=-1*s.deltaY),(i===this.scrollHeight-l.itemsInnerHeight&&n<0||0===i&&n>0)&&t.preventDefault()}),l.detectItemVisibility(l.state.selectedIdx),l.highlight(l.state.multiple?-1:l.state.selectedIdx),l.utils.triggerCallback("Open",l))},close:function(){var e=this;e.utils.triggerCallback("BeforeClose",e),t.off(".sl"),e.elements.outerWrapper.removeClass(e.classes.open),e.state.opened=!1,e.utils.triggerCallback("Close",e)},change:function(){var t=this;t.utils.triggerCallback("BeforeChange",t),t.state.multiple?(e.each(t.lookupItems,function(e){t.lookupItems[e].selected=!1,t.$element.find("option").prop("selected",!1)}),e.each(t.state.selectedIdx,function(e,s){t.lookupItems[s].selected=!0,t.$element.find("option").eq(s).prop("selected",!0)}),t.state.currValue=t.state.selectedIdx,t.setLabel(),t.utils.triggerCallback("Change",t)):t.state.currValue!==t.state.selectedIdx&&(t.$element.prop("selectedIndex",t.state.currValue=t.state.selectedIdx).data("value",t.lookupItems[t.state.selectedIdx].text),t.setLabel(),t.utils.triggerCallback("Change",t))},highlight:function(e){var t=this,s=t.$li.filter("[data-index]").removeClass("highlighted");t.utils.triggerCallback("BeforeHighlight",t),void 0===e||-1===e||t.lookupItems[e].disabled||(s.eq(t.state.highlightedIdx=e).addClass("highlighted"),t.detectItemVisibility(e),t.utils.triggerCallback("Highlight",t))},select:function(t){var s=this,l=s.$li.filter("[data-index]");if(s.utils.triggerCallback("BeforeSelect",s,t),void 0!==t&&-1!==t&&!s.lookupItems[t].disabled){if(s.state.multiple){s.state.selectedIdx=e.isArray(s.state.selectedIdx)?s.state.selectedIdx:[s.state.selectedIdx];var i=e.inArray(t,s.state.selectedIdx);-1!==i?s.state.selectedIdx.splice(i,1):s.state.selectedIdx.push(t),l.removeClass("selected").filter(function(t){return-1!==e.inArray(t,s.state.selectedIdx)}).addClass("selected")}else l.removeClass("selected").eq(s.state.selectedIdx=t).addClass("selected");s.state.multiple&&s.options.multiple.keepMenuOpen||s.close(),s.change(),s.utils.triggerCallback("Select",s,t)}},destroy:function(e){var t=this;t.state&&t.state.enabled&&(t.elements.items.add(t.elements.wrapper).add(t.elements.input).remove(),e||t.$element.removeData("selectric").removeData("value"),t.$element.prop("tabindex",t.originalTabindex).off(".sl").off(t.eventTriggers).unwrap().unwrap(),t.state.enabled=!1)}},e.fn.selectric=function(t){return this.each(function(){var s=e.data(this,"selectric");s&&!s.disableOnMobile?"string"==typeof t&&s[t]?s[t]():s.init(t):e.data(this,"selectric",new n(this,t))})},e.fn.selectric.defaults={onChange:function(t){e(t).change()},maxHeight:300,keySearchTimeout:500,arrowButtonMarkup:'<b class="button">&#x25be;</b>',disableOnMobile:!1,nativeOnMobile:!0,openOnFocus:!0,openOnHover:!1,hoverIntentTimeout:500,expandToItemText:!1,responsive:!1,preventWindowScroll:!0,inheritOriginalWidth:!1,allowWrap:!0,forceRenderAbove:!1,forceRenderBelow:!1,stopPropagation:!0,optionsItemBuilder:"{text}",labelBuilder:"{text}",listBuilder:!1,keys:{previous:[37,38],next:[39,40],select:[9,13,27],open:[13,32,37,38,39,40],close:[9,27]},customClass:{prefix:"selectric",camelCase:!1},multiple:{separator:", ",keepMenuOpen:!0,maxLabelEntries:!1}}});
/*! js-cookie v3.0.0-beta.3 | MIT */
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):(e=e||self,function(){var n=e.Cookies,r=e.Cookies=t();r.noConflict=function(){return e.Cookies=n,r}}())}(this,function(){"use strict";var e={read:function(e){return e.replace(/(%[\dA-F]{2})+/gi,decodeURIComponent)},write:function(e){return encodeURIComponent(e).replace(/%(2[346BF]|3[AC-F]|40|5[BDE]|60|7[BCD])/g,decodeURIComponent)}};function t(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var r in n)e[r]=n[r]}return e}return function n(r,o){function i(e,n,i){if("undefined"!=typeof document){"number"==typeof(i=t({},o,i)).expires&&(i.expires=new Date(Date.now()+864e5*i.expires)),i.expires&&(i.expires=i.expires.toUTCString()),n=r.write(n,e),e=encodeURIComponent(e).replace(/%(2[346B]|5E|60|7C)/g,decodeURIComponent).replace(/[()]/g,escape);var c="";for(var u in i)i[u]&&(c+="; "+u,!0!==i[u]&&(c+="="+i[u].split(";")[0]));return document.cookie=e+"="+n+c}}return Object.create({set:i,get:function(t){if("undefined"!=typeof document&&(!arguments.length||t)){for(var n=document.cookie?document.cookie.split("; "):[],o={},i=0;i<n.length;i++){var c=n[i].split("="),u=c.slice(1).join("=");'"'===u[0]&&(u=u.slice(1,-1));try{var f=e.read(c[0]);if(o[f]=r.read(u,f),t===f)break}catch(e){}}return t?o[t]:o}},remove:function(e,n){i(e,"",t({},n,{expires:-1}))},withAttributes:function(e){return n(this.converter,t({},this.attributes,e))},withConverter:function(e){return n(t({},this.converter,e),this.attributes)}},{attributes:{value:Object.freeze(o)},converter:{value:Object.freeze(r)}})}(e,{path:"/"})});

!function(b,m){"use strict";(new function(){var s=function(e,n){return e.replace(/\{(\d+)\}/g,function(e,t){return n[t]||e})},u=function(e){return e.join(" - ")};this.i=function(){var e,t=m.querySelectorAll(".share-btn");for(e=t.length;e--;)n(t[e])};var n=function(e){var t,n=e.querySelectorAll("a");for(t=n.length;t--;)r(n[t],{id:"",url:c(e),title:i(e),desc:a(e)})},r=function(e,t){t.id=l(e,"data-id"),t.id&&o(e,"click",t)},c=function(e){return l(e,"data-url")||location.href||" "},i=function(e){return l(e,"data-title")||m.title||" "},a=function(e){var t=m.querySelector("meta[name=description]");return l(e,"data-desc")||t&&l(t,"content")||" "},o=function(e,t,n){var r=function(){p(n.id,n.url,n.title,n.desc)};e.addEventListener?e.addEventListener(t,r):e.attachEvent("on"+t,function(){r.call(e)})},l=function(e,t){return e.getAttribute(t)},h=function(e){return encodeURIComponent(e)},p=function(e,t,n,r){var c=h(t),i=h(r),a=h(n),o=a||i||"";switch(e){case"fb":d(s("https://www.facebook.com/sharer/sharer.php?u={0}",[c]),n);break;case"vk":d(s("https://vk.com/share.php?url={0}&title={1}",[c,u([a,i])]),n);break;case"tw":d(s("https://twitter.com/intent/tweet?url={0}&text={1}",[c,u([a,i])]),n);break;case"tg":d(s("https://t.me/share/url?url={0}&text={1}",[c,u([a,i])]),n);break;case"pk":d(s("https://getpocket.com/edit?url={0}&title={1}",[c,u([a,i])]),n);break;case"re":d(s("https://reddit.com/submit/?url={0}",[c]),n);break;case"ev":d(s("https://www.evernote.com/clip.action?url={0}&t={1}",[c,a]),n);break;case"in":d(s("https://www.linkedin.com/shareArticle?mini=true&url={0}&title={1}&summary={2}&source={0}",[c,a,u([a,i])]),n);break;case"pi":d(s("https://pinterest.com/pin/create/button/?url={0}&media={0}&description={1}",[c,u([a,i])]),n);break;case"sk":d(s("https://web.skype.com/share?url={0}&source=button&text={1}",[c,u([a,i])]),n);break;case"wa":d(s("whatsapp://send?text={0}%20{1}",[u([a,i]),c]),n);break;case"ok":d(s("https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&service=odnoklassniki&st.shareUrl={0}",[c]),n);break;case"tu":d(s("https://www.tumblr.com/widgets/share/tool?posttype=link&title={0}&caption={0}&content={1}&canonicalUrl={1}&shareSource=tumblr_share_button",[u([a,i]),c]),n);break;case"hn":d(s("https://news.ycombinator.com/submitlink?t={0}&u={1}",[u([a,i]),c]),n);break;case"xi":d(s("https://www.xing.com/app/user?op=share;url={0};title={1}",[c,u([a,i])]),n);break;case"mail":0<a.length&&0<i.length&&(o=u([a,i])),0<o.length&&(o+=" / "),0<a.length&&(a+=" / "),location.href=s("mailto:?Subject={0}{1}&body={2}{3}",[a,n,o,c]);break;case"print":window.print()}},d=function(e,t){var n=void 0!==b.screenLeft?b.screenLeft:screen.left,r=void 0!==b.screenTop?b.screenTop:screen.top,c=(b.innerWidth||m.documentElement.clientWidth||screen.width)/2-300+n,i=(b.innerHeight||m.documentElement.clientHeight||screen.height)/3-400/3+r,a=b.open(e,"",s("resizable,toolbar=yes,location=yes,scrollbars=yes,menubar=yes,width={0},height={1},top={2},left={3}",[600,400,i,c]));null!==a&&a.focus&&a.focus()}}).i()}(window,document);
/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.8.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
/* global window, document, define, jQuery, setInterval, clearInterval */
;(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {
    'use strict';
    var Slick = window.Slick || {};

    Slick = (function() {

        var instanceUid = 0;

        function Slick(element, settings) {

            var _ = this, dataSettings;

            _.defaults = {
                accessibility: true,
                adaptiveHeight: false,
                appendArrows: $(element),
                appendDots: $(element),
                arrows: true,
                asNavFor: null,
                prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
                nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
                autoplay: false,
                autoplaySpeed: 3000,
                centerMode: false,
                centerPadding: '50px',
                cssEase: 'ease',
                customPaging: function(slider, i) {
                    return $('<button type="button" />').text(i + 1);
                },
                dots: false,
                dotsClass: 'slick-dots',
                draggable: true,
                easing: 'linear',
                edgeFriction: 0.35,
                fade: false,
                focusOnSelect: false,
                focusOnChange: false,
                infinite: true,
                initialSlide: 0,
                lazyLoad: 'ondemand',
                mobileFirst: false,
                pauseOnHover: true,
                pauseOnFocus: true,
                pauseOnDotsHover: false,
                respondTo: 'window',
                responsive: null,
                rows: 1,
                rtl: false,
                slide: '',
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: true,
                swipeToSlide: false,
                touchMove: true,
                touchThreshold: 5,
                useCSS: true,
                useTransform: true,
                variableWidth: false,
                vertical: false,
                verticalSwiping: false,
                waitForAnimate: true,
                zIndex: 1000
            };

            _.initials = {
                animating: false,
                dragging: false,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                scrolling: false,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: false,
                slideOffset: 0,
                swipeLeft: null,
                swiping: false,
                $list: null,
                touchObject: {},
                transformsEnabled: false,
                unslicked: false
            };

            $.extend(_, _.initials);

            _.activeBreakpoint = null;
            _.animType = null;
            _.animProp = null;
            _.breakpoints = [];
            _.breakpointSettings = [];
            _.cssTransitions = false;
            _.focussed = false;
            _.interrupted = false;
            _.hidden = 'hidden';
            _.paused = true;
            _.positionProp = null;
            _.respondTo = null;
            _.rowCount = 1;
            _.shouldClick = true;
            _.$slider = $(element);
            _.$slidesCache = null;
            _.transformType = null;
            _.transitionType = null;
            _.visibilityChange = 'visibilitychange';
            _.windowWidth = 0;
            _.windowTimer = null;

            dataSettings = $(element).data('slick') || {};

            _.options = $.extend({}, _.defaults, settings, dataSettings);

            _.currentSlide = _.options.initialSlide;

            _.originalSettings = _.options;

            if (typeof document.mozHidden !== 'undefined') {
                _.hidden = 'mozHidden';
                _.visibilityChange = 'mozvisibilitychange';
            } else if (typeof document.webkitHidden !== 'undefined') {
                _.hidden = 'webkitHidden';
                _.visibilityChange = 'webkitvisibilitychange';
            }

            _.autoPlay = $.proxy(_.autoPlay, _);
            _.autoPlayClear = $.proxy(_.autoPlayClear, _);
            _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);
            _.changeSlide = $.proxy(_.changeSlide, _);
            _.clickHandler = $.proxy(_.clickHandler, _);
            _.selectHandler = $.proxy(_.selectHandler, _);
            _.setPosition = $.proxy(_.setPosition, _);
            _.swipeHandler = $.proxy(_.swipeHandler, _);
            _.dragHandler = $.proxy(_.dragHandler, _);
            _.keyHandler = $.proxy(_.keyHandler, _);

            _.instanceUid = instanceUid++;

            // A simple way to check for HTML strings
            // Strict HTML recognition (must start with <)
            // Extracted from jQuery v1.11 source
            _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;


            _.registerBreakpoints();
            _.init(true);

        }

        return Slick;

    }());

    Slick.prototype.activateADA = function() {
        var _ = this;

        _.$slideTrack.find('.slick-active').attr({
            'aria-hidden': 'false'
        }).find('a, input, button, select').attr({
            'tabindex': '0'
        });

    };

    Slick.prototype.addSlide = Slick.prototype.slickAdd = function(markup, index, addBefore) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            addBefore = index;
            index = null;
        } else if (index < 0 || (index >= _.slideCount)) {
            return false;
        }

        _.unload();

        if (typeof(index) === 'number') {
            if (index === 0 && _.$slides.length === 0) {
                $(markup).appendTo(_.$slideTrack);
            } else if (addBefore) {
                $(markup).insertBefore(_.$slides.eq(index));
            } else {
                $(markup).insertAfter(_.$slides.eq(index));
            }
        } else {
            if (addBefore === true) {
                $(markup).prependTo(_.$slideTrack);
            } else {
                $(markup).appendTo(_.$slideTrack);
            }
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).detach();

        _.$slideTrack.append(_.$slides);

        _.$slides.each(function(index, element) {
            $(element).attr('data-slick-index', index);
        });

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.animateHeight = function() {
        var _ = this;
        if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
            _.$list.animate({
                height: targetHeight
            }, _.options.speed);
        }
    };

    Slick.prototype.animateSlide = function(targetLeft, callback) {

        var animProps = {},
            _ = this;

        _.animateHeight();

        if (_.options.rtl === true && _.options.vertical === false) {
            targetLeft = -targetLeft;
        }
        if (_.transformsEnabled === false) {
            if (_.options.vertical === false) {
                _.$slideTrack.animate({
                    left: targetLeft
                }, _.options.speed, _.options.easing, callback);
            } else {
                _.$slideTrack.animate({
                    top: targetLeft
                }, _.options.speed, _.options.easing, callback);
            }

        } else {

            if (_.cssTransitions === false) {
                if (_.options.rtl === true) {
                    _.currentLeft = -(_.currentLeft);
                }
                $({
                    animStart: _.currentLeft
                }).animate({
                    animStart: targetLeft
                }, {
                    duration: _.options.speed,
                    easing: _.options.easing,
                    step: function(now) {
                        now = Math.ceil(now);
                        if (_.options.vertical === false) {
                            animProps[_.animType] = 'translate(' +
                                now + 'px, 0px)';
                            _.$slideTrack.css(animProps);
                        } else {
                            animProps[_.animType] = 'translate(0px,' +
                                now + 'px)';
                            _.$slideTrack.css(animProps);
                        }
                    },
                    complete: function() {
                        if (callback) {
                            callback.call();
                        }
                    }
                });

            } else {

                _.applyTransition();
                targetLeft = Math.ceil(targetLeft);

                if (_.options.vertical === false) {
                    animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
                } else {
                    animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
                }
                _.$slideTrack.css(animProps);

                if (callback) {
                    setTimeout(function() {

                        _.disableTransition();

                        callback.call();
                    }, _.options.speed);
                }

            }

        }

    };

    Slick.prototype.getNavTarget = function() {

        var _ = this,
            asNavFor = _.options.asNavFor;

        if ( asNavFor && asNavFor !== null ) {
            asNavFor = $(asNavFor).not(_.$slider);
        }

        return asNavFor;

    };

    Slick.prototype.asNavFor = function(index) {

        var _ = this,
            asNavFor = _.getNavTarget();

        if ( asNavFor !== null && typeof asNavFor === 'object' ) {
            asNavFor.each(function() {
                var target = $(this).slick('getSlick');
                if(!target.unslicked) {
                    target.slideHandler(index, true);
                }
            });
        }

    };

    Slick.prototype.applyTransition = function(slide) {

        var _ = this,
            transition = {};

        if (_.options.fade === false) {
            transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
        } else {
            transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
        }

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.autoPlay = function() {

        var _ = this;

        _.autoPlayClear();

        if ( _.slideCount > _.options.slidesToShow ) {
            _.autoPlayTimer = setInterval( _.autoPlayIterator, _.options.autoplaySpeed );
        }

    };

    Slick.prototype.autoPlayClear = function() {

        var _ = this;

        if (_.autoPlayTimer) {
            clearInterval(_.autoPlayTimer);
        }

    };

    Slick.prototype.autoPlayIterator = function() {

        var _ = this,
            slideTo = _.currentSlide + _.options.slidesToScroll;

        if ( !_.paused && !_.interrupted && !_.focussed ) {

            if ( _.options.infinite === false ) {

                if ( _.direction === 1 && ( _.currentSlide + 1 ) === ( _.slideCount - 1 )) {
                    _.direction = 0;
                }

                else if ( _.direction === 0 ) {

                    slideTo = _.currentSlide - _.options.slidesToScroll;

                    if ( _.currentSlide - 1 === 0 ) {
                        _.direction = 1;
                    }

                }

            }

            _.slideHandler( slideTo );

        }

    };

    Slick.prototype.buildArrows = function() {

        var _ = this;

        if (_.options.arrows === true ) {

            _.$prevArrow = $(_.options.prevArrow).addClass('slick-arrow');
            _.$nextArrow = $(_.options.nextArrow).addClass('slick-arrow');

            if( _.slideCount > _.options.slidesToShow ) {

                _.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
                _.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

                if (_.htmlExpr.test(_.options.prevArrow)) {
                    _.$prevArrow.prependTo(_.options.appendArrows);
                }

                if (_.htmlExpr.test(_.options.nextArrow)) {
                    _.$nextArrow.appendTo(_.options.appendArrows);
                }

                if (_.options.infinite !== true) {
                    _.$prevArrow
                        .addClass('slick-disabled')
                        .attr('aria-disabled', 'true');
                }

            } else {

                _.$prevArrow.add( _.$nextArrow )

                    .addClass('slick-hidden')
                    .attr({
                        'aria-disabled': 'true',
                        'tabindex': '-1'
                    });

            }

        }

    };

    Slick.prototype.buildDots = function() {

        var _ = this,
            i, dot;

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$slider.addClass('slick-dotted');

            dot = $('<ul />').addClass(_.options.dotsClass);

            for (i = 0; i <= _.getDotCount(); i += 1) {
                dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
            }

            _.$dots = dot.appendTo(_.options.appendDots);

            _.$dots.find('li').first().addClass('slick-active');

        }

    };

    Slick.prototype.buildOut = function() {

        var _ = this;

        _.$slides =
            _.$slider
                .children( _.options.slide + ':not(.slick-cloned)')
                .addClass('slick-slide');

        _.slideCount = _.$slides.length;

        _.$slides.each(function(index, element) {
            $(element)
                .attr('data-slick-index', index)
                .data('originalStyling', $(element).attr('style') || '');
        });

        _.$slider.addClass('slick-slider');

        _.$slideTrack = (_.slideCount === 0) ?
            $('<div class="slick-track"/>').appendTo(_.$slider) :
            _.$slides.wrapAll('<div class="slick-track"/>').parent();

        _.$list = _.$slideTrack.wrap(
            '<div class="slick-list"/>').parent();
        _.$slideTrack.css('opacity', 0);

        if (_.options.centerMode === true || _.options.swipeToSlide === true) {
            _.options.slidesToScroll = 1;
        }

        $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

        _.setupInfinite();

        _.buildArrows();

        _.buildDots();

        _.updateDots();


        _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

        if (_.options.draggable === true) {
            _.$list.addClass('draggable');
        }

    };

    Slick.prototype.buildRows = function() {

        var _ = this, a, b, c, newSlides, numOfSlides, originalSlides,slidesPerSection;

        newSlides = document.createDocumentFragment();
        originalSlides = _.$slider.children();

        if(_.options.rows > 0) {

            slidesPerSection = _.options.slidesPerRow * _.options.rows;
            numOfSlides = Math.ceil(
                originalSlides.length / slidesPerSection
            );

            for(a = 0; a < numOfSlides; a++){
                var slide = document.createElement('div');
                for(b = 0; b < _.options.rows; b++) {
                    var row = document.createElement('div');
                    for(c = 0; c < _.options.slidesPerRow; c++) {
                        var target = (a * slidesPerSection + ((b * _.options.slidesPerRow) + c));
                        if (originalSlides.get(target)) {
                            row.appendChild(originalSlides.get(target));
                        }
                    }
                    slide.appendChild(row);
                }
                newSlides.appendChild(slide);
            }

            _.$slider.empty().append(newSlides);
            _.$slider.children().children().children()
                .css({
                    'width':(100 / _.options.slidesPerRow) + '%',
                    'display': 'inline-block'
                });

        }

    };

    Slick.prototype.checkResponsive = function(initial, forceUpdate) {

        var _ = this,
            breakpoint, targetBreakpoint, respondToWidth, triggerBreakpoint = false;
        var sliderWidth = _.$slider.width();
        var windowWidth = window.innerWidth || $(window).width();

        if (_.respondTo === 'window') {
            respondToWidth = windowWidth;
        } else if (_.respondTo === 'slider') {
            respondToWidth = sliderWidth;
        } else if (_.respondTo === 'min') {
            respondToWidth = Math.min(windowWidth, sliderWidth);
        }

        if ( _.options.responsive &&
            _.options.responsive.length &&
            _.options.responsive !== null) {

            targetBreakpoint = null;

            for (breakpoint in _.breakpoints) {
                if (_.breakpoints.hasOwnProperty(breakpoint)) {
                    if (_.originalSettings.mobileFirst === false) {
                        if (respondToWidth < _.breakpoints[breakpoint]) {
                            targetBreakpoint = _.breakpoints[breakpoint];
                        }
                    } else {
                        if (respondToWidth > _.breakpoints[breakpoint]) {
                            targetBreakpoint = _.breakpoints[breakpoint];
                        }
                    }
                }
            }

            if (targetBreakpoint !== null) {
                if (_.activeBreakpoint !== null) {
                    if (targetBreakpoint !== _.activeBreakpoint || forceUpdate) {
                        _.activeBreakpoint =
                            targetBreakpoint;
                        if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
                            _.unslick(targetBreakpoint);
                        } else {
                            _.options = $.extend({}, _.originalSettings,
                                _.breakpointSettings[
                                    targetBreakpoint]);
                            if (initial === true) {
                                _.currentSlide = _.options.initialSlide;
                            }
                            _.refresh(initial);
                        }
                        triggerBreakpoint = targetBreakpoint;
                    }
                } else {
                    _.activeBreakpoint = targetBreakpoint;
                    if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
                        _.unslick(targetBreakpoint);
                    } else {
                        _.options = $.extend({}, _.originalSettings,
                            _.breakpointSettings[
                                targetBreakpoint]);
                        if (initial === true) {
                            _.currentSlide = _.options.initialSlide;
                        }
                        _.refresh(initial);
                    }
                    triggerBreakpoint = targetBreakpoint;
                }
            } else {
                if (_.activeBreakpoint !== null) {
                    _.activeBreakpoint = null;
                    _.options = _.originalSettings;
                    if (initial === true) {
                        _.currentSlide = _.options.initialSlide;
                    }
                    _.refresh(initial);
                    triggerBreakpoint = targetBreakpoint;
                }
            }

            // only trigger breakpoints during an actual break. not on initialize.
            if( !initial && triggerBreakpoint !== false ) {
                _.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
            }
        }

    };

    Slick.prototype.changeSlide = function(event, dontAnimate) {

        var _ = this,
            $target = $(event.currentTarget),
            indexOffset, slideOffset, unevenOffset;

        // If target is a link, prevent default action.
        if($target.is('a')) {
            event.preventDefault();
        }

        // If target is not the <li> element (ie: a child), find the <li>.
        if(!$target.is('li')) {
            $target = $target.closest('li');
        }

        unevenOffset = (_.slideCount % _.options.slidesToScroll !== 0);
        indexOffset = unevenOffset ? 0 : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

        switch (event.data.message) {

            case 'previous':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : _.options.slidesToShow - indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                    _.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
                }
                break;

            case 'next':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                    _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
                }
                break;

            case 'index':
                var index = event.data.index === 0 ? 0 :
                    event.data.index || $target.index() * _.options.slidesToScroll;

                _.slideHandler(_.checkNavigable(index), false, dontAnimate);
                $target.children().trigger('focus');
                break;

            default:
                return;
        }

    };

    Slick.prototype.checkNavigable = function(index) {

        var _ = this,
            navigables, prevNavigable;

        navigables = _.getNavigableIndexes();
        prevNavigable = 0;
        if (index > navigables[navigables.length - 1]) {
            index = navigables[navigables.length - 1];
        } else {
            for (var n in navigables) {
                if (index < navigables[n]) {
                    index = prevNavigable;
                    break;
                }
                prevNavigable = navigables[n];
            }
        }

        return index;
    };

    Slick.prototype.cleanUpEvents = function() {

        var _ = this;

        if (_.options.dots && _.$dots !== null) {

            $('li', _.$dots)
                .off('click.slick', _.changeSlide)
                .off('mouseenter.slick', $.proxy(_.interrupt, _, true))
                .off('mouseleave.slick', $.proxy(_.interrupt, _, false));

            if (_.options.accessibility === true) {
                _.$dots.off('keydown.slick', _.keyHandler);
            }
        }

        _.$slider.off('focus.slick blur.slick');

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow && _.$prevArrow.off('click.slick', _.changeSlide);
            _.$nextArrow && _.$nextArrow.off('click.slick', _.changeSlide);

            if (_.options.accessibility === true) {
                _.$prevArrow && _.$prevArrow.off('keydown.slick', _.keyHandler);
                _.$nextArrow && _.$nextArrow.off('keydown.slick', _.keyHandler);
            }
        }

        _.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);
        _.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);
        _.$list.off('touchend.slick mouseup.slick', _.swipeHandler);
        _.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);

        _.$list.off('click.slick', _.clickHandler);

        $(document).off(_.visibilityChange, _.visibility);

        _.cleanUpSlideEvents();

        if (_.options.accessibility === true) {
            _.$list.off('keydown.slick', _.keyHandler);
        }

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().off('click.slick', _.selectHandler);
        }

        $(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);

        $(window).off('resize.slick.slick-' + _.instanceUid, _.resize);

        $('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);

        $(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);

    };

    Slick.prototype.cleanUpSlideEvents = function() {

        var _ = this;

        _.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));
        _.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));

    };

    Slick.prototype.cleanUpRows = function() {

        var _ = this, originalSlides;

        if(_.options.rows > 0) {
            originalSlides = _.$slides.children().children();
            originalSlides.removeAttr('style');
            _.$slider.empty().append(originalSlides);
        }

    };

    Slick.prototype.clickHandler = function(event) {

        var _ = this;

        if (_.shouldClick === false) {
            event.stopImmediatePropagation();
            event.stopPropagation();
            event.preventDefault();
        }

    };

    Slick.prototype.destroy = function(refresh) {

        var _ = this;

        _.autoPlayClear();

        _.touchObject = {};

        _.cleanUpEvents();

        $('.slick-cloned', _.$slider).detach();

        if (_.$dots) {
            _.$dots.remove();
        }

        if ( _.$prevArrow && _.$prevArrow.length ) {

            _.$prevArrow
                .removeClass('slick-disabled slick-arrow slick-hidden')
                .removeAttr('aria-hidden aria-disabled tabindex')
                .css('display','');

            if ( _.htmlExpr.test( _.options.prevArrow )) {
                _.$prevArrow.remove();
            }
        }

        if ( _.$nextArrow && _.$nextArrow.length ) {

            _.$nextArrow
                .removeClass('slick-disabled slick-arrow slick-hidden')
                .removeAttr('aria-hidden aria-disabled tabindex')
                .css('display','');

            if ( _.htmlExpr.test( _.options.nextArrow )) {
                _.$nextArrow.remove();
            }
        }


        if (_.$slides) {

            _.$slides
                .removeClass('slick-slide slick-active slick-center slick-visible slick-current')
                .removeAttr('aria-hidden')
                .removeAttr('data-slick-index')
                .each(function(){
                    $(this).attr('style', $(this).data('originalStyling'));
                });

            _.$slideTrack.children(this.options.slide).detach();

            _.$slideTrack.detach();

            _.$list.detach();

            _.$slider.append(_.$slides);
        }

        _.cleanUpRows();

        _.$slider.removeClass('slick-slider');
        _.$slider.removeClass('slick-initialized');
        _.$slider.removeClass('slick-dotted');

        _.unslicked = true;

        if(!refresh) {
            _.$slider.trigger('destroy', [_]);
        }

    };

    Slick.prototype.disableTransition = function(slide) {

        var _ = this,
            transition = {};

        transition[_.transitionType] = '';

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.fadeSlide = function(slideIndex, callback) {

        var _ = this;

        if (_.cssTransitions === false) {

            _.$slides.eq(slideIndex).css({
                zIndex: _.options.zIndex
            });

            _.$slides.eq(slideIndex).animate({
                opacity: 1
            }, _.options.speed, _.options.easing, callback);

        } else {

            _.applyTransition(slideIndex);

            _.$slides.eq(slideIndex).css({
                opacity: 1,
                zIndex: _.options.zIndex
            });

            if (callback) {
                setTimeout(function() {

                    _.disableTransition(slideIndex);

                    callback.call();
                }, _.options.speed);
            }

        }

    };

    Slick.prototype.fadeSlideOut = function(slideIndex) {

        var _ = this;

        if (_.cssTransitions === false) {

            _.$slides.eq(slideIndex).animate({
                opacity: 0,
                zIndex: _.options.zIndex - 2
            }, _.options.speed, _.options.easing);

        } else {

            _.applyTransition(slideIndex);

            _.$slides.eq(slideIndex).css({
                opacity: 0,
                zIndex: _.options.zIndex - 2
            });

        }

    };

    Slick.prototype.filterSlides = Slick.prototype.slickFilter = function(filter) {

        var _ = this;

        if (filter !== null) {

            _.$slidesCache = _.$slides;

            _.unload();

            _.$slideTrack.children(this.options.slide).detach();

            _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.focusHandler = function() {

        var _ = this;

        _.$slider
            .off('focus.slick blur.slick')
            .on('focus.slick blur.slick', '*', function(event) {

            event.stopImmediatePropagation();
            var $sf = $(this);

            setTimeout(function() {

                if( _.options.pauseOnFocus ) {
                    _.focussed = $sf.is(':focus');
                    _.autoPlay();
                }

            }, 0);

        });
    };

    Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function() {

        var _ = this;
        return _.currentSlide;

    };

    Slick.prototype.getDotCount = function() {

        var _ = this;

        var breakPoint = 0;
        var counter = 0;
        var pagerQty = 0;

        if (_.options.infinite === true) {
            if (_.slideCount <= _.options.slidesToShow) {
                 ++pagerQty;
            } else {
                while (breakPoint < _.slideCount) {
                    ++pagerQty;
                    breakPoint = counter + _.options.slidesToScroll;
                    counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
                }
            }
        } else if (_.options.centerMode === true) {
            pagerQty = _.slideCount;
        } else if(!_.options.asNavFor) {
            pagerQty = 1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
        }else {
            while (breakPoint < _.slideCount) {
                ++pagerQty;
                breakPoint = counter + _.options.slidesToScroll;
                counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
            }
        }

        return pagerQty - 1;

    };

    Slick.prototype.getLeft = function(slideIndex) {

        var _ = this,
            targetLeft,
            verticalHeight,
            verticalOffset = 0,
            targetSlide,
            coef;

        _.slideOffset = 0;
        verticalHeight = _.$slides.first().outerHeight(true);

        if (_.options.infinite === true) {
            if (_.slideCount > _.options.slidesToShow) {
                _.slideOffset = (_.slideWidth * _.options.slidesToShow) * -1;
                coef = -1

                if (_.options.vertical === true && _.options.centerMode === true) {
                    if (_.options.slidesToShow === 2) {
                        coef = -1.5;
                    } else if (_.options.slidesToShow === 1) {
                        coef = -2
                    }
                }
                verticalOffset = (verticalHeight * _.options.slidesToShow) * coef;
            }
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
                    if (slideIndex > _.slideCount) {
                        _.slideOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth) * -1;
                        verticalOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight) * -1;
                    } else {
                        _.slideOffset = ((_.slideCount % _.options.slidesToScroll) * _.slideWidth) * -1;
                        verticalOffset = ((_.slideCount % _.options.slidesToScroll) * verticalHeight) * -1;
                    }
                }
            }
        } else {
            if (slideIndex + _.options.slidesToShow > _.slideCount) {
                _.slideOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * _.slideWidth;
                verticalOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * verticalHeight;
            }
        }

        if (_.slideCount <= _.options.slidesToShow) {
            _.slideOffset = 0;
            verticalOffset = 0;
        }

        if (_.options.centerMode === true && _.slideCount <= _.options.slidesToShow) {
            _.slideOffset = ((_.slideWidth * Math.floor(_.options.slidesToShow)) / 2) - ((_.slideWidth * _.slideCount) / 2);
        } else if (_.options.centerMode === true && _.options.infinite === true) {
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
        } else if (_.options.centerMode === true) {
            _.slideOffset = 0;
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
        }

        if (_.options.vertical === false) {
            targetLeft = ((slideIndex * _.slideWidth) * -1) + _.slideOffset;
        } else {
            targetLeft = ((slideIndex * verticalHeight) * -1) + verticalOffset;
        }

        if (_.options.variableWidth === true) {

            if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
            } else {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
            }

            if (_.options.rtl === true) {
                if (targetSlide[0]) {
                    targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
                } else {
                    targetLeft =  0;
                }
            } else {
                targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
            }

            if (_.options.centerMode === true) {
                if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                    targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
                } else {
                    targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
                }

                if (_.options.rtl === true) {
                    if (targetSlide[0]) {
                        targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
                    } else {
                        targetLeft =  0;
                    }
                } else {
                    targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
                }

                targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
            }
        }

        return targetLeft;

    };

    Slick.prototype.getOption = Slick.prototype.slickGetOption = function(option) {

        var _ = this;

        return _.options[option];

    };

    Slick.prototype.getNavigableIndexes = function() {

        var _ = this,
            breakPoint = 0,
            counter = 0,
            indexes = [],
            max;

        if (_.options.infinite === false) {
            max = _.slideCount;
        } else {
            breakPoint = _.options.slidesToScroll * -1;
            counter = _.options.slidesToScroll * -1;
            max = _.slideCount * 2;
        }

        while (breakPoint < max) {
            indexes.push(breakPoint);
            breakPoint = counter + _.options.slidesToScroll;
            counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
        }

        return indexes;

    };

    Slick.prototype.getSlick = function() {

        return this;

    };

    Slick.prototype.getSlideCount = function() {

        var _ = this,
            slidesTraversed, swipedSlide, centerOffset;

        centerOffset = _.options.centerMode === true ? _.slideWidth * Math.floor(_.options.slidesToShow / 2) : 0;

        if (_.options.swipeToSlide === true) {
            _.$slideTrack.find('.slick-slide').each(function(index, slide) {
                if (slide.offsetLeft - centerOffset + ($(slide).outerWidth() / 2) > (_.swipeLeft * -1)) {
                    swipedSlide = slide;
                    return false;
                }
            });

            slidesTraversed = Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide) || 1;

            return slidesTraversed;

        } else {
            return _.options.slidesToScroll;
        }

    };

    Slick.prototype.goTo = Slick.prototype.slickGoTo = function(slide, dontAnimate) {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'index',
                index: parseInt(slide)
            }
        }, dontAnimate);

    };

    Slick.prototype.init = function(creation) {

        var _ = this;

        if (!$(_.$slider).hasClass('slick-initialized')) {

            $(_.$slider).addClass('slick-initialized');

            _.buildRows();
            _.buildOut();
            _.setProps();
            _.startLoad();
            _.loadSlider();
            _.initializeEvents();
            _.updateArrows();
            _.updateDots();
            _.checkResponsive(true);
            _.focusHandler();

        }

        if (creation) {
            _.$slider.trigger('init', [_]);
        }

        if (_.options.accessibility === true) {
            _.initADA();
        }

        if ( _.options.autoplay ) {

            _.paused = false;
            _.autoPlay();

        }

    };

    Slick.prototype.initADA = function() {
        var _ = this,
                numDotGroups = Math.ceil(_.slideCount / _.options.slidesToShow),
                tabControlIndexes = _.getNavigableIndexes().filter(function(val) {
                    return (val >= 0) && (val < _.slideCount);
                });

        _.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
            'aria-hidden': 'true',
            'tabindex': '-1'
        }).find('a, input, button, select').attr({
            'tabindex': '-1'
        });

        if (_.$dots !== null) {
            _.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function(i) {
                var slideControlIndex = tabControlIndexes.indexOf(i);

                $(this).attr({
                    'role': 'tabpanel',
                    'id': 'slick-slide' + _.instanceUid + i,
                    'tabindex': -1
                });

                if (slideControlIndex !== -1) {
                   var ariaButtonControl = 'slick-slide-control' + _.instanceUid + slideControlIndex
                   if ($('#' + ariaButtonControl).length) {
                     $(this).attr({
                         'aria-describedby': ariaButtonControl
                     });
                   }
                }
            });

            _.$dots.attr('role', 'tablist').find('li').each(function(i) {
                var mappedSlideIndex = tabControlIndexes[i];

                $(this).attr({
                    'role': 'presentation'
                });

                $(this).find('button').first().attr({
                    'role': 'tab',
                    'id': 'slick-slide-control' + _.instanceUid + i,
                    'aria-controls': 'slick-slide' + _.instanceUid + mappedSlideIndex,
                    'aria-label': (i + 1) + ' of ' + numDotGroups,
                    'aria-selected': null,
                    'tabindex': '-1'
                });

            }).eq(_.currentSlide).find('button').attr({
                'aria-selected': 'true',
                'tabindex': '0'
            }).end();
        }

        for (var i=_.currentSlide, max=i+_.options.slidesToShow; i < max; i++) {
          if (_.options.focusOnChange) {
            _.$slides.eq(i).attr({'tabindex': '0'});
          } else {
            _.$slides.eq(i).removeAttr('tabindex');
          }
        }

        _.activateADA();

    };

    Slick.prototype.initArrowEvents = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow
               .off('click.slick')
               .on('click.slick', {
                    message: 'previous'
               }, _.changeSlide);
            _.$nextArrow
               .off('click.slick')
               .on('click.slick', {
                    message: 'next'
               }, _.changeSlide);

            if (_.options.accessibility === true) {
                _.$prevArrow.on('keydown.slick', _.keyHandler);
                _.$nextArrow.on('keydown.slick', _.keyHandler);
            }
        }

    };

    Slick.prototype.initDotEvents = function() {

        var _ = this;

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
            $('li', _.$dots).on('click.slick', {
                message: 'index'
            }, _.changeSlide);

            if (_.options.accessibility === true) {
                _.$dots.on('keydown.slick', _.keyHandler);
            }
        }

        if (_.options.dots === true && _.options.pauseOnDotsHover === true && _.slideCount > _.options.slidesToShow) {

            $('li', _.$dots)
                .on('mouseenter.slick', $.proxy(_.interrupt, _, true))
                .on('mouseleave.slick', $.proxy(_.interrupt, _, false));

        }

    };

    Slick.prototype.initSlideEvents = function() {

        var _ = this;

        if ( _.options.pauseOnHover ) {

            _.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));
            _.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));

        }

    };

    Slick.prototype.initializeEvents = function() {

        var _ = this;

        _.initArrowEvents();

        _.initDotEvents();
        _.initSlideEvents();

        _.$list.on('touchstart.slick mousedown.slick', {
            action: 'start'
        }, _.swipeHandler);
        _.$list.on('touchmove.slick mousemove.slick', {
            action: 'move'
        }, _.swipeHandler);
        _.$list.on('touchend.slick mouseup.slick', {
            action: 'end'
        }, _.swipeHandler);
        _.$list.on('touchcancel.slick mouseleave.slick', {
            action: 'end'
        }, _.swipeHandler);

        _.$list.on('click.slick', _.clickHandler);

        $(document).on(_.visibilityChange, $.proxy(_.visibility, _));

        if (_.options.accessibility === true) {
            _.$list.on('keydown.slick', _.keyHandler);
        }

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().on('click.slick', _.selectHandler);
        }

        $(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));

        $(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));

        $('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);

        $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
        $(_.setPosition);

    };

    Slick.prototype.initUI = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.show();
            _.$nextArrow.show();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.show();

        }

    };

    Slick.prototype.keyHandler = function(event) {

        var _ = this;
         //Dont slide if the cursor is inside the form fields and arrow keys are pressed
        if(!event.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
            if (event.keyCode === 37 && _.options.accessibility === true) {
                _.changeSlide({
                    data: {
                        message: _.options.rtl === true ? 'next' :  'previous'
                    }
                });
            } else if (event.keyCode === 39 && _.options.accessibility === true) {
                _.changeSlide({
                    data: {
                        message: _.options.rtl === true ? 'previous' : 'next'
                    }
                });
            }
        }

    };

    Slick.prototype.lazyLoad = function() {

        var _ = this,
            loadRange, cloneRange, rangeStart, rangeEnd;

        function loadImages(imagesScope) {

            $('img[data-lazy]', imagesScope).each(function() {

                var image = $(this),
                    imageSource = $(this).attr('data-lazy'),
                    imageSrcSet = $(this).attr('data-srcset'),
                    imageSizes  = $(this).attr('data-sizes') || _.$slider.attr('data-sizes'),
                    imageToLoad = document.createElement('img');

                imageToLoad.onload = function() {

                    image
                        .animate({ opacity: 0 }, 100, function() {

                            if (imageSrcSet) {
                                image
                                    .attr('srcset', imageSrcSet );

                                if (imageSizes) {
                                    image
                                        .attr('sizes', imageSizes );
                                }
                            }

                            image
                                .attr('src', imageSource)
                                .animate({ opacity: 1 }, 200, function() {
                                    image
                                        .removeAttr('data-lazy data-srcset data-sizes')
                                        .removeClass('slick-loading');
                                });
                            _.$slider.trigger('lazyLoaded', [_, image, imageSource]);
                        });

                };

                imageToLoad.onerror = function() {

                    image
                        .removeAttr( 'data-lazy' )
                        .removeClass( 'slick-loading' )
                        .addClass( 'slick-lazyload-error' );

                    _.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);

                };

                imageToLoad.src = imageSource;

            });

        }

        if (_.options.centerMode === true) {
            if (_.options.infinite === true) {
                rangeStart = _.currentSlide + (_.options.slidesToShow / 2 + 1);
                rangeEnd = rangeStart + _.options.slidesToShow + 2;
            } else {
                rangeStart = Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
                rangeEnd = 2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
            }
        } else {
            rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
            rangeEnd = Math.ceil(rangeStart + _.options.slidesToShow);
            if (_.options.fade === true) {
                if (rangeStart > 0) rangeStart--;
                if (rangeEnd <= _.slideCount) rangeEnd++;
            }
        }

        loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);

        if (_.options.lazyLoad === 'anticipated') {
            var prevSlide = rangeStart - 1,
                nextSlide = rangeEnd,
                $slides = _.$slider.find('.slick-slide');

            for (var i = 0; i < _.options.slidesToScroll; i++) {
                if (prevSlide < 0) prevSlide = _.slideCount - 1;
                loadRange = loadRange.add($slides.eq(prevSlide));
                loadRange = loadRange.add($slides.eq(nextSlide));
                prevSlide--;
                nextSlide++;
            }
        }

        loadImages(loadRange);

        if (_.slideCount <= _.options.slidesToShow) {
            cloneRange = _.$slider.find('.slick-slide');
            loadImages(cloneRange);
        } else
        if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
            cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
            loadImages(cloneRange);
        } else if (_.currentSlide === 0) {
            cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
            loadImages(cloneRange);
        }

    };

    Slick.prototype.loadSlider = function() {

        var _ = this;

        _.setPosition();

        _.$slideTrack.css({
            opacity: 1
        });

        _.$slider.removeClass('slick-loading');

        _.initUI();

        if (_.options.lazyLoad === 'progressive') {
            _.progressiveLazyLoad();
        }

    };

    Slick.prototype.next = Slick.prototype.slickNext = function() {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'next'
            }
        });

    };

    Slick.prototype.orientationChange = function() {

        var _ = this;

        _.checkResponsive();
        _.setPosition();

    };

    Slick.prototype.pause = Slick.prototype.slickPause = function() {

        var _ = this;

        _.autoPlayClear();
        _.paused = true;

    };

    Slick.prototype.play = Slick.prototype.slickPlay = function() {

        var _ = this;

        _.autoPlay();
        _.options.autoplay = true;
        _.paused = false;
        _.focussed = false;
        _.interrupted = false;

    };

    Slick.prototype.postSlide = function(index) {

        var _ = this;

        if( !_.unslicked ) {

            _.$slider.trigger('afterChange', [_, index]);

            _.animating = false;

            if (_.slideCount > _.options.slidesToShow) {
                _.setPosition();
            }

            _.swipeLeft = null;

            if ( _.options.autoplay ) {
                _.autoPlay();
            }

            if (_.options.accessibility === true) {
                _.initADA();

                if (_.options.focusOnChange) {
                    var $currentSlide = $(_.$slides.get(_.currentSlide));
                    $currentSlide.attr('tabindex', 0).focus();
                }
            }

        }

    };

    Slick.prototype.prev = Slick.prototype.slickPrev = function() {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'previous'
            }
        });

    };

    Slick.prototype.preventDefault = function(event) {

        event.preventDefault();

    };

    Slick.prototype.progressiveLazyLoad = function( tryCount ) {

        tryCount = tryCount || 1;

        var _ = this,
            $imgsToLoad = $( 'img[data-lazy]', _.$slider ),
            image,
            imageSource,
            imageSrcSet,
            imageSizes,
            imageToLoad;

        if ( $imgsToLoad.length ) {

            image = $imgsToLoad.first();
            imageSource = image.attr('data-lazy');
            imageSrcSet = image.attr('data-srcset');
            imageSizes  = image.attr('data-sizes') || _.$slider.attr('data-sizes');
            imageToLoad = document.createElement('img');

            imageToLoad.onload = function() {

                if (imageSrcSet) {
                    image
                        .attr('srcset', imageSrcSet );

                    if (imageSizes) {
                        image
                            .attr('sizes', imageSizes );
                    }
                }

                image
                    .attr( 'src', imageSource )
                    .removeAttr('data-lazy data-srcset data-sizes')
                    .removeClass('slick-loading');

                if ( _.options.adaptiveHeight === true ) {
                    _.setPosition();
                }

                _.$slider.trigger('lazyLoaded', [ _, image, imageSource ]);
                _.progressiveLazyLoad();

            };

            imageToLoad.onerror = function() {

                if ( tryCount < 3 ) {

                    /**
                     * try to load the image 3 times,
                     * leave a slight delay so we don't get
                     * servers blocking the request.
                     */
                    setTimeout( function() {
                        _.progressiveLazyLoad( tryCount + 1 );
                    }, 500 );

                } else {

                    image
                        .removeAttr( 'data-lazy' )
                        .removeClass( 'slick-loading' )
                        .addClass( 'slick-lazyload-error' );

                    _.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);

                    _.progressiveLazyLoad();

                }

            };

            imageToLoad.src = imageSource;

        } else {

            _.$slider.trigger('allImagesLoaded', [ _ ]);

        }

    };

    Slick.prototype.refresh = function( initializing ) {

        var _ = this, currentSlide, lastVisibleIndex;

        lastVisibleIndex = _.slideCount - _.options.slidesToShow;

        // in non-infinite sliders, we don't want to go past the
        // last visible index.
        if( !_.options.infinite && ( _.currentSlide > lastVisibleIndex )) {
            _.currentSlide = lastVisibleIndex;
        }

        // if less slides than to show, go to start.
        if ( _.slideCount <= _.options.slidesToShow ) {
            _.currentSlide = 0;

        }

        currentSlide = _.currentSlide;

        _.destroy(true);

        $.extend(_, _.initials, { currentSlide: currentSlide });

        _.init();

        if( !initializing ) {

            _.changeSlide({
                data: {
                    message: 'index',
                    index: currentSlide
                }
            }, false);

        }

    };

    Slick.prototype.registerBreakpoints = function() {

        var _ = this, breakpoint, currentBreakpoint, l,
            responsiveSettings = _.options.responsive || null;

        if ( $.type(responsiveSettings) === 'array' && responsiveSettings.length ) {

            _.respondTo = _.options.respondTo || 'window';

            for ( breakpoint in responsiveSettings ) {

                l = _.breakpoints.length-1;

                if (responsiveSettings.hasOwnProperty(breakpoint)) {
                    currentBreakpoint = responsiveSettings[breakpoint].breakpoint;

                    // loop through the breakpoints and cut out any existing
                    // ones with the same breakpoint number, we don't want dupes.
                    while( l >= 0 ) {
                        if( _.breakpoints[l] && _.breakpoints[l] === currentBreakpoint ) {
                            _.breakpoints.splice(l,1);
                        }
                        l--;
                    }

                    _.breakpoints.push(currentBreakpoint);
                    _.breakpointSettings[currentBreakpoint] = responsiveSettings[breakpoint].settings;

                }

            }

            _.breakpoints.sort(function(a, b) {
                return ( _.options.mobileFirst ) ? a-b : b-a;
            });

        }

    };

    Slick.prototype.reinit = function() {

        var _ = this;

        _.$slides =
            _.$slideTrack
                .children(_.options.slide)
                .addClass('slick-slide');

        _.slideCount = _.$slides.length;

        if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
            _.currentSlide = _.currentSlide - _.options.slidesToScroll;
        }

        if (_.slideCount <= _.options.slidesToShow) {
            _.currentSlide = 0;
        }

        _.registerBreakpoints();

        _.setProps();
        _.setupInfinite();
        _.buildArrows();
        _.updateArrows();
        _.initArrowEvents();
        _.buildDots();
        _.updateDots();
        _.initDotEvents();
        _.cleanUpSlideEvents();
        _.initSlideEvents();

        _.checkResponsive(false, true);

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().on('click.slick', _.selectHandler);
        }

        _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

        _.setPosition();
        _.focusHandler();

        _.paused = !_.options.autoplay;
        _.autoPlay();

        _.$slider.trigger('reInit', [_]);

    };

    Slick.prototype.resize = function() {

        var _ = this;

        if ($(window).width() !== _.windowWidth) {
            clearTimeout(_.windowDelay);
            _.windowDelay = window.setTimeout(function() {
                _.windowWidth = $(window).width();
                _.checkResponsive();
                if( !_.unslicked ) { _.setPosition(); }
            }, 50);
        }
    };

    Slick.prototype.removeSlide = Slick.prototype.slickRemove = function(index, removeBefore, removeAll) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            removeBefore = index;
            index = removeBefore === true ? 0 : _.slideCount - 1;
        } else {
            index = removeBefore === true ? --index : index;
        }

        if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
            return false;
        }

        _.unload();

        if (removeAll === true) {
            _.$slideTrack.children().remove();
        } else {
            _.$slideTrack.children(this.options.slide).eq(index).remove();
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).detach();

        _.$slideTrack.append(_.$slides);

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.setCSS = function(position) {

        var _ = this,
            positionProps = {},
            x, y;

        if (_.options.rtl === true) {
            position = -position;
        }
        x = _.positionProp == 'left' ? Math.ceil(position) + 'px' : '0px';
        y = _.positionProp == 'top' ? Math.ceil(position) + 'px' : '0px';

        positionProps[_.positionProp] = position;

        if (_.transformsEnabled === false) {
            _.$slideTrack.css(positionProps);
        } else {
            positionProps = {};
            if (_.cssTransitions === false) {
                positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';
                _.$slideTrack.css(positionProps);
            } else {
                positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';
                _.$slideTrack.css(positionProps);
            }
        }

    };

    Slick.prototype.setDimensions = function() {

        var _ = this;

        if (_.options.vertical === false) {
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: ('0px ' + _.options.centerPadding)
                });
            }
        } else {
            _.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: (_.options.centerPadding + ' 0px')
                });
            }
        }

        _.listWidth = _.$list.width();
        _.listHeight = _.$list.height();


        if (_.options.vertical === false && _.options.variableWidth === false) {
            _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);
            _.$slideTrack.width(Math.ceil((_.slideWidth * _.$slideTrack.children('.slick-slide').length)));

        } else if (_.options.variableWidth === true) {
            _.$slideTrack.width(5000 * _.slideCount);
        } else {
            _.slideWidth = Math.ceil(_.listWidth);
            _.$slideTrack.height(Math.ceil((_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length)));
        }

        var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();
        if (_.options.variableWidth === false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);

    };

    Slick.prototype.setFade = function() {

        var _ = this,
            targetLeft;

        _.$slides.each(function(index, element) {
            targetLeft = (_.slideWidth * index) * -1;
            if (_.options.rtl === true) {
                $(element).css({
                    position: 'relative',
                    right: targetLeft,
                    top: 0,
                    zIndex: _.options.zIndex - 2,
                    opacity: 0
                });
            } else {
                $(element).css({
                    position: 'relative',
                    left: targetLeft,
                    top: 0,
                    zIndex: _.options.zIndex - 2,
                    opacity: 0
                });
            }
        });

        _.$slides.eq(_.currentSlide).css({
            zIndex: _.options.zIndex - 1,
            opacity: 1
        });

    };

    Slick.prototype.setHeight = function() {

        var _ = this;

        if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
            _.$list.css('height', targetHeight);
        }

    };

    Slick.prototype.setOption =
    Slick.prototype.slickSetOption = function() {

        /**
         * accepts arguments in format of:
         *
         *  - for changing a single option's value:
         *     .slick("setOption", option, value, refresh )
         *
         *  - for changing a set of responsive options:
         *     .slick("setOption", 'responsive', [{}, ...], refresh )
         *
         *  - for updating multiple values at once (not responsive)
         *     .slick("setOption", { 'option': value, ... }, refresh )
         */

        var _ = this, l, item, option, value, refresh = false, type;

        if( $.type( arguments[0] ) === 'object' ) {

            option =  arguments[0];
            refresh = arguments[1];
            type = 'multiple';

        } else if ( $.type( arguments[0] ) === 'string' ) {

            option =  arguments[0];
            value = arguments[1];
            refresh = arguments[2];

            if ( arguments[0] === 'responsive' && $.type( arguments[1] ) === 'array' ) {

                type = 'responsive';

            } else if ( typeof arguments[1] !== 'undefined' ) {

                type = 'single';

            }

        }

        if ( type === 'single' ) {

            _.options[option] = value;


        } else if ( type === 'multiple' ) {

            $.each( option , function( opt, val ) {

                _.options[opt] = val;

            });


        } else if ( type === 'responsive' ) {

            for ( item in value ) {

                if( $.type( _.options.responsive ) !== 'array' ) {

                    _.options.responsive = [ value[item] ];

                } else {

                    l = _.options.responsive.length-1;

                    // loop through the responsive object and splice out duplicates.
                    while( l >= 0 ) {

                        if( _.options.responsive[l].breakpoint === value[item].breakpoint ) {

                            _.options.responsive.splice(l,1);

                        }

                        l--;

                    }

                    _.options.responsive.push( value[item] );

                }

            }

        }

        if ( refresh ) {

            _.unload();
            _.reinit();

        }

    };

    Slick.prototype.setPosition = function() {

        var _ = this;

        _.setDimensions();

        _.setHeight();

        if (_.options.fade === false) {
            _.setCSS(_.getLeft(_.currentSlide));
        } else {
            _.setFade();
        }

        _.$slider.trigger('setPosition', [_]);

    };

    Slick.prototype.setProps = function() {

        var _ = this,
            bodyStyle = document.body.style;

        _.positionProp = _.options.vertical === true ? 'top' : 'left';

        if (_.positionProp === 'top') {
            _.$slider.addClass('slick-vertical');
        } else {
            _.$slider.removeClass('slick-vertical');
        }

        if (bodyStyle.WebkitTransition !== undefined ||
            bodyStyle.MozTransition !== undefined ||
            bodyStyle.msTransition !== undefined) {
            if (_.options.useCSS === true) {
                _.cssTransitions = true;
            }
        }

        if ( _.options.fade ) {
            if ( typeof _.options.zIndex === 'number' ) {
                if( _.options.zIndex < 3 ) {
                    _.options.zIndex = 3;
                }
            } else {
                _.options.zIndex = _.defaults.zIndex;
            }
        }

        if (bodyStyle.OTransform !== undefined) {
            _.animType = 'OTransform';
            _.transformType = '-o-transform';
            _.transitionType = 'OTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.MozTransform !== undefined) {
            _.animType = 'MozTransform';
            _.transformType = '-moz-transform';
            _.transitionType = 'MozTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.webkitTransform !== undefined) {
            _.animType = 'webkitTransform';
            _.transformType = '-webkit-transform';
            _.transitionType = 'webkitTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.msTransform !== undefined) {
            _.animType = 'msTransform';
            _.transformType = '-ms-transform';
            _.transitionType = 'msTransition';
            if (bodyStyle.msTransform === undefined) _.animType = false;
        }
        if (bodyStyle.transform !== undefined && _.animType !== false) {
            _.animType = 'transform';
            _.transformType = 'transform';
            _.transitionType = 'transition';
        }
        _.transformsEnabled = _.options.useTransform && (_.animType !== null && _.animType !== false);
    };


    Slick.prototype.setSlideClasses = function(index) {

        var _ = this,
            centerOffset, allSlides, indexOffset, remainder;

        allSlides = _.$slider
            .find('.slick-slide')
            .removeClass('slick-active slick-center slick-current')
            .attr('aria-hidden', 'true');

        _.$slides
            .eq(index)
            .addClass('slick-current');

        if (_.options.centerMode === true) {

            var evenCoef = _.options.slidesToShow % 2 === 0 ? 1 : 0;

            centerOffset = Math.floor(_.options.slidesToShow / 2);

            if (_.options.infinite === true) {

                if (index >= centerOffset && index <= (_.slideCount - 1) - centerOffset) {
                    _.$slides
                        .slice(index - centerOffset + evenCoef, index + centerOffset + 1)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                } else {

                    indexOffset = _.options.slidesToShow + index;
                    allSlides
                        .slice(indexOffset - centerOffset + 1 + evenCoef, indexOffset + centerOffset + 2)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                }

                if (index === 0) {

                    allSlides
                        .eq(allSlides.length - 1 - _.options.slidesToShow)
                        .addClass('slick-center');

                } else if (index === _.slideCount - 1) {

                    allSlides
                        .eq(_.options.slidesToShow)
                        .addClass('slick-center');

                }

            }

            _.$slides
                .eq(index)
                .addClass('slick-center');

        } else {

            if (index >= 0 && index <= (_.slideCount - _.options.slidesToShow)) {

                _.$slides
                    .slice(index, index + _.options.slidesToShow)
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

            } else if (allSlides.length <= _.options.slidesToShow) {

                allSlides
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

            } else {

                remainder = _.slideCount % _.options.slidesToShow;
                indexOffset = _.options.infinite === true ? _.options.slidesToShow + index : index;

                if (_.options.slidesToShow == _.options.slidesToScroll && (_.slideCount - index) < _.options.slidesToShow) {

                    allSlides
                        .slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                } else {

                    allSlides
                        .slice(indexOffset, indexOffset + _.options.slidesToShow)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                }

            }

        }

        if (_.options.lazyLoad === 'ondemand' || _.options.lazyLoad === 'anticipated') {
            _.lazyLoad();
        }
    };

    Slick.prototype.setupInfinite = function() {

        var _ = this,
            i, slideIndex, infiniteCount;

        if (_.options.fade === true) {
            _.options.centerMode = false;
        }

        if (_.options.infinite === true && _.options.fade === false) {

            slideIndex = null;

            if (_.slideCount > _.options.slidesToShow) {

                if (_.options.centerMode === true) {
                    infiniteCount = _.options.slidesToShow + 1;
                } else {
                    infiniteCount = _.options.slidesToShow;
                }

                for (i = _.slideCount; i > (_.slideCount -
                        infiniteCount); i -= 1) {
                    slideIndex = i - 1;
                    $(_.$slides[slideIndex]).clone(true).attr('id', '')
                        .attr('data-slick-index', slideIndex - _.slideCount)
                        .prependTo(_.$slideTrack).addClass('slick-cloned');
                }
                for (i = 0; i < infiniteCount  + _.slideCount; i += 1) {
                    slideIndex = i;
                    $(_.$slides[slideIndex]).clone(true).attr('id', '')
                        .attr('data-slick-index', slideIndex + _.slideCount)
                        .appendTo(_.$slideTrack).addClass('slick-cloned');
                }
                _.$slideTrack.find('.slick-cloned').find('[id]').each(function() {
                    $(this).attr('id', '');
                });

            }

        }

    };

    Slick.prototype.interrupt = function( toggle ) {

        var _ = this;

        if( !toggle ) {
            _.autoPlay();
        }
        _.interrupted = toggle;

    };

    Slick.prototype.selectHandler = function(event) {

        var _ = this;

        var targetElement =
            $(event.target).is('.slick-slide') ?
                $(event.target) :
                $(event.target).parents('.slick-slide');

        var index = parseInt(targetElement.attr('data-slick-index'));

        if (!index) index = 0;

        if (_.slideCount <= _.options.slidesToShow) {

            _.slideHandler(index, false, true);
            return;

        }

        _.slideHandler(index);

    };

    Slick.prototype.slideHandler = function(index, sync, dontAnimate) {

        var targetSlide, animSlide, oldSlide, slideLeft, targetLeft = null,
            _ = this, navTarget;

        sync = sync || false;

        if (_.animating === true && _.options.waitForAnimate === true) {
            return;
        }

        if (_.options.fade === true && _.currentSlide === index) {
            return;
        }

        if (sync === false) {
            _.asNavFor(index);
        }

        targetSlide = index;
        targetLeft = _.getLeft(targetSlide);
        slideLeft = _.getLeft(_.currentSlide);

        _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

        if (_.options.infinite === false && _.options.centerMode === false && (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)) {
            if (_.options.fade === false) {
                targetSlide = _.currentSlide;
                if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
                    _.animateSlide(slideLeft, function() {
                        _.postSlide(targetSlide);
                    });
                } else {
                    _.postSlide(targetSlide);
                }
            }
            return;
        } else if (_.options.infinite === false && _.options.centerMode === true && (index < 0 || index > (_.slideCount - _.options.slidesToScroll))) {
            if (_.options.fade === false) {
                targetSlide = _.currentSlide;
                if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
                    _.animateSlide(slideLeft, function() {
                        _.postSlide(targetSlide);
                    });
                } else {
                    _.postSlide(targetSlide);
                }
            }
            return;
        }

        if ( _.options.autoplay ) {
            clearInterval(_.autoPlayTimer);
        }

        if (targetSlide < 0) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = _.slideCount - (_.slideCount % _.options.slidesToScroll);
            } else {
                animSlide = _.slideCount + targetSlide;
            }
        } else if (targetSlide >= _.slideCount) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = 0;
            } else {
                animSlide = targetSlide - _.slideCount;
            }
        } else {
            animSlide = targetSlide;
        }

        _.animating = true;

        _.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);

        oldSlide = _.currentSlide;
        _.currentSlide = animSlide;

        _.setSlideClasses(_.currentSlide);

        if ( _.options.asNavFor ) {

            navTarget = _.getNavTarget();
            navTarget = navTarget.slick('getSlick');

            if ( navTarget.slideCount <= navTarget.options.slidesToShow ) {
                navTarget.setSlideClasses(_.currentSlide);
            }

        }

        _.updateDots();
        _.updateArrows();

        if (_.options.fade === true) {
            if (dontAnimate !== true) {

                _.fadeSlideOut(oldSlide);

                _.fadeSlide(animSlide, function() {
                    _.postSlide(animSlide);
                });

            } else {
                _.postSlide(animSlide);
            }
            _.animateHeight();
            return;
        }

        if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
            _.animateSlide(targetLeft, function() {
                _.postSlide(animSlide);
            });
        } else {
            _.postSlide(animSlide);
        }

    };

    Slick.prototype.startLoad = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.hide();
            _.$nextArrow.hide();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.hide();

        }

        _.$slider.addClass('slick-loading');

    };

    Slick.prototype.swipeDirection = function() {

        var xDist, yDist, r, swipeAngle, _ = this;

        xDist = _.touchObject.startX - _.touchObject.curX;
        yDist = _.touchObject.startY - _.touchObject.curY;
        r = Math.atan2(yDist, xDist);

        swipeAngle = Math.round(r * 180 / Math.PI);
        if (swipeAngle < 0) {
            swipeAngle = 360 - Math.abs(swipeAngle);
        }

        if ((swipeAngle <= 45) && (swipeAngle >= 0)) {
            return (_.options.rtl === false ? 'left' : 'right');
        }
        if ((swipeAngle <= 360) && (swipeAngle >= 315)) {
            return (_.options.rtl === false ? 'left' : 'right');
        }
        if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
            return (_.options.rtl === false ? 'right' : 'left');
        }
        if (_.options.verticalSwiping === true) {
            if ((swipeAngle >= 35) && (swipeAngle <= 135)) {
                return 'down';
            } else {
                return 'up';
            }
        }

        return 'vertical';

    };

    Slick.prototype.swipeEnd = function(event) {

        var _ = this,
            slideCount,
            direction;

        _.dragging = false;
        _.swiping = false;

        if (_.scrolling) {
            _.scrolling = false;
            return false;
        }

        _.interrupted = false;
        _.shouldClick = ( _.touchObject.swipeLength > 10 ) ? false : true;

        if ( _.touchObject.curX === undefined ) {
            return false;
        }

        if ( _.touchObject.edgeHit === true ) {
            _.$slider.trigger('edge', [_, _.swipeDirection() ]);
        }

        if ( _.touchObject.swipeLength >= _.touchObject.minSwipe ) {

            direction = _.swipeDirection();

            switch ( direction ) {

                case 'left':
                case 'down':

                    slideCount =
                        _.options.swipeToSlide ?
                            _.checkNavigable( _.currentSlide + _.getSlideCount() ) :
                            _.currentSlide + _.getSlideCount();

                    _.currentDirection = 0;

                    break;

                case 'right':
                case 'up':

                    slideCount =
                        _.options.swipeToSlide ?
                            _.checkNavigable( _.currentSlide - _.getSlideCount() ) :
                            _.currentSlide - _.getSlideCount();

                    _.currentDirection = 1;

                    break;

                default:


            }

            if( direction != 'vertical' ) {

                _.slideHandler( slideCount );
                _.touchObject = {};
                _.$slider.trigger('swipe', [_, direction ]);

            }

        } else {

            if ( _.touchObject.startX !== _.touchObject.curX ) {

                _.slideHandler( _.currentSlide );
                _.touchObject = {};

            }

        }

    };

    Slick.prototype.swipeHandler = function(event) {

        var _ = this;

        if ((_.options.swipe === false) || ('ontouchend' in document && _.options.swipe === false)) {
            return;
        } else if (_.options.draggable === false && event.type.indexOf('mouse') !== -1) {
            return;
        }

        _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ?
            event.originalEvent.touches.length : 1;

        _.touchObject.minSwipe = _.listWidth / _.options
            .touchThreshold;

        if (_.options.verticalSwiping === true) {
            _.touchObject.minSwipe = _.listHeight / _.options
                .touchThreshold;
        }

        switch (event.data.action) {

            case 'start':
                _.swipeStart(event);
                break;

            case 'move':
                _.swipeMove(event);
                break;

            case 'end':
                _.swipeEnd(event);
                break;

        }

    };

    Slick.prototype.swipeMove = function(event) {

        var _ = this,
            edgeWasHit = false,
            curLeft, swipeDirection, swipeLength, positionOffset, touches, verticalSwipeLength;

        touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

        if (!_.dragging || _.scrolling || touches && touches.length !== 1) {
            return false;
        }

        curLeft = _.getLeft(_.currentSlide);

        _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
        _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;

        _.touchObject.swipeLength = Math.round(Math.sqrt(
            Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

        verticalSwipeLength = Math.round(Math.sqrt(
            Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));

        if (!_.options.verticalSwiping && !_.swiping && verticalSwipeLength > 4) {
            _.scrolling = true;
            return false;
        }

        if (_.options.verticalSwiping === true) {
            _.touchObject.swipeLength = verticalSwipeLength;
        }

        swipeDirection = _.swipeDirection();

        if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
            _.swiping = true;
            event.preventDefault();
        }

        positionOffset = (_.options.rtl === false ? 1 : -1) * (_.touchObject.curX > _.touchObject.startX ? 1 : -1);
        if (_.options.verticalSwiping === true) {
            positionOffset = _.touchObject.curY > _.touchObject.startY ? 1 : -1;
        }


        swipeLength = _.touchObject.swipeLength;

        _.touchObject.edgeHit = false;

        if (_.options.infinite === false) {
            if ((_.currentSlide === 0 && swipeDirection === 'right') || (_.currentSlide >= _.getDotCount() && swipeDirection === 'left')) {
                swipeLength = _.touchObject.swipeLength * _.options.edgeFriction;
                _.touchObject.edgeHit = true;
            }
        }

        if (_.options.vertical === false) {
            _.swipeLeft = curLeft + swipeLength * positionOffset;
        } else {
            _.swipeLeft = curLeft + (swipeLength * (_.$list.height() / _.listWidth)) * positionOffset;
        }
        if (_.options.verticalSwiping === true) {
            _.swipeLeft = curLeft + swipeLength * positionOffset;
        }

        if (_.options.fade === true || _.options.touchMove === false) {
            return false;
        }

        if (_.animating === true) {
            _.swipeLeft = null;
            return false;
        }

        _.setCSS(_.swipeLeft);

    };

    Slick.prototype.swipeStart = function(event) {

        var _ = this,
            touches;

        _.interrupted = true;

        if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
            _.touchObject = {};
            return false;
        }

        if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
            touches = event.originalEvent.touches[0];
        }

        _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
        _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;

        _.dragging = true;

    };

    Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function() {

        var _ = this;

        if (_.$slidesCache !== null) {

            _.unload();

            _.$slideTrack.children(this.options.slide).detach();

            _.$slidesCache.appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.unload = function() {

        var _ = this;

        $('.slick-cloned', _.$slider).remove();

        if (_.$dots) {
            _.$dots.remove();
        }

        if (_.$prevArrow && _.htmlExpr.test(_.options.prevArrow)) {
            _.$prevArrow.remove();
        }

        if (_.$nextArrow && _.htmlExpr.test(_.options.nextArrow)) {
            _.$nextArrow.remove();
        }

        _.$slides
            .removeClass('slick-slide slick-active slick-visible slick-current')
            .attr('aria-hidden', 'true')
            .css('width', '');

    };

    Slick.prototype.unslick = function(fromBreakpoint) {

        var _ = this;
        _.$slider.trigger('unslick', [_, fromBreakpoint]);
        _.destroy();

    };

    Slick.prototype.updateArrows = function() {

        var _ = this,
            centerOffset;

        centerOffset = Math.floor(_.options.slidesToShow / 2);

        if ( _.options.arrows === true &&
            _.slideCount > _.options.slidesToShow &&
            !_.options.infinite ) {

            _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
            _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            if (_.currentSlide === 0) {

                _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow && _.options.centerMode === false) {

                _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            } else if (_.currentSlide >= _.slideCount - 1 && _.options.centerMode === true) {

                _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            }

        }

    };

    Slick.prototype.updateDots = function() {

        var _ = this;

        if (_.$dots !== null) {

            _.$dots
                .find('li')
                    .removeClass('slick-active')
                    .end();

            _.$dots
                .find('li')
                .eq(Math.floor(_.currentSlide / _.options.slidesToScroll))
                .addClass('slick-active');

        }

    };

    Slick.prototype.visibility = function() {

        var _ = this;

        if ( _.options.autoplay ) {

            if ( document[_.hidden] ) {

                _.interrupted = true;

            } else {

                _.interrupted = false;

            }

        }

    };

    $.fn.slick = function() {
        var _ = this,
            opt = arguments[0],
            args = Array.prototype.slice.call(arguments, 1),
            l = _.length,
            i,
            ret;
        for (i = 0; i < l; i++) {
            if (typeof opt == 'object' || typeof opt == 'undefined')
                _[i].slick = new Slick(_[i], opt);
            else
                ret = _[i].slick[opt].apply(_[i].slick, args);
            if (typeof ret != 'undefined') return ret;
        }
        return _;
    };

}));

/*
 * jQuery Superfish Menu Plugin - v1.7.10
 * Copyright (c) 2018 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *	http://www.opensource.org/licenses/mit-license.php
 *	http://www.gnu.org/licenses/gpl.html
 */

;(function ($, w) {
	"use strict";

	var methods = (function () {
		// private properties and methods go here
		var c = {
				bcClass: 'sf-breadcrumb',
				menuClass: 'sf-js-enabled',
				anchorClass: 'sf-with-ul',
				menuArrowClass: 'sf-arrows'
			},
			ios = (function () {
				var ios = /^(?![\w\W]*Windows Phone)[\w\W]*(iPhone|iPad|iPod)/i.test(navigator.userAgent);
				if (ios) {
					// tap anywhere on iOS to unfocus a submenu
					$('html').css('cursor', 'pointer').on('click', $.noop);
				}
				return ios;
			})(),
			wp7 = (function () {
				var style = document.documentElement.style;
				return ('behavior' in style && 'fill' in style && /iemobile/i.test(navigator.userAgent));
			})(),
			unprefixedPointerEvents = (function () {
				return (!!w.PointerEvent);
			})(),
			toggleMenuClasses = function ($menu, o, add) {
				var classes = c.menuClass,
					method;
				if (o.cssArrows) {
					classes += ' ' + c.menuArrowClass;
				}
				method = (add) ? 'addClass' : 'removeClass';
				$menu[method](classes);
			},
			setPathToCurrent = function ($menu, o) {
				return $menu.find('li.' + o.pathClass).slice(0, o.pathLevels)
					.addClass(o.hoverClass + ' ' + c.bcClass)
						.filter(function () {
							return ($(this).children(o.popUpSelector).hide().show().length);
						}).removeClass(o.pathClass);
			},
			toggleAnchorClass = function ($li, add) {
				var method = (add) ? 'addClass' : 'removeClass';
				$li.children('a')[method](c.anchorClass);
			},
			toggleTouchAction = function ($menu) {
				var msTouchAction = $menu.css('ms-touch-action');
				var touchAction = $menu.css('touch-action');
				touchAction = touchAction || msTouchAction;
				touchAction = (touchAction === 'pan-y') ? 'auto' : 'pan-y';
				$menu.css({
					'ms-touch-action': touchAction,
					'touch-action': touchAction
				});
			},
			getMenu = function ($el) {
				return $el.closest('.' + c.menuClass);
			},
			getOptions = function ($el) {
				return getMenu($el).data('sfOptions');
			},
			over = function () {
				var $this = $(this),
					o = getOptions($this);
				clearTimeout(o.sfTimer);
				$this.siblings().superfish('hide').end().superfish('show');
			},
			close = function (o) {
				o.retainPath = ($.inArray(this[0], o.$path) > -1);
				this.superfish('hide');

				if (!this.parents('.' + o.hoverClass).length) {
					o.onIdle.call(getMenu(this));
					if (o.$path.length) {
						$.proxy(over, o.$path)();
					}
				}
			},
			out = function () {
				var $this = $(this),
					o = getOptions($this);
				if (ios) {
					$.proxy(close, $this, o)();
				}
				else {
					clearTimeout(o.sfTimer);
					o.sfTimer = setTimeout($.proxy(close, $this, o), o.delay);
				}
			},
			touchHandler = function (e) {
				var $this = $(this),
					o = getOptions($this),
					$ul = $this.siblings(e.data.popUpSelector);

				if (o.onHandleTouch.call($ul) === false) {
					return this;
				}

				if ($ul.length > 0 && $ul.is(':hidden')) {
					$this.one('click.superfish', false);
					if (e.type === 'MSPointerDown' || e.type === 'pointerdown') {
						$this.trigger('focus');
					} else {
						$.proxy(over, $this.parent('li'))();
					}
				}
			},
			applyHandlers = function ($menu, o) {
				var targets = 'li:has(' + o.popUpSelector + ')';
				if ($.fn.hoverIntent && !o.disableHI) {
					$menu.hoverIntent(over, out, targets);
				}
				else {
					$menu
						.on('mouseenter.superfish', targets, over)
						.on('mouseleave.superfish', targets, out);
				}
				var touchevent = 'MSPointerDown.superfish';
				if (unprefixedPointerEvents) {
					touchevent = 'pointerdown.superfish';
				}
				if (!ios) {
					touchevent += ' touchend.superfish';
				}
				if (wp7) {
					touchevent += ' mousedown.superfish';
				}
				$menu
					.on('focusin.superfish', 'li', over)
					.on('focusout.superfish', 'li', out)
					.on(touchevent, 'a', o, touchHandler);
			};

		return {
			// public methods
			hide: function (instant) {
				if (this.length) {
					var $this = this,
						o = getOptions($this);
					if (!o) {
						return this;
					}
					var not = (o.retainPath === true) ? o.$path : '',
						$ul = $this.find('li.' + o.hoverClass).add(this).not(not).removeClass(o.hoverClass).children(o.popUpSelector),
						speed = o.speedOut;

					if (instant) {
						$ul.show();
						speed = 0;
					}
					o.retainPath = false;

					if (o.onBeforeHide.call($ul) === false) {
						return this;
					}

					$ul.stop(true, true).animate(o.animationOut, speed, function () {
						var $this = $(this);
						o.onHide.call($this);
					});
				}
				return this;
			},
			show: function () {
				var o = getOptions(this);
				if (!o) {
					return this;
				}
				var $this = this.addClass(o.hoverClass),
					$ul = $this.children(o.popUpSelector);

				if (o.onBeforeShow.call($ul) === false) {
					return this;
				}

				$ul.stop(true, true).animate(o.animation, o.speed, function () {
					o.onShow.call($ul);
				});
				return this;
			},
			destroy: function () {
				return this.each(function () {
					var $this = $(this),
						o = $this.data('sfOptions'),
						$hasPopUp;
					if (!o) {
						return false;
					}
					$hasPopUp = $this.find(o.popUpSelector).parent('li');
					clearTimeout(o.sfTimer);
					toggleMenuClasses($this, o);
					toggleAnchorClass($hasPopUp);
					toggleTouchAction($this);
					// remove event handlers
					$this.off('.superfish').off('.hoverIntent');
					// clear animation's inline display style
					$hasPopUp.children(o.popUpSelector).attr('style', function (i, style) {
						if (typeof style !== 'undefined') {
							return style.replace(/display[^;]+;?/g, '');
						}
					});
					// reset 'current' path classes
					o.$path.removeClass(o.hoverClass + ' ' + c.bcClass).addClass(o.pathClass);
					$this.find('.' + o.hoverClass).removeClass(o.hoverClass);
					o.onDestroy.call($this);
					$this.removeData('sfOptions');
				});
			},
			init: function (op) {
				return this.each(function () {
					var $this = $(this);
					if ($this.data('sfOptions')) {
						return false;
					}
					var o = $.extend({}, $.fn.superfish.defaults, op),
						$hasPopUp = $this.find(o.popUpSelector).parent('li');
					o.$path = setPathToCurrent($this, o);

					$this.data('sfOptions', o);

					toggleMenuClasses($this, o, true);
					toggleAnchorClass($hasPopUp, true);
					toggleTouchAction($this);
					applyHandlers($this, o);

					$hasPopUp.not('.' + c.bcClass).superfish('hide', true);

					o.onInit.call(this);
				});
			}
		};
	})();

	$.fn.superfish = function (method, args) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === 'object' || ! method) {
			return methods.init.apply(this, arguments);
		}
		else {
			return $.error('Method ' +  method + ' does not exist on jQuery.fn.superfish');
		}
	};

	$.fn.superfish.defaults = {
		popUpSelector: 'ul,.sf-mega', // within menu context
		hoverClass: 'sfHover',
		pathClass: 'overrideThisToUse',
		pathLevels: 1,
		delay: 800,
		animation: {opacity: 'show'},
		animationOut: {opacity: 'hide'},
		speed: 'normal',
		speedOut: 'fast',
		cssArrows: true,
		disableHI: false,
		onInit: $.noop,
		onBeforeShow: $.noop,
		onShow: $.noop,
		onBeforeHide: $.noop,
		onHide: $.noop,
		onIdle: $.noop,
		onDestroy: $.noop,
		onHandleTouch: $.noop
	};

})(jQuery, window);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNsaXBib2FyZC5taW4uanMiLCJob3ZlckludGVudC5qcyIsImpxdWVyeS1zb3J0YWJsZS5qcyIsImpxdWVyeS1zdmctcmVwbGFjZXIuanMiLCJqcXVlcnkuZmFuY3lib3gubWluLmpzIiwianF1ZXJ5LnNlbGVjdHJpYy5qcyIsImpxdWVyeS5zZWxlY3RyaWMubWluLmpzIiwianMuY29va2llLm1pbi5qcyIsInNoYXJlLWJ1dHRvbnMuanMiLCJzbGljay5qcyIsInN1cGVyZmlzaC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2pIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNwckJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDOUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN0bENBO0FBQ0E7QUNEQTtBQUNBO0FBQ0E7QUNGQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ244RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6InBsdWdpbnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogY2xpcGJvYXJkLmpzIHYyLjAuNFxyXG4gKiBodHRwczovL3plbm9yb2NoYS5naXRodWIuaW8vY2xpcGJvYXJkLmpzXHJcbiAqIFxyXG4gKiBMaWNlbnNlZCBNSVQgwqkgWmVubyBSb2NoYVxyXG4gKi9cclxuIWZ1bmN0aW9uKHQsZSl7XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHMmJlwib2JqZWN0XCI9PXR5cGVvZiBtb2R1bGU/bW9kdWxlLmV4cG9ydHM9ZSgpOlwiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoW10sZSk6XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHM/ZXhwb3J0cy5DbGlwYm9hcmRKUz1lKCk6dC5DbGlwYm9hcmRKUz1lKCl9KHRoaXMsZnVuY3Rpb24oKXtyZXR1cm4gZnVuY3Rpb24obil7dmFyIG89e307ZnVuY3Rpb24gcih0KXtpZihvW3RdKXJldHVybiBvW3RdLmV4cG9ydHM7dmFyIGU9b1t0XT17aTp0LGw6ITEsZXhwb3J0czp7fX07cmV0dXJuIG5bdF0uY2FsbChlLmV4cG9ydHMsZSxlLmV4cG9ydHMsciksZS5sPSEwLGUuZXhwb3J0c31yZXR1cm4gci5tPW4sci5jPW8sci5kPWZ1bmN0aW9uKHQsZSxuKXtyLm8odCxlKXx8T2JqZWN0LmRlZmluZVByb3BlcnR5KHQsZSx7ZW51bWVyYWJsZTohMCxnZXQ6bn0pfSxyLnI9ZnVuY3Rpb24odCl7XCJ1bmRlZmluZWRcIiE9dHlwZW9mIFN5bWJvbCYmU3ltYm9sLnRvU3RyaW5nVGFnJiZPYmplY3QuZGVmaW5lUHJvcGVydHkodCxTeW1ib2wudG9TdHJpbmdUYWcse3ZhbHVlOlwiTW9kdWxlXCJ9KSxPYmplY3QuZGVmaW5lUHJvcGVydHkodCxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KX0sci50PWZ1bmN0aW9uKGUsdCl7aWYoMSZ0JiYoZT1yKGUpKSw4JnQpcmV0dXJuIGU7aWYoNCZ0JiZcIm9iamVjdFwiPT10eXBlb2YgZSYmZSYmZS5fX2VzTW9kdWxlKXJldHVybiBlO3ZhciBuPU9iamVjdC5jcmVhdGUobnVsbCk7aWYoci5yKG4pLE9iamVjdC5kZWZpbmVQcm9wZXJ0eShuLFwiZGVmYXVsdFwiLHtlbnVtZXJhYmxlOiEwLHZhbHVlOmV9KSwyJnQmJlwic3RyaW5nXCIhPXR5cGVvZiBlKWZvcih2YXIgbyBpbiBlKXIuZChuLG8sZnVuY3Rpb24odCl7cmV0dXJuIGVbdF19LmJpbmQobnVsbCxvKSk7cmV0dXJuIG59LHIubj1mdW5jdGlvbih0KXt2YXIgZT10JiZ0Ll9fZXNNb2R1bGU/ZnVuY3Rpb24oKXtyZXR1cm4gdC5kZWZhdWx0fTpmdW5jdGlvbigpe3JldHVybiB0fTtyZXR1cm4gci5kKGUsXCJhXCIsZSksZX0sci5vPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbCh0LGUpfSxyLnA9XCJcIixyKHIucz0wKX0oW2Z1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgcj1cImZ1bmN0aW9uXCI9PXR5cGVvZiBTeW1ib2wmJlwic3ltYm9sXCI9PXR5cGVvZiBTeW1ib2wuaXRlcmF0b3I/ZnVuY3Rpb24odCl7cmV0dXJuIHR5cGVvZiB0fTpmdW5jdGlvbih0KXtyZXR1cm4gdCYmXCJmdW5jdGlvblwiPT10eXBlb2YgU3ltYm9sJiZ0LmNvbnN0cnVjdG9yPT09U3ltYm9sJiZ0IT09U3ltYm9sLnByb3RvdHlwZT9cInN5bWJvbFwiOnR5cGVvZiB0fSxpPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gbyh0LGUpe2Zvcih2YXIgbj0wO248ZS5sZW5ndGg7bisrKXt2YXIgbz1lW25dO28uZW51bWVyYWJsZT1vLmVudW1lcmFibGV8fCExLG8uY29uZmlndXJhYmxlPSEwLFwidmFsdWVcImluIG8mJihvLndyaXRhYmxlPSEwKSxPYmplY3QuZGVmaW5lUHJvcGVydHkodCxvLmtleSxvKX19cmV0dXJuIGZ1bmN0aW9uKHQsZSxuKXtyZXR1cm4gZSYmbyh0LnByb3RvdHlwZSxlKSxuJiZvKHQsbiksdH19KCksYT1vKG4oMSkpLGM9byhuKDMpKSx1PW8obig0KSk7ZnVuY3Rpb24gbyh0KXtyZXR1cm4gdCYmdC5fX2VzTW9kdWxlP3Q6e2RlZmF1bHQ6dH19dmFyIGw9ZnVuY3Rpb24odCl7ZnVuY3Rpb24gbyh0LGUpeyFmdW5jdGlvbih0LGUpe2lmKCEodCBpbnN0YW5jZW9mIGUpKXRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIil9KHRoaXMsbyk7dmFyIG49ZnVuY3Rpb24odCxlKXtpZighdCl0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7cmV0dXJuIWV8fFwib2JqZWN0XCIhPXR5cGVvZiBlJiZcImZ1bmN0aW9uXCIhPXR5cGVvZiBlP3Q6ZX0odGhpcywoby5fX3Byb3RvX198fE9iamVjdC5nZXRQcm90b3R5cGVPZihvKSkuY2FsbCh0aGlzKSk7cmV0dXJuIG4ucmVzb2x2ZU9wdGlvbnMoZSksbi5saXN0ZW5DbGljayh0KSxufXJldHVybiBmdW5jdGlvbih0LGUpe2lmKFwiZnVuY3Rpb25cIiE9dHlwZW9mIGUmJm51bGwhPT1lKXRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiK3R5cGVvZiBlKTt0LnByb3RvdHlwZT1PYmplY3QuY3JlYXRlKGUmJmUucHJvdG90eXBlLHtjb25zdHJ1Y3Rvcjp7dmFsdWU6dCxlbnVtZXJhYmxlOiExLHdyaXRhYmxlOiEwLGNvbmZpZ3VyYWJsZTohMH19KSxlJiYoT2JqZWN0LnNldFByb3RvdHlwZU9mP09iamVjdC5zZXRQcm90b3R5cGVPZih0LGUpOnQuX19wcm90b19fPWUpfShvLGMuZGVmYXVsdCksaShvLFt7a2V5OlwicmVzb2x2ZU9wdGlvbnNcIix2YWx1ZTpmdW5jdGlvbigpe3ZhciB0PTA8YXJndW1lbnRzLmxlbmd0aCYmdm9pZCAwIT09YXJndW1lbnRzWzBdP2FyZ3VtZW50c1swXTp7fTt0aGlzLmFjdGlvbj1cImZ1bmN0aW9uXCI9PXR5cGVvZiB0LmFjdGlvbj90LmFjdGlvbjp0aGlzLmRlZmF1bHRBY3Rpb24sdGhpcy50YXJnZXQ9XCJmdW5jdGlvblwiPT10eXBlb2YgdC50YXJnZXQ/dC50YXJnZXQ6dGhpcy5kZWZhdWx0VGFyZ2V0LHRoaXMudGV4dD1cImZ1bmN0aW9uXCI9PXR5cGVvZiB0LnRleHQ/dC50ZXh0OnRoaXMuZGVmYXVsdFRleHQsdGhpcy5jb250YWluZXI9XCJvYmplY3RcIj09PXIodC5jb250YWluZXIpP3QuY29udGFpbmVyOmRvY3VtZW50LmJvZHl9fSx7a2V5OlwibGlzdGVuQ2xpY2tcIix2YWx1ZTpmdW5jdGlvbih0KXt2YXIgZT10aGlzO3RoaXMubGlzdGVuZXI9KDAsdS5kZWZhdWx0KSh0LFwiY2xpY2tcIixmdW5jdGlvbih0KXtyZXR1cm4gZS5vbkNsaWNrKHQpfSl9fSx7a2V5Olwib25DbGlja1wiLHZhbHVlOmZ1bmN0aW9uKHQpe3ZhciBlPXQuZGVsZWdhdGVUYXJnZXR8fHQuY3VycmVudFRhcmdldDt0aGlzLmNsaXBib2FyZEFjdGlvbiYmKHRoaXMuY2xpcGJvYXJkQWN0aW9uPW51bGwpLHRoaXMuY2xpcGJvYXJkQWN0aW9uPW5ldyBhLmRlZmF1bHQoe2FjdGlvbjp0aGlzLmFjdGlvbihlKSx0YXJnZXQ6dGhpcy50YXJnZXQoZSksdGV4dDp0aGlzLnRleHQoZSksY29udGFpbmVyOnRoaXMuY29udGFpbmVyLHRyaWdnZXI6ZSxlbWl0dGVyOnRoaXN9KX19LHtrZXk6XCJkZWZhdWx0QWN0aW9uXCIsdmFsdWU6ZnVuY3Rpb24odCl7cmV0dXJuIHMoXCJhY3Rpb25cIix0KX19LHtrZXk6XCJkZWZhdWx0VGFyZ2V0XCIsdmFsdWU6ZnVuY3Rpb24odCl7dmFyIGU9cyhcInRhcmdldFwiLHQpO2lmKGUpcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZSl9fSx7a2V5OlwiZGVmYXVsdFRleHRcIix2YWx1ZTpmdW5jdGlvbih0KXtyZXR1cm4gcyhcInRleHRcIix0KX19LHtrZXk6XCJkZXN0cm95XCIsdmFsdWU6ZnVuY3Rpb24oKXt0aGlzLmxpc3RlbmVyLmRlc3Ryb3koKSx0aGlzLmNsaXBib2FyZEFjdGlvbiYmKHRoaXMuY2xpcGJvYXJkQWN0aW9uLmRlc3Ryb3koKSx0aGlzLmNsaXBib2FyZEFjdGlvbj1udWxsKX19XSxbe2tleTpcImlzU3VwcG9ydGVkXCIsdmFsdWU6ZnVuY3Rpb24oKXt2YXIgdD0wPGFyZ3VtZW50cy5sZW5ndGgmJnZvaWQgMCE9PWFyZ3VtZW50c1swXT9hcmd1bWVudHNbMF06W1wiY29weVwiLFwiY3V0XCJdLGU9XCJzdHJpbmdcIj09dHlwZW9mIHQ/W3RdOnQsbj0hIWRvY3VtZW50LnF1ZXJ5Q29tbWFuZFN1cHBvcnRlZDtyZXR1cm4gZS5mb3JFYWNoKGZ1bmN0aW9uKHQpe249biYmISFkb2N1bWVudC5xdWVyeUNvbW1hbmRTdXBwb3J0ZWQodCl9KSxufX1dKSxvfSgpO2Z1bmN0aW9uIHModCxlKXt2YXIgbj1cImRhdGEtY2xpcGJvYXJkLVwiK3Q7aWYoZS5oYXNBdHRyaWJ1dGUobikpcmV0dXJuIGUuZ2V0QXR0cmlidXRlKG4pfXQuZXhwb3J0cz1sfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIG8scj1cImZ1bmN0aW9uXCI9PXR5cGVvZiBTeW1ib2wmJlwic3ltYm9sXCI9PXR5cGVvZiBTeW1ib2wuaXRlcmF0b3I/ZnVuY3Rpb24odCl7cmV0dXJuIHR5cGVvZiB0fTpmdW5jdGlvbih0KXtyZXR1cm4gdCYmXCJmdW5jdGlvblwiPT10eXBlb2YgU3ltYm9sJiZ0LmNvbnN0cnVjdG9yPT09U3ltYm9sJiZ0IT09U3ltYm9sLnByb3RvdHlwZT9cInN5bWJvbFwiOnR5cGVvZiB0fSxpPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gbyh0LGUpe2Zvcih2YXIgbj0wO248ZS5sZW5ndGg7bisrKXt2YXIgbz1lW25dO28uZW51bWVyYWJsZT1vLmVudW1lcmFibGV8fCExLG8uY29uZmlndXJhYmxlPSEwLFwidmFsdWVcImluIG8mJihvLndyaXRhYmxlPSEwKSxPYmplY3QuZGVmaW5lUHJvcGVydHkodCxvLmtleSxvKX19cmV0dXJuIGZ1bmN0aW9uKHQsZSxuKXtyZXR1cm4gZSYmbyh0LnByb3RvdHlwZSxlKSxuJiZvKHQsbiksdH19KCksYT1uKDIpLGM9KG89YSkmJm8uX19lc01vZHVsZT9vOntkZWZhdWx0Om99O3ZhciB1PWZ1bmN0aW9uKCl7ZnVuY3Rpb24gZSh0KXshZnVuY3Rpb24odCxlKXtpZighKHQgaW5zdGFuY2VvZiBlKSl0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpfSh0aGlzLGUpLHRoaXMucmVzb2x2ZU9wdGlvbnModCksdGhpcy5pbml0U2VsZWN0aW9uKCl9cmV0dXJuIGkoZSxbe2tleTpcInJlc29sdmVPcHRpb25zXCIsdmFsdWU6ZnVuY3Rpb24oKXt2YXIgdD0wPGFyZ3VtZW50cy5sZW5ndGgmJnZvaWQgMCE9PWFyZ3VtZW50c1swXT9hcmd1bWVudHNbMF06e307dGhpcy5hY3Rpb249dC5hY3Rpb24sdGhpcy5jb250YWluZXI9dC5jb250YWluZXIsdGhpcy5lbWl0dGVyPXQuZW1pdHRlcix0aGlzLnRhcmdldD10LnRhcmdldCx0aGlzLnRleHQ9dC50ZXh0LHRoaXMudHJpZ2dlcj10LnRyaWdnZXIsdGhpcy5zZWxlY3RlZFRleHQ9XCJcIn19LHtrZXk6XCJpbml0U2VsZWN0aW9uXCIsdmFsdWU6ZnVuY3Rpb24oKXt0aGlzLnRleHQ/dGhpcy5zZWxlY3RGYWtlKCk6dGhpcy50YXJnZXQmJnRoaXMuc2VsZWN0VGFyZ2V0KCl9fSx7a2V5Olwic2VsZWN0RmFrZVwiLHZhbHVlOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpcyxlPVwicnRsXCI9PWRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJkaXJcIik7dGhpcy5yZW1vdmVGYWtlKCksdGhpcy5mYWtlSGFuZGxlckNhbGxiYWNrPWZ1bmN0aW9uKCl7cmV0dXJuIHQucmVtb3ZlRmFrZSgpfSx0aGlzLmZha2VIYW5kbGVyPXRoaXMuY29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLHRoaXMuZmFrZUhhbmRsZXJDYWxsYmFjayl8fCEwLHRoaXMuZmFrZUVsZW09ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInRleHRhcmVhXCIpLHRoaXMuZmFrZUVsZW0uc3R5bGUuZm9udFNpemU9XCIxMnB0XCIsdGhpcy5mYWtlRWxlbS5zdHlsZS5ib3JkZXI9XCIwXCIsdGhpcy5mYWtlRWxlbS5zdHlsZS5wYWRkaW5nPVwiMFwiLHRoaXMuZmFrZUVsZW0uc3R5bGUubWFyZ2luPVwiMFwiLHRoaXMuZmFrZUVsZW0uc3R5bGUucG9zaXRpb249XCJhYnNvbHV0ZVwiLHRoaXMuZmFrZUVsZW0uc3R5bGVbZT9cInJpZ2h0XCI6XCJsZWZ0XCJdPVwiLTk5OTlweFwiO3ZhciBuPXdpbmRvdy5wYWdlWU9mZnNldHx8ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDt0aGlzLmZha2VFbGVtLnN0eWxlLnRvcD1uK1wicHhcIix0aGlzLmZha2VFbGVtLnNldEF0dHJpYnV0ZShcInJlYWRvbmx5XCIsXCJcIiksdGhpcy5mYWtlRWxlbS52YWx1ZT10aGlzLnRleHQsdGhpcy5jb250YWluZXIuYXBwZW5kQ2hpbGQodGhpcy5mYWtlRWxlbSksdGhpcy5zZWxlY3RlZFRleHQ9KDAsYy5kZWZhdWx0KSh0aGlzLmZha2VFbGVtKSx0aGlzLmNvcHlUZXh0KCl9fSx7a2V5OlwicmVtb3ZlRmFrZVwiLHZhbHVlOmZ1bmN0aW9uKCl7dGhpcy5mYWtlSGFuZGxlciYmKHRoaXMuY29udGFpbmVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLHRoaXMuZmFrZUhhbmRsZXJDYWxsYmFjayksdGhpcy5mYWtlSGFuZGxlcj1udWxsLHRoaXMuZmFrZUhhbmRsZXJDYWxsYmFjaz1udWxsKSx0aGlzLmZha2VFbGVtJiYodGhpcy5jb250YWluZXIucmVtb3ZlQ2hpbGQodGhpcy5mYWtlRWxlbSksdGhpcy5mYWtlRWxlbT1udWxsKX19LHtrZXk6XCJzZWxlY3RUYXJnZXRcIix2YWx1ZTpmdW5jdGlvbigpe3RoaXMuc2VsZWN0ZWRUZXh0PSgwLGMuZGVmYXVsdCkodGhpcy50YXJnZXQpLHRoaXMuY29weVRleHQoKX19LHtrZXk6XCJjb3B5VGV4dFwiLHZhbHVlOmZ1bmN0aW9uKCl7dmFyIGU9dm9pZCAwO3RyeXtlPWRvY3VtZW50LmV4ZWNDb21tYW5kKHRoaXMuYWN0aW9uKX1jYXRjaCh0KXtlPSExfXRoaXMuaGFuZGxlUmVzdWx0KGUpfX0se2tleTpcImhhbmRsZVJlc3VsdFwiLHZhbHVlOmZ1bmN0aW9uKHQpe3RoaXMuZW1pdHRlci5lbWl0KHQ/XCJzdWNjZXNzXCI6XCJlcnJvclwiLHthY3Rpb246dGhpcy5hY3Rpb24sdGV4dDp0aGlzLnNlbGVjdGVkVGV4dCx0cmlnZ2VyOnRoaXMudHJpZ2dlcixjbGVhclNlbGVjdGlvbjp0aGlzLmNsZWFyU2VsZWN0aW9uLmJpbmQodGhpcyl9KX19LHtrZXk6XCJjbGVhclNlbGVjdGlvblwiLHZhbHVlOmZ1bmN0aW9uKCl7dGhpcy50cmlnZ2VyJiZ0aGlzLnRyaWdnZXIuZm9jdXMoKSx3aW5kb3cuZ2V0U2VsZWN0aW9uKCkucmVtb3ZlQWxsUmFuZ2VzKCl9fSx7a2V5OlwiZGVzdHJveVwiLHZhbHVlOmZ1bmN0aW9uKCl7dGhpcy5yZW1vdmVGYWtlKCl9fSx7a2V5OlwiYWN0aW9uXCIsc2V0OmZ1bmN0aW9uKCl7dmFyIHQ9MDxhcmd1bWVudHMubGVuZ3RoJiZ2b2lkIDAhPT1hcmd1bWVudHNbMF0/YXJndW1lbnRzWzBdOlwiY29weVwiO2lmKHRoaXMuX2FjdGlvbj10LFwiY29weVwiIT09dGhpcy5fYWN0aW9uJiZcImN1dFwiIT09dGhpcy5fYWN0aW9uKXRocm93IG5ldyBFcnJvcignSW52YWxpZCBcImFjdGlvblwiIHZhbHVlLCB1c2UgZWl0aGVyIFwiY29weVwiIG9yIFwiY3V0XCInKX0sZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuX2FjdGlvbn19LHtrZXk6XCJ0YXJnZXRcIixzZXQ6ZnVuY3Rpb24odCl7aWYodm9pZCAwIT09dCl7aWYoIXR8fFwib2JqZWN0XCIhPT0odm9pZCAwPT09dD9cInVuZGVmaW5lZFwiOnIodCkpfHwxIT09dC5ub2RlVHlwZSl0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgXCJ0YXJnZXRcIiB2YWx1ZSwgdXNlIGEgdmFsaWQgRWxlbWVudCcpO2lmKFwiY29weVwiPT09dGhpcy5hY3Rpb24mJnQuaGFzQXR0cmlidXRlKFwiZGlzYWJsZWRcIikpdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIFwidGFyZ2V0XCIgYXR0cmlidXRlLiBQbGVhc2UgdXNlIFwicmVhZG9ubHlcIiBpbnN0ZWFkIG9mIFwiZGlzYWJsZWRcIiBhdHRyaWJ1dGUnKTtpZihcImN1dFwiPT09dGhpcy5hY3Rpb24mJih0Lmhhc0F0dHJpYnV0ZShcInJlYWRvbmx5XCIpfHx0Lmhhc0F0dHJpYnV0ZShcImRpc2FibGVkXCIpKSl0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgXCJ0YXJnZXRcIiBhdHRyaWJ1dGUuIFlvdSBjYW5cXCd0IGN1dCB0ZXh0IGZyb20gZWxlbWVudHMgd2l0aCBcInJlYWRvbmx5XCIgb3IgXCJkaXNhYmxlZFwiIGF0dHJpYnV0ZXMnKTt0aGlzLl90YXJnZXQ9dH19LGdldDpmdW5jdGlvbigpe3JldHVybiB0aGlzLl90YXJnZXR9fV0pLGV9KCk7dC5leHBvcnRzPXV9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3ZhciBlO2lmKFwiU0VMRUNUXCI9PT10Lm5vZGVOYW1lKXQuZm9jdXMoKSxlPXQudmFsdWU7ZWxzZSBpZihcIklOUFVUXCI9PT10Lm5vZGVOYW1lfHxcIlRFWFRBUkVBXCI9PT10Lm5vZGVOYW1lKXt2YXIgbj10Lmhhc0F0dHJpYnV0ZShcInJlYWRvbmx5XCIpO258fHQuc2V0QXR0cmlidXRlKFwicmVhZG9ubHlcIixcIlwiKSx0LnNlbGVjdCgpLHQuc2V0U2VsZWN0aW9uUmFuZ2UoMCx0LnZhbHVlLmxlbmd0aCksbnx8dC5yZW1vdmVBdHRyaWJ1dGUoXCJyZWFkb25seVwiKSxlPXQudmFsdWV9ZWxzZXt0Lmhhc0F0dHJpYnV0ZShcImNvbnRlbnRlZGl0YWJsZVwiKSYmdC5mb2N1cygpO3ZhciBvPXdpbmRvdy5nZXRTZWxlY3Rpb24oKSxyPWRvY3VtZW50LmNyZWF0ZVJhbmdlKCk7ci5zZWxlY3ROb2RlQ29udGVudHModCksby5yZW1vdmVBbGxSYW5nZXMoKSxvLmFkZFJhbmdlKHIpLGU9by50b1N0cmluZygpfXJldHVybiBlfX0sZnVuY3Rpb24odCxlKXtmdW5jdGlvbiBuKCl7fW4ucHJvdG90eXBlPXtvbjpmdW5jdGlvbih0LGUsbil7dmFyIG89dGhpcy5lfHwodGhpcy5lPXt9KTtyZXR1cm4ob1t0XXx8KG9bdF09W10pKS5wdXNoKHtmbjplLGN0eDpufSksdGhpc30sb25jZTpmdW5jdGlvbih0LGUsbil7dmFyIG89dGhpcztmdW5jdGlvbiByKCl7by5vZmYodCxyKSxlLmFwcGx5KG4sYXJndW1lbnRzKX1yZXR1cm4gci5fPWUsdGhpcy5vbih0LHIsbil9LGVtaXQ6ZnVuY3Rpb24odCl7Zm9yKHZhciBlPVtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLDEpLG49KCh0aGlzLmV8fCh0aGlzLmU9e30pKVt0XXx8W10pLnNsaWNlKCksbz0wLHI9bi5sZW5ndGg7bzxyO28rKyluW29dLmZuLmFwcGx5KG5bb10uY3R4LGUpO3JldHVybiB0aGlzfSxvZmY6ZnVuY3Rpb24odCxlKXt2YXIgbj10aGlzLmV8fCh0aGlzLmU9e30pLG89blt0XSxyPVtdO2lmKG8mJmUpZm9yKHZhciBpPTAsYT1vLmxlbmd0aDtpPGE7aSsrKW9baV0uZm4hPT1lJiZvW2ldLmZuLl8hPT1lJiZyLnB1c2gob1tpXSk7cmV0dXJuIHIubGVuZ3RoP25bdF09cjpkZWxldGUgblt0XSx0aGlzfX0sdC5leHBvcnRzPW59LGZ1bmN0aW9uKHQsZSxuKXt2YXIgZD1uKDUpLGg9big2KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4pe2lmKCF0JiYhZSYmIW4pdGhyb3cgbmV3IEVycm9yKFwiTWlzc2luZyByZXF1aXJlZCBhcmd1bWVudHNcIik7aWYoIWQuc3RyaW5nKGUpKXRocm93IG5ldyBUeXBlRXJyb3IoXCJTZWNvbmQgYXJndW1lbnQgbXVzdCBiZSBhIFN0cmluZ1wiKTtpZighZC5mbihuKSl0aHJvdyBuZXcgVHlwZUVycm9yKFwiVGhpcmQgYXJndW1lbnQgbXVzdCBiZSBhIEZ1bmN0aW9uXCIpO2lmKGQubm9kZSh0KSlyZXR1cm4gcz1lLGY9biwobD10KS5hZGRFdmVudExpc3RlbmVyKHMsZikse2Rlc3Ryb3k6ZnVuY3Rpb24oKXtsLnJlbW92ZUV2ZW50TGlzdGVuZXIocyxmKX19O2lmKGQubm9kZUxpc3QodCkpcmV0dXJuIGE9dCxjPWUsdT1uLEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoYSxmdW5jdGlvbih0KXt0LmFkZEV2ZW50TGlzdGVuZXIoYyx1KX0pLHtkZXN0cm95OmZ1bmN0aW9uKCl7QXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChhLGZ1bmN0aW9uKHQpe3QucmVtb3ZlRXZlbnRMaXN0ZW5lcihjLHUpfSl9fTtpZihkLnN0cmluZyh0KSlyZXR1cm4gbz10LHI9ZSxpPW4saChkb2N1bWVudC5ib2R5LG8scixpKTt0aHJvdyBuZXcgVHlwZUVycm9yKFwiRmlyc3QgYXJndW1lbnQgbXVzdCBiZSBhIFN0cmluZywgSFRNTEVsZW1lbnQsIEhUTUxDb2xsZWN0aW9uLCBvciBOb2RlTGlzdFwiKTt2YXIgbyxyLGksYSxjLHUsbCxzLGZ9fSxmdW5jdGlvbih0LG4pe24ubm9kZT1mdW5jdGlvbih0KXtyZXR1cm4gdm9pZCAwIT09dCYmdCBpbnN0YW5jZW9mIEhUTUxFbGVtZW50JiYxPT09dC5ub2RlVHlwZX0sbi5ub2RlTGlzdD1mdW5jdGlvbih0KXt2YXIgZT1PYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodCk7cmV0dXJuIHZvaWQgMCE9PXQmJihcIltvYmplY3QgTm9kZUxpc3RdXCI9PT1lfHxcIltvYmplY3QgSFRNTENvbGxlY3Rpb25dXCI9PT1lKSYmXCJsZW5ndGhcImluIHQmJigwPT09dC5sZW5ndGh8fG4ubm9kZSh0WzBdKSl9LG4uc3RyaW5nPWZ1bmN0aW9uKHQpe3JldHVyblwic3RyaW5nXCI9PXR5cGVvZiB0fHx0IGluc3RhbmNlb2YgU3RyaW5nfSxuLmZuPWZ1bmN0aW9uKHQpe3JldHVyblwiW29iamVjdCBGdW5jdGlvbl1cIj09PU9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh0KX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgYT1uKDcpO2Z1bmN0aW9uIGkodCxlLG4sbyxyKXt2YXIgaT1mdW5jdGlvbihlLG4sdCxvKXtyZXR1cm4gZnVuY3Rpb24odCl7dC5kZWxlZ2F0ZVRhcmdldD1hKHQudGFyZ2V0LG4pLHQuZGVsZWdhdGVUYXJnZXQmJm8uY2FsbChlLHQpfX0uYXBwbHkodGhpcyxhcmd1bWVudHMpO3JldHVybiB0LmFkZEV2ZW50TGlzdGVuZXIobixpLHIpLHtkZXN0cm95OmZ1bmN0aW9uKCl7dC5yZW1vdmVFdmVudExpc3RlbmVyKG4saSxyKX19fXQuZXhwb3J0cz1mdW5jdGlvbih0LGUsbixvLHIpe3JldHVyblwiZnVuY3Rpb25cIj09dHlwZW9mIHQuYWRkRXZlbnRMaXN0ZW5lcj9pLmFwcGx5KG51bGwsYXJndW1lbnRzKTpcImZ1bmN0aW9uXCI9PXR5cGVvZiBuP2kuYmluZChudWxsLGRvY3VtZW50KS5hcHBseShudWxsLGFyZ3VtZW50cyk6KFwic3RyaW5nXCI9PXR5cGVvZiB0JiYodD1kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHQpKSxBcnJheS5wcm90b3R5cGUubWFwLmNhbGwodCxmdW5jdGlvbih0KXtyZXR1cm4gaSh0LGUsbixvLHIpfSkpfX0sZnVuY3Rpb24odCxlKXtpZihcInVuZGVmaW5lZFwiIT10eXBlb2YgRWxlbWVudCYmIUVsZW1lbnQucHJvdG90eXBlLm1hdGNoZXMpe3ZhciBuPUVsZW1lbnQucHJvdG90eXBlO24ubWF0Y2hlcz1uLm1hdGNoZXNTZWxlY3Rvcnx8bi5tb3pNYXRjaGVzU2VsZWN0b3J8fG4ubXNNYXRjaGVzU2VsZWN0b3J8fG4ub01hdGNoZXNTZWxlY3Rvcnx8bi53ZWJraXRNYXRjaGVzU2VsZWN0b3J9dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7Zm9yKDt0JiY5IT09dC5ub2RlVHlwZTspe2lmKFwiZnVuY3Rpb25cIj09dHlwZW9mIHQubWF0Y2hlcyYmdC5tYXRjaGVzKGUpKXJldHVybiB0O3Q9dC5wYXJlbnROb2RlfX19XSl9KTsiLCIvKipcbiAqIGhvdmVySW50ZW50IGlzIHNpbWlsYXIgdG8galF1ZXJ5J3MgYnVpbHQtaW4gXCJob3ZlclwiIG1ldGhvZCBleGNlcHQgdGhhdFxuICogaW5zdGVhZCBvZiBmaXJpbmcgdGhlIGhhbmRsZXJJbiBmdW5jdGlvbiBpbW1lZGlhdGVseSwgaG92ZXJJbnRlbnQgY2hlY2tzXG4gKiB0byBzZWUgaWYgdGhlIHVzZXIncyBtb3VzZSBoYXMgc2xvd2VkIGRvd24gKGJlbmVhdGggdGhlIHNlbnNpdGl2aXR5XG4gKiB0aHJlc2hvbGQpIGJlZm9yZSBmaXJpbmcgdGhlIGV2ZW50LiBUaGUgaGFuZGxlck91dCBmdW5jdGlvbiBpcyBvbmx5XG4gKiBjYWxsZWQgYWZ0ZXIgYSBtYXRjaGluZyBoYW5kbGVySW4uXG4gKlxuICogaG92ZXJJbnRlbnQgcjcgLy8gMjAxMy4wMy4xMSAvLyBqUXVlcnkgMS45LjErXG4gKiBodHRwOi8vY2hlcm5lLm5ldC9icmlhbi9yZXNvdXJjZXMvanF1ZXJ5LmhvdmVySW50ZW50Lmh0bWxcbiAqXG4gKiBZb3UgbWF5IHVzZSBob3ZlckludGVudCB1bmRlciB0aGUgdGVybXMgb2YgdGhlIE1JVCBsaWNlbnNlLiBCYXNpY2FsbHkgdGhhdFxuICogbWVhbnMgeW91IGFyZSBmcmVlIHRvIHVzZSBob3ZlckludGVudCBhcyBsb25nIGFzIHRoaXMgaGVhZGVyIGlzIGxlZnQgaW50YWN0LlxuICogQ29weXJpZ2h0IDIwMDcsIDIwMTMgQnJpYW4gQ2hlcm5lXG4gKlxuICogLy8gYmFzaWMgdXNhZ2UgLi4uIGp1c3QgbGlrZSAuaG92ZXIoKVxuICogLmhvdmVySW50ZW50KCBoYW5kbGVySW4sIGhhbmRsZXJPdXQgKVxuICogLmhvdmVySW50ZW50KCBoYW5kbGVySW5PdXQgKVxuICpcbiAqIC8vIGJhc2ljIHVzYWdlIC4uLiB3aXRoIGV2ZW50IGRlbGVnYXRpb24hXG4gKiAuaG92ZXJJbnRlbnQoIGhhbmRsZXJJbiwgaGFuZGxlck91dCwgc2VsZWN0b3IgKVxuICogLmhvdmVySW50ZW50KCBoYW5kbGVySW5PdXQsIHNlbGVjdG9yIClcbiAqXG4gKiAvLyB1c2luZyBhIGJhc2ljIGNvbmZpZ3VyYXRpb24gb2JqZWN0XG4gKiAuaG92ZXJJbnRlbnQoIGNvbmZpZyApXG4gKlxuICogQHBhcmFtICBoYW5kbGVySW4gICBmdW5jdGlvbiBPUiBjb25maWd1cmF0aW9uIG9iamVjdFxuICogQHBhcmFtICBoYW5kbGVyT3V0ICBmdW5jdGlvbiBPUiBzZWxlY3RvciBmb3IgZGVsZWdhdGlvbiBPUiB1bmRlZmluZWRcbiAqIEBwYXJhbSAgc2VsZWN0b3IgICAgc2VsZWN0b3IgT1IgdW5kZWZpbmVkXG4gKiBAYXV0aG9yIEJyaWFuIENoZXJuZSA8YnJpYW4oYXQpY2hlcm5lKGRvdCluZXQ+XG4gKiovXG4oZnVuY3Rpb24oJCkge1xuICAgICQuZm4uaG92ZXJJbnRlbnQgPSBmdW5jdGlvbihoYW5kbGVySW4saGFuZGxlck91dCxzZWxlY3Rvcikge1xuXG4gICAgICAgIC8vIGRlZmF1bHQgY29uZmlndXJhdGlvbiB2YWx1ZXNcbiAgICAgICAgdmFyIGNmZyA9IHtcbiAgICAgICAgICAgIGludGVydmFsOiAxMDAsXG4gICAgICAgICAgICBzZW5zaXRpdml0eTogNyxcbiAgICAgICAgICAgIHRpbWVvdXQ6IDBcbiAgICAgICAgfTtcblxuICAgICAgICBpZiAoIHR5cGVvZiBoYW5kbGVySW4gPT09IFwib2JqZWN0XCIgKSB7XG4gICAgICAgICAgICBjZmcgPSAkLmV4dGVuZChjZmcsIGhhbmRsZXJJbiApO1xuICAgICAgICB9IGVsc2UgaWYgKCQuaXNGdW5jdGlvbihoYW5kbGVyT3V0KSkge1xuICAgICAgICAgICAgY2ZnID0gJC5leHRlbmQoY2ZnLCB7IG92ZXI6IGhhbmRsZXJJbiwgb3V0OiBoYW5kbGVyT3V0LCBzZWxlY3Rvcjogc2VsZWN0b3IgfSApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2ZnID0gJC5leHRlbmQoY2ZnLCB7IG92ZXI6IGhhbmRsZXJJbiwgb3V0OiBoYW5kbGVySW4sIHNlbGVjdG9yOiBoYW5kbGVyT3V0IH0gKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIGluc3RhbnRpYXRlIHZhcmlhYmxlc1xuICAgICAgICAvLyBjWCwgY1kgPSBjdXJyZW50IFggYW5kIFkgcG9zaXRpb24gb2YgbW91c2UsIHVwZGF0ZWQgYnkgbW91c2Vtb3ZlIGV2ZW50XG4gICAgICAgIC8vIHBYLCBwWSA9IHByZXZpb3VzIFggYW5kIFkgcG9zaXRpb24gb2YgbW91c2UsIHNldCBieSBtb3VzZW92ZXIgYW5kIHBvbGxpbmcgaW50ZXJ2YWxcbiAgICAgICAgdmFyIGNYLCBjWSwgcFgsIHBZO1xuXG4gICAgICAgIC8vIEEgcHJpdmF0ZSBmdW5jdGlvbiBmb3IgZ2V0dGluZyBtb3VzZSBwb3NpdGlvblxuICAgICAgICB2YXIgdHJhY2sgPSBmdW5jdGlvbihldikge1xuICAgICAgICAgICAgY1ggPSBldi5wYWdlWDtcbiAgICAgICAgICAgIGNZID0gZXYucGFnZVk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gQSBwcml2YXRlIGZ1bmN0aW9uIGZvciBjb21wYXJpbmcgY3VycmVudCBhbmQgcHJldmlvdXMgbW91c2UgcG9zaXRpb25cbiAgICAgICAgdmFyIGNvbXBhcmUgPSBmdW5jdGlvbihldixvYikge1xuICAgICAgICAgICAgb2IuaG92ZXJJbnRlbnRfdCA9IGNsZWFyVGltZW91dChvYi5ob3ZlckludGVudF90KTtcbiAgICAgICAgICAgIC8vIGNvbXBhcmUgbW91c2UgcG9zaXRpb25zIHRvIHNlZSBpZiB0aGV5J3ZlIGNyb3NzZWQgdGhlIHRocmVzaG9sZFxuICAgICAgICAgICAgaWYgKCAoIE1hdGguYWJzKHBYLWNYKSArIE1hdGguYWJzKHBZLWNZKSApIDwgY2ZnLnNlbnNpdGl2aXR5ICkge1xuICAgICAgICAgICAgICAgICQob2IpLm9mZihcIm1vdXNlbW92ZS5ob3ZlckludGVudFwiLHRyYWNrKTtcbiAgICAgICAgICAgICAgICAvLyBzZXQgaG92ZXJJbnRlbnQgc3RhdGUgdG8gdHJ1ZSAoc28gbW91c2VPdXQgY2FuIGJlIGNhbGxlZClcbiAgICAgICAgICAgICAgICBvYi5ob3ZlckludGVudF9zID0gMTtcbiAgICAgICAgICAgICAgICByZXR1cm4gY2ZnLm92ZXIuYXBwbHkob2IsW2V2XSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIHNldCBwcmV2aW91cyBjb29yZGluYXRlcyBmb3IgbmV4dCB0aW1lXG4gICAgICAgICAgICAgICAgcFggPSBjWDsgcFkgPSBjWTtcbiAgICAgICAgICAgICAgICAvLyB1c2Ugc2VsZi1jYWxsaW5nIHRpbWVvdXQsIGd1YXJhbnRlZXMgaW50ZXJ2YWxzIGFyZSBzcGFjZWQgb3V0IHByb3Blcmx5IChhdm9pZHMgSmF2YVNjcmlwdCB0aW1lciBidWdzKVxuICAgICAgICAgICAgICAgIG9iLmhvdmVySW50ZW50X3QgPSBzZXRUaW1lb3V0KCBmdW5jdGlvbigpe2NvbXBhcmUoZXYsIG9iKTt9ICwgY2ZnLmludGVydmFsICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gQSBwcml2YXRlIGZ1bmN0aW9uIGZvciBkZWxheWluZyB0aGUgbW91c2VPdXQgZnVuY3Rpb25cbiAgICAgICAgdmFyIGRlbGF5ID0gZnVuY3Rpb24oZXYsb2IpIHtcbiAgICAgICAgICAgIG9iLmhvdmVySW50ZW50X3QgPSBjbGVhclRpbWVvdXQob2IuaG92ZXJJbnRlbnRfdCk7XG4gICAgICAgICAgICBvYi5ob3ZlckludGVudF9zID0gMDtcbiAgICAgICAgICAgIHJldHVybiBjZmcub3V0LmFwcGx5KG9iLFtldl0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIEEgcHJpdmF0ZSBmdW5jdGlvbiBmb3IgaGFuZGxpbmcgbW91c2UgJ2hvdmVyaW5nJ1xuICAgICAgICB2YXIgaGFuZGxlSG92ZXIgPSBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAvLyBjb3B5IG9iamVjdHMgdG8gYmUgcGFzc2VkIGludG8gdCAocmVxdWlyZWQgZm9yIGV2ZW50IG9iamVjdCB0byBiZSBwYXNzZWQgaW4gSUUpXG4gICAgICAgICAgICB2YXIgZXYgPSBqUXVlcnkuZXh0ZW5kKHt9LGUpO1xuICAgICAgICAgICAgdmFyIG9iID0gdGhpcztcblxuICAgICAgICAgICAgLy8gY2FuY2VsIGhvdmVySW50ZW50IHRpbWVyIGlmIGl0IGV4aXN0c1xuICAgICAgICAgICAgaWYgKG9iLmhvdmVySW50ZW50X3QpIHsgb2IuaG92ZXJJbnRlbnRfdCA9IGNsZWFyVGltZW91dChvYi5ob3ZlckludGVudF90KTsgfVxuXG4gICAgICAgICAgICAvLyBpZiBlLnR5cGUgPT0gXCJtb3VzZWVudGVyXCJcbiAgICAgICAgICAgIGlmIChlLnR5cGUgPT0gXCJtb3VzZWVudGVyXCIpIHtcbiAgICAgICAgICAgICAgICAvLyBzZXQgXCJwcmV2aW91c1wiIFggYW5kIFkgcG9zaXRpb24gYmFzZWQgb24gaW5pdGlhbCBlbnRyeSBwb2ludFxuICAgICAgICAgICAgICAgIHBYID0gZXYucGFnZVg7IHBZID0gZXYucGFnZVk7XG4gICAgICAgICAgICAgICAgLy8gdXBkYXRlIFwiY3VycmVudFwiIFggYW5kIFkgcG9zaXRpb24gYmFzZWQgb24gbW91c2Vtb3ZlXG4gICAgICAgICAgICAgICAgJChvYikub24oXCJtb3VzZW1vdmUuaG92ZXJJbnRlbnRcIix0cmFjayk7XG4gICAgICAgICAgICAgICAgLy8gc3RhcnQgcG9sbGluZyBpbnRlcnZhbCAoc2VsZi1jYWxsaW5nIHRpbWVvdXQpIHRvIGNvbXBhcmUgbW91c2UgY29vcmRpbmF0ZXMgb3ZlciB0aW1lXG4gICAgICAgICAgICAgICAgaWYgKG9iLmhvdmVySW50ZW50X3MgIT0gMSkgeyBvYi5ob3ZlckludGVudF90ID0gc2V0VGltZW91dCggZnVuY3Rpb24oKXtjb21wYXJlKGV2LG9iKTt9ICwgY2ZnLmludGVydmFsICk7fVxuXG4gICAgICAgICAgICAgICAgLy8gZWxzZSBlLnR5cGUgPT0gXCJtb3VzZWxlYXZlXCJcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgLy8gdW5iaW5kIGV4cGVuc2l2ZSBtb3VzZW1vdmUgZXZlbnRcbiAgICAgICAgICAgICAgICAkKG9iKS5vZmYoXCJtb3VzZW1vdmUuaG92ZXJJbnRlbnRcIix0cmFjayk7XG4gICAgICAgICAgICAgICAgLy8gaWYgaG92ZXJJbnRlbnQgc3RhdGUgaXMgdHJ1ZSwgdGhlbiBjYWxsIHRoZSBtb3VzZU91dCBmdW5jdGlvbiBhZnRlciB0aGUgc3BlY2lmaWVkIGRlbGF5XG4gICAgICAgICAgICAgICAgaWYgKG9iLmhvdmVySW50ZW50X3MgPT0gMSkgeyBvYi5ob3ZlckludGVudF90ID0gc2V0VGltZW91dCggZnVuY3Rpb24oKXtkZWxheShldixvYik7fSAsIGNmZy50aW1lb3V0ICk7fVxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIGxpc3RlbiBmb3IgbW91c2VlbnRlciBhbmQgbW91c2VsZWF2ZVxuICAgICAgICByZXR1cm4gdGhpcy5vbih7J21vdXNlZW50ZXIuaG92ZXJJbnRlbnQnOmhhbmRsZUhvdmVyLCdtb3VzZWxlYXZlLmhvdmVySW50ZW50JzpoYW5kbGVIb3Zlcn0sIGNmZy5zZWxlY3Rvcik7XG4gICAgfTtcbn0pKGpRdWVyeSk7IiwiLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAqICBqcXVlcnktc29ydGFibGUuanMgdjAuOS4xM1xyXG4gKiAgaHR0cDovL2pvaG5ueS5naXRodWIuY29tL2pxdWVyeS1zb3J0YWJsZS9cclxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAqICBDb3B5cmlnaHQgKGMpIDIwMTIgSm9uYXMgdm9uIEFuZHJpYW5cclxuICogIEFsbCByaWdodHMgcmVzZXJ2ZWQuXHJcbiAqXHJcbiAqICBSZWRpc3RyaWJ1dGlvbiBhbmQgdXNlIGluIHNvdXJjZSBhbmQgYmluYXJ5IGZvcm1zLCB3aXRoIG9yIHdpdGhvdXRcclxuICogIG1vZGlmaWNhdGlvbiwgYXJlIHBlcm1pdHRlZCBwcm92aWRlZCB0aGF0IHRoZSBmb2xsb3dpbmcgY29uZGl0aW9ucyBhcmUgbWV0OlxyXG4gKiAgKiBSZWRpc3RyaWJ1dGlvbnMgb2Ygc291cmNlIGNvZGUgbXVzdCByZXRhaW4gdGhlIGFib3ZlIGNvcHlyaWdodFxyXG4gKiAgICBub3RpY2UsIHRoaXMgbGlzdCBvZiBjb25kaXRpb25zIGFuZCB0aGUgZm9sbG93aW5nIGRpc2NsYWltZXIuXHJcbiAqICAqIFJlZGlzdHJpYnV0aW9ucyBpbiBiaW5hcnkgZm9ybSBtdXN0IHJlcHJvZHVjZSB0aGUgYWJvdmUgY29weXJpZ2h0XHJcbiAqICAgIG5vdGljZSwgdGhpcyBsaXN0IG9mIGNvbmRpdGlvbnMgYW5kIHRoZSBmb2xsb3dpbmcgZGlzY2xhaW1lciBpbiB0aGVcclxuICogICAgZG9jdW1lbnRhdGlvbiBhbmQvb3Igb3RoZXIgbWF0ZXJpYWxzIHByb3ZpZGVkIHdpdGggdGhlIGRpc3RyaWJ1dGlvbi5cclxuICogICogVGhlIG5hbWUgb2YgdGhlIGF1dGhvciBtYXkgbm90IGJlIHVzZWQgdG8gZW5kb3JzZSBvciBwcm9tb3RlIHByb2R1Y3RzXHJcbiAqICAgIGRlcml2ZWQgZnJvbSB0aGlzIHNvZnR3YXJlIHdpdGhvdXQgc3BlY2lmaWMgcHJpb3Igd3JpdHRlbiBwZXJtaXNzaW9uLlxyXG4gKlxyXG4gKiAgVEhJUyBTT0ZUV0FSRSBJUyBQUk9WSURFRCBCWSBUSEUgQ09QWVJJR0hUIEhPTERFUlMgQU5EIENPTlRSSUJVVE9SUyBcIkFTIElTXCIgQU5EXHJcbiAqICBBTlkgRVhQUkVTUyBPUiBJTVBMSUVEIFdBUlJBTlRJRVMsIElOQ0xVRElORywgQlVUIE5PVCBMSU1JVEVEIFRPLCBUSEUgSU1QTElFRFxyXG4gKiAgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFkgQU5EIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFSRVxyXG4gKiAgRElTQ0xBSU1FRC4gSU4gTk8gRVZFTlQgU0hBTEwgPENPUFlSSUdIVCBIT0xERVI+IEJFIExJQUJMRSBGT1IgQU5ZXHJcbiAqICBESVJFQ1QsIElORElSRUNULCBJTkNJREVOVEFMLCBTUEVDSUFMLCBFWEVNUExBUlksIE9SIENPTlNFUVVFTlRJQUwgREFNQUdFU1xyXG4gKiAgKElOQ0xVRElORywgQlVUIE5PVCBMSU1JVEVEIFRPLCBQUk9DVVJFTUVOVCBPRiBTVUJTVElUVVRFIEdPT0RTIE9SIFNFUlZJQ0VTO1xyXG4gKiAgTE9TUyBPRiBVU0UsIERBVEEsIE9SIFBST0ZJVFM7IE9SIEJVU0lORVNTIElOVEVSUlVQVElPTikgSE9XRVZFUiBDQVVTRUQgQU5EXHJcbiAqICBPTiBBTlkgVEhFT1JZIE9GIExJQUJJTElUWSwgV0hFVEhFUiBJTiBDT05UUkFDVCwgU1RSSUNUIExJQUJJTElUWSwgT1IgVE9SVFxyXG4gKiAgKElOQ0xVRElORyBORUdMSUdFTkNFIE9SIE9USEVSV0lTRSkgQVJJU0lORyBJTiBBTlkgV0FZIE9VVCBPRiBUSEUgVVNFIE9GIFRISVNcclxuICogIFNPRlRXQVJFLCBFVkVOIElGIEFEVklTRUQgT0YgVEhFIFBPU1NJQklMSVRZIE9GIFNVQ0ggREFNQUdFLlxyXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXHJcblxyXG4hZnVuY3Rpb24gKCAkLCB3aW5kb3csIHBsdWdpbk5hbWUsIHVuZGVmaW5lZCl7XHJcbiAgdmFyIGNvbnRhaW5lckRlZmF1bHRzID0ge1xyXG4gICAgLy8gSWYgdHJ1ZSwgaXRlbXMgY2FuIGJlIGRyYWdnZWQgZnJvbSB0aGlzIGNvbnRhaW5lclxyXG4gICAgZHJhZzogdHJ1ZSxcclxuICAgIC8vIElmIHRydWUsIGl0ZW1zIGNhbiBiZSBkcm9wZWQgb250byB0aGlzIGNvbnRhaW5lclxyXG4gICAgZHJvcDogdHJ1ZSxcclxuICAgIC8vIEV4Y2x1ZGUgaXRlbXMgZnJvbSBiZWluZyBkcmFnZ2FibGUsIGlmIHRoZVxyXG4gICAgLy8gc2VsZWN0b3IgbWF0Y2hlcyB0aGUgaXRlbVxyXG4gICAgZXhjbHVkZTogXCJcIixcclxuICAgIC8vIElmIHRydWUsIHNlYXJjaCBmb3IgbmVzdGVkIGNvbnRhaW5lcnMgd2l0aGluIGFuIGl0ZW0uSWYgeW91IG5lc3QgY29udGFpbmVycyxcclxuICAgIC8vIGVpdGhlciB0aGUgb3JpZ2luYWwgc2VsZWN0b3Igd2l0aCB3aGljaCB5b3UgY2FsbCB0aGUgcGx1Z2luIG11c3Qgb25seSBtYXRjaCB0aGUgdG9wIGNvbnRhaW5lcnMsXHJcbiAgICAvLyBvciB5b3UgbmVlZCB0byBzcGVjaWZ5IGEgZ3JvdXAgKHNlZSB0aGUgYm9vdHN0cmFwIG5hdiBleGFtcGxlKVxyXG4gICAgbmVzdGVkOiB0cnVlLFxyXG4gICAgLy8gSWYgdHJ1ZSwgdGhlIGl0ZW1zIGFyZSBhc3N1bWVkIHRvIGJlIGFycmFuZ2VkIHZlcnRpY2FsbHlcclxuICAgIHZlcnRpY2FsOiB0cnVlXHJcbiAgfSwgLy8gZW5kIGNvbnRhaW5lciBkZWZhdWx0c1xyXG4gIGdyb3VwRGVmYXVsdHMgPSB7XHJcbiAgICAvLyBUaGlzIGlzIGV4ZWN1dGVkIGFmdGVyIHRoZSBwbGFjZWhvbGRlciBoYXMgYmVlbiBtb3ZlZC5cclxuICAgIC8vICRjbG9zZXN0SXRlbU9yQ29udGFpbmVyIGNvbnRhaW5zIHRoZSBjbG9zZXN0IGl0ZW0sIHRoZSBwbGFjZWhvbGRlclxyXG4gICAgLy8gaGFzIGJlZW4gcHV0IGF0IG9yIHRoZSBjbG9zZXN0IGVtcHR5IENvbnRhaW5lciwgdGhlIHBsYWNlaG9sZGVyIGhhc1xyXG4gICAgLy8gYmVlbiBhcHBlbmRlZCB0by5cclxuICAgIGFmdGVyTW92ZTogZnVuY3Rpb24gKCRwbGFjZWhvbGRlciwgY29udGFpbmVyLCAkY2xvc2VzdEl0ZW1PckNvbnRhaW5lcikge1xyXG4gICAgfSxcclxuICAgIC8vIFRoZSBleGFjdCBjc3MgcGF0aCBiZXR3ZWVuIHRoZSBjb250YWluZXIgYW5kIGl0cyBpdGVtcywgZS5nLiBcIj4gdGJvZHlcIlxyXG4gICAgY29udGFpbmVyUGF0aDogXCJcIixcclxuICAgIC8vIFRoZSBjc3Mgc2VsZWN0b3Igb2YgdGhlIGNvbnRhaW5lcnNcclxuICAgIGNvbnRhaW5lclNlbGVjdG9yOiBcIm9sLCB1bFwiLFxyXG4gICAgLy8gRGlzdGFuY2UgdGhlIG1vdXNlIGhhcyB0byB0cmF2ZWwgdG8gc3RhcnQgZHJhZ2dpbmdcclxuICAgIGRpc3RhbmNlOiAwLFxyXG4gICAgLy8gVGltZSBpbiBtaWxsaXNlY29uZHMgYWZ0ZXIgbW91c2Vkb3duIHVudGlsIGRyYWdnaW5nIHNob3VsZCBzdGFydC5cclxuICAgIC8vIFRoaXMgb3B0aW9uIGNhbiBiZSB1c2VkIHRvIHByZXZlbnQgdW53YW50ZWQgZHJhZ3Mgd2hlbiBjbGlja2luZyBvbiBhbiBlbGVtZW50LlxyXG4gICAgZGVsYXk6IDAsXHJcbiAgICAvLyBUaGUgY3NzIHNlbGVjdG9yIG9mIHRoZSBkcmFnIGhhbmRsZVxyXG4gICAgaGFuZGxlOiBcIlwiLFxyXG4gICAgLy8gVGhlIGV4YWN0IGNzcyBwYXRoIGJldHdlZW4gdGhlIGl0ZW0gYW5kIGl0cyBzdWJjb250YWluZXJzLlxyXG4gICAgLy8gSXQgc2hvdWxkIG9ubHkgbWF0Y2ggdGhlIGltbWVkaWF0ZSBpdGVtcyBvZiBhIGNvbnRhaW5lci5cclxuICAgIC8vIE5vIGl0ZW0gb2YgYSBzdWJjb250YWluZXIgc2hvdWxkIGJlIG1hdGNoZWQuIEUuZy4gZm9yIG9sPmRpdj5saSB0aGUgaXRlbVBhdGggaXMgXCI+IGRpdlwiXHJcbiAgICBpdGVtUGF0aDogXCJcIixcclxuICAgIC8vIFRoZSBjc3Mgc2VsZWN0b3Igb2YgdGhlIGl0ZW1zXHJcbiAgICBpdGVtU2VsZWN0b3I6IFwibGlcIixcclxuICAgIC8vIFRoZSBjbGFzcyBnaXZlbiB0byBcImJvZHlcIiB3aGlsZSBhbiBpdGVtIGlzIGJlaW5nIGRyYWdnZWRcclxuICAgIGJvZHlDbGFzczogXCJkcmFnZ2luZ1wiLFxyXG4gICAgLy8gVGhlIGNsYXNzIGdpdmluZyB0byBhbiBpdGVtIHdoaWxlIGJlaW5nIGRyYWdnZWRcclxuICAgIGRyYWdnZWRDbGFzczogXCJkcmFnZ2VkXCIsXHJcbiAgICAvLyBDaGVjayBpZiB0aGUgZHJhZ2dlZCBpdGVtIG1heSBiZSBpbnNpZGUgdGhlIGNvbnRhaW5lci5cclxuICAgIC8vIFVzZSB3aXRoIGNhcmUsIHNpbmNlIHRoZSBzZWFyY2ggZm9yIGEgdmFsaWQgY29udGFpbmVyIGVudGFpbHMgYSBkZXB0aCBmaXJzdCBzZWFyY2hcclxuICAgIC8vIGFuZCBtYXkgYmUgcXVpdGUgZXhwZW5zaXZlLlxyXG4gICAgaXNWYWxpZFRhcmdldDogZnVuY3Rpb24gKCRpdGVtLCBjb250YWluZXIpIHtcclxuICAgICAgcmV0dXJuIHRydWVcclxuICAgIH0sXHJcbiAgICAvLyBFeGVjdXRlZCBiZWZvcmUgb25Ecm9wIGlmIHBsYWNlaG9sZGVyIGlzIGRldGFjaGVkLlxyXG4gICAgLy8gVGhpcyBoYXBwZW5zIGlmIHB1bGxQbGFjZWhvbGRlciBpcyBzZXQgdG8gZmFsc2UgYW5kIHRoZSBkcm9wIG9jY3VycyBvdXRzaWRlIGEgY29udGFpbmVyLlxyXG4gICAgb25DYW5jZWw6IGZ1bmN0aW9uICgkaXRlbSwgY29udGFpbmVyLCBfc3VwZXIsIGV2ZW50KSB7XHJcbiAgICB9LFxyXG4gICAgLy8gRXhlY3V0ZWQgYXQgdGhlIGJlZ2lubmluZyBvZiBhIG1vdXNlIG1vdmUgZXZlbnQuXHJcbiAgICAvLyBUaGUgUGxhY2Vob2xkZXIgaGFzIG5vdCBiZWVuIG1vdmVkIHlldC5cclxuICAgIG9uRHJhZzogZnVuY3Rpb24gKCRpdGVtLCBwb3NpdGlvbiwgX3N1cGVyLCBldmVudCkge1xyXG4gICAgICAkaXRlbS5jc3MocG9zaXRpb24pXHJcbiAgICB9LFxyXG4gICAgLy8gQ2FsbGVkIGFmdGVyIHRoZSBkcmFnIGhhcyBiZWVuIHN0YXJ0ZWQsXHJcbiAgICAvLyB0aGF0IGlzIHRoZSBtb3VzZSBidXR0b24gaXMgYmVpbmcgaGVsZCBkb3duIGFuZFxyXG4gICAgLy8gdGhlIG1vdXNlIGlzIG1vdmluZy5cclxuICAgIC8vIFRoZSBjb250YWluZXIgaXMgdGhlIGNsb3Nlc3QgaW5pdGlhbGl6ZWQgY29udGFpbmVyLlxyXG4gICAgLy8gVGhlcmVmb3JlIGl0IG1pZ2h0IG5vdCBiZSB0aGUgY29udGFpbmVyLCB0aGF0IGFjdHVhbGx5IGNvbnRhaW5zIHRoZSBpdGVtLlxyXG4gICAgb25EcmFnU3RhcnQ6IGZ1bmN0aW9uICgkaXRlbSwgY29udGFpbmVyLCBfc3VwZXIsIGV2ZW50KSB7XHJcbiAgICAgICRpdGVtLmNzcyh7XHJcbiAgICAgICAgaGVpZ2h0OiAkaXRlbS5vdXRlckhlaWdodCgpLFxyXG4gICAgICAgIHdpZHRoOiAkaXRlbS5vdXRlcldpZHRoKClcclxuICAgICAgfSlcclxuICAgICAgJGl0ZW0uYWRkQ2xhc3MoY29udGFpbmVyLmdyb3VwLm9wdGlvbnMuZHJhZ2dlZENsYXNzKVxyXG4gICAgICAkKFwiYm9keVwiKS5hZGRDbGFzcyhjb250YWluZXIuZ3JvdXAub3B0aW9ucy5ib2R5Q2xhc3MpXHJcbiAgICB9LFxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIG1vdXNlIGJ1dHRvbiBpcyBiZWluZyByZWxlYXNlZFxyXG4gICAgb25Ecm9wOiBmdW5jdGlvbiAoJGl0ZW0sIGNvbnRhaW5lciwgX3N1cGVyLCBldmVudCkge1xyXG4gICAgICAkaXRlbS5yZW1vdmVDbGFzcyhjb250YWluZXIuZ3JvdXAub3B0aW9ucy5kcmFnZ2VkQ2xhc3MpLnJlbW92ZUF0dHIoXCJzdHlsZVwiKVxyXG4gICAgICAkKFwiYm9keVwiKS5yZW1vdmVDbGFzcyhjb250YWluZXIuZ3JvdXAub3B0aW9ucy5ib2R5Q2xhc3MpXHJcbiAgICB9LFxyXG4gICAgLy8gQ2FsbGVkIG9uIG1vdXNlZG93bi4gSWYgZmFsc3kgdmFsdWUgaXMgcmV0dXJuZWQsIHRoZSBkcmFnZ2luZyB3aWxsIG5vdCBzdGFydC5cclxuICAgIC8vIElnbm9yZSBpZiBlbGVtZW50IGNsaWNrZWQgaXMgaW5wdXQsIHNlbGVjdCBvciB0ZXh0YXJlYVxyXG4gICAgb25Nb3VzZWRvd246IGZ1bmN0aW9uICgkaXRlbSwgX3N1cGVyLCBldmVudCkge1xyXG4gICAgICBpZiAoIWV2ZW50LnRhcmdldC5ub2RlTmFtZS5tYXRjaCgvXihpbnB1dHxzZWxlY3R8dGV4dGFyZWEpJC9pKSkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcclxuICAgICAgICByZXR1cm4gdHJ1ZVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgLy8gVGhlIGNsYXNzIG9mIHRoZSBwbGFjZWhvbGRlciAobXVzdCBtYXRjaCBwbGFjZWhvbGRlciBvcHRpb24gbWFya3VwKVxyXG4gICAgcGxhY2Vob2xkZXJDbGFzczogXCJwbGFjZWhvbGRlclwiLFxyXG4gICAgLy8gVGVtcGxhdGUgZm9yIHRoZSBwbGFjZWhvbGRlci4gQ2FuIGJlIGFueSB2YWxpZCBqUXVlcnkgaW5wdXRcclxuICAgIC8vIGUuZy4gYSBzdHJpbmcsIGEgRE9NIGVsZW1lbnQuXHJcbiAgICAvLyBUaGUgcGxhY2Vob2xkZXIgbXVzdCBoYXZlIHRoZSBjbGFzcyBcInBsYWNlaG9sZGVyXCJcclxuICAgIHBsYWNlaG9sZGVyOiAnPGxpIGNsYXNzPVwicGxhY2Vob2xkZXJcIj48L2xpPicsXHJcbiAgICAvLyBJZiB0cnVlLCB0aGUgcG9zaXRpb24gb2YgdGhlIHBsYWNlaG9sZGVyIGlzIGNhbGN1bGF0ZWQgb24gZXZlcnkgbW91c2Vtb3ZlLlxyXG4gICAgLy8gSWYgZmFsc2UsIGl0IGlzIG9ubHkgY2FsY3VsYXRlZCB3aGVuIHRoZSBtb3VzZSBpcyBhYm92ZSBhIGNvbnRhaW5lci5cclxuICAgIHB1bGxQbGFjZWhvbGRlcjogdHJ1ZSxcclxuICAgIC8vIFNwZWNpZmllcyBzZXJpYWxpemF0aW9uIG9mIHRoZSBjb250YWluZXIgZ3JvdXAuXHJcbiAgICAvLyBUaGUgcGFpciAkcGFyZW50LyRjaGlsZHJlbiBpcyBlaXRoZXIgY29udGFpbmVyL2l0ZW1zIG9yIGl0ZW0vc3ViY29udGFpbmVycy5cclxuICAgIHNlcmlhbGl6ZTogZnVuY3Rpb24gKCRwYXJlbnQsICRjaGlsZHJlbiwgcGFyZW50SXNDb250YWluZXIpIHtcclxuICAgICAgdmFyIHJlc3VsdCA9ICQuZXh0ZW5kKHt9LCAkcGFyZW50LmRhdGEoKSlcclxuXHJcbiAgICAgIGlmKHBhcmVudElzQ29udGFpbmVyKVxyXG4gICAgICAgIHJldHVybiBbJGNoaWxkcmVuXVxyXG4gICAgICBlbHNlIGlmICgkY2hpbGRyZW5bMF0pe1xyXG4gICAgICAgIHJlc3VsdC5jaGlsZHJlbiA9ICRjaGlsZHJlblxyXG4gICAgICB9XHJcblxyXG4gICAgICBkZWxldGUgcmVzdWx0LnN1YkNvbnRhaW5lcnNcclxuICAgICAgZGVsZXRlIHJlc3VsdC5zb3J0YWJsZVxyXG5cclxuICAgICAgcmV0dXJuIHJlc3VsdFxyXG4gICAgfSxcclxuICAgIC8vIFNldCB0b2xlcmFuY2Ugd2hpbGUgZHJhZ2dpbmcuIFBvc2l0aXZlIHZhbHVlcyBkZWNyZWFzZSBzZW5zaXRpdml0eSxcclxuICAgIC8vIG5lZ2F0aXZlIHZhbHVlcyBpbmNyZWFzZSBpdC5cclxuICAgIHRvbGVyYW5jZTogMFxyXG4gIH0sIC8vIGVuZCBncm91cCBkZWZhdWx0c1xyXG4gIGNvbnRhaW5lckdyb3VwcyA9IHt9LFxyXG4gIGdyb3VwQ291bnRlciA9IDAsXHJcbiAgZW1wdHlCb3ggPSB7XHJcbiAgICBsZWZ0OiAwLFxyXG4gICAgdG9wOiAwLFxyXG4gICAgYm90dG9tOiAwLFxyXG4gICAgcmlnaHQ6MFxyXG4gIH0sXHJcbiAgZXZlbnROYW1lcyA9IHtcclxuICAgIHN0YXJ0OiBcInRvdWNoc3RhcnQuc29ydGFibGUgbW91c2Vkb3duLnNvcnRhYmxlXCIsXHJcbiAgICBkcm9wOiBcInRvdWNoZW5kLnNvcnRhYmxlIHRvdWNoY2FuY2VsLnNvcnRhYmxlIG1vdXNldXAuc29ydGFibGVcIixcclxuICAgIGRyYWc6IFwidG91Y2htb3ZlLnNvcnRhYmxlIG1vdXNlbW92ZS5zb3J0YWJsZVwiLFxyXG4gICAgc2Nyb2xsOiBcInNjcm9sbC5zb3J0YWJsZVwiXHJcbiAgfSxcclxuICBzdWJDb250YWluZXJLZXkgPSBcInN1YkNvbnRhaW5lcnNcIlxyXG5cclxuICAvKlxyXG4gICAqIGEgaXMgQXJyYXkgW2xlZnQsIHJpZ2h0LCB0b3AsIGJvdHRvbV1cclxuICAgKiBiIGlzIGFycmF5IFtsZWZ0LCB0b3BdXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gZChhLGIpIHtcclxuICAgIHZhciB4ID0gTWF0aC5tYXgoMCwgYVswXSAtIGJbMF0sIGJbMF0gLSBhWzFdKSxcclxuICAgIHkgPSBNYXRoLm1heCgwLCBhWzJdIC0gYlsxXSwgYlsxXSAtIGFbM10pXHJcbiAgICByZXR1cm4geCt5O1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gc2V0RGltZW5zaW9ucyhhcnJheSwgZGltZW5zaW9ucywgdG9sZXJhbmNlLCB1c2VPZmZzZXQpIHtcclxuICAgIHZhciBpID0gYXJyYXkubGVuZ3RoLFxyXG4gICAgb2Zmc2V0TWV0aG9kID0gdXNlT2Zmc2V0ID8gXCJvZmZzZXRcIiA6IFwicG9zaXRpb25cIlxyXG4gICAgdG9sZXJhbmNlID0gdG9sZXJhbmNlIHx8IDBcclxuXHJcbiAgICB3aGlsZShpLS0pe1xyXG4gICAgICB2YXIgZWwgPSBhcnJheVtpXS5lbCA/IGFycmF5W2ldLmVsIDogJChhcnJheVtpXSksXHJcbiAgICAgIC8vIHVzZSBmaXR0aW5nIG1ldGhvZFxyXG4gICAgICBwb3MgPSBlbFtvZmZzZXRNZXRob2RdKClcclxuICAgICAgcG9zLmxlZnQgKz0gcGFyc2VJbnQoZWwuY3NzKCdtYXJnaW4tbGVmdCcpLCAxMClcclxuICAgICAgcG9zLnRvcCArPSBwYXJzZUludChlbC5jc3MoJ21hcmdpbi10b3AnKSwxMClcclxuICAgICAgZGltZW5zaW9uc1tpXSA9IFtcclxuICAgICAgICBwb3MubGVmdCAtIHRvbGVyYW5jZSxcclxuICAgICAgICBwb3MubGVmdCArIGVsLm91dGVyV2lkdGgoKSArIHRvbGVyYW5jZSxcclxuICAgICAgICBwb3MudG9wIC0gdG9sZXJhbmNlLFxyXG4gICAgICAgIHBvcy50b3AgKyBlbC5vdXRlckhlaWdodCgpICsgdG9sZXJhbmNlXHJcbiAgICAgIF1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGdldFJlbGF0aXZlUG9zaXRpb24ocG9pbnRlciwgZWxlbWVudCkge1xyXG4gICAgdmFyIG9mZnNldCA9IGVsZW1lbnQub2Zmc2V0KClcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGxlZnQ6IHBvaW50ZXIubGVmdCAtIG9mZnNldC5sZWZ0LFxyXG4gICAgICB0b3A6IHBvaW50ZXIudG9wIC0gb2Zmc2V0LnRvcFxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gc29ydEJ5RGlzdGFuY2VEZXNjKGRpbWVuc2lvbnMsIHBvaW50ZXIsIGxhc3RQb2ludGVyKSB7XHJcbiAgICBwb2ludGVyID0gW3BvaW50ZXIubGVmdCwgcG9pbnRlci50b3BdXHJcbiAgICBsYXN0UG9pbnRlciA9IGxhc3RQb2ludGVyICYmIFtsYXN0UG9pbnRlci5sZWZ0LCBsYXN0UG9pbnRlci50b3BdXHJcblxyXG4gICAgdmFyIGRpbSxcclxuICAgIGkgPSBkaW1lbnNpb25zLmxlbmd0aCxcclxuICAgIGRpc3RhbmNlcyA9IFtdXHJcblxyXG4gICAgd2hpbGUoaS0tKXtcclxuICAgICAgZGltID0gZGltZW5zaW9uc1tpXVxyXG4gICAgICBkaXN0YW5jZXNbaV0gPSBbaSxkKGRpbSxwb2ludGVyKSwgbGFzdFBvaW50ZXIgJiYgZChkaW0sIGxhc3RQb2ludGVyKV1cclxuICAgIH1cclxuICAgIGRpc3RhbmNlcyA9IGRpc3RhbmNlcy5zb3J0KGZ1bmN0aW9uICAoYSxiKSB7XHJcbiAgICAgIHJldHVybiBiWzFdIC0gYVsxXSB8fCBiWzJdIC0gYVsyXSB8fCBiWzBdIC0gYVswXVxyXG4gICAgfSlcclxuXHJcbiAgICAvLyBsYXN0IGVudHJ5IGlzIHRoZSBjbG9zZXN0XHJcbiAgICByZXR1cm4gZGlzdGFuY2VzXHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBDb250YWluZXJHcm91cChvcHRpb25zKSB7XHJcbiAgICB0aGlzLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgZ3JvdXBEZWZhdWx0cywgb3B0aW9ucylcclxuICAgIHRoaXMuY29udGFpbmVycyA9IFtdXHJcblxyXG4gICAgaWYoIXRoaXMub3B0aW9ucy5yb290R3JvdXApe1xyXG4gICAgICB0aGlzLnNjcm9sbFByb3h5ID0gJC5wcm94eSh0aGlzLnNjcm9sbCwgdGhpcylcclxuICAgICAgdGhpcy5kcmFnUHJveHkgPSAkLnByb3h5KHRoaXMuZHJhZywgdGhpcylcclxuICAgICAgdGhpcy5kcm9wUHJveHkgPSAkLnByb3h5KHRoaXMuZHJvcCwgdGhpcylcclxuICAgICAgdGhpcy5wbGFjZWhvbGRlciA9ICQodGhpcy5vcHRpb25zLnBsYWNlaG9sZGVyKVxyXG5cclxuICAgICAgaWYoIW9wdGlvbnMuaXNWYWxpZFRhcmdldClcclxuICAgICAgICB0aGlzLm9wdGlvbnMuaXNWYWxpZFRhcmdldCA9IHVuZGVmaW5lZFxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQ29udGFpbmVyR3JvdXAuZ2V0ID0gZnVuY3Rpb24gIChvcHRpb25zKSB7XHJcbiAgICBpZighY29udGFpbmVyR3JvdXBzW29wdGlvbnMuZ3JvdXBdKSB7XHJcbiAgICAgIGlmKG9wdGlvbnMuZ3JvdXAgPT09IHVuZGVmaW5lZClcclxuICAgICAgICBvcHRpb25zLmdyb3VwID0gZ3JvdXBDb3VudGVyICsrXHJcblxyXG4gICAgICBjb250YWluZXJHcm91cHNbb3B0aW9ucy5ncm91cF0gPSBuZXcgQ29udGFpbmVyR3JvdXAob3B0aW9ucylcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gY29udGFpbmVyR3JvdXBzW29wdGlvbnMuZ3JvdXBdXHJcbiAgfVxyXG5cclxuICBDb250YWluZXJHcm91cC5wcm90b3R5cGUgPSB7XHJcbiAgICBkcmFnSW5pdDogZnVuY3Rpb24gIChlLCBpdGVtQ29udGFpbmVyKSB7XHJcbiAgICAgIHRoaXMuJGRvY3VtZW50ID0gJChpdGVtQ29udGFpbmVyLmVsWzBdLm93bmVyRG9jdW1lbnQpXHJcblxyXG4gICAgICAvLyBnZXQgaXRlbSB0byBkcmFnXHJcbiAgICAgIHZhciBjbG9zZXN0SXRlbSA9ICQoZS50YXJnZXQpLmNsb3Nlc3QodGhpcy5vcHRpb25zLml0ZW1TZWxlY3Rvcik7XHJcbiAgICAgIC8vIHVzaW5nIHRoZSBsZW5ndGggb2YgdGhpcyBpdGVtLCBwcmV2ZW50cyB0aGUgcGx1Z2luIGZyb20gYmVpbmcgc3RhcnRlZCBpZiB0aGVyZSBpcyBubyBoYW5kbGUgYmVpbmcgY2xpY2tlZCBvbi5cclxuICAgICAgLy8gdGhpcyBtYXkgYWxzbyBiZSBoZWxwZnVsIGluIGluc3RhbnRpYXRpbmcgbXVsdGlkcmFnLlxyXG4gICAgICBpZiAoY2xvc2VzdEl0ZW0ubGVuZ3RoKSB7XHJcbiAgICAgICAgdGhpcy5pdGVtID0gY2xvc2VzdEl0ZW07XHJcbiAgICAgICAgdGhpcy5pdGVtQ29udGFpbmVyID0gaXRlbUNvbnRhaW5lcjtcclxuICAgICAgICBpZiAodGhpcy5pdGVtLmlzKHRoaXMub3B0aW9ucy5leGNsdWRlKSB8fCAhdGhpcy5vcHRpb25zLm9uTW91c2Vkb3duKHRoaXMuaXRlbSwgZ3JvdXBEZWZhdWx0cy5vbk1vdXNlZG93biwgZSkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFBvaW50ZXIoZSk7XHJcbiAgICAgICAgdGhpcy50b2dnbGVMaXN0ZW5lcnMoJ29uJyk7XHJcbiAgICAgICAgdGhpcy5zZXR1cERlbGF5VGltZXIoKTtcclxuICAgICAgICB0aGlzLmRyYWdJbml0RG9uZSA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBkcmFnOiBmdW5jdGlvbiAgKGUpIHtcclxuICAgICAgaWYoIXRoaXMuZHJhZ2dpbmcpe1xyXG4gICAgICAgIGlmKCF0aGlzLmRpc3RhbmNlTWV0KGUpIHx8ICF0aGlzLmRlbGF5TWV0KVxyXG4gICAgICAgICAgcmV0dXJuXHJcblxyXG4gICAgICAgIHRoaXMub3B0aW9ucy5vbkRyYWdTdGFydCh0aGlzLml0ZW0sIHRoaXMuaXRlbUNvbnRhaW5lciwgZ3JvdXBEZWZhdWx0cy5vbkRyYWdTdGFydCwgZSlcclxuICAgICAgICB0aGlzLml0ZW0uYmVmb3JlKHRoaXMucGxhY2Vob2xkZXIpXHJcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IHRydWVcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5zZXRQb2ludGVyKGUpXHJcbiAgICAgIC8vIHBsYWNlIGl0ZW0gdW5kZXIgdGhlIGN1cnNvclxyXG4gICAgICB0aGlzLm9wdGlvbnMub25EcmFnKHRoaXMuaXRlbSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBnZXRSZWxhdGl2ZVBvc2l0aW9uKHRoaXMucG9pbnRlciwgdGhpcy5pdGVtLm9mZnNldFBhcmVudCgpKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBncm91cERlZmF1bHRzLm9uRHJhZyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBlKVxyXG5cclxuICAgICAgdmFyIHAgPSB0aGlzLmdldFBvaW50ZXIoZSksXHJcbiAgICAgIGJveCA9IHRoaXMuc2FtZVJlc3VsdEJveCxcclxuICAgICAgdCA9IHRoaXMub3B0aW9ucy50b2xlcmFuY2VcclxuXHJcbiAgICAgIGlmKCFib3ggfHwgYm94LnRvcCAtIHQgPiBwLnRvcCB8fCBib3guYm90dG9tICsgdCA8IHAudG9wIHx8IGJveC5sZWZ0IC0gdCA+IHAubGVmdCB8fCBib3gucmlnaHQgKyB0IDwgcC5sZWZ0KVxyXG4gICAgICAgIGlmKCF0aGlzLnNlYXJjaFZhbGlkVGFyZ2V0KCkpe1xyXG4gICAgICAgICAgdGhpcy5wbGFjZWhvbGRlci5kZXRhY2goKVxyXG4gICAgICAgICAgdGhpcy5sYXN0QXBwZW5kZWRJdGVtID0gdW5kZWZpbmVkXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRyb3A6IGZ1bmN0aW9uICAoZSkge1xyXG4gICAgICB0aGlzLnRvZ2dsZUxpc3RlbmVycygnb2ZmJylcclxuXHJcbiAgICAgIHRoaXMuZHJhZ0luaXREb25lID0gZmFsc2VcclxuXHJcbiAgICAgIGlmKHRoaXMuZHJhZ2dpbmcpe1xyXG4gICAgICAgIC8vIHByb2Nlc3NpbmcgRHJvcCwgY2hlY2sgaWYgcGxhY2Vob2xkZXIgaXMgZGV0YWNoZWRcclxuICAgICAgICBpZih0aGlzLnBsYWNlaG9sZGVyLmNsb3Nlc3QoXCJodG1sXCIpWzBdKXtcclxuICAgICAgICAgIHRoaXMucGxhY2Vob2xkZXIuYmVmb3JlKHRoaXMuaXRlbSkuZGV0YWNoKClcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5vcHRpb25zLm9uQ2FuY2VsKHRoaXMuaXRlbSwgdGhpcy5pdGVtQ29udGFpbmVyLCBncm91cERlZmF1bHRzLm9uQ2FuY2VsLCBlKVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9wdGlvbnMub25Ecm9wKHRoaXMuaXRlbSwgdGhpcy5nZXRDb250YWluZXIodGhpcy5pdGVtKSwgZ3JvdXBEZWZhdWx0cy5vbkRyb3AsIGUpXHJcblxyXG4gICAgICAgIC8vIGNsZWFudXBcclxuICAgICAgICB0aGlzLmNsZWFyRGltZW5zaW9ucygpXHJcbiAgICAgICAgdGhpcy5jbGVhck9mZnNldFBhcmVudCgpXHJcbiAgICAgICAgdGhpcy5sYXN0QXBwZW5kZWRJdGVtID0gdGhpcy5zYW1lUmVzdWx0Qm94ID0gdW5kZWZpbmVkXHJcbiAgICAgICAgdGhpcy5kcmFnZ2luZyA9IGZhbHNlXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBzZWFyY2hWYWxpZFRhcmdldDogZnVuY3Rpb24gIChwb2ludGVyLCBsYXN0UG9pbnRlcikge1xyXG4gICAgICBpZighcG9pbnRlcil7XHJcbiAgICAgICAgcG9pbnRlciA9IHRoaXMucmVsYXRpdmVQb2ludGVyIHx8IHRoaXMucG9pbnRlclxyXG4gICAgICAgIGxhc3RQb2ludGVyID0gdGhpcy5sYXN0UmVsYXRpdmVQb2ludGVyIHx8IHRoaXMubGFzdFBvaW50ZXJcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGRpc3RhbmNlcyA9IHNvcnRCeURpc3RhbmNlRGVzYyh0aGlzLmdldENvbnRhaW5lckRpbWVuc2lvbnMoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb2ludGVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhc3RQb2ludGVyKSxcclxuICAgICAgaSA9IGRpc3RhbmNlcy5sZW5ndGhcclxuXHJcbiAgICAgIHdoaWxlKGktLSl7XHJcbiAgICAgICAgdmFyIGluZGV4ID0gZGlzdGFuY2VzW2ldWzBdLFxyXG4gICAgICAgIGRpc3RhbmNlID0gZGlzdGFuY2VzW2ldWzFdXHJcblxyXG4gICAgICAgIGlmKCFkaXN0YW5jZSB8fCB0aGlzLm9wdGlvbnMucHVsbFBsYWNlaG9sZGVyKXtcclxuICAgICAgICAgIHZhciBjb250YWluZXIgPSB0aGlzLmNvbnRhaW5lcnNbaW5kZXhdXHJcbiAgICAgICAgICBpZighY29udGFpbmVyLmRpc2FibGVkKXtcclxuICAgICAgICAgICAgaWYoIXRoaXMuJGdldE9mZnNldFBhcmVudCgpKXtcclxuICAgICAgICAgICAgICB2YXIgb2Zmc2V0UGFyZW50ID0gY29udGFpbmVyLmdldEl0ZW1PZmZzZXRQYXJlbnQoKVxyXG4gICAgICAgICAgICAgIHBvaW50ZXIgPSBnZXRSZWxhdGl2ZVBvc2l0aW9uKHBvaW50ZXIsIG9mZnNldFBhcmVudClcclxuICAgICAgICAgICAgICBsYXN0UG9pbnRlciA9IGdldFJlbGF0aXZlUG9zaXRpb24obGFzdFBvaW50ZXIsIG9mZnNldFBhcmVudClcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZihjb250YWluZXIuc2VhcmNoVmFsaWRUYXJnZXQocG9pbnRlciwgbGFzdFBvaW50ZXIpKVxyXG4gICAgICAgICAgICAgIHJldHVybiB0cnVlXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGlmKHRoaXMuc2FtZVJlc3VsdEJveClcclxuICAgICAgICB0aGlzLnNhbWVSZXN1bHRCb3ggPSB1bmRlZmluZWRcclxuICAgIH0sXHJcbiAgICBtb3ZlUGxhY2Vob2xkZXI6IGZ1bmN0aW9uICAoY29udGFpbmVyLCBpdGVtLCBtZXRob2QsIHNhbWVSZXN1bHRCb3gpIHtcclxuICAgICAgdmFyIGxhc3RBcHBlbmRlZEl0ZW0gPSB0aGlzLmxhc3RBcHBlbmRlZEl0ZW1cclxuICAgICAgaWYoIXNhbWVSZXN1bHRCb3ggJiYgbGFzdEFwcGVuZGVkSXRlbSAmJiBsYXN0QXBwZW5kZWRJdGVtWzBdID09PSBpdGVtWzBdKVxyXG4gICAgICAgIHJldHVybjtcclxuXHJcbiAgICAgIGl0ZW1bbWV0aG9kXSh0aGlzLnBsYWNlaG9sZGVyKVxyXG4gICAgICB0aGlzLmxhc3RBcHBlbmRlZEl0ZW0gPSBpdGVtXHJcbiAgICAgIHRoaXMuc2FtZVJlc3VsdEJveCA9IHNhbWVSZXN1bHRCb3hcclxuICAgICAgdGhpcy5vcHRpb25zLmFmdGVyTW92ZSh0aGlzLnBsYWNlaG9sZGVyLCBjb250YWluZXIsIGl0ZW0pXHJcbiAgICB9LFxyXG4gICAgZ2V0Q29udGFpbmVyRGltZW5zaW9uczogZnVuY3Rpb24gICgpIHtcclxuICAgICAgaWYoIXRoaXMuY29udGFpbmVyRGltZW5zaW9ucylcclxuICAgICAgICBzZXREaW1lbnNpb25zKHRoaXMuY29udGFpbmVycywgdGhpcy5jb250YWluZXJEaW1lbnNpb25zID0gW10sIHRoaXMub3B0aW9ucy50b2xlcmFuY2UsICF0aGlzLiRnZXRPZmZzZXRQYXJlbnQoKSlcclxuICAgICAgcmV0dXJuIHRoaXMuY29udGFpbmVyRGltZW5zaW9uc1xyXG4gICAgfSxcclxuICAgIGdldENvbnRhaW5lcjogZnVuY3Rpb24gIChlbGVtZW50KSB7XHJcbiAgICAgIHJldHVybiBlbGVtZW50LmNsb3Nlc3QodGhpcy5vcHRpb25zLmNvbnRhaW5lclNlbGVjdG9yKS5kYXRhKHBsdWdpbk5hbWUpXHJcbiAgICB9LFxyXG4gICAgJGdldE9mZnNldFBhcmVudDogZnVuY3Rpb24gICgpIHtcclxuICAgICAgaWYodGhpcy5vZmZzZXRQYXJlbnQgPT09IHVuZGVmaW5lZCl7XHJcbiAgICAgICAgdmFyIGkgPSB0aGlzLmNvbnRhaW5lcnMubGVuZ3RoIC0gMSxcclxuICAgICAgICBvZmZzZXRQYXJlbnQgPSB0aGlzLmNvbnRhaW5lcnNbaV0uZ2V0SXRlbU9mZnNldFBhcmVudCgpXHJcblxyXG4gICAgICAgIGlmKCF0aGlzLm9wdGlvbnMucm9vdEdyb3VwKXtcclxuICAgICAgICAgIHdoaWxlKGktLSl7XHJcbiAgICAgICAgICAgIGlmKG9mZnNldFBhcmVudFswXSAhPSB0aGlzLmNvbnRhaW5lcnNbaV0uZ2V0SXRlbU9mZnNldFBhcmVudCgpWzBdKXtcclxuICAgICAgICAgICAgICAvLyBJZiBldmVyeSBjb250YWluZXIgaGFzIHRoZSBzYW1lIG9mZnNldCBwYXJlbnQsXHJcbiAgICAgICAgICAgICAgLy8gdXNlIHBvc2l0aW9uKCkgd2hpY2ggaXMgcmVsYXRpdmUgdG8gdGhpcyBwYXJlbnQsXHJcbiAgICAgICAgICAgICAgLy8gb3RoZXJ3aXNlIHVzZSBvZmZzZXQoKVxyXG4gICAgICAgICAgICAgIC8vIGNvbXBhcmUgI3NldERpbWVuc2lvbnNcclxuICAgICAgICAgICAgICBvZmZzZXRQYXJlbnQgPSBmYWxzZVxyXG4gICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm9mZnNldFBhcmVudCA9IG9mZnNldFBhcmVudFxyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzLm9mZnNldFBhcmVudFxyXG4gICAgfSxcclxuICAgIHNldFBvaW50ZXI6IGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgIHZhciBwb2ludGVyID0gdGhpcy5nZXRQb2ludGVyKGUpXHJcblxyXG4gICAgICBpZih0aGlzLiRnZXRPZmZzZXRQYXJlbnQoKSl7XHJcbiAgICAgICAgdmFyIHJlbGF0aXZlUG9pbnRlciA9IGdldFJlbGF0aXZlUG9zaXRpb24ocG9pbnRlciwgdGhpcy4kZ2V0T2Zmc2V0UGFyZW50KCkpXHJcbiAgICAgICAgdGhpcy5sYXN0UmVsYXRpdmVQb2ludGVyID0gdGhpcy5yZWxhdGl2ZVBvaW50ZXJcclxuICAgICAgICB0aGlzLnJlbGF0aXZlUG9pbnRlciA9IHJlbGF0aXZlUG9pbnRlclxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLmxhc3RQb2ludGVyID0gdGhpcy5wb2ludGVyXHJcbiAgICAgIHRoaXMucG9pbnRlciA9IHBvaW50ZXJcclxuICAgIH0sXHJcbiAgICBkaXN0YW5jZU1ldDogZnVuY3Rpb24gKGUpIHtcclxuICAgICAgdmFyIGN1cnJlbnRQb2ludGVyID0gdGhpcy5nZXRQb2ludGVyKGUpXHJcbiAgICAgIHJldHVybiAoTWF0aC5tYXgoXHJcbiAgICAgICAgTWF0aC5hYnModGhpcy5wb2ludGVyLmxlZnQgLSBjdXJyZW50UG9pbnRlci5sZWZ0KSxcclxuICAgICAgICBNYXRoLmFicyh0aGlzLnBvaW50ZXIudG9wIC0gY3VycmVudFBvaW50ZXIudG9wKVxyXG4gICAgICApID49IHRoaXMub3B0aW9ucy5kaXN0YW5jZSlcclxuICAgIH0sXHJcbiAgICBnZXRQb2ludGVyOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgIHZhciBvID0gZS5vcmlnaW5hbEV2ZW50IHx8IGUub3JpZ2luYWxFdmVudC50b3VjaGVzICYmIGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdXHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgbGVmdDogZS5wYWdlWCB8fCBvLnBhZ2VYLFxyXG4gICAgICAgIHRvcDogZS5wYWdlWSB8fCBvLnBhZ2VZXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBzZXR1cERlbGF5VGltZXI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyIHRoYXQgPSB0aGlzXHJcbiAgICAgIHRoaXMuZGVsYXlNZXQgPSAhdGhpcy5vcHRpb25zLmRlbGF5XHJcblxyXG4gICAgICAvLyBpbml0IGRlbGF5IHRpbWVyIGlmIG5lZWRlZFxyXG4gICAgICBpZiAoIXRoaXMuZGVsYXlNZXQpIHtcclxuICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5fbW91c2VEZWxheVRpbWVyKTtcclxuICAgICAgICB0aGlzLl9tb3VzZURlbGF5VGltZXIgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgdGhhdC5kZWxheU1ldCA9IHRydWVcclxuICAgICAgICB9LCB0aGlzLm9wdGlvbnMuZGVsYXkpXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBzY3JvbGw6IGZ1bmN0aW9uICAoZSkge1xyXG4gICAgICB0aGlzLmNsZWFyRGltZW5zaW9ucygpXHJcbiAgICAgIHRoaXMuY2xlYXJPZmZzZXRQYXJlbnQoKSAvLyBUT0RPIGlzIHRoaXMgbmVlZGVkP1xyXG4gICAgfSxcclxuICAgIHRvZ2dsZUxpc3RlbmVyczogZnVuY3Rpb24gKG1ldGhvZCkge1xyXG4gICAgICB2YXIgdGhhdCA9IHRoaXMsXHJcbiAgICAgIGV2ZW50cyA9IFsnZHJhZycsJ2Ryb3AnLCdzY3JvbGwnXVxyXG5cclxuICAgICAgJC5lYWNoKGV2ZW50cyxmdW5jdGlvbiAgKGksZXZlbnQpIHtcclxuICAgICAgICB0aGF0LiRkb2N1bWVudFttZXRob2RdKGV2ZW50TmFtZXNbZXZlbnRdLCB0aGF0W2V2ZW50ICsgJ1Byb3h5J10pXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgY2xlYXJPZmZzZXRQYXJlbnQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdGhpcy5vZmZzZXRQYXJlbnQgPSB1bmRlZmluZWRcclxuICAgIH0sXHJcbiAgICAvLyBSZWN1cnNpdmVseSBjbGVhciBjb250YWluZXIgYW5kIGl0ZW0gZGltZW5zaW9uc1xyXG4gICAgY2xlYXJEaW1lbnNpb25zOiBmdW5jdGlvbiAgKCkge1xyXG4gICAgICB0aGlzLnRyYXZlcnNlKGZ1bmN0aW9uKG9iamVjdCl7XHJcbiAgICAgICAgb2JqZWN0Ll9jbGVhckRpbWVuc2lvbnMoKVxyXG4gICAgICB9KVxyXG4gICAgfSxcclxuICAgIHRyYXZlcnNlOiBmdW5jdGlvbihjYWxsYmFjaykge1xyXG4gICAgICBjYWxsYmFjayh0aGlzKVxyXG4gICAgICB2YXIgaSA9IHRoaXMuY29udGFpbmVycy5sZW5ndGhcclxuICAgICAgd2hpbGUoaS0tKXtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lcnNbaV0udHJhdmVyc2UoY2FsbGJhY2spXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBfY2xlYXJEaW1lbnNpb25zOiBmdW5jdGlvbigpe1xyXG4gICAgICB0aGlzLmNvbnRhaW5lckRpbWVuc2lvbnMgPSB1bmRlZmluZWRcclxuICAgIH0sXHJcbiAgICBfZGVzdHJveTogZnVuY3Rpb24gKCkge1xyXG4gICAgICBjb250YWluZXJHcm91cHNbdGhpcy5vcHRpb25zLmdyb3VwXSA9IHVuZGVmaW5lZFxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gQ29udGFpbmVyKGVsZW1lbnQsIG9wdGlvbnMpIHtcclxuICAgIHRoaXMuZWwgPSBlbGVtZW50XHJcbiAgICB0aGlzLm9wdGlvbnMgPSAkLmV4dGVuZCgge30sIGNvbnRhaW5lckRlZmF1bHRzLCBvcHRpb25zKVxyXG5cclxuICAgIHRoaXMuZ3JvdXAgPSBDb250YWluZXJHcm91cC5nZXQodGhpcy5vcHRpb25zKVxyXG4gICAgdGhpcy5yb290R3JvdXAgPSB0aGlzLm9wdGlvbnMucm9vdEdyb3VwIHx8IHRoaXMuZ3JvdXBcclxuICAgIHRoaXMuaGFuZGxlID0gdGhpcy5yb290R3JvdXAub3B0aW9ucy5oYW5kbGUgfHwgdGhpcy5yb290R3JvdXAub3B0aW9ucy5pdGVtU2VsZWN0b3JcclxuXHJcbiAgICB2YXIgaXRlbVBhdGggPSB0aGlzLnJvb3RHcm91cC5vcHRpb25zLml0ZW1QYXRoXHJcbiAgICB0aGlzLnRhcmdldCA9IGl0ZW1QYXRoID8gdGhpcy5lbC5maW5kKGl0ZW1QYXRoKSA6IHRoaXMuZWxcclxuXHJcbiAgICB0aGlzLnRhcmdldC5vbihldmVudE5hbWVzLnN0YXJ0LCB0aGlzLmhhbmRsZSwgJC5wcm94eSh0aGlzLmRyYWdJbml0LCB0aGlzKSlcclxuXHJcbiAgICBpZih0aGlzLm9wdGlvbnMuZHJvcClcclxuICAgICAgdGhpcy5ncm91cC5jb250YWluZXJzLnB1c2godGhpcylcclxuICB9XHJcblxyXG4gIENvbnRhaW5lci5wcm90b3R5cGUgPSB7XHJcbiAgICBkcmFnSW5pdDogZnVuY3Rpb24gIChlKSB7XHJcbiAgICAgIHZhciByb290R3JvdXAgPSB0aGlzLnJvb3RHcm91cFxyXG5cclxuICAgICAgaWYoICF0aGlzLmRpc2FibGVkICYmXHJcbiAgICAgICAgICAhcm9vdEdyb3VwLmRyYWdJbml0RG9uZSAmJlxyXG4gICAgICAgICAgdGhpcy5vcHRpb25zLmRyYWcgJiZcclxuICAgICAgICAgIHRoaXMuaXNWYWxpZERyYWcoZSkpIHtcclxuICAgICAgICByb290R3JvdXAuZHJhZ0luaXQoZSwgdGhpcylcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGlzVmFsaWREcmFnOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgIHJldHVybiBlLndoaWNoID09IDEgfHxcclxuICAgICAgICBlLnR5cGUgPT0gXCJ0b3VjaHN0YXJ0XCIgJiYgZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXMubGVuZ3RoID09IDFcclxuICAgIH0sXHJcbiAgICBzZWFyY2hWYWxpZFRhcmdldDogZnVuY3Rpb24gIChwb2ludGVyLCBsYXN0UG9pbnRlcikge1xyXG4gICAgICB2YXIgZGlzdGFuY2VzID0gc29ydEJ5RGlzdGFuY2VEZXNjKHRoaXMuZ2V0SXRlbURpbWVuc2lvbnMoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb2ludGVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhc3RQb2ludGVyKSxcclxuICAgICAgaSA9IGRpc3RhbmNlcy5sZW5ndGgsXHJcbiAgICAgIHJvb3RHcm91cCA9IHRoaXMucm9vdEdyb3VwLFxyXG4gICAgICB2YWxpZFRhcmdldCA9ICFyb290R3JvdXAub3B0aW9ucy5pc1ZhbGlkVGFyZ2V0IHx8XHJcbiAgICAgICAgcm9vdEdyb3VwLm9wdGlvbnMuaXNWYWxpZFRhcmdldChyb290R3JvdXAuaXRlbSwgdGhpcylcclxuXHJcbiAgICAgIGlmKCFpICYmIHZhbGlkVGFyZ2V0KXtcclxuICAgICAgICByb290R3JvdXAubW92ZVBsYWNlaG9sZGVyKHRoaXMsIHRoaXMudGFyZ2V0LCBcImFwcGVuZFwiKVxyXG4gICAgICAgIHJldHVybiB0cnVlXHJcbiAgICAgIH0gZWxzZVxyXG4gICAgICAgIHdoaWxlKGktLSl7XHJcbiAgICAgICAgICB2YXIgaW5kZXggPSBkaXN0YW5jZXNbaV1bMF0sXHJcbiAgICAgICAgICBkaXN0YW5jZSA9IGRpc3RhbmNlc1tpXVsxXVxyXG4gICAgICAgICAgaWYoIWRpc3RhbmNlICYmIHRoaXMuaGFzQ2hpbGRHcm91cChpbmRleCkpe1xyXG4gICAgICAgICAgICB2YXIgZm91bmQgPSB0aGlzLmdldENvbnRhaW5lckdyb3VwKGluZGV4KS5zZWFyY2hWYWxpZFRhcmdldChwb2ludGVyLCBsYXN0UG9pbnRlcilcclxuICAgICAgICAgICAgaWYoZm91bmQpXHJcbiAgICAgICAgICAgICAgcmV0dXJuIHRydWVcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGVsc2UgaWYodmFsaWRUYXJnZXQpe1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVQbGFjZWhvbGRlcihpbmRleCwgcG9pbnRlcilcclxuICAgICAgICAgICAgcmV0dXJuIHRydWVcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbW92ZVBsYWNlaG9sZGVyOiBmdW5jdGlvbiAgKGluZGV4LCBwb2ludGVyKSB7XHJcbiAgICAgIHZhciBpdGVtID0gJCh0aGlzLml0ZW1zW2luZGV4XSksXHJcbiAgICAgIGRpbSA9IHRoaXMuaXRlbURpbWVuc2lvbnNbaW5kZXhdLFxyXG4gICAgICBtZXRob2QgPSBcImFmdGVyXCIsXHJcbiAgICAgIHdpZHRoID0gaXRlbS5vdXRlcldpZHRoKCksXHJcbiAgICAgIGhlaWdodCA9IGl0ZW0ub3V0ZXJIZWlnaHQoKSxcclxuICAgICAgb2Zmc2V0ID0gaXRlbS5vZmZzZXQoKSxcclxuICAgICAgc2FtZVJlc3VsdEJveCA9IHtcclxuICAgICAgICBsZWZ0OiBvZmZzZXQubGVmdCxcclxuICAgICAgICByaWdodDogb2Zmc2V0LmxlZnQgKyB3aWR0aCxcclxuICAgICAgICB0b3A6IG9mZnNldC50b3AsXHJcbiAgICAgICAgYm90dG9tOiBvZmZzZXQudG9wICsgaGVpZ2h0XHJcbiAgICAgIH1cclxuICAgICAgaWYodGhpcy5vcHRpb25zLnZlcnRpY2FsKXtcclxuICAgICAgICB2YXIgeUNlbnRlciA9IChkaW1bMl0gKyBkaW1bM10pIC8gMixcclxuICAgICAgICBpblVwcGVySGFsZiA9IHBvaW50ZXIudG9wIDw9IHlDZW50ZXJcclxuICAgICAgICBpZihpblVwcGVySGFsZil7XHJcbiAgICAgICAgICBtZXRob2QgPSBcImJlZm9yZVwiXHJcbiAgICAgICAgICBzYW1lUmVzdWx0Qm94LmJvdHRvbSAtPSBoZWlnaHQgLyAyXHJcbiAgICAgICAgfSBlbHNlXHJcbiAgICAgICAgICBzYW1lUmVzdWx0Qm94LnRvcCArPSBoZWlnaHQgLyAyXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdmFyIHhDZW50ZXIgPSAoZGltWzBdICsgZGltWzFdKSAvIDIsXHJcbiAgICAgICAgaW5MZWZ0SGFsZiA9IHBvaW50ZXIubGVmdCA8PSB4Q2VudGVyXHJcbiAgICAgICAgaWYoaW5MZWZ0SGFsZil7XHJcbiAgICAgICAgICBtZXRob2QgPSBcImJlZm9yZVwiXHJcbiAgICAgICAgICBzYW1lUmVzdWx0Qm94LnJpZ2h0IC09IHdpZHRoIC8gMlxyXG4gICAgICAgIH0gZWxzZVxyXG4gICAgICAgICAgc2FtZVJlc3VsdEJveC5sZWZ0ICs9IHdpZHRoIC8gMlxyXG4gICAgICB9XHJcbiAgICAgIGlmKHRoaXMuaGFzQ2hpbGRHcm91cChpbmRleCkpXHJcbiAgICAgICAgc2FtZVJlc3VsdEJveCA9IGVtcHR5Qm94XHJcbiAgICAgIHRoaXMucm9vdEdyb3VwLm1vdmVQbGFjZWhvbGRlcih0aGlzLCBpdGVtLCBtZXRob2QsIHNhbWVSZXN1bHRCb3gpXHJcbiAgICB9LFxyXG4gICAgZ2V0SXRlbURpbWVuc2lvbnM6IGZ1bmN0aW9uICAoKSB7XHJcbiAgICAgIGlmKCF0aGlzLml0ZW1EaW1lbnNpb25zKXtcclxuICAgICAgICB0aGlzLml0ZW1zID0gdGhpcy4kZ2V0Q2hpbGRyZW4odGhpcy5lbCwgXCJpdGVtXCIpLmZpbHRlcihcclxuICAgICAgICAgIFwiOm5vdCguXCIgKyB0aGlzLmdyb3VwLm9wdGlvbnMucGxhY2Vob2xkZXJDbGFzcyArIFwiLCAuXCIgKyB0aGlzLmdyb3VwLm9wdGlvbnMuZHJhZ2dlZENsYXNzICsgXCIpXCJcclxuICAgICAgICApLmdldCgpXHJcbiAgICAgICAgc2V0RGltZW5zaW9ucyh0aGlzLml0ZW1zLCB0aGlzLml0ZW1EaW1lbnNpb25zID0gW10sIHRoaXMub3B0aW9ucy50b2xlcmFuY2UpXHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHRoaXMuaXRlbURpbWVuc2lvbnNcclxuICAgIH0sXHJcbiAgICBnZXRJdGVtT2Zmc2V0UGFyZW50OiBmdW5jdGlvbiAgKCkge1xyXG4gICAgICB2YXIgb2Zmc2V0UGFyZW50LFxyXG4gICAgICBlbCA9IHRoaXMuZWxcclxuICAgICAgLy8gU2luY2UgZWwgbWlnaHQgYmUgZW1wdHkgd2UgaGF2ZSB0byBjaGVjayBlbCBpdHNlbGYgYW5kXHJcbiAgICAgIC8vIGNhbiBub3QgZG8gc29tZXRoaW5nIGxpa2UgZWwuY2hpbGRyZW4oKS5maXJzdCgpLm9mZnNldFBhcmVudCgpXHJcbiAgICAgIGlmKGVsLmNzcyhcInBvc2l0aW9uXCIpID09PSBcInJlbGF0aXZlXCIgfHwgZWwuY3NzKFwicG9zaXRpb25cIikgPT09IFwiYWJzb2x1dGVcIiAgfHwgZWwuY3NzKFwicG9zaXRpb25cIikgPT09IFwiZml4ZWRcIilcclxuICAgICAgICBvZmZzZXRQYXJlbnQgPSBlbFxyXG4gICAgICBlbHNlXHJcbiAgICAgICAgb2Zmc2V0UGFyZW50ID0gZWwub2Zmc2V0UGFyZW50KClcclxuICAgICAgcmV0dXJuIG9mZnNldFBhcmVudFxyXG4gICAgfSxcclxuICAgIGhhc0NoaWxkR3JvdXA6IGZ1bmN0aW9uIChpbmRleCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5vcHRpb25zLm5lc3RlZCAmJiB0aGlzLmdldENvbnRhaW5lckdyb3VwKGluZGV4KVxyXG4gICAgfSxcclxuICAgIGdldENvbnRhaW5lckdyb3VwOiBmdW5jdGlvbiAgKGluZGV4KSB7XHJcbiAgICAgIHZhciBjaGlsZEdyb3VwID0gJC5kYXRhKHRoaXMuaXRlbXNbaW5kZXhdLCBzdWJDb250YWluZXJLZXkpXHJcbiAgICAgIGlmKCBjaGlsZEdyb3VwID09PSB1bmRlZmluZWQpe1xyXG4gICAgICAgIHZhciBjaGlsZENvbnRhaW5lcnMgPSB0aGlzLiRnZXRDaGlsZHJlbih0aGlzLml0ZW1zW2luZGV4XSwgXCJjb250YWluZXJcIilcclxuICAgICAgICBjaGlsZEdyb3VwID0gZmFsc2VcclxuXHJcbiAgICAgICAgaWYoY2hpbGRDb250YWluZXJzWzBdKXtcclxuICAgICAgICAgIHZhciBvcHRpb25zID0gJC5leHRlbmQoe30sIHRoaXMub3B0aW9ucywge1xyXG4gICAgICAgICAgICByb290R3JvdXA6IHRoaXMucm9vdEdyb3VwLFxyXG4gICAgICAgICAgICBncm91cDogZ3JvdXBDb3VudGVyICsrXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgY2hpbGRHcm91cCA9IGNoaWxkQ29udGFpbmVyc1twbHVnaW5OYW1lXShvcHRpb25zKS5kYXRhKHBsdWdpbk5hbWUpLmdyb3VwXHJcbiAgICAgICAgfVxyXG4gICAgICAgICQuZGF0YSh0aGlzLml0ZW1zW2luZGV4XSwgc3ViQ29udGFpbmVyS2V5LCBjaGlsZEdyb3VwKVxyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBjaGlsZEdyb3VwXHJcbiAgICB9LFxyXG4gICAgJGdldENoaWxkcmVuOiBmdW5jdGlvbiAocGFyZW50LCB0eXBlKSB7XHJcbiAgICAgIHZhciBvcHRpb25zID0gdGhpcy5yb290R3JvdXAub3B0aW9ucyxcclxuICAgICAgcGF0aCA9IG9wdGlvbnNbdHlwZSArIFwiUGF0aFwiXSxcclxuICAgICAgc2VsZWN0b3IgPSBvcHRpb25zW3R5cGUgKyBcIlNlbGVjdG9yXCJdXHJcblxyXG4gICAgICBwYXJlbnQgPSAkKHBhcmVudClcclxuICAgICAgaWYocGF0aClcclxuICAgICAgICBwYXJlbnQgPSBwYXJlbnQuZmluZChwYXRoKVxyXG5cclxuICAgICAgcmV0dXJuIHBhcmVudC5jaGlsZHJlbihzZWxlY3RvcilcclxuICAgIH0sXHJcbiAgICBfc2VyaWFsaXplOiBmdW5jdGlvbiAocGFyZW50LCBpc0NvbnRhaW5lcikge1xyXG4gICAgICB2YXIgdGhhdCA9IHRoaXMsXHJcbiAgICAgIGNoaWxkVHlwZSA9IGlzQ29udGFpbmVyID8gXCJpdGVtXCIgOiBcImNvbnRhaW5lclwiLFxyXG5cclxuICAgICAgY2hpbGRyZW4gPSB0aGlzLiRnZXRDaGlsZHJlbihwYXJlbnQsIGNoaWxkVHlwZSkubm90KHRoaXMub3B0aW9ucy5leGNsdWRlKS5tYXAoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB0aGF0Ll9zZXJpYWxpemUoJCh0aGlzKSwgIWlzQ29udGFpbmVyKVxyXG4gICAgICB9KS5nZXQoKVxyXG5cclxuICAgICAgcmV0dXJuIHRoaXMucm9vdEdyb3VwLm9wdGlvbnMuc2VyaWFsaXplKHBhcmVudCwgY2hpbGRyZW4sIGlzQ29udGFpbmVyKVxyXG4gICAgfSxcclxuICAgIHRyYXZlcnNlOiBmdW5jdGlvbihjYWxsYmFjaykge1xyXG4gICAgICAkLmVhY2godGhpcy5pdGVtcyB8fCBbXSwgZnVuY3Rpb24oaXRlbSl7XHJcbiAgICAgICAgdmFyIGdyb3VwID0gJC5kYXRhKHRoaXMsIHN1YkNvbnRhaW5lcktleSlcclxuICAgICAgICBpZihncm91cClcclxuICAgICAgICAgIGdyb3VwLnRyYXZlcnNlKGNhbGxiYWNrKVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGNhbGxiYWNrKHRoaXMpXHJcbiAgICB9LFxyXG4gICAgX2NsZWFyRGltZW5zaW9uczogZnVuY3Rpb24gICgpIHtcclxuICAgICAgdGhpcy5pdGVtRGltZW5zaW9ucyA9IHVuZGVmaW5lZFxyXG4gICAgfSxcclxuICAgIF9kZXN0cm95OiBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIHRoYXQgPSB0aGlzO1xyXG5cclxuICAgICAgdGhpcy50YXJnZXQub2ZmKGV2ZW50TmFtZXMuc3RhcnQsIHRoaXMuaGFuZGxlKTtcclxuICAgICAgdGhpcy5lbC5yZW1vdmVEYXRhKHBsdWdpbk5hbWUpXHJcblxyXG4gICAgICBpZih0aGlzLm9wdGlvbnMuZHJvcClcclxuICAgICAgICB0aGlzLmdyb3VwLmNvbnRhaW5lcnMgPSAkLmdyZXAodGhpcy5ncm91cC5jb250YWluZXJzLCBmdW5jdGlvbih2YWwpe1xyXG4gICAgICAgICAgcmV0dXJuIHZhbCAhPSB0aGF0XHJcbiAgICAgICAgfSlcclxuXHJcbiAgICAgICQuZWFjaCh0aGlzLml0ZW1zIHx8IFtdLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICQucmVtb3ZlRGF0YSh0aGlzLCBzdWJDb250YWluZXJLZXkpXHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB2YXIgQVBJID0ge1xyXG4gICAgZW5hYmxlOiBmdW5jdGlvbigpIHtcclxuICAgICAgdGhpcy50cmF2ZXJzZShmdW5jdGlvbihvYmplY3Qpe1xyXG4gICAgICAgIG9iamVjdC5kaXNhYmxlZCA9IGZhbHNlXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgZGlzYWJsZTogZnVuY3Rpb24gKCl7XHJcbiAgICAgIHRoaXMudHJhdmVyc2UoZnVuY3Rpb24ob2JqZWN0KXtcclxuICAgICAgICBvYmplY3QuZGlzYWJsZWQgPSB0cnVlXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgc2VyaWFsaXplOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLl9zZXJpYWxpemUodGhpcy5lbCwgdHJ1ZSlcclxuICAgIH0sXHJcbiAgICByZWZyZXNoOiBmdW5jdGlvbigpIHtcclxuICAgICAgdGhpcy50cmF2ZXJzZShmdW5jdGlvbihvYmplY3Qpe1xyXG4gICAgICAgIG9iamVjdC5fY2xlYXJEaW1lbnNpb25zKClcclxuICAgICAgfSlcclxuICAgIH0sXHJcbiAgICBkZXN0cm95OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHRoaXMudHJhdmVyc2UoZnVuY3Rpb24ob2JqZWN0KXtcclxuICAgICAgICBvYmplY3QuX2Rlc3Ryb3koKTtcclxuICAgICAgfSlcclxuICAgIH1cclxuICB9XHJcblxyXG4gICQuZXh0ZW5kKENvbnRhaW5lci5wcm90b3R5cGUsIEFQSSlcclxuXHJcbiAgLyoqXHJcbiAgICogalF1ZXJ5IEFQSVxyXG4gICAqXHJcbiAgICogUGFyYW1ldGVycyBhcmVcclxuICAgKiAgIGVpdGhlciBvcHRpb25zIG9uIGluaXRcclxuICAgKiAgIG9yIGEgbWV0aG9kIG5hbWUgZm9sbG93ZWQgYnkgYXJndW1lbnRzIHRvIHBhc3MgdG8gdGhlIG1ldGhvZFxyXG4gICAqL1xyXG4gICQuZm5bcGx1Z2luTmFtZV0gPSBmdW5jdGlvbihtZXRob2RPck9wdGlvbnMpIHtcclxuICAgIHZhciBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKVxyXG5cclxuICAgIHJldHVybiB0aGlzLm1hcChmdW5jdGlvbigpe1xyXG4gICAgICB2YXIgJHQgPSAkKHRoaXMpLFxyXG4gICAgICBvYmplY3QgPSAkdC5kYXRhKHBsdWdpbk5hbWUpXHJcblxyXG4gICAgICBpZihvYmplY3QgJiYgQVBJW21ldGhvZE9yT3B0aW9uc10pXHJcbiAgICAgICAgcmV0dXJuIEFQSVttZXRob2RPck9wdGlvbnNdLmFwcGx5KG9iamVjdCwgYXJncykgfHwgdGhpc1xyXG4gICAgICBlbHNlIGlmKCFvYmplY3QgJiYgKG1ldGhvZE9yT3B0aW9ucyA9PT0gdW5kZWZpbmVkIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZW9mIG1ldGhvZE9yT3B0aW9ucyA9PT0gXCJvYmplY3RcIikpXHJcbiAgICAgICAgJHQuZGF0YShwbHVnaW5OYW1lLCBuZXcgQ29udGFpbmVyKCR0LCBtZXRob2RPck9wdGlvbnMpKVxyXG5cclxuICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH0pO1xyXG4gIH07XHJcblxyXG59KGpRdWVyeSwgd2luZG93LCAnc29ydGFibGUnKTtcclxuIiwiLypcclxuICogUmVwbGFjZSBhbGwgU1ZHIGltYWdlcyB3aXRoIGlubGluZSBTVkdcclxuICovXHJcbmpRdWVyeSgnaW1nLnN2ZycpLmVhY2goZnVuY3Rpb24oKXtcclxuICAgIHZhciAkaW1nID0galF1ZXJ5KHRoaXMpO1xyXG4gICAgdmFyIGltZ0lEID0gJGltZy5hdHRyKCdpZCcpO1xyXG4gICAgdmFyIGltZ0NsYXNzID0gJGltZy5hdHRyKCdjbGFzcycpO1xyXG4gICAgdmFyIGltZ1VSTCA9ICRpbWcuYXR0cignc3JjJyk7XHJcblxyXG4gICAgalF1ZXJ5LmdldChpbWdVUkwsIGZ1bmN0aW9uKGRhdGEpIHtcclxuICAgICAgICAvLyBHZXQgdGhlIFNWRyB0YWcsIGlnbm9yZSB0aGUgcmVzdFxyXG4gICAgICAgIHZhciAkc3ZnID0galF1ZXJ5KGRhdGEpLmZpbmQoJ3N2ZycpO1xyXG5cclxuICAgICAgICAvLyBBZGQgcmVwbGFjZWQgaW1hZ2UncyBJRCB0byB0aGUgbmV3IFNWR1xyXG4gICAgICAgIGlmKHR5cGVvZiBpbWdJRCAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgJHN2ZyA9ICRzdmcuYXR0cignaWQnLCBpbWdJRCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIEFkZCByZXBsYWNlZCBpbWFnZSdzIGNsYXNzZXMgdG8gdGhlIG5ldyBTVkdcclxuICAgICAgICBpZih0eXBlb2YgaW1nQ2xhc3MgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgICRzdmcgPSAkc3ZnLmF0dHIoJ2NsYXNzJywgaW1nQ2xhc3MrJyByZXBsYWNlZC1zdmcnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFJlbW92ZSBhbnkgaW52YWxpZCBYTUwgdGFncyBhcyBwZXIgaHR0cDovL3ZhbGlkYXRvci53My5vcmdcclxuICAgICAgICAkc3ZnID0gJHN2Zy5yZW1vdmVBdHRyKCd4bWxuczphJyk7XHJcblxyXG4gICAgICAgIC8vIFJlcGxhY2UgaW1hZ2Ugd2l0aCBuZXcgU1ZHXHJcbiAgICAgICAgJGltZy5yZXBsYWNlV2l0aCgkc3ZnKTtcclxuXHJcbiAgICB9LCAneG1sJyk7XHJcblxyXG59KTsiLCIvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4vLyBmYW5jeUJveCB2My41LjdcclxuLy9cclxuLy8gTGljZW5zZWQgR1BMdjMgZm9yIG9wZW4gc291cmNlIHVzZVxyXG4vLyBvciBmYW5jeUJveCBDb21tZXJjaWFsIExpY2Vuc2UgZm9yIGNvbW1lcmNpYWwgdXNlXHJcbi8vXHJcbi8vIGh0dHA6Ly9mYW5jeWFwcHMuY29tL2ZhbmN5Ym94L1xyXG4vLyBDb3B5cmlnaHQgMjAxOSBmYW5jeUFwcHNcclxuLy9cclxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuIWZ1bmN0aW9uKHQsZSxuLG8pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGkodCxlKXt2YXIgbyxpLGEscz1bXSxyPTA7dCYmdC5pc0RlZmF1bHRQcmV2ZW50ZWQoKXx8KHQucHJldmVudERlZmF1bHQoKSxlPWV8fHt9LHQmJnQuZGF0YSYmKGU9aCh0LmRhdGEub3B0aW9ucyxlKSksbz1lLiR0YXJnZXR8fG4odC5jdXJyZW50VGFyZ2V0KS50cmlnZ2VyKFwiYmx1clwiKSwoYT1uLmZhbmN5Ym94LmdldEluc3RhbmNlKCkpJiZhLiR0cmlnZ2VyJiZhLiR0cmlnZ2VyLmlzKG8pfHwoZS5zZWxlY3Rvcj9zPW4oZS5zZWxlY3Rvcik6KGk9by5hdHRyKFwiZGF0YS1mYW5jeWJveFwiKXx8XCJcIixpPyhzPXQuZGF0YT90LmRhdGEuaXRlbXM6W10scz1zLmxlbmd0aD9zLmZpbHRlcignW2RhdGEtZmFuY3lib3g9XCInK2krJ1wiXScpOm4oJ1tkYXRhLWZhbmN5Ym94PVwiJytpKydcIl0nKSk6cz1bb10pLHI9bihzKS5pbmRleChvKSxyPDAmJihyPTApLGE9bi5mYW5jeWJveC5vcGVuKHMsZSxyKSxhLiR0cmlnZ2VyPW8pKX1pZih0LmNvbnNvbGU9dC5jb25zb2xlfHx7aW5mbzpmdW5jdGlvbih0KXt9fSxuKXtpZihuLmZuLmZhbmN5Ym94KXJldHVybiB2b2lkIGNvbnNvbGUuaW5mbyhcImZhbmN5Qm94IGFscmVhZHkgaW5pdGlhbGl6ZWRcIik7dmFyIGE9e2Nsb3NlRXhpc3Rpbmc6ITEsbG9vcDohMSxndXR0ZXI6NTAsa2V5Ym9hcmQ6ITAscHJldmVudENhcHRpb25PdmVybGFwOiEwLGFycm93czohMCxpbmZvYmFyOiEwLHNtYWxsQnRuOlwiYXV0b1wiLHRvb2xiYXI6XCJhdXRvXCIsYnV0dG9uczpbXCJ6b29tXCIsXCJzbGlkZVNob3dcIixcInRodW1ic1wiLFwiY2xvc2VcIl0saWRsZVRpbWU6Myxwcm90ZWN0OiExLG1vZGFsOiExLGltYWdlOntwcmVsb2FkOiExfSxhamF4OntzZXR0aW5nczp7ZGF0YTp7ZmFuY3lib3g6ITB9fX0saWZyYW1lOnt0cGw6JzxpZnJhbWUgaWQ9XCJmYW5jeWJveC1mcmFtZXtybmR9XCIgbmFtZT1cImZhbmN5Ym94LWZyYW1le3JuZH1cIiBjbGFzcz1cImZhbmN5Ym94LWlmcmFtZVwiIGFsbG93ZnVsbHNjcmVlbj1cImFsbG93ZnVsbHNjcmVlblwiIGFsbG93PVwiYXV0b3BsYXk7IGZ1bGxzY3JlZW5cIiBzcmM9XCJcIj48L2lmcmFtZT4nLHByZWxvYWQ6ITAsY3NzOnt9LGF0dHI6e3Njcm9sbGluZzpcImF1dG9cIn19LHZpZGVvOnt0cGw6Jzx2aWRlbyBjbGFzcz1cImZhbmN5Ym94LXZpZGVvXCIgY29udHJvbHMgY29udHJvbHNMaXN0PVwibm9kb3dubG9hZFwiIHBvc3Rlcj1cInt7cG9zdGVyfX1cIj48c291cmNlIHNyYz1cInt7c3JjfX1cIiB0eXBlPVwie3tmb3JtYXR9fVwiIC8+U29ycnksIHlvdXIgYnJvd3NlciBkb2VzblxcJ3Qgc3VwcG9ydCBlbWJlZGRlZCB2aWRlb3MsIDxhIGhyZWY9XCJ7e3NyY319XCI+ZG93bmxvYWQ8L2E+IGFuZCB3YXRjaCB3aXRoIHlvdXIgZmF2b3JpdGUgdmlkZW8gcGxheWVyITwvdmlkZW8+Jyxmb3JtYXQ6XCJcIixhdXRvU3RhcnQ6ITB9LGRlZmF1bHRUeXBlOlwiaW1hZ2VcIixhbmltYXRpb25FZmZlY3Q6XCJ6b29tXCIsYW5pbWF0aW9uRHVyYXRpb246MzY2LHpvb21PcGFjaXR5OlwiYXV0b1wiLHRyYW5zaXRpb25FZmZlY3Q6XCJmYWRlXCIsdHJhbnNpdGlvbkR1cmF0aW9uOjM2NixzbGlkZUNsYXNzOlwiXCIsYmFzZUNsYXNzOlwiXCIsYmFzZVRwbDonPGRpdiBjbGFzcz1cImZhbmN5Ym94LWNvbnRhaW5lclwiIHJvbGU9XCJkaWFsb2dcIiB0YWJpbmRleD1cIi0xXCI+PGRpdiBjbGFzcz1cImZhbmN5Ym94LWJnXCI+PC9kaXY+PGRpdiBjbGFzcz1cImZhbmN5Ym94LWlubmVyXCI+PGRpdiBjbGFzcz1cImZhbmN5Ym94LWluZm9iYXJcIj48c3BhbiBkYXRhLWZhbmN5Ym94LWluZGV4Pjwvc3Bhbj4mbmJzcDsvJm5ic3A7PHNwYW4gZGF0YS1mYW5jeWJveC1jb3VudD48L3NwYW4+PC9kaXY+PGRpdiBjbGFzcz1cImZhbmN5Ym94LXRvb2xiYXJcIj57e2J1dHRvbnN9fTwvZGl2PjxkaXYgY2xhc3M9XCJmYW5jeWJveC1uYXZpZ2F0aW9uXCI+e3thcnJvd3N9fTwvZGl2PjxkaXYgY2xhc3M9XCJmYW5jeWJveC1zdGFnZVwiPjwvZGl2PjxkaXYgY2xhc3M9XCJmYW5jeWJveC1jYXB0aW9uXCI+PGRpdiBjbGFzcz1cImZhbmN5Ym94LWNhcHRpb25fX2JvZHlcIj48L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj4nLHNwaW5uZXJUcGw6JzxkaXYgY2xhc3M9XCJmYW5jeWJveC1sb2FkaW5nXCI+PC9kaXY+JyxlcnJvclRwbDonPGRpdiBjbGFzcz1cImZhbmN5Ym94LWVycm9yXCI+PHA+e3tFUlJPUn19PC9wPjwvZGl2PicsYnRuVHBsOntkb3dubG9hZDonPGEgZG93bmxvYWQgZGF0YS1mYW5jeWJveC1kb3dubG9hZCBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLWRvd25sb2FkXCIgdGl0bGU9XCJ7e0RPV05MT0FEfX1cIiBocmVmPVwiamF2YXNjcmlwdDo7XCI+PHN2ZyB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIgdmlld0JveD1cIjAgMCAyNCAyNFwiPjxwYXRoIGQ9XCJNMTguNjIgMTcuMDlWMTlINS4zOHYtMS45MXptLTIuOTctNi45NkwxNyAxMS40NWwtNSA0Ljg3LTUtNC44NyAxLjM2LTEuMzIgMi42OCAyLjY0VjVoMS45MnY3Ljc3elwiLz48L3N2Zz48L2E+Jyx6b29tOic8YnV0dG9uIGRhdGEtZmFuY3lib3gtem9vbSBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLXpvb21cIiB0aXRsZT1cInt7Wk9PTX19XCI+PHN2ZyB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIgdmlld0JveD1cIjAgMCAyNCAyNFwiPjxwYXRoIGQ9XCJNMTguNyAxNy4zbC0zLTNhNS45IDUuOSAwIDAgMC0uNi03LjYgNS45IDUuOSAwIDAgMC04LjQgMCA1LjkgNS45IDAgMCAwIDAgOC40IDUuOSA1LjkgMCAwIDAgNy43LjdsMyAzYTEgMSAwIDAgMCAxLjMgMGMuNC0uNS40LTEgMC0xLjV6TTguMSAxMy44YTQgNCAwIDAgMSAwLTUuNyA0IDQgMCAwIDEgNS43IDAgNCA0IDAgMCAxIDAgNS43IDQgNCAwIDAgMS01LjcgMHpcIi8+PC9zdmc+PC9idXR0b24+JyxjbG9zZTonPGJ1dHRvbiBkYXRhLWZhbmN5Ym94LWNsb3NlIGNsYXNzPVwiZmFuY3lib3gtYnV0dG9uIGZhbmN5Ym94LWJ1dHRvbi0tY2xvc2VcIiB0aXRsZT1cInt7Q0xPU0V9fVwiPjxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHZpZXdCb3g9XCIwIDAgMjQgMjRcIj48cGF0aCBkPVwiTTEyIDEwLjZMNi42IDUuMiA1LjIgNi42bDUuNCA1LjQtNS40IDUuNCAxLjQgMS40IDUuNC01LjQgNS40IDUuNCAxLjQtMS40LTUuNC01LjQgNS40LTUuNC0xLjQtMS40LTUuNCA1LjR6XCIvPjwvc3ZnPjwvYnV0dG9uPicsYXJyb3dMZWZ0Oic8YnV0dG9uIGRhdGEtZmFuY3lib3gtcHJldiBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLWFycm93X2xlZnRcIiB0aXRsZT1cInt7UFJFVn19XCI+PGRpdj48c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB2aWV3Qm94PVwiMCAwIDI0IDI0XCI+PHBhdGggZD1cIk0xMS4yOCAxNS43bC0xLjM0IDEuMzdMNSAxMmw0Ljk0LTUuMDcgMS4zNCAxLjM4LTIuNjggMi43MkgxOXYxLjk0SDguNnpcIi8+PC9zdmc+PC9kaXY+PC9idXR0b24+JyxhcnJvd1JpZ2h0Oic8YnV0dG9uIGRhdGEtZmFuY3lib3gtbmV4dCBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLWFycm93X3JpZ2h0XCIgdGl0bGU9XCJ7e05FWFR9fVwiPjxkaXY+PHN2ZyB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIgdmlld0JveD1cIjAgMCAyNCAyNFwiPjxwYXRoIGQ9XCJNMTUuNCAxMi45N2wtMi42OCAyLjcyIDEuMzQgMS4zOEwxOSAxMmwtNC45NC01LjA3LTEuMzQgMS4zOCAyLjY4IDIuNzJINXYxLjk0elwiLz48L3N2Zz48L2Rpdj48L2J1dHRvbj4nLHNtYWxsQnRuOic8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBkYXRhLWZhbmN5Ym94LWNsb3NlIGNsYXNzPVwiZmFuY3lib3gtYnV0dG9uIGZhbmN5Ym94LWNsb3NlLXNtYWxsXCIgdGl0bGU9XCJ7e0NMT1NFfX1cIj48c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB2ZXJzaW9uPVwiMVwiIHZpZXdCb3g9XCIwIDAgMjQgMjRcIj48cGF0aCBkPVwiTTEzIDEybDUtNS0xLTEtNSA1LTUtNS0xIDEgNSA1LTUgNSAxIDEgNS01IDUgNSAxLTF6XCIvPjwvc3ZnPjwvYnV0dG9uPid9LHBhcmVudEVsOlwiYm9keVwiLGhpZGVTY3JvbGxiYXI6ITAsYXV0b0ZvY3VzOiEwLGJhY2tGb2N1czohMCx0cmFwRm9jdXM6ITAsZnVsbFNjcmVlbjp7YXV0b1N0YXJ0OiExfSx0b3VjaDp7dmVydGljYWw6ITAsbW9tZW50dW06ITB9LGhhc2g6bnVsbCxtZWRpYTp7fSxzbGlkZVNob3c6e2F1dG9TdGFydDohMSxzcGVlZDozZTN9LHRodW1iczp7YXV0b1N0YXJ0OiExLGhpZGVPbkNsb3NlOiEwLHBhcmVudEVsOlwiLmZhbmN5Ym94LWNvbnRhaW5lclwiLGF4aXM6XCJ5XCJ9LHdoZWVsOlwiYXV0b1wiLG9uSW5pdDpuLm5vb3AsYmVmb3JlTG9hZDpuLm5vb3AsYWZ0ZXJMb2FkOm4ubm9vcCxiZWZvcmVTaG93Om4ubm9vcCxhZnRlclNob3c6bi5ub29wLGJlZm9yZUNsb3NlOm4ubm9vcCxhZnRlckNsb3NlOm4ubm9vcCxvbkFjdGl2YXRlOm4ubm9vcCxvbkRlYWN0aXZhdGU6bi5ub29wLGNsaWNrQ29udGVudDpmdW5jdGlvbih0LGUpe3JldHVyblwiaW1hZ2VcIj09PXQudHlwZSYmXCJ6b29tXCJ9LGNsaWNrU2xpZGU6XCJjbG9zZVwiLGNsaWNrT3V0c2lkZTpcImNsb3NlXCIsZGJsY2xpY2tDb250ZW50OiExLGRibGNsaWNrU2xpZGU6ITEsZGJsY2xpY2tPdXRzaWRlOiExLG1vYmlsZTp7cHJldmVudENhcHRpb25PdmVybGFwOiExLGlkbGVUaW1lOiExLGNsaWNrQ29udGVudDpmdW5jdGlvbih0LGUpe3JldHVyblwiaW1hZ2VcIj09PXQudHlwZSYmXCJ0b2dnbGVDb250cm9sc1wifSxjbGlja1NsaWRlOmZ1bmN0aW9uKHQsZSl7cmV0dXJuXCJpbWFnZVwiPT09dC50eXBlP1widG9nZ2xlQ29udHJvbHNcIjpcImNsb3NlXCJ9LGRibGNsaWNrQ29udGVudDpmdW5jdGlvbih0LGUpe3JldHVyblwiaW1hZ2VcIj09PXQudHlwZSYmXCJ6b29tXCJ9LGRibGNsaWNrU2xpZGU6ZnVuY3Rpb24odCxlKXtyZXR1cm5cImltYWdlXCI9PT10LnR5cGUmJlwiem9vbVwifX0sbGFuZzpcImVuXCIsaTE4bjp7ZW46e0NMT1NFOlwiQ2xvc2VcIixORVhUOlwiTmV4dFwiLFBSRVY6XCJQcmV2aW91c1wiLEVSUk9SOlwiVGhlIHJlcXVlc3RlZCBjb250ZW50IGNhbm5vdCBiZSBsb2FkZWQuIDxici8+IFBsZWFzZSB0cnkgYWdhaW4gbGF0ZXIuXCIsUExBWV9TVEFSVDpcIlN0YXJ0IHNsaWRlc2hvd1wiLFBMQVlfU1RPUDpcIlBhdXNlIHNsaWRlc2hvd1wiLEZVTExfU0NSRUVOOlwiRnVsbCBzY3JlZW5cIixUSFVNQlM6XCJUaHVtYm5haWxzXCIsRE9XTkxPQUQ6XCJEb3dubG9hZFwiLFNIQVJFOlwiU2hhcmVcIixaT09NOlwiWm9vbVwifSxkZTp7Q0xPU0U6XCJTY2hsaWUmc3psaWc7ZW5cIixORVhUOlwiV2VpdGVyXCIsUFJFVjpcIlp1ciZ1dW1sO2NrXCIsRVJST1I6XCJEaWUgYW5nZWZvcmRlcnRlbiBEYXRlbiBrb25udGVuIG5pY2h0IGdlbGFkZW4gd2VyZGVuLiA8YnIvPiBCaXR0ZSB2ZXJzdWNoZW4gU2llIGVzIHNwJmF1bWw7dGVyIG5vY2htYWwuXCIsUExBWV9TVEFSVDpcIkRpYXNjaGF1IHN0YXJ0ZW5cIixQTEFZX1NUT1A6XCJEaWFzY2hhdSBiZWVuZGVuXCIsRlVMTF9TQ1JFRU46XCJWb2xsYmlsZFwiLFRIVU1CUzpcIlZvcnNjaGF1YmlsZGVyXCIsRE9XTkxPQUQ6XCJIZXJ1bnRlcmxhZGVuXCIsU0hBUkU6XCJUZWlsZW5cIixaT09NOlwiVmVyZ3Imb3VtbDsmc3psaWc7ZXJuXCJ9fX0scz1uKHQpLHI9bihlKSxjPTAsbD1mdW5jdGlvbih0KXtyZXR1cm4gdCYmdC5oYXNPd25Qcm9wZXJ0eSYmdCBpbnN0YW5jZW9mIG59LGQ9ZnVuY3Rpb24oKXtyZXR1cm4gdC5yZXF1ZXN0QW5pbWF0aW9uRnJhbWV8fHQud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lfHx0Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZXx8dC5vUmVxdWVzdEFuaW1hdGlvbkZyYW1lfHxmdW5jdGlvbihlKXtyZXR1cm4gdC5zZXRUaW1lb3V0KGUsMWUzLzYwKX19KCksdT1mdW5jdGlvbigpe3JldHVybiB0LmNhbmNlbEFuaW1hdGlvbkZyYW1lfHx0LndlYmtpdENhbmNlbEFuaW1hdGlvbkZyYW1lfHx0Lm1vekNhbmNlbEFuaW1hdGlvbkZyYW1lfHx0Lm9DYW5jZWxBbmltYXRpb25GcmFtZXx8ZnVuY3Rpb24oZSl7dC5jbGVhclRpbWVvdXQoZSl9fSgpLGY9ZnVuY3Rpb24oKXt2YXIgdCxuPWUuY3JlYXRlRWxlbWVudChcImZha2VlbGVtZW50XCIpLG89e3RyYW5zaXRpb246XCJ0cmFuc2l0aW9uZW5kXCIsT1RyYW5zaXRpb246XCJvVHJhbnNpdGlvbkVuZFwiLE1velRyYW5zaXRpb246XCJ0cmFuc2l0aW9uZW5kXCIsV2Via2l0VHJhbnNpdGlvbjpcIndlYmtpdFRyYW5zaXRpb25FbmRcIn07Zm9yKHQgaW4gbylpZih2b2lkIDAhPT1uLnN0eWxlW3RdKXJldHVybiBvW3RdO3JldHVyblwidHJhbnNpdGlvbmVuZFwifSgpLHA9ZnVuY3Rpb24odCl7cmV0dXJuIHQmJnQubGVuZ3RoJiZ0WzBdLm9mZnNldEhlaWdodH0saD1mdW5jdGlvbih0LGUpe3ZhciBvPW4uZXh0ZW5kKCEwLHt9LHQsZSk7cmV0dXJuIG4uZWFjaChlLGZ1bmN0aW9uKHQsZSl7bi5pc0FycmF5KGUpJiYob1t0XT1lKX0pLG99LGc9ZnVuY3Rpb24odCl7dmFyIG8saTtyZXR1cm4hKCF0fHx0Lm93bmVyRG9jdW1lbnQhPT1lKSYmKG4oXCIuZmFuY3lib3gtY29udGFpbmVyXCIpLmNzcyhcInBvaW50ZXItZXZlbnRzXCIsXCJub25lXCIpLG89e3g6dC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0K3Qub2Zmc2V0V2lkdGgvMix5OnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wK3Qub2Zmc2V0SGVpZ2h0LzJ9LGk9ZS5lbGVtZW50RnJvbVBvaW50KG8ueCxvLnkpPT09dCxuKFwiLmZhbmN5Ym94LWNvbnRhaW5lclwiKS5jc3MoXCJwb2ludGVyLWV2ZW50c1wiLFwiXCIpLGkpfSxiPWZ1bmN0aW9uKHQsZSxvKXt2YXIgaT10aGlzO2kub3B0cz1oKHtpbmRleDpvfSxuLmZhbmN5Ym94LmRlZmF1bHRzKSxuLmlzUGxhaW5PYmplY3QoZSkmJihpLm9wdHM9aChpLm9wdHMsZSkpLG4uZmFuY3lib3guaXNNb2JpbGUmJihpLm9wdHM9aChpLm9wdHMsaS5vcHRzLm1vYmlsZSkpLGkuaWQ9aS5vcHRzLmlkfHwrK2MsaS5jdXJySW5kZXg9cGFyc2VJbnQoaS5vcHRzLmluZGV4LDEwKXx8MCxpLnByZXZJbmRleD1udWxsLGkucHJldlBvcz1udWxsLGkuY3VyclBvcz0wLGkuZmlyc3RSdW49ITAsaS5ncm91cD1bXSxpLnNsaWRlcz17fSxpLmFkZENvbnRlbnQodCksaS5ncm91cC5sZW5ndGgmJmkuaW5pdCgpfTtuLmV4dGVuZChiLnByb3RvdHlwZSx7aW5pdDpmdW5jdGlvbigpe3ZhciBvLGksYT10aGlzLHM9YS5ncm91cFthLmN1cnJJbmRleF0scj1zLm9wdHM7ci5jbG9zZUV4aXN0aW5nJiZuLmZhbmN5Ym94LmNsb3NlKCEwKSxuKFwiYm9keVwiKS5hZGRDbGFzcyhcImZhbmN5Ym94LWFjdGl2ZVwiKSwhbi5mYW5jeWJveC5nZXRJbnN0YW5jZSgpJiYhMSE9PXIuaGlkZVNjcm9sbGJhciYmIW4uZmFuY3lib3guaXNNb2JpbGUmJmUuYm9keS5zY3JvbGxIZWlnaHQ+dC5pbm5lckhlaWdodCYmKG4oXCJoZWFkXCIpLmFwcGVuZCgnPHN0eWxlIGlkPVwiZmFuY3lib3gtc3R5bGUtbm9zY3JvbGxcIiB0eXBlPVwidGV4dC9jc3NcIj4uY29tcGVuc2F0ZS1mb3Itc2Nyb2xsYmFye21hcmdpbi1yaWdodDonKyh0LmlubmVyV2lkdGgtZS5kb2N1bWVudEVsZW1lbnQuY2xpZW50V2lkdGgpK1wicHg7fTwvc3R5bGU+XCIpLG4oXCJib2R5XCIpLmFkZENsYXNzKFwiY29tcGVuc2F0ZS1mb3Itc2Nyb2xsYmFyXCIpKSxpPVwiXCIsbi5lYWNoKHIuYnV0dG9ucyxmdW5jdGlvbih0LGUpe2krPXIuYnRuVHBsW2VdfHxcIlwifSksbz1uKGEudHJhbnNsYXRlKGEsci5iYXNlVHBsLnJlcGxhY2UoXCJ7e2J1dHRvbnN9fVwiLGkpLnJlcGxhY2UoXCJ7e2Fycm93c319XCIsci5idG5UcGwuYXJyb3dMZWZ0K3IuYnRuVHBsLmFycm93UmlnaHQpKSkuYXR0cihcImlkXCIsXCJmYW5jeWJveC1jb250YWluZXItXCIrYS5pZCkuYWRkQ2xhc3Moci5iYXNlQ2xhc3MpLmRhdGEoXCJGYW5jeUJveFwiLGEpLmFwcGVuZFRvKHIucGFyZW50RWwpLGEuJHJlZnM9e2NvbnRhaW5lcjpvfSxbXCJiZ1wiLFwiaW5uZXJcIixcImluZm9iYXJcIixcInRvb2xiYXJcIixcInN0YWdlXCIsXCJjYXB0aW9uXCIsXCJuYXZpZ2F0aW9uXCJdLmZvckVhY2goZnVuY3Rpb24odCl7YS4kcmVmc1t0XT1vLmZpbmQoXCIuZmFuY3lib3gtXCIrdCl9KSxhLnRyaWdnZXIoXCJvbkluaXRcIiksYS5hY3RpdmF0ZSgpLGEuanVtcFRvKGEuY3VyckluZGV4KX0sdHJhbnNsYXRlOmZ1bmN0aW9uKHQsZSl7dmFyIG49dC5vcHRzLmkxOG5bdC5vcHRzLmxhbmddfHx0Lm9wdHMuaTE4bi5lbjtyZXR1cm4gZS5yZXBsYWNlKC9cXHtcXHsoXFx3KylcXH1cXH0vZyxmdW5jdGlvbih0LGUpe3JldHVybiB2b2lkIDA9PT1uW2VdP3Q6bltlXX0pfSxhZGRDb250ZW50OmZ1bmN0aW9uKHQpe3ZhciBlLG89dGhpcyxpPW4ubWFrZUFycmF5KHQpO24uZWFjaChpLGZ1bmN0aW9uKHQsZSl7dmFyIGksYSxzLHIsYyxsPXt9LGQ9e307bi5pc1BsYWluT2JqZWN0KGUpPyhsPWUsZD1lLm9wdHN8fGUpOlwib2JqZWN0XCI9PT1uLnR5cGUoZSkmJm4oZSkubGVuZ3RoPyhpPW4oZSksZD1pLmRhdGEoKXx8e30sZD1uLmV4dGVuZCghMCx7fSxkLGQub3B0aW9ucyksZC4kb3JpZz1pLGwuc3JjPW8ub3B0cy5zcmN8fGQuc3JjfHxpLmF0dHIoXCJocmVmXCIpLGwudHlwZXx8bC5zcmN8fChsLnR5cGU9XCJpbmxpbmVcIixsLnNyYz1lKSk6bD17dHlwZTpcImh0bWxcIixzcmM6ZStcIlwifSxsLm9wdHM9bi5leHRlbmQoITAse30sby5vcHRzLGQpLG4uaXNBcnJheShkLmJ1dHRvbnMpJiYobC5vcHRzLmJ1dHRvbnM9ZC5idXR0b25zKSxuLmZhbmN5Ym94LmlzTW9iaWxlJiZsLm9wdHMubW9iaWxlJiYobC5vcHRzPWgobC5vcHRzLGwub3B0cy5tb2JpbGUpKSxhPWwudHlwZXx8bC5vcHRzLnR5cGUscj1sLnNyY3x8XCJcIiwhYSYmciYmKChzPXIubWF0Y2goL1xcLihtcDR8bW92fG9ndnx3ZWJtKSgoXFw/fCMpLiopPyQvaSkpPyhhPVwidmlkZW9cIixsLm9wdHMudmlkZW8uZm9ybWF0fHwobC5vcHRzLnZpZGVvLmZvcm1hdD1cInZpZGVvL1wiKyhcIm9ndlwiPT09c1sxXT9cIm9nZ1wiOnNbMV0pKSk6ci5tYXRjaCgvKF5kYXRhOmltYWdlXFwvW2EtejAtOStcXC89XSosKXwoXFwuKGpwKGV8Z3xlZyl8Z2lmfHBuZ3xibXB8d2VicHxzdmd8aWNvKSgoXFw/fCMpLiopPyQpL2kpP2E9XCJpbWFnZVwiOnIubWF0Y2goL1xcLihwZGYpKChcXD98IykuKik/JC9pKT8oYT1cImlmcmFtZVwiLGw9bi5leHRlbmQoITAsbCx7Y29udGVudFR5cGU6XCJwZGZcIixvcHRzOntpZnJhbWU6e3ByZWxvYWQ6ITF9fX0pKTpcIiNcIj09PXIuY2hhckF0KDApJiYoYT1cImlubGluZVwiKSksYT9sLnR5cGU9YTpvLnRyaWdnZXIoXCJvYmplY3ROZWVkc1R5cGVcIixsKSxsLmNvbnRlbnRUeXBlfHwobC5jb250ZW50VHlwZT1uLmluQXJyYXkobC50eXBlLFtcImh0bWxcIixcImlubGluZVwiLFwiYWpheFwiXSk+LTE/XCJodG1sXCI6bC50eXBlKSxsLmluZGV4PW8uZ3JvdXAubGVuZ3RoLFwiYXV0b1wiPT1sLm9wdHMuc21hbGxCdG4mJihsLm9wdHMuc21hbGxCdG49bi5pbkFycmF5KGwudHlwZSxbXCJodG1sXCIsXCJpbmxpbmVcIixcImFqYXhcIl0pPi0xKSxcImF1dG9cIj09PWwub3B0cy50b29sYmFyJiYobC5vcHRzLnRvb2xiYXI9IWwub3B0cy5zbWFsbEJ0biksbC4kdGh1bWI9bC5vcHRzLiR0aHVtYnx8bnVsbCxsLm9wdHMuJHRyaWdnZXImJmwuaW5kZXg9PT1vLm9wdHMuaW5kZXgmJihsLiR0aHVtYj1sLm9wdHMuJHRyaWdnZXIuZmluZChcImltZzpmaXJzdFwiKSxsLiR0aHVtYi5sZW5ndGgmJihsLm9wdHMuJG9yaWc9bC5vcHRzLiR0cmlnZ2VyKSksbC4kdGh1bWImJmwuJHRodW1iLmxlbmd0aHx8IWwub3B0cy4kb3JpZ3x8KGwuJHRodW1iPWwub3B0cy4kb3JpZy5maW5kKFwiaW1nOmZpcnN0XCIpKSxsLiR0aHVtYiYmIWwuJHRodW1iLmxlbmd0aCYmKGwuJHRodW1iPW51bGwpLGwudGh1bWI9bC5vcHRzLnRodW1ifHwobC4kdGh1bWI/bC4kdGh1bWJbMF0uc3JjOm51bGwpLFwiZnVuY3Rpb25cIj09PW4udHlwZShsLm9wdHMuY2FwdGlvbikmJihsLm9wdHMuY2FwdGlvbj1sLm9wdHMuY2FwdGlvbi5hcHBseShlLFtvLGxdKSksXCJmdW5jdGlvblwiPT09bi50eXBlKG8ub3B0cy5jYXB0aW9uKSYmKGwub3B0cy5jYXB0aW9uPW8ub3B0cy5jYXB0aW9uLmFwcGx5KGUsW28sbF0pKSxsLm9wdHMuY2FwdGlvbiBpbnN0YW5jZW9mIG58fChsLm9wdHMuY2FwdGlvbj12b2lkIDA9PT1sLm9wdHMuY2FwdGlvbj9cIlwiOmwub3B0cy5jYXB0aW9uK1wiXCIpLFwiYWpheFwiPT09bC50eXBlJiYoYz1yLnNwbGl0KC9cXHMrLywyKSxjLmxlbmd0aD4xJiYobC5zcmM9Yy5zaGlmdCgpLGwub3B0cy5maWx0ZXI9Yy5zaGlmdCgpKSksbC5vcHRzLm1vZGFsJiYobC5vcHRzPW4uZXh0ZW5kKCEwLGwub3B0cyx7dHJhcEZvY3VzOiEwLGluZm9iYXI6MCx0b29sYmFyOjAsc21hbGxCdG46MCxrZXlib2FyZDowLHNsaWRlU2hvdzowLGZ1bGxTY3JlZW46MCx0aHVtYnM6MCx0b3VjaDowLGNsaWNrQ29udGVudDohMSxjbGlja1NsaWRlOiExLGNsaWNrT3V0c2lkZTohMSxkYmxjbGlja0NvbnRlbnQ6ITEsZGJsY2xpY2tTbGlkZTohMSxkYmxjbGlja091dHNpZGU6ITF9KSksby5ncm91cC5wdXNoKGwpfSksT2JqZWN0LmtleXMoby5zbGlkZXMpLmxlbmd0aCYmKG8udXBkYXRlQ29udHJvbHMoKSwoZT1vLlRodW1icykmJmUuaXNBY3RpdmUmJihlLmNyZWF0ZSgpLGUuZm9jdXMoKSkpfSxhZGRFdmVudHM6ZnVuY3Rpb24oKXt2YXIgZT10aGlzO2UucmVtb3ZlRXZlbnRzKCksZS4kcmVmcy5jb250YWluZXIub24oXCJjbGljay5mYi1jbG9zZVwiLFwiW2RhdGEtZmFuY3lib3gtY2xvc2VdXCIsZnVuY3Rpb24odCl7dC5zdG9wUHJvcGFnYXRpb24oKSx0LnByZXZlbnREZWZhdWx0KCksZS5jbG9zZSh0KX0pLm9uKFwidG91Y2hzdGFydC5mYi1wcmV2IGNsaWNrLmZiLXByZXZcIixcIltkYXRhLWZhbmN5Ym94LXByZXZdXCIsZnVuY3Rpb24odCl7dC5zdG9wUHJvcGFnYXRpb24oKSx0LnByZXZlbnREZWZhdWx0KCksZS5wcmV2aW91cygpfSkub24oXCJ0b3VjaHN0YXJ0LmZiLW5leHQgY2xpY2suZmItbmV4dFwiLFwiW2RhdGEtZmFuY3lib3gtbmV4dF1cIixmdW5jdGlvbih0KXt0LnN0b3BQcm9wYWdhdGlvbigpLHQucHJldmVudERlZmF1bHQoKSxlLm5leHQoKX0pLm9uKFwiY2xpY2suZmJcIixcIltkYXRhLWZhbmN5Ym94LXpvb21dXCIsZnVuY3Rpb24odCl7ZVtlLmlzU2NhbGVkRG93bigpP1wic2NhbGVUb0FjdHVhbFwiOlwic2NhbGVUb0ZpdFwiXSgpfSkscy5vbihcIm9yaWVudGF0aW9uY2hhbmdlLmZiIHJlc2l6ZS5mYlwiLGZ1bmN0aW9uKHQpe3QmJnQub3JpZ2luYWxFdmVudCYmXCJyZXNpemVcIj09PXQub3JpZ2luYWxFdmVudC50eXBlPyhlLnJlcXVlc3RJZCYmdShlLnJlcXVlc3RJZCksZS5yZXF1ZXN0SWQ9ZChmdW5jdGlvbigpe2UudXBkYXRlKHQpfSkpOihlLmN1cnJlbnQmJlwiaWZyYW1lXCI9PT1lLmN1cnJlbnQudHlwZSYmZS4kcmVmcy5zdGFnZS5oaWRlKCksc2V0VGltZW91dChmdW5jdGlvbigpe2UuJHJlZnMuc3RhZ2Uuc2hvdygpLGUudXBkYXRlKHQpfSxuLmZhbmN5Ym94LmlzTW9iaWxlPzYwMDoyNTApKX0pLHIub24oXCJrZXlkb3duLmZiXCIsZnVuY3Rpb24odCl7dmFyIG89bi5mYW5jeWJveD9uLmZhbmN5Ym94LmdldEluc3RhbmNlKCk6bnVsbCxpPW8uY3VycmVudCxhPXQua2V5Q29kZXx8dC53aGljaDtpZig5PT1hKXJldHVybiB2b2lkKGkub3B0cy50cmFwRm9jdXMmJmUuZm9jdXModCkpO2lmKCEoIWkub3B0cy5rZXlib2FyZHx8dC5jdHJsS2V5fHx0LmFsdEtleXx8dC5zaGlmdEtleXx8bih0LnRhcmdldCkuaXMoXCJpbnB1dCx0ZXh0YXJlYSx2aWRlbyxhdWRpbyxzZWxlY3RcIikpKXJldHVybiA4PT09YXx8Mjc9PT1hPyh0LnByZXZlbnREZWZhdWx0KCksdm9pZCBlLmNsb3NlKHQpKTozNz09PWF8fDM4PT09YT8odC5wcmV2ZW50RGVmYXVsdCgpLHZvaWQgZS5wcmV2aW91cygpKTozOT09PWF8fDQwPT09YT8odC5wcmV2ZW50RGVmYXVsdCgpLHZvaWQgZS5uZXh0KCkpOnZvaWQgZS50cmlnZ2VyKFwiYWZ0ZXJLZXlkb3duXCIsdCxhKX0pLGUuZ3JvdXBbZS5jdXJySW5kZXhdLm9wdHMuaWRsZVRpbWUmJihlLmlkbGVTZWNvbmRzQ291bnRlcj0wLHIub24oXCJtb3VzZW1vdmUuZmItaWRsZSBtb3VzZWxlYXZlLmZiLWlkbGUgbW91c2Vkb3duLmZiLWlkbGUgdG91Y2hzdGFydC5mYi1pZGxlIHRvdWNobW92ZS5mYi1pZGxlIHNjcm9sbC5mYi1pZGxlIGtleWRvd24uZmItaWRsZVwiLGZ1bmN0aW9uKHQpe2UuaWRsZVNlY29uZHNDb3VudGVyPTAsZS5pc0lkbGUmJmUuc2hvd0NvbnRyb2xzKCksZS5pc0lkbGU9ITF9KSxlLmlkbGVJbnRlcnZhbD10LnNldEludGVydmFsKGZ1bmN0aW9uKCl7KytlLmlkbGVTZWNvbmRzQ291bnRlcj49ZS5ncm91cFtlLmN1cnJJbmRleF0ub3B0cy5pZGxlVGltZSYmIWUuaXNEcmFnZ2luZyYmKGUuaXNJZGxlPSEwLGUuaWRsZVNlY29uZHNDb3VudGVyPTAsZS5oaWRlQ29udHJvbHMoKSl9LDFlMykpfSxyZW1vdmVFdmVudHM6ZnVuY3Rpb24oKXt2YXIgZT10aGlzO3Mub2ZmKFwib3JpZW50YXRpb25jaGFuZ2UuZmIgcmVzaXplLmZiXCIpLHIub2ZmKFwia2V5ZG93bi5mYiAuZmItaWRsZVwiKSx0aGlzLiRyZWZzLmNvbnRhaW5lci5vZmYoXCIuZmItY2xvc2UgLmZiLXByZXYgLmZiLW5leHRcIiksZS5pZGxlSW50ZXJ2YWwmJih0LmNsZWFySW50ZXJ2YWwoZS5pZGxlSW50ZXJ2YWwpLGUuaWRsZUludGVydmFsPW51bGwpfSxwcmV2aW91czpmdW5jdGlvbih0KXtyZXR1cm4gdGhpcy5qdW1wVG8odGhpcy5jdXJyUG9zLTEsdCl9LG5leHQ6ZnVuY3Rpb24odCl7cmV0dXJuIHRoaXMuanVtcFRvKHRoaXMuY3VyclBvcysxLHQpfSxqdW1wVG86ZnVuY3Rpb24odCxlKXt2YXIgbyxpLGEscyxyLGMsbCxkLHUsZj10aGlzLGg9Zi5ncm91cC5sZW5ndGg7aWYoIShmLmlzRHJhZ2dpbmd8fGYuaXNDbG9zaW5nfHxmLmlzQW5pbWF0aW5nJiZmLmZpcnN0UnVuKSl7aWYodD1wYXJzZUludCh0LDEwKSwhKGE9Zi5jdXJyZW50P2YuY3VycmVudC5vcHRzLmxvb3A6Zi5vcHRzLmxvb3ApJiYodDwwfHx0Pj1oKSlyZXR1cm4hMTtpZihvPWYuZmlyc3RSdW49IU9iamVjdC5rZXlzKGYuc2xpZGVzKS5sZW5ndGgscj1mLmN1cnJlbnQsZi5wcmV2SW5kZXg9Zi5jdXJySW5kZXgsZi5wcmV2UG9zPWYuY3VyclBvcyxzPWYuY3JlYXRlU2xpZGUodCksaD4xJiYoKGF8fHMuaW5kZXg8aC0xKSYmZi5jcmVhdGVTbGlkZSh0KzEpLChhfHxzLmluZGV4PjApJiZmLmNyZWF0ZVNsaWRlKHQtMSkpLGYuY3VycmVudD1zLGYuY3VyckluZGV4PXMuaW5kZXgsZi5jdXJyUG9zPXMucG9zLGYudHJpZ2dlcihcImJlZm9yZVNob3dcIixvKSxmLnVwZGF0ZUNvbnRyb2xzKCkscy5mb3JjZWREdXJhdGlvbj12b2lkIDAsbi5pc051bWVyaWMoZSk/cy5mb3JjZWREdXJhdGlvbj1lOmU9cy5vcHRzW28/XCJhbmltYXRpb25EdXJhdGlvblwiOlwidHJhbnNpdGlvbkR1cmF0aW9uXCJdLGU9cGFyc2VJbnQoZSwxMCksaT1mLmlzTW92ZWQocykscy4kc2xpZGUuYWRkQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tY3VycmVudFwiKSxvKXJldHVybiBzLm9wdHMuYW5pbWF0aW9uRWZmZWN0JiZlJiZmLiRyZWZzLmNvbnRhaW5lci5jc3MoXCJ0cmFuc2l0aW9uLWR1cmF0aW9uXCIsZStcIm1zXCIpLGYuJHJlZnMuY29udGFpbmVyLmFkZENsYXNzKFwiZmFuY3lib3gtaXMtb3BlblwiKS50cmlnZ2VyKFwiZm9jdXNcIiksZi5sb2FkU2xpZGUocyksdm9pZCBmLnByZWxvYWQoXCJpbWFnZVwiKTtjPW4uZmFuY3lib3guZ2V0VHJhbnNsYXRlKHIuJHNsaWRlKSxsPW4uZmFuY3lib3guZ2V0VHJhbnNsYXRlKGYuJHJlZnMuc3RhZ2UpLG4uZWFjaChmLnNsaWRlcyxmdW5jdGlvbih0LGUpe24uZmFuY3lib3guc3RvcChlLiRzbGlkZSwhMCl9KSxyLnBvcyE9PXMucG9zJiYoci5pc0NvbXBsZXRlPSExKSxyLiRzbGlkZS5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1jb21wbGV0ZSBmYW5jeWJveC1zbGlkZS0tY3VycmVudFwiKSxpPyh1PWMubGVmdC0oci5wb3MqYy53aWR0aCtyLnBvcypyLm9wdHMuZ3V0dGVyKSxuLmVhY2goZi5zbGlkZXMsZnVuY3Rpb24odCxvKXtvLiRzbGlkZS5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWFuaW1hdGVkXCIpLnJlbW92ZUNsYXNzKGZ1bmN0aW9uKHQsZSl7cmV0dXJuKGUubWF0Y2goLyhefFxccylmYW5jeWJveC1meC1cXFMrL2cpfHxbXSkuam9pbihcIiBcIil9KTt2YXIgaT1vLnBvcypjLndpZHRoK28ucG9zKm8ub3B0cy5ndXR0ZXI7bi5mYW5jeWJveC5zZXRUcmFuc2xhdGUoby4kc2xpZGUse3RvcDowLGxlZnQ6aS1sLmxlZnQrdX0pLG8ucG9zIT09cy5wb3MmJm8uJHNsaWRlLmFkZENsYXNzKFwiZmFuY3lib3gtc2xpZGUtLVwiKyhvLnBvcz5zLnBvcz9cIm5leHRcIjpcInByZXZpb3VzXCIpKSxwKG8uJHNsaWRlKSxuLmZhbmN5Ym94LmFuaW1hdGUoby4kc2xpZGUse3RvcDowLGxlZnQ6KG8ucG9zLXMucG9zKSpjLndpZHRoKyhvLnBvcy1zLnBvcykqby5vcHRzLmd1dHRlcn0sZSxmdW5jdGlvbigpe28uJHNsaWRlLmNzcyh7dHJhbnNmb3JtOlwiXCIsb3BhY2l0eTpcIlwifSkucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tbmV4dCBmYW5jeWJveC1zbGlkZS0tcHJldmlvdXNcIiksby5wb3M9PT1mLmN1cnJQb3MmJmYuY29tcGxldGUoKX0pfSkpOmUmJnMub3B0cy50cmFuc2l0aW9uRWZmZWN0JiYoZD1cImZhbmN5Ym94LWFuaW1hdGVkIGZhbmN5Ym94LWZ4LVwiK3Mub3B0cy50cmFuc2l0aW9uRWZmZWN0LHIuJHNsaWRlLmFkZENsYXNzKFwiZmFuY3lib3gtc2xpZGUtLVwiKyhyLnBvcz5zLnBvcz9cIm5leHRcIjpcInByZXZpb3VzXCIpKSxuLmZhbmN5Ym94LmFuaW1hdGUoci4kc2xpZGUsZCxlLGZ1bmN0aW9uKCl7ci4kc2xpZGUucmVtb3ZlQ2xhc3MoZCkucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tbmV4dCBmYW5jeWJveC1zbGlkZS0tcHJldmlvdXNcIil9LCExKSkscy5pc0xvYWRlZD9mLnJldmVhbENvbnRlbnQocyk6Zi5sb2FkU2xpZGUocyksZi5wcmVsb2FkKFwiaW1hZ2VcIil9fSxjcmVhdGVTbGlkZTpmdW5jdGlvbih0KXt2YXIgZSxvLGk9dGhpcztyZXR1cm4gbz10JWkuZ3JvdXAubGVuZ3RoLG89bzwwP2kuZ3JvdXAubGVuZ3RoK286bywhaS5zbGlkZXNbdF0mJmkuZ3JvdXBbb10mJihlPW4oJzxkaXYgY2xhc3M9XCJmYW5jeWJveC1zbGlkZVwiPjwvZGl2PicpLmFwcGVuZFRvKGkuJHJlZnMuc3RhZ2UpLGkuc2xpZGVzW3RdPW4uZXh0ZW5kKCEwLHt9LGkuZ3JvdXBbb10se3Bvczp0LCRzbGlkZTplLGlzTG9hZGVkOiExfSksaS51cGRhdGVTbGlkZShpLnNsaWRlc1t0XSkpLGkuc2xpZGVzW3RdfSxzY2FsZVRvQWN0dWFsOmZ1bmN0aW9uKHQsZSxvKXt2YXIgaSxhLHMscixjLGw9dGhpcyxkPWwuY3VycmVudCx1PWQuJGNvbnRlbnQsZj1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShkLiRzbGlkZSkud2lkdGgscD1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShkLiRzbGlkZSkuaGVpZ2h0LGg9ZC53aWR0aCxnPWQuaGVpZ2h0O2wuaXNBbmltYXRpbmd8fGwuaXNNb3ZlZCgpfHwhdXx8XCJpbWFnZVwiIT1kLnR5cGV8fCFkLmlzTG9hZGVkfHxkLmhhc0Vycm9yfHwobC5pc0FuaW1hdGluZz0hMCxuLmZhbmN5Ym94LnN0b3AodSksdD12b2lkIDA9PT10Py41KmY6dCxlPXZvaWQgMD09PWU/LjUqcDplLGk9bi5mYW5jeWJveC5nZXRUcmFuc2xhdGUodSksaS50b3AtPW4uZmFuY3lib3guZ2V0VHJhbnNsYXRlKGQuJHNsaWRlKS50b3AsaS5sZWZ0LT1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShkLiRzbGlkZSkubGVmdCxyPWgvaS53aWR0aCxjPWcvaS5oZWlnaHQsYT0uNSpmLS41Kmgscz0uNSpwLS41KmcsaD5mJiYoYT1pLmxlZnQqci0odCpyLXQpLGE+MCYmKGE9MCksYTxmLWgmJihhPWYtaCkpLGc+cCYmKHM9aS50b3AqYy0oZSpjLWUpLHM+MCYmKHM9MCksczxwLWcmJihzPXAtZykpLGwudXBkYXRlQ3Vyc29yKGgsZyksbi5mYW5jeWJveC5hbmltYXRlKHUse3RvcDpzLGxlZnQ6YSxzY2FsZVg6cixzY2FsZVk6Y30sb3x8MzY2LGZ1bmN0aW9uKCl7bC5pc0FuaW1hdGluZz0hMX0pLGwuU2xpZGVTaG93JiZsLlNsaWRlU2hvdy5pc0FjdGl2ZSYmbC5TbGlkZVNob3cuc3RvcCgpKX0sc2NhbGVUb0ZpdDpmdW5jdGlvbih0KXt2YXIgZSxvPXRoaXMsaT1vLmN1cnJlbnQsYT1pLiRjb250ZW50O28uaXNBbmltYXRpbmd8fG8uaXNNb3ZlZCgpfHwhYXx8XCJpbWFnZVwiIT1pLnR5cGV8fCFpLmlzTG9hZGVkfHxpLmhhc0Vycm9yfHwoby5pc0FuaW1hdGluZz0hMCxuLmZhbmN5Ym94LnN0b3AoYSksZT1vLmdldEZpdFBvcyhpKSxvLnVwZGF0ZUN1cnNvcihlLndpZHRoLGUuaGVpZ2h0KSxuLmZhbmN5Ym94LmFuaW1hdGUoYSx7dG9wOmUudG9wLGxlZnQ6ZS5sZWZ0LHNjYWxlWDplLndpZHRoL2Eud2lkdGgoKSxzY2FsZVk6ZS5oZWlnaHQvYS5oZWlnaHQoKX0sdHx8MzY2LGZ1bmN0aW9uKCl7by5pc0FuaW1hdGluZz0hMX0pKX0sZ2V0Rml0UG9zOmZ1bmN0aW9uKHQpe3ZhciBlLG8saSxhLHM9dGhpcyxyPXQuJGNvbnRlbnQsYz10LiRzbGlkZSxsPXQud2lkdGh8fHQub3B0cy53aWR0aCxkPXQuaGVpZ2h0fHx0Lm9wdHMuaGVpZ2h0LHU9e307cmV0dXJuISEodC5pc0xvYWRlZCYmciYmci5sZW5ndGgpJiYoZT1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShzLiRyZWZzLnN0YWdlKS53aWR0aCxvPW4uZmFuY3lib3guZ2V0VHJhbnNsYXRlKHMuJHJlZnMuc3RhZ2UpLmhlaWdodCxlLT1wYXJzZUZsb2F0KGMuY3NzKFwicGFkZGluZ0xlZnRcIikpK3BhcnNlRmxvYXQoYy5jc3MoXCJwYWRkaW5nUmlnaHRcIikpK3BhcnNlRmxvYXQoci5jc3MoXCJtYXJnaW5MZWZ0XCIpKStwYXJzZUZsb2F0KHIuY3NzKFwibWFyZ2luUmlnaHRcIikpLG8tPXBhcnNlRmxvYXQoYy5jc3MoXCJwYWRkaW5nVG9wXCIpKStwYXJzZUZsb2F0KGMuY3NzKFwicGFkZGluZ0JvdHRvbVwiKSkrcGFyc2VGbG9hdChyLmNzcyhcIm1hcmdpblRvcFwiKSkrcGFyc2VGbG9hdChyLmNzcyhcIm1hcmdpbkJvdHRvbVwiKSksbCYmZHx8KGw9ZSxkPW8pLGk9TWF0aC5taW4oMSxlL2wsby9kKSxsKj1pLGQqPWksbD5lLS41JiYobD1lKSxkPm8tLjUmJihkPW8pLFwiaW1hZ2VcIj09PXQudHlwZT8odS50b3A9TWF0aC5mbG9vciguNSooby1kKSkrcGFyc2VGbG9hdChjLmNzcyhcInBhZGRpbmdUb3BcIikpLHUubGVmdD1NYXRoLmZsb29yKC41KihlLWwpKStwYXJzZUZsb2F0KGMuY3NzKFwicGFkZGluZ0xlZnRcIikpKTpcInZpZGVvXCI9PT10LmNvbnRlbnRUeXBlJiYoYT10Lm9wdHMud2lkdGgmJnQub3B0cy5oZWlnaHQ/bC9kOnQub3B0cy5yYXRpb3x8MTYvOSxkPmwvYT9kPWwvYTpsPmQqYSYmKGw9ZCphKSksdS53aWR0aD1sLHUuaGVpZ2h0PWQsdSl9LHVwZGF0ZTpmdW5jdGlvbih0KXt2YXIgZT10aGlzO24uZWFjaChlLnNsaWRlcyxmdW5jdGlvbihuLG8pe2UudXBkYXRlU2xpZGUobyx0KX0pfSx1cGRhdGVTbGlkZTpmdW5jdGlvbih0LGUpe3ZhciBvPXRoaXMsaT10JiZ0LiRjb250ZW50LGE9dC53aWR0aHx8dC5vcHRzLndpZHRoLHM9dC5oZWlnaHR8fHQub3B0cy5oZWlnaHQscj10LiRzbGlkZTtvLmFkanVzdENhcHRpb24odCksaSYmKGF8fHN8fFwidmlkZW9cIj09PXQuY29udGVudFR5cGUpJiYhdC5oYXNFcnJvciYmKG4uZmFuY3lib3guc3RvcChpKSxuLmZhbmN5Ym94LnNldFRyYW5zbGF0ZShpLG8uZ2V0Rml0UG9zKHQpKSx0LnBvcz09PW8uY3VyclBvcyYmKG8uaXNBbmltYXRpbmc9ITEsby51cGRhdGVDdXJzb3IoKSkpLG8uYWRqdXN0TGF5b3V0KHQpLHIubGVuZ3RoJiYoci50cmlnZ2VyKFwicmVmcmVzaFwiKSx0LnBvcz09PW8uY3VyclBvcyYmby4kcmVmcy50b29sYmFyLmFkZChvLiRyZWZzLm5hdmlnYXRpb24uZmluZChcIi5mYW5jeWJveC1idXR0b24tLWFycm93X3JpZ2h0XCIpKS50b2dnbGVDbGFzcyhcImNvbXBlbnNhdGUtZm9yLXNjcm9sbGJhclwiLHIuZ2V0KDApLnNjcm9sbEhlaWdodD5yLmdldCgwKS5jbGllbnRIZWlnaHQpKSxvLnRyaWdnZXIoXCJvblVwZGF0ZVwiLHQsZSl9LGNlbnRlclNsaWRlOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMsbz1lLmN1cnJlbnQsaT1vLiRzbGlkZTshZS5pc0Nsb3NpbmcmJm8mJihpLnNpYmxpbmdzKCkuY3NzKHt0cmFuc2Zvcm06XCJcIixvcGFjaXR5OlwiXCJ9KSxpLnBhcmVudCgpLmNoaWxkcmVuKCkucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tcHJldmlvdXMgZmFuY3lib3gtc2xpZGUtLW5leHRcIiksbi5mYW5jeWJveC5hbmltYXRlKGkse3RvcDowLGxlZnQ6MCxvcGFjaXR5OjF9LHZvaWQgMD09PXQ/MDp0LGZ1bmN0aW9uKCl7aS5jc3Moe3RyYW5zZm9ybTpcIlwiLG9wYWNpdHk6XCJcIn0pLG8uaXNDb21wbGV0ZXx8ZS5jb21wbGV0ZSgpfSwhMSkpfSxpc01vdmVkOmZ1bmN0aW9uKHQpe3ZhciBlLG8saT10fHx0aGlzLmN1cnJlbnQ7cmV0dXJuISFpJiYobz1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZSh0aGlzLiRyZWZzLnN0YWdlKSxlPW4uZmFuY3lib3guZ2V0VHJhbnNsYXRlKGkuJHNsaWRlKSwhaS4kc2xpZGUuaGFzQ2xhc3MoXCJmYW5jeWJveC1hbmltYXRlZFwiKSYmKE1hdGguYWJzKGUudG9wLW8udG9wKT4uNXx8TWF0aC5hYnMoZS5sZWZ0LW8ubGVmdCk+LjUpKX0sdXBkYXRlQ3Vyc29yOmZ1bmN0aW9uKHQsZSl7dmFyIG8saSxhPXRoaXMscz1hLmN1cnJlbnQscj1hLiRyZWZzLmNvbnRhaW5lcjtzJiYhYS5pc0Nsb3NpbmcmJmEuR3Vlc3R1cmVzJiYoci5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLXpvb21hYmxlIGZhbmN5Ym94LWNhbi16b29tSW4gZmFuY3lib3gtY2FuLXpvb21PdXQgZmFuY3lib3gtY2FuLXN3aXBlIGZhbmN5Ym94LWNhbi1wYW5cIiksbz1hLmNhblBhbih0LGUpLGk9ISFvfHxhLmlzWm9vbWFibGUoKSxyLnRvZ2dsZUNsYXNzKFwiZmFuY3lib3gtaXMtem9vbWFibGVcIixpKSxuKFwiW2RhdGEtZmFuY3lib3gtem9vbV1cIikucHJvcChcImRpc2FibGVkXCIsIWkpLG8/ci5hZGRDbGFzcyhcImZhbmN5Ym94LWNhbi1wYW5cIik6aSYmKFwiem9vbVwiPT09cy5vcHRzLmNsaWNrQ29udGVudHx8bi5pc0Z1bmN0aW9uKHMub3B0cy5jbGlja0NvbnRlbnQpJiZcInpvb21cIj09cy5vcHRzLmNsaWNrQ29udGVudChzKSk/ci5hZGRDbGFzcyhcImZhbmN5Ym94LWNhbi16b29tSW5cIik6cy5vcHRzLnRvdWNoJiYocy5vcHRzLnRvdWNoLnZlcnRpY2FsfHxhLmdyb3VwLmxlbmd0aD4xKSYmXCJ2aWRlb1wiIT09cy5jb250ZW50VHlwZSYmci5hZGRDbGFzcyhcImZhbmN5Ym94LWNhbi1zd2lwZVwiKSl9LGlzWm9vbWFibGU6ZnVuY3Rpb24oKXt2YXIgdCxlPXRoaXMsbj1lLmN1cnJlbnQ7aWYobiYmIWUuaXNDbG9zaW5nJiZcImltYWdlXCI9PT1uLnR5cGUmJiFuLmhhc0Vycm9yKXtpZighbi5pc0xvYWRlZClyZXR1cm4hMDtpZigodD1lLmdldEZpdFBvcyhuKSkmJihuLndpZHRoPnQud2lkdGh8fG4uaGVpZ2h0PnQuaGVpZ2h0KSlyZXR1cm4hMH1yZXR1cm4hMX0saXNTY2FsZWREb3duOmZ1bmN0aW9uKHQsZSl7dmFyIG89dGhpcyxpPSExLGE9by5jdXJyZW50LHM9YS4kY29udGVudDtyZXR1cm4gdm9pZCAwIT09dCYmdm9pZCAwIT09ZT9pPXQ8YS53aWR0aCYmZTxhLmhlaWdodDpzJiYoaT1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShzKSxpPWkud2lkdGg8YS53aWR0aCYmaS5oZWlnaHQ8YS5oZWlnaHQpLGl9LGNhblBhbjpmdW5jdGlvbih0LGUpe3ZhciBvPXRoaXMsaT1vLmN1cnJlbnQsYT1udWxsLHM9ITE7cmV0dXJuXCJpbWFnZVwiPT09aS50eXBlJiYoaS5pc0NvbXBsZXRlfHx0JiZlKSYmIWkuaGFzRXJyb3ImJihzPW8uZ2V0Rml0UG9zKGkpLHZvaWQgMCE9PXQmJnZvaWQgMCE9PWU/YT17d2lkdGg6dCxoZWlnaHQ6ZX06aS5pc0NvbXBsZXRlJiYoYT1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShpLiRjb250ZW50KSksYSYmcyYmKHM9TWF0aC5hYnMoYS53aWR0aC1zLndpZHRoKT4xLjV8fE1hdGguYWJzKGEuaGVpZ2h0LXMuaGVpZ2h0KT4xLjUpKSxzfSxsb2FkU2xpZGU6ZnVuY3Rpb24odCl7dmFyIGUsbyxpLGE9dGhpcztpZighdC5pc0xvYWRpbmcmJiF0LmlzTG9hZGVkKXtpZih0LmlzTG9hZGluZz0hMCwhMT09PWEudHJpZ2dlcihcImJlZm9yZUxvYWRcIix0KSlyZXR1cm4gdC5pc0xvYWRpbmc9ITEsITE7c3dpdGNoKGU9dC50eXBlLG89dC4kc2xpZGUsby5vZmYoXCJyZWZyZXNoXCIpLnRyaWdnZXIoXCJvblJlc2V0XCIpLmFkZENsYXNzKHQub3B0cy5zbGlkZUNsYXNzKSxlKXtjYXNlXCJpbWFnZVwiOmEuc2V0SW1hZ2UodCk7YnJlYWs7Y2FzZVwiaWZyYW1lXCI6YS5zZXRJZnJhbWUodCk7YnJlYWs7Y2FzZVwiaHRtbFwiOmEuc2V0Q29udGVudCh0LHQuc3JjfHx0LmNvbnRlbnQpO2JyZWFrO2Nhc2VcInZpZGVvXCI6YS5zZXRDb250ZW50KHQsdC5vcHRzLnZpZGVvLnRwbC5yZXBsYWNlKC9cXHtcXHtzcmNcXH1cXH0vZ2ksdC5zcmMpLnJlcGxhY2UoXCJ7e2Zvcm1hdH19XCIsdC5vcHRzLnZpZGVvRm9ybWF0fHx0Lm9wdHMudmlkZW8uZm9ybWF0fHxcIlwiKS5yZXBsYWNlKFwie3twb3N0ZXJ9fVwiLHQudGh1bWJ8fFwiXCIpKTticmVhaztjYXNlXCJpbmxpbmVcIjpuKHQuc3JjKS5sZW5ndGg/YS5zZXRDb250ZW50KHQsbih0LnNyYykpOmEuc2V0RXJyb3IodCk7YnJlYWs7Y2FzZVwiYWpheFwiOmEuc2hvd0xvYWRpbmcodCksaT1uLmFqYXgobi5leHRlbmQoe30sdC5vcHRzLmFqYXguc2V0dGluZ3Mse3VybDp0LnNyYyxzdWNjZXNzOmZ1bmN0aW9uKGUsbil7XCJzdWNjZXNzXCI9PT1uJiZhLnNldENvbnRlbnQodCxlKX0sZXJyb3I6ZnVuY3Rpb24oZSxuKXtlJiZcImFib3J0XCIhPT1uJiZhLnNldEVycm9yKHQpfX0pKSxvLm9uZShcIm9uUmVzZXRcIixmdW5jdGlvbigpe2kuYWJvcnQoKX0pO2JyZWFrO2RlZmF1bHQ6YS5zZXRFcnJvcih0KX1yZXR1cm4hMH19LHNldEltYWdlOmZ1bmN0aW9uKHQpe3ZhciBvLGk9dGhpcztzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7dmFyIGU9dC4kaW1hZ2U7aS5pc0Nsb3Npbmd8fCF0LmlzTG9hZGluZ3x8ZSYmZS5sZW5ndGgmJmVbMF0uY29tcGxldGV8fHQuaGFzRXJyb3J8fGkuc2hvd0xvYWRpbmcodCl9LDUwKSxpLmNoZWNrU3Jjc2V0KHQpLHQuJGNvbnRlbnQ9bignPGRpdiBjbGFzcz1cImZhbmN5Ym94LWNvbnRlbnRcIj48L2Rpdj4nKS5hZGRDbGFzcyhcImZhbmN5Ym94LWlzLWhpZGRlblwiKS5hcHBlbmRUbyh0LiRzbGlkZS5hZGRDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1pbWFnZVwiKSksITEhPT10Lm9wdHMucHJlbG9hZCYmdC5vcHRzLndpZHRoJiZ0Lm9wdHMuaGVpZ2h0JiZ0LnRodW1iJiYodC53aWR0aD10Lm9wdHMud2lkdGgsdC5oZWlnaHQ9dC5vcHRzLmhlaWdodCxvPWUuY3JlYXRlRWxlbWVudChcImltZ1wiKSxvLm9uZXJyb3I9ZnVuY3Rpb24oKXtuKHRoaXMpLnJlbW92ZSgpLHQuJGdob3N0PW51bGx9LG8ub25sb2FkPWZ1bmN0aW9uKCl7aS5hZnRlckxvYWQodCl9LHQuJGdob3N0PW4obykuYWRkQ2xhc3MoXCJmYW5jeWJveC1pbWFnZVwiKS5hcHBlbmRUbyh0LiRjb250ZW50KS5hdHRyKFwic3JjXCIsdC50aHVtYikpLGkuc2V0QmlnSW1hZ2UodCl9LGNoZWNrU3Jjc2V0OmZ1bmN0aW9uKGUpe3ZhciBuLG8saSxhLHM9ZS5vcHRzLnNyY3NldHx8ZS5vcHRzLmltYWdlLnNyY3NldDtpZihzKXtpPXQuZGV2aWNlUGl4ZWxSYXRpb3x8MSxhPXQuaW5uZXJXaWR0aCppLG89cy5zcGxpdChcIixcIikubWFwKGZ1bmN0aW9uKHQpe3ZhciBlPXt9O3JldHVybiB0LnRyaW0oKS5zcGxpdCgvXFxzKy8pLmZvckVhY2goZnVuY3Rpb24odCxuKXt2YXIgbz1wYXJzZUludCh0LnN1YnN0cmluZygwLHQubGVuZ3RoLTEpLDEwKTtpZigwPT09bilyZXR1cm4gZS51cmw9dDtvJiYoZS52YWx1ZT1vLGUucG9zdGZpeD10W3QubGVuZ3RoLTFdKX0pLGV9KSxvLnNvcnQoZnVuY3Rpb24odCxlKXtyZXR1cm4gdC52YWx1ZS1lLnZhbHVlfSk7Zm9yKHZhciByPTA7cjxvLmxlbmd0aDtyKyspe3ZhciBjPW9bcl07aWYoXCJ3XCI9PT1jLnBvc3RmaXgmJmMudmFsdWU+PWF8fFwieFwiPT09Yy5wb3N0Zml4JiZjLnZhbHVlPj1pKXtuPWM7YnJlYWt9fSFuJiZvLmxlbmd0aCYmKG49b1tvLmxlbmd0aC0xXSksbiYmKGUuc3JjPW4udXJsLGUud2lkdGgmJmUuaGVpZ2h0JiZcIndcIj09bi5wb3N0Zml4JiYoZS5oZWlnaHQ9ZS53aWR0aC9lLmhlaWdodCpuLnZhbHVlLGUud2lkdGg9bi52YWx1ZSksZS5vcHRzLnNyY3NldD1zKX19LHNldEJpZ0ltYWdlOmZ1bmN0aW9uKHQpe3ZhciBvPXRoaXMsaT1lLmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiksYT1uKGkpO3QuJGltYWdlPWEub25lKFwiZXJyb3JcIixmdW5jdGlvbigpe28uc2V0RXJyb3IodCl9KS5vbmUoXCJsb2FkXCIsZnVuY3Rpb24oKXt2YXIgZTt0LiRnaG9zdHx8KG8ucmVzb2x2ZUltYWdlU2xpZGVTaXplKHQsdGhpcy5uYXR1cmFsV2lkdGgsdGhpcy5uYXR1cmFsSGVpZ2h0KSxvLmFmdGVyTG9hZCh0KSksby5pc0Nsb3Npbmd8fCh0Lm9wdHMuc3Jjc2V0JiYoZT10Lm9wdHMuc2l6ZXMsZSYmXCJhdXRvXCIhPT1lfHwoZT0odC53aWR0aC90LmhlaWdodD4xJiZzLndpZHRoKCkvcy5oZWlnaHQoKT4xP1wiMTAwXCI6TWF0aC5yb3VuZCh0LndpZHRoL3QuaGVpZ2h0KjEwMCkpK1widndcIiksYS5hdHRyKFwic2l6ZXNcIixlKS5hdHRyKFwic3Jjc2V0XCIsdC5vcHRzLnNyY3NldCkpLHQuJGdob3N0JiZzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7dC4kZ2hvc3QmJiFvLmlzQ2xvc2luZyYmdC4kZ2hvc3QuaGlkZSgpfSxNYXRoLm1pbigzMDAsTWF0aC5tYXgoMWUzLHQuaGVpZ2h0LzE2MDApKSksby5oaWRlTG9hZGluZyh0KSl9KS5hZGRDbGFzcyhcImZhbmN5Ym94LWltYWdlXCIpLmF0dHIoXCJzcmNcIix0LnNyYykuYXBwZW5kVG8odC4kY29udGVudCksKGkuY29tcGxldGV8fFwiY29tcGxldGVcIj09aS5yZWFkeVN0YXRlKSYmYS5uYXR1cmFsV2lkdGgmJmEubmF0dXJhbEhlaWdodD9hLnRyaWdnZXIoXCJsb2FkXCIpOmkuZXJyb3ImJmEudHJpZ2dlcihcImVycm9yXCIpfSxyZXNvbHZlSW1hZ2VTbGlkZVNpemU6ZnVuY3Rpb24odCxlLG4pe3ZhciBvPXBhcnNlSW50KHQub3B0cy53aWR0aCwxMCksaT1wYXJzZUludCh0Lm9wdHMuaGVpZ2h0LDEwKTt0LndpZHRoPWUsdC5oZWlnaHQ9bixvPjAmJih0LndpZHRoPW8sdC5oZWlnaHQ9TWF0aC5mbG9vcihvKm4vZSkpLGk+MCYmKHQud2lkdGg9TWF0aC5mbG9vcihpKmUvbiksdC5oZWlnaHQ9aSl9LHNldElmcmFtZTpmdW5jdGlvbih0KXt2YXIgZSxvPXRoaXMsaT10Lm9wdHMuaWZyYW1lLGE9dC4kc2xpZGU7dC4kY29udGVudD1uKCc8ZGl2IGNsYXNzPVwiZmFuY3lib3gtY29udGVudCcrKGkucHJlbG9hZD9cIiBmYW5jeWJveC1pcy1oaWRkZW5cIjpcIlwiKSsnXCI+PC9kaXY+JykuY3NzKGkuY3NzKS5hcHBlbmRUbyhhKSxhLmFkZENsYXNzKFwiZmFuY3lib3gtc2xpZGUtLVwiK3QuY29udGVudFR5cGUpLHQuJGlmcmFtZT1lPW4oaS50cGwucmVwbGFjZSgvXFx7cm5kXFx9L2csKG5ldyBEYXRlKS5nZXRUaW1lKCkpKS5hdHRyKGkuYXR0cikuYXBwZW5kVG8odC4kY29udGVudCksaS5wcmVsb2FkPyhvLnNob3dMb2FkaW5nKHQpLGUub24oXCJsb2FkLmZiIGVycm9yLmZiXCIsZnVuY3Rpb24oZSl7dGhpcy5pc1JlYWR5PTEsdC4kc2xpZGUudHJpZ2dlcihcInJlZnJlc2hcIiksby5hZnRlckxvYWQodCl9KSxhLm9uKFwicmVmcmVzaC5mYlwiLGZ1bmN0aW9uKCl7dmFyIG4sbyxzPXQuJGNvbnRlbnQscj1pLmNzcy53aWR0aCxjPWkuY3NzLmhlaWdodDtpZigxPT09ZVswXS5pc1JlYWR5KXt0cnl7bj1lLmNvbnRlbnRzKCksbz1uLmZpbmQoXCJib2R5XCIpfWNhdGNoKHQpe31vJiZvLmxlbmd0aCYmby5jaGlsZHJlbigpLmxlbmd0aCYmKGEuY3NzKFwib3ZlcmZsb3dcIixcInZpc2libGVcIikscy5jc3Moe3dpZHRoOlwiMTAwJVwiLFwibWF4LXdpZHRoXCI6XCIxMDAlXCIsaGVpZ2h0OlwiOTk5OXB4XCJ9KSx2b2lkIDA9PT1yJiYocj1NYXRoLmNlaWwoTWF0aC5tYXgob1swXS5jbGllbnRXaWR0aCxvLm91dGVyV2lkdGgoITApKSkpLHMuY3NzKFwid2lkdGhcIixyfHxcIlwiKS5jc3MoXCJtYXgtd2lkdGhcIixcIlwiKSx2b2lkIDA9PT1jJiYoYz1NYXRoLmNlaWwoTWF0aC5tYXgob1swXS5jbGllbnRIZWlnaHQsby5vdXRlckhlaWdodCghMCkpKSkscy5jc3MoXCJoZWlnaHRcIixjfHxcIlwiKSxhLmNzcyhcIm92ZXJmbG93XCIsXCJhdXRvXCIpKSxzLnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtaXMtaGlkZGVuXCIpfX0pKTpvLmFmdGVyTG9hZCh0KSxlLmF0dHIoXCJzcmNcIix0LnNyYyksYS5vbmUoXCJvblJlc2V0XCIsZnVuY3Rpb24oKXt0cnl7bih0aGlzKS5maW5kKFwiaWZyYW1lXCIpLmhpZGUoKS51bmJpbmQoKS5hdHRyKFwic3JjXCIsXCIvL2Fib3V0OmJsYW5rXCIpfWNhdGNoKHQpe31uKHRoaXMpLm9mZihcInJlZnJlc2guZmJcIikuZW1wdHkoKSx0LmlzTG9hZGVkPSExLHQuaXNSZXZlYWxlZD0hMX0pfSxzZXRDb250ZW50OmZ1bmN0aW9uKHQsZSl7dmFyIG89dGhpcztvLmlzQ2xvc2luZ3x8KG8uaGlkZUxvYWRpbmcodCksdC4kY29udGVudCYmbi5mYW5jeWJveC5zdG9wKHQuJGNvbnRlbnQpLHQuJHNsaWRlLmVtcHR5KCksbChlKSYmZS5wYXJlbnQoKS5sZW5ndGg/KChlLmhhc0NsYXNzKFwiZmFuY3lib3gtY29udGVudFwiKXx8ZS5wYXJlbnQoKS5oYXNDbGFzcyhcImZhbmN5Ym94LWNvbnRlbnRcIikpJiZlLnBhcmVudHMoXCIuZmFuY3lib3gtc2xpZGVcIikudHJpZ2dlcihcIm9uUmVzZXRcIiksdC4kcGxhY2Vob2xkZXI9bihcIjxkaXY+XCIpLmhpZGUoKS5pbnNlcnRBZnRlcihlKSxlLmNzcyhcImRpc3BsYXlcIixcImlubGluZS1ibG9ja1wiKSk6dC5oYXNFcnJvcnx8KFwic3RyaW5nXCI9PT1uLnR5cGUoZSkmJihlPW4oXCI8ZGl2PlwiKS5hcHBlbmQobi50cmltKGUpKS5jb250ZW50cygpKSx0Lm9wdHMuZmlsdGVyJiYoZT1uKFwiPGRpdj5cIikuaHRtbChlKS5maW5kKHQub3B0cy5maWx0ZXIpKSksdC4kc2xpZGUub25lKFwib25SZXNldFwiLGZ1bmN0aW9uKCl7bih0aGlzKS5maW5kKFwidmlkZW8sYXVkaW9cIikudHJpZ2dlcihcInBhdXNlXCIpLHQuJHBsYWNlaG9sZGVyJiYodC4kcGxhY2Vob2xkZXIuYWZ0ZXIoZS5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWNvbnRlbnRcIikuaGlkZSgpKS5yZW1vdmUoKSx0LiRwbGFjZWhvbGRlcj1udWxsKSx0LiRzbWFsbEJ0biYmKHQuJHNtYWxsQnRuLnJlbW92ZSgpLHQuJHNtYWxsQnRuPW51bGwpLHQuaGFzRXJyb3J8fChuKHRoaXMpLmVtcHR5KCksdC5pc0xvYWRlZD0hMSx0LmlzUmV2ZWFsZWQ9ITEpfSksbihlKS5hcHBlbmRUbyh0LiRzbGlkZSksbihlKS5pcyhcInZpZGVvLGF1ZGlvXCIpJiYobihlKS5hZGRDbGFzcyhcImZhbmN5Ym94LXZpZGVvXCIpLG4oZSkud3JhcChcIjxkaXY+PC9kaXY+XCIpLHQuY29udGVudFR5cGU9XCJ2aWRlb1wiLHQub3B0cy53aWR0aD10Lm9wdHMud2lkdGh8fG4oZSkuYXR0cihcIndpZHRoXCIpLHQub3B0cy5oZWlnaHQ9dC5vcHRzLmhlaWdodHx8bihlKS5hdHRyKFwiaGVpZ2h0XCIpKSx0LiRjb250ZW50PXQuJHNsaWRlLmNoaWxkcmVuKCkuZmlsdGVyKFwiZGl2LGZvcm0sbWFpbix2aWRlbyxhdWRpbyxhcnRpY2xlLC5mYW5jeWJveC1jb250ZW50XCIpLmZpcnN0KCksdC4kY29udGVudC5zaWJsaW5ncygpLmhpZGUoKSx0LiRjb250ZW50Lmxlbmd0aHx8KHQuJGNvbnRlbnQ9dC4kc2xpZGUud3JhcElubmVyKFwiPGRpdj48L2Rpdj5cIikuY2hpbGRyZW4oKS5maXJzdCgpKSx0LiRjb250ZW50LmFkZENsYXNzKFwiZmFuY3lib3gtY29udGVudFwiKSx0LiRzbGlkZS5hZGRDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1cIit0LmNvbnRlbnRUeXBlKSxvLmFmdGVyTG9hZCh0KSl9LHNldEVycm9yOmZ1bmN0aW9uKHQpe3QuaGFzRXJyb3I9ITAsdC4kc2xpZGUudHJpZ2dlcihcIm9uUmVzZXRcIikucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tXCIrdC5jb250ZW50VHlwZSkuYWRkQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tZXJyb3JcIiksdC5jb250ZW50VHlwZT1cImh0bWxcIix0aGlzLnNldENvbnRlbnQodCx0aGlzLnRyYW5zbGF0ZSh0LHQub3B0cy5lcnJvclRwbCkpLHQucG9zPT09dGhpcy5jdXJyUG9zJiYodGhpcy5pc0FuaW1hdGluZz0hMSl9LHNob3dMb2FkaW5nOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7KHQ9dHx8ZS5jdXJyZW50KSYmIXQuJHNwaW5uZXImJih0LiRzcGlubmVyPW4oZS50cmFuc2xhdGUoZSxlLm9wdHMuc3Bpbm5lclRwbCkpLmFwcGVuZFRvKHQuJHNsaWRlKS5oaWRlKCkuZmFkZUluKFwiZmFzdFwiKSl9LGhpZGVMb2FkaW5nOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7KHQ9dHx8ZS5jdXJyZW50KSYmdC4kc3Bpbm5lciYmKHQuJHNwaW5uZXIuc3RvcCgpLnJlbW92ZSgpLGRlbGV0ZSB0LiRzcGlubmVyKX0sYWZ0ZXJMb2FkOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7ZS5pc0Nsb3Npbmd8fCh0LmlzTG9hZGluZz0hMSx0LmlzTG9hZGVkPSEwLGUudHJpZ2dlcihcImFmdGVyTG9hZFwiLHQpLGUuaGlkZUxvYWRpbmcodCksIXQub3B0cy5zbWFsbEJ0bnx8dC4kc21hbGxCdG4mJnQuJHNtYWxsQnRuLmxlbmd0aHx8KHQuJHNtYWxsQnRuPW4oZS50cmFuc2xhdGUodCx0Lm9wdHMuYnRuVHBsLnNtYWxsQnRuKSkuYXBwZW5kVG8odC4kY29udGVudCkpLHQub3B0cy5wcm90ZWN0JiZ0LiRjb250ZW50JiYhdC5oYXNFcnJvciYmKHQuJGNvbnRlbnQub24oXCJjb250ZXh0bWVudS5mYlwiLGZ1bmN0aW9uKHQpe3JldHVybiAyPT10LmJ1dHRvbiYmdC5wcmV2ZW50RGVmYXVsdCgpLCEwfSksXCJpbWFnZVwiPT09dC50eXBlJiZuKCc8ZGl2IGNsYXNzPVwiZmFuY3lib3gtc3BhY2ViYWxsXCI+PC9kaXY+JykuYXBwZW5kVG8odC4kY29udGVudCkpLGUuYWRqdXN0Q2FwdGlvbih0KSxlLmFkanVzdExheW91dCh0KSx0LnBvcz09PWUuY3VyclBvcyYmZS51cGRhdGVDdXJzb3IoKSxlLnJldmVhbENvbnRlbnQodCkpfSxhZGp1c3RDYXB0aW9uOmZ1bmN0aW9uKHQpe3ZhciBlLG49dGhpcyxvPXR8fG4uY3VycmVudCxpPW8ub3B0cy5jYXB0aW9uLGE9by5vcHRzLnByZXZlbnRDYXB0aW9uT3ZlcmxhcCxzPW4uJHJlZnMuY2FwdGlvbixyPSExO3MudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1jYXB0aW9uLS1zZXBhcmF0ZVwiLGEpLGEmJmkmJmkubGVuZ3RoJiYoby5wb3MhPT1uLmN1cnJQb3M/KGU9cy5jbG9uZSgpLmFwcGVuZFRvKHMucGFyZW50KCkpLGUuY2hpbGRyZW4oKS5lcSgwKS5lbXB0eSgpLmh0bWwoaSkscj1lLm91dGVySGVpZ2h0KCEwKSxlLmVtcHR5KCkucmVtb3ZlKCkpOm4uJGNhcHRpb24mJihyPW4uJGNhcHRpb24ub3V0ZXJIZWlnaHQoITApKSxvLiRzbGlkZS5jc3MoXCJwYWRkaW5nLWJvdHRvbVwiLHJ8fFwiXCIpKX0sYWRqdXN0TGF5b3V0OmZ1bmN0aW9uKHQpe3ZhciBlLG4sbyxpLGE9dGhpcyxzPXR8fGEuY3VycmVudDtzLmlzTG9hZGVkJiYhMCE9PXMub3B0cy5kaXNhYmxlTGF5b3V0Rml4JiYocy4kY29udGVudC5jc3MoXCJtYXJnaW4tYm90dG9tXCIsXCJcIikscy4kY29udGVudC5vdXRlckhlaWdodCgpPnMuJHNsaWRlLmhlaWdodCgpKy41JiYobz1zLiRzbGlkZVswXS5zdHlsZVtcInBhZGRpbmctYm90dG9tXCJdLGk9cy4kc2xpZGUuY3NzKFwicGFkZGluZy1ib3R0b21cIikscGFyc2VGbG9hdChpKT4wJiYoZT1zLiRzbGlkZVswXS5zY3JvbGxIZWlnaHQscy4kc2xpZGUuY3NzKFwicGFkZGluZy1ib3R0b21cIiwwKSxNYXRoLmFicyhlLXMuJHNsaWRlWzBdLnNjcm9sbEhlaWdodCk8MSYmKG49aSkscy4kc2xpZGUuY3NzKFwicGFkZGluZy1ib3R0b21cIixvKSkpLHMuJGNvbnRlbnQuY3NzKFwibWFyZ2luLWJvdHRvbVwiLG4pKX0scmV2ZWFsQ29udGVudDpmdW5jdGlvbih0KXt2YXIgZSxvLGksYSxzPXRoaXMscj10LiRzbGlkZSxjPSExLGw9ITEsZD1zLmlzTW92ZWQodCksdT10LmlzUmV2ZWFsZWQ7cmV0dXJuIHQuaXNSZXZlYWxlZD0hMCxlPXQub3B0c1tzLmZpcnN0UnVuP1wiYW5pbWF0aW9uRWZmZWN0XCI6XCJ0cmFuc2l0aW9uRWZmZWN0XCJdLGk9dC5vcHRzW3MuZmlyc3RSdW4/XCJhbmltYXRpb25EdXJhdGlvblwiOlwidHJhbnNpdGlvbkR1cmF0aW9uXCJdLGk9cGFyc2VJbnQodm9pZCAwPT09dC5mb3JjZWREdXJhdGlvbj9pOnQuZm9yY2VkRHVyYXRpb24sMTApLCFkJiZ0LnBvcz09PXMuY3VyclBvcyYmaXx8KGU9ITEpLFwiem9vbVwiPT09ZSYmKHQucG9zPT09cy5jdXJyUG9zJiZpJiZcImltYWdlXCI9PT10LnR5cGUmJiF0Lmhhc0Vycm9yJiYobD1zLmdldFRodW1iUG9zKHQpKT9jPXMuZ2V0Rml0UG9zKHQpOmU9XCJmYWRlXCIpLFwiem9vbVwiPT09ZT8ocy5pc0FuaW1hdGluZz0hMCxjLnNjYWxlWD1jLndpZHRoL2wud2lkdGgsYy5zY2FsZVk9Yy5oZWlnaHQvbC5oZWlnaHQsYT10Lm9wdHMuem9vbU9wYWNpdHksXCJhdXRvXCI9PWEmJihhPU1hdGguYWJzKHQud2lkdGgvdC5oZWlnaHQtbC53aWR0aC9sLmhlaWdodCk+LjEpLGEmJihsLm9wYWNpdHk9LjEsYy5vcGFjaXR5PTEpLG4uZmFuY3lib3guc2V0VHJhbnNsYXRlKHQuJGNvbnRlbnQucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1pcy1oaWRkZW5cIiksbCkscCh0LiRjb250ZW50KSx2b2lkIG4uZmFuY3lib3guYW5pbWF0ZSh0LiRjb250ZW50LGMsaSxmdW5jdGlvbigpe3MuaXNBbmltYXRpbmc9ITEscy5jb21wbGV0ZSgpfSkpOihzLnVwZGF0ZVNsaWRlKHQpLGU/KG4uZmFuY3lib3guc3RvcChyKSxvPVwiZmFuY3lib3gtc2xpZGUtLVwiKyh0LnBvcz49cy5wcmV2UG9zP1wibmV4dFwiOlwicHJldmlvdXNcIikrXCIgZmFuY3lib3gtYW5pbWF0ZWQgZmFuY3lib3gtZngtXCIrZSxyLmFkZENsYXNzKG8pLnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtc2xpZGUtLWN1cnJlbnRcIiksdC4kY29udGVudC5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLWhpZGRlblwiKSxwKHIpLFwiaW1hZ2VcIiE9PXQudHlwZSYmdC4kY29udGVudC5oaWRlKCkuc2hvdygwKSx2b2lkIG4uZmFuY3lib3guYW5pbWF0ZShyLFwiZmFuY3lib3gtc2xpZGUtLWN1cnJlbnRcIixpLGZ1bmN0aW9uKCl7ci5yZW1vdmVDbGFzcyhvKS5jc3Moe3RyYW5zZm9ybTpcIlwiLG9wYWNpdHk6XCJcIn0pLHQucG9zPT09cy5jdXJyUG9zJiZzLmNvbXBsZXRlKCl9LCEwKSk6KHQuJGNvbnRlbnQucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1pcy1oaWRkZW5cIiksdXx8IWR8fFwiaW1hZ2VcIiE9PXQudHlwZXx8dC5oYXNFcnJvcnx8dC4kY29udGVudC5oaWRlKCkuZmFkZUluKFwiZmFzdFwiKSx2b2lkKHQucG9zPT09cy5jdXJyUG9zJiZzLmNvbXBsZXRlKCkpKSl9LGdldFRodW1iUG9zOmZ1bmN0aW9uKHQpe3ZhciBlLG8saSxhLHMscj0hMSxjPXQuJHRodW1iO3JldHVybiEoIWN8fCFnKGNbMF0pKSYmKGU9bi5mYW5jeWJveC5nZXRUcmFuc2xhdGUoYyksbz1wYXJzZUZsb2F0KGMuY3NzKFwiYm9yZGVyLXRvcC13aWR0aFwiKXx8MCksaT1wYXJzZUZsb2F0KGMuY3NzKFwiYm9yZGVyLXJpZ2h0LXdpZHRoXCIpfHwwKSxhPXBhcnNlRmxvYXQoYy5jc3MoXCJib3JkZXItYm90dG9tLXdpZHRoXCIpfHwwKSxzPXBhcnNlRmxvYXQoYy5jc3MoXCJib3JkZXItbGVmdC13aWR0aFwiKXx8MCkscj17dG9wOmUudG9wK28sbGVmdDplLmxlZnQrcyx3aWR0aDplLndpZHRoLWktcyxoZWlnaHQ6ZS5oZWlnaHQtby1hLHNjYWxlWDoxLHNjYWxlWToxfSxlLndpZHRoPjAmJmUuaGVpZ2h0PjAmJnIpfSxjb21wbGV0ZTpmdW5jdGlvbigpe3ZhciB0LGU9dGhpcyxvPWUuY3VycmVudCxpPXt9OyFlLmlzTW92ZWQoKSYmby5pc0xvYWRlZCYmKG8uaXNDb21wbGV0ZXx8KG8uaXNDb21wbGV0ZT0hMCxvLiRzbGlkZS5zaWJsaW5ncygpLnRyaWdnZXIoXCJvblJlc2V0XCIpLGUucHJlbG9hZChcImlubGluZVwiKSxwKG8uJHNsaWRlKSxvLiRzbGlkZS5hZGRDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1jb21wbGV0ZVwiKSxuLmVhY2goZS5zbGlkZXMsZnVuY3Rpb24odCxvKXtvLnBvcz49ZS5jdXJyUG9zLTEmJm8ucG9zPD1lLmN1cnJQb3MrMT9pW28ucG9zXT1vOm8mJihuLmZhbmN5Ym94LnN0b3Aoby4kc2xpZGUpLG8uJHNsaWRlLm9mZigpLnJlbW92ZSgpKX0pLGUuc2xpZGVzPWkpLGUuaXNBbmltYXRpbmc9ITEsZS51cGRhdGVDdXJzb3IoKSxlLnRyaWdnZXIoXCJhZnRlclNob3dcIiksby5vcHRzLnZpZGVvLmF1dG9TdGFydCYmby4kc2xpZGUuZmluZChcInZpZGVvLGF1ZGlvXCIpLmZpbHRlcihcIjp2aXNpYmxlOmZpcnN0XCIpLnRyaWdnZXIoXCJwbGF5XCIpLm9uZShcImVuZGVkXCIsZnVuY3Rpb24oKXtEb2N1bWVudC5leGl0RnVsbHNjcmVlbj9Eb2N1bWVudC5leGl0RnVsbHNjcmVlbigpOnRoaXMud2Via2l0RXhpdEZ1bGxzY3JlZW4mJnRoaXMud2Via2l0RXhpdEZ1bGxzY3JlZW4oKSxlLm5leHQoKX0pLG8ub3B0cy5hdXRvRm9jdXMmJlwiaHRtbFwiPT09by5jb250ZW50VHlwZSYmKHQ9by4kY29udGVudC5maW5kKFwiaW5wdXRbYXV0b2ZvY3VzXTplbmFibGVkOnZpc2libGU6Zmlyc3RcIiksdC5sZW5ndGg/dC50cmlnZ2VyKFwiZm9jdXNcIik6ZS5mb2N1cyhudWxsLCEwKSksby4kc2xpZGUuc2Nyb2xsVG9wKDApLnNjcm9sbExlZnQoMCkpfSxwcmVsb2FkOmZ1bmN0aW9uKHQpe3ZhciBlLG4sbz10aGlzO28uZ3JvdXAubGVuZ3RoPDJ8fChuPW8uc2xpZGVzW28uY3VyclBvcysxXSxlPW8uc2xpZGVzW28uY3VyclBvcy0xXSxlJiZlLnR5cGU9PT10JiZvLmxvYWRTbGlkZShlKSxuJiZuLnR5cGU9PT10JiZvLmxvYWRTbGlkZShuKSl9LGZvY3VzOmZ1bmN0aW9uKHQsbyl7dmFyIGksYSxzPXRoaXMscj1bXCJhW2hyZWZdXCIsXCJhcmVhW2hyZWZdXCIsJ2lucHV0Om5vdChbZGlzYWJsZWRdKTpub3QoW3R5cGU9XCJoaWRkZW5cIl0pOm5vdChbYXJpYS1oaWRkZW5dKScsXCJzZWxlY3Q6bm90KFtkaXNhYmxlZF0pOm5vdChbYXJpYS1oaWRkZW5dKVwiLFwidGV4dGFyZWE6bm90KFtkaXNhYmxlZF0pOm5vdChbYXJpYS1oaWRkZW5dKVwiLFwiYnV0dG9uOm5vdChbZGlzYWJsZWRdKTpub3QoW2FyaWEtaGlkZGVuXSlcIixcImlmcmFtZVwiLFwib2JqZWN0XCIsXCJlbWJlZFwiLFwidmlkZW9cIixcImF1ZGlvXCIsXCJbY29udGVudGVkaXRhYmxlXVwiLCdbdGFiaW5kZXhdOm5vdChbdGFiaW5kZXhePVwiLVwiXSknXS5qb2luKFwiLFwiKTtzLmlzQ2xvc2luZ3x8KGk9IXQmJnMuY3VycmVudCYmcy5jdXJyZW50LmlzQ29tcGxldGU/cy5jdXJyZW50LiRzbGlkZS5maW5kKFwiKjp2aXNpYmxlXCIrKG8/XCI6bm90KC5mYW5jeWJveC1jbG9zZS1zbWFsbClcIjpcIlwiKSk6cy4kcmVmcy5jb250YWluZXIuZmluZChcIio6dmlzaWJsZVwiKSxpPWkuZmlsdGVyKHIpLmZpbHRlcihmdW5jdGlvbigpe3JldHVyblwiaGlkZGVuXCIhPT1uKHRoaXMpLmNzcyhcInZpc2liaWxpdHlcIikmJiFuKHRoaXMpLmhhc0NsYXNzKFwiZGlzYWJsZWRcIil9KSxpLmxlbmd0aD8oYT1pLmluZGV4KGUuYWN0aXZlRWxlbWVudCksdCYmdC5zaGlmdEtleT8oYTwwfHwwPT1hKSYmKHQucHJldmVudERlZmF1bHQoKSxpLmVxKGkubGVuZ3RoLTEpLnRyaWdnZXIoXCJmb2N1c1wiKSk6KGE8MHx8YT09aS5sZW5ndGgtMSkmJih0JiZ0LnByZXZlbnREZWZhdWx0KCksaS5lcSgwKS50cmlnZ2VyKFwiZm9jdXNcIikpKTpzLiRyZWZzLmNvbnRhaW5lci50cmlnZ2VyKFwiZm9jdXNcIikpfSxhY3RpdmF0ZTpmdW5jdGlvbigpe3ZhciB0PXRoaXM7bihcIi5mYW5jeWJveC1jb250YWluZXJcIikuZWFjaChmdW5jdGlvbigpe3ZhciBlPW4odGhpcykuZGF0YShcIkZhbmN5Qm94XCIpO2UmJmUuaWQhPT10LmlkJiYhZS5pc0Nsb3NpbmcmJihlLnRyaWdnZXIoXCJvbkRlYWN0aXZhdGVcIiksZS5yZW1vdmVFdmVudHMoKSxlLmlzVmlzaWJsZT0hMSl9KSx0LmlzVmlzaWJsZT0hMCwodC5jdXJyZW50fHx0LmlzSWRsZSkmJih0LnVwZGF0ZSgpLHQudXBkYXRlQ29udHJvbHMoKSksdC50cmlnZ2VyKFwib25BY3RpdmF0ZVwiKSx0LmFkZEV2ZW50cygpfSxjbG9zZTpmdW5jdGlvbih0LGUpe3ZhciBvLGksYSxzLHIsYyxsLHU9dGhpcyxmPXUuY3VycmVudCxoPWZ1bmN0aW9uKCl7dS5jbGVhblVwKHQpfTtyZXR1cm4hdS5pc0Nsb3NpbmcmJih1LmlzQ2xvc2luZz0hMCwhMT09PXUudHJpZ2dlcihcImJlZm9yZUNsb3NlXCIsdCk/KHUuaXNDbG9zaW5nPSExLGQoZnVuY3Rpb24oKXt1LnVwZGF0ZSgpfSksITEpOih1LnJlbW92ZUV2ZW50cygpLGE9Zi4kY29udGVudCxvPWYub3B0cy5hbmltYXRpb25FZmZlY3QsaT1uLmlzTnVtZXJpYyhlKT9lOm8/Zi5vcHRzLmFuaW1hdGlvbkR1cmF0aW9uOjAsZi4kc2xpZGUucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0tY29tcGxldGUgZmFuY3lib3gtc2xpZGUtLW5leHQgZmFuY3lib3gtc2xpZGUtLXByZXZpb3VzIGZhbmN5Ym94LWFuaW1hdGVkXCIpLCEwIT09dD9uLmZhbmN5Ym94LnN0b3AoZi4kc2xpZGUpOm89ITEsZi4kc2xpZGUuc2libGluZ3MoKS50cmlnZ2VyKFwib25SZXNldFwiKS5yZW1vdmUoKSxpJiZ1LiRyZWZzLmNvbnRhaW5lci5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLW9wZW5cIikuYWRkQ2xhc3MoXCJmYW5jeWJveC1pcy1jbG9zaW5nXCIpLmNzcyhcInRyYW5zaXRpb24tZHVyYXRpb25cIixpK1wibXNcIiksdS5oaWRlTG9hZGluZyhmKSx1LmhpZGVDb250cm9scyghMCksdS51cGRhdGVDdXJzb3IoKSxcInpvb21cIiE9PW98fGEmJmkmJlwiaW1hZ2VcIj09PWYudHlwZSYmIXUuaXNNb3ZlZCgpJiYhZi5oYXNFcnJvciYmKGw9dS5nZXRUaHVtYlBvcyhmKSl8fChvPVwiZmFkZVwiKSxcInpvb21cIj09PW8/KG4uZmFuY3lib3guc3RvcChhKSxzPW4uZmFuY3lib3guZ2V0VHJhbnNsYXRlKGEpLGM9e3RvcDpzLnRvcCxsZWZ0OnMubGVmdCxzY2FsZVg6cy53aWR0aC9sLndpZHRoLHNjYWxlWTpzLmhlaWdodC9sLmhlaWdodCx3aWR0aDpsLndpZHRoLGhlaWdodDpsLmhlaWdodH0scj1mLm9wdHMuem9vbU9wYWNpdHksXHJcblwiYXV0b1wiPT1yJiYocj1NYXRoLmFicyhmLndpZHRoL2YuaGVpZ2h0LWwud2lkdGgvbC5oZWlnaHQpPi4xKSxyJiYobC5vcGFjaXR5PTApLG4uZmFuY3lib3guc2V0VHJhbnNsYXRlKGEsYykscChhKSxuLmZhbmN5Ym94LmFuaW1hdGUoYSxsLGksaCksITApOihvJiZpP24uZmFuY3lib3guYW5pbWF0ZShmLiRzbGlkZS5hZGRDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1wcmV2aW91c1wiKS5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LXNsaWRlLS1jdXJyZW50XCIpLFwiZmFuY3lib3gtYW5pbWF0ZWQgZmFuY3lib3gtZngtXCIrbyxpLGgpOiEwPT09dD9zZXRUaW1lb3V0KGgsaSk6aCgpLCEwKSkpfSxjbGVhblVwOmZ1bmN0aW9uKGUpe3ZhciBvLGksYSxzPXRoaXMscj1zLmN1cnJlbnQub3B0cy4kb3JpZztzLmN1cnJlbnQuJHNsaWRlLnRyaWdnZXIoXCJvblJlc2V0XCIpLHMuJHJlZnMuY29udGFpbmVyLmVtcHR5KCkucmVtb3ZlKCkscy50cmlnZ2VyKFwiYWZ0ZXJDbG9zZVwiLGUpLHMuY3VycmVudC5vcHRzLmJhY2tGb2N1cyYmKHImJnIubGVuZ3RoJiZyLmlzKFwiOnZpc2libGVcIil8fChyPXMuJHRyaWdnZXIpLHImJnIubGVuZ3RoJiYoaT10LnNjcm9sbFgsYT10LnNjcm9sbFksci50cmlnZ2VyKFwiZm9jdXNcIiksbihcImh0bWwsIGJvZHlcIikuc2Nyb2xsVG9wKGEpLnNjcm9sbExlZnQoaSkpKSxzLmN1cnJlbnQ9bnVsbCxvPW4uZmFuY3lib3guZ2V0SW5zdGFuY2UoKSxvP28uYWN0aXZhdGUoKToobihcImJvZHlcIikucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1hY3RpdmUgY29tcGVuc2F0ZS1mb3Itc2Nyb2xsYmFyXCIpLG4oXCIjZmFuY3lib3gtc3R5bGUtbm9zY3JvbGxcIikucmVtb3ZlKCkpfSx0cmlnZ2VyOmZ1bmN0aW9uKHQsZSl7dmFyIG8saT1BcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsMSksYT10aGlzLHM9ZSYmZS5vcHRzP2U6YS5jdXJyZW50O2lmKHM/aS51bnNoaWZ0KHMpOnM9YSxpLnVuc2hpZnQoYSksbi5pc0Z1bmN0aW9uKHMub3B0c1t0XSkmJihvPXMub3B0c1t0XS5hcHBseShzLGkpKSwhMT09PW8pcmV0dXJuIG87XCJhZnRlckNsb3NlXCIhPT10JiZhLiRyZWZzP2EuJHJlZnMuY29udGFpbmVyLnRyaWdnZXIodCtcIi5mYlwiLGkpOnIudHJpZ2dlcih0K1wiLmZiXCIsaSl9LHVwZGF0ZUNvbnRyb2xzOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpcyxvPXQuY3VycmVudCxpPW8uaW5kZXgsYT10LiRyZWZzLmNvbnRhaW5lcixzPXQuJHJlZnMuY2FwdGlvbixyPW8ub3B0cy5jYXB0aW9uO28uJHNsaWRlLnRyaWdnZXIoXCJyZWZyZXNoXCIpLHImJnIubGVuZ3RoPyh0LiRjYXB0aW9uPXMscy5jaGlsZHJlbigpLmVxKDApLmh0bWwocikpOnQuJGNhcHRpb249bnVsbCx0Lmhhc0hpZGRlbkNvbnRyb2xzfHx0LmlzSWRsZXx8dC5zaG93Q29udHJvbHMoKSxhLmZpbmQoXCJbZGF0YS1mYW5jeWJveC1jb3VudF1cIikuaHRtbCh0Lmdyb3VwLmxlbmd0aCksYS5maW5kKFwiW2RhdGEtZmFuY3lib3gtaW5kZXhdXCIpLmh0bWwoaSsxKSxhLmZpbmQoXCJbZGF0YS1mYW5jeWJveC1wcmV2XVwiKS5wcm9wKFwiZGlzYWJsZWRcIiwhby5vcHRzLmxvb3AmJmk8PTApLGEuZmluZChcIltkYXRhLWZhbmN5Ym94LW5leHRdXCIpLnByb3AoXCJkaXNhYmxlZFwiLCFvLm9wdHMubG9vcCYmaT49dC5ncm91cC5sZW5ndGgtMSksXCJpbWFnZVwiPT09by50eXBlP2EuZmluZChcIltkYXRhLWZhbmN5Ym94LXpvb21dXCIpLnNob3coKS5lbmQoKS5maW5kKFwiW2RhdGEtZmFuY3lib3gtZG93bmxvYWRdXCIpLmF0dHIoXCJocmVmXCIsby5vcHRzLmltYWdlLnNyY3x8by5zcmMpLnNob3coKTpvLm9wdHMudG9vbGJhciYmYS5maW5kKFwiW2RhdGEtZmFuY3lib3gtZG93bmxvYWRdLFtkYXRhLWZhbmN5Ym94LXpvb21dXCIpLmhpZGUoKSxuKGUuYWN0aXZlRWxlbWVudCkuaXMoXCI6aGlkZGVuLFtkaXNhYmxlZF1cIikmJnQuJHJlZnMuY29udGFpbmVyLnRyaWdnZXIoXCJmb2N1c1wiKX0saGlkZUNvbnRyb2xzOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMsbj1bXCJpbmZvYmFyXCIsXCJ0b29sYmFyXCIsXCJuYXZcIl07IXQmJmUuY3VycmVudC5vcHRzLnByZXZlbnRDYXB0aW9uT3ZlcmxhcHx8bi5wdXNoKFwiY2FwdGlvblwiKSx0aGlzLiRyZWZzLmNvbnRhaW5lci5yZW1vdmVDbGFzcyhuLm1hcChmdW5jdGlvbih0KXtyZXR1cm5cImZhbmN5Ym94LXNob3ctXCIrdH0pLmpvaW4oXCIgXCIpKSx0aGlzLmhhc0hpZGRlbkNvbnRyb2xzPSEwfSxzaG93Q29udHJvbHM6ZnVuY3Rpb24oKXt2YXIgdD10aGlzLGU9dC5jdXJyZW50P3QuY3VycmVudC5vcHRzOnQub3B0cyxuPXQuJHJlZnMuY29udGFpbmVyO3QuaGFzSGlkZGVuQ29udHJvbHM9ITEsdC5pZGxlU2Vjb25kc0NvdW50ZXI9MCxuLnRvZ2dsZUNsYXNzKFwiZmFuY3lib3gtc2hvdy10b29sYmFyXCIsISghZS50b29sYmFyfHwhZS5idXR0b25zKSkudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1zaG93LWluZm9iYXJcIiwhIShlLmluZm9iYXImJnQuZ3JvdXAubGVuZ3RoPjEpKS50b2dnbGVDbGFzcyhcImZhbmN5Ym94LXNob3ctY2FwdGlvblwiLCEhdC4kY2FwdGlvbikudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1zaG93LW5hdlwiLCEhKGUuYXJyb3dzJiZ0Lmdyb3VwLmxlbmd0aD4xKSkudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1pcy1tb2RhbFwiLCEhZS5tb2RhbCl9LHRvZ2dsZUNvbnRyb2xzOmZ1bmN0aW9uKCl7dGhpcy5oYXNIaWRkZW5Db250cm9scz90aGlzLnNob3dDb250cm9scygpOnRoaXMuaGlkZUNvbnRyb2xzKCl9fSksbi5mYW5jeWJveD17dmVyc2lvbjpcIjMuNS43XCIsZGVmYXVsdHM6YSxnZXRJbnN0YW5jZTpmdW5jdGlvbih0KXt2YXIgZT1uKCcuZmFuY3lib3gtY29udGFpbmVyOm5vdChcIi5mYW5jeWJveC1pcy1jbG9zaW5nXCIpOmxhc3QnKS5kYXRhKFwiRmFuY3lCb3hcIiksbz1BcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsMSk7cmV0dXJuIGUgaW5zdGFuY2VvZiBiJiYoXCJzdHJpbmdcIj09PW4udHlwZSh0KT9lW3RdLmFwcGx5KGUsbyk6XCJmdW5jdGlvblwiPT09bi50eXBlKHQpJiZ0LmFwcGx5KGUsbyksZSl9LG9wZW46ZnVuY3Rpb24odCxlLG4pe3JldHVybiBuZXcgYih0LGUsbil9LGNsb3NlOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuZ2V0SW5zdGFuY2UoKTtlJiYoZS5jbG9zZSgpLCEwPT09dCYmdGhpcy5jbG9zZSh0KSl9LGRlc3Ryb3k6ZnVuY3Rpb24oKXt0aGlzLmNsb3NlKCEwKSxyLmFkZChcImJvZHlcIikub2ZmKFwiY2xpY2suZmItc3RhcnRcIixcIioqXCIpfSxpc01vYmlsZTovQW5kcm9pZHx3ZWJPU3xpUGhvbmV8aVBhZHxpUG9kfEJsYWNrQmVycnl8SUVNb2JpbGV8T3BlcmEgTWluaS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCksdXNlM2Q6ZnVuY3Rpb24oKXt2YXIgbj1lLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7cmV0dXJuIHQuZ2V0Q29tcHV0ZWRTdHlsZSYmdC5nZXRDb21wdXRlZFN0eWxlKG4pJiZ0LmdldENvbXB1dGVkU3R5bGUobikuZ2V0UHJvcGVydHlWYWx1ZShcInRyYW5zZm9ybVwiKSYmIShlLmRvY3VtZW50TW9kZSYmZS5kb2N1bWVudE1vZGU8MTEpfSgpLGdldFRyYW5zbGF0ZTpmdW5jdGlvbih0KXt2YXIgZTtyZXR1cm4hKCF0fHwhdC5sZW5ndGgpJiYoZT10WzBdLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLHt0b3A6ZS50b3B8fDAsbGVmdDplLmxlZnR8fDAsd2lkdGg6ZS53aWR0aCxoZWlnaHQ6ZS5oZWlnaHQsb3BhY2l0eTpwYXJzZUZsb2F0KHQuY3NzKFwib3BhY2l0eVwiKSl9KX0sc2V0VHJhbnNsYXRlOmZ1bmN0aW9uKHQsZSl7dmFyIG49XCJcIixvPXt9O2lmKHQmJmUpcmV0dXJuIHZvaWQgMD09PWUubGVmdCYmdm9pZCAwPT09ZS50b3B8fChuPSh2b2lkIDA9PT1lLmxlZnQ/dC5wb3NpdGlvbigpLmxlZnQ6ZS5sZWZ0KStcInB4LCBcIisodm9pZCAwPT09ZS50b3A/dC5wb3NpdGlvbigpLnRvcDplLnRvcCkrXCJweFwiLG49dGhpcy51c2UzZD9cInRyYW5zbGF0ZTNkKFwiK24rXCIsIDBweClcIjpcInRyYW5zbGF0ZShcIituK1wiKVwiKSx2b2lkIDAhPT1lLnNjYWxlWCYmdm9pZCAwIT09ZS5zY2FsZVk/bis9XCIgc2NhbGUoXCIrZS5zY2FsZVgrXCIsIFwiK2Uuc2NhbGVZK1wiKVwiOnZvaWQgMCE9PWUuc2NhbGVYJiYobis9XCIgc2NhbGVYKFwiK2Uuc2NhbGVYK1wiKVwiKSxuLmxlbmd0aCYmKG8udHJhbnNmb3JtPW4pLHZvaWQgMCE9PWUub3BhY2l0eSYmKG8ub3BhY2l0eT1lLm9wYWNpdHkpLHZvaWQgMCE9PWUud2lkdGgmJihvLndpZHRoPWUud2lkdGgpLHZvaWQgMCE9PWUuaGVpZ2h0JiYoby5oZWlnaHQ9ZS5oZWlnaHQpLHQuY3NzKG8pfSxhbmltYXRlOmZ1bmN0aW9uKHQsZSxvLGksYSl7dmFyIHMscj10aGlzO24uaXNGdW5jdGlvbihvKSYmKGk9byxvPW51bGwpLHIuc3RvcCh0KSxzPXIuZ2V0VHJhbnNsYXRlKHQpLHQub24oZixmdW5jdGlvbihjKXsoIWN8fCFjLm9yaWdpbmFsRXZlbnR8fHQuaXMoYy5vcmlnaW5hbEV2ZW50LnRhcmdldCkmJlwiei1pbmRleFwiIT1jLm9yaWdpbmFsRXZlbnQucHJvcGVydHlOYW1lKSYmKHIuc3RvcCh0KSxuLmlzTnVtZXJpYyhvKSYmdC5jc3MoXCJ0cmFuc2l0aW9uLWR1cmF0aW9uXCIsXCJcIiksbi5pc1BsYWluT2JqZWN0KGUpP3ZvaWQgMCE9PWUuc2NhbGVYJiZ2b2lkIDAhPT1lLnNjYWxlWSYmci5zZXRUcmFuc2xhdGUodCx7dG9wOmUudG9wLGxlZnQ6ZS5sZWZ0LHdpZHRoOnMud2lkdGgqZS5zY2FsZVgsaGVpZ2h0OnMuaGVpZ2h0KmUuc2NhbGVZLHNjYWxlWDoxLHNjYWxlWToxfSk6ITAhPT1hJiZ0LnJlbW92ZUNsYXNzKGUpLG4uaXNGdW5jdGlvbihpKSYmaShjKSl9KSxuLmlzTnVtZXJpYyhvKSYmdC5jc3MoXCJ0cmFuc2l0aW9uLWR1cmF0aW9uXCIsbytcIm1zXCIpLG4uaXNQbGFpbk9iamVjdChlKT8odm9pZCAwIT09ZS5zY2FsZVgmJnZvaWQgMCE9PWUuc2NhbGVZJiYoZGVsZXRlIGUud2lkdGgsZGVsZXRlIGUuaGVpZ2h0LHQucGFyZW50KCkuaGFzQ2xhc3MoXCJmYW5jeWJveC1zbGlkZS0taW1hZ2VcIikmJnQucGFyZW50KCkuYWRkQ2xhc3MoXCJmYW5jeWJveC1pcy1zY2FsaW5nXCIpKSxuLmZhbmN5Ym94LnNldFRyYW5zbGF0ZSh0LGUpKTp0LmFkZENsYXNzKGUpLHQuZGF0YShcInRpbWVyXCIsc2V0VGltZW91dChmdW5jdGlvbigpe3QudHJpZ2dlcihmKX0sbyszMykpfSxzdG9wOmZ1bmN0aW9uKHQsZSl7dCYmdC5sZW5ndGgmJihjbGVhclRpbWVvdXQodC5kYXRhKFwidGltZXJcIikpLGUmJnQudHJpZ2dlcihmKSx0Lm9mZihmKS5jc3MoXCJ0cmFuc2l0aW9uLWR1cmF0aW9uXCIsXCJcIiksdC5wYXJlbnQoKS5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLXNjYWxpbmdcIikpfX0sbi5mbi5mYW5jeWJveD1mdW5jdGlvbih0KXt2YXIgZTtyZXR1cm4gdD10fHx7fSxlPXQuc2VsZWN0b3J8fCExLGU/bihcImJvZHlcIikub2ZmKFwiY2xpY2suZmItc3RhcnRcIixlKS5vbihcImNsaWNrLmZiLXN0YXJ0XCIsZSx7b3B0aW9uczp0fSxpKTp0aGlzLm9mZihcImNsaWNrLmZiLXN0YXJ0XCIpLm9uKFwiY2xpY2suZmItc3RhcnRcIix7aXRlbXM6dGhpcyxvcHRpb25zOnR9LGkpLHRoaXN9LHIub24oXCJjbGljay5mYi1zdGFydFwiLFwiW2RhdGEtZmFuY3lib3hdXCIsaSksci5vbihcImNsaWNrLmZiLXN0YXJ0XCIsXCJbZGF0YS1mYW5jeWJveC10cmlnZ2VyXVwiLGZ1bmN0aW9uKHQpe24oJ1tkYXRhLWZhbmN5Ym94PVwiJytuKHRoaXMpLmF0dHIoXCJkYXRhLWZhbmN5Ym94LXRyaWdnZXJcIikrJ1wiXScpLmVxKG4odGhpcykuYXR0cihcImRhdGEtZmFuY3lib3gtaW5kZXhcIil8fDApLnRyaWdnZXIoXCJjbGljay5mYi1zdGFydFwiLHskdHJpZ2dlcjpuKHRoaXMpfSl9KSxmdW5jdGlvbigpe3ZhciB0PW51bGw7ci5vbihcIm1vdXNlZG93biBtb3VzZXVwIGZvY3VzIGJsdXJcIixcIi5mYW5jeWJveC1idXR0b25cIixmdW5jdGlvbihlKXtzd2l0Y2goZS50eXBlKXtjYXNlXCJtb3VzZWRvd25cIjp0PW4odGhpcyk7YnJlYWs7Y2FzZVwibW91c2V1cFwiOnQ9bnVsbDticmVhaztjYXNlXCJmb2N1c2luXCI6bihcIi5mYW5jeWJveC1idXR0b25cIikucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1mb2N1c1wiKSxuKHRoaXMpLmlzKHQpfHxuKHRoaXMpLmlzKFwiW2Rpc2FibGVkXVwiKXx8bih0aGlzKS5hZGRDbGFzcyhcImZhbmN5Ym94LWZvY3VzXCIpO2JyZWFrO2Nhc2VcImZvY3Vzb3V0XCI6bihcIi5mYW5jeWJveC1idXR0b25cIikucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1mb2N1c1wiKX19KX0oKX19KHdpbmRvdyxkb2N1bWVudCxqUXVlcnkpLGZ1bmN0aW9uKHQpe1widXNlIHN0cmljdFwiO3ZhciBlPXt5b3V0dWJlOnttYXRjaGVyOi8oeW91dHViZVxcLmNvbXx5b3V0dVxcLmJlfHlvdXR1YmVcXC1ub2Nvb2tpZVxcLmNvbSlcXC8od2F0Y2hcXD8oLiomKT92PXx2XFwvfHVcXC98ZW1iZWRcXC8/KT8odmlkZW9zZXJpZXNcXD9saXN0PSguKil8W1xcdy1dezExfXxcXD9saXN0VHlwZT0oLiopJmxpc3Q9KC4qKSkoLiopL2kscGFyYW1zOnthdXRvcGxheToxLGF1dG9oaWRlOjEsZnM6MSxyZWw6MCxoZDoxLHdtb2RlOlwidHJhbnNwYXJlbnRcIixlbmFibGVqc2FwaToxLGh0bWw1OjF9LHBhcmFtUGxhY2U6OCx0eXBlOlwiaWZyYW1lXCIsdXJsOlwiaHR0cHM6Ly93d3cueW91dHViZS1ub2Nvb2tpZS5jb20vZW1iZWQvJDRcIix0aHVtYjpcImh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyQ0L2hxZGVmYXVsdC5qcGdcIn0sdmltZW86e21hdGNoZXI6L14uK3ZpbWVvLmNvbVxcLyguKlxcLyk/KFtcXGRdKykoLiopPy8scGFyYW1zOnthdXRvcGxheToxLGhkOjEsc2hvd190aXRsZToxLHNob3dfYnlsaW5lOjEsc2hvd19wb3J0cmFpdDowLGZ1bGxzY3JlZW46MX0scGFyYW1QbGFjZTozLHR5cGU6XCJpZnJhbWVcIix1cmw6XCIvL3BsYXllci52aW1lby5jb20vdmlkZW8vJDJcIn0saW5zdGFncmFtOnttYXRjaGVyOi8oaW5zdGFnclxcLmFtfGluc3RhZ3JhbVxcLmNvbSlcXC9wXFwvKFthLXpBLVowLTlfXFwtXSspXFwvPy9pLHR5cGU6XCJpbWFnZVwiLHVybDpcIi8vJDEvcC8kMi9tZWRpYS8/c2l6ZT1sXCJ9LGdtYXBfcGxhY2U6e21hdGNoZXI6LyhtYXBzXFwuKT9nb29nbGVcXC4oW2Etel17MiwzfShcXC5bYS16XXsyfSk/KVxcLygoKG1hcHNcXC8ocGxhY2VcXC8oLiopXFwvKT9cXEAoLiopLChcXGQrLj9cXGQrPyl6KSl8KFxcP2xsPSkpKC4qKT8vaSx0eXBlOlwiaWZyYW1lXCIsdXJsOmZ1bmN0aW9uKHQpe3JldHVyblwiLy9tYXBzLmdvb2dsZS5cIit0WzJdK1wiLz9sbD1cIisodFs5XT90WzldK1wiJno9XCIrTWF0aC5mbG9vcih0WzEwXSkrKHRbMTJdP3RbMTJdLnJlcGxhY2UoL15cXC8vLFwiJlwiKTpcIlwiKTp0WzEyXStcIlwiKS5yZXBsYWNlKC9cXD8vLFwiJlwiKStcIiZvdXRwdXQ9XCIrKHRbMTJdJiZ0WzEyXS5pbmRleE9mKFwibGF5ZXI9Y1wiKT4wP1wic3ZlbWJlZFwiOlwiZW1iZWRcIil9fSxnbWFwX3NlYXJjaDp7bWF0Y2hlcjovKG1hcHNcXC4pP2dvb2dsZVxcLihbYS16XXsyLDN9KFxcLlthLXpdezJ9KT8pXFwvKG1hcHNcXC9zZWFyY2hcXC8pKC4qKS9pLHR5cGU6XCJpZnJhbWVcIix1cmw6ZnVuY3Rpb24odCl7cmV0dXJuXCIvL21hcHMuZ29vZ2xlLlwiK3RbMl0rXCIvbWFwcz9xPVwiK3RbNV0ucmVwbGFjZShcInF1ZXJ5PVwiLFwicT1cIikucmVwbGFjZShcImFwaT0xXCIsXCJcIikrXCImb3V0cHV0PWVtYmVkXCJ9fX0sbj1mdW5jdGlvbihlLG4sbyl7aWYoZSlyZXR1cm4gbz1vfHxcIlwiLFwib2JqZWN0XCI9PT10LnR5cGUobykmJihvPXQucGFyYW0obywhMCkpLHQuZWFjaChuLGZ1bmN0aW9uKHQsbil7ZT1lLnJlcGxhY2UoXCIkXCIrdCxufHxcIlwiKX0pLG8ubGVuZ3RoJiYoZSs9KGUuaW5kZXhPZihcIj9cIik+MD9cIiZcIjpcIj9cIikrbyksZX07dChkb2N1bWVudCkub24oXCJvYmplY3ROZWVkc1R5cGUuZmJcIixmdW5jdGlvbihvLGksYSl7dmFyIHMscixjLGwsZCx1LGYscD1hLnNyY3x8XCJcIixoPSExO3M9dC5leHRlbmQoITAse30sZSxhLm9wdHMubWVkaWEpLHQuZWFjaChzLGZ1bmN0aW9uKGUsbyl7aWYoYz1wLm1hdGNoKG8ubWF0Y2hlcikpe2lmKGg9by50eXBlLGY9ZSx1PXt9LG8ucGFyYW1QbGFjZSYmY1tvLnBhcmFtUGxhY2VdKXtkPWNbby5wYXJhbVBsYWNlXSxcIj9cIj09ZFswXSYmKGQ9ZC5zdWJzdHJpbmcoMSkpLGQ9ZC5zcGxpdChcIiZcIik7Zm9yKHZhciBpPTA7aTxkLmxlbmd0aDsrK2kpe3ZhciBzPWRbaV0uc3BsaXQoXCI9XCIsMik7Mj09cy5sZW5ndGgmJih1W3NbMF1dPWRlY29kZVVSSUNvbXBvbmVudChzWzFdLnJlcGxhY2UoL1xcKy9nLFwiIFwiKSkpfX1yZXR1cm4gbD10LmV4dGVuZCghMCx7fSxvLnBhcmFtcyxhLm9wdHNbZV0sdSkscD1cImZ1bmN0aW9uXCI9PT10LnR5cGUoby51cmwpP28udXJsLmNhbGwodGhpcyxjLGwsYSk6bihvLnVybCxjLGwpLHI9XCJmdW5jdGlvblwiPT09dC50eXBlKG8udGh1bWIpP28udGh1bWIuY2FsbCh0aGlzLGMsbCxhKTpuKG8udGh1bWIsYyksXCJ5b3V0dWJlXCI9PT1lP3A9cC5yZXBsYWNlKC8mdD0oKFxcZCspbSk/KFxcZCspcy8sZnVuY3Rpb24odCxlLG4sbyl7cmV0dXJuXCImc3RhcnQ9XCIrKChuPzYwKnBhcnNlSW50KG4sMTApOjApK3BhcnNlSW50KG8sMTApKX0pOlwidmltZW9cIj09PWUmJihwPXAucmVwbGFjZShcIiYlMjNcIixcIiNcIikpLCExfX0pLGg/KGEub3B0cy50aHVtYnx8YS5vcHRzLiR0aHVtYiYmYS5vcHRzLiR0aHVtYi5sZW5ndGh8fChhLm9wdHMudGh1bWI9ciksXCJpZnJhbWVcIj09PWgmJihhLm9wdHM9dC5leHRlbmQoITAsYS5vcHRzLHtpZnJhbWU6e3ByZWxvYWQ6ITEsYXR0cjp7c2Nyb2xsaW5nOlwibm9cIn19fSkpLHQuZXh0ZW5kKGEse3R5cGU6aCxzcmM6cCxvcmlnU3JjOmEuc3JjLGNvbnRlbnRTb3VyY2U6Zixjb250ZW50VHlwZTpcImltYWdlXCI9PT1oP1wiaW1hZ2VcIjpcImdtYXBfcGxhY2VcIj09Znx8XCJnbWFwX3NlYXJjaFwiPT1mP1wibWFwXCI6XCJ2aWRlb1wifSkpOnAmJihhLnR5cGU9YS5vcHRzLmRlZmF1bHRUeXBlKX0pO3ZhciBvPXt5b3V0dWJlOntzcmM6XCJodHRwczovL3d3dy55b3V0dWJlLmNvbS9pZnJhbWVfYXBpXCIsY2xhc3M6XCJZVFwiLGxvYWRpbmc6ITEsbG9hZGVkOiExfSx2aW1lbzp7c3JjOlwiaHR0cHM6Ly9wbGF5ZXIudmltZW8uY29tL2FwaS9wbGF5ZXIuanNcIixjbGFzczpcIlZpbWVvXCIsbG9hZGluZzohMSxsb2FkZWQ6ITF9LGxvYWQ6ZnVuY3Rpb24odCl7dmFyIGUsbj10aGlzO2lmKHRoaXNbdF0ubG9hZGVkKXJldHVybiB2b2lkIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtuLmRvbmUodCl9KTt0aGlzW3RdLmxvYWRpbmd8fCh0aGlzW3RdLmxvYWRpbmc9ITAsZT1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic2NyaXB0XCIpLGUudHlwZT1cInRleHQvamF2YXNjcmlwdFwiLGUuc3JjPXRoaXNbdF0uc3JjLFwieW91dHViZVwiPT09dD93aW5kb3cub25Zb3VUdWJlSWZyYW1lQVBJUmVhZHk9ZnVuY3Rpb24oKXtuW3RdLmxvYWRlZD0hMCxuLmRvbmUodCl9OmUub25sb2FkPWZ1bmN0aW9uKCl7blt0XS5sb2FkZWQ9ITAsbi5kb25lKHQpfSxkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGUpKX0sZG9uZTpmdW5jdGlvbihlKXt2YXIgbixvLGk7XCJ5b3V0dWJlXCI9PT1lJiZkZWxldGUgd2luZG93Lm9uWW91VHViZUlmcmFtZUFQSVJlYWR5LChuPXQuZmFuY3lib3guZ2V0SW5zdGFuY2UoKSkmJihvPW4uY3VycmVudC4kY29udGVudC5maW5kKFwiaWZyYW1lXCIpLFwieW91dHViZVwiPT09ZSYmdm9pZCAwIT09WVQmJllUP2k9bmV3IFlULlBsYXllcihvLmF0dHIoXCJpZFwiKSx7ZXZlbnRzOntvblN0YXRlQ2hhbmdlOmZ1bmN0aW9uKHQpezA9PXQuZGF0YSYmbi5uZXh0KCl9fX0pOlwidmltZW9cIj09PWUmJnZvaWQgMCE9PVZpbWVvJiZWaW1lbyYmKGk9bmV3IFZpbWVvLlBsYXllcihvKSxpLm9uKFwiZW5kZWRcIixmdW5jdGlvbigpe24ubmV4dCgpfSkpKX19O3QoZG9jdW1lbnQpLm9uKHtcImFmdGVyU2hvdy5mYlwiOmZ1bmN0aW9uKHQsZSxuKXtlLmdyb3VwLmxlbmd0aD4xJiYoXCJ5b3V0dWJlXCI9PT1uLmNvbnRlbnRTb3VyY2V8fFwidmltZW9cIj09PW4uY29udGVudFNvdXJjZSkmJm8ubG9hZChuLmNvbnRlbnRTb3VyY2UpfX0pfShqUXVlcnkpLGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgbz1mdW5jdGlvbigpe3JldHVybiB0LnJlcXVlc3RBbmltYXRpb25GcmFtZXx8dC53ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWV8fHQubW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lfHx0Lm9SZXF1ZXN0QW5pbWF0aW9uRnJhbWV8fGZ1bmN0aW9uKGUpe3JldHVybiB0LnNldFRpbWVvdXQoZSwxZTMvNjApfX0oKSxpPWZ1bmN0aW9uKCl7cmV0dXJuIHQuY2FuY2VsQW5pbWF0aW9uRnJhbWV8fHQud2Via2l0Q2FuY2VsQW5pbWF0aW9uRnJhbWV8fHQubW96Q2FuY2VsQW5pbWF0aW9uRnJhbWV8fHQub0NhbmNlbEFuaW1hdGlvbkZyYW1lfHxmdW5jdGlvbihlKXt0LmNsZWFyVGltZW91dChlKX19KCksYT1mdW5jdGlvbihlKXt2YXIgbj1bXTtlPWUub3JpZ2luYWxFdmVudHx8ZXx8dC5lLGU9ZS50b3VjaGVzJiZlLnRvdWNoZXMubGVuZ3RoP2UudG91Y2hlczplLmNoYW5nZWRUb3VjaGVzJiZlLmNoYW5nZWRUb3VjaGVzLmxlbmd0aD9lLmNoYW5nZWRUb3VjaGVzOltlXTtmb3IodmFyIG8gaW4gZSllW29dLnBhZ2VYP24ucHVzaCh7eDplW29dLnBhZ2VYLHk6ZVtvXS5wYWdlWX0pOmVbb10uY2xpZW50WCYmbi5wdXNoKHt4OmVbb10uY2xpZW50WCx5OmVbb10uY2xpZW50WX0pO3JldHVybiBufSxzPWZ1bmN0aW9uKHQsZSxuKXtyZXR1cm4gZSYmdD9cInhcIj09PW4/dC54LWUueDpcInlcIj09PW4/dC55LWUueTpNYXRoLnNxcnQoTWF0aC5wb3codC54LWUueCwyKStNYXRoLnBvdyh0LnktZS55LDIpKTowfSxyPWZ1bmN0aW9uKHQpe2lmKHQuaXMoJ2EsYXJlYSxidXR0b24sW3JvbGU9XCJidXR0b25cIl0saW5wdXQsbGFiZWwsc2VsZWN0LHN1bW1hcnksdGV4dGFyZWEsdmlkZW8sYXVkaW8saWZyYW1lJyl8fG4uaXNGdW5jdGlvbih0LmdldCgwKS5vbmNsaWNrKXx8dC5kYXRhKFwic2VsZWN0YWJsZVwiKSlyZXR1cm4hMDtmb3IodmFyIGU9MCxvPXRbMF0uYXR0cmlidXRlcyxpPW8ubGVuZ3RoO2U8aTtlKyspaWYoXCJkYXRhLWZhbmN5Ym94LVwiPT09b1tlXS5ub2RlTmFtZS5zdWJzdHIoMCwxNCkpcmV0dXJuITA7cmV0dXJuITF9LGM9ZnVuY3Rpb24oZSl7dmFyIG49dC5nZXRDb21wdXRlZFN0eWxlKGUpW1wib3ZlcmZsb3cteVwiXSxvPXQuZ2V0Q29tcHV0ZWRTdHlsZShlKVtcIm92ZXJmbG93LXhcIl0saT0oXCJzY3JvbGxcIj09PW58fFwiYXV0b1wiPT09bikmJmUuc2Nyb2xsSGVpZ2h0PmUuY2xpZW50SGVpZ2h0LGE9KFwic2Nyb2xsXCI9PT1vfHxcImF1dG9cIj09PW8pJiZlLnNjcm9sbFdpZHRoPmUuY2xpZW50V2lkdGg7cmV0dXJuIGl8fGF9LGw9ZnVuY3Rpb24odCl7Zm9yKHZhciBlPSExOzspe2lmKGU9Yyh0LmdldCgwKSkpYnJlYWs7aWYodD10LnBhcmVudCgpLCF0Lmxlbmd0aHx8dC5oYXNDbGFzcyhcImZhbmN5Ym94LXN0YWdlXCIpfHx0LmlzKFwiYm9keVwiKSlicmVha31yZXR1cm4gZX0sZD1mdW5jdGlvbih0KXt2YXIgZT10aGlzO2UuaW5zdGFuY2U9dCxlLiRiZz10LiRyZWZzLmJnLGUuJHN0YWdlPXQuJHJlZnMuc3RhZ2UsZS4kY29udGFpbmVyPXQuJHJlZnMuY29udGFpbmVyLGUuZGVzdHJveSgpLGUuJGNvbnRhaW5lci5vbihcInRvdWNoc3RhcnQuZmIudG91Y2ggbW91c2Vkb3duLmZiLnRvdWNoXCIsbi5wcm94eShlLFwib250b3VjaHN0YXJ0XCIpKX07ZC5wcm90b3R5cGUuZGVzdHJveT1mdW5jdGlvbigpe3ZhciB0PXRoaXM7dC4kY29udGFpbmVyLm9mZihcIi5mYi50b3VjaFwiKSxuKGUpLm9mZihcIi5mYi50b3VjaFwiKSx0LnJlcXVlc3RJZCYmKGkodC5yZXF1ZXN0SWQpLHQucmVxdWVzdElkPW51bGwpLHQudGFwcGVkJiYoY2xlYXJUaW1lb3V0KHQudGFwcGVkKSx0LnRhcHBlZD1udWxsKX0sZC5wcm90b3R5cGUub250b3VjaHN0YXJ0PWZ1bmN0aW9uKG8pe3ZhciBpPXRoaXMsYz1uKG8udGFyZ2V0KSxkPWkuaW5zdGFuY2UsdT1kLmN1cnJlbnQsZj11LiRzbGlkZSxwPXUuJGNvbnRlbnQsaD1cInRvdWNoc3RhcnRcIj09by50eXBlO2lmKGgmJmkuJGNvbnRhaW5lci5vZmYoXCJtb3VzZWRvd24uZmIudG91Y2hcIiksKCFvLm9yaWdpbmFsRXZlbnR8fDIhPW8ub3JpZ2luYWxFdmVudC5idXR0b24pJiZmLmxlbmd0aCYmYy5sZW5ndGgmJiFyKGMpJiYhcihjLnBhcmVudCgpKSYmKGMuaXMoXCJpbWdcIil8fCEoby5vcmlnaW5hbEV2ZW50LmNsaWVudFg+Y1swXS5jbGllbnRXaWR0aCtjLm9mZnNldCgpLmxlZnQpKSl7aWYoIXV8fGQuaXNBbmltYXRpbmd8fHUuJHNsaWRlLmhhc0NsYXNzKFwiZmFuY3lib3gtYW5pbWF0ZWRcIikpcmV0dXJuIG8uc3RvcFByb3BhZ2F0aW9uKCksdm9pZCBvLnByZXZlbnREZWZhdWx0KCk7aS5yZWFsUG9pbnRzPWkuc3RhcnRQb2ludHM9YShvKSxpLnN0YXJ0UG9pbnRzLmxlbmd0aCYmKHUudG91Y2gmJm8uc3RvcFByb3BhZ2F0aW9uKCksaS5zdGFydEV2ZW50PW8saS5jYW5UYXA9ITAsaS4kdGFyZ2V0PWMsaS4kY29udGVudD1wLGkub3B0cz11Lm9wdHMudG91Y2gsaS5pc1Bhbm5pbmc9ITEsaS5pc1N3aXBpbmc9ITEsaS5pc1pvb21pbmc9ITEsaS5pc1Njcm9sbGluZz0hMSxpLmNhblBhbj1kLmNhblBhbigpLGkuc3RhcnRUaW1lPShuZXcgRGF0ZSkuZ2V0VGltZSgpLGkuZGlzdGFuY2VYPWkuZGlzdGFuY2VZPWkuZGlzdGFuY2U9MCxpLmNhbnZhc1dpZHRoPU1hdGgucm91bmQoZlswXS5jbGllbnRXaWR0aCksaS5jYW52YXNIZWlnaHQ9TWF0aC5yb3VuZChmWzBdLmNsaWVudEhlaWdodCksaS5jb250ZW50TGFzdFBvcz1udWxsLGkuY29udGVudFN0YXJ0UG9zPW4uZmFuY3lib3guZ2V0VHJhbnNsYXRlKGkuJGNvbnRlbnQpfHx7dG9wOjAsbGVmdDowfSxpLnNsaWRlclN0YXJ0UG9zPW4uZmFuY3lib3guZ2V0VHJhbnNsYXRlKGYpLGkuc3RhZ2VQb3M9bi5mYW5jeWJveC5nZXRUcmFuc2xhdGUoZC4kcmVmcy5zdGFnZSksaS5zbGlkZXJTdGFydFBvcy50b3AtPWkuc3RhZ2VQb3MudG9wLGkuc2xpZGVyU3RhcnRQb3MubGVmdC09aS5zdGFnZVBvcy5sZWZ0LGkuY29udGVudFN0YXJ0UG9zLnRvcC09aS5zdGFnZVBvcy50b3AsaS5jb250ZW50U3RhcnRQb3MubGVmdC09aS5zdGFnZVBvcy5sZWZ0LG4oZSkub2ZmKFwiLmZiLnRvdWNoXCIpLm9uKGg/XCJ0b3VjaGVuZC5mYi50b3VjaCB0b3VjaGNhbmNlbC5mYi50b3VjaFwiOlwibW91c2V1cC5mYi50b3VjaCBtb3VzZWxlYXZlLmZiLnRvdWNoXCIsbi5wcm94eShpLFwib250b3VjaGVuZFwiKSkub24oaD9cInRvdWNobW92ZS5mYi50b3VjaFwiOlwibW91c2Vtb3ZlLmZiLnRvdWNoXCIsbi5wcm94eShpLFwib250b3VjaG1vdmVcIikpLG4uZmFuY3lib3guaXNNb2JpbGUmJmUuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLGkub25zY3JvbGwsITApLCgoaS5vcHRzfHxpLmNhblBhbikmJihjLmlzKGkuJHN0YWdlKXx8aS4kc3RhZ2UuZmluZChjKS5sZW5ndGgpfHwoYy5pcyhcIi5mYW5jeWJveC1pbWFnZVwiKSYmby5wcmV2ZW50RGVmYXVsdCgpLG4uZmFuY3lib3guaXNNb2JpbGUmJmMucGFyZW50cyhcIi5mYW5jeWJveC1jYXB0aW9uXCIpLmxlbmd0aCkpJiYoaS5pc1Njcm9sbGFibGU9bChjKXx8bChjLnBhcmVudCgpKSxuLmZhbmN5Ym94LmlzTW9iaWxlJiZpLmlzU2Nyb2xsYWJsZXx8by5wcmV2ZW50RGVmYXVsdCgpLCgxPT09aS5zdGFydFBvaW50cy5sZW5ndGh8fHUuaGFzRXJyb3IpJiYoaS5jYW5QYW4/KG4uZmFuY3lib3guc3RvcChpLiRjb250ZW50KSxpLmlzUGFubmluZz0hMCk6aS5pc1N3aXBpbmc9ITAsaS4kY29udGFpbmVyLmFkZENsYXNzKFwiZmFuY3lib3gtaXMtZ3JhYmJpbmdcIikpLDI9PT1pLnN0YXJ0UG9pbnRzLmxlbmd0aCYmXCJpbWFnZVwiPT09dS50eXBlJiYodS5pc0xvYWRlZHx8dS4kZ2hvc3QpJiYoaS5jYW5UYXA9ITEsaS5pc1N3aXBpbmc9ITEsaS5pc1Bhbm5pbmc9ITEsaS5pc1pvb21pbmc9ITAsbi5mYW5jeWJveC5zdG9wKGkuJGNvbnRlbnQpLGkuY2VudGVyUG9pbnRTdGFydFg9LjUqKGkuc3RhcnRQb2ludHNbMF0ueCtpLnN0YXJ0UG9pbnRzWzFdLngpLW4odCkuc2Nyb2xsTGVmdCgpLGkuY2VudGVyUG9pbnRTdGFydFk9LjUqKGkuc3RhcnRQb2ludHNbMF0ueStpLnN0YXJ0UG9pbnRzWzFdLnkpLW4odCkuc2Nyb2xsVG9wKCksaS5wZXJjZW50YWdlT2ZJbWFnZUF0UGluY2hQb2ludFg9KGkuY2VudGVyUG9pbnRTdGFydFgtaS5jb250ZW50U3RhcnRQb3MubGVmdCkvaS5jb250ZW50U3RhcnRQb3Mud2lkdGgsaS5wZXJjZW50YWdlT2ZJbWFnZUF0UGluY2hQb2ludFk9KGkuY2VudGVyUG9pbnRTdGFydFktaS5jb250ZW50U3RhcnRQb3MudG9wKS9pLmNvbnRlbnRTdGFydFBvcy5oZWlnaHQsaS5zdGFydERpc3RhbmNlQmV0d2VlbkZpbmdlcnM9cyhpLnN0YXJ0UG9pbnRzWzBdLGkuc3RhcnRQb2ludHNbMV0pKSkpfX0sZC5wcm90b3R5cGUub25zY3JvbGw9ZnVuY3Rpb24odCl7dmFyIG49dGhpcztuLmlzU2Nyb2xsaW5nPSEwLGUucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLG4ub25zY3JvbGwsITApfSxkLnByb3RvdHlwZS5vbnRvdWNobW92ZT1mdW5jdGlvbih0KXt2YXIgZT10aGlzO3JldHVybiB2b2lkIDAhPT10Lm9yaWdpbmFsRXZlbnQuYnV0dG9ucyYmMD09PXQub3JpZ2luYWxFdmVudC5idXR0b25zP3ZvaWQgZS5vbnRvdWNoZW5kKHQpOmUuaXNTY3JvbGxpbmc/dm9pZChlLmNhblRhcD0hMSk6KGUubmV3UG9pbnRzPWEodCksdm9pZCgoZS5vcHRzfHxlLmNhblBhbikmJmUubmV3UG9pbnRzLmxlbmd0aCYmZS5uZXdQb2ludHMubGVuZ3RoJiYoZS5pc1N3aXBpbmcmJiEwPT09ZS5pc1N3aXBpbmd8fHQucHJldmVudERlZmF1bHQoKSxlLmRpc3RhbmNlWD1zKGUubmV3UG9pbnRzWzBdLGUuc3RhcnRQb2ludHNbMF0sXCJ4XCIpLGUuZGlzdGFuY2VZPXMoZS5uZXdQb2ludHNbMF0sZS5zdGFydFBvaW50c1swXSxcInlcIiksZS5kaXN0YW5jZT1zKGUubmV3UG9pbnRzWzBdLGUuc3RhcnRQb2ludHNbMF0pLGUuZGlzdGFuY2U+MCYmKGUuaXNTd2lwaW5nP2Uub25Td2lwZSh0KTplLmlzUGFubmluZz9lLm9uUGFuKCk6ZS5pc1pvb21pbmcmJmUub25ab29tKCkpKSkpfSxkLnByb3RvdHlwZS5vblN3aXBlPWZ1bmN0aW9uKGUpe3ZhciBhLHM9dGhpcyxyPXMuaW5zdGFuY2UsYz1zLmlzU3dpcGluZyxsPXMuc2xpZGVyU3RhcnRQb3MubGVmdHx8MDtpZighMCE9PWMpXCJ4XCI9PWMmJihzLmRpc3RhbmNlWD4wJiYocy5pbnN0YW5jZS5ncm91cC5sZW5ndGg8Mnx8MD09PXMuaW5zdGFuY2UuY3VycmVudC5pbmRleCYmIXMuaW5zdGFuY2UuY3VycmVudC5vcHRzLmxvb3ApP2wrPU1hdGgucG93KHMuZGlzdGFuY2VYLC44KTpzLmRpc3RhbmNlWDwwJiYocy5pbnN0YW5jZS5ncm91cC5sZW5ndGg8Mnx8cy5pbnN0YW5jZS5jdXJyZW50LmluZGV4PT09cy5pbnN0YW5jZS5ncm91cC5sZW5ndGgtMSYmIXMuaW5zdGFuY2UuY3VycmVudC5vcHRzLmxvb3ApP2wtPU1hdGgucG93KC1zLmRpc3RhbmNlWCwuOCk6bCs9cy5kaXN0YW5jZVgpLHMuc2xpZGVyTGFzdFBvcz17dG9wOlwieFwiPT1jPzA6cy5zbGlkZXJTdGFydFBvcy50b3Arcy5kaXN0YW5jZVksbGVmdDpsfSxzLnJlcXVlc3RJZCYmKGkocy5yZXF1ZXN0SWQpLHMucmVxdWVzdElkPW51bGwpLHMucmVxdWVzdElkPW8oZnVuY3Rpb24oKXtzLnNsaWRlckxhc3RQb3MmJihuLmVhY2gocy5pbnN0YW5jZS5zbGlkZXMsZnVuY3Rpb24odCxlKXt2YXIgbz1lLnBvcy1zLmluc3RhbmNlLmN1cnJQb3M7bi5mYW5jeWJveC5zZXRUcmFuc2xhdGUoZS4kc2xpZGUse3RvcDpzLnNsaWRlckxhc3RQb3MudG9wLGxlZnQ6cy5zbGlkZXJMYXN0UG9zLmxlZnQrbypzLmNhbnZhc1dpZHRoK28qZS5vcHRzLmd1dHRlcn0pfSkscy4kY29udGFpbmVyLmFkZENsYXNzKFwiZmFuY3lib3gtaXMtc2xpZGluZ1wiKSl9KTtlbHNlIGlmKE1hdGguYWJzKHMuZGlzdGFuY2UpPjEwKXtpZihzLmNhblRhcD0hMSxyLmdyb3VwLmxlbmd0aDwyJiZzLm9wdHMudmVydGljYWw/cy5pc1N3aXBpbmc9XCJ5XCI6ci5pc0RyYWdnaW5nfHwhMT09PXMub3B0cy52ZXJ0aWNhbHx8XCJhdXRvXCI9PT1zLm9wdHMudmVydGljYWwmJm4odCkud2lkdGgoKT44MDA/cy5pc1N3aXBpbmc9XCJ4XCI6KGE9TWF0aC5hYnMoMTgwKk1hdGguYXRhbjIocy5kaXN0YW5jZVkscy5kaXN0YW5jZVgpL01hdGguUEkpLHMuaXNTd2lwaW5nPWE+NDUmJmE8MTM1P1wieVwiOlwieFwiKSxcInlcIj09PXMuaXNTd2lwaW5nJiZuLmZhbmN5Ym94LmlzTW9iaWxlJiZzLmlzU2Nyb2xsYWJsZSlyZXR1cm4gdm9pZChzLmlzU2Nyb2xsaW5nPSEwKTtyLmlzRHJhZ2dpbmc9cy5pc1N3aXBpbmcscy5zdGFydFBvaW50cz1zLm5ld1BvaW50cyxuLmVhY2goci5zbGlkZXMsZnVuY3Rpb24odCxlKXt2YXIgbyxpO24uZmFuY3lib3guc3RvcChlLiRzbGlkZSksbz1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShlLiRzbGlkZSksaT1uLmZhbmN5Ym94LmdldFRyYW5zbGF0ZShyLiRyZWZzLnN0YWdlKSxlLiRzbGlkZS5jc3Moe3RyYW5zZm9ybTpcIlwiLG9wYWNpdHk6XCJcIixcInRyYW5zaXRpb24tZHVyYXRpb25cIjpcIlwifSkucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1hbmltYXRlZFwiKS5yZW1vdmVDbGFzcyhmdW5jdGlvbih0LGUpe3JldHVybihlLm1hdGNoKC8oXnxcXHMpZmFuY3lib3gtZngtXFxTKy9nKXx8W10pLmpvaW4oXCIgXCIpfSksZS5wb3M9PT1yLmN1cnJlbnQucG9zJiYocy5zbGlkZXJTdGFydFBvcy50b3A9by50b3AtaS50b3Ascy5zbGlkZXJTdGFydFBvcy5sZWZ0PW8ubGVmdC1pLmxlZnQpLG4uZmFuY3lib3guc2V0VHJhbnNsYXRlKGUuJHNsaWRlLHt0b3A6by50b3AtaS50b3AsbGVmdDpvLmxlZnQtaS5sZWZ0fSl9KSxyLlNsaWRlU2hvdyYmci5TbGlkZVNob3cuaXNBY3RpdmUmJnIuU2xpZGVTaG93LnN0b3AoKX19LGQucHJvdG90eXBlLm9uUGFuPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcztpZihzKHQubmV3UG9pbnRzWzBdLHQucmVhbFBvaW50c1swXSk8KG4uZmFuY3lib3guaXNNb2JpbGU/MTA6NSkpcmV0dXJuIHZvaWQodC5zdGFydFBvaW50cz10Lm5ld1BvaW50cyk7dC5jYW5UYXA9ITEsdC5jb250ZW50TGFzdFBvcz10LmxpbWl0TW92ZW1lbnQoKSx0LnJlcXVlc3RJZCYmaSh0LnJlcXVlc3RJZCksdC5yZXF1ZXN0SWQ9byhmdW5jdGlvbigpe24uZmFuY3lib3guc2V0VHJhbnNsYXRlKHQuJGNvbnRlbnQsdC5jb250ZW50TGFzdFBvcyl9KX0sZC5wcm90b3R5cGUubGltaXRNb3ZlbWVudD1mdW5jdGlvbigpe3ZhciB0LGUsbixvLGksYSxzPXRoaXMscj1zLmNhbnZhc1dpZHRoLGM9cy5jYW52YXNIZWlnaHQsbD1zLmRpc3RhbmNlWCxkPXMuZGlzdGFuY2VZLHU9cy5jb250ZW50U3RhcnRQb3MsZj11LmxlZnQscD11LnRvcCxoPXUud2lkdGgsZz11LmhlaWdodDtyZXR1cm4gaT1oPnI/ZitsOmYsYT1wK2QsdD1NYXRoLm1heCgwLC41KnItLjUqaCksZT1NYXRoLm1heCgwLC41KmMtLjUqZyksbj1NYXRoLm1pbihyLWgsLjUqci0uNSpoKSxvPU1hdGgubWluKGMtZywuNSpjLS41KmcpLGw+MCYmaT50JiYoaT10LTErTWF0aC5wb3coLXQrZitsLC44KXx8MCksbDwwJiZpPG4mJihpPW4rMS1NYXRoLnBvdyhuLWYtbCwuOCl8fDApLGQ+MCYmYT5lJiYoYT1lLTErTWF0aC5wb3coLWUrcCtkLC44KXx8MCksZDwwJiZhPG8mJihhPW8rMS1NYXRoLnBvdyhvLXAtZCwuOCl8fDApLHt0b3A6YSxsZWZ0Oml9fSxkLnByb3RvdHlwZS5saW1pdFBvc2l0aW9uPWZ1bmN0aW9uKHQsZSxuLG8pe3ZhciBpPXRoaXMsYT1pLmNhbnZhc1dpZHRoLHM9aS5jYW52YXNIZWlnaHQ7cmV0dXJuIG4+YT8odD10PjA/MDp0LHQ9dDxhLW4/YS1uOnQpOnQ9TWF0aC5tYXgoMCxhLzItbi8yKSxvPnM/KGU9ZT4wPzA6ZSxlPWU8cy1vP3MtbzplKTplPU1hdGgubWF4KDAscy8yLW8vMikse3RvcDplLGxlZnQ6dH19LGQucHJvdG90eXBlLm9uWm9vbT1mdW5jdGlvbigpe3ZhciBlPXRoaXMsYT1lLmNvbnRlbnRTdGFydFBvcyxyPWEud2lkdGgsYz1hLmhlaWdodCxsPWEubGVmdCxkPWEudG9wLHU9cyhlLm5ld1BvaW50c1swXSxlLm5ld1BvaW50c1sxXSksZj11L2Uuc3RhcnREaXN0YW5jZUJldHdlZW5GaW5nZXJzLHA9TWF0aC5mbG9vcihyKmYpLGg9TWF0aC5mbG9vcihjKmYpLGc9KHItcCkqZS5wZXJjZW50YWdlT2ZJbWFnZUF0UGluY2hQb2ludFgsYj0oYy1oKSplLnBlcmNlbnRhZ2VPZkltYWdlQXRQaW5jaFBvaW50WSxtPShlLm5ld1BvaW50c1swXS54K2UubmV3UG9pbnRzWzFdLngpLzItbih0KS5zY3JvbGxMZWZ0KCksdj0oZS5uZXdQb2ludHNbMF0ueStlLm5ld1BvaW50c1sxXS55KS8yLW4odCkuc2Nyb2xsVG9wKCkseT1tLWUuY2VudGVyUG9pbnRTdGFydFgseD12LWUuY2VudGVyUG9pbnRTdGFydFksdz1sKyhnK3kpLCQ9ZCsoYit4KSxTPXt0b3A6JCxsZWZ0Oncsc2NhbGVYOmYsc2NhbGVZOmZ9O2UuY2FuVGFwPSExLGUubmV3V2lkdGg9cCxlLm5ld0hlaWdodD1oLGUuY29udGVudExhc3RQb3M9UyxlLnJlcXVlc3RJZCYmaShlLnJlcXVlc3RJZCksZS5yZXF1ZXN0SWQ9byhmdW5jdGlvbigpe24uZmFuY3lib3guc2V0VHJhbnNsYXRlKGUuJGNvbnRlbnQsZS5jb250ZW50TGFzdFBvcyl9KX0sZC5wcm90b3R5cGUub250b3VjaGVuZD1mdW5jdGlvbih0KXt2YXIgbz10aGlzLHM9by5pc1N3aXBpbmcscj1vLmlzUGFubmluZyxjPW8uaXNab29taW5nLGw9by5pc1Njcm9sbGluZztpZihvLmVuZFBvaW50cz1hKHQpLG8uZE1zPU1hdGgubWF4KChuZXcgRGF0ZSkuZ2V0VGltZSgpLW8uc3RhcnRUaW1lLDEpLG8uJGNvbnRhaW5lci5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLWdyYWJiaW5nXCIpLG4oZSkub2ZmKFwiLmZiLnRvdWNoXCIpLGUucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLG8ub25zY3JvbGwsITApLG8ucmVxdWVzdElkJiYoaShvLnJlcXVlc3RJZCksby5yZXF1ZXN0SWQ9bnVsbCksby5pc1N3aXBpbmc9ITEsby5pc1Bhbm5pbmc9ITEsby5pc1pvb21pbmc9ITEsby5pc1Njcm9sbGluZz0hMSxvLmluc3RhbmNlLmlzRHJhZ2dpbmc9ITEsby5jYW5UYXApcmV0dXJuIG8ub25UYXAodCk7by5zcGVlZD0xMDAsby52ZWxvY2l0eVg9by5kaXN0YW5jZVgvby5kTXMqLjUsby52ZWxvY2l0eVk9by5kaXN0YW5jZVkvby5kTXMqLjUscj9vLmVuZFBhbm5pbmcoKTpjP28uZW5kWm9vbWluZygpOm8uZW5kU3dpcGluZyhzLGwpfSxkLnByb3RvdHlwZS5lbmRTd2lwaW5nPWZ1bmN0aW9uKHQsZSl7dmFyIG89dGhpcyxpPSExLGE9by5pbnN0YW5jZS5ncm91cC5sZW5ndGgscz1NYXRoLmFicyhvLmRpc3RhbmNlWCkscj1cInhcIj09dCYmYT4xJiYoby5kTXM+MTMwJiZzPjEwfHxzPjUwKTtvLnNsaWRlckxhc3RQb3M9bnVsbCxcInlcIj09dCYmIWUmJk1hdGguYWJzKG8uZGlzdGFuY2VZKT41MD8obi5mYW5jeWJveC5hbmltYXRlKG8uaW5zdGFuY2UuY3VycmVudC4kc2xpZGUse3RvcDpvLnNsaWRlclN0YXJ0UG9zLnRvcCtvLmRpc3RhbmNlWSsxNTAqby52ZWxvY2l0eVksb3BhY2l0eTowfSwyMDApLGk9by5pbnN0YW5jZS5jbG9zZSghMCwyNTApKTpyJiZvLmRpc3RhbmNlWD4wP2k9by5pbnN0YW5jZS5wcmV2aW91cygzMDApOnImJm8uZGlzdGFuY2VYPDAmJihpPW8uaW5zdGFuY2UubmV4dCgzMDApKSwhMSE9PWl8fFwieFwiIT10JiZcInlcIiE9dHx8by5pbnN0YW5jZS5jZW50ZXJTbGlkZSgyMDApLG8uJGNvbnRhaW5lci5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWlzLXNsaWRpbmdcIil9LGQucHJvdG90eXBlLmVuZFBhbm5pbmc9ZnVuY3Rpb24oKXt2YXIgdCxlLG8saT10aGlzO2kuY29udGVudExhc3RQb3MmJighMT09PWkub3B0cy5tb21lbnR1bXx8aS5kTXM+MzUwPyh0PWkuY29udGVudExhc3RQb3MubGVmdCxlPWkuY29udGVudExhc3RQb3MudG9wKToodD1pLmNvbnRlbnRMYXN0UG9zLmxlZnQrNTAwKmkudmVsb2NpdHlYLGU9aS5jb250ZW50TGFzdFBvcy50b3ArNTAwKmkudmVsb2NpdHlZKSxvPWkubGltaXRQb3NpdGlvbih0LGUsaS5jb250ZW50U3RhcnRQb3Mud2lkdGgsaS5jb250ZW50U3RhcnRQb3MuaGVpZ2h0KSxvLndpZHRoPWkuY29udGVudFN0YXJ0UG9zLndpZHRoLG8uaGVpZ2h0PWkuY29udGVudFN0YXJ0UG9zLmhlaWdodCxuLmZhbmN5Ym94LmFuaW1hdGUoaS4kY29udGVudCxvLDM2NikpfSxkLnByb3RvdHlwZS5lbmRab29taW5nPWZ1bmN0aW9uKCl7dmFyIHQsZSxvLGksYT10aGlzLHM9YS5pbnN0YW5jZS5jdXJyZW50LHI9YS5uZXdXaWR0aCxjPWEubmV3SGVpZ2h0O2EuY29udGVudExhc3RQb3MmJih0PWEuY29udGVudExhc3RQb3MubGVmdCxlPWEuY29udGVudExhc3RQb3MudG9wLGk9e3RvcDplLGxlZnQ6dCx3aWR0aDpyLGhlaWdodDpjLHNjYWxlWDoxLHNjYWxlWToxfSxuLmZhbmN5Ym94LnNldFRyYW5zbGF0ZShhLiRjb250ZW50LGkpLHI8YS5jYW52YXNXaWR0aCYmYzxhLmNhbnZhc0hlaWdodD9hLmluc3RhbmNlLnNjYWxlVG9GaXQoMTUwKTpyPnMud2lkdGh8fGM+cy5oZWlnaHQ/YS5pbnN0YW5jZS5zY2FsZVRvQWN0dWFsKGEuY2VudGVyUG9pbnRTdGFydFgsYS5jZW50ZXJQb2ludFN0YXJ0WSwxNTApOihvPWEubGltaXRQb3NpdGlvbih0LGUscixjKSxuLmZhbmN5Ym94LmFuaW1hdGUoYS4kY29udGVudCxvLDE1MCkpKX0sZC5wcm90b3R5cGUub25UYXA9ZnVuY3Rpb24oZSl7dmFyIG8saT10aGlzLHM9bihlLnRhcmdldCkscj1pLmluc3RhbmNlLGM9ci5jdXJyZW50LGw9ZSYmYShlKXx8aS5zdGFydFBvaW50cyxkPWxbMF0/bFswXS54LW4odCkuc2Nyb2xsTGVmdCgpLWkuc3RhZ2VQb3MubGVmdDowLHU9bFswXT9sWzBdLnktbih0KS5zY3JvbGxUb3AoKS1pLnN0YWdlUG9zLnRvcDowLGY9ZnVuY3Rpb24odCl7dmFyIG89Yy5vcHRzW3RdO2lmKG4uaXNGdW5jdGlvbihvKSYmKG89by5hcHBseShyLFtjLGVdKSksbylzd2l0Y2gobyl7Y2FzZVwiY2xvc2VcIjpyLmNsb3NlKGkuc3RhcnRFdmVudCk7YnJlYWs7Y2FzZVwidG9nZ2xlQ29udHJvbHNcIjpyLnRvZ2dsZUNvbnRyb2xzKCk7YnJlYWs7Y2FzZVwibmV4dFwiOnIubmV4dCgpO2JyZWFrO2Nhc2VcIm5leHRPckNsb3NlXCI6ci5ncm91cC5sZW5ndGg+MT9yLm5leHQoKTpyLmNsb3NlKGkuc3RhcnRFdmVudCk7YnJlYWs7Y2FzZVwiem9vbVwiOlwiaW1hZ2VcIj09Yy50eXBlJiYoYy5pc0xvYWRlZHx8Yy4kZ2hvc3QpJiYoci5jYW5QYW4oKT9yLnNjYWxlVG9GaXQoKTpyLmlzU2NhbGVkRG93bigpP3Iuc2NhbGVUb0FjdHVhbChkLHUpOnIuZ3JvdXAubGVuZ3RoPDImJnIuY2xvc2UoaS5zdGFydEV2ZW50KSl9fTtpZigoIWUub3JpZ2luYWxFdmVudHx8MiE9ZS5vcmlnaW5hbEV2ZW50LmJ1dHRvbikmJihzLmlzKFwiaW1nXCIpfHwhKGQ+c1swXS5jbGllbnRXaWR0aCtzLm9mZnNldCgpLmxlZnQpKSl7aWYocy5pcyhcIi5mYW5jeWJveC1iZywuZmFuY3lib3gtaW5uZXIsLmZhbmN5Ym94LW91dGVyLC5mYW5jeWJveC1jb250YWluZXJcIikpbz1cIk91dHNpZGVcIjtlbHNlIGlmKHMuaXMoXCIuZmFuY3lib3gtc2xpZGVcIikpbz1cIlNsaWRlXCI7ZWxzZXtpZighci5jdXJyZW50LiRjb250ZW50fHwhci5jdXJyZW50LiRjb250ZW50LmZpbmQocykuYWRkQmFjaygpLmZpbHRlcihzKS5sZW5ndGgpcmV0dXJuO289XCJDb250ZW50XCJ9aWYoaS50YXBwZWQpe2lmKGNsZWFyVGltZW91dChpLnRhcHBlZCksaS50YXBwZWQ9bnVsbCxNYXRoLmFicyhkLWkudGFwWCk+NTB8fE1hdGguYWJzKHUtaS50YXBZKT41MClyZXR1cm4gdGhpcztmKFwiZGJsY2xpY2tcIitvKX1lbHNlIGkudGFwWD1kLGkudGFwWT11LGMub3B0c1tcImRibGNsaWNrXCIrb10mJmMub3B0c1tcImRibGNsaWNrXCIrb10hPT1jLm9wdHNbXCJjbGlja1wiK29dP2kudGFwcGVkPXNldFRpbWVvdXQoZnVuY3Rpb24oKXtpLnRhcHBlZD1udWxsLHIuaXNBbmltYXRpbmd8fGYoXCJjbGlja1wiK28pfSw1MDApOmYoXCJjbGlja1wiK28pO3JldHVybiB0aGlzfX0sbihlKS5vbihcIm9uQWN0aXZhdGUuZmJcIixmdW5jdGlvbih0LGUpe2UmJiFlLkd1ZXN0dXJlcyYmKGUuR3Vlc3R1cmVzPW5ldyBkKGUpKX0pLm9uKFwiYmVmb3JlQ2xvc2UuZmJcIixmdW5jdGlvbih0LGUpe2UmJmUuR3Vlc3R1cmVzJiZlLkd1ZXN0dXJlcy5kZXN0cm95KCl9KX0od2luZG93LGRvY3VtZW50LGpRdWVyeSksZnVuY3Rpb24odCxlKXtcInVzZSBzdHJpY3RcIjtlLmV4dGVuZCghMCxlLmZhbmN5Ym94LmRlZmF1bHRzLHtidG5UcGw6e3NsaWRlU2hvdzonPGJ1dHRvbiBkYXRhLWZhbmN5Ym94LXBsYXkgY2xhc3M9XCJmYW5jeWJveC1idXR0b24gZmFuY3lib3gtYnV0dG9uLS1wbGF5XCIgdGl0bGU9XCJ7e1BMQVlfU1RBUlR9fVwiPjxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHZpZXdCb3g9XCIwIDAgMjQgMjRcIj48cGF0aCBkPVwiTTYuNSA1LjR2MTMuMmwxMS02LjZ6XCIvPjwvc3ZnPjxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHZpZXdCb3g9XCIwIDAgMjQgMjRcIj48cGF0aCBkPVwiTTguMzMgNS43NWgyLjJ2MTIuNWgtMi4yVjUuNzV6bTUuMTUgMGgyLjJ2MTIuNWgtMi4yVjUuNzV6XCIvPjwvc3ZnPjwvYnV0dG9uPid9LHNsaWRlU2hvdzp7YXV0b1N0YXJ0OiExLHNwZWVkOjNlMyxwcm9ncmVzczohMH19KTt2YXIgbj1mdW5jdGlvbih0KXt0aGlzLmluc3RhbmNlPXQsdGhpcy5pbml0KCl9O2UuZXh0ZW5kKG4ucHJvdG90eXBlLHt0aW1lcjpudWxsLGlzQWN0aXZlOiExLCRidXR0b246bnVsbCxpbml0OmZ1bmN0aW9uKCl7dmFyIHQ9dGhpcyxuPXQuaW5zdGFuY2Usbz1uLmdyb3VwW24uY3VyckluZGV4XS5vcHRzLnNsaWRlU2hvdzt0LiRidXR0b249bi4kcmVmcy50b29sYmFyLmZpbmQoXCJbZGF0YS1mYW5jeWJveC1wbGF5XVwiKS5vbihcImNsaWNrXCIsZnVuY3Rpb24oKXt0LnRvZ2dsZSgpfSksbi5ncm91cC5sZW5ndGg8Mnx8IW8/dC4kYnV0dG9uLmhpZGUoKTpvLnByb2dyZXNzJiYodC4kcHJvZ3Jlc3M9ZSgnPGRpdiBjbGFzcz1cImZhbmN5Ym94LXByb2dyZXNzXCI+PC9kaXY+JykuYXBwZW5kVG8obi4kcmVmcy5pbm5lcikpfSxzZXQ6ZnVuY3Rpb24odCl7dmFyIG49dGhpcyxvPW4uaW5zdGFuY2UsaT1vLmN1cnJlbnQ7aSYmKCEwPT09dHx8aS5vcHRzLmxvb3B8fG8uY3VyckluZGV4PG8uZ3JvdXAubGVuZ3RoLTEpP24uaXNBY3RpdmUmJlwidmlkZW9cIiE9PWkuY29udGVudFR5cGUmJihuLiRwcm9ncmVzcyYmZS5mYW5jeWJveC5hbmltYXRlKG4uJHByb2dyZXNzLnNob3coKSx7c2NhbGVYOjF9LGkub3B0cy5zbGlkZVNob3cuc3BlZWQpLG4udGltZXI9c2V0VGltZW91dChmdW5jdGlvbigpe28uY3VycmVudC5vcHRzLmxvb3B8fG8uY3VycmVudC5pbmRleCE9by5ncm91cC5sZW5ndGgtMT9vLm5leHQoKTpvLmp1bXBUbygwKX0saS5vcHRzLnNsaWRlU2hvdy5zcGVlZCkpOihuLnN0b3AoKSxvLmlkbGVTZWNvbmRzQ291bnRlcj0wLG8uc2hvd0NvbnRyb2xzKCkpfSxjbGVhcjpmdW5jdGlvbigpe3ZhciB0PXRoaXM7Y2xlYXJUaW1lb3V0KHQudGltZXIpLHQudGltZXI9bnVsbCx0LiRwcm9ncmVzcyYmdC4kcHJvZ3Jlc3MucmVtb3ZlQXR0cihcInN0eWxlXCIpLmhpZGUoKX0sc3RhcnQ6ZnVuY3Rpb24oKXt2YXIgdD10aGlzLGU9dC5pbnN0YW5jZS5jdXJyZW50O2UmJih0LiRidXR0b24uYXR0cihcInRpdGxlXCIsKGUub3B0cy5pMThuW2Uub3B0cy5sYW5nXXx8ZS5vcHRzLmkxOG4uZW4pLlBMQVlfU1RPUCkucmVtb3ZlQ2xhc3MoXCJmYW5jeWJveC1idXR0b24tLXBsYXlcIikuYWRkQ2xhc3MoXCJmYW5jeWJveC1idXR0b24tLXBhdXNlXCIpLHQuaXNBY3RpdmU9ITAsZS5pc0NvbXBsZXRlJiZ0LnNldCghMCksdC5pbnN0YW5jZS50cmlnZ2VyKFwib25TbGlkZVNob3dDaGFuZ2VcIiwhMCkpfSxzdG9wOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpcyxlPXQuaW5zdGFuY2UuY3VycmVudDt0LmNsZWFyKCksdC4kYnV0dG9uLmF0dHIoXCJ0aXRsZVwiLChlLm9wdHMuaTE4bltlLm9wdHMubGFuZ118fGUub3B0cy5pMThuLmVuKS5QTEFZX1NUQVJUKS5yZW1vdmVDbGFzcyhcImZhbmN5Ym94LWJ1dHRvbi0tcGF1c2VcIikuYWRkQ2xhc3MoXCJmYW5jeWJveC1idXR0b24tLXBsYXlcIiksdC5pc0FjdGl2ZT0hMSx0Lmluc3RhbmNlLnRyaWdnZXIoXCJvblNsaWRlU2hvd0NoYW5nZVwiLCExKSx0LiRwcm9ncmVzcyYmdC4kcHJvZ3Jlc3MucmVtb3ZlQXR0cihcInN0eWxlXCIpLmhpZGUoKX0sdG9nZ2xlOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpczt0LmlzQWN0aXZlP3Quc3RvcCgpOnQuc3RhcnQoKX19KSxlKHQpLm9uKHtcIm9uSW5pdC5mYlwiOmZ1bmN0aW9uKHQsZSl7ZSYmIWUuU2xpZGVTaG93JiYoZS5TbGlkZVNob3c9bmV3IG4oZSkpfSxcImJlZm9yZVNob3cuZmJcIjpmdW5jdGlvbih0LGUsbixvKXt2YXIgaT1lJiZlLlNsaWRlU2hvdztvP2kmJm4ub3B0cy5zbGlkZVNob3cuYXV0b1N0YXJ0JiZpLnN0YXJ0KCk6aSYmaS5pc0FjdGl2ZSYmaS5jbGVhcigpfSxcImFmdGVyU2hvdy5mYlwiOmZ1bmN0aW9uKHQsZSxuKXt2YXIgbz1lJiZlLlNsaWRlU2hvdztvJiZvLmlzQWN0aXZlJiZvLnNldCgpfSxcImFmdGVyS2V5ZG93bi5mYlwiOmZ1bmN0aW9uKG4sbyxpLGEscyl7dmFyIHI9byYmby5TbGlkZVNob3c7IXJ8fCFpLm9wdHMuc2xpZGVTaG93fHw4MCE9PXMmJjMyIT09c3x8ZSh0LmFjdGl2ZUVsZW1lbnQpLmlzKFwiYnV0dG9uLGEsaW5wdXRcIil8fChhLnByZXZlbnREZWZhdWx0KCksci50b2dnbGUoKSl9LFwiYmVmb3JlQ2xvc2UuZmIgb25EZWFjdGl2YXRlLmZiXCI6ZnVuY3Rpb24odCxlKXt2YXIgbj1lJiZlLlNsaWRlU2hvdztuJiZuLnN0b3AoKX19KSxlKHQpLm9uKFwidmlzaWJpbGl0eWNoYW5nZVwiLGZ1bmN0aW9uKCl7dmFyIG49ZS5mYW5jeWJveC5nZXRJbnN0YW5jZSgpLG89biYmbi5TbGlkZVNob3c7byYmby5pc0FjdGl2ZSYmKHQuaGlkZGVuP28uY2xlYXIoKTpvLnNldCgpKX0pfShkb2N1bWVudCxqUXVlcnkpLGZ1bmN0aW9uKHQsZSl7XCJ1c2Ugc3RyaWN0XCI7dmFyIG49ZnVuY3Rpb24oKXtmb3IodmFyIGU9W1tcInJlcXVlc3RGdWxsc2NyZWVuXCIsXCJleGl0RnVsbHNjcmVlblwiLFwiZnVsbHNjcmVlbkVsZW1lbnRcIixcImZ1bGxzY3JlZW5FbmFibGVkXCIsXCJmdWxsc2NyZWVuY2hhbmdlXCIsXCJmdWxsc2NyZWVuZXJyb3JcIl0sW1wid2Via2l0UmVxdWVzdEZ1bGxzY3JlZW5cIixcIndlYmtpdEV4aXRGdWxsc2NyZWVuXCIsXCJ3ZWJraXRGdWxsc2NyZWVuRWxlbWVudFwiLFwid2Via2l0RnVsbHNjcmVlbkVuYWJsZWRcIixcIndlYmtpdGZ1bGxzY3JlZW5jaGFuZ2VcIixcIndlYmtpdGZ1bGxzY3JlZW5lcnJvclwiXSxbXCJ3ZWJraXRSZXF1ZXN0RnVsbFNjcmVlblwiLFwid2Via2l0Q2FuY2VsRnVsbFNjcmVlblwiLFwid2Via2l0Q3VycmVudEZ1bGxTY3JlZW5FbGVtZW50XCIsXCJ3ZWJraXRDYW5jZWxGdWxsU2NyZWVuXCIsXCJ3ZWJraXRmdWxsc2NyZWVuY2hhbmdlXCIsXCJ3ZWJraXRmdWxsc2NyZWVuZXJyb3JcIl0sW1wibW96UmVxdWVzdEZ1bGxTY3JlZW5cIixcIm1vekNhbmNlbEZ1bGxTY3JlZW5cIixcIm1vekZ1bGxTY3JlZW5FbGVtZW50XCIsXCJtb3pGdWxsU2NyZWVuRW5hYmxlZFwiLFwibW96ZnVsbHNjcmVlbmNoYW5nZVwiLFwibW96ZnVsbHNjcmVlbmVycm9yXCJdLFtcIm1zUmVxdWVzdEZ1bGxzY3JlZW5cIixcIm1zRXhpdEZ1bGxzY3JlZW5cIixcIm1zRnVsbHNjcmVlbkVsZW1lbnRcIixcIm1zRnVsbHNjcmVlbkVuYWJsZWRcIixcIk1TRnVsbHNjcmVlbkNoYW5nZVwiLFwiTVNGdWxsc2NyZWVuRXJyb3JcIl1dLG49e30sbz0wO288ZS5sZW5ndGg7bysrKXt2YXIgaT1lW29dO2lmKGkmJmlbMV1pbiB0KXtmb3IodmFyIGE9MDthPGkubGVuZ3RoO2ErKyluW2VbMF1bYV1dPWlbYV07cmV0dXJuIG59fXJldHVybiExfSgpO2lmKG4pe3ZhciBvPXtyZXF1ZXN0OmZ1bmN0aW9uKGUpe2U9ZXx8dC5kb2N1bWVudEVsZW1lbnQsZVtuLnJlcXVlc3RGdWxsc2NyZWVuXShlLkFMTE9XX0tFWUJPQVJEX0lOUFVUKX0sZXhpdDpmdW5jdGlvbigpe3Rbbi5leGl0RnVsbHNjcmVlbl0oKX0sdG9nZ2xlOmZ1bmN0aW9uKGUpe2U9ZXx8dC5kb2N1bWVudEVsZW1lbnQsdGhpcy5pc0Z1bGxzY3JlZW4oKT90aGlzLmV4aXQoKTp0aGlzLnJlcXVlc3QoZSl9LGlzRnVsbHNjcmVlbjpmdW5jdGlvbigpe3JldHVybiBCb29sZWFuKHRbbi5mdWxsc2NyZWVuRWxlbWVudF0pfSxlbmFibGVkOmZ1bmN0aW9uKCl7cmV0dXJuIEJvb2xlYW4odFtuLmZ1bGxzY3JlZW5FbmFibGVkXSl9fTtlLmV4dGVuZCghMCxlLmZhbmN5Ym94LmRlZmF1bHRzLHtidG5UcGw6e2Z1bGxTY3JlZW46JzxidXR0b24gZGF0YS1mYW5jeWJveC1mdWxsc2NyZWVuIGNsYXNzPVwiZmFuY3lib3gtYnV0dG9uIGZhbmN5Ym94LWJ1dHRvbi0tZnNlbnRlclwiIHRpdGxlPVwie3tGVUxMX1NDUkVFTn19XCI+PHN2ZyB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIgdmlld0JveD1cIjAgMCAyNCAyNFwiPjxwYXRoIGQ9XCJNNyAxNEg1djVoNXYtMkg3di0zem0tMi00aDJWN2gzVjVINXY1em0xMiA3aC0zdjJoNXYtNWgtMnYzek0xNCA1djJoM3YzaDJWNWgtNXpcIi8+PC9zdmc+PHN2ZyB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIgdmlld0JveD1cIjAgMCAyNCAyNFwiPjxwYXRoIGQ9XCJNNSAxNmgzdjNoMnYtNUg1em0zLThINXYyaDVWNUg4em02IDExaDJ2LTNoM3YtMmgtNXptMi0xMVY1aC0ydjVoNVY4elwiLz48L3N2Zz48L2J1dHRvbj4nfSxmdWxsU2NyZWVuOnthdXRvU3RhcnQ6ITF9fSksZSh0KS5vbihuLmZ1bGxzY3JlZW5jaGFuZ2UsZnVuY3Rpb24oKXt2YXIgdD1vLmlzRnVsbHNjcmVlbigpLG49ZS5mYW5jeWJveC5nZXRJbnN0YW5jZSgpO24mJihuLmN1cnJlbnQmJlwiaW1hZ2VcIj09PW4uY3VycmVudC50eXBlJiZuLmlzQW5pbWF0aW5nJiYobi5pc0FuaW1hdGluZz0hMSxuLnVwZGF0ZSghMCwhMCwwKSxuLmlzQ29tcGxldGV8fG4uY29tcGxldGUoKSksbi50cmlnZ2VyKFwib25GdWxsc2NyZWVuQ2hhbmdlXCIsdCksbi4kcmVmcy5jb250YWluZXIudG9nZ2xlQ2xhc3MoXCJmYW5jeWJveC1pcy1mdWxsc2NyZWVuXCIsdCksbi4kcmVmcy50b29sYmFyLmZpbmQoXCJbZGF0YS1mYW5jeWJveC1mdWxsc2NyZWVuXVwiKS50b2dnbGVDbGFzcyhcImZhbmN5Ym94LWJ1dHRvbi0tZnNlbnRlclwiLCF0KS50b2dnbGVDbGFzcyhcImZhbmN5Ym94LWJ1dHRvbi0tZnNleGl0XCIsdCkpfSl9ZSh0KS5vbih7XCJvbkluaXQuZmJcIjpmdW5jdGlvbih0LGUpe3ZhciBpO2lmKCFuKXJldHVybiB2b2lkIGUuJHJlZnMudG9vbGJhci5maW5kKFwiW2RhdGEtZmFuY3lib3gtZnVsbHNjcmVlbl1cIikucmVtb3ZlKCk7ZSYmZS5ncm91cFtlLmN1cnJJbmRleF0ub3B0cy5mdWxsU2NyZWVuPyhpPWUuJHJlZnMuY29udGFpbmVyLGkub24oXCJjbGljay5mYi1mdWxsc2NyZWVuXCIsXCJbZGF0YS1mYW5jeWJveC1mdWxsc2NyZWVuXVwiLGZ1bmN0aW9uKHQpe3Quc3RvcFByb3BhZ2F0aW9uKCksdC5wcmV2ZW50RGVmYXVsdCgpLG8udG9nZ2xlKCl9KSxlLm9wdHMuZnVsbFNjcmVlbiYmITA9PT1lLm9wdHMuZnVsbFNjcmVlbi5hdXRvU3RhcnQmJm8ucmVxdWVzdCgpLGUuRnVsbFNjcmVlbj1vKTplJiZlLiRyZWZzLnRvb2xiYXIuZmluZChcIltkYXRhLWZhbmN5Ym94LWZ1bGxzY3JlZW5dXCIpLmhpZGUoKX0sXCJhZnRlcktleWRvd24uZmJcIjpmdW5jdGlvbih0LGUsbixvLGkpe2UmJmUuRnVsbFNjcmVlbiYmNzA9PT1pJiYoby5wcmV2ZW50RGVmYXVsdCgpLGUuRnVsbFNjcmVlbi50b2dnbGUoKSl9LFwiYmVmb3JlQ2xvc2UuZmJcIjpmdW5jdGlvbih0LGUpe2UmJmUuRnVsbFNjcmVlbiYmZS4kcmVmcy5jb250YWluZXIuaGFzQ2xhc3MoXCJmYW5jeWJveC1pcy1mdWxsc2NyZWVuXCIpJiZvLmV4aXQoKX19KX0oZG9jdW1lbnQsalF1ZXJ5KSxmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO3ZhciBuPVwiZmFuY3lib3gtdGh1bWJzXCI7ZS5mYW5jeWJveC5kZWZhdWx0cz1lLmV4dGVuZCghMCx7YnRuVHBsOnt0aHVtYnM6JzxidXR0b24gZGF0YS1mYW5jeWJveC10aHVtYnMgY2xhc3M9XCJmYW5jeWJveC1idXR0b24gZmFuY3lib3gtYnV0dG9uLS10aHVtYnNcIiB0aXRsZT1cInt7VEhVTUJTfX1cIj48c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB2aWV3Qm94PVwiMCAwIDI0IDI0XCI+PHBhdGggZD1cIk0xNC41OSAxNC41OWgzLjc2djMuNzZoLTMuNzZ2LTMuNzZ6bS00LjQ3IDBoMy43NnYzLjc2aC0zLjc2di0zLjc2em0tNC40NyAwaDMuNzZ2My43Nkg1LjY1di0zLjc2em04Ljk0LTQuNDdoMy43NnYzLjc2aC0zLjc2di0zLjc2em0tNC40NyAwaDMuNzZ2My43NmgtMy43NnYtMy43NnptLTQuNDcgMGgzLjc2djMuNzZINS42NXYtMy43NnptOC45NC00LjQ3aDMuNzZ2My43NmgtMy43NlY1LjY1em0tNC40NyAwaDMuNzZ2My43NmgtMy43NlY1LjY1em0tNC40NyAwaDMuNzZ2My43Nkg1LjY1VjUuNjV6XCIvPjwvc3ZnPjwvYnV0dG9uPid9LHRodW1iczp7YXV0b1N0YXJ0OiExLGhpZGVPbkNsb3NlOiEwLHBhcmVudEVsOlwiLmZhbmN5Ym94LWNvbnRhaW5lclwiLGF4aXM6XCJ5XCJ9fSxlLmZhbmN5Ym94LmRlZmF1bHRzKTt2YXIgbz1mdW5jdGlvbih0KXt0aGlzLmluaXQodCl9O2UuZXh0ZW5kKG8ucHJvdG90eXBlLHskYnV0dG9uOm51bGwsJGdyaWQ6bnVsbCwkbGlzdDpudWxsLGlzVmlzaWJsZTohMSxpc0FjdGl2ZTohMSxpbml0OmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMsbj10Lmdyb3VwLG89MDtlLmluc3RhbmNlPXQsZS5vcHRzPW5bdC5jdXJySW5kZXhdLm9wdHMudGh1bWJzLHQuVGh1bWJzPWUsZS4kYnV0dG9uPXQuJHJlZnMudG9vbGJhci5maW5kKFwiW2RhdGEtZmFuY3lib3gtdGh1bWJzXVwiKTtmb3IodmFyIGk9MCxhPW4ubGVuZ3RoO2k8YSYmKG5baV0udGh1bWImJm8rKywhKG8+MSkpO2krKyk7bz4xJiZlLm9wdHM/KGUuJGJ1dHRvbi5yZW1vdmVBdHRyKFwic3R5bGVcIikub24oXCJjbGlja1wiLGZ1bmN0aW9uKCl7ZS50b2dnbGUoKX0pLGUuaXNBY3RpdmU9ITApOmUuJGJ1dHRvbi5oaWRlKCl9LGNyZWF0ZTpmdW5jdGlvbigpe3ZhciB0LG89dGhpcyxpPW8uaW5zdGFuY2UsYT1vLm9wdHMucGFyZW50RWwscz1bXTtvLiRncmlkfHwoby4kZ3JpZD1lKCc8ZGl2IGNsYXNzPVwiJytuK1wiIFwiK24rXCItXCIrby5vcHRzLmF4aXMrJ1wiPjwvZGl2PicpLmFwcGVuZFRvKGkuJHJlZnMuY29udGFpbmVyLmZpbmQoYSkuYWRkQmFjaygpLmZpbHRlcihhKSksby4kZ3JpZC5vbihcImNsaWNrXCIsXCJhXCIsZnVuY3Rpb24oKXtpLmp1bXBUbyhlKHRoaXMpLmF0dHIoXCJkYXRhLWluZGV4XCIpKX0pKSxvLiRsaXN0fHwoby4kbGlzdD1lKCc8ZGl2IGNsYXNzPVwiJytuKydfX2xpc3RcIj4nKS5hcHBlbmRUbyhvLiRncmlkKSksZS5lYWNoKGkuZ3JvdXAsZnVuY3Rpb24oZSxuKXt0PW4udGh1bWIsdHx8XCJpbWFnZVwiIT09bi50eXBlfHwodD1uLnNyYykscy5wdXNoKCc8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgdGFiaW5kZXg9XCIwXCIgZGF0YS1pbmRleD1cIicrZSsnXCInKyh0JiZ0Lmxlbmd0aD8nIHN0eWxlPVwiYmFja2dyb3VuZC1pbWFnZTp1cmwoJyt0KycpXCInOidjbGFzcz1cImZhbmN5Ym94LXRodW1icy1taXNzaW5nXCInKStcIj48L2E+XCIpfSksby4kbGlzdFswXS5pbm5lckhUTUw9cy5qb2luKFwiXCIpLFwieFwiPT09by5vcHRzLmF4aXMmJm8uJGxpc3Qud2lkdGgocGFyc2VJbnQoby4kZ3JpZC5jc3MoXCJwYWRkaW5nLXJpZ2h0XCIpLDEwKStpLmdyb3VwLmxlbmd0aCpvLiRsaXN0LmNoaWxkcmVuKCkuZXEoMCkub3V0ZXJXaWR0aCghMCkpfSxmb2N1czpmdW5jdGlvbih0KXt2YXIgZSxuLG89dGhpcyxpPW8uJGxpc3QsYT1vLiRncmlkO28uaW5zdGFuY2UuY3VycmVudCYmKGU9aS5jaGlsZHJlbigpLnJlbW92ZUNsYXNzKFwiZmFuY3lib3gtdGh1bWJzLWFjdGl2ZVwiKS5maWx0ZXIoJ1tkYXRhLWluZGV4PVwiJytvLmluc3RhbmNlLmN1cnJlbnQuaW5kZXgrJ1wiXScpLmFkZENsYXNzKFwiZmFuY3lib3gtdGh1bWJzLWFjdGl2ZVwiKSxuPWUucG9zaXRpb24oKSxcInlcIj09PW8ub3B0cy5heGlzJiYobi50b3A8MHx8bi50b3A+aS5oZWlnaHQoKS1lLm91dGVySGVpZ2h0KCkpP2kuc3RvcCgpLmFuaW1hdGUoe3Njcm9sbFRvcDppLnNjcm9sbFRvcCgpK24udG9wfSx0KTpcInhcIj09PW8ub3B0cy5heGlzJiYobi5sZWZ0PGEuc2Nyb2xsTGVmdCgpfHxuLmxlZnQ+YS5zY3JvbGxMZWZ0KCkrKGEud2lkdGgoKS1lLm91dGVyV2lkdGgoKSkpJiZpLnBhcmVudCgpLnN0b3AoKS5hbmltYXRlKHtzY3JvbGxMZWZ0Om4ubGVmdH0sdCkpfSx1cGRhdGU6ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3QuaW5zdGFuY2UuJHJlZnMuY29udGFpbmVyLnRvZ2dsZUNsYXNzKFwiZmFuY3lib3gtc2hvdy10aHVtYnNcIix0aGlzLmlzVmlzaWJsZSksdC5pc1Zpc2libGU/KHQuJGdyaWR8fHQuY3JlYXRlKCksdC5pbnN0YW5jZS50cmlnZ2VyKFwib25UaHVtYnNTaG93XCIpLHQuZm9jdXMoMCkpOnQuJGdyaWQmJnQuaW5zdGFuY2UudHJpZ2dlcihcIm9uVGh1bWJzSGlkZVwiKSx0Lmluc3RhbmNlLnVwZGF0ZSgpfSxoaWRlOmZ1bmN0aW9uKCl7dGhpcy5pc1Zpc2libGU9ITEsdGhpcy51cGRhdGUoKX0sc2hvdzpmdW5jdGlvbigpe3RoaXMuaXNWaXNpYmxlPSEwLHRoaXMudXBkYXRlKCl9LHRvZ2dsZTpmdW5jdGlvbigpe3RoaXMuaXNWaXNpYmxlPSF0aGlzLmlzVmlzaWJsZSx0aGlzLnVwZGF0ZSgpfX0pLGUodCkub24oe1wib25Jbml0LmZiXCI6ZnVuY3Rpb24odCxlKXt2YXIgbjtlJiYhZS5UaHVtYnMmJihuPW5ldyBvKGUpLG4uaXNBY3RpdmUmJiEwPT09bi5vcHRzLmF1dG9TdGFydCYmbi5zaG93KCkpfSxcImJlZm9yZVNob3cuZmJcIjpmdW5jdGlvbih0LGUsbixvKXt2YXIgaT1lJiZlLlRodW1icztpJiZpLmlzVmlzaWJsZSYmaS5mb2N1cyhvPzA6MjUwKX0sXCJhZnRlcktleWRvd24uZmJcIjpmdW5jdGlvbih0LGUsbixvLGkpe3ZhciBhPWUmJmUuVGh1bWJzO2EmJmEuaXNBY3RpdmUmJjcxPT09aSYmKG8ucHJldmVudERlZmF1bHQoKSxhLnRvZ2dsZSgpKX0sXCJiZWZvcmVDbG9zZS5mYlwiOmZ1bmN0aW9uKHQsZSl7dmFyIG49ZSYmZS5UaHVtYnM7biYmbi5pc1Zpc2libGUmJiExIT09bi5vcHRzLmhpZGVPbkNsb3NlJiZuLiRncmlkLmhpZGUoKX19KX0oZG9jdW1lbnQsalF1ZXJ5KSxmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIG4odCl7dmFyIGU9e1wiJlwiOlwiJmFtcDtcIixcIjxcIjpcIiZsdDtcIixcIj5cIjpcIiZndDtcIiwnXCInOlwiJnF1b3Q7XCIsXCInXCI6XCImIzM5O1wiLFwiL1wiOlwiJiN4MkY7XCIsXCJgXCI6XCImI3g2MDtcIixcIj1cIjpcIiYjeDNEO1wifTtyZXR1cm4gU3RyaW5nKHQpLnJlcGxhY2UoL1smPD5cIidgPVxcL10vZyxmdW5jdGlvbih0KXtyZXR1cm4gZVt0XX0pfWUuZXh0ZW5kKCEwLGUuZmFuY3lib3guZGVmYXVsdHMse2J0blRwbDp7c2hhcmU6JzxidXR0b24gZGF0YS1mYW5jeWJveC1zaGFyZSBjbGFzcz1cImZhbmN5Ym94LWJ1dHRvbiBmYW5jeWJveC1idXR0b24tLXNoYXJlXCIgdGl0bGU9XCJ7e1NIQVJFfX1cIj48c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB2aWV3Qm94PVwiMCAwIDI0IDI0XCI+PHBhdGggZD1cIk0yLjU1IDE5YzEuNC04LjQgOS4xLTkuOCAxMS45LTkuOFY1bDcgNy03IDYuM3YtMy41Yy0yLjggMC0xMC41IDIuMS0xMS45IDQuMnpcIi8+PC9zdmc+PC9idXR0b24+J30sc2hhcmU6e3VybDpmdW5jdGlvbih0LGUpe3JldHVybiF0LmN1cnJlbnRIYXNoJiZcImlubGluZVwiIT09ZS50eXBlJiZcImh0bWxcIiE9PWUudHlwZSYmKGUub3JpZ1NyY3x8ZS5zcmMpfHx3aW5kb3cubG9jYXRpb259LFxyXG50cGw6JzxkaXYgY2xhc3M9XCJmYW5jeWJveC1zaGFyZVwiPjxoMT57e1NIQVJFfX08L2gxPjxwPjxhIGNsYXNzPVwiZmFuY3lib3gtc2hhcmVfX2J1dHRvbiBmYW5jeWJveC1zaGFyZV9fYnV0dG9uLS1mYlwiIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vc2hhcmVyL3NoYXJlci5waHA/dT17e3VybH19XCI+PHN2ZyB2aWV3Qm94PVwiMCAwIDUxMiA1MTJcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+PHBhdGggZD1cIm0yODcgNDU2di0yOTljMC0yMSA2LTM1IDM1LTM1aDM4di02M2MtNy0xLTI5LTMtNTUtMy01NCAwLTkxIDMzLTkxIDk0djMwNm0xNDMtMjU0aC0yMDV2NzJoMTk2XCIgLz48L3N2Zz48c3Bhbj5GYWNlYm9vazwvc3Bhbj48L2E+PGEgY2xhc3M9XCJmYW5jeWJveC1zaGFyZV9fYnV0dG9uIGZhbmN5Ym94LXNoYXJlX19idXR0b24tLXR3XCIgaHJlZj1cImh0dHBzOi8vdHdpdHRlci5jb20vaW50ZW50L3R3ZWV0P3VybD17e3VybH19JnRleHQ9e3tkZXNjcn19XCI+PHN2ZyB2aWV3Qm94PVwiMCAwIDUxMiA1MTJcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+PHBhdGggZD1cIm00NTYgMTMzYy0xNCA3LTMxIDExLTQ3IDEzIDE3LTEwIDMwLTI3IDM3LTQ2LTE1IDEwLTM0IDE2LTUyIDIwLTYxLTYyLTE1Ny03LTE0MSA3NS02OC0zLTEyOS0zNS0xNjktODUtMjIgMzctMTEgODYgMjYgMTA5LTEzIDAtMjYtNC0zNy05IDAgMzkgMjggNzIgNjUgODAtMTIgMy0yNSA0LTM3IDIgMTAgMzMgNDEgNTcgNzcgNTctNDIgMzAtNzcgMzgtMTIyIDM0IDE3MCAxMTEgMzc4LTMyIDM1OS0yMDggMTYtMTEgMzAtMjUgNDEtNDJ6XCIgLz48L3N2Zz48c3Bhbj5Ud2l0dGVyPC9zcGFuPjwvYT48YSBjbGFzcz1cImZhbmN5Ym94LXNoYXJlX19idXR0b24gZmFuY3lib3gtc2hhcmVfX2J1dHRvbi0tcHRcIiBocmVmPVwiaHR0cHM6Ly93d3cucGludGVyZXN0LmNvbS9waW4vY3JlYXRlL2J1dHRvbi8/dXJsPXt7dXJsfX0mZGVzY3JpcHRpb249e3tkZXNjcn19Jm1lZGlhPXt7bWVkaWF9fVwiPjxzdmcgdmlld0JveD1cIjAgMCA1MTIgNTEyXCIgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiPjxwYXRoIGQ9XCJtMjY1IDU2Yy0xMDkgMC0xNjQgNzgtMTY0IDE0NCAwIDM5IDE1IDc0IDQ3IDg3IDUgMiAxMCAwIDEyLTVsNC0xOWMyLTYgMS04LTMtMTMtOS0xMS0xNS0yNS0xNS00NSAwLTU4IDQzLTExMCAxMTMtMTEwIDYyIDAgOTYgMzggOTYgODggMCA2Ny0zMCAxMjItNzMgMTIyLTI0IDAtNDItMTktMzYtNDQgNi0yOSAyMC02MCAyMC04MSAwLTE5LTEwLTM1LTMxLTM1LTI1IDAtNDQgMjYtNDQgNjAgMCAyMSA3IDM2IDcgMzZsLTMwIDEyNWMtOCAzNy0xIDgzIDAgODcgMCAzIDQgNCA1IDIgMi0zIDMyLTM5IDQyLTc1bDE2LTY0YzggMTYgMzEgMjkgNTYgMjkgNzQgMCAxMjQtNjcgMTI0LTE1NyAwLTY5LTU4LTEzMi0xNDYtMTMyelwiIGZpbGw9XCIjZmZmXCIvPjwvc3ZnPjxzcGFuPlBpbnRlcmVzdDwvc3Bhbj48L2E+PC9wPjxwPjxpbnB1dCBjbGFzcz1cImZhbmN5Ym94LXNoYXJlX19pbnB1dFwiIHR5cGU9XCJ0ZXh0XCIgdmFsdWU9XCJ7e3VybF9yYXd9fVwiIG9uY2xpY2s9XCJzZWxlY3QoKVwiIC8+PC9wPjwvZGl2Pid9fSksZSh0KS5vbihcImNsaWNrXCIsXCJbZGF0YS1mYW5jeWJveC1zaGFyZV1cIixmdW5jdGlvbigpe3ZhciB0LG8saT1lLmZhbmN5Ym94LmdldEluc3RhbmNlKCksYT1pLmN1cnJlbnR8fG51bGw7YSYmKFwiZnVuY3Rpb25cIj09PWUudHlwZShhLm9wdHMuc2hhcmUudXJsKSYmKHQ9YS5vcHRzLnNoYXJlLnVybC5hcHBseShhLFtpLGFdKSksbz1hLm9wdHMuc2hhcmUudHBsLnJlcGxhY2UoL1xce1xce21lZGlhXFx9XFx9L2csXCJpbWFnZVwiPT09YS50eXBlP2VuY29kZVVSSUNvbXBvbmVudChhLnNyYyk6XCJcIikucmVwbGFjZSgvXFx7XFx7dXJsXFx9XFx9L2csZW5jb2RlVVJJQ29tcG9uZW50KHQpKS5yZXBsYWNlKC9cXHtcXHt1cmxfcmF3XFx9XFx9L2csbih0KSkucmVwbGFjZSgvXFx7XFx7ZGVzY3JcXH1cXH0vZyxpLiRjYXB0aW9uP2VuY29kZVVSSUNvbXBvbmVudChpLiRjYXB0aW9uLnRleHQoKSk6XCJcIiksZS5mYW5jeWJveC5vcGVuKHtzcmM6aS50cmFuc2xhdGUoaSxvKSx0eXBlOlwiaHRtbFwiLG9wdHM6e3RvdWNoOiExLGFuaW1hdGlvbkVmZmVjdDohMSxhZnRlckxvYWQ6ZnVuY3Rpb24odCxlKXtpLiRyZWZzLmNvbnRhaW5lci5vbmUoXCJiZWZvcmVDbG9zZS5mYlwiLGZ1bmN0aW9uKCl7dC5jbG9zZShudWxsLDApfSksZS4kY29udGVudC5maW5kKFwiLmZhbmN5Ym94LXNoYXJlX19idXR0b25cIikuY2xpY2soZnVuY3Rpb24oKXtyZXR1cm4gd2luZG93Lm9wZW4odGhpcy5ocmVmLFwiU2hhcmVcIixcIndpZHRoPTU1MCwgaGVpZ2h0PTQ1MFwiKSwhMX0pfSxtb2JpbGU6e2F1dG9Gb2N1czohMX19fSkpfSl9KGRvY3VtZW50LGpRdWVyeSksZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIG8oKXt2YXIgZT10LmxvY2F0aW9uLmhhc2guc3Vic3RyKDEpLG49ZS5zcGxpdChcIi1cIiksbz1uLmxlbmd0aD4xJiYvXlxcKz9cXGQrJC8udGVzdChuW24ubGVuZ3RoLTFdKT9wYXJzZUludChuLnBvcCgtMSksMTApfHwxOjEsaT1uLmpvaW4oXCItXCIpO3JldHVybntoYXNoOmUsaW5kZXg6bzwxPzE6byxnYWxsZXJ5Oml9fWZ1bmN0aW9uIGkodCl7XCJcIiE9PXQuZ2FsbGVyeSYmbihcIltkYXRhLWZhbmN5Ym94PSdcIituLmVzY2FwZVNlbGVjdG9yKHQuZ2FsbGVyeSkrXCInXVwiKS5lcSh0LmluZGV4LTEpLmZvY3VzKCkudHJpZ2dlcihcImNsaWNrLmZiLXN0YXJ0XCIpfWZ1bmN0aW9uIGEodCl7dmFyIGUsbjtyZXR1cm4hIXQmJihlPXQuY3VycmVudD90LmN1cnJlbnQub3B0czp0Lm9wdHMsXCJcIiE9PShuPWUuaGFzaHx8KGUuJG9yaWc/ZS4kb3JpZy5kYXRhKFwiZmFuY3lib3hcIil8fGUuJG9yaWcuZGF0YShcImZhbmN5Ym94LXRyaWdnZXJcIik6XCJcIikpJiZuKX1uLmVzY2FwZVNlbGVjdG9yfHwobi5lc2NhcGVTZWxlY3Rvcj1mdW5jdGlvbih0KXtyZXR1cm4odCtcIlwiKS5yZXBsYWNlKC8oW1xcMC1cXHgxZlxceDdmXXxeLT9cXGQpfF4tJHxbXlxceDgwLVxcdUZGRkZcXHctXS9nLGZ1bmN0aW9uKHQsZSl7cmV0dXJuIGU/XCJcXDBcIj09PXQ/XCLvv71cIjp0LnNsaWNlKDAsLTEpK1wiXFxcXFwiK3QuY2hhckNvZGVBdCh0Lmxlbmd0aC0xKS50b1N0cmluZygxNikrXCIgXCI6XCJcXFxcXCIrdH0pfSksbihmdW5jdGlvbigpeyExIT09bi5mYW5jeWJveC5kZWZhdWx0cy5oYXNoJiYobihlKS5vbih7XCJvbkluaXQuZmJcIjpmdW5jdGlvbih0LGUpe3ZhciBuLGk7ITEhPT1lLmdyb3VwW2UuY3VyckluZGV4XS5vcHRzLmhhc2gmJihuPW8oKSwoaT1hKGUpKSYmbi5nYWxsZXJ5JiZpPT1uLmdhbGxlcnkmJihlLmN1cnJJbmRleD1uLmluZGV4LTEpKX0sXCJiZWZvcmVTaG93LmZiXCI6ZnVuY3Rpb24obixvLGkscyl7dmFyIHI7aSYmITEhPT1pLm9wdHMuaGFzaCYmKHI9YShvKSkmJihvLmN1cnJlbnRIYXNoPXIrKG8uZ3JvdXAubGVuZ3RoPjE/XCItXCIrKGkuaW5kZXgrMSk6XCJcIiksdC5sb2NhdGlvbi5oYXNoIT09XCIjXCIrby5jdXJyZW50SGFzaCYmKHMmJiFvLm9yaWdIYXNoJiYoby5vcmlnSGFzaD10LmxvY2F0aW9uLmhhc2gpLG8uaGFzaFRpbWVyJiZjbGVhclRpbWVvdXQoby5oYXNoVGltZXIpLG8uaGFzaFRpbWVyPXNldFRpbWVvdXQoZnVuY3Rpb24oKXtcInJlcGxhY2VTdGF0ZVwiaW4gdC5oaXN0b3J5Pyh0Lmhpc3Rvcnlbcz9cInB1c2hTdGF0ZVwiOlwicmVwbGFjZVN0YXRlXCJdKHt9LGUudGl0bGUsdC5sb2NhdGlvbi5wYXRobmFtZSt0LmxvY2F0aW9uLnNlYXJjaCtcIiNcIitvLmN1cnJlbnRIYXNoKSxzJiYoby5oYXNDcmVhdGVkSGlzdG9yeT0hMCkpOnQubG9jYXRpb24uaGFzaD1vLmN1cnJlbnRIYXNoLG8uaGFzaFRpbWVyPW51bGx9LDMwMCkpKX0sXCJiZWZvcmVDbG9zZS5mYlwiOmZ1bmN0aW9uKG4sbyxpKXtpJiYhMSE9PWkub3B0cy5oYXNoJiYoY2xlYXJUaW1lb3V0KG8uaGFzaFRpbWVyKSxvLmN1cnJlbnRIYXNoJiZvLmhhc0NyZWF0ZWRIaXN0b3J5P3QuaGlzdG9yeS5iYWNrKCk6by5jdXJyZW50SGFzaCYmKFwicmVwbGFjZVN0YXRlXCJpbiB0Lmhpc3Rvcnk/dC5oaXN0b3J5LnJlcGxhY2VTdGF0ZSh7fSxlLnRpdGxlLHQubG9jYXRpb24ucGF0aG5hbWUrdC5sb2NhdGlvbi5zZWFyY2grKG8ub3JpZ0hhc2h8fFwiXCIpKTp0LmxvY2F0aW9uLmhhc2g9by5vcmlnSGFzaCksby5jdXJyZW50SGFzaD1udWxsKX19KSxuKHQpLm9uKFwiaGFzaGNoYW5nZS5mYlwiLGZ1bmN0aW9uKCl7dmFyIHQ9bygpLGU9bnVsbDtuLmVhY2gobihcIi5mYW5jeWJveC1jb250YWluZXJcIikuZ2V0KCkucmV2ZXJzZSgpLGZ1bmN0aW9uKHQsbyl7dmFyIGk9bihvKS5kYXRhKFwiRmFuY3lCb3hcIik7aWYoaSYmaS5jdXJyZW50SGFzaClyZXR1cm4gZT1pLCExfSksZT9lLmN1cnJlbnRIYXNoPT09dC5nYWxsZXJ5K1wiLVwiK3QuaW5kZXh8fDE9PT10LmluZGV4JiZlLmN1cnJlbnRIYXNoPT10LmdhbGxlcnl8fChlLmN1cnJlbnRIYXNoPW51bGwsZS5jbG9zZSgpKTpcIlwiIT09dC5nYWxsZXJ5JiZpKHQpfSksc2V0VGltZW91dChmdW5jdGlvbigpe24uZmFuY3lib3guZ2V0SW5zdGFuY2UoKXx8aShvKCkpfSw1MCkpfSl9KHdpbmRvdyxkb2N1bWVudCxqUXVlcnkpLGZ1bmN0aW9uKHQsZSl7XCJ1c2Ugc3RyaWN0XCI7dmFyIG49KG5ldyBEYXRlKS5nZXRUaW1lKCk7ZSh0KS5vbih7XCJvbkluaXQuZmJcIjpmdW5jdGlvbih0LGUsbyl7ZS4kcmVmcy5zdGFnZS5vbihcIm1vdXNld2hlZWwgRE9NTW91c2VTY3JvbGwgd2hlZWwgTW96TW91c2VQaXhlbFNjcm9sbFwiLGZ1bmN0aW9uKHQpe3ZhciBvPWUuY3VycmVudCxpPShuZXcgRGF0ZSkuZ2V0VGltZSgpO2UuZ3JvdXAubGVuZ3RoPDJ8fCExPT09by5vcHRzLndoZWVsfHxcImF1dG9cIj09PW8ub3B0cy53aGVlbCYmXCJpbWFnZVwiIT09by50eXBlfHwodC5wcmV2ZW50RGVmYXVsdCgpLHQuc3RvcFByb3BhZ2F0aW9uKCksby4kc2xpZGUuaGFzQ2xhc3MoXCJmYW5jeWJveC1hbmltYXRlZFwiKXx8KHQ9dC5vcmlnaW5hbEV2ZW50fHx0LGktbjwyNTB8fChuPWksZVsoLXQuZGVsdGFZfHwtdC5kZWx0YVh8fHQud2hlZWxEZWx0YXx8LXQuZGV0YWlsKTwwP1wibmV4dFwiOlwicHJldmlvdXNcIl0oKSkpKX0pfX0pfShkb2N1bWVudCxqUXVlcnkpOyIsIihmdW5jdGlvbihmYWN0b3J5KSB7XHJcbiAgLyogZ2xvYmFsIGRlZmluZSAqL1xyXG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbiAgaWYgKCB0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQgKSB7XHJcbiAgICBkZWZpbmUoWydqcXVlcnknXSwgZmFjdG9yeSk7XHJcbiAgfSBlbHNlIGlmICggdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcgJiYgbW9kdWxlLmV4cG9ydHMgKSB7XHJcbiAgICAvLyBOb2RlL0NvbW1vbkpTXHJcbiAgICBtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKCByb290LCBqUXVlcnkgKSB7XHJcbiAgICAgIGlmICggalF1ZXJ5ID09PSB1bmRlZmluZWQgKSB7XHJcbiAgICAgICAgaWYgKCB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyApIHtcclxuICAgICAgICAgIGpRdWVyeSA9IHJlcXVpcmUoJ2pxdWVyeScpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBqUXVlcnkgPSByZXF1aXJlKCdqcXVlcnknKShyb290KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgZmFjdG9yeShqUXVlcnkpO1xyXG4gICAgICByZXR1cm4galF1ZXJ5O1xyXG4gICAgfTtcclxuICB9IGVsc2Uge1xyXG4gICAgLy8gQnJvd3NlciBnbG9iYWxzXHJcbiAgICBmYWN0b3J5KGpRdWVyeSk7XHJcbiAgfVxyXG59KGZ1bmN0aW9uKCQpIHtcclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIHZhciAkZG9jID0gJChkb2N1bWVudCk7XHJcbiAgdmFyICR3aW4gPSAkKHdpbmRvdyk7XHJcblxyXG4gIHZhciBwbHVnaW5OYW1lID0gJ3NlbGVjdHJpYyc7XHJcbiAgdmFyIGNsYXNzTGlzdCA9ICdJbnB1dCBJdGVtcyBPcGVuIERpc2FibGVkIFRlbXBTaG93IEhpZGVTZWxlY3QgV3JhcHBlciBGb2N1cyBIb3ZlciBSZXNwb25zaXZlIEFib3ZlIEJlbG93IFNjcm9sbCBHcm91cCBHcm91cExhYmVsJztcclxuICB2YXIgZXZlbnROYW1lc3BhY2VTdWZmaXggPSAnLnNsJztcclxuXHJcbiAgdmFyIGNoYXJzID0gWydhJywgJ2UnLCAnaScsICdvJywgJ3UnLCAnbicsICdjJywgJ3knXTtcclxuICB2YXIgZGlhY3JpdGljcyA9IFtcclxuICAgIC9bXFx4RTAtXFx4RTVdL2csIC8vIGFcclxuICAgIC9bXFx4RTgtXFx4RUJdL2csIC8vIGVcclxuICAgIC9bXFx4RUMtXFx4RUZdL2csIC8vIGlcclxuICAgIC9bXFx4RjItXFx4RjZdL2csIC8vIG9cclxuICAgIC9bXFx4RjktXFx4RkNdL2csIC8vIHVcclxuICAgIC9bXFx4RjFdL2csICAgICAgLy8gblxyXG4gICAgL1tcXHhFN10vZywgICAgICAvLyBjXHJcbiAgICAvW1xceEZELVxceEZGXS9nICAvLyB5XHJcbiAgXTtcclxuXHJcbiAgLyoqXHJcbiAgICogQ3JlYXRlIGFuIGluc3RhbmNlIG9mIFNlbGVjdHJpY1xyXG4gICAqXHJcbiAgICogQGNvbnN0cnVjdG9yXHJcbiAgICogQHBhcmFtIHtOb2RlfSBlbGVtZW50IC0gVGhlICZsdDtzZWxlY3QmZ3Q7IGVsZW1lbnRcclxuICAgKiBAcGFyYW0ge29iamVjdH0gIG9wdHMgLSBPcHRpb25zXHJcbiAgICovXHJcbiAgdmFyIFNlbGVjdHJpYyA9IGZ1bmN0aW9uKGVsZW1lbnQsIG9wdHMpIHtcclxuICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgX3RoaXMuZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICBfdGhpcy4kZWxlbWVudCA9ICQoZWxlbWVudCk7XHJcblxyXG4gICAgX3RoaXMuc3RhdGUgPSB7XHJcbiAgICAgIG11bHRpcGxlICAgICAgIDogISFfdGhpcy4kZWxlbWVudC5hdHRyKCdtdWx0aXBsZScpLFxyXG4gICAgICBlbmFibGVkICAgICAgICA6IGZhbHNlLFxyXG4gICAgICBvcGVuZWQgICAgICAgICA6IGZhbHNlLFxyXG4gICAgICBjdXJyVmFsdWUgICAgICA6IC0xLFxyXG4gICAgICBzZWxlY3RlZElkeCAgICA6IC0xLFxyXG4gICAgICBoaWdobGlnaHRlZElkeCA6IC0xXHJcbiAgICB9O1xyXG5cclxuICAgIF90aGlzLmV2ZW50VHJpZ2dlcnMgPSB7XHJcbiAgICAgIG9wZW4gICAgOiBfdGhpcy5vcGVuLFxyXG4gICAgICBjbG9zZSAgIDogX3RoaXMuY2xvc2UsXHJcbiAgICAgIGRlc3Ryb3kgOiBfdGhpcy5kZXN0cm95LFxyXG4gICAgICByZWZyZXNoIDogX3RoaXMucmVmcmVzaCxcclxuICAgICAgaW5pdCAgICA6IF90aGlzLmluaXRcclxuICAgIH07XHJcblxyXG4gICAgX3RoaXMuaW5pdChvcHRzKTtcclxuICB9O1xyXG5cclxuICBTZWxlY3RyaWMucHJvdG90eXBlID0ge1xyXG4gICAgdXRpbHM6IHtcclxuICAgICAgLyoqXHJcbiAgICAgICAqIERldGVjdCBtb2JpbGUgYnJvd3NlclxyXG4gICAgICAgKlxyXG4gICAgICAgKiBAcmV0dXJuIHtib29sZWFufVxyXG4gICAgICAgKi9cclxuICAgICAgaXNNb2JpbGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiAvYW5kcm9pZHxpcChob25lfG9kfGFkKS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7XHJcbiAgICAgIH0sXHJcblxyXG4gICAgICAvKipcclxuICAgICAgICogRXNjYXBlIGVzcGVjaWFsIGNoYXJhY3RlcnMgaW4gc3RyaW5nIChodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9KYXZhU2NyaXB0L0d1aWRlL1JlZ3VsYXJfRXhwcmVzc2lvbnMpXHJcbiAgICAgICAqXHJcbiAgICAgICAqIEBwYXJhbSAge3N0cmluZ30gc3RyIC0gVGhlIHN0cmluZyB0byBiZSBlc2NhcGVkXHJcbiAgICAgICAqIEByZXR1cm4ge3N0cmluZ30gICAgICAgVGhlIHN0cmluZyB3aXRoIHRoZSBzcGVjaWFsIGNoYXJhY3RlcnMgZXNjYXBlZFxyXG4gICAgICAgKi9cclxuICAgICAgZXNjYXBlUmVnRXhwOiBmdW5jdGlvbihzdHIpIHtcclxuICAgICAgICByZXR1cm4gc3RyLnJlcGxhY2UoL1suKis/XiR7fSgpfFtcXF1cXFxcXS9nLCAnXFxcXCQmJyk7IC8vICQmIG1lYW5zIHRoZSB3aG9sZSBtYXRjaGVkIHN0cmluZ1xyXG4gICAgICB9LFxyXG5cclxuICAgICAgLyoqXHJcbiAgICAgICAqIFJlcGxhY2UgZGlhY3JpdGljc1xyXG4gICAgICAgKlxyXG4gICAgICAgKiBAcGFyYW0gIHtzdHJpbmd9IHN0ciAtIFRoZSBzdHJpbmcgdG8gcmVwbGFjZSB0aGUgZGlhY3JpdGljc1xyXG4gICAgICAgKiBAcmV0dXJuIHtzdHJpbmd9ICAgICAgIFRoZSBzdHJpbmcgd2l0aCBkaWFjcml0aWNzIHJlcGxhY2VkIHdpdGggYXNjaWkgY2hhcmFjdGVyc1xyXG4gICAgICAgKi9cclxuICAgICAgcmVwbGFjZURpYWNyaXRpY3M6IGZ1bmN0aW9uKHN0cikge1xyXG4gICAgICAgIHZhciBrID0gZGlhY3JpdGljcy5sZW5ndGg7XHJcblxyXG4gICAgICAgIHdoaWxlIChrLS0pIHtcclxuICAgICAgICAgIHN0ciA9IHN0ci50b0xvd2VyQ2FzZSgpLnJlcGxhY2UoZGlhY3JpdGljc1trXSwgY2hhcnNba10pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHN0cjtcclxuICAgICAgfSxcclxuXHJcbiAgICAgIC8qKlxyXG4gICAgICAgKiBGb3JtYXQgc3RyaW5nXHJcbiAgICAgICAqIGh0dHBzOi8vZ2lzdC5naXRodWIuY29tL2F0ZXNnb3JhbC85ODQzNzVcclxuICAgICAgICpcclxuICAgICAgICogQHBhcmFtICB7c3RyaW5nfSBmIC0gU3RyaW5nIHRvIGJlIGZvcm1hdGVkXHJcbiAgICAgICAqIEByZXR1cm4ge3N0cmluZ30gICAgIFN0cmluZyBmb3JtYXRlZFxyXG4gICAgICAgKi9cclxuICAgICAgZm9ybWF0OiBmdW5jdGlvbihmKSB7XHJcbiAgICAgICAgdmFyIGEgPSBhcmd1bWVudHM7IC8vIHN0b3JlIG91dGVyIGFyZ3VtZW50c1xyXG4gICAgICAgIHJldHVybiAoJycgKyBmKSAvLyBmb3JjZSBmb3JtYXQgc3BlY2lmaWVyIHRvIFN0cmluZ1xyXG4gICAgICAgICAgLnJlcGxhY2UoIC8vIHJlcGxhY2UgdG9rZW5zIGluIGZvcm1hdCBzcGVjaWZpZXJcclxuICAgICAgICAgICAgL1xceyg/OihcXGQrKXwoXFx3KykpXFx9L2csIC8vIG1hdGNoIHt0b2tlbn0gcmVmZXJlbmNlc1xyXG4gICAgICAgICAgICBmdW5jdGlvbihcclxuICAgICAgICAgICAgICBzLCAvLyB0aGUgbWF0Y2hlZCBzdHJpbmcgKGlnbm9yZWQpXHJcbiAgICAgICAgICAgICAgaSwgLy8gYW4gYXJndW1lbnQgaW5kZXhcclxuICAgICAgICAgICAgICBwIC8vIGEgcHJvcGVydHkgbmFtZVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICByZXR1cm4gcCAmJiBhWzFdIC8vIGlmIHByb3BlcnR5IG5hbWUgYW5kIGZpcnN0IGFyZ3VtZW50IGV4aXN0XHJcbiAgICAgICAgICAgICAgICA/IGFbMV1bcF0gLy8gcmV0dXJuIHByb3BlcnR5IGZyb20gZmlyc3QgYXJndW1lbnRcclxuICAgICAgICAgICAgICAgIDogYVtpXTsgLy8gYXNzdW1lIGFyZ3VtZW50IGluZGV4IGFuZCByZXR1cm4gaS10aCBhcmd1bWVudFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgfSxcclxuXHJcbiAgICAgIC8qKlxyXG4gICAgICAgKiBHZXQgdGhlIG5leHQgZW5hYmxlZCBpdGVtIGluIHRoZSBvcHRpb25zIGxpc3QuXHJcbiAgICAgICAqXHJcbiAgICAgICAqIEBwYXJhbSAge29iamVjdH0gc2VsZWN0SXRlbXMgLSBUaGUgb3B0aW9ucyBvYmplY3QuXHJcbiAgICAgICAqIEBwYXJhbSAge251bWJlcn0gICAgc2VsZWN0ZWQgLSBJbmRleCBvZiB0aGUgY3VycmVudGx5IHNlbGVjdGVkIG9wdGlvbi5cclxuICAgICAgICogQHJldHVybiB7b2JqZWN0fSAgICAgICAgICAgICAgIFRoZSBuZXh0IGVuYWJsZWQgaXRlbS5cclxuICAgICAgICovXHJcbiAgICAgIG5leHRFbmFibGVkSXRlbTogZnVuY3Rpb24oc2VsZWN0SXRlbXMsIHNlbGVjdGVkKSB7XHJcbiAgICAgICAgd2hpbGUgKCBzZWxlY3RJdGVtc1sgc2VsZWN0ZWQgPSAoc2VsZWN0ZWQgKyAxKSAlIHNlbGVjdEl0ZW1zLmxlbmd0aCBdLmRpc2FibGVkICkge1xyXG4gICAgICAgICAgLy8gZW1wdHlcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNlbGVjdGVkO1xyXG4gICAgICB9LFxyXG5cclxuICAgICAgLyoqXHJcbiAgICAgICAqIEdldCB0aGUgcHJldmlvdXMgZW5hYmxlZCBpdGVtIGluIHRoZSBvcHRpb25zIGxpc3QuXHJcbiAgICAgICAqXHJcbiAgICAgICAqIEBwYXJhbSAge29iamVjdH0gc2VsZWN0SXRlbXMgLSBUaGUgb3B0aW9ucyBvYmplY3QuXHJcbiAgICAgICAqIEBwYXJhbSAge251bWJlcn0gICAgc2VsZWN0ZWQgLSBJbmRleCBvZiB0aGUgY3VycmVudGx5IHNlbGVjdGVkIG9wdGlvbi5cclxuICAgICAgICogQHJldHVybiB7b2JqZWN0fSAgICAgICAgICAgICAgIFRoZSBwcmV2aW91cyBlbmFibGVkIGl0ZW0uXHJcbiAgICAgICAqL1xyXG4gICAgICBwcmV2aW91c0VuYWJsZWRJdGVtOiBmdW5jdGlvbihzZWxlY3RJdGVtcywgc2VsZWN0ZWQpIHtcclxuICAgICAgICB3aGlsZSAoIHNlbGVjdEl0ZW1zWyBzZWxlY3RlZCA9IChzZWxlY3RlZCA+IDAgPyBzZWxlY3RlZCA6IHNlbGVjdEl0ZW1zLmxlbmd0aCkgLSAxIF0uZGlzYWJsZWQgKSB7XHJcbiAgICAgICAgICAvLyBlbXB0eVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2VsZWN0ZWQ7XHJcbiAgICAgIH0sXHJcblxyXG4gICAgICAvKipcclxuICAgICAgICogVHJhbnNmb3JtIGNhbWVsQ2FzZSBzdHJpbmcgdG8gZGFzaC1jYXNlLlxyXG4gICAgICAgKlxyXG4gICAgICAgKiBAcGFyYW0gIHtzdHJpbmd9IHN0ciAtIFRoZSBjYW1lbENhc2VkIHN0cmluZy5cclxuICAgICAgICogQHJldHVybiB7c3RyaW5nfSAgICAgICBUaGUgc3RyaW5nIHRyYW5zZm9ybWVkIHRvIGRhc2gtY2FzZS5cclxuICAgICAgICovXHJcbiAgICAgIHRvRGFzaDogZnVuY3Rpb24oc3RyKSB7XHJcbiAgICAgICAgcmV0dXJuIHN0ci5yZXBsYWNlKC8oW2EtejAtOV0pKFtBLVpdKS9nLCAnJDEtJDInKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICB9LFxyXG5cclxuICAgICAgLyoqXHJcbiAgICAgICAqIENhbGxzIHRoZSBldmVudHMgcmVnaXN0ZXJlZCB3aXRoIGZ1bmN0aW9uIG5hbWUuXHJcbiAgICAgICAqXHJcbiAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSAgICBmbiAtIFRoZSBuYW1lIG9mIHRoZSBmdW5jdGlvbi5cclxuICAgICAgICogQHBhcmFtIHtudW1iZXJ9IHNjb3BlIC0gU2NvcGUgdGhhdCBzaG91bGQgYmUgc2V0IG9uIHRoZSBmdW5jdGlvbi5cclxuICAgICAgICovXHJcbiAgICAgIHRyaWdnZXJDYWxsYmFjazogZnVuY3Rpb24oZm4sIHNjb3BlKSB7XHJcbiAgICAgICAgdmFyIGVsbSA9IHNjb3BlLmVsZW1lbnQ7XHJcbiAgICAgICAgdmFyIGZ1bmMgPSBzY29wZS5vcHRpb25zWydvbicgKyBmbl07XHJcbiAgICAgICAgdmFyIGFyZ3MgPSBbZWxtXS5jb25jYXQoW10uc2xpY2UuY2FsbChhcmd1bWVudHMpLnNsaWNlKDEpKTtcclxuXHJcbiAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oZnVuYykgKSB7XHJcbiAgICAgICAgICBmdW5jLmFwcGx5KGVsbSwgYXJncyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAkKGVsbSkudHJpZ2dlcihwbHVnaW5OYW1lICsgJy0nICsgdGhpcy50b0Rhc2goZm4pLCBhcmdzKTtcclxuICAgICAgfSxcclxuXHJcbiAgICAgIC8qKlxyXG4gICAgICAgKiBUcmFuc2Zvcm0gYXJyYXkgbGlzdCB0byBjb25jYXRlbmF0ZWQgc3RyaW5nIGFuZCByZW1vdmUgZW1wdHkgdmFsdWVzXHJcbiAgICAgICAqIEBwYXJhbSAge2FycmF5fSBhcnIgLSBDbGFzcyBsaXN0XHJcbiAgICAgICAqIEByZXR1cm4ge3N0cmluZ30gICAgICBDb25jYXRlbmF0ZWQgc3RyaW5nXHJcbiAgICAgICAqL1xyXG4gICAgICBhcnJheVRvQ2xhc3NuYW1lOiBmdW5jdGlvbihhcnIpIHtcclxuICAgICAgICB2YXIgbmV3QXJyID0gJC5ncmVwKGFyciwgZnVuY3Rpb24oaXRlbSkge1xyXG4gICAgICAgICAgcmV0dXJuICEhaXRlbTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuICQudHJpbShuZXdBcnIuam9pbignICcpKTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvKiogSW5pdGlhbGl6ZXMgKi9cclxuICAgIGluaXQ6IGZ1bmN0aW9uKG9wdHMpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgIC8vIFNldCBvcHRpb25zXHJcbiAgICAgIF90aGlzLm9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbltwbHVnaW5OYW1lXS5kZWZhdWx0cywgX3RoaXMub3B0aW9ucywgb3B0cyk7XHJcblxyXG4gICAgICBfdGhpcy51dGlscy50cmlnZ2VyQ2FsbGJhY2soJ0JlZm9yZUluaXQnLCBfdGhpcyk7XHJcblxyXG4gICAgICAvLyBQcmVzZXJ2ZSBkYXRhXHJcbiAgICAgIF90aGlzLmRlc3Ryb3kodHJ1ZSk7XHJcblxyXG4gICAgICAvLyBEaXNhYmxlIG9uIG1vYmlsZSBicm93c2Vyc1xyXG4gICAgICBpZiAoIF90aGlzLm9wdGlvbnMuZGlzYWJsZU9uTW9iaWxlICYmIF90aGlzLnV0aWxzLmlzTW9iaWxlKCkgKSB7XHJcbiAgICAgICAgX3RoaXMuZGlzYWJsZU9uTW9iaWxlID0gdHJ1ZTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIEdldCBjbGFzc2VzXHJcbiAgICAgIF90aGlzLmNsYXNzZXMgPSBfdGhpcy5nZXRDbGFzc05hbWVzKCk7XHJcblxyXG4gICAgICAvLyBDcmVhdGUgZWxlbWVudHNcclxuICAgICAgdmFyIGlucHV0ICAgICAgICAgICAgICA9ICQoJzxpbnB1dC8+JywgeyAnY2xhc3MnOiBfdGhpcy5jbGFzc2VzLmlucHV0LCAncmVhZG9ubHknOiBfdGhpcy51dGlscy5pc01vYmlsZSgpIH0pO1xyXG4gICAgICB2YXIgaXRlbXMgICAgICAgICAgICAgID0gJCgnPGRpdi8+JywgICB7ICdjbGFzcyc6IF90aGlzLmNsYXNzZXMuaXRlbXMsICd0YWJpbmRleCc6IC0xIH0pO1xyXG4gICAgICB2YXIgaXRlbXNTY3JvbGwgICAgICAgID0gJCgnPGRpdi8+JywgICB7ICdjbGFzcyc6IF90aGlzLmNsYXNzZXMuc2Nyb2xsIH0pO1xyXG4gICAgICB2YXIgd3JhcHBlciAgICAgICAgICAgID0gJCgnPGRpdi8+JywgICB7ICdjbGFzcyc6IF90aGlzLmNsYXNzZXMucHJlZml4LCAnaHRtbCc6IF90aGlzLm9wdGlvbnMuYXJyb3dCdXR0b25NYXJrdXAgfSk7XHJcbiAgICAgIHZhciBsYWJlbCAgICAgICAgICAgICAgPSAkKCc8c3Bhbi8+JywgIHsgJ2NsYXNzJzogJ2xhYmVsJyB9KTtcclxuICAgICAgdmFyIG91dGVyV3JhcHBlciAgICAgICA9IF90aGlzLiRlbGVtZW50LndyYXAoJzxkaXYvPicpLnBhcmVudCgpLmFwcGVuZCh3cmFwcGVyLnByZXBlbmQobGFiZWwpLCBpdGVtcywgaW5wdXQpO1xyXG4gICAgICB2YXIgaGlkZVNlbGVjdFdyYXBwZXIgID0gJCgnPGRpdi8+JywgICB7ICdjbGFzcyc6IF90aGlzLmNsYXNzZXMuaGlkZXNlbGVjdCB9KTtcclxuXHJcbiAgICAgIF90aGlzLmVsZW1lbnRzID0ge1xyXG4gICAgICAgIGlucHV0ICAgICAgICA6IGlucHV0LFxyXG4gICAgICAgIGl0ZW1zICAgICAgICA6IGl0ZW1zLFxyXG4gICAgICAgIGl0ZW1zU2Nyb2xsICA6IGl0ZW1zU2Nyb2xsLFxyXG4gICAgICAgIHdyYXBwZXIgICAgICA6IHdyYXBwZXIsXHJcbiAgICAgICAgbGFiZWwgICAgICAgIDogbGFiZWwsXHJcbiAgICAgICAgb3V0ZXJXcmFwcGVyIDogb3V0ZXJXcmFwcGVyXHJcbiAgICAgIH07XHJcblxyXG4gICAgICBpZiAoIF90aGlzLm9wdGlvbnMubmF0aXZlT25Nb2JpbGUgJiYgX3RoaXMudXRpbHMuaXNNb2JpbGUoKSApIHtcclxuICAgICAgICBfdGhpcy5lbGVtZW50cy5pbnB1dCA9IHVuZGVmaW5lZDtcclxuICAgICAgICBoaWRlU2VsZWN0V3JhcHBlci5hZGRDbGFzcyhfdGhpcy5jbGFzc2VzLnByZWZpeCArICctaXMtbmF0aXZlJyk7XHJcblxyXG4gICAgICAgIF90aGlzLiRlbGVtZW50Lm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIF90aGlzLnJlZnJlc2goKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgX3RoaXMuJGVsZW1lbnRcclxuICAgICAgICAub24oX3RoaXMuZXZlbnRUcmlnZ2VycylcclxuICAgICAgICAud3JhcChoaWRlU2VsZWN0V3JhcHBlcik7XHJcblxyXG4gICAgICBfdGhpcy5vcmlnaW5hbFRhYmluZGV4ID0gX3RoaXMuJGVsZW1lbnQucHJvcCgndGFiaW5kZXgnKTtcclxuICAgICAgX3RoaXMuJGVsZW1lbnQucHJvcCgndGFiaW5kZXgnLCAtMSk7XHJcblxyXG4gICAgICBfdGhpcy5wb3B1bGF0ZSgpO1xyXG4gICAgICBfdGhpcy5hY3RpdmF0ZSgpO1xyXG5cclxuICAgICAgX3RoaXMudXRpbHMudHJpZ2dlckNhbGxiYWNrKCdJbml0JywgX3RoaXMpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKiogQWN0aXZhdGVzIHRoZSBwbHVnaW4gKi9cclxuICAgIGFjdGl2YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgdmFyIGhpZGRlbkNoaWxkcmVuID0gX3RoaXMuZWxlbWVudHMuaXRlbXMuY2xvc2VzdCgnOnZpc2libGUnKS5jaGlsZHJlbignOmhpZGRlbicpLmFkZENsYXNzKF90aGlzLmNsYXNzZXMudGVtcHNob3cpO1xyXG4gICAgICB2YXIgb3JpZ2luYWxXaWR0aCA9IF90aGlzLiRlbGVtZW50LndpZHRoKCk7XHJcblxyXG4gICAgICBoaWRkZW5DaGlsZHJlbi5yZW1vdmVDbGFzcyhfdGhpcy5jbGFzc2VzLnRlbXBzaG93KTtcclxuXHJcbiAgICAgIF90aGlzLnV0aWxzLnRyaWdnZXJDYWxsYmFjaygnQmVmb3JlQWN0aXZhdGUnLCBfdGhpcyk7XHJcblxyXG4gICAgICBfdGhpcy5lbGVtZW50cy5vdXRlcldyYXBwZXIucHJvcCgnY2xhc3MnLFxyXG4gICAgICAgIF90aGlzLnV0aWxzLmFycmF5VG9DbGFzc25hbWUoW1xyXG4gICAgICAgICAgX3RoaXMuY2xhc3Nlcy53cmFwcGVyLFxyXG4gICAgICAgICAgX3RoaXMuJGVsZW1lbnQucHJvcCgnY2xhc3MnKS5yZXBsYWNlKC9cXFMrL2csIF90aGlzLmNsYXNzZXMucHJlZml4ICsgJy0kJicpLFxyXG4gICAgICAgICAgX3RoaXMub3B0aW9ucy5yZXNwb25zaXZlID8gX3RoaXMuY2xhc3Nlcy5yZXNwb25zaXZlIDogJydcclxuICAgICAgICBdKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgaWYgKCBfdGhpcy5vcHRpb25zLmluaGVyaXRPcmlnaW5hbFdpZHRoICYmIG9yaWdpbmFsV2lkdGggPiAwICkge1xyXG4gICAgICAgIF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlci53aWR0aChvcmlnaW5hbFdpZHRoKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgX3RoaXMudW5iaW5kRXZlbnRzKCk7XHJcblxyXG4gICAgICBpZiAoICFfdGhpcy4kZWxlbWVudC5wcm9wKCdkaXNhYmxlZCcpICkge1xyXG4gICAgICAgIF90aGlzLnN0YXRlLmVuYWJsZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICAvLyBOb3QgZGlzYWJsZWQsIHNvLi4uIFJlbW92aW5nIGRpc2FibGVkIGNsYXNzXHJcbiAgICAgICAgX3RoaXMuZWxlbWVudHMub3V0ZXJXcmFwcGVyLnJlbW92ZUNsYXNzKF90aGlzLmNsYXNzZXMuZGlzYWJsZWQpO1xyXG5cclxuICAgICAgICAvLyBSZW1vdmUgc3R5bGVzIGZyb20gaXRlbXMgYm94XHJcbiAgICAgICAgLy8gRml4IGluY29ycmVjdCBoZWlnaHQgd2hlbiByZWZyZXNoZWQgaXMgdHJpZ2dlcmVkIHdpdGggZmV3ZXIgb3B0aW9uc1xyXG4gICAgICAgIF90aGlzLiRsaSA9IF90aGlzLmVsZW1lbnRzLml0ZW1zLnJlbW92ZUF0dHIoJ3N0eWxlJykuZmluZCgnbGknKTtcclxuXHJcbiAgICAgICAgX3RoaXMuYmluZEV2ZW50cygpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlci5hZGRDbGFzcyhfdGhpcy5jbGFzc2VzLmRpc2FibGVkKTtcclxuXHJcbiAgICAgICAgaWYgKCBfdGhpcy5lbGVtZW50cy5pbnB1dCApIHtcclxuICAgICAgICAgIF90aGlzLmVsZW1lbnRzLmlucHV0LnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBfdGhpcy51dGlscy50cmlnZ2VyQ2FsbGJhY2soJ0FjdGl2YXRlJywgX3RoaXMpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdlbmVyYXRlIGNsYXNzTmFtZXMgZm9yIGVsZW1lbnRzXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybiB7b2JqZWN0fSBDbGFzc2VzIG9iamVjdFxyXG4gICAgICovXHJcbiAgICBnZXRDbGFzc05hbWVzOiBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgdmFyIGN1c3RvbUNsYXNzID0gX3RoaXMub3B0aW9ucy5jdXN0b21DbGFzcztcclxuICAgICAgdmFyIGNsYXNzZXNPYmogPSB7fTtcclxuXHJcbiAgICAgICQuZWFjaChjbGFzc0xpc3Quc3BsaXQoJyAnKSwgZnVuY3Rpb24oaSwgY3VyckNsYXNzKSB7XHJcbiAgICAgICAgdmFyIGMgPSBjdXN0b21DbGFzcy5wcmVmaXggKyBjdXJyQ2xhc3M7XHJcbiAgICAgICAgY2xhc3Nlc09ialtjdXJyQ2xhc3MudG9Mb3dlckNhc2UoKV0gPSBjdXN0b21DbGFzcy5jYW1lbENhc2UgPyBjIDogX3RoaXMudXRpbHMudG9EYXNoKGMpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGNsYXNzZXNPYmoucHJlZml4ID0gY3VzdG9tQ2xhc3MucHJlZml4O1xyXG5cclxuICAgICAgcmV0dXJuIGNsYXNzZXNPYmo7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKiBTZXQgdGhlIGxhYmVsIHRleHQgKi9cclxuICAgIHNldExhYmVsOiBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgdmFyIGxhYmVsQnVpbGRlciA9IF90aGlzLm9wdGlvbnMubGFiZWxCdWlsZGVyO1xyXG5cclxuICAgICAgaWYgKCBfdGhpcy5zdGF0ZS5tdWx0aXBsZSApIHtcclxuICAgICAgICAvLyBNYWtlIHN1cmUgY3VycmVudFZhbHVlcyBpcyBhbiBhcnJheVxyXG4gICAgICAgIHZhciBjdXJyZW50VmFsdWVzID0gJC5pc0FycmF5KF90aGlzLnN0YXRlLmN1cnJWYWx1ZSkgPyBfdGhpcy5zdGF0ZS5jdXJyVmFsdWUgOiBbX3RoaXMuc3RhdGUuY3VyclZhbHVlXTtcclxuICAgICAgICAvLyBJJ20gbm90IGhhcHB5IHdpdGggdGhpcywgYnV0IGN1cnJlbnRWYWx1ZXMgY2FuIGJlIGFuIGVtcHR5XHJcbiAgICAgICAgLy8gYXJyYXkgYW5kIHdlIG5lZWQgdG8gZmFsbGJhY2sgdG8gdGhlIGRlZmF1bHQgb3B0aW9uLlxyXG4gICAgICAgIGN1cnJlbnRWYWx1ZXMgPSBjdXJyZW50VmFsdWVzLmxlbmd0aCA9PT0gMCA/IFswXSA6IGN1cnJlbnRWYWx1ZXM7XHJcblxyXG4gICAgICAgIHZhciBsYWJlbE1hcmt1cCA9ICQubWFwKGN1cnJlbnRWYWx1ZXMsIGZ1bmN0aW9uKHZhbHVlKSB7XHJcbiAgICAgICAgICByZXR1cm4gJC5ncmVwKF90aGlzLmxvb2t1cEl0ZW1zLCBmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBpdGVtLmluZGV4ID09PSB2YWx1ZTtcclxuICAgICAgICAgIH0pWzBdOyAvLyB3ZSBkb24ndCB3YW50IG5lc3RlZCBhcnJheXMgaGVyZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsYWJlbE1hcmt1cCA9ICQuZ3JlcChsYWJlbE1hcmt1cCwgZnVuY3Rpb24oaXRlbSkge1xyXG4gICAgICAgICAgLy8gSGlkZSBkZWZhdWx0IChwbGVhc2UgY2hvb3NlKSBpZiBtb3JlIHRoZW4gb25lIGVsZW1lbnQgd2VyZSBzZWxlY3RlZC5cclxuICAgICAgICAgIC8vIElmIG5vIG9wdGlvbiB2YWx1ZSB3ZXJlIGdpdmVuIHZhbHVlIGlzIHNldCB0byBvcHRpb24gdGV4dCBieSBkZWZhdWx0XHJcbiAgICAgICAgICBpZiAoIGxhYmVsTWFya3VwLmxlbmd0aCA+IDEgfHwgbGFiZWxNYXJrdXAubGVuZ3RoID09PSAwICkge1xyXG4gICAgICAgICAgICByZXR1cm4gJC50cmltKGl0ZW0udmFsdWUpICE9PSAnJztcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsYWJlbE1hcmt1cCA9ICQubWFwKGxhYmVsTWFya3VwLCBmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgICAgICByZXR1cm4gJC5pc0Z1bmN0aW9uKGxhYmVsQnVpbGRlcilcclxuICAgICAgICAgICAgPyBsYWJlbEJ1aWxkZXIoaXRlbSlcclxuICAgICAgICAgICAgOiBfdGhpcy51dGlscy5mb3JtYXQobGFiZWxCdWlsZGVyLCBpdGVtKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gTGltaXQgdGhlIGFtb3VudCBvZiBzZWxlY3RlZCB2YWx1ZXMgc2hvd24gaW4gbGFiZWxcclxuICAgICAgICBpZiAoIF90aGlzLm9wdGlvbnMubXVsdGlwbGUubWF4TGFiZWxFbnRyaWVzICkge1xyXG4gICAgICAgICAgaWYgKCBsYWJlbE1hcmt1cC5sZW5ndGggPj0gX3RoaXMub3B0aW9ucy5tdWx0aXBsZS5tYXhMYWJlbEVudHJpZXMgKyAxICkge1xyXG4gICAgICAgICAgICBsYWJlbE1hcmt1cCA9IGxhYmVsTWFya3VwLnNsaWNlKDAsIF90aGlzLm9wdGlvbnMubXVsdGlwbGUubWF4TGFiZWxFbnRyaWVzKTtcclxuICAgICAgICAgICAgbGFiZWxNYXJrdXAucHVzaChcclxuICAgICAgICAgICAgICAkLmlzRnVuY3Rpb24obGFiZWxCdWlsZGVyKVxyXG4gICAgICAgICAgICAgICAgPyBsYWJlbEJ1aWxkZXIoeyB0ZXh0OiAnLi4uJyB9KVxyXG4gICAgICAgICAgICAgICAgOiBfdGhpcy51dGlscy5mb3JtYXQobGFiZWxCdWlsZGVyLCB7IHRleHQ6ICcuLi4nIH0pKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGxhYmVsTWFya3VwLnNsaWNlKGxhYmVsTWFya3VwLmxlbmd0aCAtIDEpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBfdGhpcy5lbGVtZW50cy5sYWJlbC5odG1sKGxhYmVsTWFya3VwLmpvaW4oX3RoaXMub3B0aW9ucy5tdWx0aXBsZS5zZXBhcmF0b3IpKTtcclxuXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdmFyIGN1cnJJdGVtID0gX3RoaXMubG9va3VwSXRlbXNbX3RoaXMuc3RhdGUuY3VyclZhbHVlXTtcclxuXHJcbiAgICAgICAgX3RoaXMuZWxlbWVudHMubGFiZWwuaHRtbChcclxuICAgICAgICAgICQuaXNGdW5jdGlvbihsYWJlbEJ1aWxkZXIpXHJcbiAgICAgICAgICAgID8gbGFiZWxCdWlsZGVyKGN1cnJJdGVtKVxyXG4gICAgICAgICAgICA6IF90aGlzLnV0aWxzLmZvcm1hdChsYWJlbEJ1aWxkZXIsIGN1cnJJdGVtKVxyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLyoqIEdldCBhbmQgc2F2ZSB0aGUgYXZhaWxhYmxlIG9wdGlvbnMgKi9cclxuICAgIHBvcHVsYXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgdmFyICRvcHRpb25zID0gX3RoaXMuJGVsZW1lbnQuY2hpbGRyZW4oKTtcclxuICAgICAgdmFyICRqdXN0T3B0aW9ucyA9IF90aGlzLiRlbGVtZW50LmZpbmQoJ29wdGlvbicpO1xyXG4gICAgICB2YXIgJHNlbGVjdGVkID0gJGp1c3RPcHRpb25zLmZpbHRlcignOnNlbGVjdGVkJyk7XHJcbiAgICAgIHZhciBzZWxlY3RlZEluZGV4ID0gJGp1c3RPcHRpb25zLmluZGV4KCRzZWxlY3RlZCk7XHJcbiAgICAgIHZhciBjdXJySW5kZXggPSAwO1xyXG4gICAgICB2YXIgZW1wdHlWYWx1ZSA9IChfdGhpcy5zdGF0ZS5tdWx0aXBsZSA/IFtdIDogMCk7XHJcblxyXG4gICAgICBpZiAoICRzZWxlY3RlZC5sZW5ndGggPiAxICYmIF90aGlzLnN0YXRlLm11bHRpcGxlICkge1xyXG4gICAgICAgIHNlbGVjdGVkSW5kZXggPSBbXTtcclxuICAgICAgICAkc2VsZWN0ZWQuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHNlbGVjdGVkSW5kZXgucHVzaCgkKHRoaXMpLmluZGV4KCkpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBfdGhpcy5zdGF0ZS5jdXJyVmFsdWUgPSAofnNlbGVjdGVkSW5kZXggPyBzZWxlY3RlZEluZGV4IDogZW1wdHlWYWx1ZSk7XHJcbiAgICAgIF90aGlzLnN0YXRlLnNlbGVjdGVkSWR4ID0gX3RoaXMuc3RhdGUuY3VyclZhbHVlO1xyXG4gICAgICBfdGhpcy5zdGF0ZS5oaWdobGlnaHRlZElkeCA9IF90aGlzLnN0YXRlLmN1cnJWYWx1ZTtcclxuICAgICAgX3RoaXMuaXRlbXMgPSBbXTtcclxuICAgICAgX3RoaXMubG9va3VwSXRlbXMgPSBbXTtcclxuXHJcbiAgICAgIGlmICggJG9wdGlvbnMubGVuZ3RoICkge1xyXG4gICAgICAgIC8vIEJ1aWxkIG9wdGlvbnMgbWFya3VwXHJcbiAgICAgICAgJG9wdGlvbnMuZWFjaChmdW5jdGlvbihpKSB7XHJcbiAgICAgICAgICB2YXIgJGVsbSA9ICQodGhpcyk7XHJcblxyXG4gICAgICAgICAgaWYgKCAkZWxtLmlzKCdvcHRncm91cCcpICkge1xyXG5cclxuICAgICAgICAgICAgdmFyIG9wdGlvbnNHcm91cCA9IHtcclxuICAgICAgICAgICAgICBlbGVtZW50ICAgICAgIDogJGVsbSxcclxuICAgICAgICAgICAgICBsYWJlbCAgICAgICAgIDogJGVsbS5wcm9wKCdsYWJlbCcpLFxyXG4gICAgICAgICAgICAgIGdyb3VwRGlzYWJsZWQgOiAkZWxtLnByb3AoJ2Rpc2FibGVkJyksXHJcbiAgICAgICAgICAgICAgaXRlbXMgICAgICAgICA6IFtdXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAkZWxtLmNoaWxkcmVuKCkuZWFjaChmdW5jdGlvbihpKSB7XHJcbiAgICAgICAgICAgICAgdmFyICRlbG0gPSAkKHRoaXMpO1xyXG5cclxuICAgICAgICAgICAgICBvcHRpb25zR3JvdXAuaXRlbXNbaV0gPSBfdGhpcy5nZXRJdGVtRGF0YShjdXJySW5kZXgsICRlbG0sIG9wdGlvbnNHcm91cC5ncm91cERpc2FibGVkIHx8ICRlbG0ucHJvcCgnZGlzYWJsZWQnKSk7XHJcblxyXG4gICAgICAgICAgICAgIF90aGlzLmxvb2t1cEl0ZW1zW2N1cnJJbmRleF0gPSBvcHRpb25zR3JvdXAuaXRlbXNbaV07XHJcblxyXG4gICAgICAgICAgICAgIGN1cnJJbmRleCsrO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIF90aGlzLml0ZW1zW2ldID0gb3B0aW9uc0dyb3VwO1xyXG5cclxuICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICBfdGhpcy5pdGVtc1tpXSA9IF90aGlzLmdldEl0ZW1EYXRhKGN1cnJJbmRleCwgJGVsbSwgJGVsbS5wcm9wKCdkaXNhYmxlZCcpKTtcclxuXHJcbiAgICAgICAgICAgIF90aGlzLmxvb2t1cEl0ZW1zW2N1cnJJbmRleF0gPSBfdGhpcy5pdGVtc1tpXTtcclxuXHJcbiAgICAgICAgICAgIGN1cnJJbmRleCsrO1xyXG5cclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgX3RoaXMuc2V0TGFiZWwoKTtcclxuICAgICAgICBfdGhpcy5lbGVtZW50cy5pdGVtcy5hcHBlbmQoIF90aGlzLmVsZW1lbnRzLml0ZW1zU2Nyb2xsLmh0bWwoIF90aGlzLmdldEl0ZW1zTWFya3VwKF90aGlzLml0ZW1zKSApICk7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZW5lcmF0ZSBpdGVtcyBvYmplY3QgZGF0YVxyXG4gICAgICogQHBhcmFtICB7aW50ZWdlcn0gaW5kZXggICAgICAtIEN1cnJlbnQgaXRlbSBpbmRleFxyXG4gICAgICogQHBhcmFtICB7bm9kZX0gICAgJGVsbSAgICAgICAtIEN1cnJlbnQgZWxlbWVudCBub2RlXHJcbiAgICAgKiBAcGFyYW0gIHtib29sZWFufSBpc0Rpc2FibGVkIC0gQ3VycmVudCBlbGVtZW50IGRpc2FibGVkIHN0YXRlXHJcbiAgICAgKiBAcmV0dXJuIHtvYmplY3R9ICAgICAgICAgICAgICAgSXRlbSBvYmplY3RcclxuICAgICAqL1xyXG4gICAgZ2V0SXRlbURhdGE6IGZ1bmN0aW9uKGluZGV4LCAkZWxtLCBpc0Rpc2FibGVkKSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIGluZGV4ICAgICA6IGluZGV4LFxyXG4gICAgICAgIGVsZW1lbnQgICA6ICRlbG0sXHJcbiAgICAgICAgdmFsdWUgICAgIDogJGVsbS52YWwoKSxcclxuICAgICAgICBjbGFzc05hbWUgOiAkZWxtLnByb3AoJ2NsYXNzJyksXHJcbiAgICAgICAgdGV4dCAgICAgIDogJGVsbS5odG1sKCksXHJcbiAgICAgICAgc2x1ZyAgICAgIDogJC50cmltKF90aGlzLnV0aWxzLnJlcGxhY2VEaWFjcml0aWNzKCRlbG0uaHRtbCgpKSksXHJcbiAgICAgICAgYWx0ICAgICAgIDogJGVsbS5hdHRyKCdkYXRhLWFsdCcpLFxyXG4gICAgICAgIHNlbGVjdGVkICA6ICRlbG0ucHJvcCgnc2VsZWN0ZWQnKSxcclxuICAgICAgICBkaXNhYmxlZCAgOiBpc0Rpc2FibGVkXHJcbiAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2VuZXJhdGUgb3B0aW9ucyBtYXJrdXBcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gIHtvYmplY3R9IGl0ZW1zIC0gT2JqZWN0IGNvbnRhaW5pbmcgYWxsIGF2YWlsYWJsZSBvcHRpb25zXHJcbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9ICAgICAgICAgSFRNTCBmb3IgdGhlIG9wdGlvbnMgYm94XHJcbiAgICAgKi9cclxuICAgIGdldEl0ZW1zTWFya3VwOiBmdW5jdGlvbihpdGVtcykge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICB2YXIgbWFya3VwID0gJzx1bD4nO1xyXG5cclxuICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oX3RoaXMub3B0aW9ucy5saXN0QnVpbGRlcikgJiYgX3RoaXMub3B0aW9ucy5saXN0QnVpbGRlciApIHtcclxuICAgICAgICBpdGVtcyA9IF90aGlzLm9wdGlvbnMubGlzdEJ1aWxkZXIoaXRlbXMpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkLmVhY2goaXRlbXMsIGZ1bmN0aW9uKGksIGVsbSkge1xyXG4gICAgICAgIGlmICggZWxtLmxhYmVsICE9PSB1bmRlZmluZWQgKSB7XHJcblxyXG4gICAgICAgICAgbWFya3VwICs9IF90aGlzLnV0aWxzLmZvcm1hdCgnPHVsIGNsYXNzPVwiezF9XCI+PGxpIGNsYXNzPVwiezJ9XCI+ezN9PC9saT4nLFxyXG4gICAgICAgICAgICBfdGhpcy51dGlscy5hcnJheVRvQ2xhc3NuYW1lKFtcclxuICAgICAgICAgICAgICBfdGhpcy5jbGFzc2VzLmdyb3VwLFxyXG4gICAgICAgICAgICAgIGVsbS5ncm91cERpc2FibGVkID8gJ2Rpc2FibGVkJyA6ICcnLFxyXG4gICAgICAgICAgICAgIGVsbS5lbGVtZW50LnByb3AoJ2NsYXNzJylcclxuICAgICAgICAgICAgXSksXHJcbiAgICAgICAgICAgIF90aGlzLmNsYXNzZXMuZ3JvdXBsYWJlbCxcclxuICAgICAgICAgICAgZWxtLmVsZW1lbnQucHJvcCgnbGFiZWwnKVxyXG4gICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICAkLmVhY2goZWxtLml0ZW1zLCBmdW5jdGlvbihpLCBlbG0pIHtcclxuICAgICAgICAgICAgbWFya3VwICs9IF90aGlzLmdldEl0ZW1NYXJrdXAoZWxtLmluZGV4LCBlbG0pO1xyXG4gICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgbWFya3VwICs9ICc8L3VsPic7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgbWFya3VwICs9IF90aGlzLmdldEl0ZW1NYXJrdXAoZWxtLmluZGV4LCBlbG0pO1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgcmV0dXJuIG1hcmt1cCArICc8L3VsPic7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2VuZXJhdGUgZXZlcnkgb3B0aW9uIG1hcmt1cFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSAge251bWJlcn0gaW5kZXggICAgLSBJbmRleCBvZiBjdXJyZW50IGl0ZW1cclxuICAgICAqIEBwYXJhbSAge29iamVjdH0gaXRlbURhdGEgLSBDdXJyZW50IGl0ZW1cclxuICAgICAqIEByZXR1cm4ge3N0cmluZ30gICAgICAgICAgICBIVE1MIGZvciB0aGUgb3B0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldEl0ZW1NYXJrdXA6IGZ1bmN0aW9uKGluZGV4LCBpdGVtRGF0YSkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICB2YXIgaXRlbUJ1aWxkZXIgPSBfdGhpcy5vcHRpb25zLm9wdGlvbnNJdGVtQnVpbGRlcjtcclxuICAgICAgLy8gbGltaXQgYWNjZXNzIHRvIGl0ZW0gZGF0YSB0byBwcm92aWRlIGEgc2ltcGxlIGludGVyZmFjZVxyXG4gICAgICAvLyB0byBtb3N0IHJlbGV2YW50IG9wdGlvbnMuXHJcbiAgICAgIHZhciBmaWx0ZXJlZEl0ZW1EYXRhID0ge1xyXG4gICAgICAgIHZhbHVlOiBpdGVtRGF0YS52YWx1ZSxcclxuICAgICAgICB0ZXh0IDogaXRlbURhdGEudGV4dCxcclxuICAgICAgICBzbHVnIDogaXRlbURhdGEuc2x1ZyxcclxuICAgICAgICBpbmRleDogaXRlbURhdGEuaW5kZXhcclxuICAgICAgfTtcclxuXHJcbiAgICAgIHJldHVybiBfdGhpcy51dGlscy5mb3JtYXQoJzxsaSBkYXRhLWluZGV4PVwiezF9XCIgY2xhc3M9XCJ7Mn1cIj57M308L2xpPicsXHJcbiAgICAgICAgaW5kZXgsXHJcbiAgICAgICAgX3RoaXMudXRpbHMuYXJyYXlUb0NsYXNzbmFtZShbXHJcbiAgICAgICAgICBpdGVtRGF0YS5jbGFzc05hbWUsXHJcbiAgICAgICAgICBpbmRleCA9PT0gX3RoaXMuaXRlbXMubGVuZ3RoIC0gMSAgPyAnbGFzdCcgICAgIDogJycsXHJcbiAgICAgICAgICBpdGVtRGF0YS5kaXNhYmxlZCAgICAgICAgICAgICAgICAgPyAnZGlzYWJsZWQnIDogJycsXHJcbiAgICAgICAgICBpdGVtRGF0YS5zZWxlY3RlZCAgICAgICAgICAgICAgICAgPyAnc2VsZWN0ZWQnIDogJydcclxuICAgICAgICBdKSxcclxuICAgICAgICAkLmlzRnVuY3Rpb24oaXRlbUJ1aWxkZXIpXHJcbiAgICAgICAgICA/IF90aGlzLnV0aWxzLmZvcm1hdChpdGVtQnVpbGRlcihpdGVtRGF0YSwgdGhpcy4kZWxlbWVudCwgaW5kZXgpLCBpdGVtRGF0YSlcclxuICAgICAgICAgIDogX3RoaXMudXRpbHMuZm9ybWF0KGl0ZW1CdWlsZGVyLCBmaWx0ZXJlZEl0ZW1EYXRhKVxyXG4gICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKiogUmVtb3ZlIGV2ZW50cyBvbiB0aGUgZWxlbWVudHMgKi9cclxuICAgIHVuYmluZEV2ZW50czogZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICBfdGhpcy5lbGVtZW50cy53cmFwcGVyXHJcbiAgICAgICAgLmFkZChfdGhpcy4kZWxlbWVudClcclxuICAgICAgICAuYWRkKF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlcilcclxuICAgICAgICAuYWRkKF90aGlzLmVsZW1lbnRzLmlucHV0KVxyXG4gICAgICAgIC5vZmYoZXZlbnROYW1lc3BhY2VTdWZmaXgpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKiogQmluZCBldmVudHMgb24gdGhlIGVsZW1lbnRzICovXHJcbiAgICBiaW5kRXZlbnRzOiBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgIF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlci5vbignbW91c2VlbnRlcicgKyBldmVudE5hbWVzcGFjZVN1ZmZpeCArICcgbW91c2VsZWF2ZScgKyBldmVudE5hbWVzcGFjZVN1ZmZpeCwgZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICQodGhpcykudG9nZ2xlQ2xhc3MoX3RoaXMuY2xhc3Nlcy5ob3ZlciwgZS50eXBlID09PSAnbW91c2VlbnRlcicpO1xyXG5cclxuICAgICAgICAvLyBEZWxheSBjbG9zZSBlZmZlY3Qgd2hlbiBvcGVuT25Ib3ZlciBpcyB0cnVlXHJcbiAgICAgICAgaWYgKCBfdGhpcy5vcHRpb25zLm9wZW5PbkhvdmVyICkge1xyXG4gICAgICAgICAgY2xlYXJUaW1lb3V0KF90aGlzLmNsb3NlVGltZXIpO1xyXG5cclxuICAgICAgICAgIGlmICggZS50eXBlID09PSAnbW91c2VsZWF2ZScgKSB7XHJcbiAgICAgICAgICAgIF90aGlzLmNsb3NlVGltZXIgPSBzZXRUaW1lb3V0KCQucHJveHkoX3RoaXMuY2xvc2UsIF90aGlzKSwgX3RoaXMub3B0aW9ucy5ob3ZlckludGVudFRpbWVvdXQpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgX3RoaXMub3BlbigpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgICAvLyBUb2dnbGUgb3Blbi9jbG9zZVxyXG4gICAgICBfdGhpcy5lbGVtZW50cy53cmFwcGVyLm9uKCdjbGljaycgKyBldmVudE5hbWVzcGFjZVN1ZmZpeCwgZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIF90aGlzLnN0YXRlLm9wZW5lZCA/IF90aGlzLmNsb3NlKCkgOiBfdGhpcy5vcGVuKGUpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIC8vIFRyYW5zbGF0ZSBvcmlnaW5hbCBlbGVtZW50IGZvY3VzIGV2ZW50IHRvIGR1bW15IGlucHV0LlxyXG4gICAgICAvLyBEaXNhYmxlZCBvbiBtb2JpbGUgZGV2aWNlcyBiZWNhdXNlIHRoZSBkZWZhdWx0IG9wdGlvbiBsaXN0IGlzbid0XHJcbiAgICAgIC8vIHNob3duIGR1ZSB0aGUgZmFjdCB0aGF0IGhpZGRlbiBpbnB1dCBnZXRzIGZvY3VzZWRcclxuICAgICAgaWYgKCAhKF90aGlzLm9wdGlvbnMubmF0aXZlT25Nb2JpbGUgJiYgX3RoaXMudXRpbHMuaXNNb2JpbGUoKSkgKSB7XHJcbiAgICAgICAgX3RoaXMuJGVsZW1lbnQub24oJ2ZvY3VzJyArIGV2ZW50TmFtZXNwYWNlU3VmZml4LCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIF90aGlzLmVsZW1lbnRzLmlucHV0LmZvY3VzKCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIF90aGlzLmVsZW1lbnRzLmlucHV0XHJcbiAgICAgICAgICAucHJvcCh7IHRhYmluZGV4OiBfdGhpcy5vcmlnaW5hbFRhYmluZGV4LCBkaXNhYmxlZDogZmFsc2UgfSlcclxuICAgICAgICAgIC5vbigna2V5ZG93bicgKyBldmVudE5hbWVzcGFjZVN1ZmZpeCwgJC5wcm94eShfdGhpcy5oYW5kbGVLZXlzLCBfdGhpcykpXHJcbiAgICAgICAgICAub24oJ2ZvY3VzaW4nICsgZXZlbnROYW1lc3BhY2VTdWZmaXgsIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgX3RoaXMuZWxlbWVudHMub3V0ZXJXcmFwcGVyLmFkZENsYXNzKF90aGlzLmNsYXNzZXMuZm9jdXMpO1xyXG5cclxuICAgICAgICAgICAgLy8gUHJldmVudCB0aGUgZmxpY2tlciB3aGVuIGZvY3VzaW5nIG91dCBhbmQgYmFjayBhZ2FpbiBpbiB0aGUgYnJvd3NlciB3aW5kb3dcclxuICAgICAgICAgICAgX3RoaXMuZWxlbWVudHMuaW5wdXQub25lKCdibHVyJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgX3RoaXMuZWxlbWVudHMuaW5wdXQuYmx1cigpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGlmICggX3RoaXMub3B0aW9ucy5vcGVuT25Gb2N1cyAmJiAhX3RoaXMuc3RhdGUub3BlbmVkICkge1xyXG4gICAgICAgICAgICAgIF90aGlzLm9wZW4oZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAub24oJ2ZvY3Vzb3V0JyArIGV2ZW50TmFtZXNwYWNlU3VmZml4LCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgX3RoaXMuZWxlbWVudHMub3V0ZXJXcmFwcGVyLnJlbW92ZUNsYXNzKF90aGlzLmNsYXNzZXMuZm9jdXMpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC5vbignaW5wdXQgcHJvcGVydHljaGFuZ2UnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdmFyIHZhbCA9IF90aGlzLmVsZW1lbnRzLmlucHV0LnZhbCgpO1xyXG4gICAgICAgICAgICB2YXIgc2VhcmNoUmVnRXhwID0gbmV3IFJlZ0V4cCgnXicgKyBfdGhpcy51dGlscy5lc2NhcGVSZWdFeHAodmFsKSwgJ2knKTtcclxuXHJcbiAgICAgICAgICAgIC8vIENsZWFyIHNlYXJjaFxyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQoX3RoaXMucmVzZXRTdHIpO1xyXG4gICAgICAgICAgICBfdGhpcy5yZXNldFN0ciA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgX3RoaXMuZWxlbWVudHMuaW5wdXQudmFsKCcnKTtcclxuICAgICAgICAgICAgfSwgX3RoaXMub3B0aW9ucy5rZXlTZWFyY2hUaW1lb3V0KTtcclxuXHJcbiAgICAgICAgICAgIGlmICggdmFsLmxlbmd0aCApIHtcclxuICAgICAgICAgICAgICAvLyBTZWFyY2ggaW4gc2VsZWN0IG9wdGlvbnNcclxuICAgICAgICAgICAgICAkLmVhY2goX3RoaXMuaXRlbXMsIGZ1bmN0aW9uKGksIGVsbSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVsbS5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoc2VhcmNoUmVnRXhwLnRlc3QoZWxtLnRleHQpIHx8IHNlYXJjaFJlZ0V4cC50ZXN0KGVsbS5zbHVnKSkge1xyXG4gICAgICAgICAgICAgICAgICBfdGhpcy5oaWdobGlnaHQoaSk7XHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICghZWxtLmFsdCkge1xyXG4gICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB2YXIgYWx0SXRlbXMgPSBlbG0uYWx0LnNwbGl0KCd8Jyk7XHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBhaSA9IDA7IGFpIDwgYWx0SXRlbXMubGVuZ3RoOyBhaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgIGlmICghYWx0SXRlbXNbYWldKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgaWYgKHNlYXJjaFJlZ0V4cC50ZXN0KGFsdEl0ZW1zW2FpXS50cmltKCkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuaGlnaGxpZ2h0KGkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgX3RoaXMuJGxpLm9uKHtcclxuICAgICAgICAvLyBQcmV2ZW50IDxpbnB1dD4gYmx1ciBvbiBDaHJvbWVcclxuICAgICAgICBtb3VzZWRvd246IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICBfdGhpcy5zZWxlY3QoJCh0aGlzKS5kYXRhKCdpbmRleCcpKTtcclxuXHJcbiAgICAgICAgICAvLyBDaHJvbWUgZG9lc24ndCBjbG9zZSBvcHRpb25zIGJveCBpZiBzZWxlY3QgaXMgd3JhcHBlZCB3aXRoIGEgbGFiZWxcclxuICAgICAgICAgIC8vIFdlIG5lZWQgdG8gJ3JldHVybiBmYWxzZScgdG8gYXZvaWQgdGhhdFxyXG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQmVoYXZpb3Igd2hlbiBrZXlib2FyZCBrZXlzIGlzIHByZXNzZWRcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gZSAtIEV2ZW50IG9iamVjdFxyXG4gICAgICovXHJcbiAgICBoYW5kbGVLZXlzOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgIHZhciBrZXkgPSBlLndoaWNoO1xyXG4gICAgICB2YXIga2V5cyA9IF90aGlzLm9wdGlvbnMua2V5cztcclxuXHJcbiAgICAgIHZhciBpc1ByZXZLZXkgPSAkLmluQXJyYXkoa2V5LCBrZXlzLnByZXZpb3VzKSA+IC0xO1xyXG4gICAgICB2YXIgaXNOZXh0S2V5ID0gJC5pbkFycmF5KGtleSwga2V5cy5uZXh0KSA+IC0xO1xyXG4gICAgICB2YXIgaXNTZWxlY3RLZXkgPSAkLmluQXJyYXkoa2V5LCBrZXlzLnNlbGVjdCkgPiAtMTtcclxuICAgICAgdmFyIGlzT3BlbktleSA9ICQuaW5BcnJheShrZXksIGtleXMub3BlbikgPiAtMTtcclxuICAgICAgdmFyIGlkeCA9IF90aGlzLnN0YXRlLmhpZ2hsaWdodGVkSWR4O1xyXG4gICAgICB2YXIgaXNGaXJzdE9yTGFzdEl0ZW0gPSAoaXNQcmV2S2V5ICYmIGlkeCA9PT0gMCkgfHwgKGlzTmV4dEtleSAmJiAoaWR4ICsgMSkgPT09IF90aGlzLml0ZW1zLmxlbmd0aCk7XHJcbiAgICAgIHZhciBnb1RvSXRlbSA9IDA7XHJcblxyXG4gICAgICAvLyBFbnRlciAvIFNwYWNlXHJcbiAgICAgIGlmICgga2V5ID09PSAxMyB8fCBrZXkgPT09IDMyICkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gSWYgaXQncyBhIGRpcmVjdGlvbmFsIGtleVxyXG4gICAgICBpZiAoIGlzUHJldktleSB8fCBpc05leHRLZXkgKSB7XHJcbiAgICAgICAgaWYgKCAhX3RoaXMub3B0aW9ucy5hbGxvd1dyYXAgJiYgaXNGaXJzdE9yTGFzdEl0ZW0gKSB7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIGlzUHJldktleSApIHtcclxuICAgICAgICAgIGdvVG9JdGVtID0gX3RoaXMudXRpbHMucHJldmlvdXNFbmFibGVkSXRlbShfdGhpcy5sb29rdXBJdGVtcywgaWR4KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggaXNOZXh0S2V5ICkge1xyXG4gICAgICAgICAgZ29Ub0l0ZW0gPSBfdGhpcy51dGlscy5uZXh0RW5hYmxlZEl0ZW0oX3RoaXMubG9va3VwSXRlbXMsIGlkeCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfdGhpcy5oaWdobGlnaHQoZ29Ub0l0ZW0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBUYWIgLyBFbnRlciAvIEVTQ1xyXG4gICAgICBpZiAoIGlzU2VsZWN0S2V5ICYmIF90aGlzLnN0YXRlLm9wZW5lZCApIHtcclxuICAgICAgICBfdGhpcy5zZWxlY3QoaWR4KTtcclxuXHJcbiAgICAgICAgaWYgKCAhX3RoaXMuc3RhdGUubXVsdGlwbGUgfHwgIV90aGlzLm9wdGlvbnMubXVsdGlwbGUua2VlcE1lbnVPcGVuICkge1xyXG4gICAgICAgICAgX3RoaXMuY2xvc2UoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gU3BhY2UgLyBFbnRlciAvIExlZnQgLyBVcCAvIFJpZ2h0IC8gRG93blxyXG4gICAgICBpZiAoIGlzT3BlbktleSAmJiAhX3RoaXMuc3RhdGUub3BlbmVkICkge1xyXG4gICAgICAgIF90aGlzLm9wZW4oKTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvKiogVXBkYXRlIHRoZSBpdGVtcyBvYmplY3QgKi9cclxuICAgIHJlZnJlc2g6IGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgX3RoaXMucG9wdWxhdGUoKTtcclxuICAgICAgX3RoaXMuYWN0aXZhdGUoKTtcclxuICAgICAgX3RoaXMudXRpbHMudHJpZ2dlckNhbGxiYWNrKCdSZWZyZXNoJywgX3RoaXMpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKiogU2V0IG9wdGlvbnMgYm94IHdpZHRoL2hlaWdodCAqL1xyXG4gICAgc2V0T3B0aW9uc0RpbWVuc2lvbnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgLy8gQ2FsY3VsYXRlIG9wdGlvbnMgYm94IGhlaWdodFxyXG4gICAgICAvLyBTZXQgYSB0ZW1wb3JhcnkgY2xhc3Mgb24gdGhlIGhpZGRlbiBwYXJlbnQgb2YgdGhlIGVsZW1lbnRcclxuICAgICAgdmFyIGhpZGRlbkNoaWxkcmVuID0gX3RoaXMuZWxlbWVudHMuaXRlbXMuY2xvc2VzdCgnOnZpc2libGUnKS5jaGlsZHJlbignOmhpZGRlbicpLmFkZENsYXNzKF90aGlzLmNsYXNzZXMudGVtcHNob3cpO1xyXG4gICAgICB2YXIgbWF4SGVpZ2h0ID0gX3RoaXMub3B0aW9ucy5tYXhIZWlnaHQ7XHJcbiAgICAgIHZhciBpdGVtc1dpZHRoID0gX3RoaXMuZWxlbWVudHMuaXRlbXMub3V0ZXJXaWR0aCgpO1xyXG4gICAgICB2YXIgd3JhcHBlcldpZHRoID0gX3RoaXMuZWxlbWVudHMud3JhcHBlci5vdXRlcldpZHRoKCkgLSAoaXRlbXNXaWR0aCAtIF90aGlzLmVsZW1lbnRzLml0ZW1zLndpZHRoKCkpO1xyXG5cclxuICAgICAgLy8gU2V0IHRoZSBkaW1lbnNpb25zLCBtaW5pbXVtIGlzIHdyYXBwZXIgd2lkdGgsIGV4cGFuZCBmb3IgbG9uZyBpdGVtcyBpZiBvcHRpb24gaXMgdHJ1ZVxyXG4gICAgICBpZiAoICFfdGhpcy5vcHRpb25zLmV4cGFuZFRvSXRlbVRleHQgfHwgd3JhcHBlcldpZHRoID4gaXRlbXNXaWR0aCApIHtcclxuICAgICAgICBfdGhpcy5maW5hbFdpZHRoID0gd3JhcHBlcldpZHRoO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vIE1ha2Ugc3VyZSB0aGUgc2Nyb2xsYmFyIHdpZHRoIGlzIGluY2x1ZGVkXHJcbiAgICAgICAgX3RoaXMuZWxlbWVudHMuaXRlbXMuY3NzKCdvdmVyZmxvdycsICdzY3JvbGwnKTtcclxuXHJcbiAgICAgICAgLy8gU2V0IGEgcmVhbGx5IGxvbmcgd2lkdGggZm9yIF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlclxyXG4gICAgICAgIF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlci53aWR0aCg5ZTQpO1xyXG4gICAgICAgIF90aGlzLmZpbmFsV2lkdGggPSBfdGhpcy5lbGVtZW50cy5pdGVtcy53aWR0aCgpO1xyXG4gICAgICAgIC8vIFNldCBzY3JvbGwgYmFyIHRvIGF1dG9cclxuICAgICAgICBfdGhpcy5lbGVtZW50cy5pdGVtcy5jc3MoJ292ZXJmbG93JywgJycpO1xyXG4gICAgICAgIF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlci53aWR0aCgnJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIF90aGlzLmVsZW1lbnRzLml0ZW1zLndpZHRoKF90aGlzLmZpbmFsV2lkdGgpLmhlaWdodCgpID4gbWF4SGVpZ2h0ICYmIF90aGlzLmVsZW1lbnRzLml0ZW1zLmhlaWdodChtYXhIZWlnaHQpO1xyXG5cclxuICAgICAgLy8gUmVtb3ZlIHRoZSB0ZW1wb3JhcnkgY2xhc3NcclxuICAgICAgaGlkZGVuQ2hpbGRyZW4ucmVtb3ZlQ2xhc3MoX3RoaXMuY2xhc3Nlcy50ZW1wc2hvdyk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKiBEZXRlY3QgaWYgdGhlIG9wdGlvbnMgYm94IGlzIGluc2lkZSB0aGUgd2luZG93ICovXHJcbiAgICBpc0luVmlld3BvcnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKF90aGlzLm9wdGlvbnMuZm9yY2VSZW5kZXJBYm92ZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgIF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlci5hZGRDbGFzcyhfdGhpcy5jbGFzc2VzLmFib3ZlKTtcclxuICAgICAgfSBlbHNlIGlmIChfdGhpcy5vcHRpb25zLmZvcmNlUmVuZGVyQmVsb3cgPT09IHRydWUpIHtcclxuICAgICAgICBfdGhpcy5lbGVtZW50cy5vdXRlcldyYXBwZXIuYWRkQ2xhc3MoX3RoaXMuY2xhc3Nlcy5iZWxvdyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdmFyIHNjcm9sbFRvcCA9ICR3aW4uc2Nyb2xsVG9wKCk7XHJcbiAgICAgICAgdmFyIHdpbkhlaWdodCA9ICR3aW4uaGVpZ2h0KCk7XHJcbiAgICAgICAgdmFyIHVpUG9zWCA9IF90aGlzLmVsZW1lbnRzLm91dGVyV3JhcHBlci5vZmZzZXQoKS50b3A7XHJcbiAgICAgICAgdmFyIHVpSGVpZ2h0ID0gX3RoaXMuZWxlbWVudHMub3V0ZXJXcmFwcGVyLm91dGVySGVpZ2h0KCk7XHJcblxyXG4gICAgICAgIHZhciBmaXRzRG93biA9ICh1aVBvc1ggKyB1aUhlaWdodCArIF90aGlzLml0ZW1zSGVpZ2h0KSA8PSAoc2Nyb2xsVG9wICsgd2luSGVpZ2h0KTtcclxuICAgICAgICB2YXIgZml0c0Fib3ZlID0gKHVpUG9zWCAtIF90aGlzLml0ZW1zSGVpZ2h0KSA+IHNjcm9sbFRvcDtcclxuXHJcbiAgICAgICAgLy8gSWYgaXQgZG9lcyBub3QgZml0IGJlbG93LCBvbmx5IHJlbmRlciBpdFxyXG4gICAgICAgIC8vIGFib3ZlIGl0IGZpdCdzIHRoZXJlLlxyXG4gICAgICAgIC8vIEl0J3MgYWNjZXB0YWJsZSB0aGF0IHRoZSB1c2VyIG5lZWRzIHRvXHJcbiAgICAgICAgLy8gc2Nyb2xsIHRoZSB2aWV3cG9ydCB0byBzZWUgdGhlIGN1dCBvZmYgVUlcclxuICAgICAgICB2YXIgcmVuZGVyQWJvdmUgPSAhZml0c0Rvd24gJiYgZml0c0Fib3ZlO1xyXG4gICAgICAgIHZhciByZW5kZXJCZWxvdyA9ICFyZW5kZXJBYm92ZTtcclxuXHJcbiAgICAgICAgX3RoaXMuZWxlbWVudHMub3V0ZXJXcmFwcGVyLnRvZ2dsZUNsYXNzKF90aGlzLmNsYXNzZXMuYWJvdmUsIHJlbmRlckFib3ZlKTtcclxuICAgICAgICBfdGhpcy5lbGVtZW50cy5vdXRlcldyYXBwZXIudG9nZ2xlQ2xhc3MoX3RoaXMuY2xhc3Nlcy5iZWxvdywgcmVuZGVyQmVsb3cpO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGV0ZWN0IGlmIGN1cnJlbnRseSBzZWxlY3RlZCBvcHRpb24gaXMgdmlzaWJsZSBhbmQgc2Nyb2xsIHRoZSBvcHRpb25zIGJveCB0byBzaG93IGl0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtOdW1iZXJ8QXJyYXl9IGluZGV4IC0gSW5kZXggb2YgdGhlIHNlbGVjdGVkIGl0ZW1zXHJcbiAgICAgKi9cclxuICAgIGRldGVjdEl0ZW1WaXNpYmlsaXR5OiBmdW5jdGlvbihpbmRleCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICB2YXIgJGZpbHRlcmVkTGkgPSBfdGhpcy4kbGkuZmlsdGVyKCdbZGF0YS1pbmRleF0nKTtcclxuXHJcbiAgICAgIGlmICggX3RoaXMuc3RhdGUubXVsdGlwbGUgKSB7XHJcbiAgICAgICAgLy8gSWYgaW5kZXggaXMgYW4gYXJyYXksIHdlIGNhbiBhc3N1bWUgYSBtdWx0aXBsZSBzZWxlY3QgYW5kIHdlXHJcbiAgICAgICAgLy8gd2FudCB0byBzY3JvbGwgdG8gdGhlIHVwcGVybW9zdCBzZWxlY3RlZCBpdGVtIVxyXG4gICAgICAgIC8vIE1hdGgubWluLmFwcGx5KE1hdGgsIGluZGV4KSByZXR1cm5zIHRoZSBsb3dlc3QgZW50cnkgaW4gYW4gQXJyYXkuXHJcbiAgICAgICAgaW5kZXggPSAoJC5pc0FycmF5KGluZGV4KSAmJiBpbmRleC5sZW5ndGggPT09IDApID8gMCA6IGluZGV4O1xyXG4gICAgICAgIGluZGV4ID0gJC5pc0FycmF5KGluZGV4KSA/IE1hdGgubWluLmFwcGx5KE1hdGgsIGluZGV4KSA6IGluZGV4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgbGlIZWlnaHQgPSAkZmlsdGVyZWRMaS5lcShpbmRleCkub3V0ZXJIZWlnaHQoKTtcclxuICAgICAgdmFyIGxpVG9wID0gJGZpbHRlcmVkTGlbaW5kZXhdLm9mZnNldFRvcDtcclxuICAgICAgdmFyIGl0ZW1zU2Nyb2xsVG9wID0gX3RoaXMuZWxlbWVudHMuaXRlbXNTY3JvbGwuc2Nyb2xsVG9wKCk7XHJcbiAgICAgIHZhciBzY3JvbGxUID0gbGlUb3AgKyBsaUhlaWdodCAqIDI7XHJcblxyXG4gICAgICBfdGhpcy5lbGVtZW50cy5pdGVtc1Njcm9sbC5zY3JvbGxUb3AoXHJcbiAgICAgICAgc2Nyb2xsVCA+IGl0ZW1zU2Nyb2xsVG9wICsgX3RoaXMuaXRlbXNIZWlnaHQgPyBzY3JvbGxUIC0gX3RoaXMuaXRlbXNIZWlnaHQgOlxyXG4gICAgICAgICAgbGlUb3AgLSBsaUhlaWdodCA8IGl0ZW1zU2Nyb2xsVG9wID8gbGlUb3AgLSBsaUhlaWdodCA6XHJcbiAgICAgICAgICAgIGl0ZW1zU2Nyb2xsVG9wXHJcbiAgICAgICk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT3BlbiB0aGUgc2VsZWN0IG9wdGlvbnMgYm94XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtFdmVudH0gZSAtIEV2ZW50XHJcbiAgICAgKi9cclxuICAgIG9wZW46IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgIGlmICggX3RoaXMub3B0aW9ucy5uYXRpdmVPbk1vYmlsZSAmJiBfdGhpcy51dGlscy5pc01vYmlsZSgpKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBfdGhpcy51dGlscy50cmlnZ2VyQ2FsbGJhY2soJ0JlZm9yZU9wZW4nLCBfdGhpcyk7XHJcblxyXG4gICAgICBpZiAoIGUgKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGlmIChfdGhpcy5vcHRpb25zLnN0b3BQcm9wYWdhdGlvbikge1xyXG4gICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICggX3RoaXMuc3RhdGUuZW5hYmxlZCApIHtcclxuICAgICAgICBfdGhpcy5zZXRPcHRpb25zRGltZW5zaW9ucygpO1xyXG5cclxuICAgICAgICAvLyBGaW5kIGFueSBvdGhlciBvcGVuZWQgaW5zdGFuY2VzIG9mIHNlbGVjdCBhbmQgY2xvc2UgaXRcclxuICAgICAgICAkKCcuJyArIF90aGlzLmNsYXNzZXMuaGlkZXNlbGVjdCwgJy4nICsgX3RoaXMuY2xhc3Nlcy5vcGVuKS5jaGlsZHJlbigpW3BsdWdpbk5hbWVdKCdjbG9zZScpO1xyXG5cclxuICAgICAgICBfdGhpcy5zdGF0ZS5vcGVuZWQgPSB0cnVlO1xyXG4gICAgICAgIF90aGlzLml0ZW1zSGVpZ2h0ID0gX3RoaXMuZWxlbWVudHMuaXRlbXMub3V0ZXJIZWlnaHQoKTtcclxuICAgICAgICBfdGhpcy5pdGVtc0lubmVySGVpZ2h0ID0gX3RoaXMuZWxlbWVudHMuaXRlbXMuaGVpZ2h0KCk7XHJcblxyXG4gICAgICAgIC8vIFRvZ2dsZSBvcHRpb25zIGJveCB2aXNpYmlsaXR5XHJcbiAgICAgICAgX3RoaXMuZWxlbWVudHMub3V0ZXJXcmFwcGVyLmFkZENsYXNzKF90aGlzLmNsYXNzZXMub3Blbik7XHJcblxyXG4gICAgICAgIC8vIEdpdmUgZHVtbXkgaW5wdXQgZm9jdXNcclxuICAgICAgICBfdGhpcy5lbGVtZW50cy5pbnB1dC52YWwoJycpO1xyXG4gICAgICAgIGlmICggZSAmJiBlLnR5cGUgIT09ICdmb2N1c2luJyApIHtcclxuICAgICAgICAgIF90aGlzLmVsZW1lbnRzLmlucHV0LmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBEZWxheWVkIGJpbmRzIGV2ZW50cyBvbiBEb2N1bWVudCB0byBtYWtlIGxhYmVsIGNsaWNrcyB3b3JrXHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICRkb2NcclxuICAgICAgICAgICAgLm9uKCdjbGljaycgKyBldmVudE5hbWVzcGFjZVN1ZmZpeCwgJC5wcm94eShfdGhpcy5jbG9zZSwgX3RoaXMpKVxyXG4gICAgICAgICAgICAub24oJ3Njcm9sbCcgKyBldmVudE5hbWVzcGFjZVN1ZmZpeCwgJC5wcm94eShfdGhpcy5pc0luVmlld3BvcnQsIF90aGlzKSk7XHJcbiAgICAgICAgfSwgMSk7XHJcblxyXG4gICAgICAgIF90aGlzLmlzSW5WaWV3cG9ydCgpO1xyXG5cclxuICAgICAgICAvLyBQcmV2ZW50IHdpbmRvdyBzY3JvbGwgd2hlbiB1c2luZyBtb3VzZSB3aGVlbCBpbnNpZGUgaXRlbXMgYm94XHJcbiAgICAgICAgaWYgKCBfdGhpcy5vcHRpb25zLnByZXZlbnRXaW5kb3dTY3JvbGwgKSB7XHJcbiAgICAgICAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xyXG4gICAgICAgICAgJGRvYy5vbignbW91c2V3aGVlbCcgKyBldmVudE5hbWVzcGFjZVN1ZmZpeCArICcgRE9NTW91c2VTY3JvbGwnICsgZXZlbnROYW1lc3BhY2VTdWZmaXgsICcuJyArIF90aGlzLmNsYXNzZXMuc2Nyb2xsLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgIHZhciBvcmdFdmVudCA9IGUub3JpZ2luYWxFdmVudDtcclxuICAgICAgICAgICAgdmFyIHNjcm9sbFRvcCA9ICQodGhpcykuc2Nyb2xsVG9wKCk7XHJcbiAgICAgICAgICAgIHZhciBkZWx0YVkgPSAwO1xyXG5cclxuICAgICAgICAgICAgaWYgKCAnZGV0YWlsJyAgICAgIGluIG9yZ0V2ZW50ICkgeyBkZWx0YVkgPSBvcmdFdmVudC5kZXRhaWwgKiAtMTsgfVxyXG4gICAgICAgICAgICBpZiAoICd3aGVlbERlbHRhJyAgaW4gb3JnRXZlbnQgKSB7IGRlbHRhWSA9IG9yZ0V2ZW50LndoZWVsRGVsdGE7ICB9XHJcbiAgICAgICAgICAgIGlmICggJ3doZWVsRGVsdGFZJyBpbiBvcmdFdmVudCApIHsgZGVsdGFZID0gb3JnRXZlbnQud2hlZWxEZWx0YVk7IH1cclxuICAgICAgICAgICAgaWYgKCAnZGVsdGFZJyAgICAgIGluIG9yZ0V2ZW50ICkgeyBkZWx0YVkgPSBvcmdFdmVudC5kZWx0YVkgKiAtMTsgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCBzY3JvbGxUb3AgPT09ICh0aGlzLnNjcm9sbEhlaWdodCAtIF90aGlzLml0ZW1zSW5uZXJIZWlnaHQpICYmIGRlbHRhWSA8IDAgfHwgc2Nyb2xsVG9wID09PSAwICYmIGRlbHRhWSA+IDAgKSB7XHJcbiAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF90aGlzLmRldGVjdEl0ZW1WaXNpYmlsaXR5KF90aGlzLnN0YXRlLnNlbGVjdGVkSWR4KTtcclxuXHJcbiAgICAgICAgX3RoaXMuaGlnaGxpZ2h0KF90aGlzLnN0YXRlLm11bHRpcGxlID8gLTEgOiBfdGhpcy5zdGF0ZS5zZWxlY3RlZElkeCk7XHJcblxyXG4gICAgICAgIF90aGlzLnV0aWxzLnRyaWdnZXJDYWxsYmFjaygnT3BlbicsIF90aGlzKTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvKiogQ2xvc2UgdGhlIHNlbGVjdCBvcHRpb25zIGJveCAqL1xyXG4gICAgY2xvc2U6IGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgX3RoaXMudXRpbHMudHJpZ2dlckNhbGxiYWNrKCdCZWZvcmVDbG9zZScsIF90aGlzKTtcclxuXHJcbiAgICAgIC8vIFJlbW92ZSBjdXN0b20gZXZlbnRzIG9uIGRvY3VtZW50XHJcbiAgICAgICRkb2Mub2ZmKGV2ZW50TmFtZXNwYWNlU3VmZml4KTtcclxuXHJcbiAgICAgIC8vIFJlbW92ZSB2aXNpYmxlIGNsYXNzIHRvIGhpZGUgb3B0aW9ucyBib3hcclxuICAgICAgX3RoaXMuZWxlbWVudHMub3V0ZXJXcmFwcGVyLnJlbW92ZUNsYXNzKF90aGlzLmNsYXNzZXMub3Blbik7XHJcblxyXG4gICAgICBfdGhpcy5zdGF0ZS5vcGVuZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgIF90aGlzLnV0aWxzLnRyaWdnZXJDYWxsYmFjaygnQ2xvc2UnLCBfdGhpcyk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKiBTZWxlY3QgY3VycmVudCBvcHRpb24gYW5kIGNoYW5nZSB0aGUgbGFiZWwgKi9cclxuICAgIGNoYW5nZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICBfdGhpcy51dGlscy50cmlnZ2VyQ2FsbGJhY2soJ0JlZm9yZUNoYW5nZScsIF90aGlzKTtcclxuXHJcbiAgICAgIGlmICggX3RoaXMuc3RhdGUubXVsdGlwbGUgKSB7XHJcbiAgICAgICAgLy8gUmVzZXQgb2xkIHNlbGVjdGVkXHJcbiAgICAgICAgJC5lYWNoKF90aGlzLmxvb2t1cEl0ZW1zLCBmdW5jdGlvbihpZHgpIHtcclxuICAgICAgICAgIF90aGlzLmxvb2t1cEl0ZW1zW2lkeF0uc2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgIF90aGlzLiRlbGVtZW50LmZpbmQoJ29wdGlvbicpLnByb3AoJ3NlbGVjdGVkJywgZmFsc2UpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBTZXQgbmV3IHNlbGVjdGVkXHJcbiAgICAgICAgJC5lYWNoKF90aGlzLnN0YXRlLnNlbGVjdGVkSWR4LCBmdW5jdGlvbihpZHgsIHZhbHVlKSB7XHJcbiAgICAgICAgICBfdGhpcy5sb29rdXBJdGVtc1t2YWx1ZV0uc2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgX3RoaXMuJGVsZW1lbnQuZmluZCgnb3B0aW9uJykuZXEodmFsdWUpLnByb3AoJ3NlbGVjdGVkJywgdHJ1ZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIF90aGlzLnN0YXRlLmN1cnJWYWx1ZSA9IF90aGlzLnN0YXRlLnNlbGVjdGVkSWR4O1xyXG5cclxuICAgICAgICBfdGhpcy5zZXRMYWJlbCgpO1xyXG5cclxuICAgICAgICBfdGhpcy51dGlscy50cmlnZ2VyQ2FsbGJhY2soJ0NoYW5nZScsIF90aGlzKTtcclxuICAgICAgfSBlbHNlIGlmICggX3RoaXMuc3RhdGUuY3VyclZhbHVlICE9PSBfdGhpcy5zdGF0ZS5zZWxlY3RlZElkeCApIHtcclxuICAgICAgICAvLyBBcHBseSBjaGFuZ2VkIHZhbHVlIHRvIG9yaWdpbmFsIHNlbGVjdFxyXG4gICAgICAgIF90aGlzLiRlbGVtZW50XHJcbiAgICAgICAgICAucHJvcCgnc2VsZWN0ZWRJbmRleCcsIF90aGlzLnN0YXRlLmN1cnJWYWx1ZSA9IF90aGlzLnN0YXRlLnNlbGVjdGVkSWR4KVxyXG4gICAgICAgICAgLmRhdGEoJ3ZhbHVlJywgX3RoaXMubG9va3VwSXRlbXNbX3RoaXMuc3RhdGUuc2VsZWN0ZWRJZHhdLnRleHQpO1xyXG5cclxuICAgICAgICAvLyBDaGFuZ2UgbGFiZWwgdGV4dFxyXG4gICAgICAgIF90aGlzLnNldExhYmVsKCk7XHJcblxyXG4gICAgICAgIF90aGlzLnV0aWxzLnRyaWdnZXJDYWxsYmFjaygnQ2hhbmdlJywgX3RoaXMpO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSGlnaGxpZ2h0IG9wdGlvblxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGluZGV4IC0gSW5kZXggb2YgdGhlIG9wdGlvbnMgdGhhdCB3aWxsIGJlIGhpZ2hsaWdodGVkXHJcbiAgICAgKi9cclxuICAgIGhpZ2hsaWdodDogZnVuY3Rpb24oaW5kZXgpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgdmFyICRmaWx0ZXJlZExpID0gX3RoaXMuJGxpLmZpbHRlcignW2RhdGEtaW5kZXhdJykucmVtb3ZlQ2xhc3MoJ2hpZ2hsaWdodGVkJyk7XHJcblxyXG4gICAgICBfdGhpcy51dGlscy50cmlnZ2VyQ2FsbGJhY2soJ0JlZm9yZUhpZ2hsaWdodCcsIF90aGlzKTtcclxuXHJcbiAgICAgIC8vIFBhcmFtZXRlciBpbmRleCBpcyByZXF1aXJlZCBhbmQgc2hvdWxkIG5vdCBiZSBhIGRpc2FibGVkIGl0ZW1cclxuICAgICAgaWYgKCBpbmRleCA9PT0gdW5kZWZpbmVkIHx8IGluZGV4ID09PSAtMSB8fCBfdGhpcy5sb29rdXBJdGVtc1tpbmRleF0uZGlzYWJsZWQgKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkZmlsdGVyZWRMaVxyXG4gICAgICAgIC5lcShfdGhpcy5zdGF0ZS5oaWdobGlnaHRlZElkeCA9IGluZGV4KVxyXG4gICAgICAgIC5hZGRDbGFzcygnaGlnaGxpZ2h0ZWQnKTtcclxuXHJcbiAgICAgIF90aGlzLmRldGVjdEl0ZW1WaXNpYmlsaXR5KGluZGV4KTtcclxuXHJcbiAgICAgIF90aGlzLnV0aWxzLnRyaWdnZXJDYWxsYmFjaygnSGlnaGxpZ2h0JywgX3RoaXMpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbGVjdCBvcHRpb25cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gaW5kZXggLSBJbmRleCBvZiB0aGUgb3B0aW9uIHRoYXQgd2lsbCBiZSBzZWxlY3RlZFxyXG4gICAgICovXHJcbiAgICBzZWxlY3Q6IGZ1bmN0aW9uKGluZGV4KSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgIHZhciAkZmlsdGVyZWRMaSA9IF90aGlzLiRsaS5maWx0ZXIoJ1tkYXRhLWluZGV4XScpO1xyXG5cclxuICAgICAgX3RoaXMudXRpbHMudHJpZ2dlckNhbGxiYWNrKCdCZWZvcmVTZWxlY3QnLCBfdGhpcywgaW5kZXgpO1xyXG5cclxuICAgICAgLy8gUGFyYW1ldGVyIGluZGV4IGlzIHJlcXVpcmVkIGFuZCBzaG91bGQgbm90IGJlIGEgZGlzYWJsZWQgaXRlbVxyXG4gICAgICBpZiAoIGluZGV4ID09PSB1bmRlZmluZWQgfHwgaW5kZXggPT09IC0xIHx8IF90aGlzLmxvb2t1cEl0ZW1zW2luZGV4XS5kaXNhYmxlZCApIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICggX3RoaXMuc3RhdGUubXVsdGlwbGUgKSB7XHJcbiAgICAgICAgLy8gTWFrZSBzdXJlIHNlbGVjdGVkSWR4IGlzIGFuIGFycmF5XHJcbiAgICAgICAgX3RoaXMuc3RhdGUuc2VsZWN0ZWRJZHggPSAkLmlzQXJyYXkoX3RoaXMuc3RhdGUuc2VsZWN0ZWRJZHgpID8gX3RoaXMuc3RhdGUuc2VsZWN0ZWRJZHggOiBbX3RoaXMuc3RhdGUuc2VsZWN0ZWRJZHhdO1xyXG5cclxuICAgICAgICB2YXIgaGFzU2VsZWN0ZWRJbmRleCA9ICQuaW5BcnJheShpbmRleCwgX3RoaXMuc3RhdGUuc2VsZWN0ZWRJZHgpO1xyXG4gICAgICAgIGlmICggaGFzU2VsZWN0ZWRJbmRleCAhPT0gLTEgKSB7XHJcbiAgICAgICAgICBfdGhpcy5zdGF0ZS5zZWxlY3RlZElkeC5zcGxpY2UoaGFzU2VsZWN0ZWRJbmRleCwgMSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIF90aGlzLnN0YXRlLnNlbGVjdGVkSWR4LnB1c2goaW5kZXgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJGZpbHRlcmVkTGlcclxuICAgICAgICAgIC5yZW1vdmVDbGFzcygnc2VsZWN0ZWQnKVxyXG4gICAgICAgICAgLmZpbHRlcihmdW5jdGlvbihpbmRleCkge1xyXG4gICAgICAgICAgICByZXR1cm4gJC5pbkFycmF5KGluZGV4LCBfdGhpcy5zdGF0ZS5zZWxlY3RlZElkeCkgIT09IC0xO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC5hZGRDbGFzcygnc2VsZWN0ZWQnKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAkZmlsdGVyZWRMaVxyXG4gICAgICAgICAgLnJlbW92ZUNsYXNzKCdzZWxlY3RlZCcpXHJcbiAgICAgICAgICAuZXEoX3RoaXMuc3RhdGUuc2VsZWN0ZWRJZHggPSBpbmRleClcclxuICAgICAgICAgIC5hZGRDbGFzcygnc2VsZWN0ZWQnKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCAhX3RoaXMuc3RhdGUubXVsdGlwbGUgfHwgIV90aGlzLm9wdGlvbnMubXVsdGlwbGUua2VlcE1lbnVPcGVuICkge1xyXG4gICAgICAgIF90aGlzLmNsb3NlKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIF90aGlzLmNoYW5nZSgpO1xyXG5cclxuICAgICAgX3RoaXMudXRpbHMudHJpZ2dlckNhbGxiYWNrKCdTZWxlY3QnLCBfdGhpcywgaW5kZXgpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFVuYmluZCBhbmQgcmVtb3ZlXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtib29sZWFufSBwcmVzZXJ2ZURhdGEgLSBDaGVjayBpZiB0aGUgZGF0YSBvbiB0aGUgZWxlbWVudCBzaG91bGQgYmUgcmVtb3ZlZCB0b29cclxuICAgICAqL1xyXG4gICAgZGVzdHJveTogZnVuY3Rpb24ocHJlc2VydmVEYXRhKSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICBpZiAoIF90aGlzLnN0YXRlICYmIF90aGlzLnN0YXRlLmVuYWJsZWQgKSB7XHJcbiAgICAgICAgX3RoaXMuZWxlbWVudHMuaXRlbXMuYWRkKF90aGlzLmVsZW1lbnRzLndyYXBwZXIpLmFkZChfdGhpcy5lbGVtZW50cy5pbnB1dCkucmVtb3ZlKCk7XHJcblxyXG4gICAgICAgIGlmICggIXByZXNlcnZlRGF0YSApIHtcclxuICAgICAgICAgIF90aGlzLiRlbGVtZW50LnJlbW92ZURhdGEocGx1Z2luTmFtZSkucmVtb3ZlRGF0YSgndmFsdWUnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF90aGlzLiRlbGVtZW50LnByb3AoJ3RhYmluZGV4JywgX3RoaXMub3JpZ2luYWxUYWJpbmRleCkub2ZmKGV2ZW50TmFtZXNwYWNlU3VmZml4KS5vZmYoX3RoaXMuZXZlbnRUcmlnZ2VycykudW53cmFwKCkudW53cmFwKCk7XHJcblxyXG4gICAgICAgIF90aGlzLnN0YXRlLmVuYWJsZWQgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIC8vIEEgcmVhbGx5IGxpZ2h0d2VpZ2h0IHBsdWdpbiB3cmFwcGVyIGFyb3VuZCB0aGUgY29uc3RydWN0b3IsXHJcbiAgLy8gcHJldmVudGluZyBhZ2FpbnN0IG11bHRpcGxlIGluc3RhbnRpYXRpb25zXHJcbiAgJC5mbltwbHVnaW5OYW1lXSA9IGZ1bmN0aW9uKGFyZ3MpIHtcclxuICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBkYXRhID0gJC5kYXRhKHRoaXMsIHBsdWdpbk5hbWUpO1xyXG5cclxuICAgICAgaWYgKCBkYXRhICYmICFkYXRhLmRpc2FibGVPbk1vYmlsZSApIHtcclxuICAgICAgICAodHlwZW9mIGFyZ3MgPT09ICdzdHJpbmcnICYmIGRhdGFbYXJnc10pID8gZGF0YVthcmdzXSgpIDogZGF0YS5pbml0KGFyZ3MpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICQuZGF0YSh0aGlzLCBwbHVnaW5OYW1lLCBuZXcgU2VsZWN0cmljKHRoaXMsIGFyZ3MpKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogRGVmYXVsdCBwbHVnaW4gb3B0aW9uc1xyXG4gICAqXHJcbiAgICogQHR5cGUge29iamVjdH1cclxuICAgKi9cclxuICAkLmZuW3BsdWdpbk5hbWVdLmRlZmF1bHRzID0ge1xyXG4gICAgb25DaGFuZ2UgICAgICAgICAgICAgOiBmdW5jdGlvbihlbG0pIHsgJChlbG0pLmNoYW5nZSgpOyB9LFxyXG4gICAgbWF4SGVpZ2h0ICAgICAgICAgICAgOiAzMDAsXHJcbiAgICBrZXlTZWFyY2hUaW1lb3V0ICAgICA6IDUwMCxcclxuICAgIGFycm93QnV0dG9uTWFya3VwICAgIDogJzxiIGNsYXNzPVwiYnV0dG9uXCI+JiN4MjViZTs8L2I+JyxcclxuICAgIGRpc2FibGVPbk1vYmlsZSAgICAgIDogZmFsc2UsXHJcbiAgICBuYXRpdmVPbk1vYmlsZSAgICAgICA6IHRydWUsXHJcbiAgICBvcGVuT25Gb2N1cyAgICAgICAgICA6IHRydWUsXHJcbiAgICBvcGVuT25Ib3ZlciAgICAgICAgICA6IGZhbHNlLFxyXG4gICAgaG92ZXJJbnRlbnRUaW1lb3V0ICAgOiA1MDAsXHJcbiAgICBleHBhbmRUb0l0ZW1UZXh0ICAgICA6IGZhbHNlLFxyXG4gICAgcmVzcG9uc2l2ZSAgICAgICAgICAgOiBmYWxzZSxcclxuICAgIHByZXZlbnRXaW5kb3dTY3JvbGwgIDogdHJ1ZSxcclxuICAgIGluaGVyaXRPcmlnaW5hbFdpZHRoIDogZmFsc2UsXHJcbiAgICBhbGxvd1dyYXAgICAgICAgICAgICA6IHRydWUsXHJcbiAgICBmb3JjZVJlbmRlckFib3ZlICAgICA6IGZhbHNlLFxyXG4gICAgZm9yY2VSZW5kZXJCZWxvdyAgICAgOiBmYWxzZSxcclxuICAgIHN0b3BQcm9wYWdhdGlvbiAgICAgIDogdHJ1ZSxcclxuICAgIG9wdGlvbnNJdGVtQnVpbGRlciAgIDogJ3t0ZXh0fScsIC8vIGZ1bmN0aW9uKGl0ZW1EYXRhLCBlbGVtZW50LCBpbmRleClcclxuICAgIGxhYmVsQnVpbGRlciAgICAgICAgIDogJ3t0ZXh0fScsIC8vIGZ1bmN0aW9uKGN1cnJJdGVtKVxyXG4gICAgbGlzdEJ1aWxkZXIgICAgICAgICAgOiBmYWxzZSwgICAgLy8gZnVuY3Rpb24oaXRlbXMpXHJcbiAgICBrZXlzICAgICAgICAgICAgICAgICA6IHtcclxuICAgICAgcHJldmlvdXMgOiBbMzcsIDM4XSwgICAgICAgICAgICAgICAgIC8vIExlZnQgLyBVcFxyXG4gICAgICBuZXh0ICAgICA6IFszOSwgNDBdLCAgICAgICAgICAgICAgICAgLy8gUmlnaHQgLyBEb3duXHJcbiAgICAgIHNlbGVjdCAgIDogWzksIDEzLCAyN10sICAgICAgICAgICAgICAvLyBUYWIgLyBFbnRlciAvIEVzY2FwZVxyXG4gICAgICBvcGVuICAgICA6IFsxMywgMzIsIDM3LCAzOCwgMzksIDQwXSwgLy8gRW50ZXIgLyBTcGFjZSAvIExlZnQgLyBVcCAvIFJpZ2h0IC8gRG93blxyXG4gICAgICBjbG9zZSAgICA6IFs5LCAyN10gICAgICAgICAgICAgICAgICAgLy8gVGFiIC8gRXNjYXBlXHJcbiAgICB9LFxyXG4gICAgY3VzdG9tQ2xhc3MgICAgICAgICAgOiB7XHJcbiAgICAgIHByZWZpeDogcGx1Z2luTmFtZSxcclxuICAgICAgY2FtZWxDYXNlOiBmYWxzZVxyXG4gICAgfSxcclxuICAgIG11bHRpcGxlICAgICAgICAgICAgICA6IHtcclxuICAgICAgc2VwYXJhdG9yOiAnLCAnLFxyXG4gICAgICBrZWVwTWVudU9wZW46IHRydWUsXHJcbiAgICAgIG1heExhYmVsRW50cmllczogZmFsc2VcclxuICAgIH1cclxuICB9O1xyXG59KSk7XHJcbiIsIi8qISBTZWxlY3RyaWMgz58gdjEuMTMuMCAoMjAxNy0wOC0yMikgLSBnaXQuaW8vdGpsOXNRIC0gQ29weXJpZ2h0IChjKSAyMDE3IExlb25hcmRvIFNhbnRvcyAtIE1JVCBMaWNlbnNlICovXHJcbiFmdW5jdGlvbihlKXtcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFtcImpxdWVyeVwiXSxlKTpcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlJiZtb2R1bGUuZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1mdW5jdGlvbih0LHMpe3JldHVybiB2b2lkIDA9PT1zJiYocz1cInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93P3JlcXVpcmUoXCJqcXVlcnlcIik6cmVxdWlyZShcImpxdWVyeVwiKSh0KSksZShzKSxzfTplKGpRdWVyeSl9KGZ1bmN0aW9uKGUpe1widXNlIHN0cmljdFwiO3ZhciB0PWUoZG9jdW1lbnQpLHM9ZSh3aW5kb3cpLGw9W1wiYVwiLFwiZVwiLFwiaVwiLFwib1wiLFwidVwiLFwiblwiLFwiY1wiLFwieVwiXSxpPVsvW1xceEUwLVxceEU1XS9nLC9bXFx4RTgtXFx4RUJdL2csL1tcXHhFQy1cXHhFRl0vZywvW1xceEYyLVxceEY2XS9nLC9bXFx4RjktXFx4RkNdL2csL1tcXHhGMV0vZywvW1xceEU3XS9nLC9bXFx4RkQtXFx4RkZdL2ddLG49ZnVuY3Rpb24odCxzKXt2YXIgbD10aGlzO2wuZWxlbWVudD10LGwuJGVsZW1lbnQ9ZSh0KSxsLnN0YXRlPXttdWx0aXBsZTohIWwuJGVsZW1lbnQuYXR0cihcIm11bHRpcGxlXCIpLGVuYWJsZWQ6ITEsb3BlbmVkOiExLGN1cnJWYWx1ZTotMSxzZWxlY3RlZElkeDotMSxoaWdobGlnaHRlZElkeDotMX0sbC5ldmVudFRyaWdnZXJzPXtvcGVuOmwub3BlbixjbG9zZTpsLmNsb3NlLGRlc3Ryb3k6bC5kZXN0cm95LHJlZnJlc2g6bC5yZWZyZXNoLGluaXQ6bC5pbml0fSxsLmluaXQocyl9O24ucHJvdG90eXBlPXt1dGlsczp7aXNNb2JpbGU6ZnVuY3Rpb24oKXtyZXR1cm4vYW5kcm9pZHxpcChob25lfG9kfGFkKS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCl9LGVzY2FwZVJlZ0V4cDpmdW5jdGlvbihlKXtyZXR1cm4gZS5yZXBsYWNlKC9bLiorP14ke30oKXxbXFxdXFxcXF0vZyxcIlxcXFwkJlwiKX0scmVwbGFjZURpYWNyaXRpY3M6ZnVuY3Rpb24oZSl7Zm9yKHZhciB0PWkubGVuZ3RoO3QtLTspZT1lLnRvTG93ZXJDYXNlKCkucmVwbGFjZShpW3RdLGxbdF0pO3JldHVybiBlfSxmb3JtYXQ6ZnVuY3Rpb24oZSl7dmFyIHQ9YXJndW1lbnRzO3JldHVybihcIlwiK2UpLnJlcGxhY2UoL1xceyg/OihcXGQrKXwoXFx3KykpXFx9L2csZnVuY3Rpb24oZSxzLGwpe3JldHVybiBsJiZ0WzFdP3RbMV1bbF06dFtzXX0pfSxuZXh0RW5hYmxlZEl0ZW06ZnVuY3Rpb24oZSx0KXtmb3IoO2VbdD0odCsxKSVlLmxlbmd0aF0uZGlzYWJsZWQ7KTtyZXR1cm4gdH0scHJldmlvdXNFbmFibGVkSXRlbTpmdW5jdGlvbihlLHQpe2Zvcig7ZVt0PSh0PjA/dDplLmxlbmd0aCktMV0uZGlzYWJsZWQ7KTtyZXR1cm4gdH0sdG9EYXNoOmZ1bmN0aW9uKGUpe3JldHVybiBlLnJlcGxhY2UoLyhbYS16MC05XSkoW0EtWl0pL2csXCIkMS0kMlwiKS50b0xvd2VyQ2FzZSgpfSx0cmlnZ2VyQ2FsbGJhY2s6ZnVuY3Rpb24odCxzKXt2YXIgbD1zLmVsZW1lbnQsaT1zLm9wdGlvbnNbXCJvblwiK3RdLG49W2xdLmNvbmNhdChbXS5zbGljZS5jYWxsKGFyZ3VtZW50cykuc2xpY2UoMSkpO2UuaXNGdW5jdGlvbihpKSYmaS5hcHBseShsLG4pLGUobCkudHJpZ2dlcihcInNlbGVjdHJpYy1cIit0aGlzLnRvRGFzaCh0KSxuKX0sYXJyYXlUb0NsYXNzbmFtZTpmdW5jdGlvbih0KXt2YXIgcz1lLmdyZXAodCxmdW5jdGlvbihlKXtyZXR1cm4hIWV9KTtyZXR1cm4gZS50cmltKHMuam9pbihcIiBcIikpfX0saW5pdDpmdW5jdGlvbih0KXt2YXIgcz10aGlzO2lmKHMub3B0aW9ucz1lLmV4dGVuZCghMCx7fSxlLmZuLnNlbGVjdHJpYy5kZWZhdWx0cyxzLm9wdGlvbnMsdCkscy51dGlscy50cmlnZ2VyQ2FsbGJhY2soXCJCZWZvcmVJbml0XCIscykscy5kZXN0cm95KCEwKSxzLm9wdGlvbnMuZGlzYWJsZU9uTW9iaWxlJiZzLnV0aWxzLmlzTW9iaWxlKCkpcmV0dXJuIHZvaWQocy5kaXNhYmxlT25Nb2JpbGU9ITApO3MuY2xhc3Nlcz1zLmdldENsYXNzTmFtZXMoKTt2YXIgbD1lKFwiPGlucHV0Lz5cIix7Y2xhc3M6cy5jbGFzc2VzLmlucHV0LHJlYWRvbmx5OnMudXRpbHMuaXNNb2JpbGUoKX0pLGk9ZShcIjxkaXYvPlwiLHtjbGFzczpzLmNsYXNzZXMuaXRlbXMsdGFiaW5kZXg6LTF9KSxuPWUoXCI8ZGl2Lz5cIix7Y2xhc3M6cy5jbGFzc2VzLnNjcm9sbH0pLGE9ZShcIjxkaXYvPlwiLHtjbGFzczpzLmNsYXNzZXMucHJlZml4LGh0bWw6cy5vcHRpb25zLmFycm93QnV0dG9uTWFya3VwfSksbz1lKFwiPHNwYW4vPlwiLHtjbGFzczpcImxhYmVsXCJ9KSxyPXMuJGVsZW1lbnQud3JhcChcIjxkaXYvPlwiKS5wYXJlbnQoKS5hcHBlbmQoYS5wcmVwZW5kKG8pLGksbCksdT1lKFwiPGRpdi8+XCIse2NsYXNzOnMuY2xhc3Nlcy5oaWRlc2VsZWN0fSk7cy5lbGVtZW50cz17aW5wdXQ6bCxpdGVtczppLGl0ZW1zU2Nyb2xsOm4sd3JhcHBlcjphLGxhYmVsOm8sb3V0ZXJXcmFwcGVyOnJ9LHMub3B0aW9ucy5uYXRpdmVPbk1vYmlsZSYmcy51dGlscy5pc01vYmlsZSgpJiYocy5lbGVtZW50cy5pbnB1dD12b2lkIDAsdS5hZGRDbGFzcyhzLmNsYXNzZXMucHJlZml4K1wiLWlzLW5hdGl2ZVwiKSxzLiRlbGVtZW50Lm9uKFwiY2hhbmdlXCIsZnVuY3Rpb24oKXtzLnJlZnJlc2goKX0pKSxzLiRlbGVtZW50Lm9uKHMuZXZlbnRUcmlnZ2Vycykud3JhcCh1KSxzLm9yaWdpbmFsVGFiaW5kZXg9cy4kZWxlbWVudC5wcm9wKFwidGFiaW5kZXhcIikscy4kZWxlbWVudC5wcm9wKFwidGFiaW5kZXhcIiwtMSkscy5wb3B1bGF0ZSgpLHMuYWN0aXZhdGUoKSxzLnV0aWxzLnRyaWdnZXJDYWxsYmFjayhcIkluaXRcIixzKX0sYWN0aXZhdGU6ZnVuY3Rpb24oKXt2YXIgZT10aGlzLHQ9ZS5lbGVtZW50cy5pdGVtcy5jbG9zZXN0KFwiOnZpc2libGVcIikuY2hpbGRyZW4oXCI6aGlkZGVuXCIpLmFkZENsYXNzKGUuY2xhc3Nlcy50ZW1wc2hvdykscz1lLiRlbGVtZW50LndpZHRoKCk7dC5yZW1vdmVDbGFzcyhlLmNsYXNzZXMudGVtcHNob3cpLGUudXRpbHMudHJpZ2dlckNhbGxiYWNrKFwiQmVmb3JlQWN0aXZhdGVcIixlKSxlLmVsZW1lbnRzLm91dGVyV3JhcHBlci5wcm9wKFwiY2xhc3NcIixlLnV0aWxzLmFycmF5VG9DbGFzc25hbWUoW2UuY2xhc3Nlcy53cmFwcGVyLGUuJGVsZW1lbnQucHJvcChcImNsYXNzXCIpLnJlcGxhY2UoL1xcUysvZyxlLmNsYXNzZXMucHJlZml4K1wiLSQmXCIpLGUub3B0aW9ucy5yZXNwb25zaXZlP2UuY2xhc3Nlcy5yZXNwb25zaXZlOlwiXCJdKSksZS5vcHRpb25zLmluaGVyaXRPcmlnaW5hbFdpZHRoJiZzPjAmJmUuZWxlbWVudHMub3V0ZXJXcmFwcGVyLndpZHRoKHMpLGUudW5iaW5kRXZlbnRzKCksZS4kZWxlbWVudC5wcm9wKFwiZGlzYWJsZWRcIik/KGUuZWxlbWVudHMub3V0ZXJXcmFwcGVyLmFkZENsYXNzKGUuY2xhc3Nlcy5kaXNhYmxlZCksZS5lbGVtZW50cy5pbnB1dCYmZS5lbGVtZW50cy5pbnB1dC5wcm9wKFwiZGlzYWJsZWRcIiwhMCkpOihlLnN0YXRlLmVuYWJsZWQ9ITAsZS5lbGVtZW50cy5vdXRlcldyYXBwZXIucmVtb3ZlQ2xhc3MoZS5jbGFzc2VzLmRpc2FibGVkKSxlLiRsaT1lLmVsZW1lbnRzLml0ZW1zLnJlbW92ZUF0dHIoXCJzdHlsZVwiKS5maW5kKFwibGlcIiksZS5iaW5kRXZlbnRzKCkpLGUudXRpbHMudHJpZ2dlckNhbGxiYWNrKFwiQWN0aXZhdGVcIixlKX0sZ2V0Q2xhc3NOYW1lczpmdW5jdGlvbigpe3ZhciB0PXRoaXMscz10Lm9wdGlvbnMuY3VzdG9tQ2xhc3MsbD17fTtyZXR1cm4gZS5lYWNoKFwiSW5wdXQgSXRlbXMgT3BlbiBEaXNhYmxlZCBUZW1wU2hvdyBIaWRlU2VsZWN0IFdyYXBwZXIgRm9jdXMgSG92ZXIgUmVzcG9uc2l2ZSBBYm92ZSBCZWxvdyBTY3JvbGwgR3JvdXAgR3JvdXBMYWJlbFwiLnNwbGl0KFwiIFwiKSxmdW5jdGlvbihlLGkpe3ZhciBuPXMucHJlZml4K2k7bFtpLnRvTG93ZXJDYXNlKCldPXMuY2FtZWxDYXNlP246dC51dGlscy50b0Rhc2gobil9KSxsLnByZWZpeD1zLnByZWZpeCxsfSxzZXRMYWJlbDpmdW5jdGlvbigpe3ZhciB0PXRoaXMscz10Lm9wdGlvbnMubGFiZWxCdWlsZGVyO2lmKHQuc3RhdGUubXVsdGlwbGUpe3ZhciBsPWUuaXNBcnJheSh0LnN0YXRlLmN1cnJWYWx1ZSk/dC5zdGF0ZS5jdXJyVmFsdWU6W3Quc3RhdGUuY3VyclZhbHVlXTtsPTA9PT1sLmxlbmd0aD9bMF06bDt2YXIgaT1lLm1hcChsLGZ1bmN0aW9uKHMpe3JldHVybiBlLmdyZXAodC5sb29rdXBJdGVtcyxmdW5jdGlvbihlKXtyZXR1cm4gZS5pbmRleD09PXN9KVswXX0pO2k9ZS5ncmVwKGksZnVuY3Rpb24odCl7cmV0dXJuIGkubGVuZ3RoPjF8fDA9PT1pLmxlbmd0aD9cIlwiIT09ZS50cmltKHQudmFsdWUpOnR9KSxpPWUubWFwKGksZnVuY3Rpb24obCl7cmV0dXJuIGUuaXNGdW5jdGlvbihzKT9zKGwpOnQudXRpbHMuZm9ybWF0KHMsbCl9KSx0Lm9wdGlvbnMubXVsdGlwbGUubWF4TGFiZWxFbnRyaWVzJiYoaS5sZW5ndGg+PXQub3B0aW9ucy5tdWx0aXBsZS5tYXhMYWJlbEVudHJpZXMrMT8oaT1pLnNsaWNlKDAsdC5vcHRpb25zLm11bHRpcGxlLm1heExhYmVsRW50cmllcyksaS5wdXNoKGUuaXNGdW5jdGlvbihzKT9zKHt0ZXh0OlwiLi4uXCJ9KTp0LnV0aWxzLmZvcm1hdChzLHt0ZXh0OlwiLi4uXCJ9KSkpOmkuc2xpY2UoaS5sZW5ndGgtMSkpLHQuZWxlbWVudHMubGFiZWwuaHRtbChpLmpvaW4odC5vcHRpb25zLm11bHRpcGxlLnNlcGFyYXRvcikpfWVsc2V7dmFyIG49dC5sb29rdXBJdGVtc1t0LnN0YXRlLmN1cnJWYWx1ZV07dC5lbGVtZW50cy5sYWJlbC5odG1sKGUuaXNGdW5jdGlvbihzKT9zKG4pOnQudXRpbHMuZm9ybWF0KHMsbikpfX0scG9wdWxhdGU6ZnVuY3Rpb24oKXt2YXIgdD10aGlzLHM9dC4kZWxlbWVudC5jaGlsZHJlbigpLGw9dC4kZWxlbWVudC5maW5kKFwib3B0aW9uXCIpLGk9bC5maWx0ZXIoXCI6c2VsZWN0ZWRcIiksbj1sLmluZGV4KGkpLGE9MCxvPXQuc3RhdGUubXVsdGlwbGU/W106MDtpLmxlbmd0aD4xJiZ0LnN0YXRlLm11bHRpcGxlJiYobj1bXSxpLmVhY2goZnVuY3Rpb24oKXtuLnB1c2goZSh0aGlzKS5pbmRleCgpKX0pKSx0LnN0YXRlLmN1cnJWYWx1ZT1+bj9uOm8sdC5zdGF0ZS5zZWxlY3RlZElkeD10LnN0YXRlLmN1cnJWYWx1ZSx0LnN0YXRlLmhpZ2hsaWdodGVkSWR4PXQuc3RhdGUuY3VyclZhbHVlLHQuaXRlbXM9W10sdC5sb29rdXBJdGVtcz1bXSxzLmxlbmd0aCYmKHMuZWFjaChmdW5jdGlvbihzKXt2YXIgbD1lKHRoaXMpO2lmKGwuaXMoXCJvcHRncm91cFwiKSl7dmFyIGk9e2VsZW1lbnQ6bCxsYWJlbDpsLnByb3AoXCJsYWJlbFwiKSxncm91cERpc2FibGVkOmwucHJvcChcImRpc2FibGVkXCIpLGl0ZW1zOltdfTtsLmNoaWxkcmVuKCkuZWFjaChmdW5jdGlvbihzKXt2YXIgbD1lKHRoaXMpO2kuaXRlbXNbc109dC5nZXRJdGVtRGF0YShhLGwsaS5ncm91cERpc2FibGVkfHxsLnByb3AoXCJkaXNhYmxlZFwiKSksdC5sb29rdXBJdGVtc1thXT1pLml0ZW1zW3NdLGErK30pLHQuaXRlbXNbc109aX1lbHNlIHQuaXRlbXNbc109dC5nZXRJdGVtRGF0YShhLGwsbC5wcm9wKFwiZGlzYWJsZWRcIikpLHQubG9va3VwSXRlbXNbYV09dC5pdGVtc1tzXSxhKyt9KSx0LnNldExhYmVsKCksdC5lbGVtZW50cy5pdGVtcy5hcHBlbmQodC5lbGVtZW50cy5pdGVtc1Njcm9sbC5odG1sKHQuZ2V0SXRlbXNNYXJrdXAodC5pdGVtcykpKSl9LGdldEl0ZW1EYXRhOmZ1bmN0aW9uKHQscyxsKXt2YXIgaT10aGlzO3JldHVybntpbmRleDp0LGVsZW1lbnQ6cyx2YWx1ZTpzLnZhbCgpLGNsYXNzTmFtZTpzLnByb3AoXCJjbGFzc1wiKSx0ZXh0OnMuaHRtbCgpLHNsdWc6ZS50cmltKGkudXRpbHMucmVwbGFjZURpYWNyaXRpY3Mocy5odG1sKCkpKSxhbHQ6cy5hdHRyKFwiZGF0YS1hbHRcIiksc2VsZWN0ZWQ6cy5wcm9wKFwic2VsZWN0ZWRcIiksZGlzYWJsZWQ6bH19LGdldEl0ZW1zTWFya3VwOmZ1bmN0aW9uKHQpe3ZhciBzPXRoaXMsbD1cIjx1bD5cIjtyZXR1cm4gZS5pc0Z1bmN0aW9uKHMub3B0aW9ucy5saXN0QnVpbGRlcikmJnMub3B0aW9ucy5saXN0QnVpbGRlciYmKHQ9cy5vcHRpb25zLmxpc3RCdWlsZGVyKHQpKSxlLmVhY2godCxmdW5jdGlvbih0LGkpe3ZvaWQgMCE9PWkubGFiZWw/KGwrPXMudXRpbHMuZm9ybWF0KCc8dWwgY2xhc3M9XCJ7MX1cIj48bGkgY2xhc3M9XCJ7Mn1cIj57M308L2xpPicscy51dGlscy5hcnJheVRvQ2xhc3NuYW1lKFtzLmNsYXNzZXMuZ3JvdXAsaS5ncm91cERpc2FibGVkP1wiZGlzYWJsZWRcIjpcIlwiLGkuZWxlbWVudC5wcm9wKFwiY2xhc3NcIildKSxzLmNsYXNzZXMuZ3JvdXBsYWJlbCxpLmVsZW1lbnQucHJvcChcImxhYmVsXCIpKSxlLmVhY2goaS5pdGVtcyxmdW5jdGlvbihlLHQpe2wrPXMuZ2V0SXRlbU1hcmt1cCh0LmluZGV4LHQpfSksbCs9XCI8L3VsPlwiKTpsKz1zLmdldEl0ZW1NYXJrdXAoaS5pbmRleCxpKX0pLGwrXCI8L3VsPlwifSxnZXRJdGVtTWFya3VwOmZ1bmN0aW9uKHQscyl7dmFyIGw9dGhpcyxpPWwub3B0aW9ucy5vcHRpb25zSXRlbUJ1aWxkZXIsbj17dmFsdWU6cy52YWx1ZSx0ZXh0OnMudGV4dCxzbHVnOnMuc2x1ZyxpbmRleDpzLmluZGV4fTtyZXR1cm4gbC51dGlscy5mb3JtYXQoJzxsaSBkYXRhLWluZGV4PVwiezF9XCIgY2xhc3M9XCJ7Mn1cIj57M308L2xpPicsdCxsLnV0aWxzLmFycmF5VG9DbGFzc25hbWUoW3MuY2xhc3NOYW1lLHQ9PT1sLml0ZW1zLmxlbmd0aC0xP1wibGFzdFwiOlwiXCIscy5kaXNhYmxlZD9cImRpc2FibGVkXCI6XCJcIixzLnNlbGVjdGVkP1wic2VsZWN0ZWRcIjpcIlwiXSksZS5pc0Z1bmN0aW9uKGkpP2wudXRpbHMuZm9ybWF0KGkocyx0aGlzLiRlbGVtZW50LHQpLHMpOmwudXRpbHMuZm9ybWF0KGksbikpfSx1bmJpbmRFdmVudHM6ZnVuY3Rpb24oKXt2YXIgZT10aGlzO2UuZWxlbWVudHMud3JhcHBlci5hZGQoZS4kZWxlbWVudCkuYWRkKGUuZWxlbWVudHMub3V0ZXJXcmFwcGVyKS5hZGQoZS5lbGVtZW50cy5pbnB1dCkub2ZmKFwiLnNsXCIpfSxiaW5kRXZlbnRzOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpczt0LmVsZW1lbnRzLm91dGVyV3JhcHBlci5vbihcIm1vdXNlZW50ZXIuc2wgbW91c2VsZWF2ZS5zbFwiLGZ1bmN0aW9uKHMpe2UodGhpcykudG9nZ2xlQ2xhc3ModC5jbGFzc2VzLmhvdmVyLFwibW91c2VlbnRlclwiPT09cy50eXBlKSx0Lm9wdGlvbnMub3Blbk9uSG92ZXImJihjbGVhclRpbWVvdXQodC5jbG9zZVRpbWVyKSxcIm1vdXNlbGVhdmVcIj09PXMudHlwZT90LmNsb3NlVGltZXI9c2V0VGltZW91dChlLnByb3h5KHQuY2xvc2UsdCksdC5vcHRpb25zLmhvdmVySW50ZW50VGltZW91dCk6dC5vcGVuKCkpfSksdC5lbGVtZW50cy53cmFwcGVyLm9uKFwiY2xpY2suc2xcIixmdW5jdGlvbihlKXt0LnN0YXRlLm9wZW5lZD90LmNsb3NlKCk6dC5vcGVuKGUpfSksdC5vcHRpb25zLm5hdGl2ZU9uTW9iaWxlJiZ0LnV0aWxzLmlzTW9iaWxlKCl8fCh0LiRlbGVtZW50Lm9uKFwiZm9jdXMuc2xcIixmdW5jdGlvbigpe3QuZWxlbWVudHMuaW5wdXQuZm9jdXMoKX0pLHQuZWxlbWVudHMuaW5wdXQucHJvcCh7dGFiaW5kZXg6dC5vcmlnaW5hbFRhYmluZGV4LGRpc2FibGVkOiExfSkub24oXCJrZXlkb3duLnNsXCIsZS5wcm94eSh0LmhhbmRsZUtleXMsdCkpLm9uKFwiZm9jdXNpbi5zbFwiLGZ1bmN0aW9uKGUpe3QuZWxlbWVudHMub3V0ZXJXcmFwcGVyLmFkZENsYXNzKHQuY2xhc3Nlcy5mb2N1cyksdC5lbGVtZW50cy5pbnB1dC5vbmUoXCJibHVyXCIsZnVuY3Rpb24oKXt0LmVsZW1lbnRzLmlucHV0LmJsdXIoKX0pLHQub3B0aW9ucy5vcGVuT25Gb2N1cyYmIXQuc3RhdGUub3BlbmVkJiZ0Lm9wZW4oZSl9KS5vbihcImZvY3Vzb3V0LnNsXCIsZnVuY3Rpb24oKXt0LmVsZW1lbnRzLm91dGVyV3JhcHBlci5yZW1vdmVDbGFzcyh0LmNsYXNzZXMuZm9jdXMpfSkub24oXCJpbnB1dCBwcm9wZXJ0eWNoYW5nZVwiLGZ1bmN0aW9uKCl7dmFyIHM9dC5lbGVtZW50cy5pbnB1dC52YWwoKSxsPW5ldyBSZWdFeHAoXCJeXCIrdC51dGlscy5lc2NhcGVSZWdFeHAocyksXCJpXCIpO2NsZWFyVGltZW91dCh0LnJlc2V0U3RyKSx0LnJlc2V0U3RyPXNldFRpbWVvdXQoZnVuY3Rpb24oKXt0LmVsZW1lbnRzLmlucHV0LnZhbChcIlwiKX0sdC5vcHRpb25zLmtleVNlYXJjaFRpbWVvdXQpLHMubGVuZ3RoJiZlLmVhY2godC5pdGVtcyxmdW5jdGlvbihlLHMpe2lmKCFzLmRpc2FibGVkKXtpZihsLnRlc3Qocy50ZXh0KXx8bC50ZXN0KHMuc2x1ZykpcmV0dXJuIHZvaWQgdC5oaWdobGlnaHQoZSk7aWYocy5hbHQpZm9yKHZhciBpPXMuYWx0LnNwbGl0KFwifFwiKSxuPTA7bjxpLmxlbmd0aCYmaVtuXTtuKyspaWYobC50ZXN0KGlbbl0udHJpbSgpKSlyZXR1cm4gdm9pZCB0LmhpZ2hsaWdodChlKX19KX0pKSx0LiRsaS5vbih7bW91c2Vkb3duOmZ1bmN0aW9uKGUpe2UucHJldmVudERlZmF1bHQoKSxlLnN0b3BQcm9wYWdhdGlvbigpfSxjbGljazpmdW5jdGlvbigpe3JldHVybiB0LnNlbGVjdChlKHRoaXMpLmRhdGEoXCJpbmRleFwiKSksITF9fSl9LGhhbmRsZUtleXM6ZnVuY3Rpb24odCl7dmFyIHM9dGhpcyxsPXQud2hpY2gsaT1zLm9wdGlvbnMua2V5cyxuPWUuaW5BcnJheShsLGkucHJldmlvdXMpPi0xLGE9ZS5pbkFycmF5KGwsaS5uZXh0KT4tMSxvPWUuaW5BcnJheShsLGkuc2VsZWN0KT4tMSxyPWUuaW5BcnJheShsLGkub3Blbik+LTEsdT1zLnN0YXRlLmhpZ2hsaWdodGVkSWR4LHA9biYmMD09PXV8fGEmJnUrMT09PXMuaXRlbXMubGVuZ3RoLGM9MDtpZigxMyE9PWwmJjMyIT09bHx8dC5wcmV2ZW50RGVmYXVsdCgpLG58fGEpe2lmKCFzLm9wdGlvbnMuYWxsb3dXcmFwJiZwKXJldHVybjtuJiYoYz1zLnV0aWxzLnByZXZpb3VzRW5hYmxlZEl0ZW0ocy5sb29rdXBJdGVtcyx1KSksYSYmKGM9cy51dGlscy5uZXh0RW5hYmxlZEl0ZW0ocy5sb29rdXBJdGVtcyx1KSkscy5oaWdobGlnaHQoYyl9aWYobyYmcy5zdGF0ZS5vcGVuZWQpcmV0dXJuIHMuc2VsZWN0KHUpLHZvaWQocy5zdGF0ZS5tdWx0aXBsZSYmcy5vcHRpb25zLm11bHRpcGxlLmtlZXBNZW51T3Blbnx8cy5jbG9zZSgpKTtyJiYhcy5zdGF0ZS5vcGVuZWQmJnMub3BlbigpfSxyZWZyZXNoOmZ1bmN0aW9uKCl7dmFyIGU9dGhpcztlLnBvcHVsYXRlKCksZS5hY3RpdmF0ZSgpLGUudXRpbHMudHJpZ2dlckNhbGxiYWNrKFwiUmVmcmVzaFwiLGUpfSxzZXRPcHRpb25zRGltZW5zaW9uczpmdW5jdGlvbigpe3ZhciBlPXRoaXMsdD1lLmVsZW1lbnRzLml0ZW1zLmNsb3Nlc3QoXCI6dmlzaWJsZVwiKS5jaGlsZHJlbihcIjpoaWRkZW5cIikuYWRkQ2xhc3MoZS5jbGFzc2VzLnRlbXBzaG93KSxzPWUub3B0aW9ucy5tYXhIZWlnaHQsbD1lLmVsZW1lbnRzLml0ZW1zLm91dGVyV2lkdGgoKSxpPWUuZWxlbWVudHMud3JhcHBlci5vdXRlcldpZHRoKCktKGwtZS5lbGVtZW50cy5pdGVtcy53aWR0aCgpKTshZS5vcHRpb25zLmV4cGFuZFRvSXRlbVRleHR8fGk+bD9lLmZpbmFsV2lkdGg9aTooZS5lbGVtZW50cy5pdGVtcy5jc3MoXCJvdmVyZmxvd1wiLFwic2Nyb2xsXCIpLGUuZWxlbWVudHMub3V0ZXJXcmFwcGVyLndpZHRoKDllNCksZS5maW5hbFdpZHRoPWUuZWxlbWVudHMuaXRlbXMud2lkdGgoKSxlLmVsZW1lbnRzLml0ZW1zLmNzcyhcIm92ZXJmbG93XCIsXCJcIiksZS5lbGVtZW50cy5vdXRlcldyYXBwZXIud2lkdGgoXCJcIikpLGUuZWxlbWVudHMuaXRlbXMud2lkdGgoZS5maW5hbFdpZHRoKS5oZWlnaHQoKT5zJiZlLmVsZW1lbnRzLml0ZW1zLmhlaWdodChzKSx0LnJlbW92ZUNsYXNzKGUuY2xhc3Nlcy50ZW1wc2hvdyl9LGlzSW5WaWV3cG9ydDpmdW5jdGlvbigpe3ZhciBlPXRoaXM7aWYoITA9PT1lLm9wdGlvbnMuZm9yY2VSZW5kZXJBYm92ZSllLmVsZW1lbnRzLm91dGVyV3JhcHBlci5hZGRDbGFzcyhlLmNsYXNzZXMuYWJvdmUpO2Vsc2UgaWYoITA9PT1lLm9wdGlvbnMuZm9yY2VSZW5kZXJCZWxvdyllLmVsZW1lbnRzLm91dGVyV3JhcHBlci5hZGRDbGFzcyhlLmNsYXNzZXMuYmVsb3cpO2Vsc2V7dmFyIHQ9cy5zY3JvbGxUb3AoKSxsPXMuaGVpZ2h0KCksaT1lLmVsZW1lbnRzLm91dGVyV3JhcHBlci5vZmZzZXQoKS50b3Asbj1lLmVsZW1lbnRzLm91dGVyV3JhcHBlci5vdXRlckhlaWdodCgpLGE9aStuK2UuaXRlbXNIZWlnaHQ8PXQrbCxvPWktZS5pdGVtc0hlaWdodD50LHI9IWEmJm8sdT0hcjtlLmVsZW1lbnRzLm91dGVyV3JhcHBlci50b2dnbGVDbGFzcyhlLmNsYXNzZXMuYWJvdmUsciksZS5lbGVtZW50cy5vdXRlcldyYXBwZXIudG9nZ2xlQ2xhc3MoZS5jbGFzc2VzLmJlbG93LHUpfX0sZGV0ZWN0SXRlbVZpc2liaWxpdHk6ZnVuY3Rpb24odCl7dmFyIHM9dGhpcyxsPXMuJGxpLmZpbHRlcihcIltkYXRhLWluZGV4XVwiKTtzLnN0YXRlLm11bHRpcGxlJiYodD1lLmlzQXJyYXkodCkmJjA9PT10Lmxlbmd0aD8wOnQsdD1lLmlzQXJyYXkodCk/TWF0aC5taW4uYXBwbHkoTWF0aCx0KTp0KTt2YXIgaT1sLmVxKHQpLm91dGVySGVpZ2h0KCksbj1sW3RdLm9mZnNldFRvcCxhPXMuZWxlbWVudHMuaXRlbXNTY3JvbGwuc2Nyb2xsVG9wKCksbz1uKzIqaTtzLmVsZW1lbnRzLml0ZW1zU2Nyb2xsLnNjcm9sbFRvcChvPmErcy5pdGVtc0hlaWdodD9vLXMuaXRlbXNIZWlnaHQ6bi1pPGE/bi1pOmEpfSxvcGVuOmZ1bmN0aW9uKHMpe3ZhciBsPXRoaXM7aWYobC5vcHRpb25zLm5hdGl2ZU9uTW9iaWxlJiZsLnV0aWxzLmlzTW9iaWxlKCkpcmV0dXJuITE7bC51dGlscy50cmlnZ2VyQ2FsbGJhY2soXCJCZWZvcmVPcGVuXCIsbCkscyYmKHMucHJldmVudERlZmF1bHQoKSxsLm9wdGlvbnMuc3RvcFByb3BhZ2F0aW9uJiZzLnN0b3BQcm9wYWdhdGlvbigpKSxsLnN0YXRlLmVuYWJsZWQmJihsLnNldE9wdGlvbnNEaW1lbnNpb25zKCksZShcIi5cIitsLmNsYXNzZXMuaGlkZXNlbGVjdCxcIi5cIitsLmNsYXNzZXMub3BlbikuY2hpbGRyZW4oKS5zZWxlY3RyaWMoXCJjbG9zZVwiKSxsLnN0YXRlLm9wZW5lZD0hMCxsLml0ZW1zSGVpZ2h0PWwuZWxlbWVudHMuaXRlbXMub3V0ZXJIZWlnaHQoKSxsLml0ZW1zSW5uZXJIZWlnaHQ9bC5lbGVtZW50cy5pdGVtcy5oZWlnaHQoKSxsLmVsZW1lbnRzLm91dGVyV3JhcHBlci5hZGRDbGFzcyhsLmNsYXNzZXMub3BlbiksbC5lbGVtZW50cy5pbnB1dC52YWwoXCJcIikscyYmXCJmb2N1c2luXCIhPT1zLnR5cGUmJmwuZWxlbWVudHMuaW5wdXQuZm9jdXMoKSxzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7dC5vbihcImNsaWNrLnNsXCIsZS5wcm94eShsLmNsb3NlLGwpKS5vbihcInNjcm9sbC5zbFwiLGUucHJveHkobC5pc0luVmlld3BvcnQsbCkpfSwxKSxsLmlzSW5WaWV3cG9ydCgpLGwub3B0aW9ucy5wcmV2ZW50V2luZG93U2Nyb2xsJiZ0Lm9uKFwibW91c2V3aGVlbC5zbCBET01Nb3VzZVNjcm9sbC5zbFwiLFwiLlwiK2wuY2xhc3Nlcy5zY3JvbGwsZnVuY3Rpb24odCl7dmFyIHM9dC5vcmlnaW5hbEV2ZW50LGk9ZSh0aGlzKS5zY3JvbGxUb3AoKSxuPTA7XCJkZXRhaWxcImluIHMmJihuPS0xKnMuZGV0YWlsKSxcIndoZWVsRGVsdGFcImluIHMmJihuPXMud2hlZWxEZWx0YSksXCJ3aGVlbERlbHRhWVwiaW4gcyYmKG49cy53aGVlbERlbHRhWSksXCJkZWx0YVlcImluIHMmJihuPS0xKnMuZGVsdGFZKSwoaT09PXRoaXMuc2Nyb2xsSGVpZ2h0LWwuaXRlbXNJbm5lckhlaWdodCYmbjwwfHwwPT09aSYmbj4wKSYmdC5wcmV2ZW50RGVmYXVsdCgpfSksbC5kZXRlY3RJdGVtVmlzaWJpbGl0eShsLnN0YXRlLnNlbGVjdGVkSWR4KSxsLmhpZ2hsaWdodChsLnN0YXRlLm11bHRpcGxlPy0xOmwuc3RhdGUuc2VsZWN0ZWRJZHgpLGwudXRpbHMudHJpZ2dlckNhbGxiYWNrKFwiT3BlblwiLGwpKX0sY2xvc2U6ZnVuY3Rpb24oKXt2YXIgZT10aGlzO2UudXRpbHMudHJpZ2dlckNhbGxiYWNrKFwiQmVmb3JlQ2xvc2VcIixlKSx0Lm9mZihcIi5zbFwiKSxlLmVsZW1lbnRzLm91dGVyV3JhcHBlci5yZW1vdmVDbGFzcyhlLmNsYXNzZXMub3BlbiksZS5zdGF0ZS5vcGVuZWQ9ITEsZS51dGlscy50cmlnZ2VyQ2FsbGJhY2soXCJDbG9zZVwiLGUpfSxjaGFuZ2U6ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3QudXRpbHMudHJpZ2dlckNhbGxiYWNrKFwiQmVmb3JlQ2hhbmdlXCIsdCksdC5zdGF0ZS5tdWx0aXBsZT8oZS5lYWNoKHQubG9va3VwSXRlbXMsZnVuY3Rpb24oZSl7dC5sb29rdXBJdGVtc1tlXS5zZWxlY3RlZD0hMSx0LiRlbGVtZW50LmZpbmQoXCJvcHRpb25cIikucHJvcChcInNlbGVjdGVkXCIsITEpfSksZS5lYWNoKHQuc3RhdGUuc2VsZWN0ZWRJZHgsZnVuY3Rpb24oZSxzKXt0Lmxvb2t1cEl0ZW1zW3NdLnNlbGVjdGVkPSEwLHQuJGVsZW1lbnQuZmluZChcIm9wdGlvblwiKS5lcShzKS5wcm9wKFwic2VsZWN0ZWRcIiwhMCl9KSx0LnN0YXRlLmN1cnJWYWx1ZT10LnN0YXRlLnNlbGVjdGVkSWR4LHQuc2V0TGFiZWwoKSx0LnV0aWxzLnRyaWdnZXJDYWxsYmFjayhcIkNoYW5nZVwiLHQpKTp0LnN0YXRlLmN1cnJWYWx1ZSE9PXQuc3RhdGUuc2VsZWN0ZWRJZHgmJih0LiRlbGVtZW50LnByb3AoXCJzZWxlY3RlZEluZGV4XCIsdC5zdGF0ZS5jdXJyVmFsdWU9dC5zdGF0ZS5zZWxlY3RlZElkeCkuZGF0YShcInZhbHVlXCIsdC5sb29rdXBJdGVtc1t0LnN0YXRlLnNlbGVjdGVkSWR4XS50ZXh0KSx0LnNldExhYmVsKCksdC51dGlscy50cmlnZ2VyQ2FsbGJhY2soXCJDaGFuZ2VcIix0KSl9LGhpZ2hsaWdodDpmdW5jdGlvbihlKXt2YXIgdD10aGlzLHM9dC4kbGkuZmlsdGVyKFwiW2RhdGEtaW5kZXhdXCIpLnJlbW92ZUNsYXNzKFwiaGlnaGxpZ2h0ZWRcIik7dC51dGlscy50cmlnZ2VyQ2FsbGJhY2soXCJCZWZvcmVIaWdobGlnaHRcIix0KSx2b2lkIDA9PT1lfHwtMT09PWV8fHQubG9va3VwSXRlbXNbZV0uZGlzYWJsZWR8fChzLmVxKHQuc3RhdGUuaGlnaGxpZ2h0ZWRJZHg9ZSkuYWRkQ2xhc3MoXCJoaWdobGlnaHRlZFwiKSx0LmRldGVjdEl0ZW1WaXNpYmlsaXR5KGUpLHQudXRpbHMudHJpZ2dlckNhbGxiYWNrKFwiSGlnaGxpZ2h0XCIsdCkpfSxzZWxlY3Q6ZnVuY3Rpb24odCl7dmFyIHM9dGhpcyxsPXMuJGxpLmZpbHRlcihcIltkYXRhLWluZGV4XVwiKTtpZihzLnV0aWxzLnRyaWdnZXJDYWxsYmFjayhcIkJlZm9yZVNlbGVjdFwiLHMsdCksdm9pZCAwIT09dCYmLTEhPT10JiYhcy5sb29rdXBJdGVtc1t0XS5kaXNhYmxlZCl7aWYocy5zdGF0ZS5tdWx0aXBsZSl7cy5zdGF0ZS5zZWxlY3RlZElkeD1lLmlzQXJyYXkocy5zdGF0ZS5zZWxlY3RlZElkeCk/cy5zdGF0ZS5zZWxlY3RlZElkeDpbcy5zdGF0ZS5zZWxlY3RlZElkeF07dmFyIGk9ZS5pbkFycmF5KHQscy5zdGF0ZS5zZWxlY3RlZElkeCk7LTEhPT1pP3Muc3RhdGUuc2VsZWN0ZWRJZHguc3BsaWNlKGksMSk6cy5zdGF0ZS5zZWxlY3RlZElkeC5wdXNoKHQpLGwucmVtb3ZlQ2xhc3MoXCJzZWxlY3RlZFwiKS5maWx0ZXIoZnVuY3Rpb24odCl7cmV0dXJuLTEhPT1lLmluQXJyYXkodCxzLnN0YXRlLnNlbGVjdGVkSWR4KX0pLmFkZENsYXNzKFwic2VsZWN0ZWRcIil9ZWxzZSBsLnJlbW92ZUNsYXNzKFwic2VsZWN0ZWRcIikuZXEocy5zdGF0ZS5zZWxlY3RlZElkeD10KS5hZGRDbGFzcyhcInNlbGVjdGVkXCIpO3Muc3RhdGUubXVsdGlwbGUmJnMub3B0aW9ucy5tdWx0aXBsZS5rZWVwTWVudU9wZW58fHMuY2xvc2UoKSxzLmNoYW5nZSgpLHMudXRpbHMudHJpZ2dlckNhbGxiYWNrKFwiU2VsZWN0XCIscyx0KX19LGRlc3Ryb3k6ZnVuY3Rpb24oZSl7dmFyIHQ9dGhpczt0LnN0YXRlJiZ0LnN0YXRlLmVuYWJsZWQmJih0LmVsZW1lbnRzLml0ZW1zLmFkZCh0LmVsZW1lbnRzLndyYXBwZXIpLmFkZCh0LmVsZW1lbnRzLmlucHV0KS5yZW1vdmUoKSxlfHx0LiRlbGVtZW50LnJlbW92ZURhdGEoXCJzZWxlY3RyaWNcIikucmVtb3ZlRGF0YShcInZhbHVlXCIpLHQuJGVsZW1lbnQucHJvcChcInRhYmluZGV4XCIsdC5vcmlnaW5hbFRhYmluZGV4KS5vZmYoXCIuc2xcIikub2ZmKHQuZXZlbnRUcmlnZ2VycykudW53cmFwKCkudW53cmFwKCksdC5zdGF0ZS5lbmFibGVkPSExKX19LGUuZm4uc2VsZWN0cmljPWZ1bmN0aW9uKHQpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXt2YXIgcz1lLmRhdGEodGhpcyxcInNlbGVjdHJpY1wiKTtzJiYhcy5kaXNhYmxlT25Nb2JpbGU/XCJzdHJpbmdcIj09dHlwZW9mIHQmJnNbdF0/c1t0XSgpOnMuaW5pdCh0KTplLmRhdGEodGhpcyxcInNlbGVjdHJpY1wiLG5ldyBuKHRoaXMsdCkpfSl9LGUuZm4uc2VsZWN0cmljLmRlZmF1bHRzPXtvbkNoYW5nZTpmdW5jdGlvbih0KXtlKHQpLmNoYW5nZSgpfSxtYXhIZWlnaHQ6MzAwLGtleVNlYXJjaFRpbWVvdXQ6NTAwLGFycm93QnV0dG9uTWFya3VwOic8YiBjbGFzcz1cImJ1dHRvblwiPiYjeDI1YmU7PC9iPicsZGlzYWJsZU9uTW9iaWxlOiExLG5hdGl2ZU9uTW9iaWxlOiEwLG9wZW5PbkZvY3VzOiEwLG9wZW5PbkhvdmVyOiExLGhvdmVySW50ZW50VGltZW91dDo1MDAsZXhwYW5kVG9JdGVtVGV4dDohMSxyZXNwb25zaXZlOiExLHByZXZlbnRXaW5kb3dTY3JvbGw6ITAsaW5oZXJpdE9yaWdpbmFsV2lkdGg6ITEsYWxsb3dXcmFwOiEwLGZvcmNlUmVuZGVyQWJvdmU6ITEsZm9yY2VSZW5kZXJCZWxvdzohMSxzdG9wUHJvcGFnYXRpb246ITAsb3B0aW9uc0l0ZW1CdWlsZGVyOlwie3RleHR9XCIsbGFiZWxCdWlsZGVyOlwie3RleHR9XCIsbGlzdEJ1aWxkZXI6ITEsa2V5czp7cHJldmlvdXM6WzM3LDM4XSxuZXh0OlszOSw0MF0sc2VsZWN0Ols5LDEzLDI3XSxvcGVuOlsxMywzMiwzNywzOCwzOSw0MF0sY2xvc2U6WzksMjddfSxjdXN0b21DbGFzczp7cHJlZml4Olwic2VsZWN0cmljXCIsY2FtZWxDYXNlOiExfSxtdWx0aXBsZTp7c2VwYXJhdG9yOlwiLCBcIixrZWVwTWVudU9wZW46ITAsbWF4TGFiZWxFbnRyaWVzOiExfX19KTsiLCIvKiEganMtY29va2llIHYzLjAuMC1iZXRhLjMgfCBNSVQgKi9cclxuIWZ1bmN0aW9uKGUsdCl7XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHMmJlwidW5kZWZpbmVkXCIhPXR5cGVvZiBtb2R1bGU/bW9kdWxlLmV4cG9ydHM9dCgpOlwiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUodCk6KGU9ZXx8c2VsZixmdW5jdGlvbigpe3ZhciBuPWUuQ29va2llcyxyPWUuQ29va2llcz10KCk7ci5ub0NvbmZsaWN0PWZ1bmN0aW9uKCl7cmV0dXJuIGUuQ29va2llcz1uLHJ9fSgpKX0odGhpcyxmdW5jdGlvbigpe1widXNlIHN0cmljdFwiO3ZhciBlPXtyZWFkOmZ1bmN0aW9uKGUpe3JldHVybiBlLnJlcGxhY2UoLyglW1xcZEEtRl17Mn0pKy9naSxkZWNvZGVVUklDb21wb25lbnQpfSx3cml0ZTpmdW5jdGlvbihlKXtyZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGUpLnJlcGxhY2UoLyUoMlszNDZCRl18M1tBQy1GXXw0MHw1W0JERV18NjB8N1tCQ0RdKS9nLGRlY29kZVVSSUNvbXBvbmVudCl9fTtmdW5jdGlvbiB0KGUpe2Zvcih2YXIgdD0xO3Q8YXJndW1lbnRzLmxlbmd0aDt0Kyspe3ZhciBuPWFyZ3VtZW50c1t0XTtmb3IodmFyIHIgaW4gbillW3JdPW5bcl19cmV0dXJuIGV9cmV0dXJuIGZ1bmN0aW9uIG4ocixvKXtmdW5jdGlvbiBpKGUsbixpKXtpZihcInVuZGVmaW5lZFwiIT10eXBlb2YgZG9jdW1lbnQpe1wibnVtYmVyXCI9PXR5cGVvZihpPXQoe30sbyxpKSkuZXhwaXJlcyYmKGkuZXhwaXJlcz1uZXcgRGF0ZShEYXRlLm5vdygpKzg2NGU1KmkuZXhwaXJlcykpLGkuZXhwaXJlcyYmKGkuZXhwaXJlcz1pLmV4cGlyZXMudG9VVENTdHJpbmcoKSksbj1yLndyaXRlKG4sZSksZT1lbmNvZGVVUklDb21wb25lbnQoZSkucmVwbGFjZSgvJSgyWzM0NkJdfDVFfDYwfDdDKS9nLGRlY29kZVVSSUNvbXBvbmVudCkucmVwbGFjZSgvWygpXS9nLGVzY2FwZSk7dmFyIGM9XCJcIjtmb3IodmFyIHUgaW4gaSlpW3VdJiYoYys9XCI7IFwiK3UsITAhPT1pW3VdJiYoYys9XCI9XCIraVt1XS5zcGxpdChcIjtcIilbMF0pKTtyZXR1cm4gZG9jdW1lbnQuY29va2llPWUrXCI9XCIrbitjfX1yZXR1cm4gT2JqZWN0LmNyZWF0ZSh7c2V0OmksZ2V0OmZ1bmN0aW9uKHQpe2lmKFwidW5kZWZpbmVkXCIhPXR5cGVvZiBkb2N1bWVudCYmKCFhcmd1bWVudHMubGVuZ3RofHx0KSl7Zm9yKHZhciBuPWRvY3VtZW50LmNvb2tpZT9kb2N1bWVudC5jb29raWUuc3BsaXQoXCI7IFwiKTpbXSxvPXt9LGk9MDtpPG4ubGVuZ3RoO2krKyl7dmFyIGM9bltpXS5zcGxpdChcIj1cIiksdT1jLnNsaWNlKDEpLmpvaW4oXCI9XCIpOydcIic9PT11WzBdJiYodT11LnNsaWNlKDEsLTEpKTt0cnl7dmFyIGY9ZS5yZWFkKGNbMF0pO2lmKG9bZl09ci5yZWFkKHUsZiksdD09PWYpYnJlYWt9Y2F0Y2goZSl7fX1yZXR1cm4gdD9vW3RdOm99fSxyZW1vdmU6ZnVuY3Rpb24oZSxuKXtpKGUsXCJcIix0KHt9LG4se2V4cGlyZXM6LTF9KSl9LHdpdGhBdHRyaWJ1dGVzOmZ1bmN0aW9uKGUpe3JldHVybiBuKHRoaXMuY29udmVydGVyLHQoe30sdGhpcy5hdHRyaWJ1dGVzLGUpKX0sd2l0aENvbnZlcnRlcjpmdW5jdGlvbihlKXtyZXR1cm4gbih0KHt9LHRoaXMuY29udmVydGVyLGUpLHRoaXMuYXR0cmlidXRlcyl9fSx7YXR0cmlidXRlczp7dmFsdWU6T2JqZWN0LmZyZWV6ZShvKX0sY29udmVydGVyOnt2YWx1ZTpPYmplY3QuZnJlZXplKHIpfX0pfShlLHtwYXRoOlwiL1wifSl9KTtcclxuIiwiIWZ1bmN0aW9uKGIsbSl7XCJ1c2Ugc3RyaWN0XCI7KG5ldyBmdW5jdGlvbigpe3ZhciBzPWZ1bmN0aW9uKGUsbil7cmV0dXJuIGUucmVwbGFjZSgvXFx7KFxcZCspXFx9L2csZnVuY3Rpb24oZSx0KXtyZXR1cm4gblt0XXx8ZX0pfSx1PWZ1bmN0aW9uKGUpe3JldHVybiBlLmpvaW4oXCIgLSBcIil9O3RoaXMuaT1mdW5jdGlvbigpe3ZhciBlLHQ9bS5xdWVyeVNlbGVjdG9yQWxsKFwiLnNoYXJlLWJ0blwiKTtmb3IoZT10Lmxlbmd0aDtlLS07KW4odFtlXSl9O3ZhciBuPWZ1bmN0aW9uKGUpe3ZhciB0LG49ZS5xdWVyeVNlbGVjdG9yQWxsKFwiYVwiKTtmb3IodD1uLmxlbmd0aDt0LS07KXIoblt0XSx7aWQ6XCJcIix1cmw6YyhlKSx0aXRsZTppKGUpLGRlc2M6YShlKX0pfSxyPWZ1bmN0aW9uKGUsdCl7dC5pZD1sKGUsXCJkYXRhLWlkXCIpLHQuaWQmJm8oZSxcImNsaWNrXCIsdCl9LGM9ZnVuY3Rpb24oZSl7cmV0dXJuIGwoZSxcImRhdGEtdXJsXCIpfHxsb2NhdGlvbi5ocmVmfHxcIiBcIn0saT1mdW5jdGlvbihlKXtyZXR1cm4gbChlLFwiZGF0YS10aXRsZVwiKXx8bS50aXRsZXx8XCIgXCJ9LGE9ZnVuY3Rpb24oZSl7dmFyIHQ9bS5xdWVyeVNlbGVjdG9yKFwibWV0YVtuYW1lPWRlc2NyaXB0aW9uXVwiKTtyZXR1cm4gbChlLFwiZGF0YS1kZXNjXCIpfHx0JiZsKHQsXCJjb250ZW50XCIpfHxcIiBcIn0sbz1mdW5jdGlvbihlLHQsbil7dmFyIHI9ZnVuY3Rpb24oKXtwKG4uaWQsbi51cmwsbi50aXRsZSxuLmRlc2MpfTtlLmFkZEV2ZW50TGlzdGVuZXI/ZS5hZGRFdmVudExpc3RlbmVyKHQscik6ZS5hdHRhY2hFdmVudChcIm9uXCIrdCxmdW5jdGlvbigpe3IuY2FsbChlKX0pfSxsPWZ1bmN0aW9uKGUsdCl7cmV0dXJuIGUuZ2V0QXR0cmlidXRlKHQpfSxoPWZ1bmN0aW9uKGUpe3JldHVybiBlbmNvZGVVUklDb21wb25lbnQoZSl9LHA9ZnVuY3Rpb24oZSx0LG4scil7dmFyIGM9aCh0KSxpPWgociksYT1oKG4pLG89YXx8aXx8XCJcIjtzd2l0Y2goZSl7Y2FzZVwiZmJcIjpkKHMoXCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vc2hhcmVyL3NoYXJlci5waHA/dT17MH1cIixbY10pLG4pO2JyZWFrO2Nhc2VcInZrXCI6ZChzKFwiaHR0cHM6Ly92ay5jb20vc2hhcmUucGhwP3VybD17MH0mdGl0bGU9ezF9XCIsW2MsdShbYSxpXSldKSxuKTticmVhaztjYXNlXCJ0d1wiOmQocyhcImh0dHBzOi8vdHdpdHRlci5jb20vaW50ZW50L3R3ZWV0P3VybD17MH0mdGV4dD17MX1cIixbYyx1KFthLGldKV0pLG4pO2JyZWFrO2Nhc2VcInRnXCI6ZChzKFwiaHR0cHM6Ly90Lm1lL3NoYXJlL3VybD91cmw9ezB9JnRleHQ9ezF9XCIsW2MsdShbYSxpXSldKSxuKTticmVhaztjYXNlXCJwa1wiOmQocyhcImh0dHBzOi8vZ2V0cG9ja2V0LmNvbS9lZGl0P3VybD17MH0mdGl0bGU9ezF9XCIsW2MsdShbYSxpXSldKSxuKTticmVhaztjYXNlXCJyZVwiOmQocyhcImh0dHBzOi8vcmVkZGl0LmNvbS9zdWJtaXQvP3VybD17MH1cIixbY10pLG4pO2JyZWFrO2Nhc2VcImV2XCI6ZChzKFwiaHR0cHM6Ly93d3cuZXZlcm5vdGUuY29tL2NsaXAuYWN0aW9uP3VybD17MH0mdD17MX1cIixbYyxhXSksbik7YnJlYWs7Y2FzZVwiaW5cIjpkKHMoXCJodHRwczovL3d3dy5saW5rZWRpbi5jb20vc2hhcmVBcnRpY2xlP21pbmk9dHJ1ZSZ1cmw9ezB9JnRpdGxlPXsxfSZzdW1tYXJ5PXsyfSZzb3VyY2U9ezB9XCIsW2MsYSx1KFthLGldKV0pLG4pO2JyZWFrO2Nhc2VcInBpXCI6ZChzKFwiaHR0cHM6Ly9waW50ZXJlc3QuY29tL3Bpbi9jcmVhdGUvYnV0dG9uLz91cmw9ezB9Jm1lZGlhPXswfSZkZXNjcmlwdGlvbj17MX1cIixbYyx1KFthLGldKV0pLG4pO2JyZWFrO2Nhc2VcInNrXCI6ZChzKFwiaHR0cHM6Ly93ZWIuc2t5cGUuY29tL3NoYXJlP3VybD17MH0mc291cmNlPWJ1dHRvbiZ0ZXh0PXsxfVwiLFtjLHUoW2EsaV0pXSksbik7YnJlYWs7Y2FzZVwid2FcIjpkKHMoXCJ3aGF0c2FwcDovL3NlbmQ/dGV4dD17MH0lMjB7MX1cIixbdShbYSxpXSksY10pLG4pO2JyZWFrO2Nhc2VcIm9rXCI6ZChzKFwiaHR0cHM6Ly9jb25uZWN0Lm9rLnJ1L2RrP3N0LmNtZD1XaWRnZXRTaGFyZVByZXZpZXcmc2VydmljZT1vZG5va2xhc3NuaWtpJnN0LnNoYXJlVXJsPXswfVwiLFtjXSksbik7YnJlYWs7Y2FzZVwidHVcIjpkKHMoXCJodHRwczovL3d3dy50dW1ibHIuY29tL3dpZGdldHMvc2hhcmUvdG9vbD9wb3N0dHlwZT1saW5rJnRpdGxlPXswfSZjYXB0aW9uPXswfSZjb250ZW50PXsxfSZjYW5vbmljYWxVcmw9ezF9JnNoYXJlU291cmNlPXR1bWJscl9zaGFyZV9idXR0b25cIixbdShbYSxpXSksY10pLG4pO2JyZWFrO2Nhc2VcImhuXCI6ZChzKFwiaHR0cHM6Ly9uZXdzLnljb21iaW5hdG9yLmNvbS9zdWJtaXRsaW5rP3Q9ezB9JnU9ezF9XCIsW3UoW2EsaV0pLGNdKSxuKTticmVhaztjYXNlXCJ4aVwiOmQocyhcImh0dHBzOi8vd3d3LnhpbmcuY29tL2FwcC91c2VyP29wPXNoYXJlO3VybD17MH07dGl0bGU9ezF9XCIsW2MsdShbYSxpXSldKSxuKTticmVhaztjYXNlXCJtYWlsXCI6MDxhLmxlbmd0aCYmMDxpLmxlbmd0aCYmKG89dShbYSxpXSkpLDA8by5sZW5ndGgmJihvKz1cIiAvIFwiKSwwPGEubGVuZ3RoJiYoYSs9XCIgLyBcIiksbG9jYXRpb24uaHJlZj1zKFwibWFpbHRvOj9TdWJqZWN0PXswfXsxfSZib2R5PXsyfXszfVwiLFthLG4sbyxjXSk7YnJlYWs7Y2FzZVwicHJpbnRcIjp3aW5kb3cucHJpbnQoKX19LGQ9ZnVuY3Rpb24oZSx0KXt2YXIgbj12b2lkIDAhPT1iLnNjcmVlbkxlZnQ/Yi5zY3JlZW5MZWZ0OnNjcmVlbi5sZWZ0LHI9dm9pZCAwIT09Yi5zY3JlZW5Ub3A/Yi5zY3JlZW5Ub3A6c2NyZWVuLnRvcCxjPShiLmlubmVyV2lkdGh8fG0uZG9jdW1lbnRFbGVtZW50LmNsaWVudFdpZHRofHxzY3JlZW4ud2lkdGgpLzItMzAwK24saT0oYi5pbm5lckhlaWdodHx8bS5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0fHxzY3JlZW4uaGVpZ2h0KS8zLTQwMC8zK3IsYT1iLm9wZW4oZSxcIlwiLHMoXCJyZXNpemFibGUsdG9vbGJhcj15ZXMsbG9jYXRpb249eWVzLHNjcm9sbGJhcnM9eWVzLG1lbnViYXI9eWVzLHdpZHRoPXswfSxoZWlnaHQ9ezF9LHRvcD17Mn0sbGVmdD17M31cIixbNjAwLDQwMCxpLGNdKSk7bnVsbCE9PWEmJmEuZm9jdXMmJmEuZm9jdXMoKX19KS5pKCl9KHdpbmRvdyxkb2N1bWVudCk7IiwiLypcclxuICAgICBfIF8gICAgICBfICAgICAgIF9cclxuIF9fX3wgKF8pIF9fX3wgfCBfXyAgKF8pX19fXHJcbi8gX198IHwgfC8gX198IHwvIC8gIHwgLyBfX3xcclxuXFxfXyBcXCB8IHwgKF9ffCAgIDwgXyB8IFxcX18gXFxcclxufF9fXy9ffF98XFxfX198X3xcXF8oXykvIHxfX18vXHJcbiAgICAgICAgICAgICAgICAgICB8X18vXHJcblxyXG4gVmVyc2lvbjogMS44LjBcclxuICBBdXRob3I6IEtlbiBXaGVlbGVyXHJcbiBXZWJzaXRlOiBodHRwOi8va2Vud2hlZWxlci5naXRodWIuaW9cclxuICAgIERvY3M6IGh0dHA6Ly9rZW53aGVlbGVyLmdpdGh1Yi5pby9zbGlja1xyXG4gICAgUmVwbzogaHR0cDovL2dpdGh1Yi5jb20va2Vud2hlZWxlci9zbGlja1xyXG4gIElzc3VlczogaHR0cDovL2dpdGh1Yi5jb20va2Vud2hlZWxlci9zbGljay9pc3N1ZXNcclxuXHJcbiAqL1xyXG4vKiBnbG9iYWwgd2luZG93LCBkb2N1bWVudCwgZGVmaW5lLCBqUXVlcnksIHNldEludGVydmFsLCBjbGVhckludGVydmFsICovXHJcbjsoZnVuY3Rpb24oZmFjdG9yeSkge1xyXG4gICAgJ3VzZSBzdHJpY3QnO1xyXG4gICAgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xyXG4gICAgICAgIGRlZmluZShbJ2pxdWVyeSddLCBmYWN0b3J5KTtcclxuICAgIH0gZWxzZSBpZiAodHlwZW9mIGV4cG9ydHMgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgbW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KHJlcXVpcmUoJ2pxdWVyeScpKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZmFjdG9yeShqUXVlcnkpO1xyXG4gICAgfVxyXG5cclxufShmdW5jdGlvbigkKSB7XHJcbiAgICAndXNlIHN0cmljdCc7XHJcbiAgICB2YXIgU2xpY2sgPSB3aW5kb3cuU2xpY2sgfHwge307XHJcblxyXG4gICAgU2xpY2sgPSAoZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBpbnN0YW5jZVVpZCA9IDA7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIFNsaWNrKGVsZW1lbnQsIHNldHRpbmdzKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgXyA9IHRoaXMsIGRhdGFTZXR0aW5ncztcclxuXHJcbiAgICAgICAgICAgIF8uZGVmYXVsdHMgPSB7XHJcbiAgICAgICAgICAgICAgICBhY2Nlc3NpYmlsaXR5OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgYWRhcHRpdmVIZWlnaHQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgYXBwZW5kQXJyb3dzOiAkKGVsZW1lbnQpLFxyXG4gICAgICAgICAgICAgICAgYXBwZW5kRG90czogJChlbGVtZW50KSxcclxuICAgICAgICAgICAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGFzTmF2Rm9yOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcHJldkFycm93OiAnPGJ1dHRvbiBjbGFzcz1cInNsaWNrLXByZXZcIiBhcmlhLWxhYmVsPVwiUHJldmlvdXNcIiB0eXBlPVwiYnV0dG9uXCI+UHJldmlvdXM8L2J1dHRvbj4nLFxyXG4gICAgICAgICAgICAgICAgbmV4dEFycm93OiAnPGJ1dHRvbiBjbGFzcz1cInNsaWNrLW5leHRcIiBhcmlhLWxhYmVsPVwiTmV4dFwiIHR5cGU9XCJidXR0b25cIj5OZXh0PC9idXR0b24+JyxcclxuICAgICAgICAgICAgICAgIGF1dG9wbGF5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGF1dG9wbGF5U3BlZWQ6IDMwMDAsXHJcbiAgICAgICAgICAgICAgICBjZW50ZXJNb2RlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGNlbnRlclBhZGRpbmc6ICc1MHB4JyxcclxuICAgICAgICAgICAgICAgIGNzc0Vhc2U6ICdlYXNlJyxcclxuICAgICAgICAgICAgICAgIGN1c3RvbVBhZ2luZzogZnVuY3Rpb24oc2xpZGVyLCBpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICQoJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIC8+JykudGV4dChpICsgMSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZG90czogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBkb3RzQ2xhc3M6ICdzbGljay1kb3RzJyxcclxuICAgICAgICAgICAgICAgIGRyYWdnYWJsZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGVhc2luZzogJ2xpbmVhcicsXHJcbiAgICAgICAgICAgICAgICBlZGdlRnJpY3Rpb246IDAuMzUsXHJcbiAgICAgICAgICAgICAgICBmYWRlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGZvY3VzT25TZWxlY3Q6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgZm9jdXNPbkNoYW5nZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGluaXRpYWxTbGlkZTogMCxcclxuICAgICAgICAgICAgICAgIGxhenlMb2FkOiAnb25kZW1hbmQnLFxyXG4gICAgICAgICAgICAgICAgbW9iaWxlRmlyc3Q6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgcGF1c2VPbkhvdmVyOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgcGF1c2VPbkZvY3VzOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgcGF1c2VPbkRvdHNIb3ZlcjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICByZXNwb25kVG86ICd3aW5kb3cnLFxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJvd3M6IDEsXHJcbiAgICAgICAgICAgICAgICBydGw6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc2xpZGU6ICcnLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzUGVyUm93OiAxLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICBzcGVlZDogNTAwLFxyXG4gICAgICAgICAgICAgICAgc3dpcGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBzd2lwZVRvU2xpZGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdG91Y2hNb3ZlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDUsXHJcbiAgICAgICAgICAgICAgICB1c2VDU1M6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB1c2VUcmFuc2Zvcm06IHRydWUsXHJcbiAgICAgICAgICAgICAgICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHZlcnRpY2FsOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHZlcnRpY2FsU3dpcGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB3YWl0Rm9yQW5pbWF0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHpJbmRleDogMTAwMFxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgXy5pbml0aWFscyA9IHtcclxuICAgICAgICAgICAgICAgIGFuaW1hdGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBkcmFnZ2luZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBhdXRvUGxheVRpbWVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgY3VycmVudERpcmVjdGlvbjogMCxcclxuICAgICAgICAgICAgICAgIGN1cnJlbnRMZWZ0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgY3VycmVudFNsaWRlOiAwLFxyXG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uOiAxLFxyXG4gICAgICAgICAgICAgICAgJGRvdHM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBsaXN0V2lkdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBsaXN0SGVpZ2h0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgbG9hZEluZGV4OiAwLFxyXG4gICAgICAgICAgICAgICAgJG5leHRBcnJvdzogbnVsbCxcclxuICAgICAgICAgICAgICAgICRwcmV2QXJyb3c6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBzY3JvbGxpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVDb3VudDogbnVsbCxcclxuICAgICAgICAgICAgICAgIHNsaWRlV2lkdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAkc2xpZGVUcmFjazogbnVsbCxcclxuICAgICAgICAgICAgICAgICRzbGlkZXM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBzbGlkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHNsaWRlT2Zmc2V0OiAwLFxyXG4gICAgICAgICAgICAgICAgc3dpcGVMZWZ0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgc3dpcGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAkbGlzdDogbnVsbCxcclxuICAgICAgICAgICAgICAgIHRvdWNoT2JqZWN0OiB7fSxcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybXNFbmFibGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHVuc2xpY2tlZDogZmFsc2VcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICQuZXh0ZW5kKF8sIF8uaW5pdGlhbHMpO1xyXG5cclxuICAgICAgICAgICAgXy5hY3RpdmVCcmVha3BvaW50ID0gbnVsbDtcclxuICAgICAgICAgICAgXy5hbmltVHlwZSA9IG51bGw7XHJcbiAgICAgICAgICAgIF8uYW5pbVByb3AgPSBudWxsO1xyXG4gICAgICAgICAgICBfLmJyZWFrcG9pbnRzID0gW107XHJcbiAgICAgICAgICAgIF8uYnJlYWtwb2ludFNldHRpbmdzID0gW107XHJcbiAgICAgICAgICAgIF8uY3NzVHJhbnNpdGlvbnMgPSBmYWxzZTtcclxuICAgICAgICAgICAgXy5mb2N1c3NlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICBfLmludGVycnVwdGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIF8uaGlkZGVuID0gJ2hpZGRlbic7XHJcbiAgICAgICAgICAgIF8ucGF1c2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgXy5wb3NpdGlvblByb3AgPSBudWxsO1xyXG4gICAgICAgICAgICBfLnJlc3BvbmRUbyA9IG51bGw7XHJcbiAgICAgICAgICAgIF8ucm93Q291bnQgPSAxO1xyXG4gICAgICAgICAgICBfLnNob3VsZENsaWNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgXy4kc2xpZGVyID0gJChlbGVtZW50KTtcclxuICAgICAgICAgICAgXy4kc2xpZGVzQ2FjaGUgPSBudWxsO1xyXG4gICAgICAgICAgICBfLnRyYW5zZm9ybVR5cGUgPSBudWxsO1xyXG4gICAgICAgICAgICBfLnRyYW5zaXRpb25UeXBlID0gbnVsbDtcclxuICAgICAgICAgICAgXy52aXNpYmlsaXR5Q2hhbmdlID0gJ3Zpc2liaWxpdHljaGFuZ2UnO1xyXG4gICAgICAgICAgICBfLndpbmRvd1dpZHRoID0gMDtcclxuICAgICAgICAgICAgXy53aW5kb3dUaW1lciA9IG51bGw7XHJcblxyXG4gICAgICAgICAgICBkYXRhU2V0dGluZ3MgPSAkKGVsZW1lbnQpLmRhdGEoJ3NsaWNrJykgfHwge307XHJcblxyXG4gICAgICAgICAgICBfLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgXy5kZWZhdWx0cywgc2V0dGluZ3MsIGRhdGFTZXR0aW5ncyk7XHJcblxyXG4gICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IF8ub3B0aW9ucy5pbml0aWFsU2xpZGU7XHJcblxyXG4gICAgICAgICAgICBfLm9yaWdpbmFsU2V0dGluZ3MgPSBfLm9wdGlvbnM7XHJcblxyXG4gICAgICAgICAgICBpZiAodHlwZW9mIGRvY3VtZW50Lm1vekhpZGRlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgICAgIF8uaGlkZGVuID0gJ21vekhpZGRlbic7XHJcbiAgICAgICAgICAgICAgICBfLnZpc2liaWxpdHlDaGFuZ2UgPSAnbW96dmlzaWJpbGl0eWNoYW5nZSc7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGRvY3VtZW50LndlYmtpdEhpZGRlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgICAgIF8uaGlkZGVuID0gJ3dlYmtpdEhpZGRlbic7XHJcbiAgICAgICAgICAgICAgICBfLnZpc2liaWxpdHlDaGFuZ2UgPSAnd2Via2l0dmlzaWJpbGl0eWNoYW5nZSc7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIF8uYXV0b1BsYXkgPSAkLnByb3h5KF8uYXV0b1BsYXksIF8pO1xyXG4gICAgICAgICAgICBfLmF1dG9QbGF5Q2xlYXIgPSAkLnByb3h5KF8uYXV0b1BsYXlDbGVhciwgXyk7XHJcbiAgICAgICAgICAgIF8uYXV0b1BsYXlJdGVyYXRvciA9ICQucHJveHkoXy5hdXRvUGxheUl0ZXJhdG9yLCBfKTtcclxuICAgICAgICAgICAgXy5jaGFuZ2VTbGlkZSA9ICQucHJveHkoXy5jaGFuZ2VTbGlkZSwgXyk7XHJcbiAgICAgICAgICAgIF8uY2xpY2tIYW5kbGVyID0gJC5wcm94eShfLmNsaWNrSGFuZGxlciwgXyk7XHJcbiAgICAgICAgICAgIF8uc2VsZWN0SGFuZGxlciA9ICQucHJveHkoXy5zZWxlY3RIYW5kbGVyLCBfKTtcclxuICAgICAgICAgICAgXy5zZXRQb3NpdGlvbiA9ICQucHJveHkoXy5zZXRQb3NpdGlvbiwgXyk7XHJcbiAgICAgICAgICAgIF8uc3dpcGVIYW5kbGVyID0gJC5wcm94eShfLnN3aXBlSGFuZGxlciwgXyk7XHJcbiAgICAgICAgICAgIF8uZHJhZ0hhbmRsZXIgPSAkLnByb3h5KF8uZHJhZ0hhbmRsZXIsIF8pO1xyXG4gICAgICAgICAgICBfLmtleUhhbmRsZXIgPSAkLnByb3h5KF8ua2V5SGFuZGxlciwgXyk7XHJcblxyXG4gICAgICAgICAgICBfLmluc3RhbmNlVWlkID0gaW5zdGFuY2VVaWQrKztcclxuXHJcbiAgICAgICAgICAgIC8vIEEgc2ltcGxlIHdheSB0byBjaGVjayBmb3IgSFRNTCBzdHJpbmdzXHJcbiAgICAgICAgICAgIC8vIFN0cmljdCBIVE1MIHJlY29nbml0aW9uIChtdXN0IHN0YXJ0IHdpdGggPClcclxuICAgICAgICAgICAgLy8gRXh0cmFjdGVkIGZyb20galF1ZXJ5IHYxLjExIHNvdXJjZVxyXG4gICAgICAgICAgICBfLmh0bWxFeHByID0gL14oPzpcXHMqKDxbXFx3XFxXXSs+KVtePl0qKSQvO1xyXG5cclxuXHJcbiAgICAgICAgICAgIF8ucmVnaXN0ZXJCcmVha3BvaW50cygpO1xyXG4gICAgICAgICAgICBfLmluaXQodHJ1ZSk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIFNsaWNrO1xyXG5cclxuICAgIH0oKSk7XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmFjdGl2YXRlQURBID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLiRzbGlkZVRyYWNrLmZpbmQoJy5zbGljay1hY3RpdmUnKS5hdHRyKHtcclxuICAgICAgICAgICAgJ2FyaWEtaGlkZGVuJzogJ2ZhbHNlJ1xyXG4gICAgICAgIH0pLmZpbmQoJ2EsIGlucHV0LCBidXR0b24sIHNlbGVjdCcpLmF0dHIoe1xyXG4gICAgICAgICAgICAndGFiaW5kZXgnOiAnMCdcclxuICAgICAgICB9KTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5hZGRTbGlkZSA9IFNsaWNrLnByb3RvdHlwZS5zbGlja0FkZCA9IGZ1bmN0aW9uKG1hcmt1cCwgaW5kZXgsIGFkZEJlZm9yZSkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YoaW5kZXgpID09PSAnYm9vbGVhbicpIHtcclxuICAgICAgICAgICAgYWRkQmVmb3JlID0gaW5kZXg7XHJcbiAgICAgICAgICAgIGluZGV4ID0gbnVsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGluZGV4IDwgMCB8fCAoaW5kZXggPj0gXy5zbGlkZUNvdW50KSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfLnVubG9hZCgpO1xyXG5cclxuICAgICAgICBpZiAodHlwZW9mKGluZGV4KSA9PT0gJ251bWJlcicpIHtcclxuICAgICAgICAgICAgaWYgKGluZGV4ID09PSAwICYmIF8uJHNsaWRlcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICQobWFya3VwKS5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChhZGRCZWZvcmUpIHtcclxuICAgICAgICAgICAgICAgICQobWFya3VwKS5pbnNlcnRCZWZvcmUoXy4kc2xpZGVzLmVxKGluZGV4KSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKG1hcmt1cCkuaW5zZXJ0QWZ0ZXIoXy4kc2xpZGVzLmVxKGluZGV4KSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAoYWRkQmVmb3JlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAkKG1hcmt1cCkucHJlcGVuZFRvKF8uJHNsaWRlVHJhY2spO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgJChtYXJrdXApLmFwcGVuZFRvKF8uJHNsaWRlVHJhY2spO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfLiRzbGlkZXMgPSBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5zbGlkZSk7XHJcblxyXG4gICAgICAgIF8uJHNsaWRlVHJhY2suY2hpbGRyZW4odGhpcy5vcHRpb25zLnNsaWRlKS5kZXRhY2goKTtcclxuXHJcbiAgICAgICAgXy4kc2xpZGVUcmFjay5hcHBlbmQoXy4kc2xpZGVzKTtcclxuXHJcbiAgICAgICAgXy4kc2xpZGVzLmVhY2goZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgJChlbGVtZW50KS5hdHRyKCdkYXRhLXNsaWNrLWluZGV4JywgaW5kZXgpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBfLiRzbGlkZXNDYWNoZSA9IF8uJHNsaWRlcztcclxuXHJcbiAgICAgICAgXy5yZWluaXQoKTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5hbmltYXRlSGVpZ2h0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID09PSAxICYmIF8ub3B0aW9ucy5hZGFwdGl2ZUhlaWdodCA9PT0gdHJ1ZSAmJiBfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIHZhciB0YXJnZXRIZWlnaHQgPSBfLiRzbGlkZXMuZXEoXy5jdXJyZW50U2xpZGUpLm91dGVySGVpZ2h0KHRydWUpO1xyXG4gICAgICAgICAgICBfLiRsaXN0LmFuaW1hdGUoe1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB0YXJnZXRIZWlnaHRcclxuICAgICAgICAgICAgfSwgXy5vcHRpb25zLnNwZWVkKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5hbmltYXRlU2xpZGUgPSBmdW5jdGlvbih0YXJnZXRMZWZ0LCBjYWxsYmFjaykge1xyXG5cclxuICAgICAgICB2YXIgYW5pbVByb3BzID0ge30sXHJcbiAgICAgICAgICAgIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLmFuaW1hdGVIZWlnaHQoKTtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUgJiYgXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICB0YXJnZXRMZWZ0ID0gLXRhcmdldExlZnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChfLnRyYW5zZm9ybXNFbmFibGVkID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5hbmltYXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiB0YXJnZXRMZWZ0XHJcbiAgICAgICAgICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQsIF8ub3B0aW9ucy5lYXNpbmcsIGNhbGxiYWNrKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9wOiB0YXJnZXRMZWZ0XHJcbiAgICAgICAgICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQsIF8ub3B0aW9ucy5lYXNpbmcsIGNhbGxiYWNrKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgaWYgKF8uY3NzVHJhbnNpdGlvbnMgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF8uY3VycmVudExlZnQgPSAtKF8uY3VycmVudExlZnQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgJCh7XHJcbiAgICAgICAgICAgICAgICAgICAgYW5pbVN0YXJ0OiBfLmN1cnJlbnRMZWZ0XHJcbiAgICAgICAgICAgICAgICB9KS5hbmltYXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBhbmltU3RhcnQ6IHRhcmdldExlZnRcclxuICAgICAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogXy5vcHRpb25zLnNwZWVkLFxyXG4gICAgICAgICAgICAgICAgICAgIGVhc2luZzogXy5vcHRpb25zLmVhc2luZyxcclxuICAgICAgICAgICAgICAgICAgICBzdGVwOiBmdW5jdGlvbihub3cpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbm93ID0gTWF0aC5jZWlsKG5vdyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmltUHJvcHNbXy5hbmltVHlwZV0gPSAndHJhbnNsYXRlKCcgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vdyArICdweCwgMHB4KSc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyhhbmltUHJvcHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5pbVByb3BzW18uYW5pbVR5cGVdID0gJ3RyYW5zbGF0ZSgwcHgsJyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbm93ICsgJ3B4KSc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyhhbmltUHJvcHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjYWxsYmFjaykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2suY2FsbCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgIF8uYXBwbHlUcmFuc2l0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICB0YXJnZXRMZWZ0ID0gTWF0aC5jZWlsKHRhcmdldExlZnQpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYW5pbVByb3BzW18uYW5pbVR5cGVdID0gJ3RyYW5zbGF0ZTNkKCcgKyB0YXJnZXRMZWZ0ICsgJ3B4LCAwcHgsIDBweCknO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBhbmltUHJvcHNbXy5hbmltVHlwZV0gPSAndHJhbnNsYXRlM2QoMHB4LCcgKyB0YXJnZXRMZWZ0ICsgJ3B4LCAwcHgpJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKGFuaW1Qcm9wcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF8uZGlzYWJsZVRyYW5zaXRpb24oKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrLmNhbGwoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuZ2V0TmF2VGFyZ2V0ID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcyxcclxuICAgICAgICAgICAgYXNOYXZGb3IgPSBfLm9wdGlvbnMuYXNOYXZGb3I7XHJcblxyXG4gICAgICAgIGlmICggYXNOYXZGb3IgJiYgYXNOYXZGb3IgIT09IG51bGwgKSB7XHJcbiAgICAgICAgICAgIGFzTmF2Rm9yID0gJChhc05hdkZvcikubm90KF8uJHNsaWRlcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gYXNOYXZGb3I7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuYXNOYXZGb3IgPSBmdW5jdGlvbihpbmRleCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIGFzTmF2Rm9yID0gXy5nZXROYXZUYXJnZXQoKTtcclxuXHJcbiAgICAgICAgaWYgKCBhc05hdkZvciAhPT0gbnVsbCAmJiB0eXBlb2YgYXNOYXZGb3IgPT09ICdvYmplY3QnICkge1xyXG4gICAgICAgICAgICBhc05hdkZvci5lYWNoKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHRhcmdldCA9ICQodGhpcykuc2xpY2soJ2dldFNsaWNrJyk7XHJcbiAgICAgICAgICAgICAgICBpZighdGFyZ2V0LnVuc2xpY2tlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRhcmdldC5zbGlkZUhhbmRsZXIoaW5kZXgsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuYXBwbHlUcmFuc2l0aW9uID0gZnVuY3Rpb24oc2xpZGUpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uID0ge307XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbltfLnRyYW5zaXRpb25UeXBlXSA9IF8udHJhbnNmb3JtVHlwZSArICcgJyArIF8ub3B0aW9ucy5zcGVlZCArICdtcyAnICsgXy5vcHRpb25zLmNzc0Vhc2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbltfLnRyYW5zaXRpb25UeXBlXSA9ICdvcGFjaXR5ICcgKyBfLm9wdGlvbnMuc3BlZWQgKyAnbXMgJyArIF8ub3B0aW9ucy5jc3NFYXNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mYWRlID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyh0cmFuc2l0aW9uKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBfLiRzbGlkZXMuZXEoc2xpZGUpLmNzcyh0cmFuc2l0aW9uKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuYXV0b1BsYXkgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLmF1dG9QbGF5Q2xlYXIoKTtcclxuXHJcbiAgICAgICAgaWYgKCBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICkge1xyXG4gICAgICAgICAgICBfLmF1dG9QbGF5VGltZXIgPSBzZXRJbnRlcnZhbCggXy5hdXRvUGxheUl0ZXJhdG9yLCBfLm9wdGlvbnMuYXV0b3BsYXlTcGVlZCApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5hdXRvUGxheUNsZWFyID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKF8uYXV0b1BsYXlUaW1lcikge1xyXG4gICAgICAgICAgICBjbGVhckludGVydmFsKF8uYXV0b1BsYXlUaW1lcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmF1dG9QbGF5SXRlcmF0b3IgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICBzbGlkZVRvID0gXy5jdXJyZW50U2xpZGUgKyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XHJcblxyXG4gICAgICAgIGlmICggIV8ucGF1c2VkICYmICFfLmludGVycnVwdGVkICYmICFfLmZvY3Vzc2VkICkge1xyXG5cclxuICAgICAgICAgICAgaWYgKCBfLm9wdGlvbnMuaW5maW5pdGUgPT09IGZhbHNlICkge1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICggXy5kaXJlY3Rpb24gPT09IDEgJiYgKCBfLmN1cnJlbnRTbGlkZSArIDEgKSA9PT0gKCBfLnNsaWRlQ291bnQgLSAxICkpIHtcclxuICAgICAgICAgICAgICAgICAgICBfLmRpcmVjdGlvbiA9IDA7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoIF8uZGlyZWN0aW9uID09PSAwICkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBzbGlkZVRvID0gXy5jdXJyZW50U2xpZGUgLSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICggXy5jdXJyZW50U2xpZGUgLSAxID09PSAwICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLmRpcmVjdGlvbiA9IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIF8uc2xpZGVIYW5kbGVyKCBzbGlkZVRvICk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5idWlsZEFycm93cyA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuYXJyb3dzID09PSB0cnVlICkge1xyXG5cclxuICAgICAgICAgICAgXy4kcHJldkFycm93ID0gJChfLm9wdGlvbnMucHJldkFycm93KS5hZGRDbGFzcygnc2xpY2stYXJyb3cnKTtcclxuICAgICAgICAgICAgXy4kbmV4dEFycm93ID0gJChfLm9wdGlvbnMubmV4dEFycm93KS5hZGRDbGFzcygnc2xpY2stYXJyb3cnKTtcclxuXHJcbiAgICAgICAgICAgIGlmKCBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICkge1xyXG5cclxuICAgICAgICAgICAgICAgIF8uJHByZXZBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2staGlkZGVuJykucmVtb3ZlQXR0cignYXJpYS1oaWRkZW4gdGFiaW5kZXgnKTtcclxuICAgICAgICAgICAgICAgIF8uJG5leHRBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2staGlkZGVuJykucmVtb3ZlQXR0cignYXJpYS1oaWRkZW4gdGFiaW5kZXgnKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5wcmV2QXJyb3cpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgXy4kcHJldkFycm93LnByZXBlbmRUbyhfLm9wdGlvbnMuYXBwZW5kQXJyb3dzKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5uZXh0QXJyb3cpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgXy4kbmV4dEFycm93LmFwcGVuZFRvKF8ub3B0aW9ucy5hcHBlbmRBcnJvd3MpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgIT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBfLiRwcmV2QXJyb3dcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1kaXNhYmxlZCcpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ3RydWUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgXy4kcHJldkFycm93LmFkZCggXy4kbmV4dEFycm93IClcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1oaWRkZW4nKVxyXG4gICAgICAgICAgICAgICAgICAgIC5hdHRyKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ2FyaWEtZGlzYWJsZWQnOiAndHJ1ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICd0YWJpbmRleCc6ICctMSdcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmJ1aWxkRG90cyA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIGksIGRvdDtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5kb3RzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuXHJcbiAgICAgICAgICAgIF8uJHNsaWRlci5hZGRDbGFzcygnc2xpY2stZG90dGVkJyk7XHJcblxyXG4gICAgICAgICAgICBkb3QgPSAkKCc8dWwgLz4nKS5hZGRDbGFzcyhfLm9wdGlvbnMuZG90c0NsYXNzKTtcclxuXHJcbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPD0gXy5nZXREb3RDb3VudCgpOyBpICs9IDEpIHtcclxuICAgICAgICAgICAgICAgIGRvdC5hcHBlbmQoJCgnPGxpIC8+JykuYXBwZW5kKF8ub3B0aW9ucy5jdXN0b21QYWdpbmcuY2FsbCh0aGlzLCBfLCBpKSkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBfLiRkb3RzID0gZG90LmFwcGVuZFRvKF8ub3B0aW9ucy5hcHBlbmREb3RzKTtcclxuXHJcbiAgICAgICAgICAgIF8uJGRvdHMuZmluZCgnbGknKS5maXJzdCgpLmFkZENsYXNzKCdzbGljay1hY3RpdmUnKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmJ1aWxkT3V0ID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgXy4kc2xpZGVzID1cclxuICAgICAgICAgICAgXy4kc2xpZGVyXHJcbiAgICAgICAgICAgICAgICAuY2hpbGRyZW4oIF8ub3B0aW9ucy5zbGlkZSArICc6bm90KC5zbGljay1jbG9uZWQpJylcclxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stc2xpZGUnKTtcclxuXHJcbiAgICAgICAgXy5zbGlkZUNvdW50ID0gXy4kc2xpZGVzLmxlbmd0aDtcclxuXHJcbiAgICAgICAgXy4kc2xpZGVzLmVhY2goZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgJChlbGVtZW50KVxyXG4gICAgICAgICAgICAgICAgLmF0dHIoJ2RhdGEtc2xpY2staW5kZXgnLCBpbmRleClcclxuICAgICAgICAgICAgICAgIC5kYXRhKCdvcmlnaW5hbFN0eWxpbmcnLCAkKGVsZW1lbnQpLmF0dHIoJ3N0eWxlJykgfHwgJycpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBfLiRzbGlkZXIuYWRkQ2xhc3MoJ3NsaWNrLXNsaWRlcicpO1xyXG5cclxuICAgICAgICBfLiRzbGlkZVRyYWNrID0gKF8uc2xpZGVDb3VudCA9PT0gMCkgP1xyXG4gICAgICAgICAgICAkKCc8ZGl2IGNsYXNzPVwic2xpY2stdHJhY2tcIi8+JykuYXBwZW5kVG8oXy4kc2xpZGVyKSA6XHJcbiAgICAgICAgICAgIF8uJHNsaWRlcy53cmFwQWxsKCc8ZGl2IGNsYXNzPVwic2xpY2stdHJhY2tcIi8+JykucGFyZW50KCk7XHJcblxyXG4gICAgICAgIF8uJGxpc3QgPSBfLiRzbGlkZVRyYWNrLndyYXAoXHJcbiAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwic2xpY2stbGlzdFwiLz4nKS5wYXJlbnQoKTtcclxuICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcygnb3BhY2l0eScsIDApO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUgfHwgXy5vcHRpb25zLnN3aXBlVG9TbGlkZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgPSAxO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJCgnaW1nW2RhdGEtbGF6eV0nLCBfLiRzbGlkZXIpLm5vdCgnW3NyY10nKS5hZGRDbGFzcygnc2xpY2stbG9hZGluZycpO1xyXG5cclxuICAgICAgICBfLnNldHVwSW5maW5pdGUoKTtcclxuXHJcbiAgICAgICAgXy5idWlsZEFycm93cygpO1xyXG5cclxuICAgICAgICBfLmJ1aWxkRG90cygpO1xyXG5cclxuICAgICAgICBfLnVwZGF0ZURvdHMoKTtcclxuXHJcblxyXG4gICAgICAgIF8uc2V0U2xpZGVDbGFzc2VzKHR5cGVvZiBfLmN1cnJlbnRTbGlkZSA9PT0gJ251bWJlcicgPyBfLmN1cnJlbnRTbGlkZSA6IDApO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmRyYWdnYWJsZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBfLiRsaXN0LmFkZENsYXNzKCdkcmFnZ2FibGUnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuYnVpbGRSb3dzID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcywgYSwgYiwgYywgbmV3U2xpZGVzLCBudW1PZlNsaWRlcywgb3JpZ2luYWxTbGlkZXMsc2xpZGVzUGVyU2VjdGlvbjtcclxuXHJcbiAgICAgICAgbmV3U2xpZGVzID0gZG9jdW1lbnQuY3JlYXRlRG9jdW1lbnRGcmFnbWVudCgpO1xyXG4gICAgICAgIG9yaWdpbmFsU2xpZGVzID0gXy4kc2xpZGVyLmNoaWxkcmVuKCk7XHJcblxyXG4gICAgICAgIGlmKF8ub3B0aW9ucy5yb3dzID4gMCkge1xyXG5cclxuICAgICAgICAgICAgc2xpZGVzUGVyU2VjdGlvbiA9IF8ub3B0aW9ucy5zbGlkZXNQZXJSb3cgKiBfLm9wdGlvbnMucm93cztcclxuICAgICAgICAgICAgbnVtT2ZTbGlkZXMgPSBNYXRoLmNlaWwoXHJcbiAgICAgICAgICAgICAgICBvcmlnaW5hbFNsaWRlcy5sZW5ndGggLyBzbGlkZXNQZXJTZWN0aW9uXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBmb3IoYSA9IDA7IGEgPCBudW1PZlNsaWRlczsgYSsrKXtcclxuICAgICAgICAgICAgICAgIHZhciBzbGlkZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICAgICAgICAgICAgZm9yKGIgPSAwOyBiIDwgXy5vcHRpb25zLnJvd3M7IGIrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciByb3cgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgICAgICAgICAgICAgICAgICBmb3IoYyA9IDA7IGMgPCBfLm9wdGlvbnMuc2xpZGVzUGVyUm93OyBjKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRhcmdldCA9IChhICogc2xpZGVzUGVyU2VjdGlvbiArICgoYiAqIF8ub3B0aW9ucy5zbGlkZXNQZXJSb3cpICsgYykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAob3JpZ2luYWxTbGlkZXMuZ2V0KHRhcmdldCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdy5hcHBlbmRDaGlsZChvcmlnaW5hbFNsaWRlcy5nZXQodGFyZ2V0KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGUuYXBwZW5kQ2hpbGQocm93KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG5ld1NsaWRlcy5hcHBlbmRDaGlsZChzbGlkZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIF8uJHNsaWRlci5lbXB0eSgpLmFwcGVuZChuZXdTbGlkZXMpO1xyXG4gICAgICAgICAgICBfLiRzbGlkZXIuY2hpbGRyZW4oKS5jaGlsZHJlbigpLmNoaWxkcmVuKClcclxuICAgICAgICAgICAgICAgIC5jc3Moe1xyXG4gICAgICAgICAgICAgICAgICAgICd3aWR0aCc6KDEwMCAvIF8ub3B0aW9ucy5zbGlkZXNQZXJSb3cpICsgJyUnLFxyXG4gICAgICAgICAgICAgICAgICAgICdkaXNwbGF5JzogJ2lubGluZS1ibG9jaydcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuY2hlY2tSZXNwb25zaXZlID0gZnVuY3Rpb24oaW5pdGlhbCwgZm9yY2VVcGRhdGUpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICBicmVha3BvaW50LCB0YXJnZXRCcmVha3BvaW50LCByZXNwb25kVG9XaWR0aCwgdHJpZ2dlckJyZWFrcG9pbnQgPSBmYWxzZTtcclxuICAgICAgICB2YXIgc2xpZGVyV2lkdGggPSBfLiRzbGlkZXIud2lkdGgoKTtcclxuICAgICAgICB2YXIgd2luZG93V2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aCB8fCAkKHdpbmRvdykud2lkdGgoKTtcclxuXHJcbiAgICAgICAgaWYgKF8ucmVzcG9uZFRvID09PSAnd2luZG93Jykge1xyXG4gICAgICAgICAgICByZXNwb25kVG9XaWR0aCA9IHdpbmRvd1dpZHRoO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoXy5yZXNwb25kVG8gPT09ICdzbGlkZXInKSB7XHJcbiAgICAgICAgICAgIHJlc3BvbmRUb1dpZHRoID0gc2xpZGVyV2lkdGg7XHJcbiAgICAgICAgfSBlbHNlIGlmIChfLnJlc3BvbmRUbyA9PT0gJ21pbicpIHtcclxuICAgICAgICAgICAgcmVzcG9uZFRvV2lkdGggPSBNYXRoLm1pbih3aW5kb3dXaWR0aCwgc2xpZGVyV2lkdGgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCBfLm9wdGlvbnMucmVzcG9uc2l2ZSAmJlxyXG4gICAgICAgICAgICBfLm9wdGlvbnMucmVzcG9uc2l2ZS5sZW5ndGggJiZcclxuICAgICAgICAgICAgXy5vcHRpb25zLnJlc3BvbnNpdmUgIT09IG51bGwpIHtcclxuXHJcbiAgICAgICAgICAgIHRhcmdldEJyZWFrcG9pbnQgPSBudWxsO1xyXG5cclxuICAgICAgICAgICAgZm9yIChicmVha3BvaW50IGluIF8uYnJlYWtwb2ludHMpIHtcclxuICAgICAgICAgICAgICAgIGlmIChfLmJyZWFrcG9pbnRzLmhhc093blByb3BlcnR5KGJyZWFrcG9pbnQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKF8ub3JpZ2luYWxTZXR0aW5ncy5tb2JpbGVGaXJzdCA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbmRUb1dpZHRoIDwgXy5icmVha3BvaW50c1ticmVha3BvaW50XSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0QnJlYWtwb2ludCA9IF8uYnJlYWtwb2ludHNbYnJlYWtwb2ludF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uZFRvV2lkdGggPiBfLmJyZWFrcG9pbnRzW2JyZWFrcG9pbnRdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRCcmVha3BvaW50ID0gXy5icmVha3BvaW50c1ticmVha3BvaW50XTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRhcmdldEJyZWFrcG9pbnQgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGlmIChfLmFjdGl2ZUJyZWFrcG9pbnQgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0QnJlYWtwb2ludCAhPT0gXy5hY3RpdmVCcmVha3BvaW50IHx8IGZvcmNlVXBkYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF8uYWN0aXZlQnJlYWtwb2ludCA9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRCcmVha3BvaW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoXy5icmVha3BvaW50U2V0dGluZ3NbdGFyZ2V0QnJlYWtwb2ludF0gPT09ICd1bnNsaWNrJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy51bnNsaWNrKHRhcmdldEJyZWFrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zID0gJC5leHRlbmQoe30sIF8ub3JpZ2luYWxTZXR0aW5ncyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmJyZWFrcG9pbnRTZXR0aW5nc1tcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0QnJlYWtwb2ludF0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGluaXRpYWwgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IF8ub3B0aW9ucy5pbml0aWFsU2xpZGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLnJlZnJlc2goaW5pdGlhbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdHJpZ2dlckJyZWFrcG9pbnQgPSB0YXJnZXRCcmVha3BvaW50O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgXy5hY3RpdmVCcmVha3BvaW50ID0gdGFyZ2V0QnJlYWtwb2ludDtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoXy5icmVha3BvaW50U2V0dGluZ3NbdGFyZ2V0QnJlYWtwb2ludF0gPT09ICd1bnNsaWNrJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLnVuc2xpY2sodGFyZ2V0QnJlYWtwb2ludCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zID0gJC5leHRlbmQoe30sIF8ub3JpZ2luYWxTZXR0aW5ncyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uYnJlYWtwb2ludFNldHRpbmdzW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldEJyZWFrcG9pbnRdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGluaXRpYWwgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uY3VycmVudFNsaWRlID0gXy5vcHRpb25zLmluaXRpYWxTbGlkZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLnJlZnJlc2goaW5pdGlhbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRyaWdnZXJCcmVha3BvaW50ID0gdGFyZ2V0QnJlYWtwb2ludDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmIChfLmFjdGl2ZUJyZWFrcG9pbnQgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICBfLmFjdGl2ZUJyZWFrcG9pbnQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIF8ub3B0aW9ucyA9IF8ub3JpZ2luYWxTZXR0aW5ncztcclxuICAgICAgICAgICAgICAgICAgICBpZiAoaW5pdGlhbCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IF8ub3B0aW9ucy5pbml0aWFsU2xpZGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIF8ucmVmcmVzaChpbml0aWFsKTtcclxuICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyQnJlYWtwb2ludCA9IHRhcmdldEJyZWFrcG9pbnQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIG9ubHkgdHJpZ2dlciBicmVha3BvaW50cyBkdXJpbmcgYW4gYWN0dWFsIGJyZWFrLiBub3Qgb24gaW5pdGlhbGl6ZS5cclxuICAgICAgICAgICAgaWYoICFpbml0aWFsICYmIHRyaWdnZXJCcmVha3BvaW50ICE9PSBmYWxzZSApIHtcclxuICAgICAgICAgICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdicmVha3BvaW50JywgW18sIHRyaWdnZXJCcmVha3BvaW50XSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuY2hhbmdlU2xpZGUgPSBmdW5jdGlvbihldmVudCwgZG9udEFuaW1hdGUpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICAkdGFyZ2V0ID0gJChldmVudC5jdXJyZW50VGFyZ2V0KSxcclxuICAgICAgICAgICAgaW5kZXhPZmZzZXQsIHNsaWRlT2Zmc2V0LCB1bmV2ZW5PZmZzZXQ7XHJcblxyXG4gICAgICAgIC8vIElmIHRhcmdldCBpcyBhIGxpbmssIHByZXZlbnQgZGVmYXVsdCBhY3Rpb24uXHJcbiAgICAgICAgaWYoJHRhcmdldC5pcygnYScpKSB7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBJZiB0YXJnZXQgaXMgbm90IHRoZSA8bGk+IGVsZW1lbnQgKGllOiBhIGNoaWxkKSwgZmluZCB0aGUgPGxpPi5cclxuICAgICAgICBpZighJHRhcmdldC5pcygnbGknKSkge1xyXG4gICAgICAgICAgICAkdGFyZ2V0ID0gJHRhcmdldC5jbG9zZXN0KCdsaScpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdW5ldmVuT2Zmc2V0ID0gKF8uc2xpZGVDb3VudCAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAhPT0gMCk7XHJcbiAgICAgICAgaW5kZXhPZmZzZXQgPSB1bmV2ZW5PZmZzZXQgPyAwIDogKF8uc2xpZGVDb3VudCAtIF8uY3VycmVudFNsaWRlKSAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbDtcclxuXHJcbiAgICAgICAgc3dpdGNoIChldmVudC5kYXRhLm1lc3NhZ2UpIHtcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ3ByZXZpb3VzJzpcclxuICAgICAgICAgICAgICAgIHNsaWRlT2Zmc2V0ID0gaW5kZXhPZmZzZXQgPT09IDAgPyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgOiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC0gaW5kZXhPZmZzZXQ7XHJcbiAgICAgICAgICAgICAgICBpZiAoXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xyXG4gICAgICAgICAgICAgICAgICAgIF8uc2xpZGVIYW5kbGVyKF8uY3VycmVudFNsaWRlIC0gc2xpZGVPZmZzZXQsIGZhbHNlLCBkb250QW5pbWF0ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ25leHQnOlxyXG4gICAgICAgICAgICAgICAgc2xpZGVPZmZzZXQgPSBpbmRleE9mZnNldCA9PT0gMCA/IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCA6IGluZGV4T2Zmc2V0O1xyXG4gICAgICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgICAgICAgICBfLnNsaWRlSGFuZGxlcihfLmN1cnJlbnRTbGlkZSArIHNsaWRlT2Zmc2V0LCBmYWxzZSwgZG9udEFuaW1hdGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICdpbmRleCc6XHJcbiAgICAgICAgICAgICAgICB2YXIgaW5kZXggPSBldmVudC5kYXRhLmluZGV4ID09PSAwID8gMCA6XHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQuZGF0YS5pbmRleCB8fCAkdGFyZ2V0LmluZGV4KCkgKiBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XHJcblxyXG4gICAgICAgICAgICAgICAgXy5zbGlkZUhhbmRsZXIoXy5jaGVja05hdmlnYWJsZShpbmRleCksIGZhbHNlLCBkb250QW5pbWF0ZSk7XHJcbiAgICAgICAgICAgICAgICAkdGFyZ2V0LmNoaWxkcmVuKCkudHJpZ2dlcignZm9jdXMnKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuY2hlY2tOYXZpZ2FibGUgPSBmdW5jdGlvbihpbmRleCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIG5hdmlnYWJsZXMsIHByZXZOYXZpZ2FibGU7XHJcblxyXG4gICAgICAgIG5hdmlnYWJsZXMgPSBfLmdldE5hdmlnYWJsZUluZGV4ZXMoKTtcclxuICAgICAgICBwcmV2TmF2aWdhYmxlID0gMDtcclxuICAgICAgICBpZiAoaW5kZXggPiBuYXZpZ2FibGVzW25hdmlnYWJsZXMubGVuZ3RoIC0gMV0pIHtcclxuICAgICAgICAgICAgaW5kZXggPSBuYXZpZ2FibGVzW25hdmlnYWJsZXMubGVuZ3RoIC0gMV07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZm9yICh2YXIgbiBpbiBuYXZpZ2FibGVzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoaW5kZXggPCBuYXZpZ2FibGVzW25dKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaW5kZXggPSBwcmV2TmF2aWdhYmxlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcHJldk5hdmlnYWJsZSA9IG5hdmlnYWJsZXNbbl07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBpbmRleDtcclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmNsZWFuVXBFdmVudHMgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmRvdHMgJiYgXy4kZG90cyAhPT0gbnVsbCkge1xyXG5cclxuICAgICAgICAgICAgJCgnbGknLCBfLiRkb3RzKVxyXG4gICAgICAgICAgICAgICAgLm9mZignY2xpY2suc2xpY2snLCBfLmNoYW5nZVNsaWRlKVxyXG4gICAgICAgICAgICAgICAgLm9mZignbW91c2VlbnRlci5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIHRydWUpKVxyXG4gICAgICAgICAgICAgICAgLm9mZignbW91c2VsZWF2ZS5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIGZhbHNlKSk7XHJcblxyXG4gICAgICAgICAgICBpZiAoXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgIF8uJGRvdHMub2ZmKCdrZXlkb3duLnNsaWNrJywgXy5rZXlIYW5kbGVyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXy4kc2xpZGVyLm9mZignZm9jdXMuc2xpY2sgYmx1ci5zbGljaycpO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmFycm93cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcbiAgICAgICAgICAgIF8uJHByZXZBcnJvdyAmJiBfLiRwcmV2QXJyb3cub2ZmKCdjbGljay5zbGljaycsIF8uY2hhbmdlU2xpZGUpO1xyXG4gICAgICAgICAgICBfLiRuZXh0QXJyb3cgJiYgXy4kbmV4dEFycm93Lm9mZignY2xpY2suc2xpY2snLCBfLmNoYW5nZVNsaWRlKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuYWNjZXNzaWJpbGl0eSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgXy4kcHJldkFycm93ICYmIF8uJHByZXZBcnJvdy5vZmYoJ2tleWRvd24uc2xpY2snLCBfLmtleUhhbmRsZXIpO1xyXG4gICAgICAgICAgICAgICAgXy4kbmV4dEFycm93ICYmIF8uJG5leHRBcnJvdy5vZmYoJ2tleWRvd24uc2xpY2snLCBfLmtleUhhbmRsZXIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfLiRsaXN0Lm9mZigndG91Y2hzdGFydC5zbGljayBtb3VzZWRvd24uc2xpY2snLCBfLnN3aXBlSGFuZGxlcik7XHJcbiAgICAgICAgXy4kbGlzdC5vZmYoJ3RvdWNobW92ZS5zbGljayBtb3VzZW1vdmUuc2xpY2snLCBfLnN3aXBlSGFuZGxlcik7XHJcbiAgICAgICAgXy4kbGlzdC5vZmYoJ3RvdWNoZW5kLnNsaWNrIG1vdXNldXAuc2xpY2snLCBfLnN3aXBlSGFuZGxlcik7XHJcbiAgICAgICAgXy4kbGlzdC5vZmYoJ3RvdWNoY2FuY2VsLnNsaWNrIG1vdXNlbGVhdmUuc2xpY2snLCBfLnN3aXBlSGFuZGxlcik7XHJcblxyXG4gICAgICAgIF8uJGxpc3Qub2ZmKCdjbGljay5zbGljaycsIF8uY2xpY2tIYW5kbGVyKTtcclxuXHJcbiAgICAgICAgJChkb2N1bWVudCkub2ZmKF8udmlzaWJpbGl0eUNoYW5nZSwgXy52aXNpYmlsaXR5KTtcclxuXHJcbiAgICAgICAgXy5jbGVhblVwU2xpZGVFdmVudHMoKTtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5hY2Nlc3NpYmlsaXR5ID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIF8uJGxpc3Qub2ZmKCdrZXlkb3duLnNsaWNrJywgXy5rZXlIYW5kbGVyKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZm9jdXNPblNlbGVjdCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAkKF8uJHNsaWRlVHJhY2spLmNoaWxkcmVuKCkub2ZmKCdjbGljay5zbGljaycsIF8uc2VsZWN0SGFuZGxlcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAkKHdpbmRvdykub2ZmKCdvcmllbnRhdGlvbmNoYW5nZS5zbGljay5zbGljay0nICsgXy5pbnN0YW5jZVVpZCwgXy5vcmllbnRhdGlvbkNoYW5nZSk7XHJcblxyXG4gICAgICAgICQod2luZG93KS5vZmYoJ3Jlc2l6ZS5zbGljay5zbGljay0nICsgXy5pbnN0YW5jZVVpZCwgXy5yZXNpemUpO1xyXG5cclxuICAgICAgICAkKCdbZHJhZ2dhYmxlIT10cnVlXScsIF8uJHNsaWRlVHJhY2spLm9mZignZHJhZ3N0YXJ0JywgXy5wcmV2ZW50RGVmYXVsdCk7XHJcblxyXG4gICAgICAgICQod2luZG93KS5vZmYoJ2xvYWQuc2xpY2suc2xpY2stJyArIF8uaW5zdGFuY2VVaWQsIF8uc2V0UG9zaXRpb24pO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmNsZWFuVXBTbGlkZUV2ZW50cyA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIF8uJGxpc3Qub2ZmKCdtb3VzZWVudGVyLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgdHJ1ZSkpO1xyXG4gICAgICAgIF8uJGxpc3Qub2ZmKCdtb3VzZWxlYXZlLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgZmFsc2UpKTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5jbGVhblVwUm93cyA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsIG9yaWdpbmFsU2xpZGVzO1xyXG5cclxuICAgICAgICBpZihfLm9wdGlvbnMucm93cyA+IDApIHtcclxuICAgICAgICAgICAgb3JpZ2luYWxTbGlkZXMgPSBfLiRzbGlkZXMuY2hpbGRyZW4oKS5jaGlsZHJlbigpO1xyXG4gICAgICAgICAgICBvcmlnaW5hbFNsaWRlcy5yZW1vdmVBdHRyKCdzdHlsZScpO1xyXG4gICAgICAgICAgICBfLiRzbGlkZXIuZW1wdHkoKS5hcHBlbmQob3JpZ2luYWxTbGlkZXMpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5jbGlja0hhbmRsZXIgPSBmdW5jdGlvbihldmVudCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmIChfLnNob3VsZENsaWNrID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICBldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbihyZWZyZXNoKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgXy5hdXRvUGxheUNsZWFyKCk7XHJcblxyXG4gICAgICAgIF8udG91Y2hPYmplY3QgPSB7fTtcclxuXHJcbiAgICAgICAgXy5jbGVhblVwRXZlbnRzKCk7XHJcblxyXG4gICAgICAgICQoJy5zbGljay1jbG9uZWQnLCBfLiRzbGlkZXIpLmRldGFjaCgpO1xyXG5cclxuICAgICAgICBpZiAoXy4kZG90cykge1xyXG4gICAgICAgICAgICBfLiRkb3RzLnJlbW92ZSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCBfLiRwcmV2QXJyb3cgJiYgXy4kcHJldkFycm93Lmxlbmd0aCApIHtcclxuXHJcbiAgICAgICAgICAgIF8uJHByZXZBcnJvd1xyXG4gICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdzbGljay1kaXNhYmxlZCBzbGljay1hcnJvdyBzbGljay1oaWRkZW4nKVxyXG4gICAgICAgICAgICAgICAgLnJlbW92ZUF0dHIoJ2FyaWEtaGlkZGVuIGFyaWEtZGlzYWJsZWQgdGFiaW5kZXgnKVxyXG4gICAgICAgICAgICAgICAgLmNzcygnZGlzcGxheScsJycpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCBfLmh0bWxFeHByLnRlc3QoIF8ub3B0aW9ucy5wcmV2QXJyb3cgKSkge1xyXG4gICAgICAgICAgICAgICAgXy4kcHJldkFycm93LnJlbW92ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIF8uJG5leHRBcnJvdyAmJiBfLiRuZXh0QXJyb3cubGVuZ3RoICkge1xyXG5cclxuICAgICAgICAgICAgXy4kbmV4dEFycm93XHJcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3NsaWNrLWRpc2FibGVkIHNsaWNrLWFycm93IHNsaWNrLWhpZGRlbicpXHJcbiAgICAgICAgICAgICAgICAucmVtb3ZlQXR0cignYXJpYS1oaWRkZW4gYXJpYS1kaXNhYmxlZCB0YWJpbmRleCcpXHJcbiAgICAgICAgICAgICAgICAuY3NzKCdkaXNwbGF5JywnJyk7XHJcblxyXG4gICAgICAgICAgICBpZiAoIF8uaHRtbEV4cHIudGVzdCggXy5vcHRpb25zLm5leHRBcnJvdyApKSB7XHJcbiAgICAgICAgICAgICAgICBfLiRuZXh0QXJyb3cucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBpZiAoXy4kc2xpZGVzKSB7XHJcblxyXG4gICAgICAgICAgICBfLiRzbGlkZXNcclxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnc2xpY2stc2xpZGUgc2xpY2stYWN0aXZlIHNsaWNrLWNlbnRlciBzbGljay12aXNpYmxlIHNsaWNrLWN1cnJlbnQnKVxyXG4gICAgICAgICAgICAgICAgLnJlbW92ZUF0dHIoJ2FyaWEtaGlkZGVuJylcclxuICAgICAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdkYXRhLXNsaWNrLWluZGV4JylcclxuICAgICAgICAgICAgICAgIC5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5hdHRyKCdzdHlsZScsICQodGhpcykuZGF0YSgnb3JpZ2luYWxTdHlsaW5nJykpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5zbGlkZSkuZGV0YWNoKCk7XHJcblxyXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmRldGFjaCgpO1xyXG5cclxuICAgICAgICAgICAgXy4kbGlzdC5kZXRhY2goKTtcclxuXHJcbiAgICAgICAgICAgIF8uJHNsaWRlci5hcHBlbmQoXy4kc2xpZGVzKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF8uY2xlYW5VcFJvd3MoKTtcclxuXHJcbiAgICAgICAgXy4kc2xpZGVyLnJlbW92ZUNsYXNzKCdzbGljay1zbGlkZXInKTtcclxuICAgICAgICBfLiRzbGlkZXIucmVtb3ZlQ2xhc3MoJ3NsaWNrLWluaXRpYWxpemVkJyk7XHJcbiAgICAgICAgXy4kc2xpZGVyLnJlbW92ZUNsYXNzKCdzbGljay1kb3R0ZWQnKTtcclxuXHJcbiAgICAgICAgXy51bnNsaWNrZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICBpZighcmVmcmVzaCkge1xyXG4gICAgICAgICAgICBfLiRzbGlkZXIudHJpZ2dlcignZGVzdHJveScsIFtfXSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmRpc2FibGVUcmFuc2l0aW9uID0gZnVuY3Rpb24oc2xpZGUpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uID0ge307XHJcblxyXG4gICAgICAgIHRyYW5zaXRpb25bXy50cmFuc2l0aW9uVHlwZV0gPSAnJztcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mYWRlID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyh0cmFuc2l0aW9uKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBfLiRzbGlkZXMuZXEoc2xpZGUpLmNzcyh0cmFuc2l0aW9uKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuZmFkZVNsaWRlID0gZnVuY3Rpb24oc2xpZGVJbmRleCwgY2FsbGJhY2spIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBpZiAoXy5jc3NUcmFuc2l0aW9ucyA9PT0gZmFsc2UpIHtcclxuXHJcbiAgICAgICAgICAgIF8uJHNsaWRlcy5lcShzbGlkZUluZGV4KS5jc3Moe1xyXG4gICAgICAgICAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgXy4kc2xpZGVzLmVxKHNsaWRlSW5kZXgpLmFuaW1hdGUoe1xyXG4gICAgICAgICAgICAgICAgb3BhY2l0eTogMVxyXG4gICAgICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQsIF8ub3B0aW9ucy5lYXNpbmcsIGNhbGxiYWNrKTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgIF8uYXBwbHlUcmFuc2l0aW9uKHNsaWRlSW5kZXgpO1xyXG5cclxuICAgICAgICAgICAgXy4kc2xpZGVzLmVxKHNsaWRlSW5kZXgpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxyXG4gICAgICAgICAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBfLmRpc2FibGVUcmFuc2l0aW9uKHNsaWRlSW5kZXgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5mYWRlU2xpZGVPdXQgPSBmdW5jdGlvbihzbGlkZUluZGV4KSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKF8uY3NzVHJhbnNpdGlvbnMgPT09IGZhbHNlKSB7XHJcblxyXG4gICAgICAgICAgICBfLiRzbGlkZXMuZXEoc2xpZGVJbmRleCkuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxyXG4gICAgICAgICAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4IC0gMlxyXG4gICAgICAgICAgICB9LCBfLm9wdGlvbnMuc3BlZWQsIF8ub3B0aW9ucy5lYXNpbmcpO1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgXy5hcHBseVRyYW5zaXRpb24oc2xpZGVJbmRleCk7XHJcblxyXG4gICAgICAgICAgICBfLiRzbGlkZXMuZXEoc2xpZGVJbmRleCkuY3NzKHtcclxuICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAsXHJcbiAgICAgICAgICAgICAgICB6SW5kZXg6IF8ub3B0aW9ucy56SW5kZXggLSAyXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuZmlsdGVyU2xpZGVzID0gU2xpY2sucHJvdG90eXBlLnNsaWNrRmlsdGVyID0gZnVuY3Rpb24oZmlsdGVyKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKGZpbHRlciAhPT0gbnVsbCkge1xyXG5cclxuICAgICAgICAgICAgXy4kc2xpZGVzQ2FjaGUgPSBfLiRzbGlkZXM7XHJcblxyXG4gICAgICAgICAgICBfLnVubG9hZCgpO1xyXG5cclxuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmRldGFjaCgpO1xyXG5cclxuICAgICAgICAgICAgXy4kc2xpZGVzQ2FjaGUuZmlsdGVyKGZpbHRlcikuYXBwZW5kVG8oXy4kc2xpZGVUcmFjayk7XHJcblxyXG4gICAgICAgICAgICBfLnJlaW5pdCgpO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuZm9jdXNIYW5kbGVyID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgXy4kc2xpZGVyXHJcbiAgICAgICAgICAgIC5vZmYoJ2ZvY3VzLnNsaWNrIGJsdXIuc2xpY2snKVxyXG4gICAgICAgICAgICAub24oJ2ZvY3VzLnNsaWNrIGJsdXIuc2xpY2snLCAnKicsIGZ1bmN0aW9uKGV2ZW50KSB7XHJcblxyXG4gICAgICAgICAgICBldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgdmFyICRzZiA9ICQodGhpcyk7XHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICAgICAgICAgIGlmKCBfLm9wdGlvbnMucGF1c2VPbkZvY3VzICkge1xyXG4gICAgICAgICAgICAgICAgICAgIF8uZm9jdXNzZWQgPSAkc2YuaXMoJzpmb2N1cycpO1xyXG4gICAgICAgICAgICAgICAgICAgIF8uYXV0b1BsYXkoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0sIDApO1xyXG5cclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmdldEN1cnJlbnQgPSBTbGljay5wcm90b3R5cGUuc2xpY2tDdXJyZW50U2xpZGUgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG4gICAgICAgIHJldHVybiBfLmN1cnJlbnRTbGlkZTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5nZXREb3RDb3VudCA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIHZhciBicmVha1BvaW50ID0gMDtcclxuICAgICAgICB2YXIgY291bnRlciA9IDA7XHJcbiAgICAgICAgdmFyIHBhZ2VyUXR5ID0gMDtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBpZiAoXy5zbGlkZUNvdW50IDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgICAgICArK3BhZ2VyUXR5O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgd2hpbGUgKGJyZWFrUG9pbnQgPCBfLnNsaWRlQ291bnQpIHtcclxuICAgICAgICAgICAgICAgICAgICArK3BhZ2VyUXR5O1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrUG9pbnQgPSBjb3VudGVyICsgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvdW50ZXIgKz0gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsIDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgPyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgOiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBwYWdlclF0eSA9IF8uc2xpZGVDb3VudDtcclxuICAgICAgICB9IGVsc2UgaWYoIV8ub3B0aW9ucy5hc05hdkZvcikge1xyXG4gICAgICAgICAgICBwYWdlclF0eSA9IDEgKyBNYXRoLmNlaWwoKF8uc2xpZGVDb3VudCAtIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIC8gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsKTtcclxuICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICAgIHdoaWxlIChicmVha1BvaW50IDwgXy5zbGlkZUNvdW50KSB7XHJcbiAgICAgICAgICAgICAgICArK3BhZ2VyUXR5O1xyXG4gICAgICAgICAgICAgICAgYnJlYWtQb2ludCA9IGNvdW50ZXIgKyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XHJcbiAgICAgICAgICAgICAgICBjb3VudGVyICs9IF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID8gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsIDogXy5vcHRpb25zLnNsaWRlc1RvU2hvdztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHBhZ2VyUXR5IC0gMTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5nZXRMZWZ0ID0gZnVuY3Rpb24oc2xpZGVJbmRleCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIHRhcmdldExlZnQsXHJcbiAgICAgICAgICAgIHZlcnRpY2FsSGVpZ2h0LFxyXG4gICAgICAgICAgICB2ZXJ0aWNhbE9mZnNldCA9IDAsXHJcbiAgICAgICAgICAgIHRhcmdldFNsaWRlLFxyXG4gICAgICAgICAgICBjb2VmO1xyXG5cclxuICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gMDtcclxuICAgICAgICB2ZXJ0aWNhbEhlaWdodCA9IF8uJHNsaWRlcy5maXJzdCgpLm91dGVySGVpZ2h0KHRydWUpO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmluZmluaXRlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIGlmIChfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcbiAgICAgICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gKF8uc2xpZGVXaWR0aCAqIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpICogLTE7XHJcbiAgICAgICAgICAgICAgICBjb2VmID0gLTFcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSB0cnVlICYmIF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgPT09IDIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29lZiA9IC0xLjU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZWYgPSAtMlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHZlcnRpY2FsT2Zmc2V0ID0gKHZlcnRpY2FsSGVpZ2h0ICogXy5vcHRpb25zLnNsaWRlc1RvU2hvdykgKiBjb2VmO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChfLnNsaWRlQ291bnQgJSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgIT09IDApIHtcclxuICAgICAgICAgICAgICAgIGlmIChzbGlkZUluZGV4ICsgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsID4gXy5zbGlkZUNvdW50ICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc2xpZGVJbmRleCA+IF8uc2xpZGVDb3VudCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gKChfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC0gKHNsaWRlSW5kZXggLSBfLnNsaWRlQ291bnQpKSAqIF8uc2xpZGVXaWR0aCkgKiAtMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmVydGljYWxPZmZzZXQgPSAoKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLSAoc2xpZGVJbmRleCAtIF8uc2xpZGVDb3VudCkpICogdmVydGljYWxIZWlnaHQpICogLTE7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXy5zbGlkZU9mZnNldCA9ICgoXy5zbGlkZUNvdW50ICUgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsKSAqIF8uc2xpZGVXaWR0aCkgKiAtMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmVydGljYWxPZmZzZXQgPSAoKF8uc2xpZGVDb3VudCAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCkgKiB2ZXJ0aWNhbEhlaWdodCkgKiAtMTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAoc2xpZGVJbmRleCArIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgPiBfLnNsaWRlQ291bnQpIHtcclxuICAgICAgICAgICAgICAgIF8uc2xpZGVPZmZzZXQgPSAoKHNsaWRlSW5kZXggKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSAtIF8uc2xpZGVDb3VudCkgKiBfLnNsaWRlV2lkdGg7XHJcbiAgICAgICAgICAgICAgICB2ZXJ0aWNhbE9mZnNldCA9ICgoc2xpZGVJbmRleCArIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIC0gXy5zbGlkZUNvdW50KSAqIHZlcnRpY2FsSGVpZ2h0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoXy5zbGlkZUNvdW50IDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgXy5zbGlkZU9mZnNldCA9IDA7XHJcbiAgICAgICAgICAgIHZlcnRpY2FsT2Zmc2V0ID0gMDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPD0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xyXG4gICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ID0gKChfLnNsaWRlV2lkdGggKiBNYXRoLmZsb29yKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpKSAvIDIpIC0gKChfLnNsaWRlV2lkdGggKiBfLnNsaWRlQ291bnQpIC8gMik7XHJcbiAgICAgICAgfSBlbHNlIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSAmJiBfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgXy5zbGlkZU9mZnNldCArPSBfLnNsaWRlV2lkdGggKiBNYXRoLmZsb29yKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyKSAtIF8uc2xpZGVXaWR0aDtcclxuICAgICAgICB9IGVsc2UgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIF8uc2xpZGVPZmZzZXQgPSAwO1xyXG4gICAgICAgICAgICBfLnNsaWRlT2Zmc2V0ICs9IF8uc2xpZGVXaWR0aCAqIE1hdGguZmxvb3IoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgdGFyZ2V0TGVmdCA9ICgoc2xpZGVJbmRleCAqIF8uc2xpZGVXaWR0aCkgKiAtMSkgKyBfLnNsaWRlT2Zmc2V0O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRhcmdldExlZnQgPSAoKHNsaWRlSW5kZXggKiB2ZXJ0aWNhbEhlaWdodCkgKiAtMSkgKyB2ZXJ0aWNhbE9mZnNldDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMudmFyaWFibGVXaWR0aCA9PT0gdHJ1ZSkge1xyXG5cclxuICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IHx8IF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgIHRhcmdldFNsaWRlID0gXy4kc2xpZGVUcmFjay5jaGlsZHJlbignLnNsaWNrLXNsaWRlJykuZXEoc2xpZGVJbmRleCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0YXJnZXRTbGlkZSA9IF8uJHNsaWRlVHJhY2suY2hpbGRyZW4oJy5zbGljay1zbGlkZScpLmVxKHNsaWRlSW5kZXggKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0YXJnZXRTbGlkZVswXSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRhcmdldExlZnQgPSAoXy4kc2xpZGVUcmFjay53aWR0aCgpIC0gdGFyZ2V0U2xpZGVbMF0ub2Zmc2V0TGVmdCAtIHRhcmdldFNsaWRlLndpZHRoKCkpICogLTE7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRhcmdldExlZnQgPSAgMDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRhcmdldExlZnQgPSB0YXJnZXRTbGlkZVswXSA/IHRhcmdldFNsaWRlWzBdLm9mZnNldExlZnQgKiAtMSA6IDA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93IHx8IF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB0YXJnZXRTbGlkZSA9IF8uJHNsaWRlVHJhY2suY2hpbGRyZW4oJy5zbGljay1zbGlkZScpLmVxKHNsaWRlSW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0YXJnZXRTbGlkZSA9IF8uJHNsaWRlVHJhY2suY2hpbGRyZW4oJy5zbGljay1zbGlkZScpLmVxKHNsaWRlSW5kZXggKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICsgMSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0U2xpZGVbMF0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0TGVmdCA9IChfLiRzbGlkZVRyYWNrLndpZHRoKCkgLSB0YXJnZXRTbGlkZVswXS5vZmZzZXRMZWZ0IC0gdGFyZ2V0U2xpZGUud2lkdGgoKSkgKiAtMTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRMZWZ0ID0gIDA7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0YXJnZXRMZWZ0ID0gdGFyZ2V0U2xpZGVbMF0gPyB0YXJnZXRTbGlkZVswXS5vZmZzZXRMZWZ0ICogLTEgOiAwO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRhcmdldExlZnQgKz0gKF8uJGxpc3Qud2lkdGgoKSAtIHRhcmdldFNsaWRlLm91dGVyV2lkdGgoKSkgLyAyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGFyZ2V0TGVmdDtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5nZXRPcHRpb24gPSBTbGljay5wcm90b3R5cGUuc2xpY2tHZXRPcHRpb24gPSBmdW5jdGlvbihvcHRpb24pIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICByZXR1cm4gXy5vcHRpb25zW29wdGlvbl07XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuZ2V0TmF2aWdhYmxlSW5kZXhlcyA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIGJyZWFrUG9pbnQgPSAwLFxyXG4gICAgICAgICAgICBjb3VudGVyID0gMCxcclxuICAgICAgICAgICAgaW5kZXhlcyA9IFtdLFxyXG4gICAgICAgICAgICBtYXg7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIG1heCA9IF8uc2xpZGVDb3VudDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBicmVha1BvaW50ID0gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsICogLTE7XHJcbiAgICAgICAgICAgIGNvdW50ZXIgPSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgKiAtMTtcclxuICAgICAgICAgICAgbWF4ID0gXy5zbGlkZUNvdW50ICogMjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHdoaWxlIChicmVha1BvaW50IDwgbWF4KSB7XHJcbiAgICAgICAgICAgIGluZGV4ZXMucHVzaChicmVha1BvaW50KTtcclxuICAgICAgICAgICAgYnJlYWtQb2ludCA9IGNvdW50ZXIgKyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGw7XHJcbiAgICAgICAgICAgIGNvdW50ZXIgKz0gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsIDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgPyBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgOiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGluZGV4ZXM7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuZ2V0U2xpY2sgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuZ2V0U2xpZGVDb3VudCA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIHNsaWRlc1RyYXZlcnNlZCwgc3dpcGVkU2xpZGUsIGNlbnRlck9mZnNldDtcclxuXHJcbiAgICAgICAgY2VudGVyT2Zmc2V0ID0gXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUgPyBfLnNsaWRlV2lkdGggKiBNYXRoLmZsb29yKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyKSA6IDA7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuc3dpcGVUb1NsaWRlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suZmluZCgnLnNsaWNrLXNsaWRlJykuZWFjaChmdW5jdGlvbihpbmRleCwgc2xpZGUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChzbGlkZS5vZmZzZXRMZWZ0IC0gY2VudGVyT2Zmc2V0ICsgKCQoc2xpZGUpLm91dGVyV2lkdGgoKSAvIDIpID4gKF8uc3dpcGVMZWZ0ICogLTEpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3dpcGVkU2xpZGUgPSBzbGlkZTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgc2xpZGVzVHJhdmVyc2VkID0gTWF0aC5hYnMoJChzd2lwZWRTbGlkZSkuYXR0cignZGF0YS1zbGljay1pbmRleCcpIC0gXy5jdXJyZW50U2xpZGUpIHx8IDE7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gc2xpZGVzVHJhdmVyc2VkO1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5nb1RvID0gU2xpY2sucHJvdG90eXBlLnNsaWNrR29UbyA9IGZ1bmN0aW9uKHNsaWRlLCBkb250QW5pbWF0ZSkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIF8uY2hhbmdlU2xpZGUoe1xyXG4gICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiAnaW5kZXgnLFxyXG4gICAgICAgICAgICAgICAgaW5kZXg6IHBhcnNlSW50KHNsaWRlKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgZG9udEFuaW1hdGUpO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbihjcmVhdGlvbikge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmICghJChfLiRzbGlkZXIpLmhhc0NsYXNzKCdzbGljay1pbml0aWFsaXplZCcpKSB7XHJcblxyXG4gICAgICAgICAgICAkKF8uJHNsaWRlcikuYWRkQ2xhc3MoJ3NsaWNrLWluaXRpYWxpemVkJyk7XHJcblxyXG4gICAgICAgICAgICBfLmJ1aWxkUm93cygpO1xyXG4gICAgICAgICAgICBfLmJ1aWxkT3V0KCk7XHJcbiAgICAgICAgICAgIF8uc2V0UHJvcHMoKTtcclxuICAgICAgICAgICAgXy5zdGFydExvYWQoKTtcclxuICAgICAgICAgICAgXy5sb2FkU2xpZGVyKCk7XHJcbiAgICAgICAgICAgIF8uaW5pdGlhbGl6ZUV2ZW50cygpO1xyXG4gICAgICAgICAgICBfLnVwZGF0ZUFycm93cygpO1xyXG4gICAgICAgICAgICBfLnVwZGF0ZURvdHMoKTtcclxuICAgICAgICAgICAgXy5jaGVja1Jlc3BvbnNpdmUodHJ1ZSk7XHJcbiAgICAgICAgICAgIF8uZm9jdXNIYW5kbGVyKCk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNyZWF0aW9uKSB7XHJcbiAgICAgICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdpbml0JywgW19dKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuYWNjZXNzaWJpbGl0eSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBfLmluaXRBREEoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggXy5vcHRpb25zLmF1dG9wbGF5ICkge1xyXG5cclxuICAgICAgICAgICAgXy5wYXVzZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgXy5hdXRvUGxheSgpO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuaW5pdEFEQSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBfID0gdGhpcyxcclxuICAgICAgICAgICAgICAgIG51bURvdEdyb3VwcyA9IE1hdGguY2VpbChfLnNsaWRlQ291bnQgLyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSxcclxuICAgICAgICAgICAgICAgIHRhYkNvbnRyb2xJbmRleGVzID0gXy5nZXROYXZpZ2FibGVJbmRleGVzKCkuZmlsdGVyKGZ1bmN0aW9uKHZhbCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAodmFsID49IDApICYmICh2YWwgPCBfLnNsaWRlQ291bnQpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIF8uJHNsaWRlcy5hZGQoXy4kc2xpZGVUcmFjay5maW5kKCcuc2xpY2stY2xvbmVkJykpLmF0dHIoe1xyXG4gICAgICAgICAgICAnYXJpYS1oaWRkZW4nOiAndHJ1ZScsXHJcbiAgICAgICAgICAgICd0YWJpbmRleCc6ICctMSdcclxuICAgICAgICB9KS5maW5kKCdhLCBpbnB1dCwgYnV0dG9uLCBzZWxlY3QnKS5hdHRyKHtcclxuICAgICAgICAgICAgJ3RhYmluZGV4JzogJy0xJ1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoXy4kZG90cyAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICBfLiRzbGlkZXMubm90KF8uJHNsaWRlVHJhY2suZmluZCgnLnNsaWNrLWNsb25lZCcpKS5lYWNoKGZ1bmN0aW9uKGkpIHtcclxuICAgICAgICAgICAgICAgIHZhciBzbGlkZUNvbnRyb2xJbmRleCA9IHRhYkNvbnRyb2xJbmRleGVzLmluZGV4T2YoaSk7XHJcblxyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5hdHRyKHtcclxuICAgICAgICAgICAgICAgICAgICAncm9sZSc6ICd0YWJwYW5lbCcsXHJcbiAgICAgICAgICAgICAgICAgICAgJ2lkJzogJ3NsaWNrLXNsaWRlJyArIF8uaW5zdGFuY2VVaWQgKyBpLFxyXG4gICAgICAgICAgICAgICAgICAgICd0YWJpbmRleCc6IC0xXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoc2xpZGVDb250cm9sSW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICB2YXIgYXJpYUJ1dHRvbkNvbnRyb2wgPSAnc2xpY2stc2xpZGUtY29udHJvbCcgKyBfLmluc3RhbmNlVWlkICsgc2xpZGVDb250cm9sSW5kZXhcclxuICAgICAgICAgICAgICAgICAgIGlmICgkKCcjJyArIGFyaWFCdXR0b25Db250cm9sKS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5hdHRyKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICdhcmlhLWRlc2NyaWJlZGJ5JzogYXJpYUJ1dHRvbkNvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgXy4kZG90cy5hdHRyKCdyb2xlJywgJ3RhYmxpc3QnKS5maW5kKCdsaScpLmVhY2goZnVuY3Rpb24oaSkge1xyXG4gICAgICAgICAgICAgICAgdmFyIG1hcHBlZFNsaWRlSW5kZXggPSB0YWJDb250cm9sSW5kZXhlc1tpXTtcclxuXHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmF0dHIoe1xyXG4gICAgICAgICAgICAgICAgICAgICdyb2xlJzogJ3ByZXNlbnRhdGlvbidcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICQodGhpcykuZmluZCgnYnV0dG9uJykuZmlyc3QoKS5hdHRyKHtcclxuICAgICAgICAgICAgICAgICAgICAncm9sZSc6ICd0YWInLFxyXG4gICAgICAgICAgICAgICAgICAgICdpZCc6ICdzbGljay1zbGlkZS1jb250cm9sJyArIF8uaW5zdGFuY2VVaWQgKyBpLFxyXG4gICAgICAgICAgICAgICAgICAgICdhcmlhLWNvbnRyb2xzJzogJ3NsaWNrLXNsaWRlJyArIF8uaW5zdGFuY2VVaWQgKyBtYXBwZWRTbGlkZUluZGV4LFxyXG4gICAgICAgICAgICAgICAgICAgICdhcmlhLWxhYmVsJzogKGkgKyAxKSArICcgb2YgJyArIG51bURvdEdyb3VwcyxcclxuICAgICAgICAgICAgICAgICAgICAnYXJpYS1zZWxlY3RlZCc6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgJ3RhYmluZGV4JzogJy0xJ1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9KS5lcShfLmN1cnJlbnRTbGlkZSkuZmluZCgnYnV0dG9uJykuYXR0cih7XHJcbiAgICAgICAgICAgICAgICAnYXJpYS1zZWxlY3RlZCc6ICd0cnVlJyxcclxuICAgICAgICAgICAgICAgICd0YWJpbmRleCc6ICcwJ1xyXG4gICAgICAgICAgICB9KS5lbmQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZvciAodmFyIGk9Xy5jdXJyZW50U2xpZGUsIG1heD1pK18ub3B0aW9ucy5zbGlkZXNUb1Nob3c7IGkgPCBtYXg7IGkrKykge1xyXG4gICAgICAgICAgaWYgKF8ub3B0aW9ucy5mb2N1c09uQ2hhbmdlKSB7XHJcbiAgICAgICAgICAgIF8uJHNsaWRlcy5lcShpKS5hdHRyKHsndGFiaW5kZXgnOiAnMCd9KTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIF8uJHNsaWRlcy5lcShpKS5yZW1vdmVBdHRyKCd0YWJpbmRleCcpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXy5hY3RpdmF0ZUFEQSgpO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmluaXRBcnJvd0V2ZW50cyA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuYXJyb3dzID09PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgXy4kcHJldkFycm93XHJcbiAgICAgICAgICAgICAgIC5vZmYoJ2NsaWNrLnNsaWNrJylcclxuICAgICAgICAgICAgICAgLm9uKCdjbGljay5zbGljaycsIHtcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAncHJldmlvdXMnXHJcbiAgICAgICAgICAgICAgIH0sIF8uY2hhbmdlU2xpZGUpO1xyXG4gICAgICAgICAgICBfLiRuZXh0QXJyb3dcclxuICAgICAgICAgICAgICAgLm9mZignY2xpY2suc2xpY2snKVxyXG4gICAgICAgICAgICAgICAub24oJ2NsaWNrLnNsaWNrJywge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICduZXh0J1xyXG4gICAgICAgICAgICAgICB9LCBfLmNoYW5nZVNsaWRlKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuYWNjZXNzaWJpbGl0eSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgXy4kcHJldkFycm93Lm9uKCdrZXlkb3duLnNsaWNrJywgXy5rZXlIYW5kbGVyKTtcclxuICAgICAgICAgICAgICAgIF8uJG5leHRBcnJvdy5vbigna2V5ZG93bi5zbGljaycsIF8ua2V5SGFuZGxlcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuaW5pdERvdEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZG90cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcbiAgICAgICAgICAgICQoJ2xpJywgXy4kZG90cykub24oJ2NsaWNrLnNsaWNrJywge1xyXG4gICAgICAgICAgICAgICAgbWVzc2FnZTogJ2luZGV4J1xyXG4gICAgICAgICAgICB9LCBfLmNoYW5nZVNsaWRlKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuYWNjZXNzaWJpbGl0eSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgXy4kZG90cy5vbigna2V5ZG93bi5zbGljaycsIF8ua2V5SGFuZGxlcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZG90cyA9PT0gdHJ1ZSAmJiBfLm9wdGlvbnMucGF1c2VPbkRvdHNIb3ZlciA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcblxyXG4gICAgICAgICAgICAkKCdsaScsIF8uJGRvdHMpXHJcbiAgICAgICAgICAgICAgICAub24oJ21vdXNlZW50ZXIuc2xpY2snLCAkLnByb3h5KF8uaW50ZXJydXB0LCBfLCB0cnVlKSlcclxuICAgICAgICAgICAgICAgIC5vbignbW91c2VsZWF2ZS5zbGljaycsICQucHJveHkoXy5pbnRlcnJ1cHQsIF8sIGZhbHNlKSk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5pbml0U2xpZGVFdmVudHMgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBpZiAoIF8ub3B0aW9ucy5wYXVzZU9uSG92ZXIgKSB7XHJcblxyXG4gICAgICAgICAgICBfLiRsaXN0Lm9uKCdtb3VzZWVudGVyLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgdHJ1ZSkpO1xyXG4gICAgICAgICAgICBfLiRsaXN0Lm9uKCdtb3VzZWxlYXZlLnNsaWNrJywgJC5wcm94eShfLmludGVycnVwdCwgXywgZmFsc2UpKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmluaXRpYWxpemVFdmVudHMgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLmluaXRBcnJvd0V2ZW50cygpO1xyXG5cclxuICAgICAgICBfLmluaXREb3RFdmVudHMoKTtcclxuICAgICAgICBfLmluaXRTbGlkZUV2ZW50cygpO1xyXG5cclxuICAgICAgICBfLiRsaXN0Lm9uKCd0b3VjaHN0YXJ0LnNsaWNrIG1vdXNlZG93bi5zbGljaycsIHtcclxuICAgICAgICAgICAgYWN0aW9uOiAnc3RhcnQnXHJcbiAgICAgICAgfSwgXy5zd2lwZUhhbmRsZXIpO1xyXG4gICAgICAgIF8uJGxpc3Qub24oJ3RvdWNobW92ZS5zbGljayBtb3VzZW1vdmUuc2xpY2snLCB7XHJcbiAgICAgICAgICAgIGFjdGlvbjogJ21vdmUnXHJcbiAgICAgICAgfSwgXy5zd2lwZUhhbmRsZXIpO1xyXG4gICAgICAgIF8uJGxpc3Qub24oJ3RvdWNoZW5kLnNsaWNrIG1vdXNldXAuc2xpY2snLCB7XHJcbiAgICAgICAgICAgIGFjdGlvbjogJ2VuZCdcclxuICAgICAgICB9LCBfLnN3aXBlSGFuZGxlcik7XHJcbiAgICAgICAgXy4kbGlzdC5vbigndG91Y2hjYW5jZWwuc2xpY2sgbW91c2VsZWF2ZS5zbGljaycsIHtcclxuICAgICAgICAgICAgYWN0aW9uOiAnZW5kJ1xyXG4gICAgICAgIH0sIF8uc3dpcGVIYW5kbGVyKTtcclxuXHJcbiAgICAgICAgXy4kbGlzdC5vbignY2xpY2suc2xpY2snLCBfLmNsaWNrSGFuZGxlcik7XHJcblxyXG4gICAgICAgICQoZG9jdW1lbnQpLm9uKF8udmlzaWJpbGl0eUNoYW5nZSwgJC5wcm94eShfLnZpc2liaWxpdHksIF8pKTtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5hY2Nlc3NpYmlsaXR5ID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIF8uJGxpc3Qub24oJ2tleWRvd24uc2xpY2snLCBfLmtleUhhbmRsZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mb2N1c09uU2VsZWN0ID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICQoXy4kc2xpZGVUcmFjaykuY2hpbGRyZW4oKS5vbignY2xpY2suc2xpY2snLCBfLnNlbGVjdEhhbmRsZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJCh3aW5kb3cpLm9uKCdvcmllbnRhdGlvbmNoYW5nZS5zbGljay5zbGljay0nICsgXy5pbnN0YW5jZVVpZCwgJC5wcm94eShfLm9yaWVudGF0aW9uQ2hhbmdlLCBfKSk7XHJcblxyXG4gICAgICAgICQod2luZG93KS5vbigncmVzaXplLnNsaWNrLnNsaWNrLScgKyBfLmluc3RhbmNlVWlkLCAkLnByb3h5KF8ucmVzaXplLCBfKSk7XHJcblxyXG4gICAgICAgICQoJ1tkcmFnZ2FibGUhPXRydWVdJywgXy4kc2xpZGVUcmFjaykub24oJ2RyYWdzdGFydCcsIF8ucHJldmVudERlZmF1bHQpO1xyXG5cclxuICAgICAgICAkKHdpbmRvdykub24oJ2xvYWQuc2xpY2suc2xpY2stJyArIF8uaW5zdGFuY2VVaWQsIF8uc2V0UG9zaXRpb24pO1xyXG4gICAgICAgICQoXy5zZXRQb3NpdGlvbik7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuaW5pdFVJID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5hcnJvd3MgPT09IHRydWUgJiYgXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xyXG5cclxuICAgICAgICAgICAgXy4kcHJldkFycm93LnNob3coKTtcclxuICAgICAgICAgICAgXy4kbmV4dEFycm93LnNob3coKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmRvdHMgPT09IHRydWUgJiYgXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xyXG5cclxuICAgICAgICAgICAgXy4kZG90cy5zaG93KCk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5rZXlIYW5kbGVyID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG4gICAgICAgICAvL0RvbnQgc2xpZGUgaWYgdGhlIGN1cnNvciBpcyBpbnNpZGUgdGhlIGZvcm0gZmllbGRzIGFuZCBhcnJvdyBrZXlzIGFyZSBwcmVzc2VkXHJcbiAgICAgICAgaWYoIWV2ZW50LnRhcmdldC50YWdOYW1lLm1hdGNoKCdURVhUQVJFQXxJTlBVVHxTRUxFQ1QnKSkge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzcgJiYgXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgIF8uY2hhbmdlU2xpZGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSA/ICduZXh0JyA6ICAncHJldmlvdXMnXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzkgJiYgXy5vcHRpb25zLmFjY2Vzc2liaWxpdHkgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgIF8uY2hhbmdlU2xpZGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSA/ICdwcmV2aW91cycgOiAnbmV4dCdcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5sYXp5TG9hZCA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIGxvYWRSYW5nZSwgY2xvbmVSYW5nZSwgcmFuZ2VTdGFydCwgcmFuZ2VFbmQ7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGxvYWRJbWFnZXMoaW1hZ2VzU2NvcGUpIHtcclxuXHJcbiAgICAgICAgICAgICQoJ2ltZ1tkYXRhLWxhenldJywgaW1hZ2VzU2NvcGUpLmVhY2goZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIGltYWdlID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgICAgICAgICBpbWFnZVNvdXJjZSA9ICQodGhpcykuYXR0cignZGF0YS1sYXp5JyksXHJcbiAgICAgICAgICAgICAgICAgICAgaW1hZ2VTcmNTZXQgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtc3Jjc2V0JyksXHJcbiAgICAgICAgICAgICAgICAgICAgaW1hZ2VTaXplcyAgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtc2l6ZXMnKSB8fCBfLiRzbGlkZXIuYXR0cignZGF0YS1zaXplcycpLFxyXG4gICAgICAgICAgICAgICAgICAgIGltYWdlVG9Mb2FkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW1nJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaW1hZ2VUb0xvYWQub25sb2FkID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGltYWdlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hbmltYXRlKHsgb3BhY2l0eTogMCB9LCAxMDAsIGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpbWFnZVNyY1NldCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGltYWdlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdzcmNzZXQnLCBpbWFnZVNyY1NldCApO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaW1hZ2VTaXplcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbWFnZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ3NpemVzJywgaW1hZ2VTaXplcyApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbWFnZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdzcmMnLCBpbWFnZVNvdXJjZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYW5pbWF0ZSh7IG9wYWNpdHk6IDEgfSwgMjAwLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdkYXRhLWxhenkgZGF0YS1zcmNzZXQgZGF0YS1zaXplcycpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3NsaWNrLWxvYWRpbmcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdsYXp5TG9hZGVkJywgW18sIGltYWdlLCBpbWFnZVNvdXJjZV0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIGltYWdlVG9Mb2FkLm9uZXJyb3IgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaW1hZ2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUF0dHIoICdkYXRhLWxhenknIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCAnc2xpY2stbG9hZGluZycgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoICdzbGljay1sYXp5bG9hZC1lcnJvcicgKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2xhenlMb2FkRXJyb3InLCBbIF8sIGltYWdlLCBpbWFnZVNvdXJjZSBdKTtcclxuXHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIGltYWdlVG9Mb2FkLnNyYyA9IGltYWdlU291cmNlO1xyXG5cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgIHJhbmdlU3RhcnQgPSBfLmN1cnJlbnRTbGlkZSArIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC8gMiArIDEpO1xyXG4gICAgICAgICAgICAgICAgcmFuZ2VFbmQgPSByYW5nZVN0YXJ0ICsgXy5vcHRpb25zLnNsaWRlc1RvU2hvdyArIDI7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByYW5nZVN0YXJ0ID0gTWF0aC5tYXgoMCwgXy5jdXJyZW50U2xpZGUgLSAoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIgKyAxKSk7XHJcbiAgICAgICAgICAgICAgICByYW5nZUVuZCA9IDIgKyAoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIgKyAxKSArIF8uY3VycmVudFNsaWRlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmFuZ2VTdGFydCA9IF8ub3B0aW9ucy5pbmZpbml0ZSA/IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKyBfLmN1cnJlbnRTbGlkZSA6IF8uY3VycmVudFNsaWRlO1xyXG4gICAgICAgICAgICByYW5nZUVuZCA9IE1hdGguY2VpbChyYW5nZVN0YXJ0ICsgXy5vcHRpb25zLnNsaWRlc1RvU2hvdyk7XHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJhbmdlU3RhcnQgPiAwKSByYW5nZVN0YXJ0LS07XHJcbiAgICAgICAgICAgICAgICBpZiAocmFuZ2VFbmQgPD0gXy5zbGlkZUNvdW50KSByYW5nZUVuZCsrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsb2FkUmFuZ2UgPSBfLiRzbGlkZXIuZmluZCgnLnNsaWNrLXNsaWRlJykuc2xpY2UocmFuZ2VTdGFydCwgcmFuZ2VFbmQpO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmxhenlMb2FkID09PSAnYW50aWNpcGF0ZWQnKSB7XHJcbiAgICAgICAgICAgIHZhciBwcmV2U2xpZGUgPSByYW5nZVN0YXJ0IC0gMSxcclxuICAgICAgICAgICAgICAgIG5leHRTbGlkZSA9IHJhbmdlRW5kLFxyXG4gICAgICAgICAgICAgICAgJHNsaWRlcyA9IF8uJHNsaWRlci5maW5kKCcuc2xpY2stc2xpZGUnKTtcclxuXHJcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmIChwcmV2U2xpZGUgPCAwKSBwcmV2U2xpZGUgPSBfLnNsaWRlQ291bnQgLSAxO1xyXG4gICAgICAgICAgICAgICAgbG9hZFJhbmdlID0gbG9hZFJhbmdlLmFkZCgkc2xpZGVzLmVxKHByZXZTbGlkZSkpO1xyXG4gICAgICAgICAgICAgICAgbG9hZFJhbmdlID0gbG9hZFJhbmdlLmFkZCgkc2xpZGVzLmVxKG5leHRTbGlkZSkpO1xyXG4gICAgICAgICAgICAgICAgcHJldlNsaWRlLS07XHJcbiAgICAgICAgICAgICAgICBuZXh0U2xpZGUrKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbG9hZEltYWdlcyhsb2FkUmFuZ2UpO1xyXG5cclxuICAgICAgICBpZiAoXy5zbGlkZUNvdW50IDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgY2xvbmVSYW5nZSA9IF8uJHNsaWRlci5maW5kKCcuc2xpY2stc2xpZGUnKTtcclxuICAgICAgICAgICAgbG9hZEltYWdlcyhjbG9uZVJhbmdlKTtcclxuICAgICAgICB9IGVsc2VcclxuICAgICAgICBpZiAoXy5jdXJyZW50U2xpZGUgPj0gXy5zbGlkZUNvdW50IC0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xyXG4gICAgICAgICAgICBjbG9uZVJhbmdlID0gXy4kc2xpZGVyLmZpbmQoJy5zbGljay1jbG9uZWQnKS5zbGljZSgwLCBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcclxuICAgICAgICAgICAgbG9hZEltYWdlcyhjbG9uZVJhbmdlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKF8uY3VycmVudFNsaWRlID09PSAwKSB7XHJcbiAgICAgICAgICAgIGNsb25lUmFuZ2UgPSBfLiRzbGlkZXIuZmluZCgnLnNsaWNrLWNsb25lZCcpLnNsaWNlKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgKiAtMSk7XHJcbiAgICAgICAgICAgIGxvYWRJbWFnZXMoY2xvbmVSYW5nZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLmxvYWRTbGlkZXIgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLnNldFBvc2l0aW9uKCk7XHJcblxyXG4gICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKHtcclxuICAgICAgICAgICAgb3BhY2l0eTogMVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBfLiRzbGlkZXIucmVtb3ZlQ2xhc3MoJ3NsaWNrLWxvYWRpbmcnKTtcclxuXHJcbiAgICAgICAgXy5pbml0VUkoKTtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5sYXp5TG9hZCA9PT0gJ3Byb2dyZXNzaXZlJykge1xyXG4gICAgICAgICAgICBfLnByb2dyZXNzaXZlTGF6eUxvYWQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUubmV4dCA9IFNsaWNrLnByb3RvdHlwZS5zbGlja05leHQgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLmNoYW5nZVNsaWRlKHtcclxuICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgbWVzc2FnZTogJ25leHQnXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5vcmllbnRhdGlvbkNoYW5nZSA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIF8uY2hlY2tSZXNwb25zaXZlKCk7XHJcbiAgICAgICAgXy5zZXRQb3NpdGlvbigpO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnBhdXNlID0gU2xpY2sucHJvdG90eXBlLnNsaWNrUGF1c2UgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLmF1dG9QbGF5Q2xlYXIoKTtcclxuICAgICAgICBfLnBhdXNlZCA9IHRydWU7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUucGxheSA9IFNsaWNrLnByb3RvdHlwZS5zbGlja1BsYXkgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLmF1dG9QbGF5KCk7XHJcbiAgICAgICAgXy5vcHRpb25zLmF1dG9wbGF5ID0gdHJ1ZTtcclxuICAgICAgICBfLnBhdXNlZCA9IGZhbHNlO1xyXG4gICAgICAgIF8uZm9jdXNzZWQgPSBmYWxzZTtcclxuICAgICAgICBfLmludGVycnVwdGVkID0gZmFsc2U7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUucG9zdFNsaWRlID0gZnVuY3Rpb24oaW5kZXgpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBpZiggIV8udW5zbGlja2VkICkge1xyXG5cclxuICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2FmdGVyQ2hhbmdlJywgW18sIGluZGV4XSk7XHJcblxyXG4gICAgICAgICAgICBfLmFuaW1hdGluZyA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgICAgIF8uc2V0UG9zaXRpb24oKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgXy5zd2lwZUxlZnQgPSBudWxsO1xyXG5cclxuICAgICAgICAgICAgaWYgKCBfLm9wdGlvbnMuYXV0b3BsYXkgKSB7XHJcbiAgICAgICAgICAgICAgICBfLmF1dG9QbGF5KCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuYWNjZXNzaWJpbGl0eSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgXy5pbml0QURBKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5mb2N1c09uQ2hhbmdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyICRjdXJyZW50U2xpZGUgPSAkKF8uJHNsaWRlcy5nZXQoXy5jdXJyZW50U2xpZGUpKTtcclxuICAgICAgICAgICAgICAgICAgICAkY3VycmVudFNsaWRlLmF0dHIoJ3RhYmluZGV4JywgMCkuZm9jdXMoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUucHJldiA9IFNsaWNrLnByb3RvdHlwZS5zbGlja1ByZXYgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBfLmNoYW5nZVNsaWRlKHtcclxuICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgbWVzc2FnZTogJ3ByZXZpb3VzJ1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUucHJldmVudERlZmF1bHQgPSBmdW5jdGlvbihldmVudCkge1xyXG5cclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnByb2dyZXNzaXZlTGF6eUxvYWQgPSBmdW5jdGlvbiggdHJ5Q291bnQgKSB7XHJcblxyXG4gICAgICAgIHRyeUNvdW50ID0gdHJ5Q291bnQgfHwgMTtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICAkaW1nc1RvTG9hZCA9ICQoICdpbWdbZGF0YS1sYXp5XScsIF8uJHNsaWRlciApLFxyXG4gICAgICAgICAgICBpbWFnZSxcclxuICAgICAgICAgICAgaW1hZ2VTb3VyY2UsXHJcbiAgICAgICAgICAgIGltYWdlU3JjU2V0LFxyXG4gICAgICAgICAgICBpbWFnZVNpemVzLFxyXG4gICAgICAgICAgICBpbWFnZVRvTG9hZDtcclxuXHJcbiAgICAgICAgaWYgKCAkaW1nc1RvTG9hZC5sZW5ndGggKSB7XHJcblxyXG4gICAgICAgICAgICBpbWFnZSA9ICRpbWdzVG9Mb2FkLmZpcnN0KCk7XHJcbiAgICAgICAgICAgIGltYWdlU291cmNlID0gaW1hZ2UuYXR0cignZGF0YS1sYXp5Jyk7XHJcbiAgICAgICAgICAgIGltYWdlU3JjU2V0ID0gaW1hZ2UuYXR0cignZGF0YS1zcmNzZXQnKTtcclxuICAgICAgICAgICAgaW1hZ2VTaXplcyAgPSBpbWFnZS5hdHRyKCdkYXRhLXNpemVzJykgfHwgXy4kc2xpZGVyLmF0dHIoJ2RhdGEtc2l6ZXMnKTtcclxuICAgICAgICAgICAgaW1hZ2VUb0xvYWQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbWcnKTtcclxuXHJcbiAgICAgICAgICAgIGltYWdlVG9Mb2FkLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChpbWFnZVNyY1NldCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGltYWdlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdzcmNzZXQnLCBpbWFnZVNyY1NldCApO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoaW1hZ2VTaXplcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbWFnZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ3NpemVzJywgaW1hZ2VTaXplcyApO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpbWFnZVxyXG4gICAgICAgICAgICAgICAgICAgIC5hdHRyKCAnc3JjJywgaW1hZ2VTb3VyY2UgKVxyXG4gICAgICAgICAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdkYXRhLWxhenkgZGF0YS1zcmNzZXQgZGF0YS1zaXplcycpXHJcbiAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdzbGljay1sb2FkaW5nJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCBfLm9wdGlvbnMuYWRhcHRpdmVIZWlnaHQgPT09IHRydWUgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgXy5zZXRQb3NpdGlvbigpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdsYXp5TG9hZGVkJywgWyBfLCBpbWFnZSwgaW1hZ2VTb3VyY2UgXSk7XHJcbiAgICAgICAgICAgICAgICBfLnByb2dyZXNzaXZlTGF6eUxvYWQoKTtcclxuXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBpbWFnZVRvTG9hZC5vbmVycm9yID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCB0cnlDb3VudCA8IDMgKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8qKlxyXG4gICAgICAgICAgICAgICAgICAgICAqIHRyeSB0byBsb2FkIHRoZSBpbWFnZSAzIHRpbWVzLFxyXG4gICAgICAgICAgICAgICAgICAgICAqIGxlYXZlIGEgc2xpZ2h0IGRlbGF5IHNvIHdlIGRvbid0IGdldFxyXG4gICAgICAgICAgICAgICAgICAgICAqIHNlcnZlcnMgYmxvY2tpbmcgdGhlIHJlcXVlc3QuXHJcbiAgICAgICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCggZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF8ucHJvZ3Jlc3NpdmVMYXp5TG9hZCggdHJ5Q291bnQgKyAxICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgNTAwICk7XHJcblxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaW1hZ2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUF0dHIoICdkYXRhLWxhenknIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCAnc2xpY2stbG9hZGluZycgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoICdzbGljay1sYXp5bG9hZC1lcnJvcicgKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2xhenlMb2FkRXJyb3InLCBbIF8sIGltYWdlLCBpbWFnZVNvdXJjZSBdKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgXy5wcm9ncmVzc2l2ZUxhenlMb2FkKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGltYWdlVG9Mb2FkLnNyYyA9IGltYWdlU291cmNlO1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ2FsbEltYWdlc0xvYWRlZCcsIFsgXyBdKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnJlZnJlc2ggPSBmdW5jdGlvbiggaW5pdGlhbGl6aW5nICkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsIGN1cnJlbnRTbGlkZSwgbGFzdFZpc2libGVJbmRleDtcclxuXHJcbiAgICAgICAgbGFzdFZpc2libGVJbmRleCA9IF8uc2xpZGVDb3VudCAtIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3c7XHJcblxyXG4gICAgICAgIC8vIGluIG5vbi1pbmZpbml0ZSBzbGlkZXJzLCB3ZSBkb24ndCB3YW50IHRvIGdvIHBhc3QgdGhlXHJcbiAgICAgICAgLy8gbGFzdCB2aXNpYmxlIGluZGV4LlxyXG4gICAgICAgIGlmKCAhXy5vcHRpb25zLmluZmluaXRlICYmICggXy5jdXJyZW50U2xpZGUgPiBsYXN0VmlzaWJsZUluZGV4ICkpIHtcclxuICAgICAgICAgICAgXy5jdXJyZW50U2xpZGUgPSBsYXN0VmlzaWJsZUluZGV4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gaWYgbGVzcyBzbGlkZXMgdGhhbiB0byBzaG93LCBnbyB0byBzdGFydC5cclxuICAgICAgICBpZiAoIF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICkge1xyXG4gICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IDA7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY3VycmVudFNsaWRlID0gXy5jdXJyZW50U2xpZGU7XHJcblxyXG4gICAgICAgIF8uZGVzdHJveSh0cnVlKTtcclxuXHJcbiAgICAgICAgJC5leHRlbmQoXywgXy5pbml0aWFscywgeyBjdXJyZW50U2xpZGU6IGN1cnJlbnRTbGlkZSB9KTtcclxuXHJcbiAgICAgICAgXy5pbml0KCk7XHJcblxyXG4gICAgICAgIGlmKCAhaW5pdGlhbGl6aW5nICkge1xyXG5cclxuICAgICAgICAgICAgXy5jaGFuZ2VTbGlkZSh7XHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ2luZGV4JyxcclxuICAgICAgICAgICAgICAgICAgICBpbmRleDogY3VycmVudFNsaWRlXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIGZhbHNlKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnJlZ2lzdGVyQnJlYWtwb2ludHMgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLCBicmVha3BvaW50LCBjdXJyZW50QnJlYWtwb2ludCwgbCxcclxuICAgICAgICAgICAgcmVzcG9uc2l2ZVNldHRpbmdzID0gXy5vcHRpb25zLnJlc3BvbnNpdmUgfHwgbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKCAkLnR5cGUocmVzcG9uc2l2ZVNldHRpbmdzKSA9PT0gJ2FycmF5JyAmJiByZXNwb25zaXZlU2V0dGluZ3MubGVuZ3RoICkge1xyXG5cclxuICAgICAgICAgICAgXy5yZXNwb25kVG8gPSBfLm9wdGlvbnMucmVzcG9uZFRvIHx8ICd3aW5kb3cnO1xyXG5cclxuICAgICAgICAgICAgZm9yICggYnJlYWtwb2ludCBpbiByZXNwb25zaXZlU2V0dGluZ3MgKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgbCA9IF8uYnJlYWtwb2ludHMubGVuZ3RoLTE7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNpdmVTZXR0aW5ncy5oYXNPd25Qcm9wZXJ0eShicmVha3BvaW50KSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRCcmVha3BvaW50ID0gcmVzcG9uc2l2ZVNldHRpbmdzW2JyZWFrcG9pbnRdLmJyZWFrcG9pbnQ7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGxvb3AgdGhyb3VnaCB0aGUgYnJlYWtwb2ludHMgYW5kIGN1dCBvdXQgYW55IGV4aXN0aW5nXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gb25lcyB3aXRoIHRoZSBzYW1lIGJyZWFrcG9pbnQgbnVtYmVyLCB3ZSBkb24ndCB3YW50IGR1cGVzLlxyXG4gICAgICAgICAgICAgICAgICAgIHdoaWxlKCBsID49IDAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCBfLmJyZWFrcG9pbnRzW2xdICYmIF8uYnJlYWtwb2ludHNbbF0gPT09IGN1cnJlbnRCcmVha3BvaW50ICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy5icmVha3BvaW50cy5zcGxpY2UobCwxKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsLS07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBfLmJyZWFrcG9pbnRzLnB1c2goY3VycmVudEJyZWFrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIF8uYnJlYWtwb2ludFNldHRpbmdzW2N1cnJlbnRCcmVha3BvaW50XSA9IHJlc3BvbnNpdmVTZXR0aW5nc1ticmVha3BvaW50XS5zZXR0aW5ncztcclxuXHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBfLmJyZWFrcG9pbnRzLnNvcnQoZnVuY3Rpb24oYSwgYikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICggXy5vcHRpb25zLm1vYmlsZUZpcnN0ICkgPyBhLWIgOiBiLWE7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUucmVpbml0ID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgXy4kc2xpZGVzID1cclxuICAgICAgICAgICAgXy4kc2xpZGVUcmFja1xyXG4gICAgICAgICAgICAgICAgLmNoaWxkcmVuKF8ub3B0aW9ucy5zbGlkZSlcclxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stc2xpZGUnKTtcclxuXHJcbiAgICAgICAgXy5zbGlkZUNvdW50ID0gXy4kc2xpZGVzLmxlbmd0aDtcclxuXHJcbiAgICAgICAgaWYgKF8uY3VycmVudFNsaWRlID49IF8uc2xpZGVDb3VudCAmJiBfLmN1cnJlbnRTbGlkZSAhPT0gMCkge1xyXG4gICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSA9IF8uY3VycmVudFNsaWRlIC0gXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcbiAgICAgICAgICAgIF8uY3VycmVudFNsaWRlID0gMDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF8ucmVnaXN0ZXJCcmVha3BvaW50cygpO1xyXG5cclxuICAgICAgICBfLnNldFByb3BzKCk7XHJcbiAgICAgICAgXy5zZXR1cEluZmluaXRlKCk7XHJcbiAgICAgICAgXy5idWlsZEFycm93cygpO1xyXG4gICAgICAgIF8udXBkYXRlQXJyb3dzKCk7XHJcbiAgICAgICAgXy5pbml0QXJyb3dFdmVudHMoKTtcclxuICAgICAgICBfLmJ1aWxkRG90cygpO1xyXG4gICAgICAgIF8udXBkYXRlRG90cygpO1xyXG4gICAgICAgIF8uaW5pdERvdEV2ZW50cygpO1xyXG4gICAgICAgIF8uY2xlYW5VcFNsaWRlRXZlbnRzKCk7XHJcbiAgICAgICAgXy5pbml0U2xpZGVFdmVudHMoKTtcclxuXHJcbiAgICAgICAgXy5jaGVja1Jlc3BvbnNpdmUoZmFsc2UsIHRydWUpO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmZvY3VzT25TZWxlY3QgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgJChfLiRzbGlkZVRyYWNrKS5jaGlsZHJlbigpLm9uKCdjbGljay5zbGljaycsIF8uc2VsZWN0SGFuZGxlcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfLnNldFNsaWRlQ2xhc3Nlcyh0eXBlb2YgXy5jdXJyZW50U2xpZGUgPT09ICdudW1iZXInID8gXy5jdXJyZW50U2xpZGUgOiAwKTtcclxuXHJcbiAgICAgICAgXy5zZXRQb3NpdGlvbigpO1xyXG4gICAgICAgIF8uZm9jdXNIYW5kbGVyKCk7XHJcblxyXG4gICAgICAgIF8ucGF1c2VkID0gIV8ub3B0aW9ucy5hdXRvcGxheTtcclxuICAgICAgICBfLmF1dG9QbGF5KCk7XHJcblxyXG4gICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdyZUluaXQnLCBbX10pO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnJlc2l6ZSA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmICgkKHdpbmRvdykud2lkdGgoKSAhPT0gXy53aW5kb3dXaWR0aCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQoXy53aW5kb3dEZWxheSk7XHJcbiAgICAgICAgICAgIF8ud2luZG93RGVsYXkgPSB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIF8ud2luZG93V2lkdGggPSAkKHdpbmRvdykud2lkdGgoKTtcclxuICAgICAgICAgICAgICAgIF8uY2hlY2tSZXNwb25zaXZlKCk7XHJcbiAgICAgICAgICAgICAgICBpZiggIV8udW5zbGlja2VkICkgeyBfLnNldFBvc2l0aW9uKCk7IH1cclxuICAgICAgICAgICAgfSwgNTApO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnJlbW92ZVNsaWRlID0gU2xpY2sucHJvdG90eXBlLnNsaWNrUmVtb3ZlID0gZnVuY3Rpb24oaW5kZXgsIHJlbW92ZUJlZm9yZSwgcmVtb3ZlQWxsKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZihpbmRleCkgPT09ICdib29sZWFuJykge1xyXG4gICAgICAgICAgICByZW1vdmVCZWZvcmUgPSBpbmRleDtcclxuICAgICAgICAgICAgaW5kZXggPSByZW1vdmVCZWZvcmUgPT09IHRydWUgPyAwIDogXy5zbGlkZUNvdW50IC0gMTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpbmRleCA9IHJlbW92ZUJlZm9yZSA9PT0gdHJ1ZSA/IC0taW5kZXggOiBpbmRleDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLnNsaWRlQ291bnQgPCAxIHx8IGluZGV4IDwgMCB8fCBpbmRleCA+IF8uc2xpZGVDb3VudCAtIDEpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXy51bmxvYWQoKTtcclxuXHJcbiAgICAgICAgaWYgKHJlbW92ZUFsbCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKCkucmVtb3ZlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmVxKGluZGV4KS5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF8uJHNsaWRlcyA9IF8uJHNsaWRlVHJhY2suY2hpbGRyZW4odGhpcy5vcHRpb25zLnNsaWRlKTtcclxuXHJcbiAgICAgICAgXy4kc2xpZGVUcmFjay5jaGlsZHJlbih0aGlzLm9wdGlvbnMuc2xpZGUpLmRldGFjaCgpO1xyXG5cclxuICAgICAgICBfLiRzbGlkZVRyYWNrLmFwcGVuZChfLiRzbGlkZXMpO1xyXG5cclxuICAgICAgICBfLiRzbGlkZXNDYWNoZSA9IF8uJHNsaWRlcztcclxuXHJcbiAgICAgICAgXy5yZWluaXQoKTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zZXRDU1MgPSBmdW5jdGlvbihwb3NpdGlvbikge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIHBvc2l0aW9uUHJvcHMgPSB7fSxcclxuICAgICAgICAgICAgeCwgeTtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgcG9zaXRpb24gPSAtcG9zaXRpb247XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHggPSBfLnBvc2l0aW9uUHJvcCA9PSAnbGVmdCcgPyBNYXRoLmNlaWwocG9zaXRpb24pICsgJ3B4JyA6ICcwcHgnO1xyXG4gICAgICAgIHkgPSBfLnBvc2l0aW9uUHJvcCA9PSAndG9wJyA/IE1hdGguY2VpbChwb3NpdGlvbikgKyAncHgnIDogJzBweCc7XHJcblxyXG4gICAgICAgIHBvc2l0aW9uUHJvcHNbXy5wb3NpdGlvblByb3BdID0gcG9zaXRpb247XHJcblxyXG4gICAgICAgIGlmIChfLnRyYW5zZm9ybXNFbmFibGVkID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyhwb3NpdGlvblByb3BzKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBwb3NpdGlvblByb3BzID0ge307XHJcbiAgICAgICAgICAgIGlmIChfLmNzc1RyYW5zaXRpb25zID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb25Qcm9wc1tfLmFuaW1UeXBlXSA9ICd0cmFuc2xhdGUoJyArIHggKyAnLCAnICsgeSArICcpJztcclxuICAgICAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suY3NzKHBvc2l0aW9uUHJvcHMpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb25Qcm9wc1tfLmFuaW1UeXBlXSA9ICd0cmFuc2xhdGUzZCgnICsgeCArICcsICcgKyB5ICsgJywgMHB4KSc7XHJcbiAgICAgICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNzcyhwb3NpdGlvblByb3BzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zZXREaW1lbnNpb25zID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbCA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICBfLiRsaXN0LmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogKCcwcHggJyArIF8ub3B0aW9ucy5jZW50ZXJQYWRkaW5nKVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBfLiRsaXN0LmhlaWdodChfLiRzbGlkZXMuZmlyc3QoKS5vdXRlckhlaWdodCh0cnVlKSAqIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpO1xyXG4gICAgICAgICAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgIF8uJGxpc3QuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAoXy5vcHRpb25zLmNlbnRlclBhZGRpbmcgKyAnIDBweCcpXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXy5saXN0V2lkdGggPSBfLiRsaXN0LndpZHRoKCk7XHJcbiAgICAgICAgXy5saXN0SGVpZ2h0ID0gXy4kbGlzdC5oZWlnaHQoKTtcclxuXHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlICYmIF8ub3B0aW9ucy52YXJpYWJsZVdpZHRoID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICBfLnNsaWRlV2lkdGggPSBNYXRoLmNlaWwoXy5saXN0V2lkdGggLyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KTtcclxuICAgICAgICAgICAgXy4kc2xpZGVUcmFjay53aWR0aChNYXRoLmNlaWwoKF8uc2xpZGVXaWR0aCAqIF8uJHNsaWRlVHJhY2suY2hpbGRyZW4oJy5zbGljay1zbGlkZScpLmxlbmd0aCkpKTtcclxuXHJcbiAgICAgICAgfSBlbHNlIGlmIChfLm9wdGlvbnMudmFyaWFibGVXaWR0aCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLndpZHRoKDUwMDAgKiBfLnNsaWRlQ291bnQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIF8uc2xpZGVXaWR0aCA9IE1hdGguY2VpbChfLmxpc3RXaWR0aCk7XHJcbiAgICAgICAgICAgIF8uJHNsaWRlVHJhY2suaGVpZ2h0KE1hdGguY2VpbCgoXy4kc2xpZGVzLmZpcnN0KCkub3V0ZXJIZWlnaHQodHJ1ZSkgKiBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKCcuc2xpY2stc2xpZGUnKS5sZW5ndGgpKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgb2Zmc2V0ID0gXy4kc2xpZGVzLmZpcnN0KCkub3V0ZXJXaWR0aCh0cnVlKSAtIF8uJHNsaWRlcy5maXJzdCgpLndpZHRoKCk7XHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy52YXJpYWJsZVdpZHRoID09PSBmYWxzZSkgXy4kc2xpZGVUcmFjay5jaGlsZHJlbignLnNsaWNrLXNsaWRlJykud2lkdGgoXy5zbGlkZVdpZHRoIC0gb2Zmc2V0KTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zZXRGYWRlID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcyxcclxuICAgICAgICAgICAgdGFyZ2V0TGVmdDtcclxuXHJcbiAgICAgICAgXy4kc2xpZGVzLmVhY2goZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgdGFyZ2V0TGVmdCA9IChfLnNsaWRlV2lkdGggKiBpbmRleCkgKiAtMTtcclxuICAgICAgICAgICAgaWYgKF8ub3B0aW9ucy5ydGwgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICQoZWxlbWVudCkuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcclxuICAgICAgICAgICAgICAgICAgICByaWdodDogdGFyZ2V0TGVmdCxcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4IC0gMixcclxuICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICQoZWxlbWVudCkuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiB0YXJnZXRMZWZ0LFxyXG4gICAgICAgICAgICAgICAgICAgIHRvcDogMCxcclxuICAgICAgICAgICAgICAgICAgICB6SW5kZXg6IF8ub3B0aW9ucy56SW5kZXggLSAyLFxyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDBcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIF8uJHNsaWRlcy5lcShfLmN1cnJlbnRTbGlkZSkuY3NzKHtcclxuICAgICAgICAgICAgekluZGV4OiBfLm9wdGlvbnMuekluZGV4IC0gMSxcclxuICAgICAgICAgICAgb3BhY2l0eTogMVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnNldEhlaWdodCA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93ID09PSAxICYmIF8ub3B0aW9ucy5hZGFwdGl2ZUhlaWdodCA9PT0gdHJ1ZSAmJiBfLm9wdGlvbnMudmVydGljYWwgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIHZhciB0YXJnZXRIZWlnaHQgPSBfLiRzbGlkZXMuZXEoXy5jdXJyZW50U2xpZGUpLm91dGVySGVpZ2h0KHRydWUpO1xyXG4gICAgICAgICAgICBfLiRsaXN0LmNzcygnaGVpZ2h0JywgdGFyZ2V0SGVpZ2h0KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuc2V0T3B0aW9uID1cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zbGlja1NldE9wdGlvbiA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBhY2NlcHRzIGFyZ3VtZW50cyBpbiBmb3JtYXQgb2Y6XHJcbiAgICAgICAgICpcclxuICAgICAgICAgKiAgLSBmb3IgY2hhbmdpbmcgYSBzaW5nbGUgb3B0aW9uJ3MgdmFsdWU6XHJcbiAgICAgICAgICogICAgIC5zbGljayhcInNldE9wdGlvblwiLCBvcHRpb24sIHZhbHVlLCByZWZyZXNoIClcclxuICAgICAgICAgKlxyXG4gICAgICAgICAqICAtIGZvciBjaGFuZ2luZyBhIHNldCBvZiByZXNwb25zaXZlIG9wdGlvbnM6XHJcbiAgICAgICAgICogICAgIC5zbGljayhcInNldE9wdGlvblwiLCAncmVzcG9uc2l2ZScsIFt7fSwgLi4uXSwgcmVmcmVzaCApXHJcbiAgICAgICAgICpcclxuICAgICAgICAgKiAgLSBmb3IgdXBkYXRpbmcgbXVsdGlwbGUgdmFsdWVzIGF0IG9uY2UgKG5vdCByZXNwb25zaXZlKVxyXG4gICAgICAgICAqICAgICAuc2xpY2soXCJzZXRPcHRpb25cIiwgeyAnb3B0aW9uJzogdmFsdWUsIC4uLiB9LCByZWZyZXNoIClcclxuICAgICAgICAgKi9cclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLCBsLCBpdGVtLCBvcHRpb24sIHZhbHVlLCByZWZyZXNoID0gZmFsc2UsIHR5cGU7XHJcblxyXG4gICAgICAgIGlmKCAkLnR5cGUoIGFyZ3VtZW50c1swXSApID09PSAnb2JqZWN0JyApIHtcclxuXHJcbiAgICAgICAgICAgIG9wdGlvbiA9ICBhcmd1bWVudHNbMF07XHJcbiAgICAgICAgICAgIHJlZnJlc2ggPSBhcmd1bWVudHNbMV07XHJcbiAgICAgICAgICAgIHR5cGUgPSAnbXVsdGlwbGUnO1xyXG5cclxuICAgICAgICB9IGVsc2UgaWYgKCAkLnR5cGUoIGFyZ3VtZW50c1swXSApID09PSAnc3RyaW5nJyApIHtcclxuXHJcbiAgICAgICAgICAgIG9wdGlvbiA9ICBhcmd1bWVudHNbMF07XHJcbiAgICAgICAgICAgIHZhbHVlID0gYXJndW1lbnRzWzFdO1xyXG4gICAgICAgICAgICByZWZyZXNoID0gYXJndW1lbnRzWzJdO1xyXG5cclxuICAgICAgICAgICAgaWYgKCBhcmd1bWVudHNbMF0gPT09ICdyZXNwb25zaXZlJyAmJiAkLnR5cGUoIGFyZ3VtZW50c1sxXSApID09PSAnYXJyYXknICkge1xyXG5cclxuICAgICAgICAgICAgICAgIHR5cGUgPSAncmVzcG9uc2l2ZSc7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCB0eXBlb2YgYXJndW1lbnRzWzFdICE9PSAndW5kZWZpbmVkJyApIHtcclxuXHJcbiAgICAgICAgICAgICAgICB0eXBlID0gJ3NpbmdsZSc7XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCB0eXBlID09PSAnc2luZ2xlJyApIHtcclxuXHJcbiAgICAgICAgICAgIF8ub3B0aW9uc1tvcHRpb25dID0gdmFsdWU7XHJcblxyXG5cclxuICAgICAgICB9IGVsc2UgaWYgKCB0eXBlID09PSAnbXVsdGlwbGUnICkge1xyXG5cclxuICAgICAgICAgICAgJC5lYWNoKCBvcHRpb24gLCBmdW5jdGlvbiggb3B0LCB2YWwgKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgXy5vcHRpb25zW29wdF0gPSB2YWw7XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgIH0gZWxzZSBpZiAoIHR5cGUgPT09ICdyZXNwb25zaXZlJyApIHtcclxuXHJcbiAgICAgICAgICAgIGZvciAoIGl0ZW0gaW4gdmFsdWUgKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYoICQudHlwZSggXy5vcHRpb25zLnJlc3BvbnNpdmUgKSAhPT0gJ2FycmF5JyApIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zLnJlc3BvbnNpdmUgPSBbIHZhbHVlW2l0ZW1dIF07XHJcblxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgbCA9IF8ub3B0aW9ucy5yZXNwb25zaXZlLmxlbmd0aC0xO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBsb29wIHRocm91Z2ggdGhlIHJlc3BvbnNpdmUgb2JqZWN0IGFuZCBzcGxpY2Ugb3V0IGR1cGxpY2F0ZXMuXHJcbiAgICAgICAgICAgICAgICAgICAgd2hpbGUoIGwgPj0gMCApIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCBfLm9wdGlvbnMucmVzcG9uc2l2ZVtsXS5icmVha3BvaW50ID09PSB2YWx1ZVtpdGVtXS5icmVha3BvaW50ICkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8ub3B0aW9ucy5yZXNwb25zaXZlLnNwbGljZShsLDEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbC0tO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIF8ub3B0aW9ucy5yZXNwb25zaXZlLnB1c2goIHZhbHVlW2l0ZW1dICk7XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggcmVmcmVzaCApIHtcclxuXHJcbiAgICAgICAgICAgIF8udW5sb2FkKCk7XHJcbiAgICAgICAgICAgIF8ucmVpbml0KCk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zZXRQb3NpdGlvbiA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIF8uc2V0RGltZW5zaW9ucygpO1xyXG5cclxuICAgICAgICBfLnNldEhlaWdodCgpO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIF8uc2V0Q1NTKF8uZ2V0TGVmdChfLmN1cnJlbnRTbGlkZSkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIF8uc2V0RmFkZSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ3NldFBvc2l0aW9uJywgW19dKTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zZXRQcm9wcyA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIGJvZHlTdHlsZSA9IGRvY3VtZW50LmJvZHkuc3R5bGU7XHJcblxyXG4gICAgICAgIF8ucG9zaXRpb25Qcm9wID0gXy5vcHRpb25zLnZlcnRpY2FsID09PSB0cnVlID8gJ3RvcCcgOiAnbGVmdCc7XHJcblxyXG4gICAgICAgIGlmIChfLnBvc2l0aW9uUHJvcCA9PT0gJ3RvcCcpIHtcclxuICAgICAgICAgICAgXy4kc2xpZGVyLmFkZENsYXNzKCdzbGljay12ZXJ0aWNhbCcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIF8uJHNsaWRlci5yZW1vdmVDbGFzcygnc2xpY2stdmVydGljYWwnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChib2R5U3R5bGUuV2Via2l0VHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkIHx8XHJcbiAgICAgICAgICAgIGJvZHlTdHlsZS5Nb3pUcmFuc2l0aW9uICE9PSB1bmRlZmluZWQgfHxcclxuICAgICAgICAgICAgYm9keVN0eWxlLm1zVHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMudXNlQ1NTID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICBfLmNzc1RyYW5zaXRpb25zID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCBfLm9wdGlvbnMuZmFkZSApIHtcclxuICAgICAgICAgICAgaWYgKCB0eXBlb2YgXy5vcHRpb25zLnpJbmRleCA9PT0gJ251bWJlcicgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiggXy5vcHRpb25zLnpJbmRleCA8IDMgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgXy5vcHRpb25zLnpJbmRleCA9IDM7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBfLm9wdGlvbnMuekluZGV4ID0gXy5kZWZhdWx0cy56SW5kZXg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChib2R5U3R5bGUuT1RyYW5zZm9ybSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIF8uYW5pbVR5cGUgPSAnT1RyYW5zZm9ybSc7XHJcbiAgICAgICAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctby10cmFuc2Zvcm0nO1xyXG4gICAgICAgICAgICBfLnRyYW5zaXRpb25UeXBlID0gJ09UcmFuc2l0aW9uJztcclxuICAgICAgICAgICAgaWYgKGJvZHlTdHlsZS5wZXJzcGVjdGl2ZVByb3BlcnR5ID09PSB1bmRlZmluZWQgJiYgYm9keVN0eWxlLndlYmtpdFBlcnNwZWN0aXZlID09PSB1bmRlZmluZWQpIF8uYW5pbVR5cGUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGJvZHlTdHlsZS5Nb3pUcmFuc2Zvcm0gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICBfLmFuaW1UeXBlID0gJ01velRyYW5zZm9ybSc7XHJcbiAgICAgICAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctbW96LXRyYW5zZm9ybSc7XHJcbiAgICAgICAgICAgIF8udHJhbnNpdGlvblR5cGUgPSAnTW96VHJhbnNpdGlvbic7XHJcbiAgICAgICAgICAgIGlmIChib2R5U3R5bGUucGVyc3BlY3RpdmVQcm9wZXJ0eSA9PT0gdW5kZWZpbmVkICYmIGJvZHlTdHlsZS5Nb3pQZXJzcGVjdGl2ZSA9PT0gdW5kZWZpbmVkKSBfLmFuaW1UeXBlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChib2R5U3R5bGUud2Via2l0VHJhbnNmb3JtICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgXy5hbmltVHlwZSA9ICd3ZWJraXRUcmFuc2Zvcm0nO1xyXG4gICAgICAgICAgICBfLnRyYW5zZm9ybVR5cGUgPSAnLXdlYmtpdC10cmFuc2Zvcm0nO1xyXG4gICAgICAgICAgICBfLnRyYW5zaXRpb25UeXBlID0gJ3dlYmtpdFRyYW5zaXRpb24nO1xyXG4gICAgICAgICAgICBpZiAoYm9keVN0eWxlLnBlcnNwZWN0aXZlUHJvcGVydHkgPT09IHVuZGVmaW5lZCAmJiBib2R5U3R5bGUud2Via2l0UGVyc3BlY3RpdmUgPT09IHVuZGVmaW5lZCkgXy5hbmltVHlwZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoYm9keVN0eWxlLm1zVHJhbnNmb3JtICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgXy5hbmltVHlwZSA9ICdtc1RyYW5zZm9ybSc7XHJcbiAgICAgICAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICctbXMtdHJhbnNmb3JtJztcclxuICAgICAgICAgICAgXy50cmFuc2l0aW9uVHlwZSA9ICdtc1RyYW5zaXRpb24nO1xyXG4gICAgICAgICAgICBpZiAoYm9keVN0eWxlLm1zVHJhbnNmb3JtID09PSB1bmRlZmluZWQpIF8uYW5pbVR5cGUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGJvZHlTdHlsZS50cmFuc2Zvcm0gIT09IHVuZGVmaW5lZCAmJiBfLmFuaW1UeXBlICE9PSBmYWxzZSkge1xyXG4gICAgICAgICAgICBfLmFuaW1UeXBlID0gJ3RyYW5zZm9ybSc7XHJcbiAgICAgICAgICAgIF8udHJhbnNmb3JtVHlwZSA9ICd0cmFuc2Zvcm0nO1xyXG4gICAgICAgICAgICBfLnRyYW5zaXRpb25UeXBlID0gJ3RyYW5zaXRpb24nO1xyXG4gICAgICAgIH1cclxuICAgICAgICBfLnRyYW5zZm9ybXNFbmFibGVkID0gXy5vcHRpb25zLnVzZVRyYW5zZm9ybSAmJiAoXy5hbmltVHlwZSAhPT0gbnVsbCAmJiBfLmFuaW1UeXBlICE9PSBmYWxzZSk7XHJcbiAgICB9O1xyXG5cclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuc2V0U2xpZGVDbGFzc2VzID0gZnVuY3Rpb24oaW5kZXgpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICBjZW50ZXJPZmZzZXQsIGFsbFNsaWRlcywgaW5kZXhPZmZzZXQsIHJlbWFpbmRlcjtcclxuXHJcbiAgICAgICAgYWxsU2xpZGVzID0gXy4kc2xpZGVyXHJcbiAgICAgICAgICAgIC5maW5kKCcuc2xpY2stc2xpZGUnKVxyXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3NsaWNrLWFjdGl2ZSBzbGljay1jZW50ZXIgc2xpY2stY3VycmVudCcpXHJcbiAgICAgICAgICAgIC5hdHRyKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XHJcblxyXG4gICAgICAgIF8uJHNsaWRlc1xyXG4gICAgICAgICAgICAuZXEoaW5kZXgpXHJcbiAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stY3VycmVudCcpO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcclxuXHJcbiAgICAgICAgICAgIHZhciBldmVuQ29lZiA9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgJSAyID09PSAwID8gMSA6IDA7XHJcblxyXG4gICAgICAgICAgICBjZW50ZXJPZmZzZXQgPSBNYXRoLmZsb29yKF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgLyAyKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUpIHtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoaW5kZXggPj0gY2VudGVyT2Zmc2V0ICYmIGluZGV4IDw9IChfLnNsaWRlQ291bnQgLSAxKSAtIGNlbnRlck9mZnNldCkge1xyXG4gICAgICAgICAgICAgICAgICAgIF8uJHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2xpY2UoaW5kZXggLSBjZW50ZXJPZmZzZXQgKyBldmVuQ29lZiwgaW5kZXggKyBjZW50ZXJPZmZzZXQgKyAxKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdhcmlhLWhpZGRlbicsICdmYWxzZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGluZGV4T2Zmc2V0ID0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyArIGluZGV4O1xyXG4gICAgICAgICAgICAgICAgICAgIGFsbFNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2xpY2UoaW5kZXhPZmZzZXQgLSBjZW50ZXJPZmZzZXQgKyAxICsgZXZlbkNvZWYsIGluZGV4T2Zmc2V0ICsgY2VudGVyT2Zmc2V0ICsgMilcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1hY3RpdmUnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignYXJpYS1oaWRkZW4nLCAnZmFsc2UnKTtcclxuXHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGluZGV4ID09PSAwKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGFsbFNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuZXEoYWxsU2xpZGVzLmxlbmd0aCAtIDEgLSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3NsaWNrLWNlbnRlcicpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoaW5kZXggPT09IF8uc2xpZGVDb3VudCAtIDEpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgYWxsU2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5lcShfLm9wdGlvbnMuc2xpZGVzVG9TaG93KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3NsaWNrLWNlbnRlcicpO1xyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIF8uJHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgLmVxKGluZGV4KVxyXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1jZW50ZXInKTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgIGlmIChpbmRleCA+PSAwICYmIGluZGV4IDw9IChfLnNsaWRlQ291bnQgLSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSkge1xyXG5cclxuICAgICAgICAgICAgICAgIF8uJHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgIC5zbGljZShpbmRleCwgaW5kZXggKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KVxyXG4gICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stYWN0aXZlJylcclxuICAgICAgICAgICAgICAgICAgICAuYXR0cignYXJpYS1oaWRkZW4nLCAnZmFsc2UnKTtcclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYWxsU2xpZGVzLmxlbmd0aCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcblxyXG4gICAgICAgICAgICAgICAgYWxsU2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdzbGljay1hY3RpdmUnKVxyXG4gICAgICAgICAgICAgICAgICAgIC5hdHRyKCdhcmlhLWhpZGRlbicsICdmYWxzZScpO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICByZW1haW5kZXIgPSBfLnNsaWRlQ291bnQgJSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93O1xyXG4gICAgICAgICAgICAgICAgaW5kZXhPZmZzZXQgPSBfLm9wdGlvbnMuaW5maW5pdGUgPT09IHRydWUgPyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93ICsgaW5kZXggOiBpbmRleDtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyA9PSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwgJiYgKF8uc2xpZGVDb3VudCAtIGluZGV4KSA8IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgYWxsU2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zbGljZShpbmRleE9mZnNldCAtIChfLm9wdGlvbnMuc2xpZGVzVG9TaG93IC0gcmVtYWluZGVyKSwgaW5kZXhPZmZzZXQgKyByZW1haW5kZXIpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnc2xpY2stYWN0aXZlJylcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgYWxsU2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zbGljZShpbmRleE9mZnNldCwgaW5kZXhPZmZzZXQgKyBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdhcmlhLWhpZGRlbicsICdmYWxzZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmxhenlMb2FkID09PSAnb25kZW1hbmQnIHx8IF8ub3B0aW9ucy5sYXp5TG9hZCA9PT0gJ2FudGljaXBhdGVkJykge1xyXG4gICAgICAgICAgICBfLmxhenlMb2FkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuc2V0dXBJbmZpbml0ZSA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXMsXHJcbiAgICAgICAgICAgIGksIHNsaWRlSW5kZXgsIGluZmluaXRlQ291bnQ7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBfLm9wdGlvbnMuY2VudGVyTW9kZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gdHJ1ZSAmJiBfLm9wdGlvbnMuZmFkZSA9PT0gZmFsc2UpIHtcclxuXHJcbiAgICAgICAgICAgIHNsaWRlSW5kZXggPSBudWxsO1xyXG5cclxuICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZUNvdW50ID0gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyArIDE7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGluZmluaXRlQ291bnQgPSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGZvciAoaSA9IF8uc2xpZGVDb3VudDsgaSA+IChfLnNsaWRlQ291bnQgLVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZUNvdW50KTsgaSAtPSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVJbmRleCA9IGkgLSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICQoXy4kc2xpZGVzW3NsaWRlSW5kZXhdKS5jbG9uZSh0cnVlKS5hdHRyKCdpZCcsICcnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignZGF0YS1zbGljay1pbmRleCcsIHNsaWRlSW5kZXggLSBfLnNsaWRlQ291bnQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5wcmVwZW5kVG8oXy4kc2xpZGVUcmFjaykuYWRkQ2xhc3MoJ3NsaWNrLWNsb25lZCcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGluZmluaXRlQ291bnQgICsgXy5zbGlkZUNvdW50OyBpICs9IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZUluZGV4ID0gaTtcclxuICAgICAgICAgICAgICAgICAgICAkKF8uJHNsaWRlc1tzbGlkZUluZGV4XSkuY2xvbmUodHJ1ZSkuYXR0cignaWQnLCAnJylcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2RhdGEtc2xpY2staW5kZXgnLCBzbGlkZUluZGV4ICsgXy5zbGlkZUNvdW50KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYXBwZW5kVG8oXy4kc2xpZGVUcmFjaykuYWRkQ2xhc3MoJ3NsaWNrLWNsb25lZCcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXy4kc2xpZGVUcmFjay5maW5kKCcuc2xpY2stY2xvbmVkJykuZmluZCgnW2lkXScpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5hdHRyKCdpZCcsICcnKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuaW50ZXJydXB0ID0gZnVuY3Rpb24oIHRvZ2dsZSApIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBpZiggIXRvZ2dsZSApIHtcclxuICAgICAgICAgICAgXy5hdXRvUGxheSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBfLmludGVycnVwdGVkID0gdG9nZ2xlO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnNlbGVjdEhhbmRsZXIgPSBmdW5jdGlvbihldmVudCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIHZhciB0YXJnZXRFbGVtZW50ID1cclxuICAgICAgICAgICAgJChldmVudC50YXJnZXQpLmlzKCcuc2xpY2stc2xpZGUnKSA/XHJcbiAgICAgICAgICAgICAgICAkKGV2ZW50LnRhcmdldCkgOlxyXG4gICAgICAgICAgICAgICAgJChldmVudC50YXJnZXQpLnBhcmVudHMoJy5zbGljay1zbGlkZScpO1xyXG5cclxuICAgICAgICB2YXIgaW5kZXggPSBwYXJzZUludCh0YXJnZXRFbGVtZW50LmF0dHIoJ2RhdGEtc2xpY2staW5kZXgnKSk7XHJcblxyXG4gICAgICAgIGlmICghaW5kZXgpIGluZGV4ID0gMDtcclxuXHJcbiAgICAgICAgaWYgKF8uc2xpZGVDb3VudCA8PSBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcblxyXG4gICAgICAgICAgICBfLnNsaWRlSGFuZGxlcihpbmRleCwgZmFsc2UsIHRydWUpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXy5zbGlkZUhhbmRsZXIoaW5kZXgpO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnNsaWRlSGFuZGxlciA9IGZ1bmN0aW9uKGluZGV4LCBzeW5jLCBkb250QW5pbWF0ZSkge1xyXG5cclxuICAgICAgICB2YXIgdGFyZ2V0U2xpZGUsIGFuaW1TbGlkZSwgb2xkU2xpZGUsIHNsaWRlTGVmdCwgdGFyZ2V0TGVmdCA9IG51bGwsXHJcbiAgICAgICAgICAgIF8gPSB0aGlzLCBuYXZUYXJnZXQ7XHJcblxyXG4gICAgICAgIHN5bmMgPSBzeW5jIHx8IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAoXy5hbmltYXRpbmcgPT09IHRydWUgJiYgXy5vcHRpb25zLndhaXRGb3JBbmltYXRlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSAmJiBfLmN1cnJlbnRTbGlkZSA9PT0gaW5kZXgpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHN5bmMgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIF8uYXNOYXZGb3IoaW5kZXgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGFyZ2V0U2xpZGUgPSBpbmRleDtcclxuICAgICAgICB0YXJnZXRMZWZ0ID0gXy5nZXRMZWZ0KHRhcmdldFNsaWRlKTtcclxuICAgICAgICBzbGlkZUxlZnQgPSBfLmdldExlZnQoXy5jdXJyZW50U2xpZGUpO1xyXG5cclxuICAgICAgICBfLmN1cnJlbnRMZWZ0ID0gXy5zd2lwZUxlZnQgPT09IG51bGwgPyBzbGlkZUxlZnQgOiBfLnN3aXBlTGVmdDtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UgJiYgXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IGZhbHNlICYmIChpbmRleCA8IDAgfHwgaW5kZXggPiBfLmdldERvdENvdW50KCkgKiBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwpKSB7XHJcbiAgICAgICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgIHRhcmdldFNsaWRlID0gXy5jdXJyZW50U2xpZGU7XHJcbiAgICAgICAgICAgICAgICBpZiAoZG9udEFuaW1hdGUgIT09IHRydWUgJiYgXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdykge1xyXG4gICAgICAgICAgICAgICAgICAgIF8uYW5pbWF0ZVNsaWRlKHNsaWRlTGVmdCwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF8ucG9zdFNsaWRlKHRhcmdldFNsaWRlKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgXy5wb3N0U2xpZGUodGFyZ2V0U2xpZGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9IGVsc2UgaWYgKF8ub3B0aW9ucy5pbmZpbml0ZSA9PT0gZmFsc2UgJiYgXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IHRydWUgJiYgKGluZGV4IDwgMCB8fCBpbmRleCA+IChfLnNsaWRlQ291bnQgLSBfLm9wdGlvbnMuc2xpZGVzVG9TY3JvbGwpKSkge1xyXG4gICAgICAgICAgICBpZiAoXy5vcHRpb25zLmZhZGUgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICB0YXJnZXRTbGlkZSA9IF8uY3VycmVudFNsaWRlO1xyXG4gICAgICAgICAgICAgICAgaWYgKGRvbnRBbmltYXRlICE9PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgICAgICAgICBfLmFuaW1hdGVTbGlkZShzbGlkZUxlZnQsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLnBvc3RTbGlkZSh0YXJnZXRTbGlkZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIF8ucG9zdFNsaWRlKHRhcmdldFNsaWRlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIF8ub3B0aW9ucy5hdXRvcGxheSApIHtcclxuICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChfLmF1dG9QbGF5VGltZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRhcmdldFNsaWRlIDwgMCkge1xyXG4gICAgICAgICAgICBpZiAoXy5zbGlkZUNvdW50ICUgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsICE9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBhbmltU2xpZGUgPSBfLnNsaWRlQ291bnQgLSAoXy5zbGlkZUNvdW50ICUgXy5vcHRpb25zLnNsaWRlc1RvU2Nyb2xsKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGFuaW1TbGlkZSA9IF8uc2xpZGVDb3VudCArIHRhcmdldFNsaWRlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmICh0YXJnZXRTbGlkZSA+PSBfLnNsaWRlQ291bnQpIHtcclxuICAgICAgICAgICAgaWYgKF8uc2xpZGVDb3VudCAlIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCAhPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgYW5pbVNsaWRlID0gMDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGFuaW1TbGlkZSA9IHRhcmdldFNsaWRlIC0gXy5zbGlkZUNvdW50O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgYW5pbVNsaWRlID0gdGFyZ2V0U2xpZGU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfLmFuaW1hdGluZyA9IHRydWU7XHJcblxyXG4gICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdiZWZvcmVDaGFuZ2UnLCBbXywgXy5jdXJyZW50U2xpZGUsIGFuaW1TbGlkZV0pO1xyXG5cclxuICAgICAgICBvbGRTbGlkZSA9IF8uY3VycmVudFNsaWRlO1xyXG4gICAgICAgIF8uY3VycmVudFNsaWRlID0gYW5pbVNsaWRlO1xyXG5cclxuICAgICAgICBfLnNldFNsaWRlQ2xhc3NlcyhfLmN1cnJlbnRTbGlkZSk7XHJcblxyXG4gICAgICAgIGlmICggXy5vcHRpb25zLmFzTmF2Rm9yICkge1xyXG5cclxuICAgICAgICAgICAgbmF2VGFyZ2V0ID0gXy5nZXROYXZUYXJnZXQoKTtcclxuICAgICAgICAgICAgbmF2VGFyZ2V0ID0gbmF2VGFyZ2V0LnNsaWNrKCdnZXRTbGljaycpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCBuYXZUYXJnZXQuc2xpZGVDb3VudCA8PSBuYXZUYXJnZXQub3B0aW9ucy5zbGlkZXNUb1Nob3cgKSB7XHJcbiAgICAgICAgICAgICAgICBuYXZUYXJnZXQuc2V0U2xpZGVDbGFzc2VzKF8uY3VycmVudFNsaWRlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF8udXBkYXRlRG90cygpO1xyXG4gICAgICAgIF8udXBkYXRlQXJyb3dzKCk7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZmFkZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBpZiAoZG9udEFuaW1hdGUgIT09IHRydWUpIHtcclxuXHJcbiAgICAgICAgICAgICAgICBfLmZhZGVTbGlkZU91dChvbGRTbGlkZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgXy5mYWRlU2xpZGUoYW5pbVNsaWRlLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICBfLnBvc3RTbGlkZShhbmltU2xpZGUpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgXy5wb3N0U2xpZGUoYW5pbVNsaWRlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBfLmFuaW1hdGVIZWlnaHQoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGRvbnRBbmltYXRlICE9PSB0cnVlICYmIF8uc2xpZGVDb3VudCA+IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgXy5hbmltYXRlU2xpZGUodGFyZ2V0TGVmdCwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBfLnBvc3RTbGlkZShhbmltU2xpZGUpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBfLnBvc3RTbGlkZShhbmltU2xpZGUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zdGFydExvYWQgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLmFycm93cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcblxyXG4gICAgICAgICAgICBfLiRwcmV2QXJyb3cuaGlkZSgpO1xyXG4gICAgICAgICAgICBfLiRuZXh0QXJyb3cuaGlkZSgpO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuZG90cyA9PT0gdHJ1ZSAmJiBfLnNsaWRlQ291bnQgPiBfLm9wdGlvbnMuc2xpZGVzVG9TaG93KSB7XHJcblxyXG4gICAgICAgICAgICBfLiRkb3RzLmhpZGUoKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfLiRzbGlkZXIuYWRkQ2xhc3MoJ3NsaWNrLWxvYWRpbmcnKTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zd2lwZURpcmVjdGlvbiA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgeERpc3QsIHlEaXN0LCByLCBzd2lwZUFuZ2xlLCBfID0gdGhpcztcclxuXHJcbiAgICAgICAgeERpc3QgPSBfLnRvdWNoT2JqZWN0LnN0YXJ0WCAtIF8udG91Y2hPYmplY3QuY3VyWDtcclxuICAgICAgICB5RGlzdCA9IF8udG91Y2hPYmplY3Quc3RhcnRZIC0gXy50b3VjaE9iamVjdC5jdXJZO1xyXG4gICAgICAgIHIgPSBNYXRoLmF0YW4yKHlEaXN0LCB4RGlzdCk7XHJcblxyXG4gICAgICAgIHN3aXBlQW5nbGUgPSBNYXRoLnJvdW5kKHIgKiAxODAgLyBNYXRoLlBJKTtcclxuICAgICAgICBpZiAoc3dpcGVBbmdsZSA8IDApIHtcclxuICAgICAgICAgICAgc3dpcGVBbmdsZSA9IDM2MCAtIE1hdGguYWJzKHN3aXBlQW5nbGUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKChzd2lwZUFuZ2xlIDw9IDQ1KSAmJiAoc3dpcGVBbmdsZSA+PSAwKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gKF8ub3B0aW9ucy5ydGwgPT09IGZhbHNlID8gJ2xlZnQnIDogJ3JpZ2h0Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICgoc3dpcGVBbmdsZSA8PSAzNjApICYmIChzd2lwZUFuZ2xlID49IDMxNSkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChfLm9wdGlvbnMucnRsID09PSBmYWxzZSA/ICdsZWZ0JyA6ICdyaWdodCcpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoKHN3aXBlQW5nbGUgPj0gMTM1KSAmJiAoc3dpcGVBbmdsZSA8PSAyMjUpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoXy5vcHRpb25zLnJ0bCA9PT0gZmFsc2UgPyAncmlnaHQnIDogJ2xlZnQnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbFN3aXBpbmcgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgaWYgKChzd2lwZUFuZ2xlID49IDM1KSAmJiAoc3dpcGVBbmdsZSA8PSAxMzUpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2Rvd24nO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICd1cCc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAndmVydGljYWwnO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnN3aXBlRW5kID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICBzbGlkZUNvdW50LFxyXG4gICAgICAgICAgICBkaXJlY3Rpb247XHJcblxyXG4gICAgICAgIF8uZHJhZ2dpbmcgPSBmYWxzZTtcclxuICAgICAgICBfLnN3aXBpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKF8uc2Nyb2xsaW5nKSB7XHJcbiAgICAgICAgICAgIF8uc2Nyb2xsaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF8uaW50ZXJydXB0ZWQgPSBmYWxzZTtcclxuICAgICAgICBfLnNob3VsZENsaWNrID0gKCBfLnRvdWNoT2JqZWN0LnN3aXBlTGVuZ3RoID4gMTAgKSA/IGZhbHNlIDogdHJ1ZTtcclxuXHJcbiAgICAgICAgaWYgKCBfLnRvdWNoT2JqZWN0LmN1clggPT09IHVuZGVmaW5lZCApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCBfLnRvdWNoT2JqZWN0LmVkZ2VIaXQgPT09IHRydWUgKSB7XHJcbiAgICAgICAgICAgIF8uJHNsaWRlci50cmlnZ2VyKCdlZGdlJywgW18sIF8uc3dpcGVEaXJlY3Rpb24oKSBdKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA+PSBfLnRvdWNoT2JqZWN0Lm1pblN3aXBlICkge1xyXG5cclxuICAgICAgICAgICAgZGlyZWN0aW9uID0gXy5zd2lwZURpcmVjdGlvbigpO1xyXG5cclxuICAgICAgICAgICAgc3dpdGNoICggZGlyZWN0aW9uICkge1xyXG5cclxuICAgICAgICAgICAgICAgIGNhc2UgJ2xlZnQnOlxyXG4gICAgICAgICAgICAgICAgY2FzZSAnZG93bic6XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlQ291bnQgPVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLm9wdGlvbnMuc3dpcGVUb1NsaWRlID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uY2hlY2tOYXZpZ2FibGUoIF8uY3VycmVudFNsaWRlICsgXy5nZXRTbGlkZUNvdW50KCkgKSA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSArIF8uZ2V0U2xpZGVDb3VudCgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnREaXJlY3Rpb24gPSAwO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgICAgICBjYXNlICdyaWdodCc6XHJcbiAgICAgICAgICAgICAgICBjYXNlICd1cCc6XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlQ291bnQgPVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLm9wdGlvbnMuc3dpcGVUb1NsaWRlID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uY2hlY2tOYXZpZ2FibGUoIF8uY3VycmVudFNsaWRlIC0gXy5nZXRTbGlkZUNvdW50KCkgKSA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnRTbGlkZSAtIF8uZ2V0U2xpZGVDb3VudCgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBfLmN1cnJlbnREaXJlY3Rpb24gPSAxO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG5cclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmKCBkaXJlY3Rpb24gIT0gJ3ZlcnRpY2FsJyApIHtcclxuXHJcbiAgICAgICAgICAgICAgICBfLnNsaWRlSGFuZGxlciggc2xpZGVDb3VudCApO1xyXG4gICAgICAgICAgICAgICAgXy50b3VjaE9iamVjdCA9IHt9O1xyXG4gICAgICAgICAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ3N3aXBlJywgW18sIGRpcmVjdGlvbiBdKTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgIGlmICggXy50b3VjaE9iamVjdC5zdGFydFggIT09IF8udG91Y2hPYmplY3QuY3VyWCApIHtcclxuXHJcbiAgICAgICAgICAgICAgICBfLnNsaWRlSGFuZGxlciggXy5jdXJyZW50U2xpZGUgKTtcclxuICAgICAgICAgICAgICAgIF8udG91Y2hPYmplY3QgPSB7fTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnN3aXBlSGFuZGxlciA9IGZ1bmN0aW9uKGV2ZW50KSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKChfLm9wdGlvbnMuc3dpcGUgPT09IGZhbHNlKSB8fCAoJ29udG91Y2hlbmQnIGluIGRvY3VtZW50ICYmIF8ub3B0aW9ucy5zd2lwZSA9PT0gZmFsc2UpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9IGVsc2UgaWYgKF8ub3B0aW9ucy5kcmFnZ2FibGUgPT09IGZhbHNlICYmIGV2ZW50LnR5cGUuaW5kZXhPZignbW91c2UnKSAhPT0gLTEpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXy50b3VjaE9iamVjdC5maW5nZXJDb3VudCA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQgJiYgZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzICE9PSB1bmRlZmluZWQgP1xyXG4gICAgICAgICAgICBldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXMubGVuZ3RoIDogMTtcclxuXHJcbiAgICAgICAgXy50b3VjaE9iamVjdC5taW5Td2lwZSA9IF8ubGlzdFdpZHRoIC8gXy5vcHRpb25zXHJcbiAgICAgICAgICAgIC50b3VjaFRocmVzaG9sZDtcclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbFN3aXBpbmcgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgXy50b3VjaE9iamVjdC5taW5Td2lwZSA9IF8ubGlzdEhlaWdodCAvIF8ub3B0aW9uc1xyXG4gICAgICAgICAgICAgICAgLnRvdWNoVGhyZXNob2xkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc3dpdGNoIChldmVudC5kYXRhLmFjdGlvbikge1xyXG5cclxuICAgICAgICAgICAgY2FzZSAnc3RhcnQnOlxyXG4gICAgICAgICAgICAgICAgXy5zd2lwZVN0YXJ0KGV2ZW50KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSAnbW92ZSc6XHJcbiAgICAgICAgICAgICAgICBfLnN3aXBlTW92ZShldmVudCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ2VuZCc6XHJcbiAgICAgICAgICAgICAgICBfLnN3aXBlRW5kKGV2ZW50KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUuc3dpcGVNb3ZlID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICBlZGdlV2FzSGl0ID0gZmFsc2UsXHJcbiAgICAgICAgICAgIGN1ckxlZnQsIHN3aXBlRGlyZWN0aW9uLCBzd2lwZUxlbmd0aCwgcG9zaXRpb25PZmZzZXQsIHRvdWNoZXMsIHZlcnRpY2FsU3dpcGVMZW5ndGg7XHJcblxyXG4gICAgICAgIHRvdWNoZXMgPSBldmVudC5vcmlnaW5hbEV2ZW50ICE9PSB1bmRlZmluZWQgPyBldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXMgOiBudWxsO1xyXG5cclxuICAgICAgICBpZiAoIV8uZHJhZ2dpbmcgfHwgXy5zY3JvbGxpbmcgfHwgdG91Y2hlcyAmJiB0b3VjaGVzLmxlbmd0aCAhPT0gMSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjdXJMZWZ0ID0gXy5nZXRMZWZ0KF8uY3VycmVudFNsaWRlKTtcclxuXHJcbiAgICAgICAgXy50b3VjaE9iamVjdC5jdXJYID0gdG91Y2hlcyAhPT0gdW5kZWZpbmVkID8gdG91Y2hlc1swXS5wYWdlWCA6IGV2ZW50LmNsaWVudFg7XHJcbiAgICAgICAgXy50b3VjaE9iamVjdC5jdXJZID0gdG91Y2hlcyAhPT0gdW5kZWZpbmVkID8gdG91Y2hlc1swXS5wYWdlWSA6IGV2ZW50LmNsaWVudFk7XHJcblxyXG4gICAgICAgIF8udG91Y2hPYmplY3Quc3dpcGVMZW5ndGggPSBNYXRoLnJvdW5kKE1hdGguc3FydChcclxuICAgICAgICAgICAgTWF0aC5wb3coXy50b3VjaE9iamVjdC5jdXJYIC0gXy50b3VjaE9iamVjdC5zdGFydFgsIDIpKSk7XHJcblxyXG4gICAgICAgIHZlcnRpY2FsU3dpcGVMZW5ndGggPSBNYXRoLnJvdW5kKE1hdGguc3FydChcclxuICAgICAgICAgICAgTWF0aC5wb3coXy50b3VjaE9iamVjdC5jdXJZIC0gXy50b3VjaE9iamVjdC5zdGFydFksIDIpKSk7XHJcblxyXG4gICAgICAgIGlmICghXy5vcHRpb25zLnZlcnRpY2FsU3dpcGluZyAmJiAhXy5zd2lwaW5nICYmIHZlcnRpY2FsU3dpcGVMZW5ndGggPiA0KSB7XHJcbiAgICAgICAgICAgIF8uc2Nyb2xsaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy52ZXJ0aWNhbFN3aXBpbmcgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCA9IHZlcnRpY2FsU3dpcGVMZW5ndGg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzd2lwZURpcmVjdGlvbiA9IF8uc3dpcGVEaXJlY3Rpb24oKTtcclxuXHJcbiAgICAgICAgaWYgKGV2ZW50Lm9yaWdpbmFsRXZlbnQgIT09IHVuZGVmaW5lZCAmJiBfLnRvdWNoT2JqZWN0LnN3aXBlTGVuZ3RoID4gNCkge1xyXG4gICAgICAgICAgICBfLnN3aXBpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcG9zaXRpb25PZmZzZXQgPSAoXy5vcHRpb25zLnJ0bCA9PT0gZmFsc2UgPyAxIDogLTEpICogKF8udG91Y2hPYmplY3QuY3VyWCA+IF8udG91Y2hPYmplY3Quc3RhcnRYID8gMSA6IC0xKTtcclxuICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsU3dpcGluZyA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBwb3NpdGlvbk9mZnNldCA9IF8udG91Y2hPYmplY3QuY3VyWSA+IF8udG91Y2hPYmplY3Quc3RhcnRZID8gMSA6IC0xO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIHN3aXBlTGVuZ3RoID0gXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aDtcclxuXHJcbiAgICAgICAgXy50b3VjaE9iamVjdC5lZGdlSGl0ID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmIChfLm9wdGlvbnMuaW5maW5pdGUgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIGlmICgoXy5jdXJyZW50U2xpZGUgPT09IDAgJiYgc3dpcGVEaXJlY3Rpb24gPT09ICdyaWdodCcpIHx8IChfLmN1cnJlbnRTbGlkZSA+PSBfLmdldERvdENvdW50KCkgJiYgc3dpcGVEaXJlY3Rpb24gPT09ICdsZWZ0JykpIHtcclxuICAgICAgICAgICAgICAgIHN3aXBlTGVuZ3RoID0gXy50b3VjaE9iamVjdC5zd2lwZUxlbmd0aCAqIF8ub3B0aW9ucy5lZGdlRnJpY3Rpb247XHJcbiAgICAgICAgICAgICAgICBfLnRvdWNoT2JqZWN0LmVkZ2VIaXQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICBfLnN3aXBlTGVmdCA9IGN1ckxlZnQgKyBzd2lwZUxlbmd0aCAqIHBvc2l0aW9uT2Zmc2V0O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIF8uc3dpcGVMZWZ0ID0gY3VyTGVmdCArIChzd2lwZUxlbmd0aCAqIChfLiRsaXN0LmhlaWdodCgpIC8gXy5saXN0V2lkdGgpKSAqIHBvc2l0aW9uT2Zmc2V0O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoXy5vcHRpb25zLnZlcnRpY2FsU3dpcGluZyA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBfLnN3aXBlTGVmdCA9IGN1ckxlZnQgKyBzd2lwZUxlbmd0aCAqIHBvc2l0aW9uT2Zmc2V0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKF8ub3B0aW9ucy5mYWRlID09PSB0cnVlIHx8IF8ub3B0aW9ucy50b3VjaE1vdmUgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLmFuaW1hdGluZyA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBfLnN3aXBlTGVmdCA9IG51bGw7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF8uc2V0Q1NTKF8uc3dpcGVMZWZ0KTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS5zd2lwZVN0YXJ0ID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICB0b3VjaGVzO1xyXG5cclxuICAgICAgICBfLmludGVycnVwdGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgaWYgKF8udG91Y2hPYmplY3QuZmluZ2VyQ291bnQgIT09IDEgfHwgXy5zbGlkZUNvdW50IDw9IF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cpIHtcclxuICAgICAgICAgICAgXy50b3VjaE9iamVjdCA9IHt9O1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoZXZlbnQub3JpZ2luYWxFdmVudCAhPT0gdW5kZWZpbmVkICYmIGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlcyAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRvdWNoZXMgPSBldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfLnRvdWNoT2JqZWN0LnN0YXJ0WCA9IF8udG91Y2hPYmplY3QuY3VyWCA9IHRvdWNoZXMgIT09IHVuZGVmaW5lZCA/IHRvdWNoZXMucGFnZVggOiBldmVudC5jbGllbnRYO1xyXG4gICAgICAgIF8udG91Y2hPYmplY3Quc3RhcnRZID0gXy50b3VjaE9iamVjdC5jdXJZID0gdG91Y2hlcyAhPT0gdW5kZWZpbmVkID8gdG91Y2hlcy5wYWdlWSA6IGV2ZW50LmNsaWVudFk7XHJcblxyXG4gICAgICAgIF8uZHJhZ2dpbmcgPSB0cnVlO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnVuZmlsdGVyU2xpZGVzID0gU2xpY2sucHJvdG90eXBlLnNsaWNrVW5maWx0ZXIgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICBpZiAoXy4kc2xpZGVzQ2FjaGUgIT09IG51bGwpIHtcclxuXHJcbiAgICAgICAgICAgIF8udW5sb2FkKCk7XHJcblxyXG4gICAgICAgICAgICBfLiRzbGlkZVRyYWNrLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5zbGlkZSkuZGV0YWNoKCk7XHJcblxyXG4gICAgICAgICAgICBfLiRzbGlkZXNDYWNoZS5hcHBlbmRUbyhfLiRzbGlkZVRyYWNrKTtcclxuXHJcbiAgICAgICAgICAgIF8ucmVpbml0KCk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS51bmxvYWQgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzO1xyXG5cclxuICAgICAgICAkKCcuc2xpY2stY2xvbmVkJywgXy4kc2xpZGVyKS5yZW1vdmUoKTtcclxuXHJcbiAgICAgICAgaWYgKF8uJGRvdHMpIHtcclxuICAgICAgICAgICAgXy4kZG90cy5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLiRwcmV2QXJyb3cgJiYgXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5wcmV2QXJyb3cpKSB7XHJcbiAgICAgICAgICAgIF8uJHByZXZBcnJvdy5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChfLiRuZXh0QXJyb3cgJiYgXy5odG1sRXhwci50ZXN0KF8ub3B0aW9ucy5uZXh0QXJyb3cpKSB7XHJcbiAgICAgICAgICAgIF8uJG5leHRBcnJvdy5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF8uJHNsaWRlc1xyXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3NsaWNrLXNsaWRlIHNsaWNrLWFjdGl2ZSBzbGljay12aXNpYmxlIHNsaWNrLWN1cnJlbnQnKVxyXG4gICAgICAgICAgICAuYXR0cignYXJpYS1oaWRkZW4nLCAndHJ1ZScpXHJcbiAgICAgICAgICAgIC5jc3MoJ3dpZHRoJywgJycpO1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgU2xpY2sucHJvdG90eXBlLnVuc2xpY2sgPSBmdW5jdGlvbihmcm9tQnJlYWtwb2ludCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcbiAgICAgICAgXy4kc2xpZGVyLnRyaWdnZXIoJ3Vuc2xpY2snLCBbXywgZnJvbUJyZWFrcG9pbnRdKTtcclxuICAgICAgICBfLmRlc3Ryb3koKTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS51cGRhdGVBcnJvd3MgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIF8gPSB0aGlzLFxyXG4gICAgICAgICAgICBjZW50ZXJPZmZzZXQ7XHJcblxyXG4gICAgICAgIGNlbnRlck9mZnNldCA9IE1hdGguZmxvb3IoXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAvIDIpO1xyXG5cclxuICAgICAgICBpZiAoIF8ub3B0aW9ucy5hcnJvd3MgPT09IHRydWUgJiZcclxuICAgICAgICAgICAgXy5zbGlkZUNvdW50ID4gXy5vcHRpb25zLnNsaWRlc1RvU2hvdyAmJlxyXG4gICAgICAgICAgICAhXy5vcHRpb25zLmluZmluaXRlICkge1xyXG5cclxuICAgICAgICAgICAgXy4kcHJldkFycm93LnJlbW92ZUNsYXNzKCdzbGljay1kaXNhYmxlZCcpLmF0dHIoJ2FyaWEtZGlzYWJsZWQnLCAnZmFsc2UnKTtcclxuICAgICAgICAgICAgXy4kbmV4dEFycm93LnJlbW92ZUNsYXNzKCdzbGljay1kaXNhYmxlZCcpLmF0dHIoJ2FyaWEtZGlzYWJsZWQnLCAnZmFsc2UnKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChfLmN1cnJlbnRTbGlkZSA9PT0gMCkge1xyXG5cclxuICAgICAgICAgICAgICAgIF8uJHByZXZBcnJvdy5hZGRDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ3RydWUnKTtcclxuICAgICAgICAgICAgICAgIF8uJG5leHRBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ2ZhbHNlJyk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKF8uY3VycmVudFNsaWRlID49IF8uc2xpZGVDb3VudCAtIF8ub3B0aW9ucy5zbGlkZXNUb1Nob3cgJiYgXy5vcHRpb25zLmNlbnRlck1vZGUgPT09IGZhbHNlKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgXy4kbmV4dEFycm93LmFkZENsYXNzKCdzbGljay1kaXNhYmxlZCcpLmF0dHIoJ2FyaWEtZGlzYWJsZWQnLCAndHJ1ZScpO1xyXG4gICAgICAgICAgICAgICAgXy4kcHJldkFycm93LnJlbW92ZUNsYXNzKCdzbGljay1kaXNhYmxlZCcpLmF0dHIoJ2FyaWEtZGlzYWJsZWQnLCAnZmFsc2UnKTtcclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoXy5jdXJyZW50U2xpZGUgPj0gXy5zbGlkZUNvdW50IC0gMSAmJiBfLm9wdGlvbnMuY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xyXG5cclxuICAgICAgICAgICAgICAgIF8uJG5leHRBcnJvdy5hZGRDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ3RydWUnKTtcclxuICAgICAgICAgICAgICAgIF8uJHByZXZBcnJvdy5yZW1vdmVDbGFzcygnc2xpY2stZGlzYWJsZWQnKS5hdHRyKCdhcmlhLWRpc2FibGVkJywgJ2ZhbHNlJyk7XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNsaWNrLnByb3RvdHlwZS51cGRhdGVEb3RzID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHZhciBfID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKF8uJGRvdHMgIT09IG51bGwpIHtcclxuXHJcbiAgICAgICAgICAgIF8uJGRvdHNcclxuICAgICAgICAgICAgICAgIC5maW5kKCdsaScpXHJcbiAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdzbGljay1hY3RpdmUnKVxyXG4gICAgICAgICAgICAgICAgICAgIC5lbmQoKTtcclxuXHJcbiAgICAgICAgICAgIF8uJGRvdHNcclxuICAgICAgICAgICAgICAgIC5maW5kKCdsaScpXHJcbiAgICAgICAgICAgICAgICAuZXEoTWF0aC5mbG9vcihfLmN1cnJlbnRTbGlkZSAvIF8ub3B0aW9ucy5zbGlkZXNUb1Njcm9sbCkpXHJcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3NsaWNrLWFjdGl2ZScpO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBTbGljay5wcm90b3R5cGUudmlzaWJpbGl0eSA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICB2YXIgXyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmICggXy5vcHRpb25zLmF1dG9wbGF5ICkge1xyXG5cclxuICAgICAgICAgICAgaWYgKCBkb2N1bWVudFtfLmhpZGRlbl0gKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgXy5pbnRlcnJ1cHRlZCA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgIF8uaW50ZXJydXB0ZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcblxyXG4gICAgJC5mbi5zbGljayA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBfID0gdGhpcyxcclxuICAgICAgICAgICAgb3B0ID0gYXJndW1lbnRzWzBdLFxyXG4gICAgICAgICAgICBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcclxuICAgICAgICAgICAgbCA9IF8ubGVuZ3RoLFxyXG4gICAgICAgICAgICBpLFxyXG4gICAgICAgICAgICByZXQ7XHJcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IGw7IGkrKykge1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIG9wdCA9PSAnb2JqZWN0JyB8fCB0eXBlb2Ygb3B0ID09ICd1bmRlZmluZWQnKVxyXG4gICAgICAgICAgICAgICAgX1tpXS5zbGljayA9IG5ldyBTbGljayhfW2ldLCBvcHQpO1xyXG4gICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICByZXQgPSBfW2ldLnNsaWNrW29wdF0uYXBwbHkoX1tpXS5zbGljaywgYXJncyk7XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgcmV0ICE9ICd1bmRlZmluZWQnKSByZXR1cm4gcmV0O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gXztcclxuICAgIH07XHJcblxyXG59KSk7XHJcbiIsIi8qXG4gKiBqUXVlcnkgU3VwZXJmaXNoIE1lbnUgUGx1Z2luIC0gdjEuNy4xMFxuICogQ29weXJpZ2h0IChjKSAyMDE4IEpvZWwgQmlyY2hcbiAqXG4gKiBEdWFsIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgYW5kIEdQTCBsaWNlbnNlczpcbiAqXHRodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxuICpcdGh0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwuaHRtbFxuICovXG5cbjsoZnVuY3Rpb24gKCQsIHcpIHtcblx0XCJ1c2Ugc3RyaWN0XCI7XG5cblx0dmFyIG1ldGhvZHMgPSAoZnVuY3Rpb24gKCkge1xuXHRcdC8vIHByaXZhdGUgcHJvcGVydGllcyBhbmQgbWV0aG9kcyBnbyBoZXJlXG5cdFx0dmFyIGMgPSB7XG5cdFx0XHRcdGJjQ2xhc3M6ICdzZi1icmVhZGNydW1iJyxcblx0XHRcdFx0bWVudUNsYXNzOiAnc2YtanMtZW5hYmxlZCcsXG5cdFx0XHRcdGFuY2hvckNsYXNzOiAnc2Ytd2l0aC11bCcsXG5cdFx0XHRcdG1lbnVBcnJvd0NsYXNzOiAnc2YtYXJyb3dzJ1xuXHRcdFx0fSxcblx0XHRcdGlvcyA9IChmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHZhciBpb3MgPSAvXig/IVtcXHdcXFddKldpbmRvd3MgUGhvbmUpW1xcd1xcV10qKGlQaG9uZXxpUGFkfGlQb2QpL2kudGVzdChuYXZpZ2F0b3IudXNlckFnZW50KTtcblx0XHRcdFx0aWYgKGlvcykge1xuXHRcdFx0XHRcdC8vIHRhcCBhbnl3aGVyZSBvbiBpT1MgdG8gdW5mb2N1cyBhIHN1Ym1lbnVcblx0XHRcdFx0XHQkKCdodG1sJykuY3NzKCdjdXJzb3InLCAncG9pbnRlcicpLm9uKCdjbGljaycsICQubm9vcCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0cmV0dXJuIGlvcztcblx0XHRcdH0pKCksXG5cdFx0XHR3cDcgPSAoZnVuY3Rpb24gKCkge1xuXHRcdFx0XHR2YXIgc3R5bGUgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGU7XG5cdFx0XHRcdHJldHVybiAoJ2JlaGF2aW9yJyBpbiBzdHlsZSAmJiAnZmlsbCcgaW4gc3R5bGUgJiYgL2llbW9iaWxlL2kudGVzdChuYXZpZ2F0b3IudXNlckFnZW50KSk7XG5cdFx0XHR9KSgpLFxuXHRcdFx0dW5wcmVmaXhlZFBvaW50ZXJFdmVudHMgPSAoZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRyZXR1cm4gKCEhdy5Qb2ludGVyRXZlbnQpO1xuXHRcdFx0fSkoKSxcblx0XHRcdHRvZ2dsZU1lbnVDbGFzc2VzID0gZnVuY3Rpb24gKCRtZW51LCBvLCBhZGQpIHtcblx0XHRcdFx0dmFyIGNsYXNzZXMgPSBjLm1lbnVDbGFzcyxcblx0XHRcdFx0XHRtZXRob2Q7XG5cdFx0XHRcdGlmIChvLmNzc0Fycm93cykge1xuXHRcdFx0XHRcdGNsYXNzZXMgKz0gJyAnICsgYy5tZW51QXJyb3dDbGFzcztcblx0XHRcdFx0fVxuXHRcdFx0XHRtZXRob2QgPSAoYWRkKSA/ICdhZGRDbGFzcycgOiAncmVtb3ZlQ2xhc3MnO1xuXHRcdFx0XHQkbWVudVttZXRob2RdKGNsYXNzZXMpO1xuXHRcdFx0fSxcblx0XHRcdHNldFBhdGhUb0N1cnJlbnQgPSBmdW5jdGlvbiAoJG1lbnUsIG8pIHtcblx0XHRcdFx0cmV0dXJuICRtZW51LmZpbmQoJ2xpLicgKyBvLnBhdGhDbGFzcykuc2xpY2UoMCwgby5wYXRoTGV2ZWxzKVxuXHRcdFx0XHRcdC5hZGRDbGFzcyhvLmhvdmVyQ2xhc3MgKyAnICcgKyBjLmJjQ2xhc3MpXG5cdFx0XHRcdFx0XHQuZmlsdGVyKGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRcdFx0cmV0dXJuICgkKHRoaXMpLmNoaWxkcmVuKG8ucG9wVXBTZWxlY3RvcikuaGlkZSgpLnNob3coKS5sZW5ndGgpO1xuXHRcdFx0XHRcdFx0fSkucmVtb3ZlQ2xhc3Moby5wYXRoQ2xhc3MpO1xuXHRcdFx0fSxcblx0XHRcdHRvZ2dsZUFuY2hvckNsYXNzID0gZnVuY3Rpb24gKCRsaSwgYWRkKSB7XG5cdFx0XHRcdHZhciBtZXRob2QgPSAoYWRkKSA/ICdhZGRDbGFzcycgOiAncmVtb3ZlQ2xhc3MnO1xuXHRcdFx0XHQkbGkuY2hpbGRyZW4oJ2EnKVttZXRob2RdKGMuYW5jaG9yQ2xhc3MpO1xuXHRcdFx0fSxcblx0XHRcdHRvZ2dsZVRvdWNoQWN0aW9uID0gZnVuY3Rpb24gKCRtZW51KSB7XG5cdFx0XHRcdHZhciBtc1RvdWNoQWN0aW9uID0gJG1lbnUuY3NzKCdtcy10b3VjaC1hY3Rpb24nKTtcblx0XHRcdFx0dmFyIHRvdWNoQWN0aW9uID0gJG1lbnUuY3NzKCd0b3VjaC1hY3Rpb24nKTtcblx0XHRcdFx0dG91Y2hBY3Rpb24gPSB0b3VjaEFjdGlvbiB8fCBtc1RvdWNoQWN0aW9uO1xuXHRcdFx0XHR0b3VjaEFjdGlvbiA9ICh0b3VjaEFjdGlvbiA9PT0gJ3Bhbi15JykgPyAnYXV0bycgOiAncGFuLXknO1xuXHRcdFx0XHQkbWVudS5jc3Moe1xuXHRcdFx0XHRcdCdtcy10b3VjaC1hY3Rpb24nOiB0b3VjaEFjdGlvbixcblx0XHRcdFx0XHQndG91Y2gtYWN0aW9uJzogdG91Y2hBY3Rpb25cblx0XHRcdFx0fSk7XG5cdFx0XHR9LFxuXHRcdFx0Z2V0TWVudSA9IGZ1bmN0aW9uICgkZWwpIHtcblx0XHRcdFx0cmV0dXJuICRlbC5jbG9zZXN0KCcuJyArIGMubWVudUNsYXNzKTtcblx0XHRcdH0sXG5cdFx0XHRnZXRPcHRpb25zID0gZnVuY3Rpb24gKCRlbCkge1xuXHRcdFx0XHRyZXR1cm4gZ2V0TWVudSgkZWwpLmRhdGEoJ3NmT3B0aW9ucycpO1xuXHRcdFx0fSxcblx0XHRcdG92ZXIgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcdFx0byA9IGdldE9wdGlvbnMoJHRoaXMpO1xuXHRcdFx0XHRjbGVhclRpbWVvdXQoby5zZlRpbWVyKTtcblx0XHRcdFx0JHRoaXMuc2libGluZ3MoKS5zdXBlcmZpc2goJ2hpZGUnKS5lbmQoKS5zdXBlcmZpc2goJ3Nob3cnKTtcblx0XHRcdH0sXG5cdFx0XHRjbG9zZSA9IGZ1bmN0aW9uIChvKSB7XG5cdFx0XHRcdG8ucmV0YWluUGF0aCA9ICgkLmluQXJyYXkodGhpc1swXSwgby4kcGF0aCkgPiAtMSk7XG5cdFx0XHRcdHRoaXMuc3VwZXJmaXNoKCdoaWRlJyk7XG5cblx0XHRcdFx0aWYgKCF0aGlzLnBhcmVudHMoJy4nICsgby5ob3ZlckNsYXNzKS5sZW5ndGgpIHtcblx0XHRcdFx0XHRvLm9uSWRsZS5jYWxsKGdldE1lbnUodGhpcykpO1xuXHRcdFx0XHRcdGlmIChvLiRwYXRoLmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0JC5wcm94eShvdmVyLCBvLiRwYXRoKSgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG91dCA9IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0dmFyICR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFx0XHRvID0gZ2V0T3B0aW9ucygkdGhpcyk7XG5cdFx0XHRcdGlmIChpb3MpIHtcblx0XHRcdFx0XHQkLnByb3h5KGNsb3NlLCAkdGhpcywgbykoKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRjbGVhclRpbWVvdXQoby5zZlRpbWVyKTtcblx0XHRcdFx0XHRvLnNmVGltZXIgPSBzZXRUaW1lb3V0KCQucHJveHkoY2xvc2UsICR0aGlzLCBvKSwgby5kZWxheSk7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR0b3VjaEhhbmRsZXIgPSBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XHRcdG8gPSBnZXRPcHRpb25zKCR0aGlzKSxcblx0XHRcdFx0XHQkdWwgPSAkdGhpcy5zaWJsaW5ncyhlLmRhdGEucG9wVXBTZWxlY3Rvcik7XG5cblx0XHRcdFx0aWYgKG8ub25IYW5kbGVUb3VjaC5jYWxsKCR1bCkgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZiAoJHVsLmxlbmd0aCA+IDAgJiYgJHVsLmlzKCc6aGlkZGVuJykpIHtcblx0XHRcdFx0XHQkdGhpcy5vbmUoJ2NsaWNrLnN1cGVyZmlzaCcsIGZhbHNlKTtcblx0XHRcdFx0XHRpZiAoZS50eXBlID09PSAnTVNQb2ludGVyRG93bicgfHwgZS50eXBlID09PSAncG9pbnRlcmRvd24nKSB7XG5cdFx0XHRcdFx0XHQkdGhpcy50cmlnZ2VyKCdmb2N1cycpO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHQkLnByb3h5KG92ZXIsICR0aGlzLnBhcmVudCgnbGknKSkoKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRhcHBseUhhbmRsZXJzID0gZnVuY3Rpb24gKCRtZW51LCBvKSB7XG5cdFx0XHRcdHZhciB0YXJnZXRzID0gJ2xpOmhhcygnICsgby5wb3BVcFNlbGVjdG9yICsgJyknO1xuXHRcdFx0XHRpZiAoJC5mbi5ob3ZlckludGVudCAmJiAhby5kaXNhYmxlSEkpIHtcblx0XHRcdFx0XHQkbWVudS5ob3ZlckludGVudChvdmVyLCBvdXQsIHRhcmdldHMpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Uge1xuXHRcdFx0XHRcdCRtZW51XG5cdFx0XHRcdFx0XHQub24oJ21vdXNlZW50ZXIuc3VwZXJmaXNoJywgdGFyZ2V0cywgb3Zlcilcblx0XHRcdFx0XHRcdC5vbignbW91c2VsZWF2ZS5zdXBlcmZpc2gnLCB0YXJnZXRzLCBvdXQpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHZhciB0b3VjaGV2ZW50ID0gJ01TUG9pbnRlckRvd24uc3VwZXJmaXNoJztcblx0XHRcdFx0aWYgKHVucHJlZml4ZWRQb2ludGVyRXZlbnRzKSB7XG5cdFx0XHRcdFx0dG91Y2hldmVudCA9ICdwb2ludGVyZG93bi5zdXBlcmZpc2gnO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICghaW9zKSB7XG5cdFx0XHRcdFx0dG91Y2hldmVudCArPSAnIHRvdWNoZW5kLnN1cGVyZmlzaCc7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKHdwNykge1xuXHRcdFx0XHRcdHRvdWNoZXZlbnQgKz0gJyBtb3VzZWRvd24uc3VwZXJmaXNoJztcblx0XHRcdFx0fVxuXHRcdFx0XHQkbWVudVxuXHRcdFx0XHRcdC5vbignZm9jdXNpbi5zdXBlcmZpc2gnLCAnbGknLCBvdmVyKVxuXHRcdFx0XHRcdC5vbignZm9jdXNvdXQuc3VwZXJmaXNoJywgJ2xpJywgb3V0KVxuXHRcdFx0XHRcdC5vbih0b3VjaGV2ZW50LCAnYScsIG8sIHRvdWNoSGFuZGxlcik7XG5cdFx0XHR9O1xuXG5cdFx0cmV0dXJuIHtcblx0XHRcdC8vIHB1YmxpYyBtZXRob2RzXG5cdFx0XHRoaWRlOiBmdW5jdGlvbiAoaW5zdGFudCkge1xuXHRcdFx0XHRpZiAodGhpcy5sZW5ndGgpIHtcblx0XHRcdFx0XHR2YXIgJHRoaXMgPSB0aGlzLFxuXHRcdFx0XHRcdFx0byA9IGdldE9wdGlvbnMoJHRoaXMpO1xuXHRcdFx0XHRcdGlmICghbykge1xuXHRcdFx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHZhciBub3QgPSAoby5yZXRhaW5QYXRoID09PSB0cnVlKSA/IG8uJHBhdGggOiAnJyxcblx0XHRcdFx0XHRcdCR1bCA9ICR0aGlzLmZpbmQoJ2xpLicgKyBvLmhvdmVyQ2xhc3MpLmFkZCh0aGlzKS5ub3Qobm90KS5yZW1vdmVDbGFzcyhvLmhvdmVyQ2xhc3MpLmNoaWxkcmVuKG8ucG9wVXBTZWxlY3RvciksXG5cdFx0XHRcdFx0XHRzcGVlZCA9IG8uc3BlZWRPdXQ7XG5cblx0XHRcdFx0XHRpZiAoaW5zdGFudCkge1xuXHRcdFx0XHRcdFx0JHVsLnNob3coKTtcblx0XHRcdFx0XHRcdHNwZWVkID0gMDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0by5yZXRhaW5QYXRoID0gZmFsc2U7XG5cblx0XHRcdFx0XHRpZiAoby5vbkJlZm9yZUhpZGUuY2FsbCgkdWwpID09PSBmYWxzZSkge1xuXHRcdFx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0JHVsLnN0b3AodHJ1ZSwgdHJ1ZSkuYW5pbWF0ZShvLmFuaW1hdGlvbk91dCwgc3BlZWQsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcdFx0XHRvLm9uSGlkZS5jYWxsKCR0aGlzKTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fVxuXHRcdFx0XHRyZXR1cm4gdGhpcztcblx0XHRcdH0sXG5cdFx0XHRzaG93OiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHZhciBvID0gZ2V0T3B0aW9ucyh0aGlzKTtcblx0XHRcdFx0aWYgKCFvKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHRcdH1cblx0XHRcdFx0dmFyICR0aGlzID0gdGhpcy5hZGRDbGFzcyhvLmhvdmVyQ2xhc3MpLFxuXHRcdFx0XHRcdCR1bCA9ICR0aGlzLmNoaWxkcmVuKG8ucG9wVXBTZWxlY3Rvcik7XG5cblx0XHRcdFx0aWYgKG8ub25CZWZvcmVTaG93LmNhbGwoJHVsKSA9PT0gZmFsc2UpIHtcblx0XHRcdFx0XHRyZXR1cm4gdGhpcztcblx0XHRcdFx0fVxuXG5cdFx0XHRcdCR1bC5zdG9wKHRydWUsIHRydWUpLmFuaW1hdGUoby5hbmltYXRpb24sIG8uc3BlZWQsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRvLm9uU2hvdy5jYWxsKCR1bCk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRyZXR1cm4gdGhpcztcblx0XHRcdH0sXG5cdFx0XHRkZXN0cm95OiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcdFx0XHRvID0gJHRoaXMuZGF0YSgnc2ZPcHRpb25zJyksXG5cdFx0XHRcdFx0XHQkaGFzUG9wVXA7XG5cdFx0XHRcdFx0aWYgKCFvKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdCRoYXNQb3BVcCA9ICR0aGlzLmZpbmQoby5wb3BVcFNlbGVjdG9yKS5wYXJlbnQoJ2xpJyk7XG5cdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KG8uc2ZUaW1lcik7XG5cdFx0XHRcdFx0dG9nZ2xlTWVudUNsYXNzZXMoJHRoaXMsIG8pO1xuXHRcdFx0XHRcdHRvZ2dsZUFuY2hvckNsYXNzKCRoYXNQb3BVcCk7XG5cdFx0XHRcdFx0dG9nZ2xlVG91Y2hBY3Rpb24oJHRoaXMpO1xuXHRcdFx0XHRcdC8vIHJlbW92ZSBldmVudCBoYW5kbGVyc1xuXHRcdFx0XHRcdCR0aGlzLm9mZignLnN1cGVyZmlzaCcpLm9mZignLmhvdmVySW50ZW50Jyk7XG5cdFx0XHRcdFx0Ly8gY2xlYXIgYW5pbWF0aW9uJ3MgaW5saW5lIGRpc3BsYXkgc3R5bGVcblx0XHRcdFx0XHQkaGFzUG9wVXAuY2hpbGRyZW4oby5wb3BVcFNlbGVjdG9yKS5hdHRyKCdzdHlsZScsIGZ1bmN0aW9uIChpLCBzdHlsZSkge1xuXHRcdFx0XHRcdFx0aWYgKHR5cGVvZiBzdHlsZSAhPT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdFx0XHRcdFx0cmV0dXJuIHN0eWxlLnJlcGxhY2UoL2Rpc3BsYXlbXjtdKzs/L2csICcnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHQvLyByZXNldCAnY3VycmVudCcgcGF0aCBjbGFzc2VzXG5cdFx0XHRcdFx0by4kcGF0aC5yZW1vdmVDbGFzcyhvLmhvdmVyQ2xhc3MgKyAnICcgKyBjLmJjQ2xhc3MpLmFkZENsYXNzKG8ucGF0aENsYXNzKTtcblx0XHRcdFx0XHQkdGhpcy5maW5kKCcuJyArIG8uaG92ZXJDbGFzcykucmVtb3ZlQ2xhc3Moby5ob3ZlckNsYXNzKTtcblx0XHRcdFx0XHRvLm9uRGVzdHJveS5jYWxsKCR0aGlzKTtcblx0XHRcdFx0XHQkdGhpcy5yZW1vdmVEYXRhKCdzZk9wdGlvbnMnKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9LFxuXHRcdFx0aW5pdDogZnVuY3Rpb24gKG9wKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcdFx0aWYgKCR0aGlzLmRhdGEoJ3NmT3B0aW9ucycpKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHZhciBvID0gJC5leHRlbmQoe30sICQuZm4uc3VwZXJmaXNoLmRlZmF1bHRzLCBvcCksXG5cdFx0XHRcdFx0XHQkaGFzUG9wVXAgPSAkdGhpcy5maW5kKG8ucG9wVXBTZWxlY3RvcikucGFyZW50KCdsaScpO1xuXHRcdFx0XHRcdG8uJHBhdGggPSBzZXRQYXRoVG9DdXJyZW50KCR0aGlzLCBvKTtcblxuXHRcdFx0XHRcdCR0aGlzLmRhdGEoJ3NmT3B0aW9ucycsIG8pO1xuXG5cdFx0XHRcdFx0dG9nZ2xlTWVudUNsYXNzZXMoJHRoaXMsIG8sIHRydWUpO1xuXHRcdFx0XHRcdHRvZ2dsZUFuY2hvckNsYXNzKCRoYXNQb3BVcCwgdHJ1ZSk7XG5cdFx0XHRcdFx0dG9nZ2xlVG91Y2hBY3Rpb24oJHRoaXMpO1xuXHRcdFx0XHRcdGFwcGx5SGFuZGxlcnMoJHRoaXMsIG8pO1xuXG5cdFx0XHRcdFx0JGhhc1BvcFVwLm5vdCgnLicgKyBjLmJjQ2xhc3MpLnN1cGVyZmlzaCgnaGlkZScsIHRydWUpO1xuXG5cdFx0XHRcdFx0by5vbkluaXQuY2FsbCh0aGlzKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cdFx0fTtcblx0fSkoKTtcblxuXHQkLmZuLnN1cGVyZmlzaCA9IGZ1bmN0aW9uIChtZXRob2QsIGFyZ3MpIHtcblx0XHRpZiAobWV0aG9kc1ttZXRob2RdKSB7XG5cdFx0XHRyZXR1cm4gbWV0aG9kc1ttZXRob2RdLmFwcGx5KHRoaXMsIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSkpO1xuXHRcdH1cblx0XHRlbHNlIGlmICh0eXBlb2YgbWV0aG9kID09PSAnb2JqZWN0JyB8fCAhIG1ldGhvZCkge1xuXHRcdFx0cmV0dXJuIG1ldGhvZHMuaW5pdC5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuXHRcdH1cblx0XHRlbHNlIHtcblx0XHRcdHJldHVybiAkLmVycm9yKCdNZXRob2QgJyArICBtZXRob2QgKyAnIGRvZXMgbm90IGV4aXN0IG9uIGpRdWVyeS5mbi5zdXBlcmZpc2gnKTtcblx0XHR9XG5cdH07XG5cblx0JC5mbi5zdXBlcmZpc2guZGVmYXVsdHMgPSB7XG5cdFx0cG9wVXBTZWxlY3RvcjogJ3VsLC5zZi1tZWdhJywgLy8gd2l0aGluIG1lbnUgY29udGV4dFxuXHRcdGhvdmVyQ2xhc3M6ICdzZkhvdmVyJyxcblx0XHRwYXRoQ2xhc3M6ICdvdmVycmlkZVRoaXNUb1VzZScsXG5cdFx0cGF0aExldmVsczogMSxcblx0XHRkZWxheTogODAwLFxuXHRcdGFuaW1hdGlvbjoge29wYWNpdHk6ICdzaG93J30sXG5cdFx0YW5pbWF0aW9uT3V0OiB7b3BhY2l0eTogJ2hpZGUnfSxcblx0XHRzcGVlZDogJ25vcm1hbCcsXG5cdFx0c3BlZWRPdXQ6ICdmYXN0Jyxcblx0XHRjc3NBcnJvd3M6IHRydWUsXG5cdFx0ZGlzYWJsZUhJOiBmYWxzZSxcblx0XHRvbkluaXQ6ICQubm9vcCxcblx0XHRvbkJlZm9yZVNob3c6ICQubm9vcCxcblx0XHRvblNob3c6ICQubm9vcCxcblx0XHRvbkJlZm9yZUhpZGU6ICQubm9vcCxcblx0XHRvbkhpZGU6ICQubm9vcCxcblx0XHRvbklkbGU6ICQubm9vcCxcblx0XHRvbkRlc3Ryb3k6ICQubm9vcCxcblx0XHRvbkhhbmRsZVRvdWNoOiAkLm5vb3Bcblx0fTtcblxufSkoalF1ZXJ5LCB3aW5kb3cpO1xuIl19
