(function($){

    var featured        = $('#acf-field_5e31917fbaab0'),
        issue           = $('#acf-field_5e3191e2baab1'),
        deparment       = $('#acf-field_5e35f3dfc66b4'),
        currentIssue    = issue.val(),
        currentDept     = deparment.val();

    $(document).on('change','#acf-field_5e31917fbaab0',function(){
        if( ! $(this).is(':checked') && ( currentDept == null || currentDept == '' ) ){
            $('.term-block-submit').show();
        }else if( $(this).is(':checked') && currentIssue ){
            $('.term-block-submit').hide();
        }
    });

    $(document).on('change','#acf-field_5e3191e2baab1',function(){
        // console.log( 'issue ' + $(this).val() );

        currentIssue = $(this).val();

        if( featured.is(':checked') && currentIssue ){
            $('.term-block-submit').hide();
        }else if( currentIssue && currentDept ){
            $('.term-block-submit').hide();
        }
    });

    $(document).on('change','#acf-field_5e35f3dfc66b4',function(){
        // console.log( 'deparment ' + $(this).val() );
        currentDept = $(this).val();

        if( currentIssue && currentDept ){
            $('.term-block-submit').hide();
        }
    });

    $(document).on('click','.term-block-submit',function(){
        // console.log( 'issue ' + currentIssue );
        // console.log( 'deparment ' + currentDept );

        if( featured.is(':checked') && ( currentIssue == null || currentIssue == '' ) ){
            alert( 'Please select an Issue.' );
        }else{
            alert( 'Please select an Issue and a Department.' );
        }
    });


    $(window).on('load',function(){
        if( $('.post-new-php').length )
            var btnTarget = $('.editor-post-publish-panel__toggle');
        else
            var btnTarget = $('.editor-post-publish-button');

        if( $('.post-type-article').length ){
            btnTarget.parent('div').addClass('btn-holder');
            btnTarget.after('<span class="term-block-submit" style="display: none;"></span>');
            // console.log('testing load.....');
            if( featured.is(':checked') && ( currentIssue == null || currentIssue == '' ) ){
                btnTarget.next('.term-block-submit').show();
            }else if( ! featured.is(':checked') && ( currentIssue == null || currentIssue == '' || currentDept == null || currentDept == '' ) ){
                btnTarget.next('.term-block-submit').show();
            }
        }
    });


})(jQuery);
