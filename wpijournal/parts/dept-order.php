<?php
define('WP_USE_THEMES', false);
require( '../../../../wp-blog-header.php'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Department Articles Order</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../dept-order-style.css">
</head>
<body>

<?php
$currentIssue = wpi_journal_current_issue();
if( ! empty( $currentIssue ) ){
    $departments_page = get_field( 'departments_page', 'option' ); // post ID
    $currentOrder = get_field( 'articles_ids', $departments_page ); // string of comma separated post IDs
    
    // Make sure articles in $currentOrder belong to the current issue. If not, remove them.
    if( !empty($currentOrder) ){
        $currentOrderArticles = explode(',', $currentOrder); // convert to array, $currentOrder is a string
        $currentOrderArticlesChecked = array();
        
        if( is_array($currentOrderArticles) && !empty( $currentOrderArticles) ){
            foreach( $currentOrderArticles as $currentOrderArticle ){
                // check if the article belongs to current issue
                if( has_term($currentIssue->term_id, 'issue_cat', $currentOrderArticle) ){
                    $currentOrderArticlesChecked[] = $currentOrderArticle;
                }
            }
        }
        if( $currentOrder !== implode(',', $currentOrderArticlesChecked) ){
            $currentOrder = implode(',', $currentOrderArticlesChecked); // for query below
            update_field( 'articles_ids', $currentOrder, $departments_page); // update ACF field
        }
    }
    
    $arg = array( 'post_type' => 'article',
        'meta_query' => array(
            array(
                'key'   => 'feature_article',
                'value' => '1',
                'compare' => '!=',
            )
        ),
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'issue_cat',
                'field'    => 'term_id',
                'terms'    => $currentIssue->term_id
            ),
            array(
                'taxonomy' => 'department_cat',
                'field'    => 'slug',
                'terms'    => 'wpie',
                'operator' => 'NOT IN'
            )
        ),
        'posts_per_page' => '-1' );

    if( ! empty( $currentOrder ) ){
        $arg = array_merge( $arg, array( 'post__in' => explode( ',', $currentOrder ), 'orderby' => 'post__in' ) );
    }

    $articles = get_posts( $arg );

    $ids = array();
    if( $articles ){
        echo '<ol class="articles">';
        foreach( $articles as $item ){
            echo sprintf( '<li data-id="%s">%s - %s</li>', $item->ID, $item->ID, $item->post_title );
            $ids[] = $item->ID;
        }
        echo '</ol>';
    }else{
        echo '<h3>No articles found.</h3>';
    }
}else{
    echo '<h3>No articles found.</h3>';
} ?>

<script src="/wp-includes/js/jquery/jquery.js"></script>
<script src='../assets/scripts/jquery-sortable.min.js'></script>
<script>
jQuery(function($){
    var ids = [],
        articles = $('.articles'),
        frameHeight = articles.outerHeight() + 25;

    $('#dept-order', window.parent.document).css('height', frameHeight + 'px');

    articles.sortable({
        vertical: true,
        onDrop: function  ($item, container, _super) {
            ids = [];
            $item.removeAttr('class').removeAttr('style');
            articles.children('li').each(function(){
                ids.push( $(this).data('id') );
            });
            articles.children('li').removeAttr('style');
            // console.log( ids );
            if( ids.length ) $('#article-ids input', window.parent.document).empty().val( ids.join() );
        }
    });
});
</script>
</body>
</html>