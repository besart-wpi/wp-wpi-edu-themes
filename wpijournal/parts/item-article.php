                        <?php
                        $department = get_the_terms( $post_id, 'department_cat' );
                        $issue = get_the_terms( $post_id, 'issue_cat' );
                        $feature_article = get_field( 'feature_article' );
                        $order = get_post_meta( $post_id, 'menu_order', true ); ?>
                        <div class="item item-dept-<?php echo sanitize_key( $department[0]->slug ) ?>">
                            <figure>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php if( $youtube_video_id ){ ?>
                                        <span class="icon"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/play.svg" class="svg" alt=""></span>
                                    <?php } ?>
                                    <span class="sr"><?php the_title(); ?></span>
                                </a>
                                <?php echo $image ? wp_get_attachment_image( $image[ 'ID' ], 'thumb-highlight' ) : ''; ?>
                            </figure>
                            <div class="text">
                                <?php
                                if( $issue || $department || $feature_article ){ ?>
                                    <div class="meta">
                                        <?php if( $department ){ ?>
                                            <a href="<?php echo get_term_link( $department[0]->slug, 'department_cat' ) ?>" title="<?php echo $department[0]->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department[0]->slug ) ?>">
                                                <span><?php echo $department[0]->name; ?></span>
                                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-left.svg" alt="left bracket" class="svg"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-right.svg" alt="right bracket" class="svg">
                                            </a>
                                        <?php } ?>
                                        
                                        <?php if( $feature_article ){ ?>
                                            <span class="bracket">
                                                <span>Feature</span>
                                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-left.svg" alt="left bracket" class="svg"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-right.svg" alt="right bracket" class="svg">
                                            </span>
                                        <?php } ?>

                                        <?php $currentIssue = wpi_journal_current_issue();
                                        if( $issue ){
                                            if( $issue[0]->term_id == $currentIssue->term_id )
                                                $issueTitle = 'Current Issue';
                                            else
                                                $issueTitle = $issue[0]->name; ?>
                                            <a href="<?php echo get_term_link( $issue[0]->term_id, 'issue_cat' ); ?>" title="<?php echo $issueTitle; ?>"><?php echo $issueTitle; ?></a>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                <?php echo $subheading ? sprintf( '<p>%s</p>', $subheading ) : ''; ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="link-arrow">Read Story</a>
                            </div>
                        </div>