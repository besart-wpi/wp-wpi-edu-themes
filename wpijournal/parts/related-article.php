<?php
    $related_article = get_field( 'related_article' );
    if( $related_article ){
        $heading            = $related_article->post_title;
        $subheading         = $related_article->post_excerpt ? $related_article->post_excerpt : wp_trim_words( $related_article->post_content, 16, '...' );
        $link               = get_permalink( $related_article->ID );
        $image              = false;
        $youtube_video_id   = false;
        
        $hero_slides        = get_field( 'hero_slides', $related_article->ID );

        if( $hero_slides ){
            $image              = $hero_slides[0][ 'image' ];
            $youtube_video_id   = $hero_slides[0][ 'youtube_video_id' ];
        } ?>
        <section class="section highlight">
            <div class="wrap">
                <figure>
                    <?php if( $youtube_video_id ){ ?>
                        <a data-fancybox="" href="//www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>" class="play-video"><span class="icon"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/play.svg" class="svg" alt=""></span></a>
                    <?php }elseif( $link ){ ?>
                        <a href="<?php echo $link; ?>" title="<?php echo $heading; ?>"><span class="sr"><?php echo $heading; ?></span></a>
                    <?php } ?>
                    <?php echo $image ? wp_get_attachment_image( $image[ 'ID' ], 'thumb-highlight' ) : ''; ?>
                </figure>
                <div class="text">
                    <?php
                    $department = get_the_terms( $related_article->ID, 'department_cat' );
                    $issue = get_the_terms( $related_article->ID, 'issue_cat' );
                    if( $issue || $department ){ ?>
                        <div class="meta">
                            <?php if( $department ){ ?>
                                <a href="<?php echo get_term_link( $department[0]->slug, 'department_cat' ) ?>" title="<?php echo $department[0]->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department[0]->slug ) ?>">
                                    <span><?php echo $department[0]->name; ?></span>
                                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-left.svg" alt="left bracket" class="svg"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-right.svg" alt="right bracket" class="svg">
                                </a>
                            <?php } ?>

                            <?php $currentIssue = wpi_journal_current_issue();
                            if( $issue ){
                                if( $issue[0]->term_id == $currentIssue->term_id )
                                    $issueTitle = 'Current Issue';
                                else
                                    $issueTitle = $issue[0]->name; ?>
                                <a href="<?php echo get_term_link( $issue[0]->term_id, 'issue_cat' ); ?>" title="<?php echo $issueTitle; ?>"><?php echo $issueTitle; ?></a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php echo $heading ? sprintf( '<h2><a href="%s" title="%s">%s</a></h2>', $link, $heading, $heading ) : ''; ?>
                    <?php echo $subheading ? sprintf( '<p>%s</p>', $subheading ) : ''; ?>
                    <a href="<?php echo $link; ?>" class="link-arrow" title="<?php echo $heading; ?>">Read Story</a>
                </div>
            </div>
        </section>
    <?php } ?>