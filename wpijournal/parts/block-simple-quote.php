<?php
/**
 * Block Name: Simple Quote Block
 *
 * This is the template that displays the simple quote block
 */

// create id attribute for specific styling
$id = 'simple-quote-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

$quote = get_field( 'quote' ); ?>

            <?php if( $quote ){ ?>
                <div class="post-item quote-custom <?php echo $align_class; ?>" id="<?php echo $id; ?>">
                    <blockquote><p>&ldquo;<?php echo $quote; ?>&rdquo;</p></blockquote>
                </div>
            <?php } ?>