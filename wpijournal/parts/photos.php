<?php
$photos         = get_field( 'photos' );
$autoplay       = get_field( 'autoplay' );
$autoplay_speed = get_field( 'autoplay_speed' );
if( $photos ){ ?>
    <section class="section gallery">
        <div class="wrap">
            <div class="gal-arrows"></div>
            <div class="gal-slider" data-autoplay="<?php echo $autoplay ? 'true' : 'false'; ?>" data-speed="<?php echo $autoplay_speed; ?>">
                <?php foreach( $photos as $photo ){ ?>
                    <div class="item">
                        <div>
                            <figure>
                                <?php echo $photo[ 'link_url' ] ? sprintf( '<a href="%s" title="%s"><span class="sr">%s</span></a>', $photo[ 'link_url' ], esc_attr( strip_tags( $photo[ 'caption' ] ) ), esc_attr( $photo[ 'caption' ] ) ) : ''; ?>
                                <?php echo $photo[ 'photo' ] ? wp_get_attachment_image( $photo[ 'photo' ][ 'ID' ], 'thumb-gallery' ) : ''; ?>
                            </figure>
                            <div class="caption">
                                <?php echo $photo[ 'caption' ] ? sprintf( '<p>%s</p>', $photo[ 'caption' ] ) : ''; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <section class="section mobile-gallery"></section>
<?php } ?>