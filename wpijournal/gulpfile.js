// Require our dependencies
const autoprefixer = require( 'autoprefixer' );
const babel = require( 'gulp-babel' );
const bourbon = require( 'bourbon' ).includePaths;
const browsersync = require( 'browser-sync' ).create();
const cheerio = require( 'gulp-cheerio' );
const concat = require( 'gulp-concat' );
const cssnano = require( 'gulp-cssnano' );
const del = require( 'del' );
const eslint = require( 'gulp-eslint' );
const fs = require( 'fs' );
const gulp = require( 'gulp' );
const gutil = require( 'gulp-util' );
const mqpacker = require( 'css-mqpacker' );
const neat = require( 'bourbon-neat' ).includePaths;
const notify = require( 'gulp-notify' );
const plumber = require( 'gulp-plumber' );
const postcss = require( 'gulp-postcss' );
const rename = require( 'gulp-rename' );
const sass = require( 'gulp-sass' );
const sassdoc = require( 'sassdoc' );
const sassLint = require( 'gulp-sass-lint' );
const sort = require( 'gulp-sort' );
const sourcemaps = require( 'gulp-sourcemaps' );
const uglify = require( 'gulp-uglify' );

// Set assets paths.
const paths = {
	'css': [ './*.css', '!*.min.css' ],
	'html': [ './*.html', './**/*.html' ],
	'php': [ './*.php', './**/*.php' ],
	'sass': 'assets/sass/**/*.scss',
	'concat_scripts': 'assets/scripts/concat/*.js',
	'concat_plugin_scripts': 'assets/scripts/plugins/*.js',
	'scripts': [ 'assets/scripts/*.js', '!assets/scripts/*.min.js' ]
};

/**
 * Handle errors and alert the user.
 */
function handleErrors() {
	const args = Array.prototype.slice.call( arguments );

	notify.onError( {
		'title': 'Task Failed [<%= error.message %>',
		'message': 'See console.',
		'sound': 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
	} ).apply( this, args );

	gutil.beep(); // Beep 'sosumi' again.

	// Prevent the 'watch' task from stopping.
	this.emit( 'end' );
}

/**
 * Delete style.css and style.min.css before we minify and optimize
 */
function clean() {
	return del([ 'style.css', 'style.min.css' ]);
}

/**
 * Compile Sass and run stylesheet through PostCSS.
 *
 * https://www.npmjs.com/package/gulp-sass
 * https://www.npmjs.com/package/gulp-postcss
 * https://www.npmjs.com/package/gulp-autoprefixer
 * https://www.npmjs.com/package/css-mqpacker
 */
function post_css() {
	return gulp.src( 'assets/sass/*.scss', paths.css )

		// Deal with errors.
		.pipe( plumber( {'errorHandler': handleErrors} ) )

		// Wrap tasks in a sourcemap.
		.pipe( sourcemaps.init() )

			// Compile Sass using LibSass.
			.pipe( sass( {
				'includePaths': [].concat( bourbon, neat ),
				'errLogToConsole': true,
				'outputStyle': 'expanded' // Options: nested, expanded, compact, compressed
			} ) )

			// Parse with PostCSS plugins.
			.pipe( postcss( [
				autoprefixer( {
					'browserlist': ['last 2 versions', 'iOS >= 7', 'ie >= 11' ]
				} ),
				mqpacker( {
					'sort': true
				} )
			] ) )

		// Create sourcemap.
		.pipe( sourcemaps.write() )

		// Create style.css.
		.pipe( gulp.dest( './' ) )
		.pipe( browsersync.stream() );
}

/**
 * Minify and optimize style.css.
 *
 * https://www.npmjs.com/package/gulp-cssnano
 */
function css_nano() {
	return gulp.src( 'style.css' )
		.pipe( plumber( {'errorHandler': handleErrors} ) )
		.pipe( cssnano( {
			autoprefixer: {browserlist: [ 'last 2 versions', 'iOS >= 7', 'ie >= 11' ]},
			safe: true
		 } ) )
		.pipe( rename( 'style.min.css' ) )
		.pipe( gulp.dest( './' ) )
		.pipe( browsersync.stream() )
		// .pipe( notify({ message: 'css_nano task complete' }));
}

/**
 * Concatenate and transform JavaScript.
 *
 * https://www.npmjs.com/package/gulp-concat
 * https://github.com/babel/gulp-babel
 * https://www.npmjs.com/package/gulp-sourcemaps
 */
// gulp.task( 'concat_plugins', () =>
function concat_plugins() {
	return ( 
		gulp.src( paths.concat_plugin_scripts )
		// Start a sourcemap.
		.pipe( sourcemaps.init() )
		// Concatenate partials into a single script.
		.pipe( concat( 'plugins.js' ) )
		// Append the sourcemap to project.js.
		.pipe( sourcemaps.write() )
		// Save project.js
		.pipe( gulp.dest( 'assets/scripts' ) )
		.pipe( browsersync.stream() )
		// .pipe( notify({ message: 'concat_plugins task complete' }));
	);
}

/**
  * Minify compiled JavaScript.
  *
  * https://www.npmjs.com/package/gulp-uglify
  */
function uglify_js() {
	return (
		gulp.src( paths.scripts )
		.pipe( plumber( {'errorHandler': handleErrors} ) )
		.pipe( rename( {'suffix': '.min'} ) )
		.pipe( babel() )
		.pipe( uglify({
			'mangle': false
		}) )
		.pipe( gulp.dest( 'assets/scripts' ) )
		.pipe( browsersync.stream() )
		// .pipe( notify({ message: 'uglify_js task complete' }))
	);
}

// BrowserSync
function browserSync(done) {
	browsersync.init({
		'open': false,             // Open project in a new tab?
		'injectChanges': true,     // Auto inject changes instead of full reload.
		'proxy': 'http://wpi.localhost/', 
		'watchOptions': {
			'debounceDelay': 1000  // Wait 1 second before injecting.
		}
	});
	done();
}

// BrowserSync Reload
function browserSyncReload(done) {
	browsersync.reload();
	done();
}

/**
 * Process tasks and reload browsers on file changes.
 *
 * https://www.npmjs.com/package/browser-sync
 */
// gulp.task( 'watch', function() {
function watchFiles() {
	// Run tasks when files change.
	gulp.watch( paths.sass, gulp.series(clean, post_css, css_nano) );
	gulp.watch( paths.scripts, uglify_js );
	gulp.watch( paths.concat_plugin_scripts, concat_plugins  );
	gulp.watch( paths.php, browserSyncReload );
	gulp.watch( paths.html, browserSyncReload );
}

const watch = gulp.parallel(watchFiles, browserSync);

exports.watch = watch;
exports.scripts = gulp.series(concat_plugins, uglify_js);
exports.styles = gulp.series(clean, post_css, css_nano);
